inherit "/std/room";

void setup()
{
	set_short("Comedor Gigantoso");
	set_long("Si, si. Muy lindo todo pero antes de describir cualquier cosa vamos a crear las rooms.\n");

	add_exit("norte", "/w/hazrakh/jorhun/poblado/poblado4", "corridor");
	add_exit("oeste", "/w/hazrakh/jorhun/poblado/poblado3", "corridor");
	add_exit("este", "/w/hazrakh/jorhun/poblado/poblado5", "corridor");
	add_exit("sur", "/w/hazrakh/jorhun/poblado/cocinas", "corridor");
	fijar_luz(50);
}
