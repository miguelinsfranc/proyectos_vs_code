inherit "/std/room";

void setup()
{
	set_short("Poblado Gigantoso");
	set_long("Si, si. Muy lindo todo pero antes de describir cualquier cosa vamos a crear las rooms.\n");

	add_exit("sudeste", "/w/hazrakh/jorhun/poblado/poblado7", "path");
	add_exit("noroeste", "/w/hazrakh/jorhun/poblado/poblado5", "path");
	add_exit("sudoeste", "/w/hazrakh/jorhun/poblado/armeria", "path");
	fijar_luz(50);
}
