inherit "/std/room";

void setup()
{
	set_short("Animales Gigantoso");
	set_long("Si, si. Muy lindo todo pero antes de describir cualquier cosa vamos a crear las rooms.\n");

	add_exit("noroeste", "/w/hazrakh/jorhun/poblado/poblado7", "path");
	add_exit("oeste", "/w/hazrakh/jorhun/poblado/cultivos", "corridor");
	add_exit("sudoeste", "/w/hazrakh/jorhun/poblado/poblado8", "corridor");
	fijar_luz(50);
}
