inherit "/std/room";

void setup()
{
	set_short("Camino Gigantoso");
	set_long("Si, si. Muy lindo todo pero antes de describir cualquier cosa vamos a crear las rooms.\n");

	add_exit("sudeste", "/w/hazrakh/jorhun/poblado/camino3", "escalable");
	add_exit("noroeste", "/w/hazrakh/jorhun/poblado/templofuego", "escalable");
	fijar_luz(50);
	fijar_altura(34);
}
