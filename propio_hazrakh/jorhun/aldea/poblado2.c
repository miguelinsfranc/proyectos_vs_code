
inherit "/std/room";

void setup()
{
	set_short("Poblado Gigantoso");
	set_long("Si, si. Muy lindo todo pero antes de describir cualquier cosa vamos a crear las rooms.\n");

	add_exit("noreste", "/w/hazrakh/jorhun/poblado/poblado3", "path");
	add_exit("sudoeste", "/w/hazrakh/jorhun/poblado/poblado1", "path");
	add_exit("sudeste", "/w/hazrakh/jorhun/poblado/sauna1", "corridor");
	fijar_luz(50);
}
