inherit "/std/room";

void setup()
{
	set_short("Poblado Gigantoso");
	set_long("Si, si. Muy lindo todo pero antes de describir cualquier cosa vamos a crear las rooms.\n");

	add_exit("sudeste", "/w/hazrakh/jorhun/poblado/animales", "path");
	add_exit("noroeste", "/w/hazrakh/jorhun/poblado/poblado6", "path");
	fijar_luz(50);
}
