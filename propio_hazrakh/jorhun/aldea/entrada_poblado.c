inherit "/std/room";

void setup()
{
	set_short("Entrada montaña");
	set_long("Si, si. Muy lindo todo pero antes de describir cualquier cosa vamos a crear las rooms.\n");

	add_exit("noreste", "/w/hazrakh/jorhun/poblado/poblado1", "path");
	add_exit("abajo", "/w/hazrakh/jorhun/poblado/base_montaña", "path");
	fijar_luz(50);
}
