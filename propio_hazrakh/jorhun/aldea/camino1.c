inherit "/std/room";

void setup()
{
	set_short("Camino Gigantoso");
	set_long("Si, si. Muy lindo todo pero antes de describir cualquier cosa vamos a crear las rooms.\n");

	add_exit("noreste", "/w/hazrakh/jorhun/poblado/poblado10", "standard");
	add_exit("noroeste", "/w/hazrakh/jorhun/poblado/camino2", "escalable");
	fijar_luz(50);
	fijar_altura(4);
}
