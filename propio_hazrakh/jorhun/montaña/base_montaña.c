inherit "/std/outside";

void setup_room()
{
	set_short("Montaña Glødbjarg: Sendero Natural");
	set_long(
		"A medida que te adentras en las laderas solitarias de la montaña, la majestuosidad de la tierra agreste se despliega ante tus ojos. No hay montañas cercanas que distraigan la vista, permitiéndote sumergirte por completo en la serenidad austera de este paisaje salvaje y virgen.\n"
		"El sendero de ascenso se despliega ante ti, serpenteando de forma natural por las laderas escarpadas de la montaña. Cada paso que das se hunde en el suelo firme y compacto, carente de nieve en esta etapa temprana del ascenso. La tierra seca y rocosa se convierte en tu guía confiable a medida que avanzas, proporcionando una base sólida en tu travesía.\n"
		"A tu alrededor, la vegetación es escasa y resistente, adaptada a las condiciones rigurosas de la montaña. Pequeñas matas de hierba se asoman entre las grietas de las rocas, resistiendo valientemente los embates del viento y las inclemencias del clima. Puedes observar algunas flores silvestres de colores tenues que se aferran a la vida, dotando al paisaje de pequeños destellos de belleza en medio de su dureza.\n"
		"Las rocas que salpican la ladera de la montaña están cubiertas de musgo y líquenes, testigos silenciosos del paso de los siglos. Puedes notar cómo algunas rocas se desprendieron del terreno y quedaron suspendidas en el aire, creando pequeños salientes y aleros que brindan refugio a la vida silvestre local.\n");
}
