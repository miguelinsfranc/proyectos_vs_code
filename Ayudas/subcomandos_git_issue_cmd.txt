
Por ejemplo, para listar todas las incidencias existentes se puede utilizar el comando:
 
git issue list
Para crear una nueva incidencia, se puede utilizar el comando:
 
git issue create "Título de la incidencia" -m "Descripción de la incidencia"
En este caso, se debe reemplazar "Título de la incidencia" y "Descripción de la incidencia" por el título y la descripción correspondientes de la nueva incidencia.

• git issue close <número>: cierra la incidencia especificada por <número>.
• git issue reopen <número>: reabre la incidencia especificada por <número>.
• git issue show <número>: muestra información detallada de la incidencia especificada por <número>.
• git issue comment <número> -m "Comentario": agrega un comentario a la incidencia especificada por <número>.
• git issue assign <número> <usuario>: asigna la incidencia especificada por <número> al usuario especificado por <usuario>.
• git issue unassign <número>: desasigna la incidencia especificada por <número>.

• git issue edit <número>: permite editar la información de una incidencia existente especificada por <número>.
• git issue label <número> <etiqueta>: agrega una etiqueta a la incidencia especificada por <número>.
• git issue unlabel <número> <etiqueta>: remueve una etiqueta de la incidencia especificada por <número>.
• git issue milestone <número> <hitos>: agrega una meta a la incidencia especificada por <número>.
• git issue unmilestone <número> <hitos>: remueve una meta de la incidencia especificada por <número>.