// Neldan: 26.1.2004
// espada de kregg pa la quest del campamento

inherit "/obj/arma";

void setup() {
	
	set_base_weapon("cimitarra larga");
	set_name("espada");
	add_alias(({"espada","kregg"}));
	add_plural(({"espadas","kregg"}));
	set_short("Espada de Kregg");
	set_main_plural("Espadas de Kregg");
	set_long("Es una enorme espada con un mango grisaceo y una hoja "
		"larga y mellada con varios simbolos grabados en ella. Esta es "
		"la espada mas grande que has visto en tu vida. Su hoja es como un "
		"elfo de alta y supones el gran poder destructivo que posee.\n");
	fijar_encantamiento(1);

	set_read_mess("Esta espada pertenece a Kregg\n");

}
