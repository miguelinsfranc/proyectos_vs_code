// Neldan 2.2.04
// Jefe del campamento de los bandidos de Ostigurth
// Sierephad Ene 2k20	-- 	Revision rapida
//						-- 	Mejora del defender
// salida
//						-- 	quitar tesoro "extra" al morir
//y fijado con fijar_objeto_morir
//						--	Añadido a resets de
//anduar
/// d/anduar/respwns.c

#define SALIDA "este" // Salida que defiende para evitar que los players escapen

inherit "/obj/monster.c";
#include "../path.h"
void setup()
{
    set_name("kregg");
    add_alias(({"kregg", "semi-orco"}));
    add_plural(({"kregg", "semi-orcos"}));
    set_short("Kregg, el Semi-orco");
    set_main_plural("Jefes bandidos");
    set_long("Ves a un hombre de grandes dimensiones y musculatura, no te "
             "cuesta detectar "
             "su herencia de orco por la fea nariz de cerdo y sus afilados "
             "dientes, aun así parece "
             "poseer gran inteligencia y astucia. Al mirar sus ojos no puedes "
             "evitar sentir miedo "
             "delante de él. Kregg es el líder de los Merodeadores.\n");

    fijar_raza("semi-orco");
    fijar_clase("soldado");

    fijar_alineamiento(200);
    fijar_genero(1);

    fijar_fue(40);
    fijar_extrema(100);
    fijar_carac("des", 30);
    fijar_carac("con", 20);
    fijar_carac("int", 16);
    fijar_carac("sab", 10);
    fijar_carac("car", 12);

    set_aggressive(1);
    set_join_fight_mess(
        query_short() + " dice: ¡Fuera de aqui, sucio bastardo!\n");

    add_attack_spell(30, "jugarreta", 3);
    add_attack_spell(40, "golpecertero", 3);
    add_attack_spell(40, "golpeo", 3);
    add_attack_spell(40, "ataquedoble", 3);

    load_a_chat(
        60,
        ({
            "'JaJAja, ¿estos son los poderosos guerreros que manda Anduar para "
            "que les ayuden?",
            "'¡Os arrepentiréis de haber venido a molestarme!",
        }));

    add_clone(ITEMS + "espada_kregg.c", 1);
    add_clone("/baseobs/armaduras/brazalete_izq.c", 1);
    add_clone("/baseobs/armaduras/brazalete.c", 1);
    add_clone("/baseobs/armaduras/guantelete_izq.c", 1);
    add_clone("/baseobs/armaduras/guantelete.c", 1);
    add_clone("/baseobs/armaduras/completa.c", 1);
    add_clone("/baseobs/armaduras/gran_yelmo.c", 1);
    add_clone("/baseobs/armaduras/grebas_metalicas.c", 1);
    add_clone("/baseobs/armaduras/botas_guerra.c", 1);

    habilidad("concentracion", TO);

    fijar_estatus("Anduar", -200);
    fijar_nivel(60 + random(10));
    set_max_hp(20000);
    ajustar_dinero(1000, "cobre");
    init_equip();
    fijar_objetos_morir(({"tesoro", "varios", ({1, 2}), 1}));
    fijar_objetos_morir(({"tesoro", "arma", ({3, 6}), 1}));
    fijar_objetos_morir(({ITEMS + "espada_kregg.c"}));
    fijar_objetos_morir(({BDISENYOS + "compendio_ogro.c", 1, 10}));
}

int dame_defendiendo_salida()
{
    if (query_property("defender") || query_static_property("defender")
        //|| -1 != member_array(SALDA,ENV(TO)->keys(dame_defensores()))
    )
        return 1;

    return 0;
}

void attack_ob(object victima)
{
    ::attack_ob(victima);
    if (!dame_defendiendo_salida())
        do_command("defender " + SALIDA);
}

void attack_by(object atacante)
{
    ::attack_by(atacante);
    if (!dame_defendiendo_salida())
        do_command("defender " + SALIDA);
    /*
    if(!TO->query_static_property("defendiendo"))
    {
            //do_command("defender norte");
            do_command("defender este");
            TO->add_static_property("defendiendo",1);
    }
    */
}

int do_death(object asesino)
{

    if (asesino) {
        write_file(
            LOGS + "kregg_die.log",
            asesino->QCN + " [" + asesino->dame_nivel() + "] me mato " +
                (sizeof(dame_enemigos_recientes())
                     ? "ayudado por " +
                           query_multiple_short(dame_enemigos_recientes()) + " "
                     : "") +
                "el " + ctime() + ".\n");
    }
    asesino->nuevo_hito_mision("anduar_recompensa_terni", "muerte_kregg", 0);
    // fijar_tesoro(3 + random(4), "arma");
    return ::do_death(asesino);
}
