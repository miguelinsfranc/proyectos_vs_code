//Durmok 31/05/2003
//Snaider 26-junio-2007 : Pongo quest para conseguir puntos extra en la alianza
//Sierephad Abr 2021	--	Arreglando el mensaje de set_join_fight_mess

#include "/d/kheleb/path.h"
#define HA "/grupos/gremios/alianza.c"
inherit "/obj/conversar_npc.c";

int dame_guardia() { return 1; }

void setup()
{
    string *info;
    int i;
    
    set_name("oficial");
    set_short("Dathor, oficial de la Alianza");
    set_main_plural("Oficiales de la Alianza");
    set_long("Este es uno de los veteranos oficiales de la Alianza. Tras cumplir con numerosas misiones "
      "para beneficio y protección de los intereses de la Alianza, y tras acumular multitud de "
      "cicatrices, se encuentra alistando a las futuras promesas, a los nuevos defensores de la "
      "Alianza.\n");
    add_alias(({"oficial","dathor"}));
    add_plural("oficiales");
    fijar_nivel(60+random(10));
    fijar_clase("khazad");
    fijar_raza("enano");
    fijar_religion("eralie");
    fijar_fe(30);
    set_random_stats(16,18);
    fijar_fue(20);
    fijar_alineamiento(10000);
    fijar_genero(1);
    fijar_memoria_jugador(1);
    add_hated("bando",({"malo","renegado"}));
         add_hated("enemigo",({"Kheleb","Kattak"}));
    set_aggressive(5);
	//set_join_fight_mess("Dathor exclama: ¡Preparate a morir! ¡Por Darin!\n");
	set_join_fight_mess("'¡Preparate a morir! ¡Por Darin!\n");
    fijar_cobardia(0);
    ajustar_dinero(random(2)+2,"khaldan");
    add_clone(ARMADURAS+"armadura_alianza",1);
    add_clone(BARMADURAS+"yelmo.c",1);
    add_clone(BARMADURAS+"guantelete.c",1);
    add_clone(BARMADURAS+"botas.c",1);
    //add_clone(ARMADURAS+"brazal_alianza",1);
    add_clone(BARMAS+"martillo_de_guerra.c",2);
    add_clone(BARMADURAS+"grebas_metalicas",1);
    fijar_ciudadania("kattak",500);
    anyadir_lenguaje("adurn",100);
    fijar_estatus("Kheleb",500);
    fijar_estatus("Kattak",500);
    init_equip();
    add_attack_spell(100,"furia",0);
    add_attack_spell(100,"embestir",4);
    add_attack_spell(100,"cabezazo",4);
    add_attack_spell(100,"golpecertero",4);
    //fijar_maestrias((["martillo de guerra":100]));
    fijar_maestrias((["Aplastantes medias":100]));
    fijar_pvs_max(20000);

#include AGRESIVO

     info = explode("/grupos/gremios/alianza"->dame_ayuda(),".");
     for(i = 0;i<sizeof(info);i++)
          info[i]= "'"+info[i]+".";
     info-=({info[i-1]});
          
     nuevo_dialogo("nombre",({"'Mi nombre es Dathor y soy el viejo oficial de la Alianza de Darin. Mi trabajo "
     "consiste en administrar el papeleo del gremio.","'¿Buscas información sobre nosotros?"}));
     nuevo_dialogo("informacion",info,"nombre");
     deshabilitar("kobolds","hakesh_entregados");
     deshabilitar("enanos","cabeza_entregada");
     prohibir("misiones","chequeo_alianza");
     nuevo_dialogo("misiones",({"'Para conseguir reconocimiento en el gremio de la Alianza de Darin puedes realizar varias aventuras o misiones de diferente riesgo.",
          ":se ríe.",
          "'Completa alguna de ellas y tu trabajo será reconocido al final de la temporada."}));
     nuevo_dialogo("aventuras",({"'Varios enemigos acechan nuestras tierras.",
     "'Un grupo de kobolds se han asentado en el Bosque Moribundo, no podemos dejar que esos sucios animales campen a sus aires.",
     ":suspira.",
     "'Sin embargo, ahora nuestro principal trabajo es el de explorar el interior de Kheleb Dum, hemos descubierto la existencia de un campamento de enanos oscuros en las profundidades de las cavernas."}),"misiones");
     nuevo_dialogo("kobolds",({"'Hace un tiempo un grupo de kobolds se adentraron en uno de nuestros bosques.",
     "'Desconocemos su origen, al igual que la causa de su aparición.",
     "'Algunos dicen que un secreto divino está apunto de ser revelado... o que su magia es la causante de la invasión de setas en Zelthaim...",
     "'Sea cual sea la causa, no podemos dejar que sigan reproduciéndose a sus anchas.",
     "'Ve allí y acaba con ellos...",
     "recibir_objeto"}),"aventuras");    
     
     nuevo_dialogo("enanos",({"'Tenemos que ser rápidos.",
     "'Al parecer, un reducido número de enanos oscuros han emergido a niveles superiores de las cavernas de Kheleb Dum.",
     "'Están liderados por Arkum, un conocido duergar.",
     "'Desconocemos la razón de su ascenso, pero no podemos dejar que se extiendan a sus anchas, tenemos que acabar con ellos.",
     "recibir_cabeza"}),"aventuras");     
 
     habilitar_conversacion();
}
int chequeo_alianza(object jugador)
{
    return jugador->dame_gremio()=="alianza";
}
int frases_personalizables(string tipo, object npc, object pj)
  {
    switch(tipo)
      {
        case "bienvenida-npc":
          if(pj->query_static_property("aconsejado_alistarse"))
          {
            do_say("Hola "+pj->dame_nombre_completo()+", ¿has venido a alistarte?",0);
            pj->ajustar_xp(1000);
            pj->remove_static_property("aconsejado_alistarse");
          }
          else return 0;
          break;
        //Sacara el resto de mensajes por defecto para el resto
        default:
        return 0;
      }
     return 1;
  }
void attack_by(object atacante)
{
    ::attack_by(atacante);
    atacante->add_timed_property("perseguido_kd",1,360);
    if(!query_timed_property("aviso_alianza"))
    {
	    add_timed_property("aviso_alianza",1,25);
	    "/grupos/gremios/alianza.c"->info_canal("Dathor","%^BOLD%^%^RED%^¡¡ La fortaleza esta siendo atacada !! ¡¡ A las armas soldados !!");
    }
}
void dar_recompensa(object jugador)
{
  do_say(({"Muy bien","Estupendo","Maravilloso","Esplendido"})[random(4)]+" "+jugador->query_short()+", la Alianza está orgullosa de ti, tu trabajo será reconocido.",0);
  tell_object(jugador,query_short()+" te entrega una bolsa con monedas.\n");
  tell_accion(environment(),query_short()+" entrega a "+jugador->query_short()+" una bolsa con monedas.\n","",({jugador}),jugador);

  if(HA->dame_rango(jugador->query_name())<2)
    HA->ajustar_puntos(jugador->query_name(),4,"colaboracion");
  jugador->ajustar_dinero(24,"khaldan");
  jugador->ajustar_estatus("Kheleb",2);
  jugador->ajustar_estatus("Kattak",2);         
	jugador->ajustar_xp(5500);
}
  
void recibir_objeto(object jugador)
  {
    object *objeto=({});
    int i;
    if(!jugador || environment(jugador)!=environment()) return;
    
    
    if(jugador->query_timed_property("hakesh_entregados"))
	    { do_say("Pero hace tiempo que realizaste esa misión, espera un poco antes de volver a realizarla.",0); return; }

    i=15;
    foreach(object ob in find_match("hakeshes",jugador))
      {
        if(sizeof(objeto)==i) break;
        objeto+=({ob});
      }
    if(sizeof(objeto) < i)
      { do_say("Tráeme 15 Hakesh para demostrar tu valor y de esa manera completarás la misión.",0); return; }
       
	  tell_object(jugador,"Entregas "+i+" "+objeto[0]->query_plural()+" a "+query_short()+".\n");
	  tell_accion(environment(),jugador->query_short()+" entrega "+i+" "+objeto[0]->query_plural()+" a "+query_short()+".\n","",({jugador}),jugador);
	  objeto->dest_me();
    jugador->add_timed_property("hakesh_entregados",1,3600);
      
  dar_recompensa(jugador);
}
  void recibir_cabeza(object jugador)
  {
    object *cabezas,cabeza;
    int i;
    if(!jugador || environment(jugador)!=environment()) return;
  
    if(jugador->query_timed_property("cabeza_entregada"))
	    { do_say("Pero hace tiempo que realizaste esa misión, espera un poco antes de volver a realizarla.",0); return; }

  	if(!i= sizeof(cabezas= find_match("cabezas",jugador)))
	     { do_say("Tráeme la cabeza de Arkum, el Lider Duergar y te recompensaré.",0); return; }
    while(i--)
        if(cabezas[i]->dame_duenyo() == "arkum") cabeza = cabezas[i];
    if(!cabeza)
       { do_say("Tráeme la cabeza de Arkum, el Lider Duergar y te recompensaré.",0); return; }
        
	  tell_object(jugador,"Entregas "+cabeza->query_short()+" a "+query_short()+".\n");
	  tell_accion(environment(),jugador->query_short()+" entrega "+cabeza->query_short()+" a "+query_short()+".\n","",({jugador}),jugador);
	  cabeza->dest_me();
    jugador->add_timed_property("cabeza_entregada",1,3600);
    dar_recompensa(jugador);
  }

