// Gareth 09.11.03
// Viajeros para Caminos
// Dominio de Urlom
// Dalion 01.02.2012 -> Revisando...
/*
    Zoilder 14/02/2012:
        - Le meto -300 de estatus a los bandidos para que al matarles den un punto al menos de estatus en dicha ciudad.

    Eckol 02Abr18: Bomba humo (bandidos, componente truco ladrón).
*/

inherit "/obj/monster";

#include "/d/urlom/path.h"

void setup_rol_comun()
{
    fijar_alineamiento(-200);
    fijar_religion("eralie");
    fijar_bando("bueno");
    fijar_ciudadania("ak'anon", 300);
    add_hated("enemigo", "ak'anon");
    add_hated("relacion", "ak'anon");
    set_aggressive(6);
}
void setup() {
    int i = roll(1, 4);

    switch(i)
    {
    case 1:
        set_name("enano");
        set_short("Enano Viajero");
        set_main_plural("Enanos Viajeros");
        add_alias(({"enano","viajero"}));
        add_plural(({"enanos","viajeros"}));
        set_long("Un enano con el típico aspecto de un viajero. Lleva un fardo "
          "a sus espaldas, y ropas algo ajadas por las largas caminatas a la inte"
          "mperie. Pero a pesar de su aspecto, un brillo de inteligencia y sabidu"
          "ría aparece en su cara. Este es uno de los enanos que se dedican al es"
          "tudio de las plantas y a la herboristería, y suele pasar mucho por el "
          "Bosque de Urlom, pues hay hierbas que solo crecen en él.\n");

        fijar_raza("enano");
        fijar_nivel(12+random(3));
        set_random_stats(12,16);
        load_chat(10, ({
            1,":se atusa la barba.",
            1,":descansa un poco.",
          }));
        add_clone(ARMASBASE+"hacha_de_mano",1);
        add_clone(ARMADURASBASE+"manto",1);
        add_clone(ARMADURASBASE+"botas",1);
        ajustar_dinero(2+random(3), "oro");
        break;

    case 2:
        set_name("elfo");
        set_short("Elfo Herborista");
        set_main_plural("Elfos Herboristas");
        add_alias(({"elfo","herborista"}));
        add_plural(({"elfos","herboristas"}));
        set_long("Un elfo experto en el estudio de todo tipo de plantas y hierbas."
          " Muchos como él se ven por estos lugares, atraídos la gran mayoría por e"
          "l Bosque de Urlom.\n");
        fijar_raza("elfo");
        fijar_nivel(10+random(2));
        set_random_stats(9,14);
        load_chat(5, ({
            1,":te saluda.",
          }));

        add_clone(ARMASBASE+"espada_corta",1);
        add_clone(ARMADURASBASE+"capa",1);
        add_clone(ARMADURASBASE+"botas",1);
        ajustar_dinero(5+random(20), "plata");
        break;

    case 3:
        set_name("gnomo");
        add_alias(({"gnomo"}));
        add_plural(({"gnomos"}));
        set_short("Gnomo");
        set_main_plural("Gnomos");
        set_long("Un gnomo normal y corriente, que se encuentra haciendo el trayecto "
          "entre un poblado y otro.\n");
        fijar_raza("gnomo");
        fijar_nivel(12);
        set_random_stats(13,16);

        load_chat(15, ({
            1,":silba despreocupadamente.",
            1,":camina a paso ligero.",
            1,"'¿qué te trae por aquí?",
          }));

        add_clone(ARMASBASE+"daga",1);
        add_clone(ARMADURASBASE+"capa",1);
        ajustar_dinero(random(3), "denario");
        break;

    case 4:
        set_name("bandido");
        add_alias(({"bandido"}));
        add_plural(({"bandidos"}));
        set_short("Bandido");
        set_main_plural("Bandidos");
        set_long("Un bandido que se dedica a asaltar a los pobres viajeros "
          "que van por los caminos. No dudan en intimidar con sus armas a "
          "cualquiera que ose resistirse, incluso son capaces de llegar a "
          "matar por una simple bolsa de dinero.\n");
        fijar_raza("humano");
        set_aggressive(1);
        fijar_alineamiento(999);
        fijar_estatus("Ak'Anon",-300);
        fijar_nivel(25);
        set_random_stats(14,18);

        load_chat(20,
          ({
            1,":mira tu bolsa.",
            1,":afila su cuchillo.",
            1,"'¿qué estás mirando, pardillo?",
          }));

        add_clone(ARMASBASE+"cuchillo",1);
        add_clone(ARMADURASBASE+"cuero",1);

        if(random(2))
            add_clone("/baseobs/componentes/bomba_humo",1);

        ajustar_dinero(3+random(5), "oro");
        break;

    default:
        set_short("NPC Erróneo del Dominio de %^BOLD%^Urlom%^RESET%^: ¡Avisa a un Inmortal!");
        break;
    }

    if ( i != 4 )
        setup_rol_comun();

    fijar_genero(1);
    add_zone("caminos");
    set_move_after(5,10);
    init_equip();


}
