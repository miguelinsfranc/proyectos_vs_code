// Sierephad jun 2k18	--	corrigiendo los tesoros para k cuadren con
// tesoros pnj lish

#define HANDLER "/handlers/objetos_unicos"
#include "/d/kheleb/path.h"
inherit "/obj/baules/cofre.c";
private
void _log(string tx)
{
    write_file(LOGS + "lish.log", ctime() + " [Cofre lish]: " + tx + "\n");
}
private
void poner_premios()
{
    object b;

    fijar_tesoro(9 + random(2), "arma");
    fijar_tesoro(9 + random(2), "armadura");
    fijar_tesoro(9 + random(2), "pergamin");

    fijar_tesoro(6 + random(2), "pergamin");
    fijar_tesoro(6 + random(2), "arma");
    fijar_tesoro(6 + random(2), "varios");

    if (b = clone_object(ARMADURAS + "anillo_lish.c")) {
        b->move(this_object());
    } else {
        _log("Imposible clonar anillo de lish.");
    }

    if (b = clone_object("/baseobs/componentes/ojo_diosa_desterrada.c")) {
        b->move(this_object());
    } else {
        _log("Imposible clonar ojo de diosa desterrada.");
    }
}
void setup()
{
    set_name("cofre");
    set_short("%^BOLD%^RED%^Cofre de Lish%^RESET%^");
    set_long(" Un enorme baúl de metal rojizo de perfectísima manufactura "
             "adornado con filigranas de "
             "índole draconiana y un montón de piedras preciosas, "
             "mayoritariamente rubíes. "
             "Este cofre fue creado para Lish por algún artesano de renombre "
             "que esta atrapase y "
             "obligase a trabajar para ella antes de devorarlo. Sus "
             "dimensiones son descomunales. "
             "Una enorme cerradura con la forma de una garra guarda su "
             "contenido: sólo Lish puede "
             "abrirlo.\n");

    add_alias(({"cofre", "cofre de lish"}));
    add_plural(({"cofres", "cofres de lish"}));
    fijar_genero(1);

    // Que lo fuercen por sus medios
    fijar_abierto(0);
    setup_cerradura(100, 1, 0);
    reset_get();

    if (clonep(this_object())) {
        string *premios = allocate(0);

        poner_premios();

        foreach (object premio in all_inventory(this_object())) {
            premios += ({base_name(premio)});
        }

        _log("Se clonan estos premios: " + nice_list(premios) + ".");
    }
}
