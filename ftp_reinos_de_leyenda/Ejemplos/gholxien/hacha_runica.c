// Satyr 19.02.2016
inherit "/obj/arma.c";

string tt(string t)
{
    return "%^ORANGE%^BOLD%^" + t[0..0] + "%^RESET%^ORANGE%^" + t[1..] + "%^RESET%^";
}
void setup()
{
    fijar_arma_base("hacha a dos manos");
    
    set_name("hacha");
    set_short("Hacha Rúnica de " + tt("Kharod") + "-" + tt("Kar"));
    set_main_plural("Hachas Rúnicas de " + tt("Kharod") + "-" + tt("Kar"));
    set_long(
        "Un hacha ósea que mide más de un metro y cuya parte superior ha "
            "sido limada hasta la perfección para crear un filo letal. "
            "Parece haber sido creada íntegramente de un único hueso "
            "-un fémur, aparentemente- que ha sido recubierto en toda "
            "su superficie con runas muy toscas.\n\n"
        
        "  Su parte inferior está envuelta en cuero reforzado y de ella "
            "cuelga una cadena de finos eslabones que sirve para equilibrar "
            "el hacha. El arma es grande, tosca, pesada... pero en manos "
            "de un luchador fuerte se vuelve mucho más manejable.\n\n"
        

        "  A primera vista parece un arma primitiva, pero nada más lejos "
            "de la realidad: éste arma ha sido creada usando como material "
            "los huesos del Balrog y como técnica de fragua la herrería "
            "rúnica enana, un arte ya perdido que ha caído en desuso.\n\n"
        

        "  Los huesos de esta viciosa criatura están infundidos de tal poder "
            "que su dureza es muchísimo superior al de cualquier material "
            "mundano conocido por los primeros nacidos; dureza de la que "
            "ahora se beneficia el hacha.\n\n"

        "  Tal poder viene a un precio, puesto que la malicia de su antiguo "
            "dueño aún reside en la médula de los huesos del Balrog y "
            "cualquier arma que se haga a partir de éstos restos seguirá "
            "maldita a ojos de los dioses.\n\n"

        "  Sin embargo, el centenar de runas que recubre el fémur sirven "
            "como técnica para mantener este maléficoico poder a raya. Dichas "
            "runas, desconocidas incluso para los versados en éste tipo de "
            "hechicería, son un legado de Oneex que con las eras ha caído "
            "en el olvido.\n\n"

        "  Cuando las leyendas narran la gran maestría de los enanos en "
            "la fragua lo hacen gracias a obras de arte como la que ahora "
            "contemplas.\n\n"
    );

    generar_alias_y_plurales();
    fijar_peso(17000);
    
    //184+89
    ajustar_danyo_minimo("tabla", 20);
    nuevo_ataque("divino", "divino"  , 1, 48, 5);
    
    fijar_material(7);
    fijar_encantamiento(60);
    ajustar_vida_max(200, 1);
    
    //add_static_property("clase" , ({"Khazad Dum Uzbad"}));
    add_static_property("especializacion", ({"Guardia de Piedra"}));
    
    nuevo_efecto_basico("mataorcos", 20);
}
