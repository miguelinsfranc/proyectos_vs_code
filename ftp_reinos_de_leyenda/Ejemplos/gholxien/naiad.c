// Satyr 2007
// Naiad
/*
    Kaitaka 15Sep2012:
        - Adaptada la quest del anillo de los nyathor al sistema de
   misiones(takome_anillo_nyathor)
        - Adaptada la quest de la capa de hojas(takome_capa_hojas)
        - Adaptada la quest de la armadura de piel de oso(takome_piel_oso)
        - Adaptada la quest de la corona de los druidas(takome_corona_druida)

        07Oct2012:
        - Adaptada la quest de Llhioker takome_ruthrer_3

        sierephad Jun 2k17 -- adapto los bloqueos del currar.
    Eckol Sep17: Balanceo su vida un poco al alza.
*/
#include "/d/takome/path.h"
#include <ciudadanias.h>

// Diversos
#define HOJA ARMADURAS + "druida/hoja_arbol"
#define HOJAS_NECESARIAS                                                       \
    ({"/d/takome/rooms/thorin/",                                               \
      "/d/dendra/rooms/wareth/",                                               \
      "/d/zulk/zonas/jungla/",                                                 \
      "/d/kheleb/rooms/arboledas/",                                            \
      "/d/anduar/rooms/eloras/",                                               \
      "/d/kheleb/rooms/zelthaim"})

// Items
#define ANILLO "/clases/sacerdotes/druida/objetos/anillo.c"
#define MANTO "/clases/sacerdotes/druida/objetos/manto.c"
#define OSO "/clases/sacerdotes/druida/objetos/pechera.c"
#define CORONA "/clases/sacerdotes/druida/objetos/corona.c"
#define VARA "/clases/sacerdotes/druida/objetos/vara.c"
// Quest del anillo
#define CIERVO_KD "/d/kheleb/npcs/ciervo"
#define PLANTA "/d/urlom/objetos/pblanca.c"

// Xp de cada una de las quests
#define XP1 5000 + random(1001)  // Por cada elemento (doble al terminar)
#define XP2 2001 + random(1001)  // Por cada hoja (doble al terminar
#define XP3 8000 + random(3000)  // Por la quest de los leñadores
#define XP4 15000 + random(5000) // Por la quest de matar a Sheeta

// Log de movimientos
#define LOG LOGS + "equipo_druida.log"

inherit "/obj/conversar_npc.c";

void recompensa_lhioker(object pj, string respuesta, object rosa);

int dame_guardia()
{
    return 1;
}

int es_druida(object pj)
{
    // Castigo a un jugador
    if (pj->query_property("no_quest_druida"))
        return 0;

    if (pj->dame_clase() == "druida" && pj->dame_ciudadania() == "thorin")
        return 1;

    return 0;
}

void mensaje(string tx)
{
    do_say(tx, 0);
}

void lista_tareas(object pj)
{
    if (environment() != environment(pj))
        return;

    if (pj->query_property("no_quest_druida")) {
        do_say(pj->query_cap_name() + ", tu has sido maldito a mis ojos.", 0);
        do_say("Fuera de mi vista.", 0);
        return;
    }
    if (pj->dame_nivel() < 12) {
        do_say(
            "Desgraciadamente, " + pj->query_cap_name() + ", eres muy joven.",
            0);
        do_say("Aún no podrás ayudarme en mis menesteres.", 0);
        return;
    }

    do_say(
        pj->query_cap_name() + ", veo que eres " + pj->dame_numeral() +
            " druida poderos" + pj->dame_vocal() +
            ", ¿te interesaría realizar una de las muchas tareas que tengo "
            "pendientes?",
        0);
    // No hay breaks
    switch (pj->dame_nivel()) {
        case 26..99:
            pj->add_static_property("dialogo_corona_druida", 1);
        case 21..25:
            pj->add_static_property("dialogo_armadura_oso_druida", 1);
        case 16..20:
            pj->add_static_property("dialogo_capa_hojas_druida", 1);
        case 12..15:
            pj->add_static_property("dialogo_anillo_druida", 1);
    }
}

// Quest Llhioker
int check_rosa(object pj)
{
    // Satyr 20.04.2015
    // if (pj->dame_bando()!="bueno" && pj->dame_religion() != "ralder")
    //        return 1;

    if (!pj->dame_hito_mision("takome_ruthrer_3", "pista4"))
        return 1;
    return 0;
}
void dialogo_rosa(object pj)
{
    object o;
    mixed  resp;

    if (pj->dame_mision_completada("takome_ruthrer_3")) {
        do_say(
            "Hola, " + pj->query_short() +
                " aún recuerdo el favor que nos hiciste a todos con lo de "
                "Llhioker, vuelve despúes de un tiempo.",
            0);
        return;
    }

    o = secure_present("/baseobs/misc/rosa", pj);

    if (!o) {
        do_say(
            "No tienes ninguna rosa para darme, no se que quieres decir.\n", 0);
        return;
    }

    do_say("¡Oh!", 0);
    do_say("¡Cuan hermosa esta flor que me traes!", 0);
    do_say(
        "Estoy al tanto de lo que has hecho, " + pj->query_cap_name() + ".", 0);

    do_say("Permite que te haga entrega de una recompensa. Dime, ¿qué es lo "
           "que quieres?");

    resp = allocate(0);

    // Los ralders que hagan su misión propia
    if (pj->dame_religion() != "ralder") {
        resp += ({({"colmillo", "Colmillo de Wyvern (pendiente)"})});

        if (pj->dame_clase() == "druida") {
            resp += ({({"vara", "Vara del Génesis (arma de druida)"})});
        }
    }

    resp += ({({"espiral", "Espiral Rosa (collar)"})});

    // clang-format off
    pj->mostrar_dialogo(
        "¿Qué objeto de recompensa quieres?",
        resp,
        (:recompensa_lhioker, o:),
        0,
        0,
        1
    );
    // clang-format on
}
void recompensa_lhioker(object rosa, object pj, string respuesta)
{
    mixed item;

    if (!pj) {
        return;
    }

    switch (respuesta) {
        case "colmillo":
            do_say(
                "Acepta como recompensa este colmillo. Es una reliquia entre "
                "los druidas.",
                0);
            item = ARMADURAS + "druida/pendiente_colmillo";
            break;

        case "vara":
            do_say(
                "Esta flor, a partir de hoy, te dará sus poderes como "
                "agradecimiento a haber eliminado a ese maligno sacerdote.",
                0);
            tell_object(
                pj,
                query_short() + " toca tu " + rosa->query_short() +
                    ", que parece animarse con vida propia y crecer hasta "
                    "convertirse en "
                    "un bastón repleto de vida.\n");
            item = VARA;
            break;

        case "espiral":
            item = ARMADURAS + "espiral_rosa.c";

            do_say(
                "Así como tú me das esta buena nueva, permite que yo te de el "
                "favor de los nuestros.",
                0);
            do_emote("hace una serie de gestos sobre la rosa.");
            do_say(
                "Te devuelvo la rosa que tú me entregas. Vístela con orgullo.",
                0);
            break;
    }

    if (item &&!item = pj->entregar_objeto(item)) {
        do_say("Un error me impide entregarte tu premio.", 0);
        return;
    }

    if (pj->dame_estatus("Thorin") < NOBLE) {
        pj->ajustar_estatus("Thorin", 10 + random(11));
    }

    if ((base_name(item) + ".c") == VARA) {
        item->add_property("propietario", pj->query_name());
    }

    // Dejo esto por compatibilidad con propiedades antiguas, pero se podrá
    // quitar en un tiempo
    pj->remove_property("habla_con_naiad");
    pj->nuevo_hito_mision("takome_ruthrer_3", "mision_terminada", 0);
    rosa->dest_me();

    write_file(
        LOG,
        ctime() + " " + pj->query_cap_name() + " ha recibido " +
            item->query_short() + ".\n");
    return;
}

// Quest de la corona
status hito4(object pj)
{
    return pj->nuevo_hito_mision("takome_corona_druida", "pista1", 0);
}
void dialogo_maragedom(object pj)
{
    object corona;

    if (environment() != environment(pj))
        return;

    if (pj->dame_mision_completada("takome_corona_druida")) {
        do_say("¡Hola, " + pj->query_cap_name() + "!", 0);
        do_say("Aún no se como agradecerte tus labores con Sheeta.", 0);
        do_say(
            "Estoy orgullosa de que en los Nyathor haya gente con tanto "
            "talento.",
            0);
        do_emote("reverencia.");
        return;
    }

    // Le contamos la vida
    if (!pj->dame_hito_mision("takome_corona_druida", "pista1") &&
        !pj->dame_hito_mision("takome_corona_druida", "pista2")) {

        pj->bloquear_accion("Estás ocupado hablando con " + query_short(), 31);
        do_say(
            "¡" + pj->query_cap_name() + "!, ¡te necesito, es importante!", 0);
        call_out(
            "mensaje",
            2,
            "¡Un gran mal acecha el bosque de Maragedom, en las tierras del "
            "maligno imperio de Dendra!");
        call_out(
            "mensaje",
            5,
            "La mancha del mal de Oskuro reposa en ese bosque, pues la "
            "gigantesca calavera que yace en el medio cayó allí desde "
            "su antigua catedral, en las montañas de Cyr.");
        call_out(
            "mensaje",
            7,
            "Desde el primer día, la calavera ha estado mancillando el bosque "
            "con el poder de Oskuro, pero afortunadamente las energías "
            "naturales "
            "son muy poderosas allí y han conseguido derrotar su estigma de "
            "corrupción, ¡por lo menos en su mayor parte!");
        call_out(
            "mensaje",
            10,
            "Sin embargo, un terrible espíritu habita en el bosque y corrompe "
            "la vida con sus lamentos, con sus disgustos.");
        call_out(
            "mensaje",
            12,
            "El espíritu de una elfa torturada que sale de noche a encerrar "
            "más almas en la desesperación que ella está encerrada.");
        call_out("do_emote", 13, "llora desconsoladamente.");
        call_out(
            "mensaje",
            15,
            "Sheeta es el nombre de ese espíritu y estoy seguro de que en vida "
            "debió tener un corazón puro, pero la influencia de Oskuro... la "
            "ha dejado en su estado actual.");
        call_out(
            "mensaje", 17, "Debes terminar con su tormento, ¡definitivamente!");
        call_out(
            "mensaje",
            20,
            "Sin embargo no te servirá el simple hecho de destruirla, pues su "
            "espíritu está encerrado en su más preciado tesoro.");
        call_out(
            "mensaje",
            22,
            "Tendrás que encontrar ese tesoro y purificarlo en frente de la "
            "misma calavera para despues arrojarlo a sus fauces y que así "
            "Oskuro no sea capaz de morder su alma nunca más.");
        call_out(
            "mensaje",
            25,
            "¡Por favor " + pj->query_cap_name() +
                "!, ¡tienes que salvar el bosque del desconsolado lamento de "
                "la Banshee!");
        call_out(
            "mensaje",
            27,
            "Necesitarás las aguas más puras de toda Eirea, unas aguas que "
            "pertenezcan a un dios ancestral que en la era pasada derrotó a "
            "Oskuro.");
        call_out(
            "mensaje",
            29,
            "¡No falles, deja que Sheeta descanse en paz de una vez por "
            "todas!");
        call_out("hito4", 30, pj);
        return;
    }

    // Quest sin terminar
    if (!pj->dame_hito_mision("takome_corona_druida", "pista2")) {
        do_say("¿Aún no has terminado con la existencia de Sheeta?", 0);
        call_out(
            "mensaje", 2, "Has de hacerlo rápido, ¡recuerda lo que te dije!");
        call_out(
            "mensaje",
            5,
            "Purifica su más valioso tesoro con las aguas de un dios que se "
            "haya medido con Oskuro en eras pasadas, ¡y hazlo en frente de la "
            "calavera!");
        call_out(
            "mensaje",
            7,
            "¡Sólo así conseguirás purificar su alma para siempre!");
        return;
    }
    // Quest acabada
    else {
        tell_object(this_player(), query_short() + " te abraza, emocionada.\n");
        do_say("¡Lo has conseguido " + pj->query_cap_name() + "!", 0);
        do_say(
            "¡Sabía que lo conseguirías!, ¡ahora el espíritu de Sheeta y el "
            "bosque de Maragedom están completamente limpios gracias a ti!",
            0);
        do_say(
            "Acepta este regalo, es el mayor símbolo de distinción entre los "
            "Nyathor.",
            0);

        if (!corona = pj->entregar_objeto(CORONA))
            tell_object(
                pj, "¡Ha ocurrido un error al intentar darte la corona!\n");
        else
            corona->add_property("propietario", pj->query_name());

        // property vieja
        pj->remove_property("sheeta_limpiada");
        pj->nuevo_hito_mision("takome_corona_druida", "mision_terminada", 0);
        //      pj->add_timed_property("no_quest_corona",1,60*60*20);
        write_file(
            LOG,
            ctime() + " " + pj->query_cap_name() + " ha recibido " +
                corona->query_cap_name() + ".\n");
        pj->adjust_xp(XP4);
    }
}

// Quest de la armadura de oso
status hito3(object pj)
{
    return pj->nuevo_hito_mision("takome_piel_oso", "pista1", 0);
}

void dialogo_lenyadores(object pj)
{
    object oso;
    if (environment(pj) != environment())
        return;

    if (pj->dame_hito_mision("takome_piel_oso", "mision_terminada")) {
        do_say("¡Hola de nuevo, " + pj->query_cap_name() + "!", 0);
        do_say("Gracias a ti, la deforestación en Golthur ha disminuído.", 0);
        do_say(
            "Te buscaré de nuevo cuando su actividad vuelva a dispararse, "
            "¡gracias por tu ayuda!",
            0);
        return;
    }

    // No tiene la quest, se la damos
    if (!pj->dame_hito_mision("takome_piel_oso", "pista1")) {
        pj->bloquear_accion("Estás ocupado hablando con " + query_short(), 30);
        do_say(
            pj->query_cap_name() +
                " necesito tu fuerza para hacer algo importantísimo.",
            0);
        call_out(
            "mensaje",
            2,
            "Verás, en las tierras más nórdicas de Eirea se haya una temible "
            "fortaleza cuya altura roza el cielo.");
        call_out(
            "mensaje",
            4,
            "Esa fortaleza se llama Golthur-Orod y multitud de malignas razas "
            "moran allí, devastándolo todo.");
        call_out(
            "mensaje",
            6,
            "Hace muchos años atrás, un inmenso bosque se alzaba donde ahora "
            "solo hay cenizas: el bosque del jabalí.");
        call_out(
            "mensaje",
            8,
            "Las llamas del volcán cercano, el volcán de N'argh, redujeron el "
            "bosque a un erial y un bosque completamente calcinado.");
        call_out(
            "mensaje",
            10,
            "Un bosque en unas tierras tan tintadas por el mal no tendría "
            "salvación... pero hay una cosa que he estado observando.");
        call_out(
            "mensaje",
            13,
            "Cada año decenas de peregrinos de Eralie cabalgan hacia el rio "
            "Derebar, en ese bosque, a recoger aguas a las que atribuyen "
            "poderes divinos.");
        call_out(
            "mensaje",
            15,
            "Dichos peregrinos me han confirmado que las aguas tienen poderes "
            "curativos y que lentamente son capaces de regenerar los árboles "
            "calcinados del bosque.");
        call_out(
            "mensaje",
            17,
            "Desconozco cuanto tiempo llevará el proceso de regeneración en un "
            "bosque tan grande como ese, ¡pero si hay esperanza de "
            "recuperarlo, debemos intentarlo!");
        call_out(
            "mensaje",
            20,
            "Por si la naturaleza tuviese poco con el volcán, la fortaleza "
            "envía continuamente leñadores para destruir los pocos árboles que "
            "quedan.");
        call_out(
            "mensaje",
            23,
            "¡Debes detener esa masacre!, ¡ve allí y destrúye cuantos "
            "leñadores puedas!");
        call_out("mensaje", 25, "¡Deten su desmesurada deforestación!");
        call_out(
            "mensaje",
            27,
            "Vuelve a mi cuando hayas matado unas cuantas decenas de leñadores "
            "y te recompensaré, ¡el tiempo apremia! ¡ve!");
        call_out("hito3", 29, pj);
        pj->add_property("mata_leñadores_golthur", 1);
        return;
    }

    // Completó la quest
    if (pj->query_property("mata_leñadores_golthur") > 15) {
        do_say("¡¡Sublime " + pj->query_cap_name() + "!!", 0);
        do_say(
            "¡No hace falta que me digas nada, se perfectamente lo que ha "
            "sucedido!",
            0);
        do_say(
            "Has eliminado a esos viles orcos como las sucias sabandijas que "
            "son, ¡gracias!, ¡el bosque baldío algún día retomará su esplendor "
            "original!",
            0);
        do_say(
            "Te voy a hacer entrega de una armadura muy especial, un Oso "
            "ancestral me otorgó su piel en la hora de su muerte, con esa piel "
            "hice esta armadura.",
            0);
        do_say("¡Su espíritu y resistencia protegerá en todo momento!", 0);
        do_say(
            "¡Acéptala y continúa llevando por el mundo la esperanza natural!",
            0);

        if (!oso = pj->entregar_objeto(OSO))
            tell_object(
                pj, "¡Ha ocurrido un error al intentar darte la piel!\n");
        else
            oso->add_property("propietario", pj->query_name());

        pj->remove_property("mata_leñadores_golthur");
        pj->nuevo_hito_mision("takome_piel_oso", "mision_terminada", 0);
        // pj->add_timed_property("no_armadura_oso",1,60*60*10);
        pj->ajustar_xp(XP3);
        write_file(
            LOG,
            ctime() + " " + pj->query_cap_name() + " ha recibido " +
                oso->query_cap_name() + ".\n");
        return;
    }

    // Le faltan unos cuantos
    do_say("¡Los leñadores de Golthur han de ser detenidos!", 0);
    do_say("¡Estoy contando contigo, " + pj->query_cap_name() + "!", 0);
    do_say("¡Ve allí y termina con su cruel deforestación!", 0);
}

// Quest de la capa de hojas
status hito2(object player)
{
    return player->nuevo_hito_mision("takome_capa_hojas", "pista1", 0);
}
void dialogo_capa(object pj)
{
    string *entregadas = pj->query_property(
        "hojas_entregadas"); // Hojas que el jugador ha entregado
    string *sitios =
        ({"Bosque de Thorin",
          "Bosque de Wareth",
          "Selva húmeda",
          "Arboledas de Kheleb-Dum",
          "Bosque de Eloras",
          "Bosque de Zelthaim"});
    string *actuales = ({});
    int     contador_xp;
    object  manto;

    if (environment() != environment(pj))
        return;

    if (pj->dame_mision_completada("takome_capa_hojas")) {
        do_say(
            "Déjame agradecerte de nuevo tu labor, " + pj->query_cap_name() +
                ".",
            0);
        do_say(
            "De momento no es necesaria tu ayuda, ya tengo las hojas que "
            "necesitaba.",
            0);
        do_say("Pero no dudes que te las volveré a pedir.", 0);
        return;
    }

    if (!entregadas)
        entregadas = ({});

    // Vemos cuantas hojas tiene el tio encima
    foreach (object ob in all_inventory(pj)) {
        if (real_filename(ob) != HOJA)
            continue;

        if (ob->query_property("cortador") != pj->query_name())
            continue;

        // Solo pillamos las que nos interesen
        if (-1 == member_array(ob->query_property("sitio"), sitios))
            continue;

        // Evitamos duplicadas
        if (-1 != member_array(ob->query_property("sitio"), entregadas))
            continue;

        contador_xp++;
        entregadas += ({ob->query_property("sitio")});
        actuales += ({ob->query_property("sitio")});
        ob->dest_me();
    }

    // El dialogo de la quest normal y corriente
    if (!entregadas || entregadas == ({})) {
        do_say(
            "Es tarea de los druidas la de velar por el bien de los bosques de "
            "Eirea.",
            0);
        call_out(
            "mensaje",
            2,
            "Aunque los Nyathor surgieron para la protección del bosque de "
            "Thorin, ¡es nuestro deber defender el resto de bosques de Eirea!");
        call_out(
            "mensaje",
            4,
            "Para ello, " + pj->query_cap_name() +
                ", quiero que recorras ciertos bosques y que me traigas una "
                "hoja de cada uno de ellos.");
        call_out(
            "mensaje",
            6,
            "Con eso conseguiré conocer el estado de los bosques, si es que "
            "necesitan ayuda.");
        call_out(
            "mensaje",
            8,
            "Yo me he prometido que no saldría de Thorin hasta que este "
            "estuviese recuperado y fuera de peligro, así que necesito que "
            "alguien lo haga por mi.");
        call_out(
            "mensaje",
            10,
            "Usando tu Hoz Dorada, quiero que recorras " + nice_list(sitios) +
                " y me traigas una hoja de un árbol de cada lugar.");
        call_out("mensaje", 12, "¡Ve y hazlo por Thorin!, ¡por Eralie!");
        call_out("hito2", 13, pj);

        return;
    }

    if (actuales != ({}))
        do_say(
            "¡Oh, bravo!, ¡me has traido las hojas de " + nice_list(actuales) +
                "!",
            0);

    // Le damos la xp por cada hoja
    if (contador_xp)
        pj->ajustar_xp((XP2) *contador_xp);

    pj->add_property("hojas_entregadas", entregadas);

    // La quest terminada
    if (sizeof(entregadas) == sizeof(sitios)) {
        do_say("Excelente, ¡me has traído todo lo que necesitaba!", 0);
        do_say(
            "Gracias a ti seré capaz de comprobar si alguna enfermedad o "
            "malestar azota a los bosques que has recorrido.",
            0);
        do_say(
            "Acepta este manto como premio, está hecha de multitud de hojas "
            "como las que tu has recogido.",
            0);

        if (!manto = pj->entregar_objeto(MANTO))
            tell_object(
                pj, "¡Ha ocurrido un error al intentar darte la capa!\n");
        else
            manto->add_property("propietario", pj->query_name());

        pj->remove_property("hojas_entregadas");
        pj->nuevo_hito_mision("takome_capa_hojas", "mision_terminada", 0);
        // misiones caducidad takome_capa_hojas <segundos> para establecer el
        // lock, lo dejo en 36000
        // pj->add_timed_property("no_quest_manto",1,60*60*10);
        write_file(
            LOG,
            ctime() + " " + pj->query_cap_name() + " ha recibido " +
                manto->query_cap_name() + ".\n");
        return;
    }

    // Nos faltan hojas
    else {
        sitios = sitios - entregadas;
        do_say("Tengo ya algunas hojas tuyas, pero necesito más.", 0);
        do_say(
            "En concreto, necesito las hojas de %^CURSIVA%^" +
                nice_list(sitios) +
                "%^RESET%^, ya que me has entregado con anterioridad las de "
                "%^CURSIVA%^" +
                nice_list(entregadas) + "%^RESET%^.",
            0);
        do_say("Vuelve cuando las tengas.", 0);
    }
}

// Quest del anillo de Nyathor
status hito1(object player)
{
    return player->nuevo_hito_mision("takome_anillo_nyathor", "pista1", 0);
}

void dialogo_ciervo(object pj)
{
    object ciervo, planta;
    int    cie, pl;
    object anillo;

    if (environment() != environment(pj))
        return;
    // Por si tiene la propiedad antigua
    pj->remove_property("planta_blanca_entregada");

    if (pj->dame_mision_completada("takome_anillo_nyathor")) {
        do_say(
            pj->query_cap_name() +
                " recuerdo tu última ayuda prestada en este trabajo, ¡gracias!",
            0);
        do_say("No hace falta que lo repitas en una temporada.", 0);
        return;
    }

    // Buscamos los objetos en cuestión, primero la planta
    if (!pj->dame_hito_mision("takome_anillo_nyathor", "pista2"))
        planta = secure_present(PLANTA, pj);

    if (!planta)
        pl = pj->dame_hito_mision("takome_anillo_nyathor", "pista2");
    else {
        do_say(
            "La planta blanca de Urlom; muy bien, " + pj->query_cap_name() +
                ". Esto me será muy útil.",
            0);
        planta->dest_me();
        pj->ajustar_xp(XP1);
        pj->nuevo_hito_mision("takome_anillo_nyathor", "pista2", 0);
    }

    if (!pj->dame_hito_mision("takome_anillo_nyathor", "pista3"))
        foreach (object ob in all_inventory(environment())) {
            if (real_filename(ob) != CIERVO_KD)
                continue;

            if (ob->query_property("dominado_por") == pj) {
                ciervo = ob;
                do_emote("acaricia a " + ob->query_short() + ".");
                do_say(
                    "¡Fantástico!, veo que has conseguido traer un cervatillo. "
                    "¡Ahora déjalo marchar y esperemos que prospere en Thorin!",
                    0);
                pj->nuevo_hito_mision("takome_anillo_nyathor", "pista3", 0);
                ciervo->dest_me();
                pj->ajustar_xp(XP1);
                break;
            }
        }

    cie = pj->dame_hito_mision("takome_anillo_nyathor", "pista3");
    pl  = pj->dame_hito_mision("takome_anillo_nyathor", "pista2");

    // Si no tiene ninguna, mostramos la información de la quest
    if (!cie && !pl &&
        !pj->dame_hito_mision("takome_anillo_nyathor", "pista1")) {
        pj->bloquear_accion("Estás ocupado hablando con " + query_short(), 27);
        do_say("Verás, hay dos cosas que necesito que hagas por mí.", 0);
        call_out(
            "mensaje",
            2,
            "Como sabrás, el bosque de Thorin ha sufrido severos abusos y eso "
            "se ha notado en el ecosistema.");
        call_out(
            "mensaje",
            3,
            "Las criaturas del bosque están intranquilas y los árboles y "
            "plantas aún sufren las heridas de los incendios.");
        call_out(
            "mensaje",
            4,
            "La capacidad regenerativa del bosque está ligada en gran medida a "
            "la fuerza de sus criaturas, a Eralie y a nosotros, los Nyathor.");
        call_out(
            "mensaje",
            6,
            "Hasta el momento no hemos tenido ningún problema a la hora de "
            "restaurar el bosque, pero poco a poco va perdiendo su vitalidad.");
        call_out(
            "mensaje",
            8,
            "Ha llegado la hora de tratar de implantar nuevas especies en "
            "Thorin; necesitamos restablecer el equilibrio de la población "
            "animal y vegetal que se ha roto tras la invasión arácnida y los "
            "incendios.");
        call_out(
            "mensaje",
            12,
            "¿Conoces el bosque de Urlom, al sudoeste de Dalaensar?");
        call_out("mensaje", 15, "¿Y la senda del rey Durín?");
        call_out(
            "mensaje",
            16,
            "Quiero que me traigas dos nuevas especies, una flor y un animal.");
        call_out(
            "mensaje",
            18,
            "La flor es una rara planta blanca, símbolo de que ha sido tocada "
            "por Eralie.");
        call_out(
            "mensaje",
            20,
            "En algún lugar de Urlom podrás encontrarla. No puedo darte más "
            "detalles.");
        call_out(
            "mensaje", 22, "El animal... ¡un ciervo de la senda de Durin!");
        call_out(
            "mensaje",
            24,
            "Hace tiempo traté de implantar ciervos en Thorin, pero por "
            "desgracia, no conseguí traer un gran número de ejemplares.");
        call_out(
            "mensaje",
            26,
            "Trae todo lo que necesito y te recompensaré. ¡Pero no pierdas "
            "tiempo!, ¡ve!");
        call_out("hito1", 27, pj);
        return;
    }

    if (cie && !pl) {
        do_say(
            "¡Te falta por traerme la planta blanca de Urlom, " +
                pj->query_cap_name() + "!",
            0);
        do_say("Traela y te daré tu recompensa.", 0);
        return;
    }

    if (pl && !cie) {
        do_say(
            pj->query_cap_name() +
                " ya tengo la planta blanca de Urlom gracias a ti.",
            0);
        do_say(
            "Ahora traeme un ciervo de la senda de Durin, ¡y que este sano!",
            0);
        return;
    }
    // El lock lo controlamos con esto ahora
    if (!pj->dame_mision_completada("takome_anillo_nyathor") && pl && cie) {
        do_say(
            "¡Ya tengo todo lo que necesito, " + pj->query_cap_name() + "!", 0);
        do_say("¡Tu ayuda le vendrá muy bien al bosque!", 0);
        do_say(
            "Confio en que todo lo que he planeado salga bien. Toma, este "
            "anillo se les otorga a todos aquellos Nyathor que demuestran su "
            "valía como druidas.",
            0);
        do_say("¡Es tuyo y sólo tuyo!", 0);

        if (!anillo = pj->entregar_objeto(ANILLO))
            tell_object(
                pj, "¡Ha ocurrido un error al intentar darte el anillo!\n");
        else
            anillo->add_property("propietario", pj->query_name());

        // Este log se podría quitar
        write_file(
            LOG,
            ctime() + " " + pj->query_cap_name() + " ha recibido " +
                anillo->query_cap_name() + ".\n");
        // caducidad de la mision
        // pj->add_timed_property("no_quest_anillo",1,60*60*10);
        pj->nuevo_hito_mision("takome_anillo_nyathor", "mision_terminada", 0);
        pj->ajustar_xp((XP1) *2);
    }
}

void setup()
{
    set_name("naiad");
    add_alias(({"naiad", "semi-elfa", "guardia"}));
    add_plural(({"naiad", "semi-elfa"}));
    set_short("%^GREEN%^Naiad%^RESET%^");
    set_main_plural("imágenes de %^GREEN%^Naiad%^RESET%^");
    set_long("\tLa más bella semi-elfa cuyo nombre haya aparecido en la "
             "historia de los reinos. "
             "Joven, pero ya con una mirada de mujer, Naiad es la druida más "
             "ligada personalmente a la vida natural "
             "conocida por los Nyathor. "
             "Tal es su nivel de empatía natural, que cuando la naturaleza en "
             "cualquier parte de Eirea sufre, "
             "ella siente el intenso dolor en sus propias carnes. "
             "Pero no es puro capricho este vínculo espiritual con la vida "
             "natural; Naiad es poderosa, terriblemente poderosa"
             ", pues ha sido bendecida con todo el favor y el poder que la "
             "faceta de la vida de Eralie puede otorgar a un mortal. "
             "Su aspecto es el de una hermosísima joven; sus ojos almendrados "
             "se posan en cada una de las hojas de los árboles "
             "que la rodean, haciendo que una amplia sonrisa se dibuje en sus "
             "finos labios y su esbelta silueta se oculta con "
             "prendas hechas de la misma naturaleza que ama Naiad. "
             "Es joven, bondadosa y extremadamente gentil con los suyos; ¡pero "
             "no te engañes!, si esta aquí es porque "
             "el bosque de Thorin ha sufrido mucho y este ha elegido a Naiad "
             "como su vengadora.\n");

    fijar_religion("eralie");
    fijar_raza("semi-elfo");
    fijar_clase("druida");
    fijar_ciudadania("thorin", 999);
    fijar_fe(220);

    fijar_alineamiento(-100000);
    fijar_genero(2);

    nuevo_lenguaje("adurn", 100);
    fijar_lenguaje_actual("adurn");

    set_random_stats(18, 18);
    fijar_fue(17);
    fijar_des(19);
    fijar_sab(19);
    fijar_car(57); // :P
    fijar_nivel(41 + random(6));
    fijar_pvs_max(20000);

    add_property("sin miedo", 1);
    add_property("no desarmar", 1);

    add_loved("ciudadania", ({"thorin"}));
    add_hated("enemigo", ({"Thorin"}));
    add_hated("relacion", ({"thorin"}));
    add_hated("propiedad", ({"enemigo_thorin"}));
    add_hated("propiedad", ({"enemigo_nyathor"}));

    set_aggressive(6);

    add_attack_spell(90, "retener persona", 3);
    add_attack_spell(60, "relampago", 3);
    add_attack_spell(30, "llamar rayos", 3);
    add_attack_spell(30, "columna de fuego", 3);

    load_chat(
        15,
        ({":juguetea con las hojas de un árbol.",
          ":sonrie cuando una ardilla trepa hasta su hombro.",
          ":ahuyenta un pequeño roedor que había trepado por Loredor.",
          "'¡Loredor!, ¡no te duermas!",
          ":canta con su voz angelical, atrayendo a todas las criaturas del "
          "bosque.",
          "'Las criaturas que acuden a la zona oculta vienen buscando auxilio "
          "de los druidas, ¡jamás les ataques!",
          "'Las arañas de Thorin han sucumbido a la pestilencia de Seldar, ¡no "
          "dudes en terminar con su tormento!",
          ":llora por la vida de las arañas de Thorin.",
          "'Si quieres demostrar tu valía como druida, sólo tendrás que acudir "
          "a mi.",
          "'Aprende a respetar al bosque y el aprenderá a respetarte a ti."}));
    load_a_chat(
        30,
        ({"'¡La naturaleza te aplastará y te devorará!",
          "'¡Nunca debiste entrar en Thorin!, ¡nunca debiste desafiar a la "
          "naturaleza!",
          "'¡Espíritus del bosque!, ¡acudid a mi!",
          "'¡¡LOREDOR!!, ¡destrúye a los enemigos del bosque!"}));

    add_clone("/clases/sacerdotes/objetos/hoz_dorada.c", 1);
    add_clone(OSO, 1);
    add_clone(ANILLO, 1);
    add_clone(CORONA, 1);
    add_clone(MANTO, 1);
    init_equip();

    ajustar_dinero(12 + random(5), "sesiom");
    set_heart_beat(1);

    nuevo_dialogo(
        "nombre",
        ({":sonrie.",
          "'Mi nombre es Naiad, aunque algunos prefieren llamarme "
          "\"cascarrábias\".",
          ":sonríe a Loredor.",
          "'Camino por los reinos sobre Loredor, defendiendo la naturaleza "
          "allí donde esta me necesita."}));
    // Sobre su ent
    nuevo_dialogo(
        "loredor",
        ({":acaricia a Loredor",
          "'¿Te sorprende su presencia?",
          "'Es un antiguo espiritu del bosque, me salvó la vida en una ocasión "
          "y desde entonces me acompaña en mis búsquedas por restaurar el "
          "orden natural.",
          "'No es muy hablador, prefiere echar raíces y sentir el viento y el "
          "sol en su corteza.",
          "'Insiste en llamarme cascarrábias porque siempre le digo lo que "
          "tiene que hacer.",
          "'Pero en el fondo es gentil, no sabría vivir sin él."}),
        "nombre");
    nuevo_dialogo(
        "espiritu",
        ({"'Sí, es un ent.",
          "'Una raza de antiguos espíritus naturales que habitan en los "
          "bosques más grandes de Eirea.",
          "'Pasan desapercibidos a los ojos del mundo, dormidos tienen el "
          "aspecto de un árbol.",
          "'¡Los que los descubren suelen llevarse una gran sorpresa!",
          ":sonríe.",
          "'Loredor es un espiritu de roble, por eso es tan grande.",
          "'Voy con él a todas partes... ¡o mejor dicho, sobre él!"}),
        "loredor");
    nuevo_dialogo(
        "sobre",
        ({"'Desde aquí arriba es muy cómodo viajar, la brisa fresca te azota "
          "la cara y casi siento que "
          "puedo tocar el cielo.",
          "'Además, si me duermo, él cuidará de mi.",
          "'Lo malo es que es algo lento, cada tres pasos Loredor suele "
          "dormirse.",
          ":sonríe."}),
        "espiritu");

    // Naturaleza (esta conversación solo para druidas)
    nuevo_dialogo(
        "defendiendo",
        ({"'Sí, he intentado recuperar desde el bosque baldío hasta el bosque "
          "maldito del Rey necrófago.",
          "'Siento en mi piel el mismo dolor de la naturaleza desde muy joven, "
          "me siento elegida y por ello "
          "intento traer de nuevo la vida a los bosques moribundos de Eirea.",
          "'Cuando Thorin fue incendiado por el malvado ejército de Dendra "
          "sentí en mis carnes un dolor tan "
          "intenso como nunca antes había sentido.",
          "'Gracias a mis dotes de curación conseguí salvarme, pero la herida "
          "en mi corazón no podrá curarmela nadie.",
          "'Desde ese día Loredor y yo nos hemos desplazado a Thorin con "
          "nuestros hermanos Nyathor.",
          "'¡No permitiremos que la desgracia recaiga de nuevo sobre nuestro "
          "bosque!\n"}),
        "nombre");
    nuevo_dialogo(
        "nyathor",
        ({"'Soy íntima amiga del gremio desde hace tiempo, ¡aunque hasta hoy "
          "no había pasado aquí más de una noche!",
          ":sonrie.",
          "'Ahora paso más tiempo aquí, celebrando ritos de iniciación, "
          "defendiendo el bosque y encomendando tareas a "
          "los druidas más experimentados."}),
        "defendiendo");
    nuevo_dialogo(
        "tareas",
        ({"'Cuando dejé de lado mis viajes para venir a Thorin, también dejé "
          "de lado mis menesteres de defender "
          "los bosques dañados de Eirea.",
          "'Es por eso que solicito a los druidas más experimentados su ayuda "
          "para, así, poder cumplir las tareas que dejé desatendidas.",
          "lista_tareas"}),
        "nyathor");

    // Quests de equipo de druida
    nuevo_dialogo("ciervo", ({"dialogo_ciervo"}), "tareas");
    nuevo_dialogo("hojas", ({"dialogo_capa"}), "tareas");
    nuevo_dialogo("leñadores", ({"dialogo_lenyadores"}), "tareas");
    nuevo_dialogo("maragedom", ({"dialogo_maragedom"}), "tareas");

    // Quest de Ruthrer de acabar con Llhioker
    nuevo_dialogo("rosa", ({"dialogo_rosa"}));

    // Requisitos para las quests
    prohibir("ciervo", "dialogo_anillo_druida");
    prohibir("hojas", "dialogo_capa_hojas_druida");
    prohibir("leñadores", "dialogo_armadura_oso_druida");
    prohibir("maragedom", "dialogo_corona_druida");
    deshabilitar("rosa", "check_rosa");

    prohibir("defendiendo", "es_druida");
    habilitar_conversacion();
}

void heart_beat()
{
    object amigo = present("loredor", environment());

    object *gente;
    int     i;
    gente = users();

    ::heart_beat();

    if (dame_peleando() > 0) {
        if (!query_timed_property("no_flood")) {
            add_timed_property("no_flood", 1, 90);
            "/grupos/ciudadanias/thorin.c"->info_canal(
                "%^BOLD%^GREEN%^Naiad%^RESET%^",
                "¡Intrusos intentan destruir nuestro bosque!, ¡Loredor, "
                "ayúdame!");

            if (present("loredor", environment()))
                "/grupos/ciudadanias/thorin.c"->info_canal(
                    "Loredor",
                    "@exclama en ent: ¡¡Brurufmr memerrmuargh!!, "
                    "¡¡Thorqirruumm groum mrouerugrurumm!!");

            for (i = sizeof(gente); i--;) {
                if (environment(gente[i]) &&
                    geteuid(environment(gente[i])) == "Takome") {
                    tell_object(
                        gente[i],
                        "%^BOLD%^GREEN%^¡Un gutural grito de Loredor, el "
                        "Anciano Ent, advierte de un ataque al Bosque de "
                        "Thorin!%^RESET%^\n");
                }
            }
        }
    }

    if (amigo) {
        if (!dame_protector() && amigo) {
            fijar_protector(amigo);
            amigo->do_emote("protege a " + query_short() + ".\n");
        }

        if (!amigo->dame_amo() || amigo->dame_amo() == "salvaje") {
            amigo->fijar_amo(query_name());
        }

        if (!amigo->dame_jinete()) {
            amigo->fijar_peso_max(amigo->dame_peso_max() + dame_peso() + 500);
            queue_action("montar loredor");
        }

        if (!amigo->dame_efecto("bendicion") &&
            !query_timed_property("Bloqueo:hechizo:Bendicion")) {
            habilidad("bendicion", amigo);
            return;
        }
    }

    // Hechizos
    if (!dame_efecto("bendicion") &&
        !query_timed_property("Bloqueo:hechizo:Bendicion")) {
        habilidad("bendicion", this_object());
        return;
    }

    if (!dame_efecto("piel de corteza")) {
        habilidad("piel de corteza", this_object());
        return;
    }

    // Curas a ella
    if (dame_pvs() < dame_pvs_max() / 3 &&
        !query_timed_property("Bloqueo:hechizo:Curar heridas serias")) {
        habilidad("curar heridas serias", this_object());
        return;
    }

    if (dame_pvs() < dame_pvs_max() / 2 &&
        !query_timed_property("Bloqueo:hechizo:Curar heridas moderadas")) {
        habilidad("curar heridas moderadas", this_object());
        return;
    }

    if (dame_pvs() < dame_pvs_max() &&
        !query_timed_property("Bloqueo:hechizo:Curar heridas ligeras")) {
        habilidad("curar heridas ligeras", this_object());
        return;
    }

    // Curas al ent
    if (!amigo)
        return;

    if (amigo->dame_pvs() < amigo->dame_pvs_max() / 3 &&
        !query_timed_property("Bloqueo:hechizo:Curar heridas serias")) {
        habilidad("curar heridas serias", amigo);
        return;
    }

    if (amigo->dame_pvs() < amigo->dame_pvs_max() / 2 &&
        !query_timed_property("Bloqueo:hechizo:Curar heridas moderadas")) {
        habilidad("curar heridas moderadas", amigo);
        return;
    }

    if (amigo->dame_pvs() < amigo->dame_pvs_max() &&
        !query_timed_property("Bloqueo:hechizo:Curar heridas ligeras")) {
        habilidad("curar heridas ligeras",amigo);
        return;
    }
}

int do_death(object pl)
{
    tell_accion(environment(this_object()),"¡Una maleza desmesuradamente crecida toma a "+query_short()+" y la aleja del peligro!\n","",({}),this_object());
    dest_me();
    return 0;
}
