//Snaider 9-8-2008
//Zona para novatos de Kattak

inherit "/std/outside";
#include "../../path.h"

void setup() 
{
  object fogata;
  
  set_short("%^ORANGE%^Campamento de Naindel%^RESET%^");
  set_long("Este es el lugar de descanso de los muchos leñadores que trabajan "
  "en el campamento maderero. Varios troncos han sido colocados a modo de "
  "bancos, mientras que otros sirven de combustible para una fogata. Además,"
  "el suelo se encuentra lleno de marcas de carretas, y todas se juntan al "
  "norte del lugar, donde desaparecen bajo los adoquines del sendero a la "
  "ciudad de Kattak.\n");
  
  add_item(({"adoquines","sendero","camino"}), "Justo al noroeste del lugar "
  "comienza el sendero que recorre la ladera oeste del valle hasta la mercantil"
  " ciudad de Kattak.");

  add_item(({"bancos","banco"}), "Enormes troncos han sido cortados "
  "con el fin de ofrecer asiento a los muchos trabajadores que aquí trabajan.");
       
  fijar_luz(100);
   
  add_clone(NPCS+"humano_naindel.c",1);
  
  set_exit_color("cian_flojo");
  add_exit("noroeste",NOVATO_KATTAK+"sendero_3.c","road");
  add_exit("este",NOVATO_KATTAK+"campamento_2.c","plain");
  add_exit("sur",NOVATO_KATTAK+"campamento_3.c","plain");
  
  fogata= clone_object("/std/object");
	fogata->set_name("fogata");
	fogata->add_alias(({"fogata","fuego"}));
	fogata->add_plural(({"fogatas"}));
	fogata->set_short("Fogata");
	fogata->set_main_plural("Fogatas");
	fogata->set_long("Guarecido por un círculo de piedras y tierra se levanta "
  "un leve fuego. Su combustible no es otro que los restos de madera que el "
  "campamento desecha. Hasta las inmediaciones se acercan trabajadores del "
  "lugar, para calmar el cansancio o para resguardarse del frío y la humedad "
  "que siempre pueblan el lugar.\n");
  fogata->fijar_genero(2);
	fogata->move(this_object());
	fogata->reset_get();
}
