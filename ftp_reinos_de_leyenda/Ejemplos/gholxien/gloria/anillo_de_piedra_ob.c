// Satyr 23.09.2016
#include <baseobs_path.h>
#include <invocaciones.h>
inherit "/obj/invocacion.c";
inherit "/obj/pnjs/conversador.c";

private object anillo;

object dame_anillo()        { return anillo; }
void fijar_anillo(object a) { anillo = a;    }
void setup()
{
    object estilo;
    
    set_name("djinn");
	set_short("Djinn");
	set_main_plural("Djinns");
	set_long(
        "Una criatura mágica de medio metro de altura y piel del color del océano profundo. Sus "
            "ojos rasgados son de un intenso color amarillo.\n\n"
        
        "  De cintura para arriba tiene forma humanoide mientras que su parte inferior termina "
            "en una forma gaseosa que humea y le permite levitar.\n\n"
            
        "  Su cabeza afeitada está adornada con muchos tatuajes y sus finos dedos, cubiertos "
            "de baratijas, no dejan de mesarse una barba de chivo de color hollín.\n"
    );

    add_static_property("electrico", 100);
    add_static_property("tierra"   , 100);
    
	fijar_raza("npc/elemental");
	fijar_clase("hechicero");
    
	nuevo_estilo("basicos/electrico");
    
    if ( clonar_estilo() ) {
        estilo = dame_ob_estilo();
        estilo->quitar_ataque("tabla");
        estilo->nuevo_ataque("tabla", "electrico", 2, 40, 100);
    }
    
    fijar_altura(800);
    fijar_peso_corporal(40000);
    
	fijar_alineamiento(0);
	fijar_genero(1);

    set_random_stats(18, 18);
    fijar_extrema(100);
    fijar_con(20);
    fijar_int(25);
    fijar_sab(20);
    fijar_nivel(30);

    add_clone(BARMADURAS + "anillo.c", 1)->reset_drop();
	init_equip();
    
    nueva_habilidad_invocacion("curacion");
    nueva_habilidad_invocacion("vueltaacasa");
    nueva_habilidad_invocacion("rejuvenecimiento");
    
    seguir_incondicional();
    permitir_buscar();
    permitir_coger();
    mismos_idiomas();
    evitar_reset();
    sin_cadaver();
    
    nuevo_dialogo("nombre", ({
        "'Mi nombre no es importante. Basta decir que existo para servir.",
        "'Confío en que mi invocador encuentre mis servicios de su agrado.",
        "'Puedo conferir pequeños milagros."
    }));
    nuevo_dialogo("servir", ({
        "'Los míos existimos para servir.",
        "'Cuanto más sirvamos más poderosos nos haremos y más efectivos serán nuestros milagros.",
        "'Los más poderosos de nuestra estirpe, finalmente, pueden desear ser libres y no "
            "obedecer a nadie.",
        "'Pero yo estoy muy lejos y humildemente y desde el respeto presento mis servicios a mi invocador."
        }), "nombre"
    );
    nuevo_dialogo("milagros", ({
        "'Puedo conferir tres milagros.",
        "'Mi milagro curativo ayudará a cerrar las heridas de mi amo, así como a sanar algo del"
            "daño que haya sufrido.",
        "'El milagro de rejuvenecimiento ayudará a recuperar energías a mi amo.",
        "'El milagro de vueltaacasa devolverá a mi amo a su ciudad de origen (o a Anduar si mi amo fuese "
            "vagabundo). Este milagro agotará tantos mis energías como las vuestras y hará que el amo "
            "no pueda invocarme durante mucho tiempo."
        }),
        "nombre"
    );
    habilitar_conversacion();
}
string dame_titulo(object para) {
    return element_of(({
        ({"Mi Señor"                 , "Mi Señora"}),
        ({"Su Grandilocuente Señoría", "Su Grandilocuente Señoría"}),
        ({"Su Magnificencia"         , "Su Magnificencia"}),
        ({"Mi Amo y Señor"           , "Mi Ama y Señora"}),
    }))[para->dame_genero() == 2 ? 1 : 0];
}
string dame_mensaje_error(int tipo) {
    string a = dame_titulo(dame_invocador());
    
       switch(tipo) {
           
        case _INV_MSJ_CONFIRMACION:      
            return "Como desee " + a;
            
        case _INV_MSJ_NO_ABORTABLE:      
            return a + ", me temo que no estoy en ninguna acción abortable.";
            
        case _INV_MSJ_EN_PELEAS:         
            return "Por mas que me gustase satisfacer a "+ a + ", me temo que estoy "
                "en medio de un combate y no puedo hacerlo.";
                
        case _INV_MSJ_SIN_PELEAS:        
            return "Se que "  + a + " nunca se equivoca, pero... ¿se ha fijado en que no "
                "estoy en ningún combate?";
                
        case _INV_MSJ_MARCHARSE:         
            return "Ha sido un honor servir a " + a + ". Confío en que en el futuro "
                "se me presente otra oportunidad de hacerlo.";

        case _INV_MSJ_SIN_OBJETIVO:      
            return a + ", me temo que ese objetivo no es válido para vuestros deseos.";

        default:                         
            return ::dame_mensaje_error(tipo);
    } 
}
void orden_curacion(string t) {
    object b    = dame_invocador();
    int heridas = sizeof(b->dame_incapacidades("heridas"));
    int i;
    
    if ( query_property("milagro") ) {
        mensaje_invocador("Me temo, " + dame_titulo(b) + " que no tengo energías para más milagros.");
        return;
    }
        
    if ( b->dame_pvs() >= b->dame_pvs_max() && ! heridas) {
        mensaje_invocador(dame_titulo(b) + ", no necesitáis ese milagro. Estáis perfectamente saludable.");
        return;
    }
    
    mensaje_invocador("Que así sea, " + dame_titulo(b) + ". Usaré mis poderes para sanaros.");
    
    if ( environment() )
        tell_room(environment(), query_short() + " usa su milagro sanador sobre " + b->query_cap_name() + ".\n", b);

    
    b->spell_damage(20000, "curacion", TO);
    heridas = 1 + heridas / 2 + random(heridas / 2);
    
    foreach(string str in b->dame_incapacidades("heridas")||({})) {
        if ( i >= heridas )
            break;
        
        i++;
        
        if ( ! b->quitar_incapacidad(str, "heridas", TO) )
            mensaje_invocador("Mis poderes no pueden cerrar vuestra herida " + str + "," + dame_titulo(b) + ".");
    }
    
    tell_object(b, "Te sientes mejor cuando " + query_short() + " te toca con sus energías curativas.\n");
    add_property("milagro", 1);
}
void orden_rejuvenecimiento(string t) {
    object b = dame_invocador();

    if ( query_property("milagro") ) {
        mensaje_invocador("Me temo, " + dame_titulo(b) + " que no tengo energías para más milagros.");
        return;
    } 
    
    if ( b->dame_pe() >= b->dame_pe_max() ) {
        mensaje_invocador(dame_titulo(b) + ", no necesitáis ese milagro. Estáis perfectamente rejuvenecido.");
        return; 
    }
    
    mensaje_invocador("Que así sea, " + dame_titulo(b) + ". Usaré mis poderes para rejuveneceros.");
    
    if ( environment() )
        tell_room(environment(), query_short() + " usa su milagro rejuvenecedor sobre " + b->query_cap_name() + ".\n", b);

    tell_object(b, "Te sientes rejuvenecid" + b->dame_vocal() + " cuando " + query_short() + " "
        "te toca con sus energías curativas.\n"
    );
    b->ajustar_pe(b->dame_pe_max() / 6 + random(b->dame_pe_max() / 6));
    add_property("milagro", 1);
}
void orden_vueltaacasa(string t) {
    object b = dame_invocador(), casa;
    object anillo;
    
    if ( query_property("milagro") ) {
        mensaje_invocador("Me temo, " + dame_titulo(b) + " que no tengo energías para más milagros.");
        return;
    } 

    if ( ! anillo = dame_anillo() ) {
        mensaje_invocador("Algo va mal, ¿¡dónde está vuestro anillo, " + dame_titulo(b) + "?!");
        return;
    }
    
    if ( sizeof(filter(b->dame_enemigos_recientes(360), (: $1 && $1->query_player() :))) ) {
        mensaje_invocador("No podré hacer ese milagro mientras vuestros últimos combates contra "
            "jugadores aún sean tan recientes, " + dame_titulo(b) + "."
        );
    }
    
    if ( ! (casa = b->dame_ob_ciudadania()) || ! casa = load_object(casa->dame_posicion_inicial()) ) {
        mensaje_invocador("Algo va mal, " + dame_titulo(b) + ". ¡No puedo llevaros a casa!");
        return;
    }
    
    if ( casa == environment(b) ) {
        mensaje_invocador(dame_titulo(b) + ", ¡ya estáis en casa!");
        return;
    }
        
    anillo->poner_bloqueo_vueltaacasa(b);
    mensaje_invocador("Que así sea, " + dame_titulo(b) + ". Le llevaré a casa.");
    
    b->accion_violenta();
    tell_room(environment(b), query_short() + " abre un portal mágico que tanto " + dame_pronombre() + " "
        "como " + b->query_cap_name() + " atraviesan antes de que se cierre.\n", b);
    tell_object(b, query_short() + " abre un portal mágico. Tras cerrar los ojos y darle la mano a tu "
        "fiel sirviente te encuentras de nuevo en casa.\n"
    );
    
    TO->move(casa);
    b->move(casa);
    b->mirar();
    
    tell_accion(environment(b), "Un portal mágico se abre en la sala y deja aquí a "
        + query_multiple_short(({b, TO})) + " antes de cerrarse.\n", b);
        
    fijar_pe(0);
    b->fijar_pe(0);
    add_property("milagro", 1);
}