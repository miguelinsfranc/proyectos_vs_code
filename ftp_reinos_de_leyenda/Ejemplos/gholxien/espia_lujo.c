/*
    Satyr 2006
    Espía del culto al lujo
    Parte de una quest para inquisidores
    
    Satyr 2014, refactorizando un poco.
    
    Ver en /d/dendra/doc donde clonan estos tios
*/
#include <baseobs_path.h>
#include "/d/dendra/path.h"
inherit BASES_NPCS + "base_npc_lujo.c";


#define ANILLO ARMADURAS + "anillo_amatista.c"
#define ESCONDERSE "/habilidades/shadows/esconderse_sh.c"
#define SIGILAR "/habilidades/bribones/sigilar.c"
#define PUNYALADA "/habilidades/bribones/apunyalar.c"

// Switch para la huida
int j;
int movido;

void escondete()
{
    clone_object(ESCONDERSE)->setup_sombra(this_object(),60,0);
    set_hidden(1);
}

void preparar_descripciones()
{
    set_name("espia");

    set_short("Espía del Culto al Lujo");
    set_main_plural("Espías del Culto al Lujo");

    add_alias(({"espia","espía","espia del culto al lujo","espía del culto al lujo"}));
    add_plural(({"espias","espías","espías del culto al lujo","espias del culto al lujo"}));

    set_long(
        " Una figura que está agazapada entre las sombras del lugar y que usa sus oscuros atuendos para fundirse con ellas. "
        "La figura es escuálida, de complexión atletica y especialmente alargada; sus movimientos son rápidos como un parpadeo y parece que no "
        "pisa el suelo cuando se mueve. "
        "Parece poner especial cuidado en la observación de su entorno y estar atento en lo que pueda pasar como consecuencia a cada uno de sus "
        "pasos. Te mira con expresión de sorpresa, quizás extrañado de que le hayas encontrado.\n"
    );
}
void preparar_rol()
{
    ::preparar_rol();

    fijar_genero(1);
    fijar_clase("ladron");
    fijar_estatus("Dendra"  , -999);
    add_property("espia_lujo",   1); // Aun en el 2014 no se para que cojones es esto xD
}

void preparar_atributos()
{
    ::preparar_atributos();

    set_random_stats(18, 21);
    adjust_tmp_damage_bon(100);
    ajustar_bo(100);
    fijar_des(23);
    fijar_nivel(31);
    
    fijar_sigilar(1);
    set_hidden(1);
}
void preparar_agresividad()
{
    ::preparar_agresividad();
    add_hated("ciudadania", "dendra");
    add_hated("religion"  , "seldar");
    fijar_memoria_jugador(1);
    set_aggressive(12);
}
void preparar_equipo()
{
    add_clone(ARMADURAS  + "mascara_morada.c", 1);
    add_clone(BARMADURAS + "pantalones.c"    , 1);
    add_clone(BARMADURAS + "zapatillas.c"    , 1);
    add_clone(BARMAS     + "puñal.c"         , 1);
    add_clone(BARMADURAS + "cuero.c"         , 1);
    set_heart_beat(3);
    init_equip();
    
    ajustar_dinero(7*5,"bela");
}

void muevete()
{
    int i;
    string salida,*salidas=environment(this_object())->query_direc();

    // Paramos peleas y...

    for (i=0;i<sizeof(query_attacker_list());i++)
    {
        query_attacker_list()[i]->stop_fight(this_object());
        add_static_property(query_attacker_list()[i]->query_cap_name()+".",1);
        stop_fight(query_attacker_list()[i]);
    }

    i=0;

    salida=salidas[random(sizeof(salidas))];

    while (environment()->query_where_dir(salida)==query_static_property("ultima_salida"))
    {
        salida=salidas[random(sizeof(salidas))];

        i++;

        if (sizeof(salidas)==1)
            break;
    }

    //salida=salidas[random(sizeof(salidas))];

    if (environment()->query_door(salida)&&!environment()->query_open_door(salida))
        if (!environment()->query_locked_door(salida))
            environment()->adjust_open_door(salida,0,0);

    movido++;
    if (movido<=3+random(3))
    {
        load_object(environment()->query_where_dir(salida));
        add_static_property("ultima_salida",real_filename(environment()));
        SIGILAR->do_command(salida,this_object());
        call_out("muevete",2);
    }
    else
        clone_object(ESCONDERSE)->setup_sombra(this_object(),60);

}
void huye()
{
    string *salidas=environment(this_object())->query_direc();

    if ( ! sizeof(salidas) ) {        
        return;
    }

    if (query_hidden()) {
        return;
    }


    if (query_timed_property("bomba_humo")) {
        return;
    }

    if ( ! query_timed_property("tiempo_huida")) {
        // Si no lo matan en 90 segundos desaparece
        add_timed_property("tiempo_huida", 1, 90);
        j = 1;
    }

    do_say("¡Jamás me capturarás!", 0);
    tell_room(environment(),query_short()+" arroja una bomba de humo y desaparece ante tus ojos.\n");
    add_timed_property("bomba_humo", 1, 30);

    reset_attacker_list();
    resetear_lista_perseguidos();
    movido = 0;
    muevete();
}
void heart_beat()
{
    ::heart_beat();
    
    if ( ! query_timed_property("me_descubrieron") && ! query_hidden() ) {
        // No estoy ni escondido ni en peleas y no me han descubierto, así que voy a correr y esconderme
        if ( ! dame_peleando() && ! query_timed_property("me_descubrieron") ) {
            add_timed_property("me_descubrieron", 1, 5);
            run_away();
            clone_object(ESCONDERSE)->setup_sombra(this_object(),60);
            set_hidden(1);

            if ( ! query_timed_property("tiempo_huida"))
                add_timed_property("tiempo_huida", 1, 90);
        }
        // Estoy escondido y persigo a gente, pero no en peleas, mejor me escondo
        else if (dame_peleando() <= 0) {
            clone_object(ESCONDERSE)->setup_sombra(this_object(),60);
            set_hidden(1);        
        }
        
        // OK, estoy en peleas, si no tengo lock de bomba de humo, la uso
        if (dame_peleando() > 0 && ! query_timed_property("bomba_humo") ) {
            call_out("huye", 5);
        }
    }
    
    // Ha pasado el tiemop de huída
    if ( ! query_timed_property("tiempo_huida") && j ) {
        dest_me();
    }
}
void event_enter(object quien_entra,string mensaje,object procedencia,object *seguidores)
{
    if ( quien_entra && living(quien_entra) && quien_entra->query_player() ) {
        if ( query_static_property(quien_entra->query_cap_name() + ".")) {
        
            if (quien_entra->query_timed_property("no_apunyalar"))
                quien_entra->remove_timed_property("no_apunyalar");    
                
            if (query_hidden())
                PUNYALADA->do_command(quien_entra,this_object());
            else
                quien_entra->attack_by(this_object());
                    
            add_attacker_list(quien_entra);
        }
    }
    
    ::event_enter(quien_entra,mensaje,procedencia,seguidores);
}

void attack_by(object pj)
{
    accion_violenta();
    this_object()->destruir_esconderse();
    
    if ( pj ) {
        add_static_property(pj->query_cap_name()+".",1);
        attack_ob(pj);
        ::attack_by(pj);
    }
}

int weapon_damage(int cantidad,object atacante,object arma,string tipo_ataque)
{
    this_object()->destruir_esconderse();
    accion_violenta();
    
    if ( atacante ) {
        atacante->add_timed_property("enemigo_culto",1,20);
        add_static_property(atacante->query_cap_name()+".",1);
        attack_ob(atacante);
    }
    
    return ::weapon_damage(cantidad,atacante,arma,tipo_ataque);
}

int do_death(object asesino)
{
    object mascara;

    if ( ! random(20)) {
        tell_object(asesino, "Al darle el golpe de gracia a "+this_object()->query_short()+" ves como algo metálico se cae de sus bolsillos y rebota por el suelo.\n");
        clone_object(ANILLO)->move(environment());
    }

    if ( ( mascara = present("mascara", this_object()) ) && asesino = dame_asesino_real(asesino) ) {
        mascara->add_property("propietario", asesino->query_name());
    }

    return ::do_death(asesino);
}