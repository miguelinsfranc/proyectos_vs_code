//Sierephad Dic 2k18	-- Anitguos demonios de las profundidades de inovercy
//					 	-- Adapto a la base de demonios de Naggrung 
//						-- Mejoro descripciones y ambientacion
//						-- Les añado la habilidad de mordisco + desgarrar

#include "/d/naggrung/path.h";
inherit BASES_NPC+"base_demonios_tanar_ri.c";

object enemigo_actual;

//Gestion de los elementos descriptivos name,short,long alias
void poner_descriptivo()
{
	
	set_name("licaonoptku");
    set_short("Licaonoptku, Sabueso Infernal");
    set_main_plural("Licaonoptkus, Sabuesos Infernales");
    generar_alias_y_plurales();
    set_long("Este tipo de demonio rara vez es visto en la superficie pues suele habitar en túneles y cavernas "
		"de los abismos infernales. Este demonio es una extraña mezcla de demonio-reptil, algo peculiar, pues "
		"suele combatir como un bípedo humanoide, pero cuando se desplaza por los túneles en el interior de las "
		"cavernas lo hace como una bestia de cuatro patas. Los grandes señores del infierno los suelen dominar y "
		"utilizar para guardar y proteger el interior de sus guaridas, tarea en la que son increíblemente eficaces, "
		"debido a la ferocidad que demuestran ante los pobres incautos que osan aventurarse en sus dominios. La "
		"mayor parte de su cuerpo, desde su cabeza reptóide hasta su larga cola, está recubierto por una piel "
		"escamosa de tonos cobrizos. Tiene unas gigantescas mandíbulas llenas de afilados dientes que utiliza "
		"para morder y desgarrar la carne de sus enemigos, así como poderosas garras en sus brazos y patas.\n");
	
}

//Gestion de emotes y frases
void poner_chat()        
{ 
	load_chat(30,	({
		":abre sus fauces mostrando sus enormes y afilados dientes.",
		":abre y cierra furiosamente sus mandíbulas buscando algo que morder.",
		":espera agazapado entre la oscuridad esperando a una presa.",
		":ruge haciendo que su monstruoso rugido se propague con el eco por los alrededores.",
		"'¡¡Grrrgaaahhhhgggghhh!!",
		":olisquea el aire a su alrededor intentando detectar a alguna presa cercana.",
		":da vueltas por la zona, cambiando varias veces su forma de caminar de dos a cuatro patas.",
		":araña frenéticamente varias rocas cercanas intentando afilarse las garras de sus patas.",
		":empieza a masticar frenéticamente con sus poderosas mandíbulas los restos de un hueso hasta "
			"que termina partiéndolo en varios trozos.",
		":alza antinaturalmente su cuello reptiliano."
	}));
	
	load_a_chat(30,	({
		":comienza a serpentear a tu alrededor mientras se relame.",
		":lanza hacia delante la cabeza para intentar morderte.",
		":ruge abriendo su mandíbula de par en par mostrando sus infinitas hileras de afilados dientes.",
		":abre y cierra coléricamente sus mandíbulas intentando morderte.",
		"'¡Voy a devorarte entero!",
	}));

}

//Gestion de atributos
void poner_atributos()   
{ 

	//Caracteristicas fisicas
	fijar_genero(1);
	fijar_altura(150+random(50));
	fijar_peso(dame_altura()*1000);

	//Caracteristicas de ficha
	fijar_clase("luchador");
	set_random_stats(15,20);
	fijar_fue(20+random(10));
	set_con(20+random(10));
	
	fijar_nivel(30);
	fijar_pvs_max(4000+random(1000));
	set_move_after(5,random(10));
	
	ajustar_bo(150+random(50));
	adjust_tmp_damage_bon(70+random(40));
	
	set_heart_beat(1);
}


void heart_beat() {
	
	object *player_atacantes;
	
    ::heart_beat();

	//Buscamos un target valido
	if (!sizeof(query_attacker_list()))
		return;
	
	if (!enemigo_actual) {
		player_atacantes = filter(query_attacker_list(), (: $1->query_player() :));
		if (sizeof(player_atacantes)) {
			enemigo_actual=element_of(player_atacantes);	
		} else {
			enemigo_actual=element_of(query_attacker_list());
		}	
	}
	
	//Comprobaciones varias
	if (!enemigo_actual || !ENV(enemigo_actual)) {
		return;
	}
	
	if (ENV(TO)!=ENV(enemigo_actual))
		return;
	
	//Comprobaciones varias
	if (enemigo_actual->query_hidden()) 
		return;
	
	//Si tiene a algun objetivo mordido
	if (TO->dame_ob_mordido()) {
		if (random(query_timed_property("intento_desgarrar"))) {
			habilidad("desgarrar",TO->dame_ob_mordido()->query_name());
			enemigo_actual=0; 	
		} else {
			add_timed_property("intento_desgarrar",query_timed_property("intento_desgarrar")+1,5);
		}
		return;
	} //Si no, pos muerde
	else {
		if(!query_timed_property("bloqueo-Mordisco"))
			habilidad("mordisco",enemigo_actual->query_name());
	}
	
}


