// 2021.02.21 [Vhurkul]: Gremio de Ralder

#include "../../../include/zona_wareth.h"

inherit "/std/subterraneo";

void setup() {
object fuente;    

	add_exit(N, ROOMS_RALDER "templo", "corridor");
    add_exit(O, ROOMS_RALDER "elecciones", "corridor");
    add_exit(S, ROOMS_RALDER "entrada", "corridor");
    add_exit(E, ROOMS_RALDER "baul", "corridor");
    add_exit(NO, ROOMS_RALDER "tienda", "corridor");

    AUTOGENERAR_SALA;

    set_short("Círculo del %^RED%^Sim%^BOLD%^GREEN%^bionte%^RESET%^");

    // Especial
    add_clone("/obj/misc/tablon", 1, function (object ob) {
        ob->fijar_tablon("circulo_simbionte");
    });


if (fuente = clone_object("/obj/misc/fuente_ob.c")) {
        fuente->move(TO);
        fuente->set_name("manantial");
        fuente->set_short("Manantial subterráneo");
        fuente->set_main_plural("Manantiales subterráneos");
        fuente->generar_alias_y_plurales();
        fuente->set_long("Se trata de un manantial que contiene agua potable, que usan los adeptos del simbionte para beber, o para refrescarse en los días calurosos, ya que en dichos días la humedad es bastante cargada aquí abajo, y el excesivo calor obliga a refrescarse de vez en cuando, para no morir de deshidratación. El agua cae desde las raíces que se enrollan alrededor de las largas estalactitas, proveniente de las lluvias que caen sobre el bosque de Wareth, y filtrada através de la tierra, hasta caer en el manantial, produciendo un suave y continuo goteo, que ayuda a los druidas a relajarse mientras fuman sus alargadas pipas. Algunas canalizaciones naturales conducen el agua del manantial a los túneles del sur, donde el agua es aprovechada para el continuo crecimiento de las raíces, evitando que el manantial se desborde.\n");
    }
}
