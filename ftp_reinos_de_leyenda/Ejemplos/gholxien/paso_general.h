/**
     GUARDIA
     Alias del PNJ "guardia" que se buscará en la sala. 
     
     DIPLOMACIA_MINIMA HOSTILIDAD
     La diplomacia mínima a la que se empezará a bloquear el paso. 
     
     Por defecto toma el valor HOSTILIDAD (definido en /include/ciudadanias.h).
     
     A notar que entre anárquicos, se usará el valor de AMISTAD y no el estatus en la ciudadanía.
     
     FUNCION
     Permite cambiar el nombre de la función que gestiona el paso. Por defecto, la función se llama "pasar" y tiene este prototipo:

        int pasar(string salida,object caminante,string msj_especial)
        - Si devuelve 1, se pasa
        - Si devuelve 0, no, se puede usar notify_fail
    
     CIUDADANIA
     Permite hacer chequeos de estatus en una ciudadanía dada. 
    
     CIUDADANO y LISTA_CIUDADANIAS
     Solo permite el paso a los miembros de la ciudadania(s) especificada(s)
    
     GREMIO    
     Solo permitirá el paso a jugadores de un gremio(s) especificado(s).
     
     BANDO y LISTA_BANDOS
     Solo permitirá el paso a jugadores de un bando(s) especificado(s).
     
     RAZA y LISTA_RAZAS
     Solo permite el paso a jugadores de una raza(s) especificada(s).
     
     NO_PERMITIR_MONTURAS
     Hace que las monturas o jinetes no puedan pasar. 
             
     CIUDADANIAS_DEFENSORAS
     Un array de nombres de ciudadanía defensora. De existir, se usarán los PJs o NPCs no-escondidos de la sala para actuar como guardias, si estos no existiesen, impidiendo así el paso. 
     
     NO_PJS
     Si se define, los PJs no podrán actuar como guardias (ver CIUDADANIAS_DEFENSORAS arriba).

     CHEQUEO_ADICIONAL
     Si está definido se llamará a la función "chequeo_adicional" de la sala para decidir si puede pasar o no.
     
     La función tiene este prototipo:
        int chequeo_adicional(string salida, object caminante, objectguardia)
     
     Si devuelve 1, se pasa; si devuelve 0, no.

    PERMITIR_ESCONDIDOS  
    Permite que los personajes escondidos puedan pasar por los guardias.
    
    PERMITIR_SOLO_BRIBONES
    Hara que solo los personajes de tipo "bribon" puedan colarse en la ciudad.
    
    SALVACION_ESCONDIDOS
    El % de éxito al sigilar. Por defecto es 100%.
    Tened en cuenta que, al fallar un intento, se pone al sigilador un lock de 60+random(180) segundos.
    
    FUNC_DESCUBIERTO
    Si está definida, se evaluará la funcion con su mismo nombre y se le pasarán los siguientes parámetros:
    
        string salida, object caminante, string msj_especial, object guardia
    
    AGRESIVIDAD
    Si está definido, se atacará a los PJs/PNJs que intenten pasar sin éxito.
    Si no está definido, pero está definido CIUDADANIA se usará el sistema de diplomacias.
    Si no hay CIUDADANIA ni AGRESIVIDAD, no se atacará.
    
    SIN_SALUDOS
    Si está definido, no se saludarán a los pjs que pasen por la puerta
    
    CAOTICO_MALVADO
    Personaliza los mensajes de los guardias a los jugadores para un alineamiento más malo.
    
    
    MSJ y MSJ_ROOM    
    Mensajes que se mostrarán al jugador o a la sala en el caso de que un caminante falle. 
    
    Podéis usar los objetos "caminante" y "guardia" en el define, así como el string "salida"
    Ejemplos:
    
        #define MSJ guardia->query_short() + " impide que te acerques a la salida " + salida + "."
        #define MSJ_ROOM guardia->query_short() + " impide el paso a " + caminante->query_cap_name() + "."
        
    MSJ_DORMIDO
    Por defecto, un guardia dormido se despierta para hacer su labor, pero si definimos un mensaje en este define, no lo hará y se mostrará el mensaje al jugador.
*/
#include <estatus.h>
#include <ciudadanias.h>
#define SALVACION_ESCONDIDOS 20 
 
#define NO_PJS
#define CAOTICO_MALVADO
#define CIUDADANIA "Dendra"
#define DIPLOMACIA_MINIMA NEUTRALIDAD
#define CIUDADANIAS_DEFENSORAS ({"dendra"})
#define LISTA_BANDOS "malo","mercenario","neutral","anarquico"
#ifdef SOLO_MANDATARIOS_DE
#define CHEQUEO_ADICIONAL
int chequeo_adicional(string salida, object caminante, object guardia)
{
    object b;
    
    if ( ! guardia || caminante->query_hidden() ) {
        return 1;
    }
    
    if ( ! b = load_object(SOLO_MANDATARIOS_DE) ) {
        return 1;
    }
    
    if ( ! b->es_mandatario(caminante->query_name())) {
        guardia->do_say("Esta sala está cerrada para los de tu estatus en " + b->query_short() + ".", 0);
        return notify_fail(guardia->query_short() + " te cierra el paso.\n");
    }
    
    return 1;
}
#endif

