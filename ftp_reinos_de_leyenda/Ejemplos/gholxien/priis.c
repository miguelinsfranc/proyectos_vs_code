// Satyr 2k7 -- Reina Priis de Takome
// Baelair 5-Jul-2012. Adaptando al nuevo sistema de misiones. 
// Cambiando move() por entregar_objeto(), y event_dar() por entregar en conversación
// Satyr 2014 -- fijar_objetos_morir
// Satyr 20.07.2016 -- Diálogo para la quest del Primer Monarca
#include "../path.h"
inherit "/obj/conversar_npc.c";
int check_anyone_here()    { return 1; }

int do_say(string tx,int i)
{
    tx=" %^BOLD%^CYAN%^"+tx+"%^RESET%^ ";
    return ::do_say(tx,i);
}
void debug(string tx)    
{ 
    object b;
    if ((b=find_player("satyr"))&&b->query_property("info_quest"))
        tell_object(find_player("satyr"),"[Debug-Priis]: "+tx+"\n"); 
}

int resistencia_magica(int nivel_lanzador,string caracteristica,string escuela,object hechizo)
{
    if (hechizo)    
        switch(lower_case(hechizo->dame_nombre(hechizo))) {
            case "enmudecer":
                return 1;
        }

    return 0;
}

int frases_personalizables(string tipo_frase,object npc,object pj)
{
    object nazi;
    switch(tipo_frase)
    {
        case "despedida-npc":
            do_say("¡Que Eralie sea contigo "+pj->dame_nombre_completo()+"!",0);
            
            if (pj->query_static_property("agachado"))
            {
                pj->remove_static_property("agachado");
                do_say("¡Levántate con orgullo!",0);
            }
            if (!pj->dame_efecto("bendicion"))
                habilidad("bendicion",pj);
            break;
        case "noentiende-npc":
            do_say("Os pido disculpas por mi ignorancia, "+pj->dame_nombre_completo()+", más "
            "no entiendo la lengua que hablais.",0);
            break;
        case "bienvenida-pj":
            if ((nazi=query_property("portavoz"))&&environment(nazi)==environment())
            {
                if (pj->query_name()!=("/grupos/gremios/cruzada_eralie.c"->dame_supremo()))
                {
                    if (!pj->query_property("agachado"))
                    {
                        pj->add_static_property("agachado",1);
                        nazi->do_say("¡Cuádrate ante la reina "+pj->dame_nombre_completo()+"!, "
                        "¡muéstrale el debido respeto a su majestad!",0);
                        tell_object(pj,nazi->query_short()+" te obliga a arrodillarte "
                        "ante la reina.\n");
                    }
                    else
                        tell_object(pj,nazi->query_short()+" te mira con aprobación por "
                        "tu cuidado respeto ante la reina.\n");
                }
                else
                tell_object(pj,nazi->query_short()+" se cuadra ante ti al reconocerte como "
                +pj->dame_articulo()+" Suprem"+pj->dame_vocal()+" Cruzad"+pj->dame_vocal()+".\n");
            }
            else    return 0;
            break;
        case "bienvenida-npc":
            do_emote("hace una breve reverencia.");
            do_say("Decidme, "+pj->dame_nombre_completo()+". ¿Qué asuntos os traen ante mi?",0);
            break;
        default: return 0;
    }
    return 1;    
}

void chequea_mensaje(string msj)
{
    string tx;
    switch(string_sencillo(msj))
    {
        case "¿Y vos?, ¿teneis una audiencia?, ¡no podeis estar aquí si no la teneis!":
            tx="Mi fiel guardian, no os preocupeis. Atenderé a todo el que de buen corazón "
            "acuda a este alcazar. No teneis que mostraros tan intolerante.";
            break;    
        case "Si así lo necesitais, Mi Señora, puedo hacer que cierren durante unas horas "
             "el alcazar real para que os dejen a solas con vuestros pensamientos.":
            tx="No. Gracias, mi fiel guardián, pero no puedo detener asuntos de importancia "
            "que requieran mi atención por mis intereses personales. Estaré bien en cuanto "
            "una oración reconforte mi espíritu.";
            break;
        case "¡Y puede contar conmigo para hacerlo, excelencia!, ¡estaré a vuestro lado "
        "por siempre!":
            tx="Lo sé, mi noble protector. Habeis seguido bien el camino de Eralie, es todo "
            "un honor para mi gozar de tan bravos guardianes.";
            break;
    }
    if (tx) do_say(tx,0);
}

void event_person_say(object quien,string encabezado,string msj,string lenguaje,string *msjs)
{
    if (real_filename(quien)=="/d/takome/npcs/cruzado_real")
        call_out("chequea_mensaje",1,msj);
}

void setup()
{
    set_name("priis");
    add_alias(({"reina","priis"}));
    add_plural(({"priis","reinas"}));
    set_short("%^BOLD%^YELLOW%^Reina Priis de Takome%^RESET%^");
    set_main_plural("%^BOLD%^YELLOW%^Reina Priis de Takome%^RESET%^");
    set_long(
        "\tAnte ti la actual monarca de Takome, la reina Priis de la familia Blochessrf que fue "
        "recientemente elegida como la representante de los nobles por su inmensa sabiduría. "
        "Su figura es la de una mujer de unos 20 años que ha sido obligada a tomar el cargo demasiado "
        "prematuramente y en su expresión puedes ver atisbos de inexperiencia, aunque sus ojos "
        "brillan con una fuerte determinación. "
        "Luce ostentosos ropajes que no parecen hacerla sentir muy cómoda y una enorme corona que "
        "fue creada para ella, para que todos la reconozcan como la legítima heredera de Elder I, "
        "el anterior rey de Takome. "
        "Su cuerpo esbelto delata su juventud, así como sus cabellos rubios rizados que brotan desde "
        "su corona hasta caer y reposar en sus hombros. "
        "Es menuda y de complexión atlética, sus enormes ojos color almendra reflejan el furor "
        "interior de una mujer decidida a encontrarse con su destino, algo natural en las devotas "
        "hermanas de Eralie de los conventos de donde ella proviene, pues es una sacerdotisa "
        "aventajada de una gran carisma y sabiduría. "
        "Está agobiada y atareada con el nuevo cargo al que acaba de jurar, pero por Eralie que "
        "no parece que vaya a darse por vencida.\n");
    
    fijar_religion("eralie");
    fijar_raza("humano");
    fijar_clase("eralie");
    fijar_ciudadania("takome",999);
    fijar_alineamiento(-1000000);
    fijar_genero(2);

    set_random_stats(15,17);
    fijar_sab(25);
    fijar_car(32);    
    fijar_nivel(41+random(6));
    fijar_pvs_max(dame_pvs_max()*13);
    fijar_pe_max(dame_pe_max()*10);
    fijar_fe(220);

    add_loved("ciudadania",({"takome"}));
    add_hated("relacion",({"takome"}));
    add_hated("enemigo",({"Takome"}));

    set_aggressive(6);
    
    load_chat(30,({
        "'No defraudaré al pueblo de Takome.",
        "'¿Cual será la próxima audiencia?",
        "'Llevar un pueblo es una gran responsabilidad, pero por Eralie que aguantaré hasta el final.",
        "'El pueblo aún no confia mucho en mi, pero no les culpo. La muerte de Elder fue un duro golpe "
        "para todos. Prometo no defraudarles y seguir la sombra de nuestro gran rey.",
        "'Yo también fui gobernada por Elder. Yo también lo echo de menos. El pueblo se cree que nada "
        "tengo en común con ellos, pero mi empatía con su dolor por la muerte de Elder es un gran lazo.",
        "'Reconstruiré la esperanza de este pueblo.",
        "'¿Alguien más necesita una audiencia?",
        ":respira con dificultad debido al apretado vestido.",
        ":te mira con expresión inmutable, pero con ojos bondadosos."}));
    load_a_chat(30,({
            "'¡Por Takome que defenderé este trono hasta la muerte!",
            "'¡Nuestra vida y tradición están en este trono!, ¡Por Takome que no podrás pasar!",}));

    nuevo_lenguaje("adurn",100);
    //nuevo_lenguaje("elfico",100);    
    //nuevo_lenguaje("khadum",100);
    //nuevo_lenguaje("gnomo",100);
    fijar_lenguaje_actual("adurn"); quitar_lenguaje("dendrita");

    add_clone("/d/takome/armaduras/corona_bien.c",1);
    add_clone("/d/takome/armaduras/collar_zafiro.c",1);
    add_clone("/d/takome/armaduras/guante_priis.c",1);
    add_clone("/d/takome/armaduras/guante_priis_izq.c",1);
    add_clone("/d/takome/armaduras/vestido_real.c",1);
    add_clone("/d/takome/armaduras/zapatos_reina.c",1);
    add_clone("/baseobs/armaduras/collar.c",1);
    add_clone("/baseobs/sagrados/simbolo_eralie.c",1);

    fijar_objetos_morir(({
        "/d/takome/armaduras/corona_bien.c",
        "/d/takome/armaduras/collar_zafiro.c",
        "/d/takome/armaduras/guante_priis.c",
        "/d/takome/armaduras/guante_priis_izq.c",
        "/d/takome/armaduras/vestido_real.c",
        "/d/takome/armaduras/zapatos_reina.c",
    }));
    fijar_objetos_morir(({"tesoro", "armadura", ({8, 9}) }) );
    fijar_objetos_morir(({"tesoro", "pergamin", ({8, 9}) }) );

    
    
    nuevo_dialogo("nombre",({
        ":me inclina la cabeza y te mira con desaprobación.",
        "'Soy Priis, la actual monarca de la fortaleza de Takome.",
        "'¿Y quién sois vos que visitais un reino sin conocer a sus mandatarios?"}));
    nuevo_dialogo("audiencia",({
                ":te sonrie y mueve la mano, haciéndote gestos para que detengas tu protocolaria cortesía.",
        "'Por favor, no es necesario pedir audiencia, siempre estoy dispuesta a escuchar a cualquier "
        "persona de buen corazón.",
        "'¿Qué es lo que os trae por aquí?, ¿que puede hacer la Reina de Takome por vos?"}));    
    // Info sobre la forma de gobierno
    nuevo_dialogo("monarca",({
        "'Ante la repentina muerte de Elder I...",
        ":hace rápidamente el símbolo de Eralie sobre el pecho.",
        "'...las familias nobles han formado una nueva monarquía conmigo a la cabeza recientemente, siguiendo todo "
        "el procedimiento de nuestras tradiciones."}),"nombre");
    nuevo_dialogo("procedimiento",({
        "'Cada Rey, o reina como es mi caso, ha de nombrar un heredero a lo largo de su mandato que será el que tome el trono "
        "cuando él -o ella- muera o abdique.",
        "'En el caso de Elder, no había ningún heredero nombrado, por lo que las familias se reunieron en una asamblea y me eligieron "
        " a mi."}),
        "monarca"
    );
    nuevo_dialogo("asamblea", ({
        "'En circunstancias normales el sufragio sería universal y no solo nobiliario, pero la tradición dice:",
        "'\"hubiese de morir el monarca bajo circunstancias adversas y sin un sucesor nombrado, las familias que él en vida hubiese nombrado como nobles se "    
        "encargarán de elegir el nuevo heredero/a, pues su vinculación con el bienestar económico-político del reino es mayor que el "
        "de cualquier otro takomita\".",
        "'La elección termino hace relativamente poco y fui yo la elegida.",
                ":te hace una escueta reverencia.",
        "'Estoy aquí para servir al pueblo y a todos los hijos de Eralie."
    }),"procedimiento");
    nuevo_dialogo("eralie",({
        "'He sido criada en una hermandad de monjas de Eralie, por lo que tendré muy presente el aspecto religioso en las decisiones del "
        "día a día.",
        "'Me ceñiré a sus enseñanzas y, con ayuda de la Santa Cruzada, gobernaremos Takome de una manera que el mismo Eralie aprobaría."}),
        "asamblea");
    // Info sobre el reino
    nuevo_dialogo("reino",({
        ":te mira absolutamente extrañada.",
        "'¿Provenís de tierras lejanas, no es así?",
        "'Estais en el reino de Takome, el bastión inexpugnable del bien que nuestros ancestros han " 
        "defendido con su sangre y la de sus hijos durante eras.",
        "'Somos los herederos de la voluntad de Eralie y es nuestro menester el de liberar al mal del "
        "mundo, así como proteger a los inocentes.",
        "'Yo soy la reina de Takome, Priis, de la familia noble Blochessrf, del cuarto convento de la "
        "hermandad del corazón puro. Encantada de conoceros."}),"nombre");
    nuevo_dialogo("ancestros",({
        "'Sólo en el génesis del mundo éste ha estado limpio de todo mal.",
        "'Cuando la primera criatura de la oscuridad pisó el mundo ésta se propagó por doquier, así como "
        "sus hijos y los hijos de sus hijos.",
        "'Tal ha sido su influencia que aún a día de hoy estas criaturas moran en la tierra, conspirando "
        "de mil maneras para destruir el mundo que tanto trabajo le costo crear a los antiguos dioses.",
        "'La muerte de Elder ha sido obra suya en un intento de debilitar Takome moralmente, ¡pero "
        "resolución no ha hecho otra cosa que crecer!"}),"reino");
    // Hermandad
    nuevo_dialogo("hermandad",({
        "'Sí. En el pasado pertenecí a una hermandad de Eralie.",
        "'Desde muy joven fui instruida en las artes eclesiárquicas y aunque aún tengo votos por cumplir,"
        "gozo del nivel de adepta en la hermandad.",
        "'He sido instruída en teología, filosofía, astrología, música, historia y, como no, en magia "
        "clerical.",
        ":sonrie.",
        "'He tenido esa suerte por petenecer a una familia noble, pero confío en que bajo mi mandato "
        "la economía pueda crecer lo suficiente como para que todos en Takome tengamos exactamente "
        "las mismas oportunidades sin que el dinero o la influencia sea una variable.",
        "'En un mundo sin Mal, es posible, sin embargo, aún queda mucho trabajo por hacer."}),"reino");
    nuevo_dialogo("influencia",({
        ":se pone seria durante unos instantes.",
        "'No son pocos los que me critican, alegando que fue mi influencia y no mis aptitudes las que "
        "han hecho que me elijan.",
        "'Eso es una burda mentira, fruto de la envidia o la incomprensión... y espero que sea lo "
        "segundo, ya que la envidia es una vía muy rápida para caer en los zarcillos del mal.",
        ":sonrie de nuevo.",
        "'Pero la opinión del pueblo sobre mí no es algo que me importe actualmente, pronto verán "
        "que no busco sino su bienestar, el honor a la tradición y linaje Takomita y honrar a Elder "
        "como se merece."}),"hermandad");
    // Elder y la diadema
    nuevo_dialogo("elder",({
        ":hace rápidamente el signo de Eralie con las manos.",
        "'Una funesta desgracia, sin duda.",
        "'La muerte de Elder ha sido el golpe más duro por el que los Takomitas hemos tenido que pasar, "
        "más incluso que el cataclismo.",
        "'Su bondad y buen juicio devolvieron a Takome su prosperidad y aunque algunas ciudadanías, "
        "como Kheleb o Poldarn, tenían sus diferencias con nosotros, no entendían que todo roce "
        "era debido a una extrema, y sabia si me permite decir, cautela.",
        "'Tras la muerte de Elder, la suma cruzada Kathleen ceremonió un rito de funeral en el "
        "que se honro su nombre y su legado.",
        ":parece desconsolada.",
        "'Sin embargo, y aunque la venganza es un agujero sin salida, no hemos sido capaces de ajusticiar"
        " la crueldad del asesino de Elder.",
        "'Tampoco hemos sido capaces de encontrarle, la única pista que teníamos se reducia a monedas "
        "de Jinny, fruto sin duda de algún soborno que el cauteloso asesino tuvo que realizar para "
        "entrar en el salón del trono tan facilmente.",    
        ":me reprime una lágrima silenciosa.",
        "'No debemos promulgar la muerte del inocente, nisiqueira la muerte, pero esta es una ocasión "
        "distinta. Ni el mismísimo Eralie perdonaría el pecado contra la vida que ese misterioso asesino "
        "ha realizado. Desconocemos los intereses que lo movían, pero nada puede excusar un acto "
        "de barbarie como este.",
        "'Además, la diadema de Elder se ha perdido con su cabeza y, con ello, muchas de nuestras tradiciones.",
        "'Ha sido un duro golpe para nosotros y deberíamos luchar por terminar con la vida de su asesino y recuperar "
        "lo que es nuestro, ¡sólo así recuperaremos nuestro orgullo como Takomitas!",
        "'¡Aquél que me entregue la diadema que portaba Elder como prueba de haber "
        "encontrado y matado a su asesino será honrado con toda la gloria que pueda otorgarle!",
        "'Ahora, si no os importa, preferiría estar sola. Hablar de Elder me ha entristecido y he de"
        " buscar consuelo en la oración.",
        // damos el hito
        "hito takome_corona_bien encontrar_asesino"}),"influencia");
    nuevo_dialogo("corona del primer monarca", ({
        ":enarca una ceja.",
        "'La corona del primer monarca no es más que la diadema que ha pasado "
            "de generación en generación entre los reyes de Takome.",
        "'Antiguamente pertenecia al rey Elder I, pero con su muerte, el asesino "
            "se la llevó.",
        "'Si encuentras al asesino, encontrarás a la corona.",
        "hito takome_anarcam info_corona"
    }));
    prohibir("corona del primer monarca", "hito takome_anarcam buscar_corona");
    
    // Entregamos la diadema    
    nuevo_dialogo("entregar",({"entregar_diadema"}));
    // En caso de no recibir la corona
    nuevo_dialogo("corona",({"coronar_jugador"}));
    
    //Prohibir.. No le dejamos entregar la diadema si antes no le hemos dado el hito para buscarla o...    
    prohibir("entregar","hito takome_corona_bien encontrar_asesino");
    // ... o si ya hemos completado la mision
    deshabilitar("entregar","hito takome_corona_bien corona_recibida");
    // No se puede conversar sobre corona si no se ha entregado la diadema
    prohibir ("corona","hito takome_corona_bien diadema_entregada");
    
    habilitar_conversacion();
    init_equip();
    set_heart_beat(1);
}

int entregar_diadema(object jugador)
{
    object diadema,corona;
    if (dame_peleando())
        return 0;

    if (-1!=member_array(jugador,({query_attacker_list()+query_call_outed()})))
        return 0;

    if (jugador->query_hidden())
        return 0;
    
    if(environment(jugador)!=environment())
        return 0;
    
    if(!diadema = secure_present(ARMADURAS+"diadema_dorada2.c",jugador))
    {
        do_say("Tu falta de sensibilidad ante este tema tan serio me sorprende "+jugador->query_short()+".\nNo "
            "voy a perder más el tiempo si no tienes la diadema que tan desesperadamente "
            "busca el pueblo de Takome.",0);
        return 0;
    }
    
    if (!corona=secure_present(ARMADURAS+"corona_bien.c",this_object()))
    {
        do_say("Lo lamento, "+jugador->query_short()+", alguien ya me ha entregado la diadema de Elder.",0);
        do_say("Sospecho que lo que me entregas es una imitación.",0);
        return 0;    
    }
    
    if(jugador->nuevo_hito_mision("takome_corona_bien","diadema_entregada",0))
    {
        diadema->dest_me();
    
        if (jugador->dame_estatus("Takome")<800)
            jugador->ajustar_estatus("Takome", 300);
        else
            jugador->ajustar_estatus("Takome", 50 + random(25));

        if (jugador->dame_religion()=="eralie")
            jugador->ajustar_fe(10, 1);

        jugador->ajustar_alineamiento(-6000);
        jugador->ajustar_xp(100000+random(100000));

        do_emote("estalla en lágrimas de emoción.");
        do_say("¡No me lo puedo creer, "+jugador->dame_nombre_completo()+"!, ¡has conseguido lo que muchos daban "
            "por imposible!, ¡has encontrado y ajusticiado al asesino de Elder, nos has devuelto su diadema y has "
            "devuelto el honor a la tradición Takomita que se había perdido con su asesinato!, ¡tu acción no pasará "
            "desapercibida, te lo aseguro!, ¡el propio corazón de Eralie se ha conmovido este día!",0);
    
        do_shout("¡¡Tres hurras por "+jugador->dame_nombre_completo()+", que en este día ha conseguido devolvernos "
        "las respuestas y las tradiciones que nos habían sido arrebatadas!!, ¡¡por Takome y por Elder, que su "
        "nombre sea recordado por siempre!!");
    
        write_file(LOGS+"priis",ctime()+" "+jugador->query_cap_name()+" ha entregado la diadema.\n");
        debug(jugador->query_cap_name()+" ha entregado la diadema.\n");
    
        call_out("coronar_jugador",1,jugador);
        return 1;
    }
}

void coronar_jugador(object jugador)
{
    object corona,aux;
    
    if(environment(jugador)!=environment())
        return;
    
    // Si hemos entrado aqui y ya hemos completado la misión, pues nos saca.
    if(jugador->dame_mision_completada("takome_corona_bien"))
    {
        do_say("¡El pueblo de Takome te estará eternamente agradecido!",0);
        return;
    }
    
    if (!corona=jugador->entregar_objeto(ARMADURAS+"corona_bien.c"))
    {
        // En caso de que pase esto, existe el tema de conversación "corona"
        do_say("Estoy tan emocionada que hasta te entregaría mi corona de no ser porque la están "
            "restaurando ahora mismo.",0);
        return;
    }
    
    if(jugador->nuevo_hito_mision("takome_corona_bien","corona_recibida",0))
    {
        do_say("¡¡Toma, "+jugador->query_short()+"!!, ¡te cedo mi corona como muestra de gratitud!",0);
        aux=jugador->dame_objetos_equipados()["cabeza"][0];
        if (aux!=0)    jugador->unwear_ob(aux);
        tell_object(jugador,query_short()+" se levanta de su trono y se quita su corona, poniéndotela a ti "
            "en la cabeza.\n");
        jugador->wear_ob(corona);
    
        write_file(LOGS+"priis",ctime()+" "+jugador->query_cap_name()+" ha recibido la corona.\n");
        debug(jugador->query_cap_name()+" ha recibido la corona.\n");
    }
}

int lanza_hechizo(string spell,mixed tg)
{
    if (!tg||query_timed_property("bloqueo-"+capitalize(spell)))
        return 0;

    habilidad(lower_case(spell),tg);
}

int porcentaje_vida(mixed b)    
{ 
    int media=0;
    if (!arrayp(b)&&objectp(b))    return ((b->dame_pvs()*100)/b->dame_pvs_max()); 

    foreach(object ob in b)
        media+=porcentaje_vida(ob);        

    return media/=sizeof(b);
}

void curar(object b)
{
    int p=porcentaje_vida(b);

    if (b==this_object()&&p<30&&!query_timed_property("no_flood"))
    {
        do_say("¡Mis fieles guardianes!, ¡vuestra reina necesita vuestra ayuda!",0);
        add_timed_property("no_flood",1,20);
    }
    
    switch(p)
    {
        case 0..20:    if (lanza_hechizo("curacion",b))        return;
        case 21..40:    if (lanza_hechizo("curar heridas criticas",b))    return;
        case 41..60:    if (lanza_hechizo("curar heridas serias",b))    return;
        case 61..80:    if (lanza_hechizo("curar heridas moderadas",b))    return;
        case 81..99:    if (lanza_hechizo("curar heridas ligeras",b))    return;
    }
}

int do_death(object p)
{
    if (p)    
        p->fijar_estatus("Takome",-999);
    
    write_file(LOGS+"priis",ctime()+" ha muerto a manos de "+(!p?"alguien":p->query_cap_name())+".\n");
    debug("¡He muerto a manos de "+(!p?"alguien":p->query_cap_name())+".");
    return ::do_death(p);
}

int es_amigo(object b)
{
    if (!b||
        !living(b)||
        -1!=member_array(b,query_call_outed()+query_attacker_list()))    
        return 0;

    return (
        real_filename(b)==NPCS+"cruzado_real"||
        b->dame_ciudadania()=="takome"||
        b->dame_religion()=="eralie"||
        (b->dame_ciudadania()=="ateo"&&b->dame_alineamiento()<-1000));
}

void comprobar_seguratas()
{
    string ruta   = NPCS+"cruzado_real.c";
    object *todos = children(ruta) - ({load_object(ruta)});
    object protector;

    if (!(protector = dame_protector()) || environment() != environment(protector)) {
        fijar_protector(0);

        foreach(object ob in todos) {
            if (environment(ob) != environment()) {
                tell_room(environment(ob),
                    ob->query_short() + " se va de la sala para proteger a su reina.\n"
                );
                ob->move(environment());
                fijar_protector(ob);
                tell_room(
                    environment(),
                    ob->query_short() + " llega a la sala para proteger a su reina.\n"
                );
            }
        }
    } 
}

void heart_beat()
{
    object *amigos,*aux,menor;
    function tipo_clase=function(object b,string tipo)
    {
        if (!tipo)    tipo="luchador";
        return (-1!=member_array(tipo,b->dame_tipo_clase()));
    };

    this_object()->destruir_enmudecer();
    ::heart_beat();

    comprobar_seguratas();

    if (environment()->query_silencio())
    {
        do_move_after();
        return;
    }

    if (query_timed_property("no-skills"))    return;
    
    if (amigos=filter(all_inventory(environment()),"es_amigo"))
    {
        // Chequeo para el cántaro
        if (porcentaje_vida(amigos)<20&&sizeof(amigos)>2)
        {
            if (secure_present(SIMBOLO,this_object()))
            {
                lanza_hechizo("transformar simbolo","");
                add_timed_property("no_skills",1,14);
                return;
            }
            else if (secure_present("/hechizos/items/simbolo_eralie_ob.c",this_object()))
            {
                init_equip();
                queue_action("verter cantaro");
            }
        }

        foreach(object ob in amigos)
        {
            if (!menor)    
            {
                menor=ob;
                continue;
            }
            if (porcentaje_vida(ob)<porcentaje_vida(menor))    menor=ob;
        }    
        if (menor&&(menor->dame_pvs_max()-menor->dame_pvs())>600)
        {
            curar(menor);
            return;
        }
    }

    if (sizeof(aux=query_attacker_list()))
    {
        if(sizeof(amigos=filter(aux,(:call_other:),"dame_invocacion"))&&lanza_hechizo("repudiar",amigos[0]))
            return;

        if(sizeof(amigos=filter(aux,tipo_clase,"sacerdote"))&&lanza_hechizo("enmudecer",amigos[0]))
            return;
        
        if (sizeof(amigos=filter(aux,tipo_clase,"hechicero")))
        {
            if (amigos[0]->dame_invulnerable_hechizos()>0)
            {
                if (lanza_hechizo("disipar magia",amigos[0]->query_name()+" globo"))    return;
                if (lanza_hechizo("enmudecer",amigos[0]))                return;
            }
            else     if (lanza_hechizo("disipar magia",amigos[0]))                return;
        }
        
        if (sizeof(amigos=filter(aux,tipo_clase,"luchador")))
        {
            if (lanza_hechizo("retener persona",amigos[0]))    return;
            if (lanza_hechizo("ceguera",amigos[0]))        return;
        }

        if (sizeof(aux)>4&&lanza_hechizo("palabra divina",""))    return;
        lanza_hechizo("tormenta sagrada","");    
    }
}
