//Durmok 11/04/2003
//Snaider 30-XI-2006
//Plan de remodelación de Kattak y Kheleb. Añado guardias con más vida y con 
//avisos a cada una de las ciudades
//Eckol 15Abr15: Quito el attack_by, ahora los avisos estan en la base de guardia.

#include <mxp.h>
#include "/d/kheleb/path.h"
inherit "/obj/guardia";

void setup()
  {
    string vocal;
    if(!random(5)) fijar_genero(2);
    
    vocal=dame_vocal();
    set_name("guardia");
    add_alias(({"enano","guardia","kheleb","dum","guardian","soldado"}));
    add_plural(({"enanos","guardias","kheleb","dum","guardianes","soldados"}));
    //set_short(FONT("Angerthas",1,"Guardia de Kheleb Dum"));
    set_short("Guardia de Kheleb Dum");
    set_main_plural("Guardias de Kheleb Dum");
    set_long(capitalize(dame_numeral())+" "+({"grues"+vocal,"fornid"+vocal,"musculos"+vocal})[random(3)]+
      " enan"+vocal+" cuya misión es la de vigilar y proteger los lugares estratégicos de "
      "la ciudad enana de Kheleb Dum, tales como puertas e incluso algún yacimiento mineral "
      "importante.\n");
    fijar_raza("enano");
    fijar_bando("bueno");
    fijar_religion("eralie");
    fijar_fe(50);
    fijar_clase("khazad");
    fijar_ciudadania("kheleb",150);

    fijar_alineamiento(-5000-(random(10000)));

    fijar_fue(19);
    fijar_carac("des",16+random(3));
    fijar_carac("con",10+random(9));
    fijar_carac("int",12+random(7));
    fijar_carac("sab",10+random(9));
    fijar_carac("car",10+random(8));
    fijar_nivel(40+random(16));

    add_attack_spell(35,"furia",1);
    add_attack_spell(75,"embestir",3);
    
#include "/d/kheleb/include/agresividad.h"




    load_chat(20,
      ({
	"'Las minas antiguas son un lugar muy peligroso",
	":observa a su alrededor.",
	"'Mi misión es la de guardar este lugar con mi vida.",
	"'Daría mi vida por Darin.",
	"'Intenta no provocar altercados.",
	"'Kattak es nuestra ciudad protegida, allí vendemos nuestros materiales forjados.",
      }));

    add_clone(BARMADURAS+"placas_enana",1);
    add_clone(BARMADURAS+"yelmo",1);
    add_clone(BARMADURAS+"pantalones",1);
    add_clone(BARMADURAS+"botas",1);
    switch (random(4))
    {
    case 0: add_clone(BARMAS+"martillo_de_guerra",1);break;
    case 1: add_clone(BARMAS+"maza_a_dos_manos",1); break;
    case 2: add_clone(BARMAS+"hacha_de_batalla",2);break;
    case 3: add_clone(BARMAS+"hacha_a_dos_manos",1);break;
    }


    init_equip();
    //Se multiplicará x2 por heredar de guardia
    fijar_pvs_max(10000);
	
	fijar_opcion("bloqueo_local", 300);
}
 /*
void attack_by(object atacante)
{
    DEFENSA->aviso_asedio(TO);
    atacante->add_timed_property("perseguido_kd", 1, 360);
    ::attack_by(atacante);
}
*/
string dame_puerta() { return "Kheleb Dum"; }
