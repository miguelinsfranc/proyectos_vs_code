// Satyr 2k7
//Kaitaka oct 2011, para que no pete el nombre al morir
inherit "/obj/conversar_npc.c";
#define AGRUPARSE(x,y) ("/cmds/mortal/agruparse.c"->agrupar_npc(x,y))
status dame_heroismo()
{
    return 1;
}
status dame_poder_divino()
{
    return 1;
}
void setup()
{
	object b;
	fijar_genero(random(2)+1);

	set_long(
		"\tLos Cruzados Reales son el máximo exponente de la determinación y la valía en la escala de la gran Cruzada de Eralie. "
		"Estos individuos, que han sacrificado cualquier interés personal para defender Takome, la Cruzada y a sus altos mandatarios "
		"han sido entrenados en las artes de la benevolencia, el combate, la retórica y la predicación.\n");

    {   // Esto es para que priis pueda curarlos inequívocamente
        int id; 
        
        if (b = load_object("/handlers/generador_ids.c")) {
            b->nuevo_dominio_temporal("takome-cruzado_real");
            id = b->dame_id_temporal("takome-cruzado_real");
        }
        else {
            id = time();
        }
        
        set_name("cruzado_real_" + id);     
    }
	
	add_alias(({"cruzado","real","cruzado real"}));
	add_plural(({"cruzados","reales","cruzados reales"}));

	if (dame_genero()==1)
	{		
		set_short("Cruzado Real");
		set_long(query_long()+
			"El que tienes ante ti se trata de un varón de fuerte complexión y rasgos afilados que parece contemplar "
			"con una calma absoluta todo lo que sucede a su alrededor. "
			"Sus cabellos y ojos son invisibles a través del enorme yelmo que viste y su grueso cuello delata la fuerte "
			"complexión necesaria para pertencer al cuerpo de élite de la cruzada. "
			"Sus manos enguantadas se apoyan en la empuñadura de su arma, mientras su antebrazo izquierdo muestra "
			"orgulloso el máximo símbolo de Eralie al que un caballero puede ostentar.\n");
	}
	else
	{
		add_alias(({"cruzada"}));
		set_short("Cruzada Real");
		set_long(query_long()+
			"La que ahora observas es una fibrosa mujer de cabellos plateados que reposan en sus hombros, atravesando el resistente "
			"yelmo grande que oculta su rostro. "
			"Vigila y atiende las necesidades de su protegida -así como de cualquiera de sus alrededores- con una calma estoica "
			"y no pierde de vista a cualquier persona que entra en la sala de la reina.\n");
	}
	
	set_long(query_long()+"La dedicación de estos individuos es digna de ser narrada. Han desarrollado unas habilidades de combate "
			"casi perfectas, su dominio de teología y magia clerical es envidiable y se han ganado una fuerte reputación y honor "
			"en su ciudad gracias a estos notables dones. Son apreciados y elogiados como héroes, pues se lo han ganado "
			"a base de una vida de sacrificios y rectitud.\n");

	set_main_plural("Cruzados Reales");	
	fijar_religion("eralie");
	fijar_raza("humano");
	fijar_clase("paladin");
	fijar_ciudadania("takome",650);
	fijar_alineamiento(-100000);
	fijar_fe(200);

	fijar_fue(18);
	fijar_extrema(60+random(21));
	fijar_des(14);
	fijar_con(18);
	fijar_int(12);
	fijar_sab(17);
	fijar_car(21);
	fijar_nivel(42+random(10));
	fijar_pvs_max(dame_pvs_max()*10);
	fijar_pe_max(dame_pe_max()*10);
	ajustar_maestria("espada larga",60+random(41));
	add_loved("ciudadania",({"takome"}));
	add_hated("relacion",({"takome"}));
	add_hated("enemigo",({"Takome"}));
	set_aggressive(6);
	
	add_attack_spell(70,"cargar",3);
	add_attack_spell(100,"heroismo",1);

	quitar_lenguaje("dendrita");
	nuevo_lenguaje("adurn",100);
	nuevo_lenguaje("elfico",60);
	nuevo_lenguaje("khadum",30);
	fijar_lenguaje_actual("adurn");

	add_clone("/d/takome/armas/espada_takome.c",1);
	if (b=clone_object("/hechizos/items/simbolo_paladin_ob.c"))
	{
		b->move(this_object());
		b->setup_simbolo(this_object());
	}
	else
		add_clone("/baseobs/escudos/escudo_corporal.c",1);
		
	add_clone("/baseobs/armaduras/gran_yelmo.c",1);
	add_clone("/baseobs/armaduras/vaina.c",1);
	add_clone("/baseobs/armaduras/completa.c",1);
	add_clone("/baseobs/armaduras/botas_guerra.c",1);
	add_clone("/baseobs/armaduras/grebas_metalicas.c",1);
	add_clone("/baseobs/sagrados/simbolo_eralie.c",1);
	add_clone("/baseobs/armaduras/guantelete.c",1);
	add_clone("/baseobs/armaduras/guantelete_izq.c",1);
	init_equip();
	set_heart_beat(1);
	
	nuevo_dialogo("alcazar",({
		"'Sí, eso es lo que he dicho, el alcazar real.",
		"'Aquí es donde su excelencia, la Reina Priis, gobierna con determinación y gentileza "
		"el pueblo de Takome.",
		"'¿Y vos?, ¿teneis una audiencia?, ¡no podeis estar aquí si no la teneis!"}));
	nuevo_dialogo("audiencia",({
		":hace una breve reverencia.",
		"'Lamento lo sucedido, más el protocolo exige que todo el que quiera dirigirse a su majestad "
		"tenga que hacerlo con una previa audiencia.",
		"'En este caso, podeis hablar con ella directamente, ya que en su infinita benevolencia "
		"os lo ha permitido.",
		"'Pero sabed que os vigilamos, ¡no le hableis de manera soez u os vereis con nosotros!",
		}),"alcazar");
	nuevo_dialogo("nosotros",({
		"'Nosotros, la guardia real.",
		"'Somos miembros de una de la familias nobles cuyo sucesor no ha sido elegido como monarca "
		"y como demostración de que no hay ningún tipo de rencor, ¡nos hemos mostrado voluntarios "
		"para defender su mandato con nuestras vidas!"}),"audiencia");
	nuevo_dialogo("familias",({
		"'Pertenecemos al linaje Sadul, somos los guardaespaldas de su santidad y nos hemos ofrecido "
		"a defender el mandato de la Reina Priis, de la casa Blochessrf."}),"nosotros");
	nuevo_dialogo("real",({
		"'Somos cruzados reales, miembros de élite entrenados para defender los intereses de la "
		"monarquía Takomita y de todos sus conciudadanos.",
		"'Hemos entrenado mucho para recibir tal honor y no pensamos defraudar la confianza "
		"que ha sido depositada en nosotros.",
		"'Por Elder, Takome y nuestra nueva monarca, juro que no dejaré de defender esta ciudad "
		"y a su nueva reina mientras me quede una sóla gota de sangre en el cuerpo."}),"familias");
	
	habilitar_conversacion();
}

int frases_personalizables(string tipo_frase,object npc,object pj)
{
	switch(tipo_frase)
	{
		case "despedida-npc":
			do_say("Hasta la vista, "+pj->dame_nombre_completo()+". Que Eralie vaya contigo.",0);
			break;
		case "noentiende-npc":
			do_say("¿Perdón?, lo lamento "+pj->dame_nombre_completo()+". No tengo conocimientos "
			"de la lengua que practicais.",0);
			break;
		case "bienvenida-pj":
			tell_object(pj,query_short()+" se adelanta hacia ti y te observa con mirada "
			"inexpresiva.\n");
			break;
		case "bienvenida-npc":
			do_say("Saludos, "+pj->dame_nombre_completo()+".",0);
			do_say("¿Qué es lo que deseas y que menesteres te atraen al alcazar real?",0);
			break;
		default: return 0;
	}
	return 1;	
}

void chequea_mensaje(string msj)
{
	string tx;
	sscanf(msj,"<< %s >>",msj);
	switch(string_sencillo(msj))
	{
		case "No defraudaré al pueblo de Takome.":
			tx="Mi Reina; ¡no debeis arropar el temor en vuestro corazón! "
			"El pueblo aprenderá a amaros como lo hacemos nosotros.";
			break;				
		case "¿Cual será la próxima audiencia?":
			do_emote("comprueba una repleta libreta de notas.");
			switch(random(5))
			{
				case 0:	tx="Temo que una aburrida sesión de economía con la alta nobiliaria, "
					"Mi Señora.";	break;
				case 1:	tx="Lord Nardiel en persona dijo que vendría a veros. Aunque parece "
					"que llega tarde..."; break;
				case 2: tx="Un emisario de Veleiron que viene a cerrar algún tratado comercial, "
					"Mi Señora. Aunque creo que ya ha sido atentido por la nobiliaria.";
					break;
				case 3:	tx="Sorprendentemente, No hay ninguna audiencia preparada, alteza."; break;
				default:tx="Un heraldo de Anduar que quiere sellar algún tratado mercantil, "
					"excelencia."; break;
			}	
			break;
		case "Reconstruiré la esperanza de este pueblo.":
			tx="¡Y puede contar conmigo para hacerlo, excelencia!, ¡estaré a vuestro lado "
			"por siempre!";
			break;
		case "Ante la repentina muerte de Elder I...":
			do_emote("hace el símbolo de Eralie sobre el pecho y se arodilla en el suelo.");
			break;
		case "Una funesta desgracia, sin duda.":
			do_emote("Asiente con la cabeza, apenad"+dame_vocal()+".");
			break;
		case "Ahora, si no os importa, preferiría estar sola. Hablar de Elder me ha entristecido "
	             "y he de buscar consuelo en la oración.":
			tx="Si así lo necesitais, Mi Señora, puedo hacer que cierren durante unas horas "
			"el alcazar real para que os dejen a solas con vuestros pensamientos.";
		break;
		case "Mi fiel guardian, no os preocupeis. Atenderé a todo el que de buen corazón "
		     "acuda a este alcazar. No teneis que mostraros tan intolerante.":
			tx="Mi Señora, vuestra benevolencia por atender a todo el mundo ignorando el protocolo "
			"es digna de elogio y de ser narrada en incontables canciones sobre vos.";
			break;
		case "Lo sé, mi noble protector. Habeis seguido bien el camino de Eralie, es todo "
		"un honor para mi gozar de tan bravos guardianes.":
			do_emote("se apresura a arrodillarse ante su reina.");
			tx="Vuestras palabras me llenan de orgullo, Mi Señora. Cualquiera en mi situación "
			"daría la vida por defenderos, no es necesario que arrojeis sobre mi tales honores.";
			break;
		case "¡Mis fieles guardianes!, ¡vuestra reina necesita vuestra ayuda!":
			if (!query_timed_property("sanando"))
			{
				tx="¡Yo os defenderé mi señora!, ¡aguantad un poco más!";
				add_timed_property("sanando",1,60);
				habilidad("sanar",secure_present("/d/takome/npcs/priis.c",environment()));
			}
			break;
	}
	do_say(tx,0);
}
void event_person_say(object quien,string encabezado,string msj,string lenguaje,string *msjs)
{
	if (real_filename(quien)=="/d/takome/npcs/priis"
	    &&quien->query_property("portavoz")
	    &&quien->query_property("portavoz")==this_object())
		call_out("chequea_mensaje",1,msj);
}

void heart_beat()
{
	object b;
	::heart_beat();

	if (b=secure_present("/d/takome/npcs/priis.c",environment()))
	{
		if (!b->dame_protector())
		{
			tell_accion(environment(this_object()),query_short()+" protege a "
			 +b->query_short()+".\n","",({this_object()}),this_object());
			b->fijar_protector(this_object());
	   	}
		
		if (!dame_grupo_seguir())
			AGRUPARSE(b,this_object());	

		if (!b->query_property("portavoz")||
		   (b->query_property("portavoz")&&environment(b->query_property("portavoz"))!=environment(b)))
			b->add_property("portavoz",this_object());	
	}

	if (!sizeof(query_weapons_wielded()))
		habilidad("hoja de llamas",this_object());
}
void event_enter(object quien_entra,string mensaje,object procedencia,object *seguidores)
{
    ::event_enter(quien_entra, mensaje, procedencia, seguidores);
    
    if (quien_entra && -1 != member_array(quien_entra, query_attacker_list() + query_call_outed())) {
        quien_entra->consumir_hb();
    }
}

int do_death(object asesino)
{
    TO->set_name("cruzado real");
    return ::do_death(asesino);
}
