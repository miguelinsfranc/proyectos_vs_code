/*
        Durmok 28-11-03
        Darin version 1.0
*/
//Snaider II-2007: Incluyo conversaciones para ambientar los cambios en el reino
//Satyr 2k9 -- Quest para Khazads y Herreros Enanos
//Kaitaka 5Nov2012 -> quest de la vaina de Telchar
// Isham 15/12/2014 - Añadidos tres tesoros aleatorios entre nivel 5 y nivel 9 de tipo arma o armadura.
// Satyr 19.12.2015 -- unarmed_attack con objects
#include "/d/kheleb/path.h"
inherit "/obj/conversar_npc.c";
#define HD "/grupos/ciudadanias/kheleb.c"
int veces;
void setup() {
    set_name("darin");
    add_alias(({"darin","enano","rey"}));
    add_plural(({"enanos","reyes"}));
    set_short("Darin, Rey de Kheleb Dum");
    set_main_plural("Reyes de Kheleb Dum");
    set_long("Darin es el fornido rey de Kheleb Dum. El sabio rey enano "
      "en el que los miembros de su raza ven la imagen de su padre Durín, "
      "uno de los enanos más sabios y justos de la Segunda Edad de Eirea. Tiene "
      "un tamaño descomunal para tratarse de un enano, como si estuviera "
      "esculpido en roca. Posee una larga y frondosa barba de color rojo "
      "intenso, y un largo y greñoso cabello del mismo color repleto de "
      "trenzas. Su único ojo te observa con mirada de sabiduría, justicia "
      "y valor.\n"
    );

    fijar_bando("bueno");
    fijar_religion("eralie");
    fijar_raza("enano");
    fijar_clase("soldado");
    fijar_especializacion("khazad");
    fijar_ciudadania("kheleb",999);

    fijar_alineamiento(-1000);
    fijar_genero(1);
    fijar_fe(220);

    fijar_carac("fue",25);
    fijar_extrema(100);
    fijar_carac("des",19);
    fijar_carac("con",28);
    fijar_carac("int",15);
    fijar_carac("sab",20);
    fijar_carac("car",19);

    nuevo_lenguaje("adurn",100);
    nuevo_lenguaje("elfico",20);

#include AGRESIVO

    set_aggressive(6,0);
    fijar_memoria_jugador(1);
    add_attack_spell(60,"embestir",3);
    add_attack_spell(100,"golpecertero",3);
    add_attack_spell(40,"furia",1);
    add_attack_spell(60,"concentracion",1);

    load_chat(20,({
        "'Tras los Años Oscuros, Kheleb Dum vuelve a ser tan poderosa como fue.",
        "'Los humanos de Kattak están haciendo un trabajo expléndido con el comercio.",
        "'La Alianza de Darin se ha convertido en uno de los mayores ejercitos de Dalaensar.",
        "'Parece que el Cataclismo no afectó a la belleza de las montañas que rodean Kheleb Dum.",
        "'El clima del Valle de Kattak es húmedo y frio, espero que los humanos estén acostumbrados.",
      }));
    load_a_chat(30,({
        //":comienza a seguirte con su penetrante y poderosa mirada.",
        "'¡Akdhum Khadul!",
      }));

    //add_clone(BARMADURAS+"gran_yelmo",1);
    add_clone(ARMADURAS+"parche_darin",1);
    add_clone(BARMADURAS+"botas_guerra.c",1);
    add_clone(BARMADURAS+"grebas_metalicas",1);
    add_clone(BARMADURAS+"guantelete_izq",1);
    add_clone(BARMADURAS+"guantelete",1);
    add_clone(BARMADURAS+"completa",1);
    add_clone(BARMADURAS+"cinturon",1);
    add_clone(BARMAS+"hacha_a_dos_manos.c",1);
    //add_clone(ARMAS+"hacha_darin",1);

    fijar_pvs_max(45000);
    set_bo(400);
    fijar_nivel(70+random(10));
    adjust_tmp_damage_bon(300);
    ajustar_dinero(8,"khaldan");
    init_equip();

    INICIAR_XP_MORIR;

    fijar_objetos_morir(({"tesoro", ({"arma", "armadura"}), ({5, 8}), 3}));

    /*nuevo_dialogo("reformas",({"'Hace unos pocos meses el consejo y yo "
"decidimos preparar un proyecto de restauración que ayudara a la apertura de Kheleb "
"Dum al resto de pueblos de Dalaensar.","'Aun que no negamos ser una nación "
"aguerrida, no podemos eludir que tras los años oscuros la casta enana "
"necesita una bocanada de aire capaz de sumirla en la contienda que agita "
"Eirea.",":suspira.","'Así pues, hemos encargado a los mejores capataces "
"enanos preparar sus utensilios para trabajar una vez más en la caverna.",
"'Temprano se otearán los frutos del esfuerzo."}));*/

    //misión de la vaina de Telchar
    nuevo_dialogo("bestia", ({"dialogo_bestia"}));

    nuevo_dialogo("guardia", ({
        "'¡Veo que estás bien informad${a:v}!",
        "'Estoy buscando nuevos luchadores para que formen junto a mi una guardia real.",
        "'Deseo entrenar personalmente a varios Khazads y formar con ellos lo que llamo "
        "la guardia de piedra.",
        "'La guardia de Piedra está formada por los Khazad Dum Uzbad más bravos de todo Kheleb.",
        "'Sólo aquellos cuya sabiduría haga honor a su linaje o cuyas barbas se midan en decenas de "
        "metros pueden engrosar y tener tal honor.",
        "'Yo nombro personalmente a cada uno de ellos.",
        "'La guardia es ungida y armada por nuestros herreros rúnicos.",
        "'A cambio, jurarán por siempre que defenderán el trono pase lo que pase y "
            "podrán desprenderse de sus barbas de mallas."
      })
    );

    // Khazad
    nuevo_dialogo("ungida", ({
        "'A cada guardia de piedra lo ungimos en aceites de montaña y le damos el gran honor "
        "de blandir las armas forjadas por herreros rúnicos.",
        "'Una vez prueben su valia, deberán jurar que por siempre jamás defenderán el trono de Darin "
        "pase lo que pase."
      }),
      "guardia"
    );
    nuevo_dialogo("valia", ({
        "'Para probar su valía enfrentaremos a cada guardia de piedra contra más de un centenar "
        "de criaturas malignas.",
        "'Si es capaz de derrotarlas a todas y vuelve aquí victorioso, le haremos entrega de dicho "
        "honor."
      }),
      "ungida"
    );
    nuevo_dialogo("criaturas", ({
        ":se mesa la barba y te examina con cautela.",
        "'Puede que des la talla jovenzuel${a:v}. ¿Quieres mostrar de qué estás hech${a:v}?, ¡muy bien!",
        "'Deberás derrotar a tres centenares de demonios de tierra.",
        "'Tus armas deberán decapitar por completo a una hidra.",
        "'Uno de los gusanos gigantes tuneleadores de la montaña ha de caer bajo tu poder.",
        "'Un manto oscuro deberá morir gritando tu nombre.",
        "'Y por último, pero no menos importante, deberás destruir al azote de enanos, Golvag.",
        "inicio_quest_guardia"
      }),
      "valia"
    );
    nuevo_dialogo("demonios", ({
        "'Los demonios de tierra son criaturas rastreras y huidizas que se manifiestan en este "
        "plano material a través de portales de nexo que permiten el viaje entre las delicadas "
        "fibras del éter.",
        "'Son criaturas débiles, lo que les permite atravesar los planos sin mucha "
        "dificultad. Se encuentran cerca del bosque de ucho, guardando el laboratorio de un "
        "apestoso nigromante. Has de dar muerte al menos a 300 de estos demonios.",
        "poner_hito_demonios"
      }),
      "criaturas"
    );
    nuevo_dialogo("hidra", ({
        "'Las hidras son criaturas de forma reptil que poseen varias cabezas. Son adversarios "
        "temibles que sólo temen al ácido y al fuego. Muchos valientes guerreros han muerto al "
        "morir de cansancio tras decapitar un millar de cabezas a una de estas criaturas sin "
        "que se inmuten. En Takome, en las cuevas de Aldara, encontrarás a una de estas bestias.",
        "poner_hito_hidra"
      }),
      "criaturas"
    );
    nuevo_dialogo("manto", ({
        "'Los mantos son criaturas de suboscuridad que habitan en las entrañas de la montaña de "
        "Kheleb-Dum. Son criaturas muy feroces y sanguinarias que gustan de adoptar la forma de "
        "una sombra para posteriormente abalanzarse sobre sus víctimas. Has de dar muerte al menos "
        "a una de estas criaturas.",
        "poner_hito_manto"
      }),
      "criaturas"
    );
    nuevo_dialogo("gusano", ({
        "'Los gusanos gigantes son una de las graves amenazas de Kheleb-Dum. Deambulan por el "
        "interior de la montaña, haciendo túneles gracias a su saliva ácida. Estropean minas, "
        "asesinan enanos y destruyen yacimientos mineros. Has de demostrar que eres capaz de "
        "librar a Kheleb-Dum de, al menos, el azote de uno de estos gusanos.",
        "poner_hito_gusano"
      }),
      "criaturas"
    );
    nuevo_dialogo("golvag", ({
        "'Golvag es el espíritu de un poderoso orco que azotaba a las filas de enanos de "
        "Kheleb-Dum ya en los tiempos de darin. Se dice que blande una cimitarra capaz de "
        "destripar a un enano con un solo roce. En tiempos murió, pero su poderoso espíritu "
        "quedó vinculado en una cripta al norte de Golthur-Orod. Has de derrotarle para probar "
        "tu valía.",
        "poner_hito_golvag"
      }),
      "criaturas"
    );
    // Herrero
    nuevo_dialogo("herrero", ({
        "'Los herreros rúnicos son los fraguadores enanos más experimentados.",
        "'Conocen la mejor manera de trabajar el mithril para convertirlo en poderosas armaduras y "
        "son capaces de imbuir en sus mejores trabajos el espíritu de la montaña.",
        "'Yo me encargaré de que aprendan a realizar esos trabajos, ya que necesito preparar a mi "
        "guardia de piedra para cualquier adversario."
      }),
      "guardia"
    );
    nuevo_dialogo("preparar", ({
        "'¿Crees que das la talla, muchach${a:v}?",
        "'¿Crees que tienes lo que hay que tener para aprender a forjar con el espíritu de la "
        "montaña?",
      }),
      "herrero"
    );
    nuevo_dialogo("si", ({
        ":se rie afablemente.",
        "'Muy bien, muy bien, ${a:qcn}. Pero antes de que te enseñe nada, deberás saber que el hecho de "
        "conocer esas recetas te obligará a prestar un juramento.",
        "'Siempre y digo, siempre, te comprometerás a fraguar uno de estos objetos a cualquier "
        "guardia de piedra que te lo pida.",
        "'Bajo ninguna circunstancia buscarás beneficio personal en su creación y olvidarás "
        "cualquier problema personal que tengas con los guardias de piedra que te lo soliciten.",
        "'¿Juras que cumplirás a rajatabla todo esto?"
      }),
      "preparar"
    );
    nuevo_dialogo("juras", ({
        "ensenyar_recetas"
      }),
      "si"
    );
    nuevo_dialogo("imbuir", ({
        "'No es ninguna hechicería. La montaña nos protege, como nosotros la protegemos a ella.",
        "'Un herrero rúnico que forje objetos en su corazón y con su misma sangre será capaz "
        "de crear los objetos con los que armaré a mi poderosa guardia de piedra.",
        "'Sólo a ellos les daré el derecho a blandir semejantes creaciones. El espíritu de la "
        "montaña es caprichoso."
      }),
      "herrero"
    );

    nuevo_dialogo("completado", ({
        "'¡Por las barbas de mis ancestros!, ¡has logrado conseguir todo lo que te pedi!",
        "'Tu perserverancia y fuerza no conocen igual, joven. Arrodíllate ante mi. Arrodíllate y "
        "prepárate a prestar juramento.",
        "'Hoy la montaña ha aceptado a un${a:a} herman${a:v} Khazad como noble protector${a:a} del Trono de piedra, "
        "hoy tenemos a un${a:a} herman${a:v} más que luchará hombro con hombro por defender nuestra monarquía,"
        " así como a los espíritus de nuestros ancestros y de las montañas.",
        ":posa su arma en el hombro de su fiel juramentad${a:v}.",
        "'Desde hoy tu nombre se asociará al de un miembro de la guardia de piedra, ${a:dnc}, lo que te dará "
        "derecho a blandir armas rúnicas. Tu poderío no pasara desapercibido y desde hoy, todos "
        "sabrán que tú has pasado por mil y un pesares con el fin de levantar el trono.",
        "'¡Jura!, ¡jura joven y pronto serás un${a:a} guardia de piedra!",
        "recitar_juramentos",
        "'Y ahora..., ¡álzate!, ¡álzate como un${a:a} guerrer${a:v} de piedra, con la cabeza bien alta por "
        "el honor que acabas de conseguir!",
        "hacer_guardia_de_piedra"
      })
    );
    nuevo_dialogo("juramentos", ({
        "'¿Quieres recordar tus votos, eh?, está bien...",
        "recitar_juramentos"
      }),
    );
    nuevo_dialogo("arma", ({
        "'Todos mis guardias de piedra pueden pedir a mis herreros que blandan las armas más "
        "poderosas de todo Kheleb para ellos.",
        "'Pero yo te haré entrega de una como muestra de respeto por haber superado la dura prueba "
        "de iniciación de un Guardia de piedra."
      }),
      0,
      ({"un hacha", "un martillo"}),
      "¿Qué arma deseas recibir de " + query_short() + " en honor a tu nombramiento como guardia de piedra?"
    );

    foreach(string prohibicion in ({"criaturas", "golvag", "manto", "hidra", "demonios", "gusano"})) {
        prohibir(prohibicion, "ya_termino_quest");
    }

    prohibir("juramentos", "es_guardia_de_piedra");
    prohibir("completado", "completado");
    prohibir("ungida", "khazad_valido");
    prohibir("criaturas", "nivel_minimo");
    prohibir("arma", "khazad_valido_arma");
    prohibir("bestia", "hito kheleb_vaina pista4");
    habilitar_conversacion();
}

void dialogo_bestia(object pj)
{

    if (pj->dame_mision_completada("kheleb_vaina"))
    {
        do_say("Que alegría verte "+pj->query_short()+", aún se te recuerda en Kheleb por el servicio que nos hiciste con la Bestia Desplazadora, vuelve después de un tiempo." ,0);
        return;
    }
    do_say("Así que lo que moraba bajo Kheleb era una de esas malditas criaturas llamadas Bestias Desplazadoras",0);
    do_say("Es una bestia de las profundidades, de mas allá de nuestros dominios.",0);
    do_say("Esto es una noticia excepcional que hay que celebrar por todo lo alto.",0);
    do_say("Haré correr la voz para tu nombre sea recordado en todo Kheleb Dum.",0);
    do_say("Estoy en deuda contigo, amigo.",0);
    pj->nuevo_hito_mision("kheleb_vaina","mision_terminada",0);
    pj->ajustar_estatus("Kheleb",35);
    return;
}
// Comprobaciones
int es_guardia_de_piedra(object b)
{
    return b->dame_propiedad_virtual("guardia_de_piedra");
}
status nivel_minimo(object b)
{
    return b->dame_nivel() >= 30;
}
status completado(object b)
{
    return !es_guardia_de_piedra(b) && b->dame_mision_completada("guardia_piedra");
}
status ya_termino_quest(object b)
{
    return !b->dame_mision_completada("guardia_piedra");
}
status khazad_valido(object b)
{
    return (
      b->dame_clase() == "khazad" &&
      !es_guardia_de_piedra(b) &&
      !b->dame_mision_completada("guardia_piedra")
    );
}
//void funcion_preguntas_aprender(object jugador,string respuesta)
//void entregar_arma(object b, string tx)
void funcion_respuestas_arma(object b, string tx)
{
    string ruta = "/clases/luchadores/armas/", *msj;

    switch(tx) {
    case "un martillo":
        ruta += "martillo_khazad.c";
        msj = ({"¡Recibe pues a Trymther, Martillo defensor del trono!",
          "Este arma ha pasado de manos de mi padre a mi, que a su vez pasó de su padre a él.",
          "¡Ahora yo te lo cedo a ti, para que protejas la corona de Kheleb y a todos sus habitantes "
          "con una determinación tan sólida como su forja!",
          "Tómalo, " + b->dame_nombre_completo() + ", Guardia de Piedra de tu Rey, y "
          "comprende el impresionante honor que recae sobre ti al recoger un arma tan legendaria."});
        break;

    case "un hacha":
        ruta += "hacha_khazad.c";
        msj   = ({
          "¡Recibe pues a Gennsvôr!, ¡el filo de justicia de Kheleb!",
          "Este mismo arma ha sido supervisada por mi en su forja para asegurarme de que "
          "será digna de " + b->dame_numeral() + " guerrer" + b->dame_vocal() + " tan "
          "honorable como tú.",
          "Su filo es tan afilado que es capaz de cortar a la propia montaña.",
          "Hoy tus ancestros están orgullosos, de ti, Guardia de piedra. Pues blandir "
          "este arma es un honor tan grande como el de aceptar la corona que yo poseo."
        });
        break;

    default:
        break;
    }

    if (!b->entregar_objeto(ruta)) {
        do_say("Lamento decirte, " + b->dame_nombre_completo() + ", que no puedo darte un arma en este momento.",0);
    }
    else {
        b->fijar_especializacion("guardia_piedra");
        b->nueva_propiedad_virtual("arma_guardia_piedra", 1);
        write_file(
          LOGS + "quest_guardia_piedra.log",
          ctime() + " " + b->query_cap_name() + " recibe " + ruta + ".\n"
        );
        foreach(string j in msj) {
            do_say(j, 0);
        }
    }
}
status khazad_valido_arma(object b)
{
    return b && b->dame_mision_completada("guardia_piedra") && !b->dame_propiedad_virtual("arma_guardia_piedra");
}
// Hitos
void dar_hito(string hito, object b)
{
    if (!b->dame_hito_mision("guardia_piedra", hito)) {
        b->nuevo_hito_mision("guardia_piedra", hito, 0);
    }
}
void inicio_quest_guardia(object b)
{
    if (b->dame_mision_abierta("guardia_piedra")) {
        do_say(
          "¿Te has olvidado de tu misión, " + b->query_cap_name() + "?, prueba a escribir "
          "\"misiones\" y relee lo que te conté en su día. Hay que ver la juventud... que cabezas"
          " teneis.", 0
        );
        return;
    }
    dar_hito("guardia-piedra", b);
}
void poner_hito_golvag(object b)
{
    dar_hito("matar-golvag", b);
}
void poner_hito_hidra(object b)
{
    dar_hito("matar-hidra", b);
}
void poner_hito_gusano(object b)
{
    dar_hito("matar-gusano", b);
}
void poner_hito_demonios(object b)
{
    dar_hito("matar-diablillos", b);
}
void poner_hito_manto(object b)
{
    dar_hito("matar-manto", b);
}
// Otro
void recitar_juramentos(object b)
{
    do_say(
      "¿Juras proteger el trono pase lo que pase, sin que te importe cuan desesperada sea la "
      "situación?", 0
    );
    b->do_say("¡Lo juro!", 0);
    do_say(
      "¿Juras proteger a la montaña y a su rey, a su pueblo y a sus hijos mientras conserves "
      "un hálito de vida?", 0
    );
    b->do_say("¡Lo juro!", 0);

    do_say(
      "¿Juras que combatirás con honor y con respeto contra tus enemigos, exceptuando a los orcos"
      "cuya vileza no permite que se le traten como a un adversario digno?", 0
    );
    b->do_say("¡Lo juro!", 0);

    do_say(
      "¿Juras que no conocerás otro miedo que aquel que derive de perder a tu rey, tu familia, "
      "tu linaje o tu montaña?",0
    );
    b->do_say("¡Lo juro!", 0);
}
void hacer_guardia_de_piedra(object b)
{
    tell_object(
      b,
      "Te levantas, orgulloso, como un nuevo guardia de piedra.\n"
      "Desde hoy, deberás cumplir tus votos a rajatabla\n"
    );
    "/grupos/ciudadanias/kheleb.c"->info_canal(
      query_short(),
      "¡Tres hurras hermanos!, ¡tres hurras por " + b->query_cap_name() + " que ha demostrado "
      "ser un duro y fiel guardia de piedra!"
    );
    b->nueva_propiedad_virtual("guardia_de_piedra", 1);
    write_file(
      LOGS + "quest_guardia_piedra.log",
      ctime() + " " + b->query_cap_name() + " se ha convertido en guardia de piedra.\n"
    );
}
void ensenyar_recetas(object b)
{
    if (b->dame_raza() != "enano") {
        do_say(
          b->query_cap_name() + ", te enorgullece el hecho de querer aprender los secretos "
          "de la fragua rúnica, pero es un secreto que ha pasado de generación en generación "
          "y sólo se transmite a enanos.",0
        );
        return;
    }

    if (b->dame_oficio() != "herrero") {
        do_say(
          b->query_cap_name()+ ", necesitas ser " + b->dame_numeral() + " herrer"
          + b->dame_vocal() + " para convertirte en " + b->dame_numeral() + " herrer"
          + b->dame_vocal() + " rúnic" + b->dame_vocal() + ".", 0
        );
        return;
    }

    if (b->dame_nivel_oficio() < 10) {
        do_say(
          "Sólo cuando alcances el máximo de experiencia para un herrero podré enseñarte "
          "los primeros secretos del forjado rúnico, " + b->query_cap_name() + ".", 0
        );
        return;
    }

    if (b->dame_explorado(MIN2 + "token_darin_guardia_piedra")) {
        do_say(
          "Ya conoces todo lo que puedo enseñarte de la forja rúnica, " + b->query_cap_name() +
          ". Si quieres aprender más de sus secretos deberás buscar por ti mismo o encontrar a "
          "otro maestro.", 0
        );
        return;
    }

    {
        object token = load_object(MIN2 + "token_darin_guardia_piedra.c");

        if (!token) {
            do_say(
              "Ahora mismo no puedo enseñarte nada, " + b->query_cap_name() + ", vuelve en "
              "otro momento.", 0
            );
            return;
        }
        token->dar_token(b);
        do_say(
          "¡Que así sea, " + b->dame_nombre_completo() + "!, ¡hoy será tu bautizo de tierra!, "
          "¡que la montaña reconozca tu nombre e imbuya en las armas que fragues bañadas en su "
          "sangre la fortaleza de Kheleb Dum!", 0
        );
        do_say(
          "¡Fragua armas con mithril y gemas!, ¡créalas con todo el cariño que puedas! y cuando "
          "tengas todo en la fragua, ardiendo en su palpitante corazón ígneo, báñalo con el "
          "corazón de la montaña: ¡sus rocas!", 0
        );
        do_say(
          "Ahora sabes como fraguar los objetos que con tanto secreto hemos guardado. Fragua "
          "un martillo y un hacha para mi guardia de piedra. Recuerda quien eres. Recuerda tu "
          "juramento.", 0
        );
        do_say(
          "Ahora... ¡levántate!, pues la montaña ya te conoce y eres uno más con la tierra.", 0
        );
    }
}
int frases_personalizables(string tipo, object npc, object pj)
{
    switch(tipo) {
    case "bienvenida-npc":
        if (pj->dame_clase() == "khazad") {
            tell_object(pj, query_short() + " te hace una reverencia.\n");
            do_say(
              "¡" + pj->dame_nombre_completo() + "!, es un placer ver a uno de mis más "
              "poderos" + pj->dame_vocal() + "s guerrer" + pj->dame_vocal() + "s en "
              "mi trono. ¿Qué necesitais de vuestro rey?",
              0
            );
        }
        else if (pj->dame_raza() == "enano") {
            do_say(
              "Bienvenido " + pj->dame_nombre_completo() + ". ¿Qué puede hacer tu rey por "
              "uno de sus vasallos y por su linaje?",
              0
            );
        }
        else {
            do_say(
              "Bienvenido "+pj->dame_nombre_completo()+", el pueblo enano te da la bienvenida."
              ,0
            );
        }
        break;

    case "despedida-npc":
        do_say("Que la montaña te proteja "+pj->dame_nombre_completo()+".",0);
        break;

        //Sacara el resto de mensajes por defecto para el resto
    default:
        return 0;
    }
    return 1;
}
void attack_by(object atacante)
{
    if(!query_timed_property("aviso_darin")) {
        add_timed_property("aviso_darin",1,25);
        HD->info_canal("%^ORANGE%^Darin%^RESET%^","¡Ciudadanos! ¡¡Atacan a vuestro Rey!! ¡¡¡Arrghhh!!! ¡¡¡¡"+capitalize(pluralize(atacante->dame_raza()))+"!!!!");
    }
    atacante->add_timed_property("perseguido_kd",1,3000);
    ::attack_by(atacante);
}
int unarmed_damage(int pupa, object estilo, object atacante)
{
    if ( ! estilo || ! atacante || ! atacante->query_player() )
        return ::unarmed_damage(pupa, estilo, atacante);
    
    if ( estilo->dame_estilo_desarmado() )
        atacante->comprobar_logro("karate_a_muerte_en_eirea","desafios",1,"darin");

    return ::unarmed_damage(pupa, estilo, atacante);
}
