// Altaresh 19
#include <tiempo.h>
#include <despojo.h>
#include <recetas.h>
#include <baseobs_path.h>
inherit _RECETA_BASE;
void    setup()
{
    fijar_funcion_sala("taller");
    anyadir_herramienta("alfiler");

    // fijar_bloqueo(DIA * 3);
    fijar_nivel_maximo(10);
    fijar_puntos(10);
    fijar_turnos(6);
    fijar_pe(50);

    fijar_mensajes(
        ({"Estiras la piel de dragón y mentalmente señalas los cortes y "
          "puntadas que vas a realizar",
          "Entregas el ojo al herrero y le comentas como debe crear una "
          "hebilla de él",
          "Empiezas a darle forma al cuero ayudado por tu hechicero que, con "
          "su magia"
          "te ayuda a hacer maleable la dura piel de dragón.",
          "Sigues dando forma al cuero, mientras el hechicero recita, "
          "te vas ayudando del hilo mágico para terminarlo",
          "El herrero te entrega la hebilla realizada tal y como le pediste, "
          "ahora es el "
          "momento de unir las dos piezas."
          "Los tres dais un salto hacia atrás mientras os miráis los unos a "
          "los otros, "
          "estáis convencidos que el ojo se ha movido en el momento de "
          "terminar la creación."}));

    anyadir_componente("hierro", 1000);
    anyadir_componente(BMISC + "hilo_magico.c", 3);
    anyadir_componente(DESPOJOS + "piel/dragon_buena.c", 1);
    anyadir_componente(BCOMPONENTES + "ojo_contemplador_metalizado.c", 1);

    anyadir_ayudante("clase", "hechicero", 30, 1);
    fijar_precio(62500);
    fijar_ruta_objeto("/d/urlom/armaduras/cinturon_contemplador.c");
}
