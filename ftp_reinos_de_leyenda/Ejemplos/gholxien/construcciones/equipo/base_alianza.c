//12.05.2021 [Gholxien]: Base para el equipo de la Alianza de Darin
#include "/d/kheleb/path.h"
inherit "/obj/multiobjetos/multiarmadura.c";

int no_catalogar_armeria() {
    return 1;
}

void create() {
    ::create();

    add_static_property("gremio", "alianza");
    fijar_valor(5500);

    call_out("reajustar_propietario", 0);
}

string dame_propietario() {
    return query_property("propietario");
}
void fijar_propietario(string tx) {
    add_property("propietario", tx);
}


int dame_piezas_equipadas(object pj) {
    return sizeof(pj->dame_objeto_equipado((
                : $1->dame_conjunto_alianza()
                : )));
}
int calcular_puntos_necesarios(object pj) {
    int piezas = dame_piezas_equipadas(pj);

    if (pj->query_creator() || pj->query_avatar())
        return 0;
    return 50 + 50 * piezas;   
}

int set_in_use(int i) {
    int ptos,
    piezas;

    if (!i || !environment()->query_player()) {
        return ::set_in_use(i);
    }

    
    ptos = calcular_puntos_necesarios(environment());
    piezas = dame_piezas_equipadas(environment()) + 1;

    if (i &&
        ptos > ALIANZA_ORG->dame_puntos_totales(environment()->query_name())) {
        tell_object(
            environment(),
            "Tu renombre en la Alianza de Darin es insuficiente para vestir " +
            piezas + " pieza" + (piezas > 1 ? "s" : "") +
            " del equipo de la organización. Necesitarás al "
            "menos una puntuación de " +
            ptos + " en la organización.\n");
        return 0;
    }

    return ::set_in_use(i);
}


// Para identificarlo
int dame_conjunto_alianza() {
    return 1;
}
