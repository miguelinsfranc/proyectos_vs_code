//23.04.2021 [Gholxien]: Conjunto del bribón para alianza
#include "/d/kheleb/path.h"
inherit "/obj/conjunto_nuevo.c";

void setup() {
    set_short("%^BOLD%^MAGENTA%^Galas del Truhan de la alianza%^RESET%^");
    generar_alias_y_plurales();
    add_alias("conjunto");
    fijar_genero(-2);

    fijar_conjunto_parcial(); {
        string * piezas = ({});

        for (int i = 0; i < 8; i++) {
            piezas += ({
                ARMADURAS_ALIANZA + "multiarmadura_bribon.c"
            });
        }

        fijar_piezas(piezas);
    }

    nuevo_beneficio_caracteristica(2, "be", 25);
    nuevo_beneficio_caracteristica(5, "sigilar", 30);
    nuevo_beneficio_caracteristica(5, "esconderse", 30);
    nuevo_beneficio_caracteristica(7, "des", 1);
}

