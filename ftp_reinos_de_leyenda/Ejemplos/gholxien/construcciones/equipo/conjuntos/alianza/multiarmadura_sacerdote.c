//23.04.2021 [Gholxien]: Multiarmadura para los clérigos de la alianza
#include "/d/kheleb/path.h"
inherit BASES_EQUIPO + "base_alianza";

void setup() {
    string firm = "del Clérigo de la Alianza%^RESET%^";

    fijar_conjuntos(({
            CONJUNTOS + "alianza_sacerdote.c"            
        }));
    fijar_titulos(firm, "del Clérigo de la Alianza");
    set_short("%^BOLD%^BLACK%^Multiarmadura " + firm);
    set_main_plural("%^BOLD%^BLACK%^Multiarmaduras " + firm + "%^RESET%^");

    fijar_long_generico("Un luminoso orbe metálico adornado con el símbolo de la "
        "Alianza de Darin: un martillo y un hacha unidos por el mismo mango, sobre una montaña bajo las estrellas.\n");

    fijar_encantamiento(10);
    add_static_property("tipo", "sacerdote");
}
void setup_generico_multiobjeto() {
    ::setup_generico_multiobjeto();

    fijar_tipos_validos(({
            "gran yelmo",
            "completa",
            "guantelete",
            "grebas metalicas",
            "botas de guerra",
            "brazal"
}));
}
void setup_especifico() {
    fijar_BO(5);
}
void poner_descripcion_especifica_multiobjeto() {
    set_long(
"Las placas de esta pieza están elegantemente recurvadas y pulidas y" +
" cada una de ellas ha sido recubierta por un fino pan de oro que le da" +
" a " + dame_este() + " " + dame_armadura_base() + " un fulgor dorado. Cada placa se desliza con suavidad y flexibilidad" +
" sobre las otras de manera que el conjunto es, a la vez, resistente," +
" ligero y hasta cierto punto flexible. Estas armaduras son utilizadas" +
" por los clérigos de la Alianza de Darin en batalla, y suelen ir" +
" cubiertas con sellos de su dios en forma de hoja de roble o cántaro de" +
" agua. Las placas más grandes han sido grabadas con runas enanas que" +
" loan a Eralie. Más allá de esto, no observas grandes ostentaciones.\n");
}
void poner_short_especifico_multiobjeto() {
    string * tx = ({});

    switch (dame_armadura_base()) {
    case "gran yelmo":
        tx = ({
            "Capacete dorado",
            "Capacetes dorados"
        });
        break;

    case "completa":
        tx = ({
            "Coraza dorada",
            "Corazas doradas"
        });
        break;

    case "guantelete":
        tx = ({
            "Guantelete dorado",
            "Guanteletes dorados"
        });
        break;

    case "grebas metalicas":
        tx = ({
            "Grebas doradas",
            "pares de Grebas doradas"
        });
        break;

    case "botas de guerra":
        tx = ({
            "Botas guarnecidas",
            "pares de Botas guarnecidas"
        });
        break;

    case "brazal":
        tx = ({
            "Brazal dorado",
            "Brazales dorados"
        });
        break;

    default:
        ::poner_short_especifico_multiobjeto();
        break;
    }

    set_short("%^BOLD%^CYAN%^" + tx[0] + " del Clérigo de la Alianza%^RESET%^");
    set_main_plural(
        "%^BOLD%^CYAN%^" + tx[1] + " del Clérigo de la Alianza%^RESET%^");
    actualizar_short_lado();
}

