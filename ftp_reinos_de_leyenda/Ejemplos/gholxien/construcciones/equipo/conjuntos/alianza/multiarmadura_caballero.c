// 23.04.2021 [Gholxien]: Multiarmadura para los caballeros de la alianza
#include "/d/kheleb/path.h"
inherit BASES_EQUIPO + "base_alianza";

void setup()
{
    string firm = "del Protector del Valle%^RESET%^";

    fijar_conjuntos(({
		 CONJUNTOS + "alianza_caballero.c"
                      }));
    fijar_titulos(firm, "del Protector del Valle");
    set_short("%^BOLD%^BLACK%^Multiarmadura " + firm);
    set_main_plural("%^BOLD%^BLACK%^Multiarmaduras " + firm + "%^RESET%^");

    fijar_long_generico(
        "Un luminoso orbe metálico adornado con el símbolo de la "
        "Alianza de Darin: un martillo y un hacha unidos por el mismo mango, "
        "sobre una montaña bajo las estrellas.\n");

    fijar_encantamiento(10);
    add_static_property("tipo", "caballero");
}
void setup_generico_multiobjeto()
{
    ::setup_generico_multiobjeto();

    fijar_tipos_validos(({
        "gran yelmo",
        "completa",
        "guantelete",
        "grebas metalicas",
        "botas de guerra",
        "brazal",
        }));
}
void setup_especifico()
{
    fijar_BO(3);
    fijar_BP(2);
}
void poner_descripcion_especifica_multiobjeto()
{
    string es = dame_genero() < 0 ? "son" : "es";

    set_long(
        capitalize(dame_este()) + " " + dame_armadura_base() + " " + es + " " +
        dame_articulo() + " utilizad" + dame_vocal() +
        " por los escasos pero valerosos paladines" +
        " oriundos de la ciudad de Kattak, ungidos en el templo de Eralie y "
        "que" +
        " prestan sus servicios en la Alianza de Darin.\n"
        "En batalla, son el martillo que golpea a las tropas enemigas contra "
        "el" +
        " inquebrantable yunque de la infantería acorazada enana y sus "
        "gloriosas" +
        " cargas han salvado la batalla en más de una ocasión.\n" +
        "Cada placa y cada lámina o escama está bellamente curvada, pulida, y" +
        " acabada en un delicado esmaltado. Diminutas runas enanas casi" +
        " imperceptibles imbuyen a la pieza de mayor resistencia y hechizos" +
        " protectores, donde el poder de Eralie y la magia rúnica enana se "
        "combinan" +
        " para dar lugar a una exquisita pieza de armadura. La influencia de "
        "la" +
        " cultura del Valle de Kattak se deja sentir en el leve tono verde "
        "del" +
        " acero y en las estilizadas hojas de roble unidas a la silueta de "
        "una" +
        " montaña que han sido cinceladas  en cada pieza.\n");
}

void poner_short_especifico_multiobjeto()
{
    string *tx = ({});

    switch (dame_armadura_base()) {
        case "gran yelmo":
            tx = ({"Gran Yelmo", "Grandes Yelmos"});
            break;

        case "completa":
            tx = ({"Coraza esmaltada", "Corazas esmaltadas"});
            break;

        case "guantelete":
            tx = ({"Guantelete articulado", "Guanteletes articulados"});
            break;

        case "grebas metalicas":
            tx = ({"Grebas articuladas", "pares de Grebas articuladas"});
            break;

        case "botas de guerra":
            tx = ({"Botas acorazadas", "pares de Botas acorazadas"});
            break;

        case "brazal":
            tx = ({"Guardabrazo articulado", "Guardabrazos articulados"});
            break;

        default:
            ::poner_short_especifico_multiobjeto();
            break;
    }

    set_short("%^BOLD%^BLUE%^" + tx[0] + " del Protector del Valle%^RESET%^");
    set_main_plural(
        "%^BOLD%^BLUE%^" + tx[1] + " del Protector del Valle%^RESET%^");
    actualizar_short_lado();
}

