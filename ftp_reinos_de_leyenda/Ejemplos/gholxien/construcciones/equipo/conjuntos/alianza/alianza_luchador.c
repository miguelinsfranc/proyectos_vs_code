//23.04.2021 [Gholxien]: Conjunto del luchador para alianza
#include "/d/kheleb/path.h"
inherit "/obj/conjunto_nuevo.c";

void setup() {
    set_short("%^BOLD%^RED%^Arsenal del Guerrero de la Alianza%^RESET%^");
    generar_alias_y_plurales();
    add_alias("conjunto");
    fijar_genero(1);

    fijar_conjunto_parcial();{
        string * piezas = ({});

        for (int i = 0; i < 8; i++) {
            piezas += ({
                ARMADURAS_ALIANZA + "multiarmadura_luchador.c"
            });
        }

        fijar_piezas(piezas);
    }

nuevo_beneficio_caracteristica(2, "bo", 25);
    nuevo_beneficio_caracteristica(5, "daño", 25);
    nuevo_beneficio_caracteristica(7, "fue", 1);
}

