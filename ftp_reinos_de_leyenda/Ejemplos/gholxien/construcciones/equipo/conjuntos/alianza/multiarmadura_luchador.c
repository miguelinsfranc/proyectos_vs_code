//23.04.2021 [Gholxien]: Multiarmadura para los luchadores de la alianza
#include "/d/kheleb/path.h"
inherit BASES_EQUIPO + "base_alianza";

void setup() {
    string firm = "del Guerrero de la Alianza%^RESET%^";

    fijar_conjuntos(({
            CONJUNTOS + "alianza_luchador.c"            
        }));
    fijar_titulos(firm, "del Campeón de Darin");
    set_short("%^BOLD%^BLACK%^Multiarmadura " + firm);
    set_main_plural("%^BOLD%^BLACK%^Multiarmaduras " + firm + "%^RESET%^");

    fijar_long_generico("Un luminoso orbe metálico adornado con el símbolo de la "
        "Alianza de Darin: un martillo y un hacha unidos por el mismo mango, sobre una montaña bajo las estrellas.\n");

    fijar_encantamiento(10);
    add_static_property("tipo", "luchador");
}
void setup_generico_multiobjeto() {
    ::setup_generico_multiobjeto();

    fijar_tipos_validos(({
            "gran yelmo",
            "completa",
            "guantelete",
            "grebas metalicas",
            "botas de guerra",
            "brazal",
            "yelmo",
            "mallas",
            "manopla",
            "grebas de malla",
            "botas de campaña",
            "brazalete de mallas",
            "gorro duro",
            "cuero",
            "guante de cuero",
            "grebas",
            "botas",
            "brazalete"
        }));
}
void setup_especifico() {
    fijar_BO(4);
    add_static_property("daño", 5);
}
void poner_descripcion_especifica_multiobjeto() {
    if (dame_material() == 2) {
        set_long(
            capitalize(dame_este()) + " " + dame_armadura_base() + " forma" + dame_n() + " parte de la panoplia básica utilizada por los" +
            " guerreros de la Alianza de Darin, la infantería pesada, tanto humana" +
            " como enana, que forman la columna vertebral del ejército de las" +
            " tierras de Kheleb Dum y Kattak.\n" +
            capitalize(dame_articulo()) + " " + dame_armadura_base() + " que observas está" + dame_n() + " confeccionad" + dame_vocal() + " con lamelar, un sistema" +
            " de pequeñas láminas rectangulares orizontales y solapadas, unidas" +
            " entre ellas con intrincado hilo de resistente acero en el que se ha" +
            " vertido cierta pequeña cantidad de mithril y otros metales. Esta forma de confección" +
            " produce unas piezas de armadura que, aunque algo pesadas, son más" +
            " flexibles que las armaduras de placas o escamas.\n" +
            "Además, cuenta con" +
            " una base interior de cuero blando y ligeramente almohadillado que" +
            " amortigua las posibles contusiones.\n");
    } else {
        set_long(
            capitalize(dame_este()) + " " + dame_armadura_base() + " está" + dame_n() + " confeccionad" + dame_vocal() + " con los mejores materiales que" +
            " están disponibles en el valle de Kattak. Resulta una prenda" +
            " confortable y cálida a la par que resistente, aunque más ligera que" +
            " las armaduras más pesadas de la infantería de la Alianza. Está pensada" +
            " para las tropas que tienen que viajar deprisa y para la infantería ligera.\n");
    }
}
void poner_short_especifico_multiobjeto() {
    string * tx = ({});

    switch (dame_armadura_base()) {
    case "gran yelmo":
        tx = ({
            "Alto Yelmo lamelar",
            "Altos Yelmos lamelares"
        });
        break;

    case "yelmo":
        tx = ({
            "Alto Yelmo lamelar",
            "Altos Yelmos lamelares"
        });
        break;

    case "completa":
        tx = ({
            "Coraza lamelar",
            "Corazas lamelares"
        });
        break;

    case "guantelete":
        tx = ({
            "Guantelete lamelar",
            "Guanteletes lamelares"
        });
        break;

    case "manopla":
        tx = ({
            "Manopla lamelar",
            "Manoplas lamelares"
        });
        break;

    case "grebas metalicas":
        tx = ({
            "Grebas lamelares",
            "pares de Grebas lamelares"
        });
        break;

    case "grebas de malla":
        tx = ({
            "Grebas lamelares",
            "pares de Grebas lamelares"
        });
        break;

    case "botas de guerra":
        tx = ({
            "Botas lamelar",
            "pares de Botas lamelares"
        });
        break;

    case "botas de campaña":
        tx = ({
            "Botas de campaña",
            "pares de Botas de campaña"
        });
        break;

    case "brazal":
        tx = ({
            "Brazal lamelar",
            "Brazales lamelares"
        });
        break;

    case "brazalete de mallas":
        tx = ({
            "Brazalete lamelar",
            "Brazaletes lamelares"
        });
        break;

    case "gorro duro":
        tx = ({
            "Gorro duro",
            "Gorros duros"
        });
        break;

    case "guante de cuero":
        tx = ({
            "Guante de cuero",
            "Guantes de cuero"
        });
        break;

    default:
        ::poner_short_especifico_multiobjeto();
        return;
    }

    set_short("%^BOLD%^RED%^" + tx[0] + " del Guerrero de la Alianza%^RESET%^");
    set_main_plural(
        "%^BOLD%^RED%^" + tx[1] + " del Guerrero de la Alianza%^RESET%^");
    actualizar_short_lado();
}

