//23.04.2021 [Gholxien]: Conjunto del hechicero para alianza
#include "/d/kheleb/path.h"
inherit "/obj/conjunto_nuevo.c";

void setup() {
    set_short("%^BOLD%^YELLOW%^Ropajes Arcanos del Hechicero de la Alianza%^RESET%^");
    generar_alias_y_plurales();
    add_alias("conjunto");
    fijar_genero(-1);

    fijar_conjunto_parcial(); {
        string * piezas = ({});

        for (int i = 0; i < 8; i++) {
            piezas += ({
                ARMADURAS_ALIANZA + "multiarmadura_hechicero.c"
            });
        }

        fijar_piezas(piezas);
    }

    nuevo_beneficio_caracteristica(2, "poder_magico", 20);
    nuevo_beneficio_caracteristica(5, "poder_magico", 30);
    nuevo_beneficio_caracteristica(7, "int", 1);
}

