//23.04.2021 [Gholxien]: Conjunto del cazador para alianza
#include "/d/kheleb/path.h"
inherit "/obj/conjunto_nuevo.c";

void setup() {
    set_short("%^BOLD%^ORANGE%^Atavíos del Batidor de la Alianza%^RESET%^");
    generar_alias_y_plurales();
    add_alias("conjunto");
    fijar_genero(-1);

    fijar_conjunto_parcial(); {
        string * piezas = ({});

        for (int i = 0; i < 8; i++) {
            piezas += ({
                ARMADURAS_ALIANZA + "multiarmadura_cazador.c"
            });
        }

        fijar_piezas(piezas);
    }

    nuevo_beneficio_caracteristica(2, "critico", 5);
    nuevo_beneficio_caracteristica(5, "daño", 30);
    nuevo_beneficio_caracteristica(5, "sigilar", 15);
    nuevo_beneficio_caracteristica(5, "esconderse", 15);
    nuevo_beneficio_caracteristica(7, "des", 1);
}

