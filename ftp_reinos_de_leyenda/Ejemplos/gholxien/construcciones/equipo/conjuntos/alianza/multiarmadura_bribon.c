//23.04.2021 [Gholxien]: Multiarmadura para los bribones de la alianza
#include "/d/kheleb/path.h"
//inherit BASES_EQUIPO + "base_alianza";
inherit "/w/gholxien/base_alianza.c";

void setup() {
    string firm = "del Truhan de la alianza%^RESET%^";

    fijar_conjuntos(({
            CONJUNTOS + "alianza_bribon.c"           
        }));
    fijar_titulos(firm, "del Truhan de la Alianza");
    set_short("%^BOLD%^BLACK%^Multiarmadura " + firm);
    set_main_plural("%^BOLD%^BLACK%^Multiarmaduras " + firm + "%^RESET%^");

    fijar_long_generico("Un luminoso orbe metálico adornado con el símbolo de la "
        "Alianza de Darin: un martillo y un hacha unidos por el mismo mango, sobre una montaña bajo las estrellas.\n");

    fijar_encantamiento(10);
    add_static_property("tipo", "bribon");
}
void setup_generico_multiobjeto() {
    ::setup_generico_multiobjeto();

    fijar_tipos_validos(({
            "capucha",
            "camiseta",
            "guante",
            "pantalones",
            "zapatillas",
            "muñequera",
            "gorro duro",
            "cuero",
            "guante de cuero",
            "grebas",
            "botas",
            "brazalete"
        }));
}
void setup_especifico() {
    fijar_BO(1);
    fijar_BE(4);
}
void poner_descripcion_especifica_multiobjeto() {
    set_long(
        "Al contrario que los soldados, caballeros o clérigos de la Alianza," +
         " los bardos, ladrones, espías y demás bribones que trabajan para esta" +
         " asociación militar y que pasan gran parte de su vida en tierras" +
         " lejanas, gustan del lujo y la ostentación y lo utilizan para llevar a" +
         " cabo sus objetivos.\n" + 
        capitalize(dame_este()) + " " + dame_armadura_base() + " está" + dame_n() + " confeccionad" + dame_vocal() + " utilizando diversos" +
         " materiales, no solo el cuero más fino de Kattak, teñido de abigarrados" +
         " colores, si no tejidos nobles como la seda y el lino. El conjunto da" +
         " lugar a excelentes piezas de ropa que permiten moverse en silencio y" +
         " con fluidez por las calles de una urbe pero también pueden ser" +
         " utilizadas en las cortes más refinadas.\n");
}
void poner_short_especifico_multiobjeto() {
    string * tx = ({});

    switch (dame_armadura_base()) {
    case "pantalones":
        tx = ({
            "Bombachos",
            "Pares de bombachos"
        });
        break;

    case "zapatillas":
        tx = ({
            "Alpargatas",
            "Pares de alpargatas"
        });
        break;

    case "botas":
        tx = ({
            "Botines",
            "Pares de botines"
        });
        break;

    case "grebas":
        tx = ({
            "Bombachos",
            "Pares de bombachos"
        });
        break;

    case "capucha":
        tx = ({
            "Capucha acolchada",
            "Capuchas acolchadas"
        });
        break;

    case "gorro duro":
        tx = ({
            "Capucha acolchada",
            "Capuchas acolchadas"
        });
        break;

    case "guante de cuero":
        tx = ({
            "Mitón",
            "Mitones"
        });
        break;

case "guante":
        tx = ({
            "Mitón",
            "Mitones"
        });
        break;

    case "camiseta":
        tx = ({
            "Jubón acolchado",
            "Jubones acolchados"
        });
            break;

        case "cuero":
            tx = ({
                "Jubón acolchado",
                "Jubones acolchados"
            });
                break;

            case "brazalete":
                tx = ({
                    "Muñequera",
                    "Muñequeras"
                });
                break;

            default:
                tx = ({
                    capitalize(dame_armadura_base()),
                    capitalize(dame_armadura_base()) + "s"
                });
                break;
    }

    set_short("%^BOLD%^MAGENTA%^" + tx[0] + " del Truhan de la Alianza%^RESET%^");
    set_main_plural(
        "%^BOLD%^MAGENTA%^" + tx[1] + " del Truhan de la Alianza%^RESET%^");
    actualizar_short_lado();
}

