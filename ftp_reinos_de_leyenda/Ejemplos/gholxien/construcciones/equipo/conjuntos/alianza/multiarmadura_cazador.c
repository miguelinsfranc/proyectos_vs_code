//23.04.2021 [Gholxien]: Multiarmadura para los cazadores de la alianza
#include "/d/kheleb/path.h"
inherit BASES_EQUIPO + "base_alianza";

void setup() {
    string firm = "del Batidor de la Alianza%^RESET%^";

    fijar_conjuntos(({
            CONJUNTOS + "alianza_cazador.c"           
        }));
    fijar_titulos(firm, "del Batidor de la Alianza");
    set_short("%^BOLD%^BLACK%^Multiarmadura " + firm);
    set_main_plural("%^BOLD%^BLACK%^Multiarmaduras " + firm + "%^RESET%^");

    fijar_long_generico("Un luminoso orbe metálico adornado con el símbolo de la "
        "Alianza de Darin: un martillo y un hacha unidos por el mismo mango, sobre una montaña bajo las estrellas.\n");

    fijar_encantamiento(10);
    add_static_property("tipo", "cazador");
}
void setup_generico_multiobjeto() {
    ::setup_generico_multiobjeto();

    fijar_tipos_validos(({
            "capucha",
            "camiseta",
            "guante",
            "pantalones",
            "zapatillas",
            "muñequera",
            "gorro duro",
            "cuero",
            "guante de cuero",
            "grebas",
            "botas",
            "brazalete"
        }));
}
void setup_especifico() {
    fijar_BO(3);
    fijar_BE(2);
}
void poner_descripcion_especifica_multiobjeto() {
string es = dame_genero() >= 0 ? "" : "es";

set_long(
        capitalize(dame_este()) + " " + dame_armadura_base() + " está" + dame_n() + " confeccionad" + dame_vocal() + " con varias capas de cuero flexible" +
        " y bien trabajado. Sendas cintas y correas de cuero permiten ajustar la" +
        " pieza convenientemente a la talla de su portador y dispone de hebillas" +
        " de metal oscurecido y forrado de finísima piel para evitar que" +
        " tintineen.\n" +
        "Los bosques del Valle de Kattak suministran pieles de óptima calidad y"
        " de diversos animales que son tratadas y curtidas en las experimentadas" +
        " curtidurías de Kattak para el comercio exterior, pero los mejores" +
        " cueros se reservan para la confección de piezas como la que ahora" +
        " mismo observas. Ha sido teñida con tintes naturales que permiten" +
        " camuflarse fácilmente con el entorno, y su corte garantiza una gran" +
        " movilidad por terrenos agrestes. Un suave tejido remata la parte interior" +
        " de " + dame_este() + " " + dame_armadura_base() + " haciéndol" + dame_vocal() + " confortable" + dame_s() + " y útil" + es + " para soportar los" +
        " rigores climáticos de las tierras salvajes.\n");
}
void poner_short_especifico_multiobjeto() {
    string * tx = ({});

    switch (dame_armadura_base()) {
    case "pantalones":

    case "zapatillas":
    case "botas":
        tx = ({
            "Botas altas",
            "Pares de Botas altas"
        });
        break;

    case "grebas":
        tx = ({
            "Polainas de cuero",
            "Pares de Polainas de cuero"
        });
        break;

    case "capucha":
    case "gorro duro":
        tx = ({
            "Capucha",
            "Capuchas"
        });
        break;

    case "guante de cuero":
        tx = ({
            "Guante de cuero",
            "Guantes de cuero"
        });
        break;

    case "guante":
        tx = ({
            "Guante",
            "Guantes"
        });
        break;
   
    case "cuero":
        tx = ({
            "Tabardo de cuero",
            "Tabardos de cuero"
        });
        break;

    case "brazalete":
        tx = ({
            "Brazalete de cuero",
            "Brazaletes de cuero"
        });
        break;

    case "muñequera":
        tx = ({
            "Muñequera de cuero",
            "Muñequeras de cuero"
		});
            break;

        default:
            tx = ({
                capitalize(dame_armadura_base()),
                capitalize(dame_armadura_base()) + "s"
            });
            break;
        }

            set_short("%^BOLD%^ORANGE%^" + tx[0] + " del Batidor de la Alianza%^RESET%^");
            set_main_plural(
                "%^BOLD%^ORANGE%^" + tx[1] + " del Batidor de la Alianza%^RESET%^");
            actualizar_short_lado();
    }

