//23.04.2021 [Gholxien]: Multiarmadura para los hechiceros de la alianza
#include "/d/kheleb/path.h"
inherit BASES_EQUIPO + "base_alianza";

void setup() {
    string firm = "del Hechicero de la Alianza%^RESET%^";

    fijar_conjuntos(({
            CONJUNTOS + "alianza_hechicero.c"            
        }));
    fijar_titulos(firm, "del Hechicero de la Alianza");
    set_short("%^BOLD%^BLACK%^Multiarmadura " + firm);
    set_main_plural("%^BOLD%^BLACK%^Multiarmaduras " + firm + "%^RESET%^");

    fijar_long_generico("Un luminoso orbe metálico adornado con el símbolo de la "
        "Alianza de Darin: un martillo y un hacha unidos por el mismo mango, sobre una montaña bajo las estrellas.\n");

    fijar_encantamiento(10);
    add_static_property("tipo", "hechicero");
}
void setup_generico_multiobjeto() {
    ::setup_generico_multiobjeto();

    fijar_tipos_validos(({
            "capucha",
            "tunica",
            "guante",
            "pantalones",
            "zapatillas",
            "muñequera",
        }));
}

void setup_especifico() {
    fijar_BE(5);
    add_static_property("regeneracion_pes", 5);
}
void poner_descripcion_especifica_multiobjeto() {
    set_long(
        "Pese a que Kattak y Kheleb producen sobre todo metales, joyas, madera" +
        " y cuero, la calidad que últimamente están consiguiendo las tejedoras" +
        " humanas y enanas de Kattak está abriendo un sitio a los textiles" +
        " kattenses más allá de las montañas.\n" +
        " Utilizando los mejores tejidos de tierras lejanas como el terciopelo y" +
        " fundiéndolo con el más delicado cuero kattense, los tejedores del Valle" +
        " han elaborado " + dame_este() + " " + dame_armadura_base() + " exclusivamente para los hechiceros" +
        " alistados en la Alianza de Darin. Signos arcanos plateados jalonan " +
        dame_articulo() + " " + dame_armadura_base() + " dándole un toque arcano a esta ostentosa prenda y" +
        " revistiendo a su portador de un aura de poder y majestad.\n");
}
void poner_short_especifico_multiobjeto() {
    string * tx = ({});

    switch (dame_armadura_base()) {
    case "capucha":
        tx = ({
            "Capucha ornamentada",
            "Capuchas ornamentadas"
        });
        break;

    case "tunica":
        tx = ({
            "Túnica ornamentada",
            "Túnicas ornamentadas"
        });
        break;

    case "guante":
        tx = ({
            "Guante ornamentado",
            "Guantes ornamentados"
        });
        break;

    case "pantalones":
        tx = ({
            "Faldón ornamentado",
            "Faldones ornamentados"
        });
        break;

    case "zapatillas":
        tx = ({
            "Botines ornamentados",
            "pares de Botines ornamentados"
        });
        break;

    case "muñequera":
        tx = ({
            "Muñequera ornamentada",
            "Muñequeras ornamentadas"
        });
        break;

    default:
        ::poner_short_especifico_multiobjeto();
        break;
    }

    set_short("%^BOLD%^YELLOW%^" + tx[0] + " del Hechicero de la Alianza%^RESET%^");
    set_main_plural(
        "%^BOLD%^YELLW%^" + tx[1] + " del Hechicero de la Alianza%^RESET%^");
    actualizar_short_lado();
}

