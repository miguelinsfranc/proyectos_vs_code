// Gholxien 16.04.2021

#include <unidades.h>
inherit "/obj/arma_de_mano.c";
void setup() {
    set_name("garra");
    set_short("Garra de %^GREEN%^Lobo Blanco%^RESET%^");
    set_main_plural("Pares de garras de %^GREEN%^Lobo Blanco%^RESET%^");
    generar_alias_y_plurales();
    add_alias(({
            "garra",
            "lobo",
            "arma",
            "mano",
            "blanco"
        }));
    add_plural("garras");

    set_long("La sangre aún fresca chorrea por el hueso que debió pertenecer al brazo del lobo, junto a tendones y cartílagos que cuelgan inertes como si de hilos se tratasen. Las uñas están muy afiladas, en disposición de ataque, haciendo la forma idónea para ser acoplada en la parte posterior de alguna mano ajena, y que apretando los tendones alrededor de la muñeca ofrecen una sujeción perfecta. Sin embargo, las almohadillas de la palma parecen podridas, como si el ser al que perteneciese en vida estuviese enfermo.\n");

    fijar_genero(-2);
    ajustar_BO(10);
    ajustar_BP(10);
    nuevo_efecto_basico("vampirismo", 5);
    nuevo_efecto_basico("abundancia", __ud(5, "%"));
    fijar_valor(5 * 500);
    fijar_material(7);
    fijar_peso(1000);
    fijar_vida_max(1300);
}

int no_catalogar_armeria() { return 1; }