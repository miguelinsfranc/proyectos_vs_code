// Satyr 27.01.2018
#include <grupos.h>
inherit CLANES + "clan.c";

void setup()
{
    fijar_corto("Drakh'Gul");

    fundadores = ({"gragharg", "dogga", "naghig"});
    configuracion_comun();
}
