// 20.05.2021 [Gholxien]: Sala del fondo del foso de la garra del gremio Círculo
// del Simbionte
// clang-format off
#include "/d/dendra/path.h"
inherit "/std/room";

void setup() {
    set_short("Fondo del foso");
    set_long(
        "Mires a donde mires, solo ves fango y más fango. El mismo en el que "
        "te has metido sin pensar y que ahora empiezas a darte cuenta, te está engullendo lentamente y parece que "
        "si no te mueves acabarás con barro hasta las orejas. El olor es nauseabundo, mucho más que "
        "allá arriba y comienzas a respirar por la boca, antes de que las arcadas te impidan salir de allí. "
        "Las paredes del foso son rectas, sin demasiados salientes a los que agarrarse, por lo que empiezas a sentir un sudor "
        "frío que recorre tu espalda, pensando en como saldrás de allí si no consigues encontrar algo a lo que agarrarte para no acabar siendo tragado por el asqueroso fango.\n");

    add_item(({
            "fango",
            "barro"
        }), "Es denso y de color parduzco. Se encuentra prácticamente inmóvil, excepto por algunas zonas donde un extraño burbujeo irrumpe sobre la superficie, creando ondas que rompen en unas pequeñas olas en las paredes del foso.");
    add_item(({
            "burbujas",
            "burbujeo"
        }), "Unas grandes pompas de aire sacuden la superficie del fango con un sonoro plof. Al estallar una de las burbujas más cercanas a ti, te parece ver un resplandor.");
    add_item("resplandor", "Esperas a que otra burbuja se forme y estalle. Si, definitivamente hay algo que brilla en el fango y no parece estar enterrado a mucha profundidad. Quizá podrías sacarlo si introduces la mano.");

    add_exit("arriba", WARETH_TUNELES + "garra", "standard");
    modify_exit("arriba", ({
            "funcion",
            "pasar"
        }));
}

string * dame_mensajes_introducir_mano(object pj) {
    return ({
        "Tanteas con bastante asco el fango resbaloso y viscoso, hasta que tus dedos tocan algo duro y contundente.",
        "Lo agarras como puedes y lo sacas de un tirón del limo que parece resistirse a soltar lo que quiera que sea, pero por fin, tras un último tirón, tu mano y el objeto quedan libres. ¡No puedes creerte tu suerte! ¡Es un diamante del tamaño de la cabeza de un enano! Sonríes, llen" + pj->dame_vocal() + " de gozo, cuando un tentáculo gigante de color purpúreo, repleto de púas de aspecto ponzoñoso y ventosas más grandes que el diamante que acabas de encontrar, se abalanza hacia ti como un latigazo.",
        "El tentáculo se aferra al diamante y comienza a tirar con fuerza de él. Ya sea por el susto, o por el fango pegajoso que hace efecto de pegamento, no consigues soltar el diamante, por lo que empiezas a verte arrastrad" + pj->dame_vocal() + " hacia la superficie del asqueroso fango.",
        "Otro tentáculo salido de no se sabe donde, aparece en el borde de tu campo de visión y se enrosca en tu cintura, tirando hacia el lado contrario. Sientes que te vas a partir en dos por la presión, cuando por fin el diamante se despega de tu mano, pero entonces otra preocupación invade tu mente, ya que un gigantesco tentáculo de algo a lo que no quieres verle la cabeza y aún menos la boca, te tiene atrapad" + pj->dame_vocal() + " y empieza a costarte respirar.",
        "El tentáculo te eleva con rapidez casi hasta el borde del foso y te lanza como si se tratara de un desperdicio. Te aferras con uñas y dientes al suelo del foso, mientras escuchas como el tentáculo desaparece en el fango del fondo, unos segundos después con un desagradable chof. El olor del lugar no ha mejorado y en parte ahora gracias a la mancha de tu ropa interior.",
    });
}
/**
 * Función auxiliar que devuelve los mensajes de introducir mano que recibe la sala desde
 * la que se encuentra.
 *
 * Cada elemento del array es el mensaje de ese turno.
 */
string * dame_mensajes_introducir_mano_sala(object pj) {
    return ({
        pj->query_cap_name() + " introduce su mano en el asqueroso fango y empieza a tantear con los dedos.",
        pj->query_cap_name() + " agarra algo que se encuentra debajo del asqueroso barro y comienza a tirar de él con el fin de sacarlo. Tras un fuerte tirón, " + pj->query_cap_name() + " saca un gran diamante. Instantes después, un tentáculo asqueroso lleno de ventosas sale de entre el fango y se abalanza contra " + pj->query_cap_name() + ", como si fuera un látigo.",
        "El tentáculo se aferra al diamante y comienza a tirar con fuerza de él. Ya sea por el susto, o por el fango pegajoso que hace efecto de pegamento, " + pj->query_cap_name() + " no consigue soltar el diamante, por lo que empieza a verse arrastrad" + pj->dame_vocal() + " hacia la superficie del asqueroso fango.",
        "Otro tentáculo sale de entre el fango y se enrolla alrededor de la cintura de " + pj->query_cap_name() + ", tirando hacia el lado contrario. Tras unos instantes de angustia, por fin el diamante se despega de las manos de " + pj->query_cap_name() + ", pero el otro tentáculo sigue enrollado a su cintura, haciéndole muy difícil el respirar.",
        "El tentáculo eleva a " + pj->query_cap_name() + " hasta perderse de vista por el borde del foso. Instantes después, desciende con una velocidad pasmosa y se hunde en el fango con un asqueroso sonido a succión.",
    });
}
/**
 * Función auxiliar que muestra a un jugador y a la sala los mensajes del introducir mano.
 *
 * Se usa dentro del call_out de más abajo.
 */
void mostrar_mensajes_introducir_mano(object pj, string mensaje, string mensaje_sala) {
    if (!pj || environment(pj) != TO) {
        return;
    }

    tell_object(pj, mensaje + "\n");
    tell_accion(TO, mensaje_sala + "\n", "", ({
            pj
        }), pj);
}
/**
 * Función auxiliar que se llama al terminar la acción de introducir mano
 */
void teletransporte(object pj) {
    tell_object(pj, "Te reincorporas del suelo y retrocedes rápidamente del borde del foso, no vaya a ser que vuelva el tentáculo. Cuando miras a tu alrededor, te das cuenta de que has vuelto a la sala que hay arriba del foso.\n");
    pj->move_player("arriba", WARETH_TUNELES "garra",
        ({
            //Satyr, poner lo de los flags de razas en los mensajes de abajo
            pj->query_cap_name() + " se va hacia arriba.",
            pj->query_cap_name() + " aparece por el borde del foso"
        }), 0, "el foso");
}
/**
 * Función activada con el anyadir_comando
 */
int funcion_introducir_mano() {
    string * mensajes_pj,
     * mensajes_sala;

    /**
     * Esto es para evitar que se pueda poner "introducir mano" varias veces.
     */
    if (TP->query_timed_property("introducir_mano-" + __FILE__)) {
        return notify_fail("Ahora tienes problemas mas serios como para volver a introducir la mano en el fango.\n");
    }

    /**
     * Obtenemos mensajes
     */
    mensajes_pj = dame_mensajes_introducir_mano(TP);
    mensajes_sala = dame_mensajes_introducir_mano_sala(TP);
    TP->add_timed_property("introducir_mano-" + __FILE__, 1, sizeof(mensajes_pj) * 5 + 1);

    TP->bloquear_accion("Te encuentras en una situación  bastante difícil y no puedes hacer más que rezar por tu vida.\n", sizeof(mensajes_pj) * 5 + 1);

    /**
     * Encolamos cada mensaje con un call_out donde el tiempo se va incrementado
     * y es igual a i*5, es decir, cada cinco segundos.
     */
    for (int i = 0; i < sizeof(mensajes_pj); i++) {
        call_out(
            "mostrar_mensajes_introducir_mano",
            i * 5,
            TP,
            mensajes_pj[i],
            mensajes_sala[i]);
    }

    /**
     * Además también encolamos la función final que nos moverá a otra room
     */
    call_out("teletransporte", sizeof(mensajes_pj) * 5, TP);
    return 1;
}

void init() {
    ::init();

    anyadir_comando("introducir", "mano", "funcion_introducir_mano");
    fijar_cmd_visibilidad("introducir", "mano", 1);
}

int pasar(string direccion, object caminante) {
    return notify_fail("Intentas trepar por las paredes, pero resbalas y caes de bruces sobre el fango, tragando un poco de este. Deberás buscar otra forma de salir.\n");
}
