// 2021.03.17 [Vhurkul]: Laderas de Altyr
//13.07.2021 [Gholxien]: Añado salida a la cueva del lobo jefe

#include "../../../include/zona_cyr.h"

inherit "/std/bosque";

#define MISION_CAMPANYA_1 "altyr_principal"

void setup()
{
    add_exit(O, ROOMS_ALTYR "bosque_4", "bosque");
    add_exit(S, ROOMS_ALTYR "bosque_1", "bosque");
    
    add_exit(DE, ROOMS_ALTYR "agua_1");
    modify_exit(DE, ({"obvia", "puede_entrar_poza"}));
    modify_exit(DE, ({"funcion", "accion_entrar_poza"}));
    
	AUTOGENERAR_SALA;
}

void init()
{
    ::init();
    
    anyadir_comando("recoger", "agua", "cmd_recoger_agua");
    fijar_cmd_visibilidad("recoger", "agua", 
        !TP->dame_hito_mision_activa(MISION_CAMPANYA_1, "conversacion_alug_1") ||
        TP->dame_hito_mision_activa(MISION_CAMPANYA_1, "obtencion_muestra_agua_2")
    );

    add_action("observar_huellas", "inspeccionar");
}

int cmd_recoger_agua()
{
    if (!TP->dame_hito_mision_activa(MISION_CAMPANYA_1, "conversacion_shihon_1"))
        return 0;

    if (!TP->dame_hito_mision_activa(MISION_CAMPANYA_1, "acompañamiento_radkum"))
        return notify_fail("Sientes que alguien te observa desde el campamento cercano. Quizás "
            "deberías encontrar a Radkum, como te indicó Shihon, y que él te presente a los "
            "habitantes de la zona, antes de tocar su fuente de agua.\n");

    if (!TP->dame_hito_mision_activa(MISION_CAMPANYA_1, "conversacion_alug_1"))
        return notify_fail("Sientes que alguien te observa desde el campamento cercano. Quizás "
            "deberías presentarte y ofrecer tu ayuda al líder del asentamiento, antes de tocar "
            "su fuente de agua.\n");

    if (TP->dame_hito_mision_activa(MISION_CAMPANYA_1, "obtencion_muestra_agua_2"))
        return notify_fail("Ya recogiste el vial de agua de la poza que te pidió Shihon.\n");

    if (TP->dame_peleando())
        return notify_fail("Estás muy acelerad" + TP->dame_vocal() + " por el combate, debes "
            "calmarte primero.\n");

    if (!TP->dame_hito_mision_activa(MISION_CAMPANYA_1, "obtencion_muestra_agua_1"))
    {
        int cuantos = TP->dame_nivel_ponderado() / 6;
        
        if (cuantos < 2)
            cuantos = 2;

        tell_object(TP, "Al acercarte a la poza, escuchas gruñidos y pasos rápidos de cuadrúpedos "
            "a tus espaldas. Cuando te giras, una manada de lobos de pelaje muy pálido, casi "
            "plateado, te ha rodeado, arrinconándote. Observas al que, por lo que dijo Radkum, "
            "debe ser el líder y te fijas en que la baba que chorrea de su quijada tiene el "
            "mismo tono fosforescente que el agua. Con un aullido, ordena a su manada cobrarse "
            "tu vida, y se retira de nuevo a la espesura del bosque.\n");

        while (cuantos--)
        {
            object lobo = clone_object(NPCS_ALTYR "lobo");
            lobo->set_aggressive(3);
            lobo->move(TO);
        }

        TP->nuevo_hito_mision(MISION_CAMPANYA_1, "obtencion_muestra_agua_1", 0);
    }

    if (secure_present(NPCS_ALTYR "lobo", TO))
    {
        object lobos = filter(all_inventory(TO), (: real_filename($1) == NPCS_ALTYR "lobo" :));

        TP->accion_violenta();
        lobos->attack_ob(TP);

        tell_object(TP, CAP(query_multiple_short(lobos)) + " surgen del bosque y te atacan.\n");
        tell_accion(TO, "Cuando " + TP->QCN + " se acerca a la orilla del lago, " + 
            query_multiple_short(lobos) + " surgen del bosque y le atacan.\n", "", ({TP}), TP);

        return notify_fail("La emboscada de los lobos te impide llenar de agua el vial.\n");
    }

    tell_object(TP, "Tras librarte de los lobos, rellenas al fin tu vial con el agua de la Poza "
        "Plateada, como te pidió Shihon.\n");
    tell_accion(TO, TP->QCN + " toma algo de agua del lago y la guarda en un vial.\n", 
        "", ({TP}), TP);
    TP->nuevo_hito_mision(MISION_CAMPANYA_1, "obtencion_muestra_agua_2", 0);

    return 1;
}

int observar_huellas(string str) {
    if (str != "huellas")
        return 0;

    if (query_timed_property("huellas_observadas"))
        return notify_fail("No parece haber huellas recientes.\n");

    add_timed_property("huellas_observadas", 1, 1000);

    // Hay algun add_item("huellas") ?
    // modify_item("huellas", "Mientras observas las huellas, adviertes otras huellas que se alejan "
    //     "por la orilla fangosa de la poza plateada en dirección norte, y cuando alzas tu mirada "
    //     "descubres una salida al norte que te había pasado desapercibida.\n");

    tell_object(TP, "Observas las enormes huellas, y te percatas de que fueron impresas por un "
        "lobo. Luego, te das cuenta de que algunas de ellas se alejan en dirección sudeste, y "
        "cuando alzas la mirada ves una salida que te había pasado totalmente desapercibida.\n");

    tell_room(TO, TP->QCN + " observa unas huellas en el suelo. Cuando alza su mirada, descubre "
        "una salida camuflada entre los espesos pinos.\n", this_player());

    add_exit(SE, ROOMS_ALTYR "cuevalobo", "bosque");
    renew_exits();

    return 1;
}

int puede_entrar_poza(object pj)
{
    return 
        pj &&
        pj->dame_hito_mision_activa(MISION_CAMPANYA_1, "entregar_muestras") &&
        !pj->dame_hito_mision_activa(MISION_CAMPANYA_1, "obtencion_arganita");

}

int accion_entrar_poza(string salida, object pj, string msj)
{
    if (!puede_entrar_poza(pj))
        return notify_fail("Las aguas de la poza parecen peligrosas y no tienes ningún motivo "
            "para adentrarte en ellas.\n");

    return 1;
}
