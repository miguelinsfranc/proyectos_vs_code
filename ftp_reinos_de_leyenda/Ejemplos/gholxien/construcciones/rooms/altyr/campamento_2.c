// 2021.03.17 [Vhurkul]: Laderas de Altyr

//#include "../../../include/zona_cyr.h"
#include "/d/dendra/include/zona_cyr.h"
inherit "/std/outside";

void setup()
{
    add_exit(N, ROOMS_ALTYR "alug", "door", 4, 6);
    add_exit(O, ROOMS_ALTYR "campcentro", "path");
    add_exit(S, ROOMS_ALTYR "enfermos", "door", 4, 6);
modify_exit(S, ({
            "funcion",
            "pasar"
        }));

    AUTOGENERAR_SALA;

add_clone("/d/dendra/npcs/cyr/altyr/alug.c",1);

}

int pasar(string direccion, object caminante) {
    if (!caminante->dame_hito_mision_activa("altyr_principal", "conversacion_shihon_1"))
	return notify_fail("Intentas acceder a la yurta de los enfermos, pero un aullido proveniente del bosque te deja paralizad" + caminante->dame_vocal() + ".\n");

if (!caminante->dame_hito_mision_activa("altyr_principal", "obtencion_muestras_agua_lobos"))
return notify_fail("Das un par de pasos en dirección a la yurta de los enfermos, pero en el último momento decides respetar los deseos de Alug y te diriges hacia otro lugar.\n");

if (!caminante->dame_hito_mision_activa("altyr_principal", "obtencion_muestra_enfermos"))
return notify_fail("Ya tienes la muestra de los enfermos que necesitabas, así que decides no entrar para no cansarlos.\n");

if (caminante->dame_hito_mision_activa("altyr_principal", "permiso_alug"))
return 1;
}
