//12.07.2021 [Gholxien]: Cueva del jefe lobo del bosque de Altyr
#include "../../../include/zona_cyr.h"
inherit "/std/bosque";

void setup()
{
add_exit(S, ROOMS_ALTYR "poza", "bosque");
      modify_exit(S,({"mensaje",({"$N se adentra entre los pinos que taponan la salida $T.","Los pinos que hay en $F se mueven con fuerza, y $N aparece renqueando entre ellos y con ramitas por todo su cuerpo."})}));

add_clone("/w/gholxien/lobo_jefe",1);

AUTOGENERAR_SALA;
}
