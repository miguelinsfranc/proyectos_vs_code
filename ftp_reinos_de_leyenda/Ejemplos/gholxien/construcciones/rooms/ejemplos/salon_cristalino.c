/*
	Durmok 11/07/2003
	Torre Cristalina
*/

inherit "/std/room";
#include "path.h"
void setup()
{

  set_short("Salón de la Torre %^CYAN%^BOLD%^Cristalina%^RESET%^ (En ruinas)");
  set_long("Te encuentras en lo que antaño fué un amplio salón circular que abarcaba la base completa "
          "de la pequeña torre. Haces de luz se filtran a través de las agrietadas paredes, al igual que el silbante viento del exterior "
          "que parece rasgar cada vez más la ajada alfombra que recubre el suelo de la torre y que se adivina entre los restos de roca de cristal que inundan la estancia "
          "En la parte norte hay un portal rodeado por una serie de antorchas mágicas, que parecen haberse salvado de la ruina.\n En la parte superior del mismo hay inscritas una serie de runas marcadas en fuego.\n");
 set_light(70);

  add_item(({"antorcha", "antorchas"}), "Antochas que cuelgan de las paredes, rodeando "
     "la habitacion.\n");
      
  add_item(({"runas"}), "Habeis equivocado vuestros pasos.\nLees las runas.\n");
  add_item(({"portal"}), "Es un portal mágico de bordes dorados similar a un espejo.\n");

 add_exit("fuera","/d/dendra/rooms/caminos/bre-ysa/entrada_torre.c", "path");
}
void init()
{
  ::init();
  add_action("do_atravesar",({"atravesar","entrar","penetrar","traspasar"}));
}

int do_atravesar(string str)
{
  string gremio= TP->dame_nombre_clase();
  
  if(str != "portal" && str != "el portal" && str != "en el portal")
  { notify_fail("¿Atravesar qué?\n"); return 0; }
  else
  {
   if(gremio != "Mago Rúnico")
    { notify_fail("No te atreves a atravesar un Portal de tanto poder.\n"); return 0; }
    else
    {
      tell_object(TP,"Comienzas a penetrar en el Portal mientras este se ilumina y despide unos "
                    "destellos luminosos.\n\nTu cuerpo comienza a pesar menos y la luz se vuelve mas intensa.\n");
      tell_room(TO,TP->query_cap_name()+" comienza a atravesar el Portal mientras este se ilumina y despide "
                   "unos destellos luminosos.\n\n El cuerpo de "+TP->query_cap_name()+" comienza a difuminarse "
                   "y la luz se vuelve más intensa.\n",TP);
      set_light(80);
      call_out("do_abrirse",5);
      return 1;
    }
  }
}
int do_abrirse()
{ 
   tell_room(TO,"Una neblina azulada proveniente del Portal comienza a envolver la habitación.\n"); 
   set_light(150);
   call_out("do_abrirse2",3);
   return 1; 
}
int do_abrirse2()
{
        tell_object (TP,"\nTras unos espasmos descubres que todo ha terminado.\nAbres poco a poco "
                    "los ojos y descubres que te encuentras en otra estancia.\n");
        tell_room(TO,"\nDe pronto la luz vuelve a su estado original y contemplas que el Portal "
                     "está vacio.\n",TP);
        
       TP->move("/d/naggrung/salas/catedral/salon.c","Traspasa el Portal Mágico");
       set_light (70);
}
