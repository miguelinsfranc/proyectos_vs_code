/*
Rutseg 10-IV-2003
Quitando bugs, Ember
Aribeth: Cosa rara, cambio la joya que he visto que estaba como obsidiana por azabache, la obsidina no existe en baseobs, sin embargo
 me dice Zirskit que le esta room le dio un azabache hace 3 días y no se como si en código sale obsidiana.
Rutseg 20-I-2010
 -Bueno, deshaciendo el desagisado que tenía montado :P ahora no dará los problemas que daba al acabar la acción de bloqueo
 -Reparando un error de mapeado bajo el puente
*/
// Isham 29/11/2014 - Corregido un error que hacia que el huron hiciese danyo incluso si se tenia algun hechizo lanzado como Piel de piedra.
// 26/02/2015 - Modificado para que de la gema con entregar_objeto() en vez de moverlo directamente al jugador.

#include <tiempo.h>
#include "/d/orgoth/path.h"
inherit "/std/bosque.c";

int dame_rio() { return 1; } //Pa que puedan pescar y demás.

void setup()
{
    fijar_bosque(0);
    set_short("%^BOLD%^BLUE%^Vado sobre el Cuivinien%^RESET%^");
    if(CLIMA->query_estacion()==2)//Verano
    {
        set_long("Las aguas del río pasan muy bajas en esta época del año, "
          "lo cual posibilitan el uso del vado para pasar al otro lado del "
          "río Cuivinien.\nHay una pequeña zona rocosa en este lado del río.\n");
        add_exit("sudoeste",EARMEN+"vado_22","path");
    }
    else
    {
        set_long("Las aguas del río van muy altas en esta época del año "
          "y resulta imposible atravesar el río a través del vado.\nHay una pequeña "
          "zona rocosa en este lado del río.\n");
    }
    set_light(100);

    add_item(({"rocosa","roca"}),"Roca granítica sobre la que se ha construido la base "
      "noreste del puente. Puedes ver entre las rocas lo que parece una pequeña madriguera "
      "y marcas del paso de algún roedor o similar.");
    add_item(({"marca","marcas"}),"Restos de excrementos de algún pequeño animal, cáscaras "
      "de bellotas y algo de hierba chafada.");
    add_item("madriguera","Un pequeño agujero en la roca parece servir de refugio "
      "a algún pequeño roedor. El agujero es tan pequeño que seguramente no podrías ni "
      "meter el brazo dentro.");

    add_exit("arriba",EARMEN+"ea_23","path");
}

void init()
{
    ::init();
    anyadir_comando("meter", " <texto'mano|brazo'> en la madriguera", "meter_brazo");
}

void desmorder(object personaje,object huron)
{
    if(!personaje)
    {
        remove_static_property("metido");
        add_static_property("gema",1);
        return;
    }
    if(!huron)
    {
        //Intenta encontrarlo por aquí
        huron= present("huron",this_object());
        if(!huron)
        {
            tell_object(personaje,"Logras sacar el brazo fuera de la madriguera.\n");
            tell_accion(this_object(),personaje->query_short()+" saca el brazo fuera de la "
              "madriguera.\n","Oyes el ruído de un forcejeo.\n",({personaje}),0);
            personaje->quitar_bloqueo();
            remove_static_property("metido");
            add_static_property("gema",1);
            return;
        }
    }
    tell_object(personaje,"Logras sacar con esfuerzo el brazo fuera de la madriguera y un "
      +huron->query_short()+" sale enganchado a tu mano, cae al suelo y empieza a atacarte.\n");
    tell_accion(this_object(),personaje->query_short()+" saca con esfuerzo el brazo fuera de la "
      "madriguera y un "+huron->query_short()+" sale enganchado de su mano, cae al suelo y "
      "empieza a atacarle.\n","Oyes el ruído de un forcejeo.\n",({personaje}),0);
    huron->move(this_object());
    huron->attack_ob(personaje);
    personaje->quitar_bloqueo();
    remove_static_property("metido");
    add_static_property("gema",1);
}

int mordido(object personaje,object huron)
{
    if(!personaje)
        return 0;
    if(!huron || personaje->dame_muerto())
    {
        desmorder(personaje,0);
        return 0;
    }
    if(personaje->query_str()+random(100)>70)
    {
        desmorder(personaje,huron);
        return 1;
    }
    tell_object(personaje,huron->query_short()+" te tiene atrapad"+this_player()->query_vocal()+" y no te deja "
      "moverte.\n");
    personaje->ajustar_pvs(-roll(5,10),huron);
    return 0;
}

int meter_brazo(string tx)
{
    object huron= query_static_property("metido");

    if(huron && !huron->dame_bloqueo())
    {
        //Si el personaje que metió el brazo ya no está bloqueado, llamamos a la rutina de desbloquear:
        desmorder(this_player(),0);
    }

    if( tx == "mano" )
    {
        if(huron= query_static_property("metido")){
            tell_object(this_player(),"No puedes meter la mano en la madriguera ya que "+huron->query_short()+" tiene dentro su brazo.\n");
            return 1;
        }
        tell_accion(this_object(),this_player()->query_short()+" introduce la mano dentro de una pequeña madriguera.\n","",({this_player()}),this_player());
        tell_object(this_player(),"Metes la mano dentro de la madriguera, pero no puedes llegar muy lejos, tal vez metiendo todo el brazo...\n");
        return 1;
    }

    if( tx != "brazo" )
        return 0;
    if(huron= query_static_property("metido")){
        tell_object(this_player(),"No puedes meter el brazo en la madriguera ya que "+huron->query_short()+" tiene dentro el suyo.\n");
        return 1;
    }

    if(query_static_property("gema") && ! TP->dame_bloqueo_combate(TO, "azabache") )
    {
        tell_accion(this_object(),this_player()->query_short()+" mete el brazo dentro de una pequeña madriguera "
          "y saca lo que parece una joya.\n","",({this_player()}),this_player());
        tell_object(this_player(),"Metes el brazo dentro de la madriguera y tocas lo que parece un pequeño objeto "
          "frío y cristalino. Al sacarlo fuera observas que se trata de una gema.\n");
        add_static_property("gema_cogida",1);
        remove_static_property("gema");

        TP->entregar_objeto( BJOYAS+"azabache.c", "", 1);
        TP->nuevo_bloqueo_combate(TO, 60 * 60 * 30, 1, 0, "azabache");
        return 1;
    }

    if(query_static_property("gema") && TP->dame_bloqueo_combate(TO, "azabache") )
    {
        tell_accion(this_object(),this_player()->query_short()+" mete el brazo dentro de una pequeña madriguera "
          "y saca una piedra.\n","",({this_player()}),this_player());
        tell_object(this_player(),"Metes el brazo dentro de la madriguera y tocas lo que parece un objeto redondo. Al sacarlo fuera observas que se trata de una piedra vulgar.\n");
        add_static_property("gema_cogida",1);
        remove_static_property("gema");

        TP->entregar_objeto( BPROYECTILES+"piedra.c", "", 1);
        return 1;
    }
    
    if(random(6) || query_static_property("gema_cogida"))
    {
        tell_accion(this_object(),this_player()->query_short()+" mete el brazo dentro de una pequeña madriguera.\n",
          "",({this_player()}),this_player());
        tell_object(this_player(),"Metes el brazo dentro de la madriguera, pero parece estar vacía.\n");
        return 1;
    }
    huron= clone_object(BNPCS+"huron");
    huron->move(this_object());
    if ( TP->dame_proteccion_corporal() != 1 )
    {
        tell_object(this_player(),"Metes el brazo dentro de la madriguera y tocas con tu mano un pelaje suave que "
          "empieza a revolverse.\n¡Sientes como algo te muerde frenéticamente en la mano y empiezas a chillar de dolor!\n");
        tell_accion(this_object(),this_player()->query_short()+" mete el brazo dentro de una pequeña madriguera. Acto seguido "
          "empieza a chillar frenéticamente e intentar sacar el brazo sin éxito.\n","Oyes el ruído de un forcejeo.\n",({this_player()}),this_player());
    }
    else
    {
        tell_object( TP, "Metes el brazo dentro de  la madriguera y tocas con tu mano un pelaje suave que empieza a revolverse.\nSientes como algo te muerde frenéticamente la mano sin conseguir causarte ningún daño.\n");
        tell_accion(this_object(),this_player()->query_short()+" mete el brazo dentro de una pequeña madriguera y al cabo de un momento intenta sacar el brazo sin éxito.\n","Oyes el ruído de un forcejeo.\n",({this_player()}),this_player());
    }
    TP->unarmed_damage(-roll(5, 20), huron->dame_ob_estilo(), huron);
    this_player()->attack_by(huron);
    this_player()->accion_violenta();
    this_player()->bloquear_accion((:mordido($(this_player()),$(huron)):),120);
    add_static_property("metido",this_player());
    return 1;
}
