// Satyr 26/03/2021 01:44

inherit "/std/room.c";

void setup()
{
    set_short("Sala que muestra contenidos presentes");
    set_long(
        "Una sala que muestra contenidos en la room sin clonar objetos." +
        ">> NO << clonar objetos para este tipo de cosas. Esta es la forma " +
        "correcta de hacerlo.\n");

    fijar_luz(40);
    add_item(
        "estatua",
        "Una estatua de Satyr hecha de barquillo de chocolate y rellena de " +
            "crema de avellana.");
}

varargs string query_contents(string tx, object *ob)
{
    if (!tx) {
        tx = "";
    }

    tx += "Una magnífica estatua de Satyr.\n";

    return ::query_contents(tx, ob);
}
