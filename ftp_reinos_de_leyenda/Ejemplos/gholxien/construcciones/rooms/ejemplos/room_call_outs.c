inherit "/std/room.c";

void setup()
{
    set_short("Room con call_out");
    set_long("Un ejemplo de room. Escribe \"viajar\" para activar el viaje con "
             "call_out.\n");
}
/**
 * Función auxiliar que devuelve los mensajes de viaje que recibe un jugador.
 *
 * Cada elemento del array es el mensaje de ese turno.
 */
string *dame_mensajes_viajar_jugador(object pj)
{
    return ({
        "Te preparas para viajar.",
        "Este es tu segundo mensaje de viaje.",
        "Tu tercer mensaje de viaje.",
        "Tu cuarto mensaje de viaje.",
    });
}
/**
 * Función auxiliar que devuelve los mensajes de viaje que recibe la sala desde
 * la que se viaja.
 *
 * Cada elemento del array es el mensaje de ese turno.
 */
string *dame_mensajes_viajar_sala(object pj)
{
    return ({
        pj->query_cap_name() + " empieza a viajar.",
        pj->query_cap_name() + " sigue viajando (2).",
        pj->query_cap_name() + " sigue viajando (3).",
        pj->query_cap_name() + " sigue viajando (4).",
    });
}
/**
 * Función auxiliar que muestra a un jugador y a la sala los mensajes del viaje.
 *
 * Se usa dentro del call_out de más abajo.
 */
void mostrar_mensajes_viajar(object pj, string mensaje, string mensaje_sala)
{
    if (!pj || environment(pj) != TO) {
        return;
    }

    tell_object(pj, mensaje + "\n");
    tell_accion(TO, mensaje_sala + "\n", "", ({pj}), pj);
}
/**
 * Función auxiliar que se llama al terminar el viaje
 */
void teletransporte(object pj)
{
    if (!pj || environment(pj) != TO) {
        return;
    }

    if (pj->move("/w/comun.c")) {
        tell_object(pj, "Ocurrió un error al moverte.\n");
        return;
    }

    tell_object(pj, "Terminas tu viaje y apareces en otro sitio.\n");
    tell_accion(
        TO,
        pj->query_cap_name() + " se va hacia otro sitio.\n",
        "",
        ({pj}),
        pj);
    pj->mirar();
}
/**
 * Función activada con el anyadir_comando
 */
int funcion_viajar()
{
    string *mensajes_pj, *mensajes_sala;

    /**
     * Esto es para evitar que se pueda poner "viajar" varias veces.
     */
    if (TP->query_timed_property("viaje-" + __FILE__)) {
        return notify_fail("¡Ya estás viajando!\n");
    }

    /**
     * Obtenemos mensajes
     */
    mensajes_pj   = dame_mensajes_viajar_jugador(TP);
    mensajes_sala = dame_mensajes_viajar_sala(TP);
    TP->add_timed_property("viaje-" + __FILE__, 1, sizeof(mensajes_pj) * 3);

    /**
     * Encolamos cada mensaje con un call_out donde el tiempo se va incrementado
     * y es igual a i*2, es decir, cada dos segundos.
     */
    for (int i = 0; i < sizeof(mensajes_pj); i++) {
        call_out(
            "mostrar_mensajes_viajar",
            i * 2,
            TP,
            mensajes_pj[i],
            mensajes_sala[i]);
    }

    /**
     * Además también encolamos la función final que nos moverá a otra room
     */
    call_out("teletransporte", sizeof(mensajes_pj) * 2, TP);
    return 1;
}

void init()
{
    ::init();

    anyadir_comando("viajar", 0, "funcion_viajar");
}
