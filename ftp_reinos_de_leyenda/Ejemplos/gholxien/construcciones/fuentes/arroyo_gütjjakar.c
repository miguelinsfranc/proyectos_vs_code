//06/02/2022 [Gholxien]: Fuente Gütjjakar.
#include <baseobs_path.h>
inherit OBJETOS_ROOMS + "fuente.c";

void setup()
{
    ::setup();

    set_short("Arroyo subterráneo");
    generar_alias_y_plurales();

    set_long("Entre las grietas de la roca y a través del musgo se filtra gota a gota esta vertiente de pristina agua mineral que proviene del exterior. La corriente se acumula en un pozo de aproximadamente un "
"metro de profundidad donde el líquido se estanca brevemente para luego seguir su proceso de oxigenación a través de pequeñas cascadas.\n"
" Estas convergen en una única corriente que desaparece por una "
"pequeña fisura en el fondo de la piedra produciendo un relajante sonido que se mezcla con el eco del goteo que alimenta al arroyo. Está naturalmente iluminado por líquenes micobiontes en forma de "
"pequeñas setas que brillan tenuemente con un tono azul verdoso produciendo una armoniosa atmósfera de tranquilidad en la cueva.\n");
}
