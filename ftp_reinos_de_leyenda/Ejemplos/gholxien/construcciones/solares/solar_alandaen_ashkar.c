#include "/d/anduar/path.h"
inherit "/std/solar.c";

void setup()
{
    add_exit(O, "/d/anduar/rooms/alandaen/viviendas/vivienda_3", "standard");

    set_short("Solar en construcción");
    set_long("Un solar en construcción.\n");
    fijar_luz(50);

    fijar_ciudadania("anduar");
    fijar_dimensiones(1, 0, 2, 2);
    crear_cartel("cartel");
}
