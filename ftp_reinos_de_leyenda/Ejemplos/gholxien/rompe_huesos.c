// Nebhur Enero 2006
// Luz del alba, que tiene un 5% de provocar una incapacidad tipo contusion al golpear.
// Satyr 21.10.2015 -- Combate v2 y revisión general del fichero
#include <combate.h>
inherit "/obj/arma";

void setup()
{
    fijar_arma_base("luz del alba");
    set_name("rompehuesos");
    
    add_alias(({"rompe","rompehuesos"}));
    add_plural("rompehuesos");
    set_short("%^BOLD%^%^BLACK%^Rompe%^WHITE%^Huesos%^RESET%^");
set_main_plural("%^BOLD%^%^BLACK%^Rompe%^WHITE%^Huesos%^RESET%^");
    
    set_long("Esta arma, se parece a simple vista a una luz del alba, pero su mango está "
    "reforzado con chapas de metal. Otro detalle, es que su cadena es más larga, "
    "lo que le permite la posibilidad de alcanzar al contrario con más facilidad, "
    "a costa de perder en la defensa. Pero lo que realmente hace temible su gran "
    "bola, la cual le da nombre, es verla en manos de un Gnoll. Esta gran bola "
    "le confiere el poder de convertir los huesos en polvo.\n");

    // Es un arma ofensiva, con lo cual aporta más a la BO y pierde en la BP
    fijar_BO(dame_BO() + 15);
    fijar_BP(dame_BP() - 10);
    fijar_encantamiento(10);
    fijar_valor(14000);

    //ajustar_danyo_minimo("tabla", 40);
    nuevo_efecto_basico("matamonstruos", 40);
    
    nuevo_efecto(
        "Contusión",
        "tabla",
        "Este ataque tendrá un % de causar heridas de tipo 'contusión' "
            "igual a tu nivel de maestría. Las heridas de contusión reducirán la BO/BE/BP de "
            "tus enemigos en -40/-10/-30 y tienen probabilidad de aturdir momentáneamente "
            "a sus víctimas durante su duración.",
        ({_TRIGGER_IMPACTO_T}),
        (: $2->dame_maestria(TO) > random(100) :),
        "heridas/contusion",
        10,
        180
    );

    nuevo_efecto_basico("palo_grande", 50);
}
