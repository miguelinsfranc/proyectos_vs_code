// Satyr 2006, hasta los huevos de npcs del año de la pera -- Heraldo de la Inquisición de Seldar
/* 
    Satyr 2014 -- El satyr del 2014 también está harto de los NPCs de Satyr del 2006, que cosas.
    - Refactorizo, adapto a bases, ambientación y habilito las quests de priis/darin
    - Aún le queda otra refactorización para misiones
*/
#include <ciudadanias.h>
#include <baseobs_path.h>
#include "/d/dendra/path.h"
inherit NPCS_INQUISICION + "base_guardia_inquisicion.c";
inherit "/obj/pnjs/conversador.c";
void preparar_descripciones()
{
	set_name("rakmet");
	
	set_short("Rakmet, el Heraldo de la Inquisición");
	set_main_plural("imágenes de Rakmet, Heraldo de la Inquisición");
	
	add_alias(({"heraldo", "inquisicion", "inquisición", "heraldo de la inquisición", "heraldo de la inquisicion"}));
	add_plural(({"heraldos", "inquisicion", "inquisición", "heraldos de la inquisicion", "heraldos de la inquisición"}));

	set_long(	
        "Un hombre de piel cobriza que está ya en el otoño de su vida y no le quedan ya pelos que peinar. "
        "Rakmet fue, durante mucho tiempo, el máximo representante de la Inquisición en Galador. El mismísimo "
        "Archiprelado Vali eligió personalmente a Rakmet por su excelente reputación y su actitud moderada, "
        "aunque \"moderado\" en la Inquisición de Seldar es un adjetivo muy raramente utilizado.\n\n"
        "  A mediados de la 3ª era, el poder de la Inquisición en Galador fue mangoneado por el Sy-Gobernador "
        "y Comandante del Ejército de Dendra Aregarn y la familia Nobiliar Sengrot y un conflicto civil asoló las "
        "calles de la fortaleza del desierto.\n\n"
        " Tras el exilio de la mayor parte de la familia Nobiliar, Keltur Vorgash ordenó enviar a otro representante "
        "a Galador para tomar el control y, esta vez, envió a alguien no tan moderado, envió a Torquemada.\n\n"
        " Con Torquemada al mando, la figura de Rakmet ha pasado a un segundo plano en el panorama político de "
        "la Inquisición en Galador y éste ha cambiado la armadura completa por la túnica; sin embargo, muchos "
        "hermanos-inquisidores, hermanos-altos Inquisidores y varios regentes aún confían en su sabiduría y su "
        "bien hacer.\n\n"
    );
}
void preparar_rol()
{
    ::preparar_rol();
    
    fijar_estatus(NOMBRE_ESTATUS, HEROE);
    fijar_genero(1);
}
void preparar_atributos()
{
    fijar_clase("seldar");
	set_random_stats(19, 21);
    fijar_carac("sab", 28);
	fijar_extrema(100);
    ajustar_bo(60);
	adjust_tmp_damage_bon(60);
    fijar_nivel(50);
    fijar_pvs_max(60000);
    
    add_static_property("sin miedo"       ,  1);
	add_static_property("no_desarmar"     ,  1);
	add_static_property("ts-petrificacion", 30);
}
void preparar_chat()
{
	load_chat(15,({
        "'Aquí la única ley es Seldar.",
        "'La misión es peligrosa, Velminard, ¡y necesitamos ese templo!",
        "'¿Crees que deberíamos enviar al actual regente a realizar el trabajo?",
        ":contempla un relieve de un mapa de Naggrung en una de mas mesas de la sala."
    }));
			
	load_a_chat(15,({
        "'¡Muere en el infierno de Seldar!",
        "'¡Que el Emisario de las Serpientes te arranque los ojos!"
    }));
}
void preparar_ataques()
{
    add_attack_spell(80, "golpecertero", 3);
}
void preparar_fe_inquisicion()
{
    fijar_fe(220);
}
void preparar_equipo()
{
	add_clone(ARMADURAS   + "sotana_seldar_negra.c", 1);
    add_clone(BARMADURAS  + "pantalones.c"         , 1);
    add_clone(BARMADURAS  + "sandalias.c"          , 1);
    
    poner_inquisicion();
    poner_iglesia();
    init_equip();
    queue_action("encapucharse");
    
    fijar_objetos_morir(({"tesoro", "arma"    , ({6, 8}) }));
    fijar_objetos_morir(({"tesoro", "armadura", ({6, 8}) }));
}

/**
    THERE BE DRAGONS FAR AHEAD
    
    CODIGO VIEJO QUE AUN NO ARREGLE
*/
int habla_pj(object pj, string str)
{
    return pj ? pj->do_say(str, 0) : 0;
}

void dialogo_anillo(object pj)
{
	call_out((:do_say("Se trata de un emblema de identificación entre los miembros del Culto al Lujo.",0):),2);
	call_out((:do_say("A primera vista parece un anillo normal pero...",0):),4);
	call_out((:do_emote("saca un anillo del bolsillo y empieza a moverlo hasta que refleja un pentágono invertido."):),6);
	call_out((:do_say("La influencia de los espías es alarmante y también la facilidad con la que se infiltran.",0):),8);
	call_out((:do_say("Aquí pasa algo muy grave.",0):),9);
	call_out((:do_say("El culto al lujo ha de estar moviendo muchos hilos, es tu labor la de buscarlos.",0):),11);
	call_out((:do_say("Tienes que encontrar a alguno de sus aliados en la población o si no los tienen, la manera en la que se infiltran.",9):),12);
	call_out((:do_say("Recurre a todos los medios que tengas. Interroga, amenaza, golpea, mata... ¡que tu pulso no tiemble!",10):),14);
	call_out((:do_say("Esta es una misión muy importante.",0):),15);
	call_out((:do_say("Pon tus cinco sentidos en ella, ¡desconfía de todo el mundo!",0):),17);
	call_out((:do_say("Vaya, me da la impresión de haber tenido esta conversación contigo antes...",0):),18);
}
void busca_lujo_fin(object pj)
{
	if (environment()!=environment(pj))
		return;
	
	if (!pj->query_property("termine_culto_lujo"))
	{
		call_out((:do_say("¿Aún no has encontrado nada?",0):),1);	
		call_out((:do_say("Quizás me precipité enviándote a tí a la búsqueda de pistas del culto al lujo.",0):),2);
		call_out((:do_emote("suspira."):),9);
		call_out((:do_say("Sí, Velminard. Te alistas a la entrada. Veo que cada día eres mas observador, ¿eh?",0):),10);
		call_out((:do_say("Puedes dejar la misión si así lo deseas.",0):),15);	
		call_out((:do_say("Si no te ves capaz de encontrar al lujo o a uno de sus subordinados encontraré a alguien mejor.",0):),16);
		call_out((:do_say("No es una tarea fácil, teniendo en cuenta que podrías tener a uno de ellos delante delante de tus ojos...",0):),18);
		call_out((:do_emote("frunce el ceño y te examina meticulosamente, como si sospechase de tí."):),19);
		return;
	}
}	
void preparar_dialogos() 
{
	nuevo_dialogo("nombre",({
		"'Soy Rakmet, antiguo Heraldo de la Inquisición en Galador.",
        "'Fui elegido por el mismísimo Archiprelado de Seldar, Vali, para desempeñar semejante menester.",
		"'Llevo aquí casi más tiempo que estas paredes, por lo que debes ser nuevo si no me conoces."
    }));
	nuevo_dialogo("antiguo",({
		"'Antiguo, sí. El Elegido de la Orden de la Tortura, Torquemada, ha tomado mi puesto como heraldo.",
        "'Es un honor para mí y para la sede de la Inquisición gozar de la presencia del gran torturador en la ciudad.",
        }),
        "nombre"
    );
	nuevo_dialogo("sede",({
		"'Claro, esto es solo una sede.",
		"'La verdadera inquisición, así como sus ordenes, están en el corazón del Imperoi."}),
        "antiguo"
    );
    nuevo_dialogo("archiprelado", ({
        "'El archiprelado Vali es el máximo poder religoso del Imperio personificado y su autoridad solo está "
        "por debajo de la del mismísimo Emperador.",
        "'Aunque de todos es sabido que Keltur Vorgash, sabiamente, escucha todos los consejos de Vali.",
        "'Su influencia es tal que se ha ganado varios puestos de autoridad en el Imperio: Sumo Archiprelado de Seldar, "
        "Maestro Ritualista de la orden de D'hara y Elegido de la orden Religiosa de nuestra organizacion."
        }),
    );
    nuevo_dialogo("elegido", ({ 
        "'Los elegidos son los altos mandos de la Inquisición.",
        "'De ellos depende el bienestar e integridad de nuestra nación.",
        "'El mero hecho de que tengamos a un elegido en Galador debería henchirnos de orgullo.",
        "'Antaño eran 5, pero tras el asesinato de Juddih, las cosas se han puesto algo tensas."
        }),
        "archiprelado"
    );
	nuevo_dialogo("organizacion",({
        "'La gloriosa Inquisición de Seldar.",
		"'Formada para eliminar herejías, buscar blasfemos, ajusticiar criminales, velar por la paz y la segurdad del reino y como no, "
		"controlar la adecuada devoción a Seldar de la población.",
		"'Tus labores como inquisidor son todas esas, así como desconfiar hasta de tu madre.",
		"'El enemigo podría estar en cualquier parte, no dejes que ningún tipo de lazo ciegue tus ojos."}),
        "archiprelado"
    );
	nuevo_dialogo("paz",({
		"'Sí, la paz.",
		"'Nunca olvides eso, una de nuestras misiones es traer la paz a los reinos.",
		"'De la manera más sanguinaria que haga falta si con eso conseguimos nuestra ansiada paz."}),
        "archiprelado"
    );
	nuevo_dialogo("misiones",({
		"'El archiprelado siempre necesita que la Inquisición se mueva y yo aún soy el portador de sus mensajes.",
		"'Actualmente hay muchas misiones pendientes que yo asigno a los Inquisidores que considero oportunos.",
		"'¿Quieres ver cuales hay actualmente disponibles?"}),
        "archiprelado"
    );
	nuevo_dialogo("disponibles",({
		"'El archiprelado me ha dado estas misiones para repartir entre los miembros de la Inquisición.",
		"'Si quieres saber más de alguna, sólo pregúntalo.",
		"'El asesinato de Priis I.",
		"'La muerte del rey Darin.",
		"'El Culto al Lujo.",
		"'Si crees que has terminado alguna, ven aquí y %^BOLD%^presenta%^RESET%^ los resultados."}),
        "misiones"
    );
	nuevo_dialogo("priis",({
		":sonrie.",
		"'El viejo rey Elder era un hueso duro de roer, he de reconocerlo.",
        "'Sin embargo, la Reina Priis I de Takome no es más que una simple jovencita esperando a que "
        "los grandes nobles de Takome decidan con qué gran Lord han de casarla.",
		"'Es hora de enviarles a los Takomitas un pequeño regalo.",
		"'Es hora de acabar con su actual monarca.",
        "'Reune a un Ejército, Inquisidor.",
        "'Acaba con Priis y traenos su cabeza."
		}),
        "disponibles"
    );
	nuevo_dialogo("darin",({
		"'El consejo de Kheleb ha osado involucrarse en más de una ocasión en nuestro camino.",
		"'Necesitamos que alguien les enseñe una lección y nada mejor que pasar tras sus dos clanes mayoritarios y matar a su patético rey.",
		"'¡Traeme su cabeza y te aseguro que tu nombre nunca se olvidará en la inquisición!"}),
        "disponibles"
    );
	nuevo_dialogo("culto",({
		":suspira profundamente.",
		"'Puede que no conozcas mucho de esto, pero hay un culto que se mueve bajo nuestras propias ciudades.",
		"'Un culto que adora los excesos y los vicios ocultos, el culto al lujo.",
		"'No conocemos mucho de sus sectarios, pues ni nuestros más retorcidos torturadores han logrado sonsacarles información.",
		"'No voy a encomendarte una misión complicada, pero estamos siendo observados continuamente por sus espías.",
		"'¡Están en todas partes!, ¡en todos nuestros núcleos neurálgicos!",
		"'¡Tabernas, fronteras, torres!, ¡siempre hay alguna de esas ratas controlando nuestros movimientos!",
		"'¡A la mierda el actuar con sigilo!, ¡busca espías del Culto al Lujo y traeme sus máscaras!",
		"'¡Te recompensaré por cada uno que elimines!",
		"'Nada de amiguismos ni de torturas bondadosas, destruyelos.",
		"'Y ve con mucho cuidado, si se ven descubiertos huirán."}),
        "disponibles"
    );
	
	nuevo_dialogo("lujo"  , ({"busca_lujo_fin"}));
	nuevo_dialogo("anillo", ({"dialogo_anillo"}));
	prohibir("anillo","en_busca_del_lujo");
	prohibir("lujo","en_busca_del_lujo");
    
	habilitar_conversacion();
}

/*
 * Esta puta mierda está copiada y pegada, pronto lo pasaré a misiones, dont worry people.
 */
int presentar_resultados(object b)
{
	string nombre_log, tx_log;
    int puntitos, platinitos;
    string cargo; 
    
	if ( ! b )
		return notify_fail("¿Si no tienes nada que presentar, para qué lo intentas?\n");	
	
	if ( ! pertenece_inquisicion(TP) || TP->dame_ciudadania() != "dendra" ) {
        return notify_fail(query_short() + " enarca una ceja y te mira, extrañado, cuando te acercas "
            " a presentarle " + b->query_short() + ". Tu sentido de la sensatez te alarma de que, "
            "seguramente, un Inquisidor de Seldar no quiera tratar con alguien como tú un tema "
            "tan sensible como el que traes a su atención.\n"
        );
    }
    
    cargo = dame_cargo_inquisidor(TP) + " " + TP->dame_nombre_completo(3);
        
    switch(base_name(b)) 
    {            
        // Máscaras: inician misión y dan 100/20 platinos + 10/5 puntos de xp ( en función de lock )
        case ARMADURAS + "mascara_morada":            
            if ( b->query_property("propietario") != TP->query_name() ) {
                do_say("Esta máscara no la ha obtenido usted, " + cargo + ", pero un infiel menos es un infiel menos.", 0);
                do_say("No voy a recompensarle por el trabajo de otro, lamentablemente." , 0);
                
                tx_log = TP->query_cap_name() + " entrega una máscara que no es suya (" + b->query_propertY("propietario") + ")";
            }
            else {
                do_say("¡Un gran trabajo, "+cargo+"!", 0);
                do_say("¡Veo que has acabado con una de esas ratas!",0);
                do_say("¡Con más Inquisidores como tú estaríamos dominando el mundo ahora mismo!", 0);
                do_say("Aquí tienes tu recompensa, no es mucho, pero espero que te sirva para financiarte los gastos de tu búsqueda.", 0);
                
                puntitos   =  10 / (TP->query_timed_property("mate_espia_reciente") ? 2 : 1);
                platinitos = 100 / (TP->query_timed_property("mate_espia_reciente") ? 5 : 1);
                
                if ( ! TP->query_timed_property("mate_espia_reciente") ) {
                    TP->add_timed_property("mate_espia_reciente", 1, 60 * 60 * (random(2) + 5) );
                }
                
                tx_log = TP->query_cap_name() + " entrega una máscara morada y recibe " + puntitos + " ptos. y " + platinitos + " platinos.";
            }
            
            nombre_log = "mascaras_moradas";
        break;
        
        // Anillo: 25 puntitos e inicia la quest de alto inquisidor
        case ARMADURAS + "anillo_amatista":
            if (
                TP->query_property("en_busca_del_lujo") || 
                TP->query_property("mas_informacion")   ||
                TP->query_property("roba_a_torquemada")
                ) {
                return notify_fail("Lo que estás intentando presentar no tiene mérito alguno.\n");
            }
            
                            
            do_say("¿Dónde has encontrado esto, "+cargo+"?",0);
            call_out((:do_say("¿Sabes qué es este anillo?",0):),1);	
            call_out((:do_say("Se trata de un emblema de identificación entre los miembros del Culto al Lujo",0):),4);
            call_out((:do_say("A primera vista parece un anillo normal pero...",0):),7);
            call_out((:do_emote("te coge el anillo y empieza a moverlo hasta que refleja un pentágono invertido."):),10);
            call_out((:do_say("¿Lo ves? desconocemos que es ese icono o que representa, pero el Culto al Lujo suele usarlo mucho.",0):),14);
            call_out((:do_say("Voy a requisarte el anillo en el nombre de la Inquisición.",0):),18);
            call_out((:do_say("La influencia de los espías es alarmante y también la facilidad con la que se infiltran.",0):),22);
            call_out((:do_say("Aquí pasa algo muy grave.",0):),29);
            call_out((:do_say("El culto al lujo ha de estar moviendo muchos hilos, es tu labor la de buscarlos, "+$(cargo)+".",0):),35);
            call_out((:do_say("Tienes que encontrar a alguno de sus aliados en la población o si no los tienen, la manera en la que se infiltran.",9):),42);
            call_out((:do_say("Recurre a todos los medios que tengas. Interroga, amenaza, golpea, mata... ¡que tu pulso no tiemble!",10):),44);
            call_out((:do_say("Esta es una misión muy importante.",0):),50);
            call_out((:do_say("Pon tus cinco sentidos en ella, ¡desconfía de todo el mundo!",0):),56);

            nombre_log = "anillos_amatista";
            tx_log     = TP->query_cap_name() +" entrega un anillo amatista.";
            this_player()->add_property("en_busca_del_lujo", 1);                
            puntitos = 25;
        break;
            
        case DOCUMENTOS + "sobre":
            if (
                ! this_player()->query_property("en_busca_del_lujo") || 
                ! this_player()->query_property("mas_informacion")   ||
                ! this_player()->query_property("roba_a_torquemada")  
               ) {
                return notify_fail("Lo que estás intentando presentar no tiene mérito alguno.\n");
            }
            
            do_say("¿Qué es eso que tienes ahí, "+cargo+"?",0);
            call_out((:do_say("¿Un sobre?",0):),4);
            call_out((:do_say("Este símbolo...",0):),7);
            call_out((:do_say("No reconozco este símbolo, nunca antes lo había visto.",0):),11);
            call_out((:do_say("¿A quien puede pertenecer?.",0):),15);
            call_out("habla_pj",22,this_player(),"El infiltrado del lujo que encontré dijo que una figura enmascarada le había dado esto, no sabía nada más.");
            call_out((:do_emote("asiente levemente con la cabeza, dando a entender que comprende lo que dices."):),26);
            call_out((:do_say("Veo que ese desgraciado nisiquiera era parte plena del Culto.",0):),30);
            call_out((:do_say("Debemos excavar más hondo, ¡has de encontrar a alguien más importante dentro del culto!",0):),34);
            call_out((:do_say("¡Captúralo, interrógalo y posteriormente mátalo!",0):),39);
            call_out((:do_say("¡Averigua que es este símbolo!",0):),43);
            call_out((:do_say("¡Cuento contigo!, ¡no me falles!",0):),47);

            puntitos = 50;
            this_player()->add_property("mas_informacion",1);    
            
            nombre_log = "sobres";
            tx_log     = TP->query_cap_name() + " entrega un sobre.";
        break;
        
        case BCUERPOS + "parte_cabeza":
            if ( b->dame_duenyo() == "priis" || b->dame_duenyo() == "darin")
            {
                string apodo = b->dame_duenyo() == "priis" ? "esa fulana de Priis" : "el beodo Rey Enano";
                
                nombre_log = "cabezas";
                
                if ( b->dame_cortador() != TP->query_name() ) {
                    do_say("¡Por los demonios de Seldar!, ¡pero si es la cabeza de " + apodo + "!", 0);
                    do_say("Increíble hazaña, " + cargo + ", una lástima que no la hayáis cortado vos." , 0);
                    do_say("Me quedaré con la cabeza, pero no puedo daros nada a cambio.", 0);
                    
                    tx_log = sprintf("%s entrega cabeza de %s pero la corto %s.", 
                        TP->query_cap_name(), 
                        b->dame_duenyo(),
                        b->dame_cortador()
                    );
                }
                else {
                    do_say("¡Increíble, " + cargo + "!, ¡la cabeza de" + apodo + "!", 0);
                    do_say("Inquisidores como tú son justo lo que la organización necesita.", 0);
                    do_say("Esta hazaña pasará a los anales de la historia, al margen de que algún clérigo de "
                        "Eralie vaya a recomponer el espíritu de tu víctima.", 0);
                    do_say("Las arcas de la Inquisición aún me deben un par de favores, así que acepta este dinero, "
                        "te lo has ganado.", 0);
                    do_say("Y no te preocupes, me encargaré de que se haga saber tu hazaña.", 0);
                    
                    puntitos   = 150 / (TP->query_timed_property("cabeza_enemigo_inquisicion") ? 2 : 1);
                    platinitos = 350 / (TP->query_timed_property("cabeza_enemigo_inquisicion") ? 5 : 1);
                
                    if ( ! TP->query_timed_property("cabeza_enemigo_inquisicion") ) {
                        TP->add_timed_property("cabeza_enemigo_inquisicion", 1, 259200);
                    }
                    
                    tx_log = sprintf("%s entrega la cabeza de %s, llevándose %d platinos y %d puntos.",
                        TP->query_cap_name(), 
                        b->dame_duenyo(),
                        platinitos,
                        puntitos
                    );
                    
                    break;
                }
            }
        // Aquí no hay break, es intencional
        
        default:
            do_say(dame_cargo_inquisidor(TP) + " " + TP->dame_nombre_completo(), 0);
            do_say("¿Para qué demonios me trae " + b->query_short() + "?, ¿qué he de hacer con eso?", 0);
            
            return 1;
    }
    
    // Clean-up y recompensas
    if ( platinitos ) {
        PAGAR_DENDRA(TP, platinitos * 500);
    }
    
    if ( puntitos ) {
        PUNTOS_ORG_DENDRA(TP, "inquisicion", "puntos-misiones", puntitos);
    }
        
    b->dest_me();
    LOG_DENDRA("quests", "inquisicion_resumen"      , tx_log);
    LOG_DENDRA("quests", "inquisicion_" + nombre_log, tx_log);
	return 1;
}
	

// Interactua con el otro personaje de la sala
void event_person_say(object quien,string encabezado,string msj,string lenguaje,string *msjs)
{
	if (quien && quien->query_name() == "velminard")
	{
		switch(msj)
		{
			case "¡El templo de Ankhalas caerá si la Inquisición no nos ayuda!":
			case "¡La misión no puede esperar!, ¡debemos encontrar a Lord Ankhalas!":
				call_out((:do_say("Sabes que son tiempos difíciles, Velminard, ten paciencia.",0):),4);
			break;
			
			case "¡No es una situación que pueda esperar!":
				call_out((:do_say("¡Lo sé perfectamente Velminard!",0):),4);
				call_out((:do_say("¡Pero un desembarco frontal en Naggrung es demasiado arriesgado!",0):),5);
				call_out((:do_say("¡Sabes que no tenemos tantos hombres disponibles!",0):),6);
			break;			
			
			case "¡En Ankhalas llamamos a eso cobardía!":
				do_emote("golpea con su puño una mesa con una fuerza descomunal.");
				call_out((:do_say("¡No oses tachar a un inquisidor de cobarde!",0):),4);
				call_out((:do_say("¡La situación actual con Naggrung impide que la misión sea facil!",0):),5);
				call_out((:do_say("¡Enviaré a mis mejores hombres, pero tendrás que esperar!",0):),7);
				call_out((:do_say("¿No quieres que la misión sea un fracaso, verdad?",0):),9);
			break;
			
			case "Es sólo que me duele ver como nuestro territorio se hace más y más pequeño.":
				call_out((:do_say("No digas tonterías, Velminard.",0):),4);
				call_out((:do_say("Todo esto no es más que una prueba de fe de nuestro Señor.",0):),5);
				call_out((:do_say("Déjalo en manos de la inquisición.",0):),6);
			break;
		}	
		
	}
}


void init()
{
	::init();	
	if(TP) 
		TP->anyadir_comando(
			"presentar",
			"<objeto:todos:yo:primero'Objeto a presentar'>",TO,
			(:presentar_resultados:)
		);
}

void attack_by(object pj)
{
	object amigo = present("velminard",environment());
	
	if ( ! query_held_ob() || ! sizeof(query_held_ob()) ) {
		habilidad("martillo espiritual",this_object());
	}
	
	if (amigo && ! amigo->dame_protector() ) {
        amigo->fijar_protector(this_object());
        pj->attack_by(amigo);
    }
		
	::attack_by(pj);
}
