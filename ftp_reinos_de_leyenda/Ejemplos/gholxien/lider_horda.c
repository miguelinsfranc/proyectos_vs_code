//Nebhur feb-06  lider horda.
//Revisado por Dalion a 07-02-2010
//Kaitaka 02Feb2012 - les pongo que den algún tesoro.
//Eckol May14: Añado hito para la misión del puño de mando.
//Eckol 30May15: Cambio la manera de habilitar el dialogo "mision" para que no use un dialogo inexistente.
//Eckol Oct18: Rompehuesos en fijar objetos morir.
inherit "/obj/conversar_npc";
#include "/d/kheleb/npcs/bases/handler_goblins.c";

#include "/d/kheleb/path.h";
#include <ciudadanias.h>
#include <estatus.h>

#define BARRIDO "bloqueo-barrido"
#define APLASTAR "no_aplastar"
#define GC "golpecertero_reciente"
#define GC2 "gc_wait"
#define HORDA "hordeando"
#define ATAQUEDOBLE "bloqueo-luchador"

/*
void debug(string a)
{ tell_object(find_player("nebhur"), "CHU"+a+"\n"); }
*/

int puede_conversar_mision(object pj){
    return pj->query_static_property("puede_conversar_mision");
}

void setup ()
{
  set_name("lider");
  add_alias(({"lider","líder","gomtak"}));
  set_short("Gomtak, el líder de la Horda");    
  set_long("Es un gigante gnoll, de aspecto sucio y descuidado. Sus ojos parecen"
           " grabados en las cuencas oculares y su nariz es chata y torcida."
           " Su corpulencia no es muy grande para un gnoll, y notas que sus ,"
           " labios estan secos, quizá lleve días sin comer. Sus enormes manos, "
           " llevan grabadas, el recuerdo de numerosas horas como líder de hordas, "
           " y muchas batallas ganadas en nombre de su señor Glaiven.\n");

  fijar_raza("gnoll");
  fijar_ciudadania("mor_groddur",100);
  fijar_religion("gurthang");
  fijar_fe(150); //Lo muevo aquí, agrupa las funciones según definan rasgos generales, ataque, etc.
  fijar_clase("soldado");
  fijar_especializacion("goblinoide");
  fijar_genero(1);
  fijar_fue(25);
  fijar_extrema(100);
  fijar_des(18);
  fijar_con(18);
  fijar_int(15);
  fijar_sab(10);
  fijar_car(18);
  
  fijar_nivel(55);
  ajustar_bo(200);  //Perfecto también, el fijar_nivel se lo hubiera modificado si lo hubieras puesto antes


  adjust_tmp_damage_bon(100);
  fijar_pvs_max(18000);
  fijar_pe_max(500);


  fijar_alineamiento(20000);
  
  nuevo_lenguaje("negra",100);
  //Ember, te falta quitarle el dendrita, que lo llevan los npcs por defecto
  quitar_lenguaje("dendrita");
  //Ember y fijar el negra por defecto
  fijar_lenguaje_actual("negra"); 
  ajustar_maestria("luz del alba",100);

  load_a_chat(20,({"'¡Te voy a hacer picadillo!"}));
 
  add_clone(ARMAS+"rompe_huesos",1);
  add_clone(BESCUDOS+"escudo_corporal", 1);
  add_clone(BARMADURAS+"placas", 1);
  add_clone(BARMADURAS+"gran_yelmo", 1);
  add_clone(BARMADURAS+"brazalete", 1);
  add_clone(BARMADURAS+"brazal_izq.c", 1);
  add_clone(BARMADURAS+"grebas_metalicas", 1);
  add_clone(BARMADURAS+"botas_guerra", 1);
  add_clone(BARMADURAS+"cinturon", 1);

  fijar_objetos_morir(({"tesoro", "varios"  , ({3, 4}), 1 }));
  fijar_objetos_morir(({"tesoro", "arma"    , ({4, 5}), 1 }));
  fijar_objetos_morir(({"tesoro", "armadura", ({4, 5}), 1 }));
  fijar_objetos_morir(ARMAS+"rompe_huesos");

    
  add_loved("ciudadania","mor_groddur");
  set_aggressive(1);
    
  init_equip();

  nuevo_dialogo("nombre",({"'Zoy Gomtak, miembro de elite de la Horda Hobgoblin.",
      ":te mira con detenimiento.","'Vienez en nombre del lider?.", 
      "'Zi no ez azí olvidate de mi!", "'No rezponderé a nada."})); 
      nuevo_dialogo("prueba",({"'Vienez en nombre de Glaiven.",
      ":tiembla.","'Dame una prueba, no hablaré sin pruebaz.","entregar"}),"nombre");
  nuevo_dialogo("mision",({"'Glaiven noz encomendó una mizión.",
    ":zaca pecho.", "'Debiamoz adentrarnoz en la zubozcuridad, hazta la ciudad de Nauth'zinder.",
    "'Una vez allí recogimozs el guantelete.","'Pero todo ze noz complico, por el inútil "
    "de Zahtr.",":golpea a Zahtr con su mano izquierda."}));
  nuevo_dialogo("complicacion",({"'El inepto de Zahtr, observo un manto oscuro en el camino.",
    "'Mejor dicho, lo que el creyó era un manto oscuro, al ir a cogerlo."
    "'Ya zabez esta chuzma goblin, no puede evitar cualquier zaqueo.",
    "'El cazo ez que el manto, era un truco de zubterfugio de un Cloaker.",
    "'Empezamos a correr para ezcapar y llegamoz hazta aquí, pero en el camino..."}),"mision");
  nuevo_dialogo("camino",({"'En el camino, primero Zohtr perdió la llave del cofre, muy habil él.",
    "'Luego oimoz gritoz de enanoz, azí que decidi evitar que el puño cayeze en zuz manoz."
    "'Azí que lo escondi, debidamente....",
    ":pone mala cara."}),"complicacion");
   nuevo_dialogo("escondi",({"'Zi lo ezcondí, pero no te pienzo decir dónde.",
                                "hito morgroddur-tomar_el_mando gomtak"}),"complicacion");

    prohibir("mision","puede_conversar_mision");
    habilitar_conversacion();
    
  call_out("setup_horda",0); 
  

}

void setup_horda() 
{ 
 object ld;
 
 fijar_grupo_seguir(this_object());
 nuevo_agrupado(this_object());

 if(!environment())
	return;
 
 //La ruta para éstos es NPCS /d/kheleb/npcs
 if(ld = secure_present(NPCS+"soldado_horda1",environment()))
 {
    ld->fijar_grupo_seguir(this_object());
   TO->nuevo_agrupado(ld);
 }
 
 if(ld = secure_present(NPCS+"soldado_horda2",environment()))
 {
    ld->fijar_grupo_seguir(TO);
    TO->nuevo_agrupado(ld);
 }
}

void event_enter(object ob,string m,mixed pu,mixed ta) 
{
  ::event_enter(ob,m,pu,ta);
  if(ob->dame_especializacion()=="goblinoide") //comprobar que no este en peleas
        do_say("Zaludoz, kamarada "+ob->query_short()+".", 0);
}


void heart_beat()
{
    object *enemigos,victima,ld1,ld2;

    ::heart_beat();

    if(!sizeof(enemigos=query_attacker_list()))
      return;
      
 

    victima = seleccionar_victima(enemigos);

    if(!victima) 
      return;

    if (query_property("comm_lockout"))
      return;
      
    if (!query_timed_property(BARRIDO))
    {  
            if (sizeof(enemigos)>2)
            {                 
                habilidad("barrido", victima->query_name());
                return;
            }
    }   
    if(!query_timed_property(APLASTAR))
    {
            habilidad("barrido", victima->query_name());
            return;
    }
    if(ld1 = secure_present(NPCS+"soldado_horda1",environment()) && ld2= secure_present(NPCS+"soldado_horda2",environment()))
    {
        /*if(!query_timed_property(HORDA))
        {   
               dame_habilidad("horda")->exec_command(victima->query_name(),this_object(),"horda");
               return;
        }*/
      }
   if(!query_timed_property(GC)&&!query_timed_property(GC2))
    {
           if (random(2)==0)
           {
            habilidad("golpecertero", "rapido " + victima->query_name());
        return;
       }    
       else
       {
            habilidad("golpecertero", "poderoso " + victima->query_name());
            return;
       }
    }
    if(!query_timed_property(ATAQUEDOBLE))
    {
       habilidad("ataquedoble", victima->query_name());
           return;
    }
}

// entrega del papel que habilita la conversación sobre el tema de la misión que estaban haciendo
void entregar(object quien)
{
    object papel;
    papel = secure_present(OBJETOS+"papel_mision_punyo",quien);
    
    if (papel)
    {
        papel->dest_me();
        environment()->add_static_property("papel_entragado",1);
        do_say("Oh! Vienez en nombre de nueztro lider Glaiven.\n", 0);
        tell_object(quien,query_short()+ " realiza una breve reverencia hacia ti.\n");
        tell_accion(environment(),query_short()+" realiza una breve reverencia hacia "+quien->query_short()+".\n",
        "\n.",({quien}),this_object());
        quien->add_static_property("puede_conversar_mision",1);
    }
    else
    {
        do_say("¿Intentaz timarme?.\n", 0);
    }
    
}

int do_death(object asesino)
{
    if ( asesino ) {
        environment()->dar_token(asesino);
        write_file(LOGS+"lider_horda.log",asesino->query_short()+ " mató a "
        +query_short()+ " a nivel "+asesino->query_level()+" ayudado por "
        +nice_list((query_attacker_list()+query_call_outed())->query_short())+" el "
        +ctime()+".\n");
    }
    return ::do_death(asesino);
}


void event_death(object muerto,object *atacantes,object asesino)
{
    write_file(LOGS+"lider_horda.log",asesino->query_short()+ " mató a "+muerto->QCN+" el "+ctime()+".\n");
}

void attack_by(object pj)
{
    object npc1,npc2;
    
    if (npc1=secure_present(NPCS+"soldado_horda1",environment()))
       npc1->attack_ob(pj);
    if (npc2=secure_present(NPCS+"soldado_horda2",environment()))
       npc2->attack_ob(pj);
    
    ::attack_by(pj); //Dalion: Le faltaba añadir esto, ya que si no estaba ni npc1 ni npc2 entonces no atacaba...
}



  int frases_personalizables(string tipo, object npc, object pj)
  {
          switch(tipo)
          {
             case "despedida-pj":
                  tell_object(pj, "Cansado de la vulgaridad de "+npc->query_short()+" dejas de conversar con él.\n");
             break;
                 case "despedida-npc":
                  npc->do_command("emote se despide y escupe al suelo.");
                 break;
                 //Sacará el resto de mensajes por defecto para el resto
             case "bienvenida-pj":
                    npc->do_command("emote se rasca la entrepierna y te mira con atención.");
                    break;
             case "noentiende-pj":
                  npc->do_say("¡Inútil! Hablaz peor que mi primo con un codillo de elfo en la boca.");
                    break;
             case "noentiende-npc":
                npc->do_command("emote te mira con irá, parece que ha entendido que insultas a su familía.");
                        break;
             
             case "saludo":
                 npc->do_say("Argggghhh, por qué me molestaz?");
                 break;              
                 default:
                         return 0;
          }

          return 1;
  }
