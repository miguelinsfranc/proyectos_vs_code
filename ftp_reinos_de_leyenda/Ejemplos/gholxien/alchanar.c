// Istanor Abril'03
// Modificado: Ember 19/9/2003
// Ember 1/06/2004, para que no le desarmen, apaño hasta que Istanor decida
// algo. Dunkelheit 25-04-2007 -- añado la parte para la quest del 'protección
// fuego' Satyr 2012 -- Refactorizando Astel 05.08.15 . Bug. Estaba resistiendo
// todos los hechizos.

#include "/d/takome/path.h"
inherit "/obj/monster.c";

object espada;

// Añadido: Ember 19/9/2003
void combustion_interna()
{
    object *todos, *objetivos = ({});
    object  objetivo;
    int     i, cuantos;

    do_emote("sonríe malignamente mientras chasquea los dedos y una pequeña "
             "%^BOLD%^RED%^llama%^RESET%^ aparece en su dedo índice.\n");

    // Buscamos un objetivo...
    cuantos = 0;
    for (i = sizeof(todos = all_inventory(environment(this_object()))); i--;) {
        if (living(todos[i]) && !todos[i]->query_hidden() &&
            todos[i]->query_player() && todos[i] != this_object()) {
            objetivos += ({todos[i]});
            cuantos++;
        }
    }

    if (cuantos > 0) {
        objetivo = element_of(objetivos);
        tell_object(
            objetivo,
            "Alchanar te señala con el dedo, proyectando la "
            "%^BOLD%^RED%^llama%^RESET%^ hacia "
            "ti, que se desplaza perezosamente, conformando una trayectoria "
            "hipnótica en forma de vórtice.\n");
        tell_accion(
            ENV(TO),
            "Ves como Alchanar señala a " + objetivo->query_short() +
                " con su dedo, y una pequeña "
                "%^BOLD%^RED%^llama%^RESET%^ se dirige, perezosamente, hacia "
                "él.\n",
            "Escuchas un leve crepitar.\n",
            ({objetivo}));
    } else {
        tell_room(
            ENV(TO),
            "Alchanar, desconcertado, apaga la %^BOLD%^RED%^llama%^RESET%^ que "
            "sostenía en su dedo.\n");
        return;
    }

    if (!objetivo->dame_ts("aliento", 0, "fuego", 0)) {
        // Falla la tirada de salvacion...
        tell_object(
            objetivo,
            "¡La %^BOLD%^RED%^llama%^RESET%^ te alcanza de lleno y se interna "
            "en ti! "
            "Acto seguido, empiezas a sentir un calor sofocante, abrasador, "
            "extenuante. Te sientes desfallecer...\n");

        if (objetivo->spell_damage(-(500 + random(300)), "fuego", TO, 0) ==
                -300 &&
            objetivo->spell_damage(-(700 + random(300)), "magico", TO, 0) ==
                -300) {
            return;
        }

        tell_object(
            objetivo,
            "¡Sientes como la %^BOLD%^RED%^llama%^RESET%^ implosiona en tu "
            "interior, "
            "%^BOLD%^RED%^abrasando%^RESET%^ y "
            "%^BOLD%^RED%^calcinando%^RESET%^ tus "
            "órganos vitales haciéndote retorcer de "
            "%^BOLD%^RED%^dolor%^RESET%^!\n");
        tell_room(
            ENV(TO),
            "¡Escuchas el grito agónico de " + objetivo->query_short() +
                " mientras unas %^BOLD%^BLUE%^llamas "
                "azuladas%^RESET%^ brotan de su boca!\n",
            objetivo);
    } else {
        tell_object(
            objetivo,
            "%^BOLD%^RED%^La llama choca contra ti, pero no pasa nada.\n");
        tell_room(
            ENV(TO),
            "Alchanar mira extrañado a " + objetivo->query_short() +
                " después que la llama chocara contra su pecho "
                "y nada pasara.\n",
            objetivo);
    }
}
void setup()
{
    set_name("alchanar");
    add_alias("alchanar");
    add_plural("alchanar");
    set_short("%^BOLD%^RED%^Alchanar, el Robador de Almas%^RESET%^");
    set_main_plural("%^BOLD%^RED%^Alchanar, el Robador de Almas%^RESET%^");
    set_long("Te asustas al observar esta terrible criatura. De tres metros de "
             "altura un terrible demonio se alza ante"
             " ti. Sus alas ensombrecen toda la estancia y envuelto en llama "
             "viva no puedes mantenerte a su lado. Su piel es oscura"
             " contrastada por unos ojos amarillos que parecen penetrar en tu "
             "mente y en la mano derecha porta una espada de fuego"
             " que parece salir de su mano formando una prolongación de su "
             "ser. Se trata de Alchanar el malvado demonio Robador de"
             " Almas, pues se dice que sus ojos logran penetrar en tu alma "
             "apoderándose de ella antes de propinarte un golpe mortal"
             " con un solo mandoble de su espada que quema a sus enemigos por "
             "dentro.\n");

    // Modificado: Ember 19/9/2003
    fijar_raza("demonio");
    fijar_clase("soldado");
    fijar_religion("gurthang");
    fijar_genero(1);

    fijar_fe(100);
    fijar_alineamiento(25000);

    set_random_stats(18, 21);
    fijar_extrema(100);
    fijar_fue(30);
    fijar_des(20);
    fijar_con(25);
    fijar_int(30);
    fijar_nivel(65 + random(15));
    fijar_pvs_max(45000);
    fijar_estatus("Poldarn", -600);

    fijar_maestrias((["Cortantes pesadas":100]));

    set_aggressive(1);
    fijar_memoria_jugador(1);
    set_join_fight_mess(
        "'¡Tu alma se mantendrá aquí para toda la eternidad!\n");

    // Si no tiene esto, no utilizará la espada por culpa del estilo
    poner_estilo_combate_desarmado("pelea");

    load_a_chat(
        100,
        ({":empuña su espada con las dos manos preparándose para realizar un "
          "barrido."}));

    // Modificado: Ember 19/9/2003
    // Conjuracion por la flecha en llamas, evocacion por la bola de fuego
    add_spheres((["mayor":({"evocacion", "conjuracion"})]));
    add_attack_spell(35, "bola encadenada de fuego", 4);
    add_attack_spell(30, "golpecertero", 3);
    add_attack_spell(20, "combustion interna", ( : combustion_interna() :));

    // Ember, para que no le desarmen
    add_property("no_desarmar", 1);

    add_clone(ARMAS + "espada_ardiente.c", 1);
    add_clone(BARMADURAS + "completa.c", 1);
    add_clone(BARMADURAS + "gran_yelmo.c", 1);
    add_clone(BARMADURAS + "guantelete.c", 1);
    add_clone(BARMADURAS + "guantelete_izq.c", 1);
    add_clone(BARMADURAS + "botas_guerra.c", 1);
    add_clone(BARMADURAS + "grebas_metalicas.c", 1);
    add_clone(BARMADURAS + "brazal.c", 1);
    add_clone(BARMADURAS + "brazal_izq.c", 1);
    init_equip();

    nueva_resistencia("agua", -100, 1);
    nueva_resistencia("frio", -100, 1);
    nueva_resistencia("bien", -100, 1);
    nueva_resistencia("mal", 100, 1);
    nueva_resistencia("enfermedad", 100, 1);

    fijar_objetos_morir(ARMAS + "espada_ardiente.c");
    fijar_objetos_morir(({"tesoro", ({"arma", "armadura"}), ({5, 6}), 1}));
}
int do_death(object asesino)
{
    if (!asesino) {
        return ::do_death();
    }

    tell_object(
        asesino,
        "Cuando propinas el golpe mortal a Alchanar este cae dolorido y sus "
        "llamas se apagan.\n");
    tell_accion(
        ENV(TO),
        asesino->query_short() + " propina el golpe mortal a Alchanar y este "
                                 "cae al suelo apagándose sus llamas.\n",
        asesino->query_short() + " propina el golpe mortal a Alchanar y este "
                                 "cae al suelo apagándose sus llamas.\n",
        ({asesino}),
        asesino);

    ENV(TO)->alchanar1();

    if (asesino->query_player()) {
        asesino->add_static_property("propinador", query_name());
    }

    if (asesino->dame_mision_abierta("prueba_temple") &&
        !asesino->dame_mision_completada("prueba_temple")) {
        asesino->nuevo_hito_mision("prueba_temple", "asesine_alchanar", 0);
    }

    write_file(
        LOGS + "alchanar.log",
        ctime() + " " + asesino->query_name() + " mato a Alchanar.\n");
    return ::do_death(asesino);
}
int spell_damage(int pupa, string tipo, object lanzador, object hechizo)
{
    if (tipo == "fuego") {
        if (lanzador) {
            tell_object(
                lanzador,
                "¡" + this_object()->query_short() +
                    " arde intensamente mientras sonríe maliciosamente!\n");
            tell_accion(
                ENV(lanzador),
                "¡" + this_object()->query_short() +
                    " crepita violentamente cuando " +
                    lanzador->query_cap_name() +
                    " le alcanza con su hechizo!\n",
                "Escuchas un ruidoso crepitar.\n",
                ({lanzador}));
        }
        ::adjust_hp(-pupa, lanzador);
        return 0;
    }
    return ::spell_damage(pupa, tipo, lanzador, hechizo);
}
int resistencia_magica(
    int nivel_lanzador, string caracteristica, string escuela, object hechizo)
{
    return hechizo->dame_nivel_hechizo_real() <= 4;
}
object make_corpse()
{
    object cuerpo = ::make_corpse();
    cuerpo->add_property("drenar", "fuego"); // Alchanar da 'proteccion fuego'
    return cuerpo;
}
