#include "/d/takome/path.h"
#include <ciudadanias.h>
#include <spells.h>
#define PAQUETE "/d/anduar/items/paquete_kalb_takome"
#define LOG LOGS + "dalisha.log"
inherit H_CRUZADA + "base_npcs_cruzada.c";
void poner_piel();
void setup()
{
    set_name("dalisha");    
    set_short("Dalisha");
    set_main_plural("imágenes de Dalisha");

    set_long(
        " Una mujer de unos 30 años con cara de pocos amigos. Sus labios pintados de un fuerte "
        "color carmesí hace mucho que no conocen los movimientos necesarios para realizar sonrisas. "
        "Tiene unos brillantes ojos plateados y cabellos negros que se derraman sobre su espalda "
        "hasta llegar a la altura de la cintura. Viste una túnica escotada con emblemas arcanos en "
		"sus mangas y espalda, así como un bordado de Argan en el cuello. Como último "
        "signo identificativo, alberga el medallón de Eralie entre sus dos generosos senos. Dalisha"
        " es una emprendedora Takomita y no pertenece a la Cruzada. Su sangre de comerciante "
        "nobiliar vió una oportunidad de hacer negocio en la organización a base de importar y "
        "revender artefactos desde Anduar y así lo hizo. La nueva cruzada quería una tienda de "
        "magia y un laboratorio, pero no se los podían permitir, por lo que Dalisha se instauró "
        "como una de sus opciones. Ahora ella colabora de manera activa con la Cruzada, pero nadie "
        "sabe a ciencia cierta si su lealtad se debe a los sesioms o a la organización. Hasta "
        "ahora no ha hecho nada raro.\n"
    );

	fijar_clase("hechicero");
    fijar_especializacion("abjurador_blanco");
	fijar_fue(15);
	fijar_des(19);
	fijar_con(17);
	fijar_int(22);
	fijar_sab(17);
	fijar_car(14);
    fijar_fe(80);
    fijar_genero(2);
    fijar_alineamiento(-1000); 
    fijar_nivel(37 + random(40));
    fijar_pvs_max(dame_pvs_max() * 3);
   add_hated("enemigo","Takome");
   add_hated("relacion","takome");

	add_clone(BARMAS     + "daga.c", 1);
    add_clone(BARMADURAS + "tunica.c" ,1);
    add_clone(BARMADURAS + "pantalones.c" ,1);
    add_clone(BARMADURAS + "zapatillas.c" ,1);
    add_clone(SIMBOLO, 1);

    add_spell("cono de frio");
    add_spell("flecha de agua");
    add_spell("horrible marchitamiento");
    add_spell("poderosa negacion de la esencia");

    nuevo_dialogo("dalisha", ({
        "'Soy Dalisha Dermihar, del linaje del mismo nombre.",
        "'El blasón del tridente en el mar.",
        "'Provengo de Aldara y regento esta tienda.",
        "'¿Necesitas saber algo más o podemos ya hablar de negocios?"
        })
    );
    nuevo_dialogo("negocios", ({
        "'En mi catálogo podrás encontrar todo lo que un hechicero necesita.",
        "'Traigo los mejores hechizos arcanos desde la tienda de Bilops, en Anduar.",
        "'De vez en cuando, también tengo rarezas en la tienda, pero estas se acaban rápido.",
        "'Los primeros días del mes son los más agotadores y donde más demanda hay."
        }),
        "dalisha"
    );
    nuevo_dialogo("demanda", ({
        "'La mercancía llega a la tienda los primeros días de cada mes.",
        "'Si buscas algo, tendrás más posibilidad de encontrarlo si llegas entre el día 1 y el "
        "día 5 de cada mes.",
        "'Para saber en que día vives, sólo tienes que escribir \"calendario\"."
        }),
        "negocios"
    );
    nuevo_dialogo("bilops", ({
        "'Bueno... la tienda no es suya, claro.",
        "'Es de su maestro.",
        "'Bilops no es más que un empleado.",
        "'El caso es que llegué a un acuerdo con su franquicia.",
        "'Yo le compro a poco más de precio de coste al por mayor y lo revendo aquí con un "
        "ligero margen de beneficio."
        }),
        "negocios"
    );
    nuevo_dialogo("margen", ({
        "'Yo no soy miembro de la Cruzada de Eralie.",
        "'Soy una devota, pero mi lugar está en otra parte.",
        "'Prefiero dejar las hazañas a ejércitos disciplinados.",
        "'Yo estoy aquí por negocios...",
        "'La cruzada quería una tienda de magia y un laboratorio para sus nuevas instalaciones. "
        "Desafortunadamente, no tenían ni dinero ni tiempo para todo. Además... tampoco queda "
        "mucho espacio libre.",
        "'Yo les ofrecí un trato: me dejaban una pequeña sala y yo me encargaría de regentar y "
        "reabastecer una pequeña tienda de magia.",
        "'Así ellos no tienen que preocuparse y yo puedo vender tranquilamente mis enseres.",
        "'Dudo mucho que se aprobase la creación de una sala de hechicería más grande... los "
        "Takomitas son reacios a tratar con la hechiceria."
        }),
        "bilops"
    );
    nuevo_dialogo("hechiceria", ({
        "'En Takome creen en sus filos de plata, en Eralie y en el honor.",
        "'La hechicería en manos de gente que no sea un sacerdote, para muchos, aún está muy "
        "mal vista.",
        "'Ese es el único motivo por el que la tienda no es más grande."
        }),
        "margen"
    );
    nuevo_dialogo("compro", ({
        "'No te interesan mucho los detalles... más que nada, porque son secretos del negocio.",
        "'Sencillamente contrato a una agencia externa para que me envíen cada principios de mes "
        "una caravana repleta de artefactos y pergaminos para las ventas de dicho mes.",
        "'Si necesito hacer una petición extraordinaria, solicito a la agencia Kalb que me envié "
        "con caracter urgente un paquete asegurado."
        }),
        "bilops"
    );
    nuevo_dialogo("asegurado", ({
        ":hace una leve mueca de satisfacción, que no llega a una sonrisa.",
        "'Donde digo \"paquete asegurado\" quiero decir \"paquete encantado con runas de "
        "deflagración mayores\".",
        "'Cualquiera que no fuese yo que intentase abrirlo... ¡¡BOOOM!!"
        }),
        "compro"
    );
    nuevo_dialogo("kalb", ({
        "'Kalb es un pequeño comerciante que se ha hecho bastante influyente en Anduar.",
        "'Nadie sabe como, pero tiene un pequeño ejército de mensajeros que le hacen los recados "
        "por apenas 4 denarios. ¿De donde viene esa gentuza que trabaja por tan poco?",
        "'En fin. Con tanto margen de ganancias es normal que se hiciese famoso. No suele fallar. "
        "Siempre tiene algo que enviarme."
        }),
        "compro"
    );
    nuevo_dialogo("enviarme", ({
        "'Normalmente envía a gente sin saber muy bien a donde se dirigen.",
        "'No sería la primera vez que un goblin muere empalado en la senda del alba cuando este "
        "intentaba ganarse unas monedas.",
        "'No es que me den pena esas cosas... pero eso sólo me consigue retrasos.",
        "'Tú has entrado hasta aquí, así que... ¿por que no vás hacia Anduar y le pides a Kalb "
        "que te de algún paquete para mi?",
        "'Así me aseguro de que por lo menos envia a alguien al que no matarán nada más ver.",
        "'Sí... claro... te pagaré algo."
        }),
        "kalb"
    );
    nuevo_dialogo("acepto", ({
        "'Pues muy bien.",
        ":garabatea algo en tu frente y sientes un escalofrío en la espalda.",
        "'Ahora mis paquetes marcados no reaccionaran a tu presencia.",
        "'¡Ve allí y traeme eso cuanto antes!",
        "'Es en Anduar, en el noreste de mercado. La tienda de herramientas.",
        "hito takome_dalisha encargo_dalisha"
		//"propiedad_kalb"
        }),
        "enviarme"
    );
        
    prohibir("enviarme", "es_cruzado");
    prohibir("acepto",   "es_cruzado");
    prohibir("acepto",   "chequeo_inicial_aceptar");
    habilitar_conversacion();
    PREPARAR_NPC_CRUZADA;
    poner_piel();
}
int chequeo_inicial_aceptar(object b) 
{
    //return !b->query_timed_property("bloqueo-dalisha");
	return !b->dame_mision_completada("takome_dalisha");
}
void poner_piel()
{
    object b = clone_object(SHADOWS + "piel_de_piedra_sh.c");

    if (b) {
        b->setup_sombra(this_object(), 19 + roll(2, 3));
        tell_accion(
            environment(),
            "¡" + query_short() + " chasquea los dedos y su piel se vuelve de piedra!\n",
            "",
            ({this_object()}),
            this_object()
        );
    }
}
status rutina_de_proteccion()
{
    if (!this_object()->dame_piel_de_piedra()) {
        poner_piel();
        return 1;   
    }

    return 0;
}
void rutina_combate()
{
    if (rutina_de_proteccion()) {
        return;
    }

    if (atacar_enemigo(({"hechicero", "velian"}), "poderosa negacion de la esencia")) {
        return;
    }
    if (atacar_enemigo(({"sacerdote", "todo"}), "horrible marchitamiento")) {
        return;
    }
    if (atacar_enemigo(({"todo"}), "cono de frio")) {
        return;
    }
    if (atacar_enemigo(({"todo"}), "flecha de agua")) {
        return;
    }
}
void attack()
{
    rutina_combate();
    ::attack();
}
//quitar esta funcion
/* void propiedad_kalb(object b)
{
    b->add_property("kalb-takome", 1);
} */
status chequeo_previo_paquete(object quien, object paquete)
{
    string orig = paquete->dame_portador();
    orig        = orig ? capitalize(orig) : "el portador original de este paquete";
    
    if (paquete->dame_portador() != quien->query_name()) {
        do_say("¿Qué has hecho con " + orig + "?", 0);
        do_say("Se supone que tú no deberías traerme esto...", 0);
        do_say("Me lo quedaré, pero no te pienso dar nada.", 0);
        return 0;
    }
    
    return 1;
}
string dar_premio_jugador(object quien)
{        
    int pela   = roll(1, 100) + 75;
    string log = "";
    object cruz;
    
    log += "y recibe: [" + pela + " sesioms";
    quien->ajustar_dinero(pela, "sesiom");
    tell_object(quien, "Recibes " + pela + " monedas de sesiom.\n");
    if (quien->dame_nivel() < 25) {
        quien->ajustar_xp(20000);
        log += "/20.000 xp";
    }
    
    if (quien->dame_estatus("Takome") < NOBLE) {
        quien->ajustar_estatus("Takome", 1);
        log += "/estatus";
    }
    
    if (cruz = load_object(O_CRUZADA)) {
        cruz->otorgar_puntos("colaboracion", quien, 1);
    }
        
    return log + "]";
}
string dar_premio_hechicero(object quien)
{
    string log = "";
    string *hechizos = ({
        "espejismo", "toque_vampirico", "caida_de_pluma", "granizo"                
    });
    string aux = element_of(hechizos);    
    
    if (!quien->query_spell(replace(aux, ({"_", " "})))) {
        object pergata;
        
        pergata = clone_object(BPERGAMINOS + "magico/perg_" + aux + ".c");
        
        if (!pergata) {
            return "";
        }
        
        if (pergata->move(quien)) {
            pergata->move(environment());
        }
        
        do_say(
            "Y como me has caído bien y eres un aprendiz de hechicero... toma. "
            "Acepta este regalo.",
            0
        );
        tell_object(quien, "Recibes: " + pergata->query_short() + ".\n");
        log += " y " + base_name(pergata);
    }
    return log;
}
int event_dar(object quien, object* que)
{
    object paquete;
    string log = ctime() + " " + quien->query_cap_name() + " ";
    
    foreach(object b in que) {
        if (base_name(b) != PAQUETE) {
            return 0;
        }
        paquete = b;
    }
    
    if (!chequeo_previo_paquete(quien, paquete)) {
        write_file(LOG, log + "entrega un paquete a nombre de " + paquete->dame_portador() + ".\n");
        que->dest_me();
        return 1;
    }
    
    quien->nuevo_hito_mision("takome_dalisha","paquete_entregado",0);
	//quien->add_timed_property("bloqueo-dalisha", 1, 60 * 60 * 3);
    //quien->remove_property("kalb-takome");
    
    do_say("Gracias, " + capitalize(quien->query_name()) + "." , 0);
    do_say("Toma esto como pago.", 0);
    log += dar_premio_jugador(quien);

    if (quien->dame_clase() == "hechicero" && quien->dame_nivel() < 15) {
        log += dar_premio_hechicero(quien);
    }   
    
    write_file(LOG, log + ".\n");
    que->dest_me();   
    return 1;
}
int do_death(object b)
{
    fijar_tesoro(4 + !random(5), "pergamin");
    return ::do_death(b);
}
