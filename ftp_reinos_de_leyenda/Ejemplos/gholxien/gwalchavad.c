// Satyr 24-04-2010 -- Por la limitación de caracteres del conversar retoco la manera de mostrar la ayuda del gremio
// Eckol 2Jul15: No estaba mostrando el mensaje de error al no poder alistarse.
#include "/d/takome/path.h"
#include <ciudadanias.h>
inherit H_CRUZADA + "base_npcs_cruzada.c";
int hurras;
void poner_dialogo_mas();

void setup()
{
    set_name("gwalchavad");
    set_short("Gwalchavad, Cetro de Eralie");
    set_main_plural("imagenes de Gwalchavad, Cetro de Eralie");
    set_long(
        " Gwalchavad, una de las más míticas figuras de la Cruzada de Eralie. Es conocido por su "
        "increíble devoción y por poseer un poder curativo capaz de extirpar las peores enfermedades"
        " conocidas. Su serio semblante está agrietado por un sinfín de arrugas que denotan "
        "las decenas de años de servicio que lleva este hombre sirviendo a la Cruzada. Desde los "
        "primeros días de la organización ha servido con energía y aunque a día de hoy su espalda "
        "está más arqueada que cuando era joven, sigue dedicándole todas sus energías a la causa "
        "en la que tanto cree. Está enfundado en pesadas armaduras, pues se enorgullece de ser "
        "un sacerdote de combate, aunque raras veces marcha a la guerra ya que es obligado "
        "a permanecer en Takome debido a la estima que se le tiene a sus poderes. Luce un montón "
        "de objetos mágicos que ha recogido en sus aventuras. "
        "Es un hombre sabio y es el que se encarga de alistar a los novicios en la cruzada.\n"
    );

    fijar_clase("eralie");
    fijar_fue(18);
    fijar_des(14);
    fijar_con(18);
    fijar_int(15);
    fijar_sab(25);
    fijar_car(16);
    fijar_genero(1);
    fijar_nivel(46 + random(9));
    fijar_pvs_max(dame_pvs_max() * 7);
    ajustar_bo(80);
    ajustar_bp(90);
    fijar_fe(220);
   add_hated("enemigo","Takome");
   add_hated("relacion","takome");
    
    fijar_maestrias(([
        "maza": 40,
        "espada larga": 40
    ]));

    add_clone(BARMAS     + "maza_a_dos_manos.c", 1);
    add_clone(BARMADURAS + "completa.c", 1);
    add_clone(BARMADURAS + "gran_yelmo.c", 1);
    add_clone(BARMADURAS + "brazal.c", 1);
    add_clone(BARMADURAS + "brazal_izq.c", 1);
    add_clone(BARMADURAS + "grebas_metalicas.c", 1);
    add_clone(BARMADURAS + "botas_guerra.c", 1);
    add_clone(BARMADURAS + "guantelete.c", 1);
    add_clone(BARMADURAS + "guantelete_izq.c", 1);
    add_clone(BARMADURAS + "anillo.c", 1);
    add_clone(BARMADURAS + "amuleto.c", 1);    
    add_clone(BARMADURAS + "cinturon.c", 1);

    add_clone(ARMADURAS  + "amuleto_suerte_eralie.c", 1);
    add_clone(ARMADURAS  + "pendienteplata.c", 1);
    add_clone(ARMADURAS  + "capa_escarlata", 1);
    add_clone(CANON_ERALIE, 1);
    add_clone(SIMBOLO, 1);

    ajustar_dinero(dame_nivel() / 2, "sesiom");

    add_spell("curar heridas moderadas");
    add_spell("curar heridas criticas");
    add_spell("curar heridas ligeras");
    add_spell("curar heridas serias");
    add_spell("tormenta sagrada");
    add_spell("columna de fuego");
    add_spell("castigar al mal");
    add_spell("palabra divina");
    add_spell("debilidad bien");
    add_spell("regeneracion");
    add_spell("curacion");
    add_spell("canto");
    add_spell("bendicion");

    add_property("no_desarmar", 1);
    add_property("sin-miedo"  , 1);
    add_property("ts-paralisis"      , 5);
    add_property("ts-veneno"         , 10);
    add_property("ts-mental"         , 20);
    add_property("ts-muerte"         , 70);
    add_property("ts-petrificacion"  , 90);

    nueva_resistencia("enfermedad", 50, 1);
    nueva_resistencia("veneno", 50, 1);
    nueva_resistencia("fuego", 20, 1);
    nueva_resistencia("mal", 50, 1);

    nuevo_dialogo("nombre", ({
        "'Gwalchavad, ese es mi nombre. Gwalchavad el Cetro de Eralie.",
        "'La Cruzada es mi hogar y Eralie mi corazón, ¿y vos quién sois?",
        "'¿Buscais algo en lo que pueda ayudaros?"
        })
    );
    nuevo_dialogo("cetro", ({
        "'Me llaman \"el Cetro\" por mi habilidad con las mazas a dos manos y la curación.",
        "'Cuando era muy niño era hijo de un humilde carpintero y me dedicaba a ayudarle, moviendo "
        "enormes vigas de aquí para allí.",
        "'Pronto mi nulidad para la carpintería quedó demostrada y gracias a una misionera en sus "
        "años blancos decidí alistarme en la Cruzada. Mi padre me respaldó.",
        "'A la temprana edad de 9 años empecé a entrenar como miliciano. En aquella época, una "
        "simple maza me quedaba enorme. Acostumbrado a llevar vigas de aquí para allá, pronto "
        "desarrollé un estilo de combate basado en las armas grandes.",
        "'Sin embargo... no acudo a la guerra. No por voluntad propia, sino porque he aprendido "
        "dotes curativas a lo largo de los años y la Cruzada no quiere arriesgarme.",
        "'Cuando hay alguna intervencion importante, suelo ayudar a los sacerdotes a llevarla a "
        "cabo, si es que no la llevo yo mismo."
        }),
        "nombre"
    );
    nuevo_dialogo("intervencion", ({
        "'La guerra alimenta al mundo con muertos y heridos.",
        "'Es mi labor la de intervenir para evitar que jóvenes nobles y valerosos desfallezcan "
        "por la mancha de la guerra.",
        "'Resurrecciones, curaciones y tratamientos contra enfermedades o maldiciones son "
        "menesteres que lleva a cabo la iglesia y la Cruzada.",
        "'Aunque las más importantes han de celebrarse en el templo por los sacerdotes más "
        "favorecidos, la inmensa mayoría de las intervenciones son celebradas en los barracones "
        "de la cruzada."
        }),
        "cetro"
    );
    nuevo_dialogo("cruzada", ({
        "'La Santa Cruzada de Eralie. La organización religiosa-militar más importante y bondadosa "
        "sobre la faz de Eirea.",
        "'Te encuentras ante su sacra entrada.",
        ":hace un gesto señalando la gigantesca puerta.",
        "'Yo soy el encargado de asegurarse que no entra nadie que no se lo merezca al pasillo "
        "de los héroes. También me encargo del reclutamiento.",
        "'Tengo más labores, pero aquí sólo enfundo estas.",
        "'¿Quieres saber mas?",
        }),
        "nombre"
    );
    nuevo_dialogo("reclutamiento", ({
        "'Los novicios que deseen unirse a la Cruzada deben presentarse ante mi.",
        "'No importa si no son buenos guerreros. La Cruzada tiene hueco para contables, tenderos, "
        "mineros, alfareros, operarios, escritores, sacerdotes, médicos y hasta para taberneros.",
        "'Si eres Takomita, adoras a Eralie y quieres unirte... sólo tendrás que alistarte."
        }),
        "cruzada"
    );
    nuevo_dialogo("alistarte", ({
        "alistar_jugador"
        }),
        "reclutamiento"
    );
    nuevo_dialogo("licenciarse", ({
        "'¿Cómo?, ¿quieres abandonar la cruzada?, ¿estás seguro de eso?"
        }),
        0,
        ({"si", "no"}),
        "¿Estás seguro de querer hacerlo?, esta decisión es irreversible."
    );
    
    prohibir("licenciarse", "es_cruzado");
    poner_dialogo_mas();
    habilitar_conversacion();
    fijar_guardia();
    PREPARAR_NPC_CRUZADA;
    set_heart_beat(1);
}
int funcion_si(object b)
{
    object cruz = load_object(O_CRUZADA);
    
    if (cruz)
    {
        do_say("Vaya, es una lástima.", 0);
        
        cruz->limpiar_info(b->query_name());
		cruz->anunciar(query_short(), "Lamentablemente, " + b->query_cap_name() + " ha abandonado por voluntad propia la organización.");
    }
    else
        do_say("De acuerdo, pero inténtalo más tarde, ahora hay un error.", 0);
}
int funcion_no(object b)
{
    do_say("Me alegro de que recapacitaras.", 0);
}
void decisiones_combate()
{
    if (intentar_curar_amigo(15)) {
        return;
    }
    else {
        object enemigo = dame_enemigo_tipo(({"seldar", "chaman", "sacerdote", "hechicero", "todo"}));
        
        if (!enemigo) {
            intentar_curar_amigo(90);
        }
        else {
            {
                function es_pj = function(object b) {
                    return b && b->query_player();
                };                
                int enemigos = sizeof(filter(query_attacker_list(), es_pj));

                if (enemigos >= 3 && !dame_lock("palabra divina") && secure_present(SIMBOLO, TO)) {
                    habilidad("palabra divina", "");
                    return;
                }
            }
            
            if (enemigo->dame_alineamiento() > 0) {
                if (atacar_enemigo(enemigo, "castigar al mal")) {
                    return;
                }
            }
            
            if (atacar_enemigo(enemigo, "columna de fuego")) {
                return;
            }
            
            if (atacar_enemigo(enemigo, "tormenta sagrada")) {
                return;
            }
            
            intentar_curar_amigo(90);
        }
    }
}
void decisiones_fuera_combate()
{
    if (intentar_curar_amigo(80)) {
        return;
    }
    
    if (lanzar_hechizo("bendicion", 0, "bendicion")) {
        return;
    }
    
    if (lanzar_hechizo("canto", 0, "plegaria")) {
        return;
    }
}
int check_anyone_here()
{
    int b = ::check_anyone_here();

    return b ? b : objectp(dame_amigo_curar(50));
}
void heart_beat()
{
    if (!dame_peleando()) {
        decisiones_fuera_combate();
        ::heart_beat();
    }
    else {
        string err = catch {
            ::heart_beat();
        };
        
        if (err) {
            object b = load_object(O_CRUZADA);
            this_object()->desconcentrar_efecto();
            
            b->log("error-npc", base_name() + " " + err);
        }
    }
}
void attack()
{
    decisiones_combate();
    ::attack();
}
void hurras(object pj)
{
    if (hurras >= 3) {
        hurras = 0;
        return;
    }

    hurras++;

    do_say("¡Hip hip!", 0);

    {
        object *amigos = dame_amigos();
        object amigo;
    
        if (amigos && sizeof(amigos)) {
            amigos -= ({pj});
            amigos -= ({this_object()});
        }

        if (!sizeof(amigos) || !amigo = element_of(amigos)) {
            amigo = this_object();
        }

        amigo->do_say("¡¡HURRA!!", 0);
    }
    call_out("hurras", 4, pj);
}
void alistar_jugador(object b)
{
    object cruzada = load_object(O_CRUZADA);
    string err;

    if (find_call_out("hurras") != -1) {
        tell_object(b, query_short() + " está ocupado ahora mismo.\n");
        return;
    }

    if (!cruzada) {
        err = "Lo siento, muchach" + b->dame_vocal() + ". La cruzada no está funcionando.";
    }
    else if (b->dame_gremio() == dame_gremio()) {
        err = "Tú ya perteneces a la cruzada.";
    }
	else if (b->dame_gremio() != "sin_gremio") {
		err = "Deberás licenciarte de tu gremio antes de alistarte a la Cruzada de Eralie.";
	}
	else if (b->dame_fe() < -20) {
        err = "La santa cruzada sólo acepta a devotos. Tu fe es demasiado baja.";
    }
	else if ( ! err = cruzada->chequeo_nuevo_miembro(b)) {
		do_say(
			"¡Que así sea!, ¡se bienvenid" + b->dame_vocal() + " a la gloriosa Cruzada de Eralie!",
			0
		);
		do_say(
			"¡Tres hurras por " + b->query_cap_name() + "!", 0
		);
		
		hurras(b);
		cruzada->nuevo_miembro(b);
		cruzada->log("reclutamiento", b->query_cap_name() + " (" + b->dame_raza() + "/" + b->dame_ciudadania() + ") se alista a la cruzada.");
	}
	else{
		do_say(err,0); //Faltaba esto para mostrar el mensaje de error.
	}
}
void mostrar_ayuda(object b)
{
    object c = load_object(O_CRUZADA);

    tell_object(b, c ? c->dame_ayuda() : "La ayuda de la cruzada es imposible de cargar.\n");
}
void poner_dialogo_mas()
{
    nuevo_dialogo("más", ({
        "'Vaya vaya vaya... veo que deseas saber más. Te contaré algo que se escribió "
        "hace años sobre la Cruzada...",
        "mostrar_ayuda"
        })
    );
}    
