//Rezzah 16/11/2009

#include "../../../path.h"
#define PANEL CAVERNAS+"conclave28.c"

inherit "/std/subterraneo.c";


void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "camino_caverna");
	add_exit(NE, CAVERNAS+"conclave29.c", "corridor");
	add_exit(S, CAVERNAS+"conclave31.c", "gate");
	modify_exit(S, ({"funcion", "bloquear_acceso"}));
	add_clone(NPCS+"dragon_topacio.c",1);
}

int bloquear_acceso(string salida,object caminante,string msj_especial) {
	mixed values;
	int pupita;
	string texto_pupa;
	object panel;
	mapping elementos = (["[a]": "aire", "[t]": "tierra", "[f]": "fuego", "[h]": "agua", "[r]": "electrico"]);

	panel = find_object(PANEL);
	if (!panel) //no está instanciada, la creamos
		panel = clone_object(PANEL);
	values = panel->chequear_tablero();
	if (values[0] && !query_static_property("abierta_por_dragon") )  { //Está cerrada por el panel y el dragón no la ha abierto
		//Causamos daño
		pupita = caminante->danyo_especial( -100-(values[2]*(random(30))+random(150)), elementos[lower_case(string_sencillo(values[1]))], this_object());//daño máximo -100-(30*15+150) = 700
		if (pupita < 200) texto_pupa = "fuerte";
		else if (pupita < 300) texto_pupa = "considerable";
		else if (pupita < 400) texto_pupa = "poderosa";
		else if (pupita < 500) texto_pupa = "increíble";
		else texto_pupa = "desgarradora"; 
		tell_accion(this_object(), caminante->query_short()+" intenta cruzar la arcada sur cuando una "+texto_pupa+" sacudida mágica del elemento "+elementos[lower_case(string_sencillo(values[1]))]+" le cierra el paso.\n", "Oyes un chispazo de energía.\n",({caminante}),0);
		return notify_fail("Al intentar cruzar la arcada sur una "+texto_pupa+" sacudida mágica del elemento "+elementos[lower_case(string_sencillo(values[1]))]+" atraviesa tu cuerpo impidiéndote entrar.\n");
	} 
	return 1;
}
