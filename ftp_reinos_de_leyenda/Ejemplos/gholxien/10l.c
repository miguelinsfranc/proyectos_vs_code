// Vhurkul 16.12.2002
// Satyr   19.10.2014
inherit "/std/outside";
#include "/d/dendra/path.h"
void setup() 
{
   	fijar_luz(95);
   	set_short("%^BLACK%^BOLD%^Muralla de la Ciudadela%^RESET%^");
   	set_long(
        "El núcleo de Galador es la milenaria Ciudadela, uno de los escasos vestigios anteriores al Apocalipsis que quedan en el Reino. Las murallas han sido reformadas por el desgaste sufrido a lo largo de los siglos, y en el interior se encuentran edificios emblematicos, como la Catedral de Seldar, sede de la Inquisición, el Palacio, el Cuartel General de los Soldados de Galador, el ejército del Reino y, por último pero no por ello menos importante, la antiquísima Biblioteca de Galador.\n"
    );
    set_zone("galador_muralla");
	add_exit(E , GAL_CIU_MURALLA + "10m" , "path");
	add_exit(O , GAL_CIU_MURALLA + "10k" , "path");
	add_exit(N , GALADOR + "9l"  , "path");
	add_exit(DE, GAL_CIU + "gal1", "gate");
    modify_exit(DE,({"function","pasar"}));
    add_clone(NPCS_SOLDADOS + "soldado_imperial.c", 10 + random(3));
    
}
#include "/d/dendra/include/paso_galador.h"
