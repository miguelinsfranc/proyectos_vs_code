#include "/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/base_campamento.h"
#include <depuracion.h>

inherit "/obj/monster";


void setup()
{
    set_name("Roderick");
    add_alias("roderick");
    set_short("Roderick Faucesombra");
    set_main_plural("Roderick Faucesombras");

    set_long(
        "Roderick Faucesombra, el líder de los bandidos, se alza imponente en medio del campamento. Su estatura considerable impone respeto a todos aquellos que se atreven a enfrentarlo. A sus cuarenta años, su cabello negro azabache se entremezcla con sutiles hebras plateadas, una señal de las muchas batallas que ha librado. Las cicatrices que surcan su rostro son un testimonio silencioso de los innumerables enfrentamientos que ha superado con ferocidad. Roderick viste una armadura de cuero oscuro, finamente adornada con detalles de metal bruñido. Esta protección le confiere seguridad sin comprometer su movilidad en combate. Sobre su espalda, una capa de color rojo sangre ondea al viento, añadiendo un toque siniestro a su figura imponente. Cada paso que da emana una presencia dominante, dejando en claro que él es el líder indiscutible de esta banda de bandidos.\n"
    );

fijar_clase("soldado");
fijar_raza("humano");
    fijar_bando("mercenario");
    fijar_religion("ateo");
    fijar_estatus("Ak'Anon",-200);
    fijar_alineamiento(30000);
    fijar_genero(1);

    fijar_fue(28);
    fijar_des(15);
    fijar_con(22);
    fijar_int(18);
    fijar_sab(15);
    fijar_car(25);

	ajustar_bo(162);
	ajustar_be(88);
	ajustar_bp(419);
ajustar_armadura_natural(50);

fijar_carac("daño", 200);
fijar_pvs_max(50000);

    load_chat(20,
      ({
           "'Toma un poco de mi látigo.'",
           "'La magia es poderosa y sagrada, es mejor no abusar de ella.",
           "'Mis movimientos son rápidos y letales.'",
      }));

    add_clone(ARMASBASE + "hacha_a_dos_manos", 1);
add_clone(ARMADURASBASE + "bandas", 1);    
add_clone(ARMADURASBASE + "brazalete_mallas", 1);    
add_clone(ARMADURASBASE + "brazalete_mallas_izq", 1);    
add_clone(ARMADURASBASE + "capa", 1);    
add_clone(ARMADURASBASE + "guante_cuero", 1);    
add_clone(ARMADURASBASE + "guante_cuero_izq", 1);    
add_clone(ARMADURASBASE + "cinturon", 1);    
add_clone(ARMADURASBASE + "grebas_mallas", 1);    
add_clone(ARMADURASBASE + "botas_campaña", 1);    

fijar_maestrias((["Cortantes pesadas": 100]));



    set_aggressive(10, 2);

    fijar_nivel(77);

    anyadir_ataque_pnj(100, "vortice");
    anyadir_ataque_pnj(90, "giro", 0, (["objetivo": 3]));
    anyadir_ataque_pnj(85, "pulverizar", 0, (["objetivo": 3]));
    anyadir_ataque_pnj(100, "hender", 0, (["objetivo": 4]));
anyadir_ataque_pnj(
        100,
        "furia",
        (
            : !query_static_property("enfurecido")
            :));
    


    init_equip();
}


void heart_beat()
{
	mixed datos;
	datos=query_spell_effect("vortice");
	if(datos) {
		__debug_x("VORTICE en turno "+datos[0], "grimaek");
		if ( datos[0]<=1) {
			__debug_x("FIN VORTICE", "grimaek");
		    habilidad("vortice","");
		
		}
	}
	
	::heart_beat();
}