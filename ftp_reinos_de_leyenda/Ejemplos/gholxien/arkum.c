// Durmok 11/04/2003
//  Satyr 2k7, restringo xp

#include "/d/kheleb/path.h"
inherit "/obj/monster";
// Kheleb abierto por tunel de Nebhur
//#include <npc_xp_restringida.h>

void setup()
{
    set_name("arkum");
    add_alias(({"arkum", "guardian", "soldado", "guardia"}));
    add_alias("lider");
    add_plural(({"arkum", "guardias", "guardianes", "soldados"}));
    set_short("Arkum, el Líder Duergar");
    set_main_plural("Soldados Duergar");
    set_long(
        "Arkum es un duergar peligroso y despiadado, lleno de malicia y de "
        "rencor. Aprovechó el "
        "Cataclismo y la huída de Bendorf para esclavizar a parte de su pueblo "
        "y hacer que le sirvan. "
        "Es temido y respetado por la pequeña comunidad que esclaviza. Tiene "
        "una larga barba grisácea "
        "y la cara cubierta con tatuajes y perforaciones con pendientes.\n");
    fijar_raza("duergar");
    fijar_bando("malo");
    fijar_religion("valkar");
    fijar_estatus("Kheleb", -500);
    fijar_clase("khazad");

    fijar_alineamiento(10000);
    fijar_fe(60);
    fijar_genero(1);

    fijar_fue(21);
    fijar_carac("des", 16 + random(3));
    fijar_carac("con", 10 + random(9));
    fijar_carac("int", 12 + random(7));
    fijar_carac("sab", 10 + random(9));
    fijar_carac("car", 14 + random(5));

    load_chat(
        20,
        ({
            "'Aquí se hace lo que yo ordeno.",
            ":te mira con superioridad.",
            "'Estoy preparando un ataque a Kheleb Dum...¡van a morir esos "
            "sucios enanos!",
        }));

    add_clone(BARMADURAS + "placas_enana", 1);
    add_clone(BARMADURAS + "yelmo", 1);
    // add_clone(BARMADURAS+"pantalones",1);;
    add_clone("/baseobs/escudos/escudo.c", 1);
    if (!random(10))
        add_clone(ARMADURAS + "brazal_hierro.c", 1);
    if (!random(10))
        add_clone(ARMADURAS + "grebas_hierro.c", 1);
    add_clone(BARMADURAS + "botas", 1);
    add_clone(BARMAS + "hacha_de_batalla", 1);

    add_loved("raza", ({"duergar"}));
    add_hated("bando", ({"bueno"}));
    set_aggressive(6, 0);

    fijar_nivel(50 + random(16));
    anyadir_ataque_pnj(
        65,
        "furia",
        (
            : !query_static_property("enfurecido")
            :));
    anyadir_ataque_pnj(30, "embestir");
    anyadir_ataque_pnj(30, "golpecertero");
    init_equip();

    ajustar_dinero(1 + random(4), "khaldan");

    fijar_objetos_morir(({"tesoro", "armadura", ({1, 2}), 1}));
    fijar_objetos_morir(({"tesoro", "varios", ({1, 2}), 1}));
}
