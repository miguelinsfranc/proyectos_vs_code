// Snaider 10-IX-2005 Bestiario: Antoleon

/*Descripción:
Los antoleones son seres que viven totalmente aislados del resto de su especie.
Por ello su reproducción es muy casual y se encuentran muy poco en todo el
mundo. Es mitad hormiga y mitad araña, la zona del abdomen para arriba
corresponde a una forma arácnida, mientras que la parte inferior conserva las
caracteristicas de una hormiga Clima: Seco, principalmente desierto Frecuencia:
Muy infrecuente Organización: Solitarios Dieta: Carnívoro Inteligencia:
Animal(1) Tesoro: el que haya entre los esqueletos de sus victimas Número por
room: 1 Número en zona: 1 Nº ataques: 1 (+ veneno) Daño ataque: 1D6 Tamaño: 2
metro (2)
*/

inherit "/obj/monster.c";

void inyectar_veneno()
{
    object *atacantes = query_attacker_list(), objetivo;
    int     i         = sizeof(atacantes);

    if (!i)
        return;
    objetivo = atacantes[random(i)];
    // Tirada de salvación
    if (random(
            objetivo->dame_carac("des") + objetivo->dame_nivel() +
            objetivo->dame_be()) > random(this_object()->dame_bo() + 200))

        return;
    tell_object(
        objetivo,
        this_object()->query_short() +
            " te atrapa entre sus patas delanteras y te levanta del suelo.\n");
    tell_accion(
        ENV(objetivo),
        TO->query_short() + " atrapa a " + objetivo->query_short() +
            " con sus patas delanteras y lo levanta del suelo.\n",
        "",
        ({objetivo}),
        objetivo);
    objetivo->add_incapacidad(
        "circulatorio_daño", "venenos", 5 + dame_nivel(), 120, 30 + random(20));
}
void levantar()
{
    object *atacantes = query_attacker_list(), objetivo;
    int     i         = sizeof(atacantes);

    if (!i)
        return;
    objetivo = atacantes[random(i)];
    // Tirada de salvación
    if (random(
            objetivo->dame_carac("des") + objetivo->dame_nivel() +
            objetivo->dame_be()) > random(this_object()->dame_bo() + 200)) {
        tell_object(
            objetivo,
            "%^RED%^*%^RESET%^ " + this_object()->query_short() +
                " te intenta atrapar entre sus patas pero logras "
                "esquivarle.\n");
        tell_accion(
            ENV(objetivo),
            TO->query_short() + " intenta atrapar a " +
                objetivo->query_short() + " pero este se escapa por poco.\n",
            "",
            ({objetivo}),
            objetivo);
        return;
    } else {
        tell_object(
            objetivo,
            "%^RED%^*%^RESET%^ " + this_object()->query_short() +
                " te atrapa entre sus patas delanteras y te levanta del "
                "suelo.\n");
        tell_accion(
            ENV(objetivo),
            TO->query_short() + " atrapa a " + objetivo->query_short() +
                " con sus patas delanteras y lo levanta del suelo.\n",
            "",
            ({objetivo}),
            objetivo);
        objetivo->bloquear_accion(
            "No puedes hacer nada en esta situación.\n", 6);
    }
}

void lanzar()
{
    object *atacantes = query_attacker_list(), objetivo;
    int     i         = sizeof(atacantes);

    if (!i)
        return;
    objetivo = atacantes[random(i)];
    // Tirada de salvación
    if (random(
            objetivo->dame_carac("des") + objetivo->dame_nivel() +
            objetivo->dame_be()) > random(this_object()->dame_bo() + 200)) {
        tell_object(
            objetivo,
            this_object()->query_short() +
                " te intenta empujar con sus patas pero logras esquivarle.\n");
        tell_accion(
            ENV(objetivo),
            TO->query_short() + " intenta empujar a " +
                objetivo->query_short() + " pero este se escapa por poco.\n",
            "",
            ({objetivo}),
            objetivo);
        return;
    } else {
        tell_object(
            objetivo,
            this_object()->query_short() +
                " se levanta y te empuja contra el suelo.\n");
        tell_accion(
            ENV(objetivo),
            TO->query_short() + " se levanta y empuja a " +
                objetivo->query_short() + " contra el suelo.\n",
            "",
            ({objetivo}),
            objetivo);
        objetivo->ajustar_pvs(-random(50));
        objetivo->bloquear_accion("Te estas levantando.\n", 6);
    }
}
void setup()
{
    set_name("antoleon");
    set_short("Antoleon");
    set_main_plural("Antoleones");
    set_long(
        "Se trata de un ser mitad araña mitad hormiga, de unos dos metros "
        "de largo y metro y medio de alto. Las patas traseras  se encuentran "
        "perfectamente enfiladas a un largo y negro abdomen. La "
        "cabeza de este horroroso ser está formada por 4 grandes ojos, una "
        "gran "
        "boca y un amasijo de colmillos y patas de menor tamaño.\n");
    add_alias(({"antoleon", "cria", "cría"}));
    add_plural(({"antoleones", "crias", "crías"}));
    fijar_genero(1);
    fijar_raza("aracnido");
    fijar_peso(150000);
    fijar_tamanyo_racial(5);

    fijar_fue(18 + random(10));
    fijar_des(22 + random(10));
    fijar_con(15 + random(10));
    fijar_int(2);
    fijar_sab(1);
    fijar_car(random(3));

    fijar_bo(300);
    adjust_tmp_damage_bon(100);

    set_aggressive(3, 5);
    anyadir_ataque_pnj(40, "veneno", ( : inyectar_veneno() :));
    anyadir_ataque_pnj(70, "levantar", ( : levantar() :));
    anyadir_ataque_pnj(70, "lanzar", ( : lanzar() :));

    fijar_nivel(50 + random(20));
    // Xp adicional por el veneno
    INICIAR_XP_MORIR;

    fijar_objetos_morir(({"tesoro", "varios", ({3, 4}), 1}));
}
