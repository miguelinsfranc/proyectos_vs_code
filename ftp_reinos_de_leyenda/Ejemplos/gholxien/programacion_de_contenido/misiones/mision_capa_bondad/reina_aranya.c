//Keiron Enero 2005
//Kaitaka ENE2013 - Ahora si te atrapa a distancia te arrastra a su room.

inherit "/obj/monster.c";
#include "/d/kheleb/path.h"
#define SHADOWS "/hechizos/shadows/"


void escupir_tela()
{
        object *atacantes= query_attacker_list(),objetivo,sombra;
        int i= sizeof(atacantes);

        if(!i) return;
        objetivo= atacantes[random(i)];
        if(random(objetivo->dame_carac("des")+objetivo->dame_nivel()+objetivo->dame_bd())>random(this_object()->dame_bo()+200)) 
        {
            tell_accion(ENV(objetivo),TO->query_short()+" lanza una telaraña de forma imprevista pero no logra capturar a nadie.\n","",({objetivo}));
                return;//Tirada de salvación superada
        }

        tell_accion(ENV(TO),"¡¡"+TO->query_short()+" se dispone a lanzar una telaraña!!\n","",({objetivo}));

        if (environment(objetivo)!=environment())
        {
            tell_object(objetivo,this_object()->query_short()+" te atrapa con una fuerte telaraña aprovechando que estabas distraído y te lleva rápidamente hasta su posición.\n");
            tell_accion(ENV(objetivo),TO->query_short()+" atrapa con una fuerte telaraña a "+objetivo->query_short()+" que pensando que estaba a salvo había bajado la guardia y lo arrastra hacia su posición.\n","",({objetivo}),objetivo);
            tell_accion(ENV(TO),TO->query_short()+" atrapa con una fuerte telaraña a "+objetivo->query_short()+" que pensando que estaba a salvo había bajado la guardia y lo arrastra hacia aquí.\n","",({objetivo}),objetivo);
            objetivo->move_player("algún lugar",environment(TO));
        }
            
        tell_object(objetivo,this_object()->query_short()+" te atrapa con una fuerte telaraña.\n");
        tell_accion(ENV(objetivo),TO->query_short()+" atrapa con una fuerte telaraña a "+objetivo->query_short()+".\n","",({objetivo}),objetivo);
        sombra= clone_object(SHADOWS+"retener_sh");
        if (sombra && objetivo) 
        {
            sombra->setup_sombra(objetivo,roll(2,4));
                objetivo->add_extra_look(sombra);
        }
        
}

void setup()
{
    set_name("araña");
    set_short("Araña Reina");
    set_main_plural("Arañas reina");
    set_long("Observas una temible araña gigante de mas de cuatro metros de ancho y ocho de largo con "
        "grandes patas alrededor de su abdomen inservibles, dado que por su voluminoso peso no "
        "podría moverse por si sola. Tiene gran cantidad de ojos rojos que te miran con una "
        "grandes ansias de comida. No te descuides mucho o sera muy posible que acabes en el "
        "estómago de esta reina araña.\n");
    add_alias(({"araña","aranya","reina"}));
    add_plural(({"arañas","aranyas","reinas"}));
    fijar_genero(2);
    fijar_raza("aracnido");
    fijar_peso(100000);
    fijar_tamanyo_racial(2);
    fijar_estatus("Kheleb",-500);


    fijar_fue(roll(10,6)+5);
    fijar_carac("des",20+random(9));
    fijar_carac("con",10+random(10));
    fijar_carac("int",4);
    fijar_carac("sab",8);
    fijar_carac("car",roll(3,6));

    set_aggressive(1,5);
    add_loved("raza",({"duergar","drow","semi-drow","animal/aracnido"}));
    add_attack_spell(40,"telaraña",(:escupir_tela():));

    fijar_nivel(50+random(9));

    fijar_objetos_morir(({"tesoro", ({"arma","armadura"}), 1, 1 }));
    fijar_objetos_morir(({"tesoro", "varios"    , 1, 1 }));
}

int do_death(object quien)
{
    object ob;
    tell_accion("Al morir "+TO->query_short()+" observas como de su abdomen comienza a salir seda pura.\n");
    //Ember, evitando q pete. Demote Snaider!! xD
    ob = clone_object("/baseobs/componentes/hilo_araña.c");
    
    if(ob) {
        ob->move(ENV(TO));
        if (quien && quien->dame_mision_abierta("capa_bondad")) {
            quien->nuevo_hito_mision("capa_bondad", "tengo_hilo", 0);
        }
    }
    return ::do_death(quien);
}