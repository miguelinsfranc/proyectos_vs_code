//Keiron 2005
//Sierephad Nov 2021	

inherit "/std/item.c";

#include <dosis.h>

void setup () 
{
        
	set_name("hilo");
	set_short("Hilo de Seda de Araña");
	set_main_plural("Hilos de Seda de Araña");
	add_plural("hilos");
	set_long("Observas el hilo natural mas fino y bello que tus ojos han apreciado jamás, sacado "
			 "de la seda de una reina araña con el que se podrían confeccionar los mas bellos ropajes nunca vistos por ningún ojo mortal. "
			 "Su suavidad, y su flexibilidad, lo hacen uno de los ingredientes más solicitados por todos los sastres, y se dice que las prendas "
			 "confeccionadas con este hilo, adquieren una resistencia increíble.\n");
	fijar_valor(1500);
		
}
