// Istanor Mayo'04
// Retocado por Keiron Nov 2004
// Satyr 2012 -- Adaptándolo a misión
// Kaitaka 28Nov2012 -> ya no da xp, lo hace el handler de misiones.
// Kiruk Diciembre 2013 Nueva misión añadida /baseobs/misiones/takome/capa_ilhan.c
// Sierephad Oct 2021	--	ajustando el hilo de araña como objeto de dosis

#include <tiempo.h>
#include "/d/takome/path.h"
#include "/d/takome/npcs/humano_takome.c"

#define RUTA_CAPA_ROTA "/d/takome/items/capa_rota.c"

//Función que retarda el do_say Así no saltan todos los do_say instantaneamente.
void habla(string tx,int i)	{ call_out("do_say",i,tx); }

void setup()
{
    personaje();
    set_name("ilhan");
    set_short("Ilhan, el Sastre");
    
    add_alias(({"sastre", "ilhan"}));
    add_plural(({"sastres", "ilhan"}));
	    
    set_long(
        "Se trata de un humano de aspecto delgado y rubio. Vestido con unos pantalones a cuadros y una camisa roja, su gusto"
        " parece muy extrovertido. Posee una larga tira de cuero alrededor del cuello, que usa para medir sus trajes. Este"
        " sastre, es muy reconocido en la ciudad de Takome, y muchos nobles acuden aquí para que Ilhan, les confeccione sus"
        " mejores galas.\n"
    );
  
    load_chat(30, ({
        "'¿Os gusta mi estilo?",
        ":observa orgulloso sus prendas.",
        ":le quita las dobladuras a uno de los trajes."
    }));
    
    add_clone(BARMADURAS + "amuleto.c", 1);
    add_clone(BARMADURAS + "manto.c"  , 1);
	add_clone(ARMADURAS + "botas_ilhan.c", 1);
	add_clone(ARMADURAS + "pantalon_ilhan.c", 1);
	add_clone(ARMADURAS + "tunica_ilhan.c", 1);
	add_clone(ARMADURAS + "brazal_ilhan", 1);
	add_clone(ARMADURAS + "brazal_izq_ilhan.c", 1);
	add_clone(ARMADURAS + "guante_ilhan.c", 1);
	add_clone(ARMADURAS + "guante_izq_ilhan.c", 1);
	add_clone(ARMADURAS + "capucha_ilhan.c", 1);
    init_equip();
	
    nuevo_dialogo("nombre", ({
        "'Hola, me llamo Ilhan, y soy el sastre más diestro que existe en el reino.",
		"'Aunque ahora me invade una tristeza muy grande, no sé si podría elaborar buenos productos."}
    ));
    nuevo_dialogo("objeto", ({
        "'Bien, parece que Ehnbar te a mandado a mi. Es un hombre viejo y deteriorado, ¡pero un gran guerrero!",
        "'Bueno, a lo que íbamos, escuchando relatos de viejos caballeros antiguamente cada caballero portaba una capa con su "
        "emblema de armas grabado.",
        "'Podria intentar hacer una capa para usted, pero necesitaria algo de material."
    }), "nombre");
    nuevo_dialogo("material", ({
        "'Necesito hilo de seda de araña, un hilo especial extraído cuidadosamente desde una seda de una araña reina. "
		"Con este hilo podré fabricar tu capa, la cual tendrá una calidad y condición espectacular.",
        "'Solamente conozco un nido de estas arañas por el sudoeste del continente, pero ya no se bien donde.","'Deberas buscarlo y traerlo. Suerte caballero.",
		"hito capa_bondad busca_aranyas"
    }),"objeto");
	
	nuevo_dialogo("hilo", ({
			"dialogo_hilo"
		}));
		
	prohibir("hilo","puedo_conversar_hilo");
	deshabilitar("hilo","hito capa_bondad espera_capa");	
	
	nuevo_dialogo("fabricacion", ({
		"'Estoy tejiendo tu capa.",
		"decir_tiempo"
	}));
    nuevo_dialogo("entregar", ({
        "'Ha quedado mucho mejor de lo que yo pensaba, es una gran capa, ¡disfrutala!", 
		"dar_capa"
    }));
    nuevo_dialogo("tristeza", ({
		"'Unos ladrones me han atracado y se han llevado varias de mis valiosas prendas.",
		":entrecierra los ojos entristecido.",
		"'Sobre todo me gustaría volver a tener una de mis capas, tiene unos bordados difíciles de reproducir si no los veo.",
		"'Sospecho que quienes me atracaron pudieron ser las bandas de ladrones que se esconden en los arrabales.",
		"'Ojalá alguien me pudiera ayudar a encontrar aunque sólo fuera una de las capas que me robaron."
	}), "nombre");
	nuevo_dialogo("ayudar", ({
		"'¿De verdad? Muchas gracias.",
		"'Si consigues dar con algo de lo robado avísame.",
		"hito takome_capa_ilhan buscar_capa"
	}), "tristeza");
	nuevo_dialogo("recuperar", ({
		"entrega_capa_rota"
	}) );
	nuevo_dialogo("cuero", ({
		"'Has conseguido el cuero rojo, genial.",
		"'Espera un momento, esto es suficiente cuero para arreglar la capa y hacer otra.",
		":sonríe muy feliz.",
		":desenrolla el cuero rojo que le has traído y empieza dar unas puntadas.",
		":parece concentrado en el cuero y la capa.",
		":sigue reparando la capa.",
		"'Ya casi la tengo completamente reparada.",
		":da unas precisas puntadas a la capa.",
		":sonríe satisfecho al terminar la Capa de %^RED%^Ilhan%^RESET%^.",
		"hito takome_capa_ilhan capa_terminada"
	}) );
	
	//Prohibires de la misión Capa de Ilhan
	deshabilitar("tristeza","hito takome_capa_ilhan capa_encontrada");
	deshabilitar("ayudar", "hito takome_capa_ilhan capa_encontrada");
	
	deshabilitar("recuperar","hito takome_capa_ilhan buscar_cuero");
	prohibir("recuperar","hito takome_capa_ilhan capa_encontrada");
	
	deshabilitar("cuero","hito takome_capa_ilhan capa_terminada");
	prohibir("cuero","hito takome_capa_ilhan cuero_conseguido");
	
	
	//Prohibires de la Misión Capa de la Bondad
	prohibir("entregar", "puede_entregar_capa");
	prohibir("fabricacion", "dialogo_durante");
    prohibir("objeto"  , "jugador_correcto");  
	
    habilitar_conversacion();
}

void sigue_dialogo2(object jugador)
{
	tell_object(jugador,"Ilhan te paga 13 monedas de sesioms.\n");	
	jugador->ajustar_dinero(13,"sesiom");
	habla("No te demores por favor.",3);
}

void sigue_dialogo(object jugador)
{
	jugador->do_say("Por supuesto amigo Ilhan, volveré en cuanto pueda.\n",0);	
	habla("Muchas gracias por ofrecerte a ayudarme.",2);
	habla("Toma unas monedas para que le pagues el cuero a Fiona",4);
	call_out("sigue_dialogo2",6,jugador);
}

void entrega_capa_rota(object jugador)
{
	object capa_rota;
	if (capa_rota=secure_present(RUTA_CAPA_ROTA,jugador))
	{
		jugador->do_say("He recuperado una capa, creo que es una de las tuyas pero está destrozada.\n");
		tell_object(jugador,"Entregas la capa rota a Ilhan.\n");
		tell_room (this_object(),jugador->query_short()+" entrega algo a Ilhan.",jugador+".\n");
		capa_rota->dest_me();
		jugador->nuevo_hito_mision("takome_capa_ilhan", "buscar_cuero", 0);
		habla("Sí es de las mías. Una lástima el estado en el que la han dejado esos malditos ladrones.",2);
		habla("Podría repararla, pero neceisto cuero rojo y no me queda.",4);
		habla("Mi amiga Fiona, la sastre de Veleiron seguro que tiene. ¿Me harías el favor de ir a buscar un poco?",6);
		call_out("sigue_dialogo",8,jugador);
	}
	else
	{
		jugador->do_say("Había recuperado la capa, pero la he perdido, te prometo que la encontraré.");
	}
}

int decir_tiempo(object b)
{
	if (b)
		do_say("Necesitarás estár conectado " + pretty_time(b->query_time_remaining("tejiendo_capa_bondad")) + " para que termine tu capa.", 0);
}
status puede_entregar_capa(object player)
{
	return player &&
		 player->dame_hito_mision("capa_bondad", "espera_capa") &&
		!player->query_timed_property("tejiendo_capa_bondad") &&
		!player->dame_hito_mision("capa_bondad", "capa_terminada");
		// &&!player->dame_mision_completada("capa_bondad");
}
status dialogo_durante(object player)
{
	return player &&
		!player->dame_mision_completada("capa_bondad")          &&
		 player->dame_hito_mision("capa_bondad", "espera_capa") &&
		 player->query_timed_property("tejiendo_capa_bondad");
	;
}
status jugador_correcto(object player)
{
    return !player->dame_mision_completada("capa_bondad") && 
			player->dame_hito_mision("capa_bondad", "busca_ilhan");
}
int dar_capa(object player)
{
	if (player->entregar_objeto("/clases/caballeros/paladin/armaduras/capa_bondad.c")) {
		write_file(LOGS + "capa_bondad.log", ctime() + " " + player->query_cap_name()+" recibio una capa.\n");
		do_say("Ahí la tienes, " + player->dame_nombre_completo() + ". Ha quedado mucho mejor de lo que me esperaba.", 0);
		
//                player->ajustar_xp(50000 + random(25000));
		player->nuevo_hito_mision("capa_bondad", "capa_terminada", 0);
    }
	else {
		tell_object(player, "Un error impide cargar tu capa bondad.\n");
	}
	
    return 1;
}



// Sierephad Oct 2021	--	ajustando el hilo de araña como objeto de dosis
//						--  cambiando el event dar por un tema de concersacion
int puedo_conversar_hilo(object pj) 
{

	if (!pj || !ENV(pj))
		return 0;
	
	if (ENV(TO)!=ENV(pj))
        return 0;
	 
	if (
		pj->dame_mision_completada("capa_bondad") || 
		pj->dame_hito_mision("capa_bondad", "espera_capa")
	) return 0;
	
	
	if (pj->dame_hito_mision("capa_bondad", "tengo_hilo"))
		return 1;
	
	return 0;
}

void dialogo_hilo(object pj,string tema)
{

	object hilo;

	if (!pj || !ENV(pj)) 
		return;	
	
	if (ENV(pj)!=ENV(TO))
		return;

	if (!hilo=secure_present(BCOMPONENTES + "hilo_seda_araña",pj)) {
		do_emote("niega rotundamente con la cabeza.");
		do_say("¿Aun no has conseguido ese hilo de araña?",0);
		do_say("Ve a buscar una reina araña por el suroeste de Dalaensar y consíguelo.",0);
		return;
	}

	do_emote("asiente satisfecho mientras aplaude ligeramente.");
	do_say("¡Bien! Ya puedo ponerme manos a la obra.", 0);
	do_say("Vuelve dentro de dos días, entonces la tendré acabada.", 0);
	
	if (hilo->dame_dosis()) {
		hilo->ajustar_dosis(-1);
	} else {
		hilo->dest_me();
	}
	pj->add_timed_property("tejiendo_capa_bondad", 1, DIA);
	pj->nuevo_hito_mision("capa_bondad", "espera_capa", 0);

	return;
}


/*
int event_dar(object quien, object* que)
{
	if (!que || sizeof(que) > 1 || base_name(que[0]) != BCOMPONENTES + "hilo_seda_araña") {
		do_say("Eso no me interesa, " + quien->query_cap_name() + ".", 0);
	}
	else if (!quien->dame_mision_completada("capa_bondad") && quien->dame_hito_mision("capa_bondad", "tengo_hilo")) {
		do_say("¡Bien! Ya puedo ponerme manos a la obra.", 0);
		do_say("Vuelve dentro de dos días, entonces la tendre acabada.", 0);
		
		quien->add_timed_property("tejiendo_capa_bondad", 1, DIA);
		quien->nuevo_hito_mision("capa_bondad", "espera_capa", 0);
		return 1;
	}
	else {
		do_say("No quiero nada de eso, pero gracias." ,0);
	}
	
	return 0;
}
*/

