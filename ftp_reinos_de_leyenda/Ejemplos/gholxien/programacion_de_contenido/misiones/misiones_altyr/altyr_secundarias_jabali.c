// 2021.03.25 [gholxien]: Misión secundaria del capítulo 1 de la campaña del gremio Ralder


inherit "/baseobs/misiones/base";

void setup()
{
    fijar_nombre("¡Qué festín, qué festín!");
	fijar_tipo(2);	// Belica
	fijar_dificultad(2); // Muy fácil

	//Opcional
    //nuevo_hito("pista_inicial",
        //"Has oído hablar del Círculo del Simbionte, una organización que da cobijo a los "
        //"seguidores mercenarios de los dioses simbióticos, Ralder y Naphra, y opera por toda "
        //"Eirea en defensa de la armonía natural, protegiendo sus territorios sagrados y los "
        //"lugares naturales salvajes de la influencia devastadora de la civilización.\n\n"
        
        //"Para obtener más información sobre el gremio, encuentra a su líder, el druida elfo "
        //"Shihon. Suele viajar entre los territorios sagrados del Simbionte.\n\n", 
        
        //0);

    //Requerido
    nuevo_hito("requerimento_jabali", 
"Has conversado con el cocinero del asentamiento, el cual está preparando todo para un gran festín, "
"pero le falta el plato principal con que deleitar a su clan, un par de rollizos y jugosos jabalíes.\n\n"

"Bambo considera que tú podrías ayudarle y te ha asignado la importante misión de ir a cazar un par de estos ejemplares al bosque de Maragedom.\n\n",

        0);

    nuevo_hito("obtencion_jabali", 
"Has conseguido cazar dos buenos ejemplares de jabalí. " 
"Te alzas a cada hombro las piezas cobradas y crees que Bambo se sentirá bastante complacido al verlos.\n\n"

 "Recuerda que debes ir lo antes posible a entregarlos, pues cuanto más fresca esté la carne, mejor sabrá.\n\n",

        0);

    nuevo_hito("entregar_jabali", 
        "Has entregado los jabalíes a Bambo y has completado tu misión."
		"Se te hace la boca agua mientras el cocinero prepara las piezas y esperas que te inviten al festín.\n\n",
       
        0);	

    

    fijar_final_obligatorio(({"entregar_jabali"}));

	nueva_recompensa_xp(30000, 0);
}

int chequeo_pj(object pj)
{
    return 0;
}
