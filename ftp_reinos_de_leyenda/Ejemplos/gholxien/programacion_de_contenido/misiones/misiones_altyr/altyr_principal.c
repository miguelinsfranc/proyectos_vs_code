//08.04.2021 [Gholxien]: Misión principal de la campaña de Ralder


inherit "/baseobs/misiones/base";

void setup() {
    fijar_nombre("La poza de la luna pálida");
    fijar_tipo(5); // Mixta
    fijar_dificultad(4); // Normal

    //Requerido
    nuevo_hito("conversacion_shihon_1",
        "Has hablado con Shihon y te ha dicho que tiene una misión muy importante. "
        "No ha entrado en muchos detalles, pero te ha dicho algo de unos lobos corruptos o algo por el estilo.\n\n",

        "Te ha pedido que le traigas tres muestras. Una del agua de la poza, otra de la sangre de los lobos y la tercera, de la sangre de los heridos.",
        "También te ha dicho que busques a Radkum en la parte occidental de Wareth y que le acompañes a su campamento, él te contará toda la historia.\n\n",

        0);

    nuevo_hito("conversacion_radkum_1",
        "Has encontrado a Radkum, un niño que apenas alcanzaría los doce años, "
        "y te ha dicho que en estos tiempos cualquier ayuda es necesaria.\n\n",

        "Te has ofrecido para ayudarle 	y ha aceptado. Te ha dicho que le sigas a su campamento, "
        "y si le pierdes el rastro estará esperándote en el mismo lugar donde lo encontraste.\n\n",

        0);

    nuevo_hito("acompañamiento_Radkum",
        "Has acompañado a Radkum todo el camino hasta el asentamiento y ahora tienes "
        "una idea más concreta de lo que les ha sucedido.\n\n",

        "Has atravesado el bosque donde se encuentra la poza a la que tienes que ir a recoger el agua, así que ya sabes "
        "como encontrarla. También has conocido a la actual líder del asentamiento, Alug y  no parece tan amistosa como Radkum.\n\n",

        0);

    nuevo_hito("obtencion_muestras_agua_lobos",
        "Has conseguido dos de las tres muestras que Shihon te ha solicitado."
        "Aún deberás tomar una muestra de la sangre de los enfermos, pero para ello tendrás que conseguir que Alug confíe en ti.\n\n",

        "Sería buena idea ir a conversar con ella.\n\n",

        0);

    nuevo_hito("conversacion_alug_1",
        "Has vuelto de tu aventura en la poza plateada y has hablado con Alug, "
        "contándole con todo lujo de detalles lo sucedido allí.\n\n",

        "Alug te ha dicho que puedes ayudar a los diferentes miembros del campamento y así podrá decidir si confía del todo en ti o no. "
        "Habla con cada uno de los miembros del clan y ayúdales en lo que te pidan.\n\n",

        0);

    nuevo_hito("hechas_altyr_secundarias",
        "¡Por fin! Has hecho un duro trabajo buscando las piedras para el horno y cargándolas hasta el asentamiento, "
        "yendo a buscar las plantas medicinales, instruyendo a Alug en combate y cazando sabrosos jabalíes para el festín, pero no importa, "
        "habrá merecido la pena si por fin Alug te deja ver a los enfermos.\n\n",

        "Ve y habla con ella al respecto.\n\n",

        0);

nuevo_hito("permiso_alug",
        "Parece que Alug empieza a confiar mas en ti, por lo que "
        "te ha dejado entrar un rato a la yurta de los enfermos.\n\n",

        "Ya puedes entrar a la yurta, ¡pero procura no cansarlos o Alug se enfadará!\n\n",

        0);

    nuevo_hito("obtencion_muestra_enfermos",
        "Finalmente ganaste la suficiente confianza de Alug para dejarte ver a los enfermos y "
        "has podido tomar una muestra de sangre de ellos.\n\n",

        "Ahora ya tienes las tres muestras que te pidió Shihon, por lo que deberías ir lo antes posible a llevárselas.\n\n",

        0);

    nuevo_hito("entregar_muestras",
        "Has llegado hasta Shihon con las tres muestras recogidas y muy orgulloso de ti mismo, se las has entregado. "
        "Shihon ha estado observándolas por un largo rato, muy interesado y meditabundo. Finalmente ha llegado a la conclusión, "
        "que hay algo en el fondo de la poza que contamina el agua."
        "Para evitar males mayores, te ha pedido que vayas hasta allí y saques lo que sea que hay en sus profundidades.\n\n",

        "Vuelve a la poza plateada y bucea hasta recuperarlo. Recuerda que si no sabes nadar, antes deberás pedirle a alguno de los niños que te enseñe.\n\n",

        0);

    nuevo_hito("obtencion_arganita",
        "Has llegado hasta la Poza Plateada y tras una pequeña búsqueda buceando, has encontrado algo parecido a una roca. "
        "En cuanto la has cogido para sacarla de allí, has comenzado a sentirte mal y tus sentimientos y pensamientos han sido puestos a prueba. "
        "Con mucha dificultad, has alcanzado al fin la orilla, donde has tirado la roca. "
        "Con la caída, se ha desprendido un fragmento que has recogido y guardado para Shihon.\n\n",

        "Ahora deberás ir a hablar con él.\n\n",

        0);

    nuevo_hito("obtencion_cura",
        "as llevado a Shihon el fragmento de roca y este ha compartido contigo lo que ya sospechaba sobre lo que "
        "había hecho que los lobos cambiaran y porque los enfermos no curaban.\n\n",

        "Shihon ha terminado de preparar un remedio para los enfermos y te ha encargado llevárselo a la curandera del asentamiento del Oso Pálido, con las instrucciones para su uso.\n\n",

        0);

    nuevo_hito("entregar_cura",
        "Has llegado hasta el asentamiento y le has entregado el remedio a Enleri. Esta se ha sentido tan feliz que "
        "no ha podido ni articular palabra. Rápidamente ha ido a curar a los enfermos y te ha pedido que la sigas.\n",

        "En la yurta de los enfermos, Nhagte ha manifestado su agradecimiento tanto para Shihon como para ti y ha ofrecido a Radkum "
        "para ser un servidor del gremio y a ti te ha otorgado el mayor de los honores entre su clan.\n\n",

0);
        
		fijar_final_obligatorio(({"entregar_cura"}));

        nueva_recompensa_xp(300000, 0);
    }

    int chequeo_pj(object pj) {
        return 0;
    }
