// 2021.03.06 [Vhurkul]: Mision de inscripción en el Círculo del Simbionte


inherit "/baseobs/misiones/base";

void setup()
{
    fijar_nombre("Las plantas medicinales");
	fijar_tipo(2);	// Belica
	fijar_dificultad(2); // Muy fácil

	//Opcional
    //nuevo_hito("pista_inicial",
        //"Has oído hablar del Círculo del Simbionte, una organización que da cobijo a los "
        //"seguidores mercenarios de los dioses simbióticos, Ralder y Naphra, y opera por toda "
        //"Eirea en defensa de la armonía natural, protegiendo sus territorios sagrados y los "
        //"lugares naturales salvajes de la influencia devastadora de la civilización.\n\n"
        
        //"Para obtener más información sobre el gremio, encuentra a su líder, el druida elfo "
        //"Shihon. Suele viajar entre los territorios sagrados del Simbionte.\n\n", 
        
        //0);

    //Requerido
    nuevo_hito("apuros_enleri", 
        "Has hablado con Enleri y te ha confesado en los apuros en los que se encuentra. Ha probado con las pocas plantas que le quedan, con el fin de bajar la fiebre de los enfermos, pero no lo ha conseguido. "
"Ella cree que unas flores, con pétalos blancos y espinas rojas, podrían serle de utilidad para bajar la fiebre.\n\n"

"Te ha dicho que ha oído decir que se encuentran en el sendero que hay entre maragedom y Waret. Si le llevas 10 será suficiente.\n\n",

        0);

    nuevo_hito("obtencion_flores", 
"Logras hacerte con las diez flores que te ha dicho Enleri, aunque te has pinchado un par de veces ¡y pican mucho esas espinas!\n\n"

"Ahora deberás volver con Enleri para que empiece a elaborar una cura y poder bajar la fiebre.\n\n",


        0);

    nuevo_hito("entregar_flores", 
        "Entregas las flores a Enleri y sonríes mientras contemplas la felicidad y el alborozo de la pequeña.\n\n",
       
        0);	

    

    fijar_final_obligatorio(({"entregar_flores"}));

	nueva_recompensa_xp(30000, 0);
}

int chequeo_pj(object pj)
{
    return 0;
}
