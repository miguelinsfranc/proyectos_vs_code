// 2021.03.06 [gholxien]: Mision de recoger piedras para el horno


inherit "/baseobs/misiones/base";

void setup() {
    fijar_nombre("Construyendo el horno");
    fijar_tipo(2); // Logistica
    fijar_dificultad(2); // Muy fácil

    //Opcional
    //nuevo_hito("pista_inicial",
    //"Has oído hablar del Círculo del Simbionte, una organización que da cobijo a los "
    //"seguidores mercenarios de los dioses simbióticos, Ralder y Naphra, y opera por toda "
    //"Eirea en defensa de la armonía natural, protegiendo sus territorios sagrados y los "
    //"lugares naturales salvajes de la influencia devastadora de la civilización.\n\n"

    //"Para obtener más información sobre el gremio, encuentra a su líder, el druida elfo "
    //"Shihon. Suele viajar entre los territorios sagrados del Simbionte.\n\n",

    //0);

    //Requerido
    nuevo_hito("requerimiento_piedras",
        "Radkum ha observado que el cocinero no ha recogido suficientes piedras, y ahora "
        "están demasiado atareados como para ir a recoger más, pero tú seguro que podrás ayudarles.\n"

        "Radkum te ha dicho que en la orilla de la Poza Plateada, si cavas un poco, encontrarás piedras del tamaño adecuado. Van a ser necesarias al menos 10.\n",

        0);

    nuevo_hito("entregar_piedras",
        "Has entregado el cargamento de piedras con éxito. Radkum te ha felicitado muy contento. "
        "Te alejas disimuladamente mientras secas el sudor de tu frente, no vaya a ser que también te pidan que las metas en el hoyo.\n",

        0);

    fijar_final_obligatorio(({
            "entregar_piedras"
        }));

    nueva_recompensa_xp(30000, 0);
}

int chequeo_pj(object pj) {
    return 0;
}
