// 2021.03.06 [Vhurkul]: Mision de inscripción en el Círculo del Simbionte

#include <grupos.h>

inherit "/baseobs/misiones/base";

void setup()
{
    fijar_nombre("Savia nueva");
	fijar_tipo(3);	// Belica
	fijar_dificultad(2); // Muy fácil

	//Opcional
    nuevo_hito("pista_inicial",
        "Has oído hablar del Círculo del Simbionte, una organización que da cobijo a los "
        "seguidores mercenarios de los dioses simbióticos, Ralder y Naphra, y opera por toda "
        "Eirea en defensa de la armonía natural, protegiendo sus territorios sagrados y los "
        "lugares naturales salvajes de la influencia devastadora de la civilización.\n\n"
        
        "Para obtener más información sobre el gremio, encuentra a su líder, el druida elfo "
        "Shihon. Suele viajar entre los territorios sagrados del Simbionte.\n\n", 
        
        0);

    //Requerido
    nuevo_hito("explicar_pruebas", 
        "Shihon cree que tienes potencial para unirte al Círculo del Simbionte, pero "
        "tendrás que superar tres pruebas como requisito para tu aceptación.\n\n"
        
        "Habla con él en el Foso de la Garra, en los túneles bajo el Bosque de Wareth, para "
        "que te explique en qué consisten las pruebas.\n\n",

        0);

    nuevo_hito("prueba_valor_inicio", 
        "Shihon te ha ofrecido un lugar en la organización si logras completar tres pruebas "
        "de acceso.\n\n"
        
        "La primera prueba requiere demostrar tu valor al gremio. Algunos de los linces del "
        "Bosque de Wareth están desarrollando extrañas mutaciones, comúnmente tumoraciones "
        "parecidas a cuernos en la cabeza. Shihon quiere que caces algunos de esos linces y "
        "traigas cuatro de esos cuernos para poder examinarlos.\n\n"
        
        "Mata Linces del Bosque de Wareth hasta conseguir 4 cuernos.\n\n",

        0);

    nuevo_hito("prueba_valor_fin", 
        "Has cumplido la tarea que te encargó Shihon, reuniéndo cuatro cuernos de Lince y "
        "completando la prueba de valor.\n\n"
        
        "Reúnete con él en el Foso de la Garra, en los túneles bajo el Bosque de Wareth, para "
        "informarle de tu éxito.\n\n",
        
        0);	

    nuevo_hito("prueba_fe", 
        "Tras completar la prueba de valor, Shihon te ha revelado la segunda: demostrar tu "
        "fe en el Simbionte alcanzando la plataforma donde se encuentra la Garra cercenada "
        "del Gargante Sangreverde, %^YELLOW%^CURSIVA%^caminando al vacío%^RESET%^, si fuera "
        "necesario.\n\n"
        
        "Es importante que cumplas esta tarea demostrando tu fe y por tus propios medios. "
        "Shihon no aceptará trampas, por astutas que sean. Si tu fe flaquea, el druida elfo "
        "puede aconsejarte sobre cómo reforzarla.\n\n"
        
        "Alcanza la Garra cercenada en el Foso de la Garra, en los túneles bajo el Bosque "
        "de Wareth.\n\n",
        
        0);	

    nuevo_hito("prueba_sacrificio", 
        "Has conseguido acceder a la plataforma de la Garra cercenada, donde te mantienes "
        "en precario equilibrio.\n\n"
            
        "Para completar la última prueba deberás demostrar tu sacrificio al Simbionte, "
        "escarificando el símbolo de la garra en el dorso de tu propia mano y ofreciéndole "
        "tu sangre.\n\n"
        
        "%^YELLOW%^CURSIVA%^Escarifica tu mano%^RESET%^ junto a la Garra cercenada del "
        "Gargante Sangreverde, en los túneles bajo el Bosque de Wareth.\n\n",
    
        0);	

    nuevo_hito("completar", 
        "El poder de Ralder y Naphra ha entrado en tu cuerpo, sanando tus heridas, y ya "
        "siempre formará parte de ti. Es el momento de ocupar tu lugar como miembro del "
        "Círculo del Simbionte.\n\n"
            
        "Habla con Shihon en el Foso de la Garra, en los túneles bajo el Bosque de "
        "Wareth.\n\n",
    
        0);	

    nuevo_hito("mision_terminada", 
        "Shihon te ha felicitado por completar las tres pruebas y te ha dado la "
        "bienvenida como nuevo miembro del Círculo del Simbionte.\n\n", 
        
        0);	

    fijar_final_obligatorio(({"mision_terminada"}));

	nueva_recompensa_xp(30000, 0);
}

int chequeo_pj(object pj)
{
    return 
        pj && 
        pj->dame_gremio() == "sin_gremio" &&
        (pj->query_creator() || pj->query_avatar());
}
