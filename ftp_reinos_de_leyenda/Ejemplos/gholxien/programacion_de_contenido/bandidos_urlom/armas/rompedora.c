// Hazrakh 14/06/2022

inherit "/obj/arma.c";

void setup()
{
	fijar_arma_base("hacha a dos manos");
	set_name("hacha");
	add_alias(({"hacha","rompedora","manos","dos","hacha a dos manos"}));
	add_plural(({"hachas","rompedoras","hachas a dos manos"}));
	set_short("%^BOLD%^RED%^Rompedora%^RESET%^");
	set_main_plural("Rompedoras");
	set_long("Es una obra maestra de la forja, creada con precisión y dedicación utilizando el mejor acero disponible. Su diseño imponente exuda una presencia temible desde el primer vistazo. La hoja de esta poderosa arma presenta un filo doble, cada uno con una forma única y una función específica.\n"
	"El primer filo, recto y afilado, es una hoja perfectamente templada que brilla con una nitidez letal. Su diseño favorece los cortes limpios y precisos, permitiendo al portador del hacha atravesar cualquier oposición con facilidad. Ya sea para desmembrar a un enemigo o para abrir un camino en el campo de batalla, este filo recto es una herramienta formidable. El segundo filo, curvo y dentado, posee una apariencia salvaje y amenazadora. Sus bordes dentados y afilados son ideales para desgarrar y arrancar la carne de los oponentes. Cuando se utiliza con fuerza y ferocidad, este filo curvo se convierte en un instrumento devastador, infligiendo heridas profundas y causando estragos en las filas enemigas.\n"
	"Su mango es largo y grueso, proporcionando un agarre sólido y seguro. Está envuelto en cuero rojo intenso, un simbolismo siniestro que representa la sangre de las víctimas caídas. En el centro del mango, una empuñadura de metal detallada muestra un intrincado relieve que representa un dragón de tres cabezas con las fauces abiertas. Aunque el hacha tiene un peso considerable, su equilibrio meticuloso permite que se maneje con destreza y precisión. Se adapta a la mano de su dueño, brindando un control óptimo durante el combate. Más que un simple instrumento de muerte, esta arma es una herramienta de intimidación y dominio, capaz de infundir miedo en los corazones de aquellos que se enfrentan a su portador.\n");

	fijar_genero(2);
	fijar_encantamiento(50);
	fijar_valor(4000);

	nuevo_ataque("tabla", "cortante", 3, 62, 25);

	add_static_property("nivel_minimo", 20);
	add_static_property("messon", "El brillo amenazador de los filos dobles de esta arma se refleja en tus ojos.\n");

	nuevo_efecto_basico("penetracion", __ud(15, "%"));
	nuevo_efecto_basico("arrollar", 2, 0, 30);
	nuevo_efecto_basico("eviscerar", 50, 120, 180);
}
