// Hazrakh 17/06/2023

inherit "/obj/monster";

#include "base_campamento.h"


#define SHADOW_ESCONDERSE "/habilidades/shadows/esconderse_sh.c"
			void escondete()
			{
				if ( TO->dame_shadow_esconderse() ) {
					return;
				}

				clone_object(SHADOW_ESCONDERSE)->setup_sombra(this_object(),60,0);
				set_hidden(1);
			}

			function apunyalar_escondido(object *objetivos) {
				object objetivo = element_of(objetivos);
				/*TO->stop_fight(objetivo);
				objetivo->stop_fight(TO);*/
				tell_accion(environment(),query_short() + " retrocede hacia los árboles más cercanos y desaparece tras ellos.\n","",({TO}),TO);
				escondete();
				if (habilidad("apuñalar",objetivo)) add_timed_property("apunyaladoreciente",1,30);
			}

void setup() {
	int i = roll(1, 3);

	switch(i)
	{
		case 1:
			set_name("Patrullero Faucesombra");
			set_short("Patrullero Faucesombra");
			set_main_plural("Patrulleros Faucesombra");
			add_alias(({"bandido","patrullero", "bandido patrullero", "faucesombra"}));
			add_plural(({"bandidos", "patrulleros", "faucesombras"}));
			set_long("Está vestido con una indumentaria de cuero ceñida, y sus ropajes moldean su fibrosa musculatura, marcada por cicatrices que narran historias de incontables enfrentamientos en el estrato más bajo de la sociedad. El fulgor del acero se refleja en sus ojos, irradiando una peligrosa destreza mientras inspecciona todo a su alrededor\n."
			"Entre los bandidos que engrosan las filas de Roderick Faucesombra, estos Patrulleros destacan por su rigor y dedicación. Son los encargados de garantizar la vigilancia y seguridad del campamento, una tarea que llevan a cabo con disciplina ejemplar.\n");

	fijar_clase("soldado");
			fijar_nivel(40+random(10));
			set_random_stats(18,20);

			fijar_carac("daño", 20);

			anyadir_ataque_pnj(20, "golpecertero", 0, (["objetivo": 3]));
			anyadir_ataque_pnj(30, "aplastar", 0, (["objetivo": 3]));
			anyadir_ataque_pnj(20, "hender", 0, (["objetivo": 4]));

			load_chat(50, ({
				":ajusta su armadura con cuidado, asegurándose de que todo esté en su lugar antes de continuar su ronda de vigilancia.",
				":inspecciona el perímetro del campamento, evaluando constantemente posibles puntos débiles.",
				"recorre los límites del campamento con paso firme y cadencioso.",
				":muestra una sonrisa satisfecha al ver que todo está en orden.",
				"'creo que Roderick está perdiendo el control sobre algunos hombres. Vi varias sombras mudarse furtivamente hacia la parte suroeste del campamento durante la noche...",
				":no veo la hora de desacerme de estos malditos insectos.",
				":se frota el cuello, sintiendo la tensión acumulada después de largas horas de patrulla.",
				":se apoya en un árbol y cierra los ojos por un instante, disfrutando del aire fresco.",
				"!buscar",
				"!buscar"
			}));

			add_clone(ARMASBASE+"martillo_a_dos_manos",1);
			add_clone(ARMADURASBASE + "capucha", 1);    
add_clone(ARMADURASBASE + "cuero", 1);    
			add_clone(ARMADURASBASE + "cinturon", 1);    
			add_clone(ARMADURASBASE + "vaina", 1);
			add_clone(ARMADURASBASE + "pantalones", 1);    
			add_clone(ARMADURASBASE+"capa",1);
			add_clone(ARMADURASBASE+"botas",1);

			ajustar_dinero(8+random(5), "oro");
			break;

		case 2:
			set_name("Bandido Faucesombra");
			set_short("Bandido Faucesombra");
			set_main_plural("Bandidos Faucesombra");
			add_alias(({"bandido","faucesombra", "fauce", "bandido faucesombra", "humano", "raso"}));
			add_plural(({"bandidos","humanos", "faucesombras"}));
			set_long("El cuerpo musculoso y robusto de este bandido es una prueba tangible de su vida dedicada al combate y la supervivencia. Hombres como este son los que más abundan en las filas de Roderick Faucesombra, mercenarios y soldados desertores que buscan una vida fuera de las restricciones de la sociedad. En este caso en particular, los rasgos faciales de este hombre están marcados por cicatrices que cuentan historias de enfrentamientos pasados y encuentros mortales. Su mandíbula cuadrada y su mirada penetrante transmiten una determinación inquebrantable y una suspicacia permanente.\n"
				"El bandido lleva consigo una mochila de lona gastada, en la que almacena sus pertenencias y suministros para las expediciones y campañas. Colgando de sus hombros, una capa de tonos pardos ondea al viento, de manera a ocultarse entre el follaje cuando es necesario.\n");

			fijar_clase("cazador");
			fijar_nivel(30+random(10));
			set_random_stats(15,18);

			fijar_carac("daño", 8);

			anyadir_ataque_pnj(40, "cazar", 0, (["objetivo": 3]));
			anyadir_ataque_pnj(40, "doblegolpe", 0, (["objetivo": 3]));
			anyadir_ataque_pnj(100, "concentracion", 0, (["objetivo": 3]));

			load_chat(50, ({
				":afila meticulosamente el filo de una de sus espadas, preparándose para cualquier eventualidad.",
				":talla un pequeño objeto de madera.",
				":mata el aburrimiento jugando a las cartas con otros hombres.",
				"'¡maldito Rashen, estoy seguro de que hizo trampa en los dados de nuevo anoche!",
				"'la hija menor de Roderick sería una bestiecilla digna de domar, sino fuese porque tendría una muerte lenta y llena de espasmos al día siguiente.",
				":obserba con curiosidad el sencillo collar de madera que cada Faucesombra tiene colgado al cuello como protección elemental, entregado por la hija mayor de Roderick.",
				"'estoy cansándome de esperar, ese viejo lobo tendrá que tomar una decisión pronto antes de que se me termine la paciencia.",
				":descansa en silencio, observando el ajetreo del campamento.",
				":trae un par de conejos colgados del cinturón después de salir de caza.",
				":trae un par de liebres colgados del cinturón después de salir de caza.",
				"'a ver si me da tiempo a unirme a la cacería de hoy, dice Goldranh que vamos a buscar un par de jabalíes para esta noche."
			}));

			add_clone(ARMASBASE+"espada_larga",2);
			add_clone(ARMADURASBASE+"capa",1);
			add_clone(ARMADURASBASE+"mochila",1);
			add_clone(ARMADURASBASE+"cuero",1);
			add_clone(ARMADURASBASE+"botas",1);
			ajustar_dinero(5+random(5), "oro");
			break;

		case 3:
			set_name("Explorador Faucesombra");
			set_short("Explorador Faucesombra");
			set_main_plural("Exploradores Faucesombra");
			add_alias(({"bandido","faucesombra", "fauce", "bandido faucesombra", "humano", "explorador"}));
			add_plural(({"bandidos","humanos", "faucesombras", "exploradores"}));
			set_long("Este bandido se desliza entre las sombras como una serpiente silenciosa, apenas perceptible para aquellos que no están alerta. Es un maestro del sigilo y la discreción, seleccionado meticulosamente por Roderick Faucesombra para llevar a cabo misiones de vigilancia y asesinato. Su apariencia resulta inquietante, con una figura esbelta y ágil que se desplaza sin esfuerzo por los rincones en sombra del campamento.\n"
				"Su rostro permanece oculto tras una capucha oscura, apenas permitiendo vislumbrar unos ojos fríos y penetrantes. Es un observador paciente y astuto, dedicando tiempo a estudiar a sus presas mientras espera el momento perfecto para atacar. La determinación implacable y la destreza en el arte del asesinato y el espionaje de estos individuos los convierte en recursos inestimables para Roderick Faucesombra, y en auténticas pesadillas para aquellos que se interponen en su camino.\n");

			fijar_clase("ladron");
			fijar_nivel(40+random(15));
			set_random_stats(15,18);

			fijar_carac("daño", 8);

			anyadir_ataque_pnj(100, "concentracion", 0, (["objetivo": 3]));
			anyadir_ataque_pnj(60, "atravesar", 0, (["objetivo": 3]));
	anyadir_ataque_pnj(40, "apunyalar_escondido", (: ! (dame_bloqueo_combate("Bloqueo:habilidad:apuñalar") && query_timed_property("apunyaladoreciente")) :));

			load_chat(150, ({
				":se sienta bajo un árbol a revisar su informe.",
				":examina sus anotaciones.",
				";te atraviesa con sus ojos acerados."
			}));

			add_clone(ARMASBASE+"daga",1);
			add_clone(ARMADURASBASE+"pantalones",1);
			add_clone(ARMADURASBASE+"vaina_pequeña",1);
			add_clone(ARMADURASBASE+"cuero",1);
			add_clone(ARMADURASBASE+"botas",1);
			ajustar_dinero(5+random(5), "oro");
			break;

		default:
			set_short("¡NPC Erróneo de %^BOLD%^Hazrakh%^RESET%^: ¡Avisa a un Inmortal!");
			break;
		}

	fijar_raza("humano");
	fijar_estatus("Ak'Anon",-500);
	fijar_alineamiento(4500);
	fijar_religion("ateo");
	fijar_bando("mercenario");
	fijar_ciudadania("vagabundo");
	fijar_altura(175);
	fijar_peso_corporal(60000);

	fijar_maestrias(([
		"Cortantes medias": 60,
		"Penetrantes medias": 60,
		"Aplastantes pesadas": 60,
		"Penetrantes ligeras": 60
	]));


	fijar_pvs_max(5000);

	ajustar_bo(150);

add_static_property("faucesombra", 1);
add_loved("propiedad","faucesombra");
	add_hated("bando", "bueno");
	set_aggressive(5);
	fijar_genero(1);

	init_equip();
	
add_zone("campamento_faucesombra");
set_move_after(10,5);
}
