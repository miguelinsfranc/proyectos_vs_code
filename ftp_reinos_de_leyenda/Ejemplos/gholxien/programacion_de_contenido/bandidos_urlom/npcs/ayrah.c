#include "base_campamento.h"
#include <spells.h>
inherit "/obj/monster";


mixed event_spell(object hechizo, object lanzador, mixed objetivo, object *objetivos_adicionales, object *fuera_alcance, int turno, int inaudible) {
    string nombre_hechizo = hechizo->dame_nombre_hechizo();

	if (nombre_hechizo == "Silencio") {
        tell_object(lanzador, "Ayra clava su mirada en tus ojos, y con un rápido manotazo te lanza una maraña de eter que te desconcentra y te aturde!'.\n");
        tell_room(environment(lanzador), lanzador->query_cap_name() + " intenta formular un hechizo, pero parece tener dificultades.\n", lanzador);
        lanzador->add_timed_property("nocast", 1, 5);
        return 1;  // Cancela la formulación del hechizo en el primer turno
    }
    return 0;  // Continúa con la formulación normal del hechizo
}


void poner_piel();
void setup()
{
    set_name("Ayrah");
    add_alias("ayrah");
    set_short("Ayrah");
    set_main_plural("Ayrahs");

    set_long(
        "Ayrah irradia una aura enigmática y mística. Su cabello castaño claro cae en ondas suaves sobre sus hombros, acentuando un rostro sereno y profundo. Sus ojos, de tonalidad indescifrable, parecen contener secretos antiguos y conocimientos arcanos. Ataviada con una túnica de tonos oscuros, con detalles sutiles y elegantes, Ayrah se desliza con gracia y ligereza. La tela parece estar impregnada de un sutil brillo, como si estuviera impregnada de energía mágica. Un collar de piedras preciosas cuelga en su cuello, destelleando con un resplandor discreto cuando la luz lo alcanza. Sus manos delicadas están adornadas con anillos ornamentados, cada uno con símbolos que evocan la magia. Un suave tintineo acompaña sus movimientos, creando una melodía apenas perceptible. Ayrah se desplaza con una elegancia silenciosa, como si estuviera en comunión con los misterios ocultos de la naturaleza. Una vara esculpida en madera noble descansa en sus manos, emitiendo una sutil luminiscencia. Los grabados en su superficie parecen formar patrones antiguos, cuyos significados solo los más entendidos en las artes arcanas podrían descifrar. Aunque no sea evidente a simple vista, aquellos que observen con atención notarán la presencia de runas abjurativas, indicios sutiles de su especialización en el arte de la protección y la neutralización. La presencia de Ayrah evoca un equilibrio entre poder y serenidad, una maestría de las fuerzas ocultas que subyacen en el mundo invisible. Esconde habilidades arcanas bajo su apariencia tranquila y cautivadora. Los secretos de su especialización en la abjuración se desvelarán en el momento oportuno, cuando los hilos de la magia se entrelacen en el enfrentamiento que se avecina.\n"
    );

fijar_clase("hechicero");
fijar_especializacion("abjurador_negro");
fijar_raza("humano");
    fijar_bando("mercenario");
    fijar_religion("ateo");
    fijar_estatus("Ak'Anon",-200);
    fijar_alineamiento(30000);
    fijar_genero(2);

    fijar_fue(15);
    fijar_des(20);
    fijar_con(18);
    fijar_int(24);
    fijar_sab(15);
    fijar_car(10);

	ajustar_bo(149);
	ajustar_be(200);
	ajustar_bp(100);
ajustar_armadura_natural(20);

fijar_carac("poder_magico", 281);
fijar_pvs_max(25000);


nueva_resistencia("magico", 75, 0);
nueva_resistencia("fuego", 75, 0);
nueva_resistencia("frio", 75, 0);

add_static_property("faucesombra", 1);
add_loved("propiedad","faucesombra");
add_hated("bando", "bueno");
	set_aggressive(5);



    load_chat(50,
      ({
           ":sujeta su vara con la mano izquierda mientras se pone un mechón de su cabello tras la oreja con la mano derecha.",
           "'La magia es poderosa y sagrada, es mejor no abusar de ella.",
           "'Padre, creo que el plan que habeis trazado para la invasión de Ormerak es un poco arriesgado, os pido que lo reconsideréis.",
      "!formular cantrip:luces",
	  }));

    add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/armas/fulgor_umbrio.c", 1);
add_clone("/baseobs/tesoros/magia/manual_defensivo.c", 1);
add_clone(ARMADURASBASE + "tunica", 1);    
add_clone(ARMADURASBASE + "capucha", 1);    
add_clone(ARMADURASBASE + "brazalete", 1);    
add_clone(ARMADURASBASE + "brazalete_izq", 1);    
add_clone(ARMADURASBASE + "capa", 1);    
add_clone(ARMADURASBASE + "guante", 1);    
add_clone(ARMADURASBASE + "guante_izq", 1);    
add_clone(ARMADURASBASE + "bolsita", 1);    
add_clone(ARMADURASBASE + "cinturon", 1);    
add_clone(ARMADURASBASE + "falda", 1);    
add_clone(ARMADURASBASE + "botas", 1);    
add_clone(ARMADURASBASE + "anillo", 2);    
add_clone(ARMADURASBASE + "collar", 1);    
add_clone(ARMADURASBASE + "pendiente", 1);    

fijar_maestrias((["Aplastante ligera": 100]));

  fijar_nivel(77);

    anyadir_ataque_pnj(90, "defenestrar", 0, (["objetivo": 3]));
    anyadir_ataque_pnj(90, "flecha de llamas", 0, (["objetivo": 3]));
    anyadir_ataque_pnj(75, "flecha acida de dvneil", 0, (["objetivo": 3]));
    anyadir_ataque_pnj(75, "cono de frio", 0, (["objetivo": 3]));
anyadir_ataque_pnj(100, "devolver conjuro mayor", 0, (["objetivo": 1]));
anyadir_ataque_pnj(100, "ojo por ojo", 0, (["objetivo": 1]));
anyadir_ataque_pnj(30, "poderosa negacion de la esencia");


    init_equip();
poner_piel();

fijar_objetos_morir(({"tesoro", "pergamin", ({3, 4}), 1}));
 fijar_objetos_morir(({"/w/gholxien/programacion_de_contenido/bandidos_urlom/armas/fulgor_umbrio.c", 1, 25}));
}

void poner_piel()
{
    object b = clone_object(SHADOWS + "piel_de_piedra_sh.c");

    if (b) {
        b->setup_sombra(this_object(), 19 + roll(2, 3));
        tell_accion(
            environment(),
            "¡" + query_short() + " chasquea los dedos y su piel se vuelve de piedra!\n",
            "",
            ({this_object()}),
            this_object()
        );
    }
}

status rutina_de_proteccion()
{
    if (!this_object()->dame_piel_de_piedra()) {
        poner_piel();
        return 1;   
    }

    return 0;
}