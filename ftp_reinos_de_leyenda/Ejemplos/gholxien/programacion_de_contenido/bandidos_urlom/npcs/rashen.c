#include "base_campamento.h"
inherit "/obj/monster";

void setup()
{
    set_name("Rashen el Escurridizo");
    add_alias(({"rashen", "Rashen", "escurridizo", "el escurridizo", "rashen el escurridizo", "Rashen el Escurridizo", "guardaespaldas"}));
    set_short("Rashen el Escurridizo");
    set_main_plural("Rashens los Escurridizos");

    set_long(
        "El segundo guardaespaldas de Roderick, posee una estatura media pero una agilidad y velocidad asombrosas. Su vestimenta se compone de una túnica de cuero oscuro, ajustada al cuerpo de manera precisa, y cubierta por una capa negra que se mimetiza con las sombras del entorno. Cada paso que da es silencioso gracias a sus botas de cuero, permitiéndole moverse con destreza en el campo de batalla sin alertar a sus enemigos. En sus manos expertas, sostiene un látigo largo y flexible, con el cual muestra una maestría impresionante, y un sable de filo afilado que se convierte en su arma principal para enfrentar a cualquier oponente.\n"
    );

fijar_clase("soldado");    fijar_raza("humano");
    fijar_bando("mercenario");
    fijar_religion("ateo");
    fijar_estatus("Ak'Anon",-200);
    fijar_alineamiento(30000);
    fijar_genero(1);

    fijar_fue(22);
    fijar_carac("des", 22);
    fijar_carac("con", 20);
    fijar_carac("int", 20);
    fijar_carac("sab", 10);
    fijar_carac("car", 30);

	ajustar_bo(160);
	ajustar_be(300);
	ajustar_bp(150);
ajustar_armadura_natural(40);

fijar_carac("daño", 170);
fijar_pvs_max(30000);

nueva_resistencia("fuego", 30, 0);
nueva_resistencia("magico", 30, 0);
nueva_resistencia("frio", 30, 0);

add_static_property("faucesombra", 1);
add_loved("propiedad","faucesombra");
add_hated("bando", "bueno");
	set_aggressive(5);

    load_chat(50,
      ({
           ":azota el aire con su látigo.",
           "'Me gustaría echar una partidita de dados, pero debo proteger a Roderick. Ya jugaremos esta noche ante la fogata. Espero que halla alguna de las cocineras, dispuesta a dejarse hacer.",
           ":afila su sable con una piedra de amolar mientras silba entre los dientes y sonríe condescendiente.",
      }));

    add_clone(ARMASBASE + "latigo", 1);
add_clone(ARMASBASE + "sable", 1);
add_clone(ARMADURASBASE + "cuero", 1);    
add_clone(ARMADURASBASE + "gorro_cuero", 1);    
add_clone(ARMADURASBASE + "brazalete", 1);    
add_clone(ARMADURASBASE + "brazalete_izq", 1);    
add_clone(ARMADURASBASE + "capa", 1);    
add_clone(ARMADURASBASE + "guante_cuero", 1);    
add_clone(ARMADURASBASE + "guante_cuero_izq", 1);    
add_clone(ARMADURASBASE + "vaina", 1);    
add_clone(ARMADURASBASE + "cinturon", 1);    
add_clone(ARMADURASBASE + "grebas", 1);    
add_clone(ARMADURASBASE + "botas", 1);    

fijar_maestrias(([
"Lacerante ligera": 100,
"Cortantes medias": 100,
]));


    set_aggressive(10, 2);

    fijar_nivel(77);

    anyadir_ataque_pnj(100, "esgrimir");
    anyadir_ataque_pnj(100, "fustigar", 3);
    anyadir_ataque_pnj(85, "oleada", 3);
    anyadir_ataque_pnj(75, "ataquedoble", 3);
        anyadir_ataque_pnj(90, "giro", 0, (["objetivo": 3]));
anyadir_ataque_pnj(100, "goldranh_ataca", (:present("goldranh", environment())? present("goldranh", environment())->dame_peleando()==0 : 0:));
    anyadir_ataque_pnj(
        100,
        (: present("goldranh", environment())->queue_action("proteger rashen") :),
        (: !dame_protector() && present("goldranh", environment()) :)
    );

    init_equip();
}

function goldranh_ataca(object *objetivos) {
	int i;
	do_emote("señala con su sable al enemigo para que Goldranh le ataque.\n");
	for(i = sizeof( objetivos); i--;) {
	    present("goldranh", environment())->attack_ob(objetivos[i]);
	}
}