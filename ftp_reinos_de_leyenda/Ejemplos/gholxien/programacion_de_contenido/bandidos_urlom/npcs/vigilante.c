//gholxien 19.06.2023
#include "base_campamento.h"
inherit "/obj/monster";

void setup() {
set_name("vigilante faucesombra");
			set_main_plural("Vigilantes Faucesombra");
			add_alias(({"bandido", "vigilante", "bandido vigilante", "faucesombra"}));
			add_plural(({"vigilantes", "bandidos", "faucesombras"}));
set_short("Vigilante Faucesombra");
set_long("Se trata de un hombre enjuto y con el cabello ralo cuya misión es vigilar los alrededores colindantes al campamento.\n"
"Estos hombres suelen tener una vida muy dura en el campamento, ya que prácticamente se pasan el día al sol, sin ningún sitio en el que protegerse del sofocante calor. Aunque claro, el turno de noche también es malo, ya que por esta zona las noches suelen refrescar bastante.\n"
"Sus ropajes son de color pardo y marrón, proporcionándole un ocultamiento perfecto entre los ramajes y las hojas que rodean la hendeble plataforma. Te percata de que en la cabeza lleva algo extraño, parecen ser unos prismáticos que descansan sobre el tabique nasal, pero ke a su vez tienen una correa de cuero ke rodea la cabeza. Esta correa, colocada de forma ingeniosa, supones que sirve para tener las manos vacías si hay que luchar.\n");
}