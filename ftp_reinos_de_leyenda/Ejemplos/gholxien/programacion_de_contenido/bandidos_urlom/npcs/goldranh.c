#include "base_campamento.h"
inherit "/obj/monster";

void setup()
{
	set_name("Goldranh");
    add_alias(({"goldranh", "martillo de hierro", "martillo", "hierro", "guardaespaldas", "Goldranh Martillo de hierro"}));
    set_short("Goldranh Martillo de hierro");
    set_main_plural("Dos imágenes de Goldranh Martillo de hierro");

    set_long(
        "Goldranh Martillo de hierro, el guardaespaldas fortachón de Roderick, destaca entre los demás como un verdadero gigante entre hombres. Sus músculos prominentes se dibujan poderosamente bajo la cota de malla que protege su corpulento cuerpo. Con un escudo de gran tamaño, elaborado con un robusto metal, Goliat se presenta como un muro infranqueable para quienes se atrevan a desafiarlo. Además, maneja con destreza una cadena larga y resistente, convirtiéndola en un arma mortal con cada movimiento. Su cabello corto y una barba espesa realzan su apariencia fieramente guerrera, asegurando que nadie subestime su fuerza.\n"
    );

fijar_clase("soldado");    fijar_raza("humano");
    fijar_bando("mercenario");
    fijar_religion("ateo");
    fijar_estatus("Ak'Anon",-200);
    fijar_alineamiento(30000);
    fijar_genero(1);

    fijar_fue(24);
    fijar_des(10);
    fijar_con(20);
    fijar_int(20);
    fijar_sab(10);
    fijar_car(30);

ajustar_bo(152);
ajustar_be(20);
ajustar_bp(283);
fijar_carac("daño", 150);
fijar_pvs_max(30000);
ajustar_armadura_natural(48);

nueva_resistencia("fuego", 30, 0);
nueva_resistencia("magico", 30, 0);
nueva_resistencia("frio", 30, 0);

add_static_property("faucesombra", 1);
add_loved("propiedad","faucesombra");
add_hated("bando", "bueno");
	set_aggressive(5);


    load_chat(50,
      ({
           "'Espero que el plan de Roderick salga a pedir de boca, si no va a ser un duro golpe para la moral de los chicos.",
           ":observa con cuidado los eslavones de su cadena, en busca de manchas de errumbre u óxido.",
           ":examina su escudo, en busca de manchas de sangre u óxido.",
      "'Rashen, deja de presumir de tu agilidad, maldito bastardo."
	  }));

    add_clone("/baseobs/escudos/escudo_corporal.c", 1);
    add_clone(ARMASBASE + "cadena", 1);
add_clone(ARMADURASBASE + "completa", 1);
add_clone(ARMADURASBASE + "gran_yelmo", 1);
add_clone(ARMADURASBASE + "capa", 1);
add_clone(ARMADURASBASE + "cinturon", 1);
add_clone(ARMADURASBASE + "cinturon_anilla", 1);
add_clone(ARMADURASBASE + "grebas_metalicas", 1);
add_clone(ARMADURASBASE + "botas_guerra", 1);
add_clone(ARMADURASBASE + "brazal", 1);
add_clone(ARMADURASBASE + "brazal_izq", 1);
add_clone(ARMADURASBASE + "guantelete_izq", 1);
add_clone(ARMADURASBASE + "guantelete", 1);

    fijar_maestrias((["Lacerante media":100]));
   
   fijar_nivel(77);
	anyadir_ataque_pnj(100, "rashen_ataca", (:present("rashen", environment())? present("rashen", environment())->dame_peleando()==0 : 0:));
        anyadir_ataque_pnj(80, "ataquedoble", 3);
		anyadir_ataque_pnj(65, "atizar", 3);
anyadir_ataque_pnj(
        100,
        "aguantar",
        (
            : !query_static_property("aguantar")
            :));
            anyadir_ataque_pnj(90, "atrapar", (: ! query_spell_effect("atrapar") :));
            anyadir_ataque_pnj(100, "desplomar", (: query_spell_effect("atrapar") != 0 :));
		anyadir_ataque_pnj(85, "golpecertero", 0, (["objetivo": 3]));
    
    init_equip();
}

function rashen_ataca(object *objetivos) {
	int i;
	do_emote("señala con un gesto de su escudo al enemigo para que Rashen le ataque.\n");
	for(i = sizeof( objetivos); i--;) {
	    present("rashen", environment())->attack_ob(objetivos[i]);
	}
}