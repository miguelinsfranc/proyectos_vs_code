// Hazrakh 17/06/2023

inherit "/obj/monster";

#include "/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/base_campamento.h"

void setup() {
	int i = roll(1, 2);

	switch(i)
	{
		case 1:
			set_name("Cocinero Faucesombra");
			set_short("Cocinero Faucesombra");
			set_main_plural("Cocineros Faucesombra");
			add_alias(({"bandido", "cocinero", "bandido cocinero", "faucesombra"}));
			add_plural(({"bandidos", "cocineros", "faucesombras"}));
			set_long("A este bandido le toca el turno de cocina. Con una sartén en mano, se planta frente a la fogata, donde las llamas danzan y crepitan, proporcionando el calor necesario para su tarea culinaria. Aunque su papel principal no sea el de chef, demuestra una habilidad sorprendente en el arte de cocinar sobre el fuego abierto. Su experiencia en la clandestinidad le ha enseñado a aprovechar los recursos disponibles para preparar comidas sustanciosas.\n"
			"Con gestos ágiles y precisos, el bandido se mueve alrededor de la fogata, ajustando el calor y girando la sartén con destreza. Mientras los ingredientes frescos y, quizá saqueados, chisporrotean en el aceite caliente, se crea una fragancia tentadora que se mezcla con el aire del campamento.\n");
			fijar_genero(1);
			break;

		case 2:
			set_name("Cocinera Faucesombra");
			set_short("Cocinera Faucesombra");
			set_main_plural("Cocineras Faucesombra");
			add_alias(({"bandido", "cocinera", "bandido cocinero", "faucesombra"}));
			add_plural(({"bandidos", "cocineras", "faucesombras"}));
			set_long("A esta bandida le toca el turno de cocina. Con una sartén en mano, se planta frente a la fogata, donde las llamas danzan y crepitan, proporcionando el calor necesario para su tarea culinaria. Aunque su papel principal no sea el de chef, demuestra una habilidad sorprendente en el arte de cocinar sobre el fuego abierto. Su experiencia en la clandestinidad le ha enseñado a aprovechar los recursos disponibles para preparar comidas sustanciosas.\n"
			"Con gestos ágiles y precisos, la mujer se mueve alrededor de la fogata, ajustando el calor y girando la sartén con destreza. Mientras los ingredientes frescos y, quizá saqueados, chisporrotean en el aceite caliente, se crea una fragancia tentadora que se mezcla con el aire del campamento.\n");
			fijar_genero(2);
			break;

		default:
			set_short("¡NPC Erróneo de %^BOLD%^Hazrakh%^RESET%^: ¡Avisa a un Inmortal!");
			break;
	}

	fijar_raza("humano");
	fijar_estatus("Ak'Anon",-500);
	fijar_alineamiento(4500);
	fijar_religion("ateo");
	fijar_bando("mercenario");
	fijar_ciudadania("vagabundo");

	fijar_pvs_max(3000);
			fijar_nivel(20+random(20));
			set_random_stats(12,18);

	add_static_property("faucesombra", 1);
	add_loved("propiedad","faucesombra");
	add_hated("bando", "bueno");
	set_aggressive(5);

	load_chat(20, ({
	":sonríe con aprobación tras probar el guiso y asiente, complacido con el resultado.",
	":ajusta el fuego de la fogata, controlando cuidadosamente el calor para lograr una cocción perfecta.",
	":lucha por mantener el fuego bajo control, agregando leña o soplando suavemente cuando las llamas amenazan con apagarse.",
	":deja escapar un suspiro de alivio cuando la comida se cocina correctamente, asegurando a todos que al menos no se ha quemado en esta ocasión.",
	":gruñe de vez en cuando, sin dejar que nada se queme bajo su supervisión."
	}));

	add_clone(ARMASBASE+"sarten",1);
	add_clone("/baseobs/ropas/delantal.c", 1);    
	add_clone(ARMADURASBASE + "camiseta", 1);    
	add_clone(ARMADURASBASE + "pantalones", 1);    
	add_clone(ARMADURASBASE+"botas",1);

	ajustar_dinero(5+random(5), "oro");
	anyadir_ataque_pnj(100, "zancadilla");
	anyadir_ataque_pnj(100, "jugarreta");

init_equip();


}
