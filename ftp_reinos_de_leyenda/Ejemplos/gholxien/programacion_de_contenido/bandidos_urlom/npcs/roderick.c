#include "base_campamento.h"

inherit "/obj/monster";


void setup()
{
    set_name("Roderick");
    add_alias("roderick");
    set_short("Roderick Faucesombra");
    set_main_plural("Roderick Faucesombras");

    set_long(
        "Roderick Faucesombra, el líder de los bandidos, se alza imponente en medio del campamento. Su estatura considerable impone respeto a todos aquellos que se atreven a enfrentarlo. A sus cuarenta años, su cabello negro azabache se entremezcla con sutiles hebras plateadas, una señal de las muchas batallas que ha librado. Las cicatrices que surcan su rostro son un testimonio silencioso de los innumerables enfrentamientos que ha superado con ferocidad. Roderick viste una armadura de cuero oscuro, finamente adornada con detalles de metal bruñido. Esta protección le confiere seguridad sin comprometer su movilidad en combate. Sobre su espalda, una capa de color rojo sangre ondea al viento, añadiendo un toque siniestro a su figura imponente. Cada paso que da emana una presencia dominante, dejando en claro que él es el líder indiscutible de esta banda de bandidos.\n"
    );

fijar_clase("soldado");
fijar_raza("humano");
    fijar_bando("mercenario");
    fijar_religion("ateo");
    fijar_estatus("Ak'Anon",-200);
    fijar_alineamiento(30000);
    fijar_genero(1);

    fijar_fue(28);
    fijar_des(15);
    fijar_con(22);
    fijar_int(18);
    fijar_sab(15);
    fijar_car(10);

	ajustar_bo(122);
	ajustar_be(88);
	ajustar_bp(359);
ajustar_armadura_natural(50);

fijar_carac("daño", 148);
fijar_pvs_max(50000);

nueva_resistencia("fuego", 50, 0);
nueva_resistencia("magico", 50, 0);
nueva_resistencia("frio", 50, 0);

add_static_property("faucesombra", 1);
add_loved("propiedad","faucesombra");
add_hated("bando", "bueno");
	set_aggressive(5);

    load_chat(50,
      ({
           "'Es vital que nos hagamos con ese pueblucho abandonado. Lo fortificaremos y montaremos allí nuestra sede. Aparte, dada la situación geográfica del lugar, podemos hacernos pasar por enviados de Ak'Anon y recoger los impuestos de paso.",
           "'Mis hombres están descontentos y dicen que llevo ya muchos años al frente, pero les demostraré que aún soy capaz de conducirlos a la victoria, que nos permitirá controlar Ormerak y las ganancias lloverán cual agua en invierno.",
           ":mueve ágilmente su rompedora, y el sonido que produce el arma al cortar el aire hace que sonría con deleite.",
      ":examina un gran mapa colgado en la pared este, que recalca con gran detalle toda la extensión del reino de Urlom y la parte este del reino enano de Kheleb Dum.",
	  }));

    add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/armas/rompedora.c", 1);
add_clone(ARMADURASBASE + "bandas", 1);    
add_clone(ARMADURASBASE + "brazalete_mallas", 1);    
add_clone(ARMADURASBASE + "brazalete_mallas_izq", 1);    
add_clone(ARMADURASBASE + "talabarte_doble", 1);    
add_clone(ARMADURASBASE + "guante_cuero", 1);    
add_clone(ARMADURASBASE + "guante_cuero_izq", 1);    
add_clone(ARMADURASBASE + "cinturon", 1);    
add_clone(ARMADURASBASE + "grebas_mallas", 1);    
add_clone(ARMADURASBASE + "botas_campaña", 1);    

fijar_maestrias((["Cortantes pesadas": 100]));



    set_aggressive(10, 2);

    fijar_nivel(77);

    anyadir_ataque_pnj(80, "tajar", 0, (["objetivo": 3]));
    anyadir_ataque_pnj(90, "instigar", 0, (["objetivo": 3]));
    anyadir_ataque_pnj(60, "pulverizar", 0, (["objetivo": 3]));
    anyadir_ataque_pnj(100, "hender", 0, (["objetivo": 4]));
anyadir_ataque_pnj(
        100,
        "furia",
        (
            : !query_static_property("enfurecido")
            :));
    


    init_equip();


fijar_objetos_morir(({"tesoro", "varios", ({3, 4}), 1}));
fijar_objetos_morir(({"/w/gholxien/programacion_de_contenido/bandidos_urlom/armas/rompedora.c", 1, 25}));
}