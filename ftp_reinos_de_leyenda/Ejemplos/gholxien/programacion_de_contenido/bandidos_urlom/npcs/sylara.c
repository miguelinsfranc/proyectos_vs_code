#include "base_campamento.h"

inherit "/obj/monster";
#define SHADOW_ESCONDERSE "/habilidades/shadows/esconderse_sh.c"

void setup()
{
    set_name("sylara");
    add_alias("sylara");
    set_short("Sylara");
    set_main_plural("Sylaras");

    set_long(
        "Sylara emana una apariencia inquietante. Su cabello oscuro cae en cascada sobre sus hombros, creando un contraste notable con su piel pálida y sus ojos intensamente verdes que parecen brillar con malicia. Vestida con un largo y ajustado vestido negro, decorado con encajes y detalles góticos, Sylara exhibe su gusto por lo oscuro y misterioso. Sus manos, las cuales esconde con guantes de cuero negro, albergan frascos y herramientas utilizados en la preparación de venenos mortales. Cada gesto suyo parece teñido de una aura siniestra que advierte a los demás sobre su peligrosidad.\n"
    );

fijar_clase("ladron");
fijar_raza("humano");
    fijar_bando("mercenario");
    fijar_religion("ateo");
    fijar_estatus("Ak'Anon",-200);
    fijar_alineamiento(30000);
    fijar_genero(2);

    fijar_fue(18);
    fijar_des(28);
    fijar_con(18);
    fijar_int(20);
    fijar_sab(20);
    fijar_car(10);

	ajustar_bo(214);
	ajustar_be(181);
	ajustar_bp(300);

fijar_carac("daño", 104);
fijar_pvs_max(25000);

nueva_resistencia("fuego", 50, 0);
nueva_resistencia("magico", 50, 0);
nueva_resistencia("frio", 50, 0);

add_static_property("faucesombra", 1);
add_loved("propiedad","faucesombra");
add_hated("bando", "bueno");
	set_aggressive(5);

    load_chat(50,
      ({
           "'He de ir a recoger algunas de las setas anaranjadas que crecen en la parte sudeste del campamento para fabricar mas veneno.",
           ":lanza su daga hacia el aire, recogiéndola mas tarde por la punta y realizando un gesto imaginario con el que golpea a un hipotético enemigo en la sien.",
           ":abre un compartimento oculto en su arma por la que introduce un líquido verduzco que extrae de un frasco amarillento.",
      }));

    add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/armas/susurro_ponzoñoso.c", 2);
add_clone(ARMADURASBASE + "capucha", 1);    
add_clone(ARMADURASBASE + "camiseta", 1);    
add_clone(ARMADURASBASE + "pulsera", 1);    
add_clone(ARMADURASBASE + "pulsera_izq", 1);    
add_clone(ARMADURASBASE + "manto", 1);    
add_clone(ARMADURASBASE + "guante", 1);    
add_clone(ARMADURASBASE + "guante_izq", 1);    
add_clone(ARMADURASBASE + "cinturon", 1);    
add_clone(ARMADURASBASE + "pantalones", 1);    
add_clone(ARMADURASBASE + "zapatillas", 1);    

fijar_maestrias((["Penetrantes ligeras": 100]));



    set_aggressive(10, 2);

    fijar_nivel(77);

    anyadir_ataque_pnj(100, "estocada");
    anyadir_ataque_pnj(80, "apunyalar_escondido", (: ! (dame_bloqueo_combate("Bloqueo:habilidad:apuñalar") && query_timed_property("apunyaladoreciente")) :)); 
    anyadir_ataque_pnj(75, "herir");
    

    init_equip();

fijar_objetos_morir(({"tesoro", "varios", ({3, 4}), 1}));
 fijar_objetos_morir(({"/w/gholxien/programacion_de_contenido/bandidos_urlom/armas/susurro_ponzoñoso.c", 1, 25}));
}

void escondete()
{
    if ( TO->dame_shadow_esconderse() ) {
        return;
    }

    clone_object(SHADOW_ESCONDERSE)->setup_sombra(this_object(),60,0);
    set_hidden(1);
}

function apunyalar_escondido(object *objetivos) {
	object objetivo = element_of(objetivos);
	/*TO->stop_fight(objetivo);
	objetivo->stop_fight(TO);*/
    tell_accion(environment(),query_short() + " desaparece en una nube de humo mágico.\n","",({TO}),TO);
    escondete();
	if (habilidad("apuñalar",objetivo)) add_timed_property("apunyaladoreciente",1,30);
	
}