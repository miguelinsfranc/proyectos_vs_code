//Gholxien 17.06.2023: Mision del inventor herido para el campamento de bandidos

inherit "/baseobs/misiones/base";
#include <misiones.h>

void setup(){
	// Configuración básica.
	fijar_nombre("El invento robado");
	fijar_tipo(5);
	fijar_dificultad(5);

	// Caducidad: Sin caducidad, solo se puede hacer una vez.


	// Hitos.
	nuevo_hito("conversacion-gnomo", "Has hablado con el gnomo nombrependiente y te ha dicho que hace un par de días, unos bandidos de aspecto amenazador le han robado un invento de gran valor que pretendía transportar a la sede para su examen juzgainventos que se realiza para todos los inventos creados con el fin de valorar si son peligrosos o no. Ahora, si no consigue recuperar el invento, probablemente perderá su puesto como inventor. Te ha pedido que busques a esos bandidos y le traigas el invento. Tras una tos y un gesto de dolor, te señala hacia el sudeste y comprendes que te indica por dónde se marcharon los bandidos.", 0);
	nuevo_hito("conversacion-esclava", "Has encontrado el campamento de bandidos, y tras conversar con una esclava prisionera que retienen los bandidos, te ha confesado que al norte hay un árbol desde el que los bandidos vigilan con gran atención los alrededores, con el fin de evitar ser sorprendidos por alguna amenaza... ¿quizás esté allí el invento que buscas?", 0);
	nuevo_hito("conseguir-invento", "Has logrado sorprender al vigilante adormilado que cumplía con el turno de guardia, y tras acabar con él, de sus manos cae lo que parece ser unos prismáticos de manufactura gnoma. Parece ser que este es el invento que estabas buscando, ahora debes devolvérselo a nombrependiente.", 0);
nuevo_hito("entregar_invento", "Entregas el invento al pequeño gnomo, que anegado en lágrimas de felicidad, te entrega una gran bolsa de monedas mientras agradece tu gesto con efusivas palabras.", 0);

	// Final.
	fijar_final_obligatorio(({"entregar-invento",}));

	// Pública.
	//fijar_publica("Urlom");

	// Recompensas.
		// XP.
	nueva_recompensa_xp(250000, 0);
}