//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/outside.c";

void setup() {
    object riachuelo;

    add_exit(NE, CAMPBAN + "camp_11.c", "plain");
    add_exit(E, CAMPBAN + "camp_9.c", "plain");
    add_exit(S, CAMPBAN + "camp_7.c", "plain");
    set_light(70);
    set_short("Campamento de bandidos: Sector noroccidental");
    set_long("Al adentrarte en la zona noroeste del campamento, te encuentras con una imponente formación rocosa que se alza como una sólida muralla protectora. Las rocas, altas y angulosas, brindan una sensación de fortaleza y seguridad al área.\n"
        "En sus grietas, los bandidos han encontrado formas ingeniosas de aprovechar el entorno en beneficio propio. A medida que exploras, descubres unas discretas letrinas construidas estratégicamente entre las grietas de las rocas. Estos espacios ocultos permiten a los bandidos mantener un mínimo de higiene, evitando que los olores indeseables se propaguen y delaten su presencia.\n"
        "La disposición cuidadosa de estas instalaciones asegura que los desperdicios se mantengan ocultos y alejados del campamento principal.\n"
        "Pero eso no es todo. A pocos pasos de las letrinas, te sorprende la presencia de un pequeño riachuelo que serpentea por la zona. Sus aguas cristalinas y puras ofrecen un recurso esencial para los bandidos: un suministro de agua fresca.\n"
        "Con astucia, han aprovechado este riachuelo para establecer un área improvisada de aseo personal y baño. Aquí, entre las orillas del riachuelo, los bandidos pueden refrescarse y lavarse, reduciendo el riesgo de olores indeseables que podrían delatar sus actividades.\n");

    if (riachuelo = clone_object("/obj/misc/fuente_ob.c")) {
        riachuelo->move(TO);
        riachuelo->set_name("riachuelo");
        riachuelo->set_short("Riachuelo de aguas claras");
        riachuelo->set_main_plural("Riachuelos de aguas claras");
        riachuelo->generar_alias_y_plurales();
        riachuelo->set_long("Es un pequeño riachuelo que surge de la espesa maraña de plantas y maleza del oeste, y que a su vez delimita todo el perímetro del campamento. Sus aguas se encuentran en perfecta calma, rota por algún que otro pececillo que nada tranquilamente\n"
            "Este riachuelo proporciona unos recursos muy importantes para los bandidos. El agua que es vital para cualquier campamento, y la pesca, por muy austera que sea.\n");
    }
set_zone("campamento_faucesombra");
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/faucesombras", 2 + random(3));
}
