//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/outside.c";

void setup() {
    add_exit(N, CAMPBAN + "camp_8.c", "plain");
    add_exit(O, CAMPBAN + "camp_6.c", "plain");
    add_exit(S, CAMPBAN + "camp_2.c", "plain");
    set_light(70);
    set_short("Campamento de bandidos: Sector oriental");
    set_long("Caminas hacia la zona este del campamento, donde se encuentran más tiendas de campaña y refugios improvisados de los bandidos. Las tiendas están dispuestas en un patrón ordenado, brindando una apariencia de organización en medio del entorno salvaje.\n"
        "Los colores desgastados de las telas contrastan con el verdor circundante, mientras el viento agita suavemente las lonas. Puedes escuchar murmullos y el tintineo ocasional de utensilios, revelando la presencia de los bandidos en su santuario temporal.\n");
set_zone("campamento_faucesombra");
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/faucesombras", 2 + random(3));

}
