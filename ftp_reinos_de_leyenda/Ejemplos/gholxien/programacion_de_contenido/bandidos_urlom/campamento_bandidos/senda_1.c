//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/bosque.c";

void setup() {
    add_exit(NO, BOSQUEURLOM + "bosq3_62.c", "bosque");
    add_exit(E, CAMPBAN + "senda_2.c", "bosque");
    fijar_bosque(0);
    set_light(70);
    set_short("Bosque de Urlom: Entrada a una senda oculta");
    set_long(
        "Te encuentras al comienzo de una senda estrecha y oculta entre densos matorrales y plantas entrelazadas.\n"
        "Los juncos se inclinan sobre ti, proporcionando una sombra espesa que envuelve el camino y crea una sensación de aislamiento. La densa vegetación es tan tupida que apenas se distingue el camino que se abre ante ti, ocultándolo como un secreto celosamente guardado.\n"
        "Cada paso que das despierta el crujir suave de hojas secas y ramas quebradizas, sumergiéndote aún más en la atmósfera silenciosa y enigmática del lugar. Una suave brisa agita las hojas y las ramas entretejidas, como si susurrase secretos de un pasado oculto.\n");
}
