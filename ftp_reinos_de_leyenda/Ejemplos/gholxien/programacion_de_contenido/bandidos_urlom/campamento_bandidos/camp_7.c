//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/bosque.c";

void setup() {
    add_exit(N, CAMPBAN + "camp_10.c", "plain");
    add_exit(E, CAMPBAN + "camp_6.c", "plain");
    add_exit(S, CAMPBAN + "camp_4.c", "plain");
    set_light(70);
    fijar_bosque(2);
    set_short("Campamento de bandidos: Sector occidental");
    set_long("En la zona oeste del campamento se alza un árbol majestuoso y robusto, destacando entre la exuberante vegetación circundante. Su tronco ancho y sus ramas extendidas brindan un soporte ideal para una plataforma de madera cuidadosamente construida.\n"
        "Esta plataforma se encuentra estratégicamente ubicada en lo más alto del árbol, fusionándose hábilmente con su entorno y mimetizándose con la naturaleza que la rodea. La plataforma de vigilancia, apoyada en vigas robustas y asegurada con cuerdas discretas, se integra perfectamente con las ramas y hojas del árbol.\n"
        "Su diseño está pensado para ofrecer una posición elevada y estratégica, desde donde se obtiene una vista amplia del campamento y sus alrededores. Sin embargo, gracias a un camuflaje expertamente implementado, solo aquellos que conocen su ubicación exacta pueden distinguirla fácilmente entre la frondosidad.\n");
    add_item("arbol", "Es un árbol magestuoso que se alza en esta parte del campamento. Su tronco es recto, señalando como una torre hacia arriba, dificultando enormemente la escalada. Observas una especie de huecos en el tronco que llaman enormemente tu atención.\n");
add_item("huecos", "Lo que creías que eran un par de huecos en realidad son escalones, tallados en el mismísimo tronco del árbol que seguramente tengan la utilidad de ayudar a acceder a la plataforma que se atisba desde aquí. Los peldaños ásperos y desgastados se entrelazan con la textura natural del tronco, permitiendo un ascenso discreto y silencioso. Cada detalle ha sido meticulosamente considerado para evitar llamar la atención no deseada y garantizar la seguridad de los bandidos en su puesto de vigilancia.\n");
fijar_ruta_arbol(CAMPBAN + "plataforma_vigilancia");
set_zone("campamento_faucesombra");
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/faucesombras", 2 + random(3));

}
