//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/outside.c";

void setup() {
    add_exit(N, CAMPBAN + "camp_11.c", "plain");
    add_exit(E, CAMPBAN + "camp_8.c", "plain");
    add_exit(O, CAMPBAN + "camp_10.c", "plain");
    add_exit(S, CAMPBAN + "camp_6.c", "plain");
    set_light(70);
    set_short("Campamento de bandidos: Sector septentrional");
    set_long("Tus ojos se posan en una estructura de aspecto robusto y fortificado. Se trata de la armería de los bandidos, un edificio de madera reforzado con hierro y protegido por guardias vigilantes. Desde el interior de la armería, se escuchan sonidos metálicos y el murmullo de voces mientras los bandidos afilan sus armas y se preparan para el próximo asalto.\n"
        "Las paredes están adornadas con una variada exhibición de espadas, hachas, arcos y flechas, todas ellas testigos de los actos violentos perpetrados por estos criminales. La atmósfera es tensa, cargada con la energía del poder y la violencia latente, recordándote que estás en territorio de bandidos dispuestos a luchar por su supervivencia.\n");
set_zone("campamento_faucesombra");
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/faucesombras", 2 + random(3));

}
