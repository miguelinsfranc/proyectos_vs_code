//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/outside.c";

void setup() {
    add_exit(NE, CAMPBAN + "camp_2.c", "plain");
    add_exit(N, CAMPBAN + "camp_3.c", "plain");
    add_exit(NO, CAMPBAN + "camp_4.c", "plain");
    add_exit(S, CAMPBAN + "senda_5.c", "bosque");
    set_light(70);
    set_short("Campamento de bandidos: Entrada");
    set_long("El sendero del sur te conduce a través de una densa vegetación hasta la entrada del campamento. Los altos árboles y espesos arbustos rodean el área, ocultando el campamento a la vista de los intrusos.\n"
        "El aire está cargado de un aroma terroso y fresco, mientras avanzas por el sendero marcado solo por la luz filtrada entre las hojas. A medida que te adentras, puedes sentir una sensación de expectativa en el ambiente, como si el campamento guardara secretos oscuros entre sus sombras.\n");
}
