//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/room.c";

void setup() {
    add_exit(S, CAMPBAN + "camp_11.c", "standard");
    add_exit(N, CAMPBAN + "lider_2.c", "standard");
	modify_exit(N,({"function","pasar"}));
	modify_exit(S,({"function","pasar2"}));
    set_light(70);
    set_short("Campamento de bandidos: Sala de guardia");
    set_long("Este recinto es el lugar donde los guardaespaldas de élite de Roderick se reúnen para proteger y vigilar la tienda principal. Sus robustas paredes están construidas con una tela tensa de tonos oscuros, resistente a los embates del exterior.\n"
        "La entrada se cubre con una cortina de tela pesada que puede apartarse fácilmente para permitir el acceso. En el centro de la sala, un espacio abierto sin muebles ha sido diseñado estratégicamente para permitir un movimiento rápido y sin obstáculos.\n"
        "El suelo se encuentra revestido con una lona resistente y uniforme, proporcionando una base firme y estable. A lo largo de las paredes, se han improvisado estantes de madera donde se almacenan meticulosamente cuerdas, herramientas de supervivencia y provisiones.\n"
        "La iluminación proviene de antorchas estratégicamente ubicadas en soportes de metal fijados al suelo. Las llamas arrojan un resplandor anaranjado que danza en las paredes de tela, creando sombras que resaltan los pliegues de la tienda. A través de pequeñas aberturas en la tela, el aire fresco se filtra suavemente, manteniendo una atmósfera agradable en la sala.\n");
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/goldranh",1);
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/rashen",1);
}

int pasar(string salida,object caminante,string msj_especial) {
	if (secure_present("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/goldranh.c", TO) || secure_present("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/rashen.c", TO)) {
		return notify_fail("Los vigilantes te impiden pasar.\n");
	}
	return 1;
}

int pasar2(string salida,object caminante,string msj_especial) {
if (!secure_present("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/rashen.c", TO) && !secure_present("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/goldranh.c", TO)) return 1;
if (caminante->dame_peleando() <=0) return 1;
if (caminante->dame_ts("agilidad")) {
	tell_object(caminante,"Haces amago de retroceder, pero coges impulso y sales raud" + caminante->dame_vocal() + " de la sala, sorprendiendo con tu maniobra a los guardaespaldas.\n");
return 1;
}
		return notify_fail("Los guardaespaldas te impiden el paso con sus armas y atacan con coordinación\n");
	}