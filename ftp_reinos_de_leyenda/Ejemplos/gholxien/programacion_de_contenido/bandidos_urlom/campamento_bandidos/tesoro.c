//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/room.c";

void setup() {
    add_exit(S, CAMPBAN + "lider_2.c", "standard");
    set_light(60);
    set_short("Campamento de bandidos: Estancia de Roderick");
    set_long("Esta estancia se encuentra al norte de la zona principal de la tienda. En su interior, una sólida litera de campaña de madera espera al líder, ofreciendo descanso y comodidad. Una manta enrollada y una almohada están cuidadosamente dispuestas sobre la litera para garantizar un sueño reparador.\n"
        "A un lado de la litera, se encuentra un baúl de madera reforzado que alberga los tesoros más valiosos del grupo de bandidos. En las paredes de tela, cerca de la litera, se han colocado sencillos estantes de madera donde se guardan los objetos personales y pertenencias de Roderick Faucesombra.\n"
        "Estos estantes están meticulosamente organizados y carecen de adornos innecesarios. Una pequeña lámpara de aceite, colocada sobre un pequeño taburete junto a la litera, proporciona una iluminación suave y acogedora en la habitación. El suelo está cubierto por una lona gruesa y resistente que se extiende desde la litera hasta los bordes de la habitación.\n");
}
