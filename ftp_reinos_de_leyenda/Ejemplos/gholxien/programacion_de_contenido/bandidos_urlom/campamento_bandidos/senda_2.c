//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/bosque.c";

void setup() {
    add_exit(O, CAMPBAN + "senda_1.c", "bosque");
    add_exit(NE, CAMPBAN + "senda_3.c", "bosque");
    fijar_bosque(0);
    set_light(70);
    set_short("Bosque de Urlom: Senda silente");
    set_long(
        "Te adentras en la senda, empujando suavemente los matorrales a medida que avanzas. El sonido del viento susurra entre las hojas de los árboles altos, creando una melodía misteriosa que envuelve el ambiente.\n"
        "El suave crujir de ramas secas bajo tus pies se mezcla con el canto de pájaros ocultos entre las ramas, mientras los arbustos espinosos intentan detenerte en tu camino.\n");
}
