//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/outside.c";

void setup() {
    add_exit(NO, CAMPBAN + "camp_11.c", "plain");
    add_exit(O, CAMPBAN + "camp_9.c", "plain");
    add_exit(S, CAMPBAN + "camp_5.c", "plain");
    set_light(70);
    set_short("Campamento de bandidos: Sector nororiental");
    set_long("En la esquina noreste del campamento se extiende un área dedicada exclusivamente al entrenamiento y la práctica de habilidades. Este espacio está cuidadosamente delimitado por una cúpula de abjuración transparente que rodea el perímetro, creando una barrera difuminada que oculta de manera sutil los intensos acontecimientos que tienen lugar dentro.\n"
        "Dentro de esta área, se pueden observar varios muñecos de madera perfectamente alineados y dispuestos estratégicamente. Estos simulacros inanimados, con marcas evidentes de golpes y desgaste, aguardan pacientemente a ser utilizados como objetivos para el entrenamiento de combate.\n"
        "Sus cuerpos llenos de astillas y marcas ofrecen un testimonio silencioso de los esfuerzos arduos y las destrezas marciales que se ejercitan aquí. Además, se encuentran numerosas dianas meticulosamente colocadas a lo largo de la zona de entrenamiento.\n"
        "Estas estructuras circulares exhiben patrones de impactos precisos y están suspendidas con cuerdas tensas entre los árboles. Sirven como blancos para que los arqueros y lanzadores de proyectiles perfeccionen su puntería y precisión, enviando flechas y otros proyectiles que atraviesan el aire con destreza y determinación.\n");
set_zone("campamento_faucesombra");
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/faucesombras", 2 + random(3));

}
