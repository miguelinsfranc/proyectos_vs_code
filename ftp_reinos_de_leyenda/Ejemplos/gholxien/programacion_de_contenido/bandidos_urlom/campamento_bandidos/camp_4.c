//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/outside.c";

void setup() {
    add_exit(N, CAMPBAN + "camp_7.c", "plain");
    add_exit(E, CAMPBAN + "camp_3.c", "plain");
    add_exit(SE, CAMPBAN + "camp_1.c", "plain");
    set_light(70);
    set_short("Campamento de bandidos: Sector suroccidental");
    set_long("Al llegar a la zona suroccidental del campamento, te adentras en un laberinto de vegetación espesa que se enreda y entrelaza formando una maraña intrincada.\n"
        "Los arbustos y enredaderas parecen conspirar para ocultar lo que se encuentra más allá. A medida que te abres paso a través de pequeñas aperturas entre la maleza, descubres un conjunto de chozas rústicas dispersas sin un orden aparente.\n"
        "Estas chozas, construidas con madera áspera y hojas secas, contrastan con la disciplina y rigidez del resto del campamento. Aquí, los bandidos más peligrosos y despiadados han establecido su morada, buscando un refugio menos estructurado y más acorde con su naturaleza violenta.\n"
        "Las chozas, de aspecto rudimentario, parecen haber sido construidas rápidamente, sin preocuparse por una arquitectura consistente. A medida que te aventuras entre ellas, el ambiente se vuelve más caótico y desorganizado. Voces bajas y risas maliciosas se escuchan entre sus paredes improvisadas. En las sombras, vislumbras siluetas sospechosas moviéndose de un lado a otro, insinuando actividades clandestinas.\n");
set_zone("campamento_faucesombra");
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/faucesombras", 2 + random(3));

}
