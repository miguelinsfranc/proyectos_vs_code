//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/bosque.c";

void setup() {
    add_exit(N, CAMPBAN + "senda_3.c", "bosque");
    add_exit(NE, CAMPBAN + "senda_5.c", "bosque");
    fijar_bosque(0);
    set_light(70);
    set_short("Bosque de Urlom: Senda umbría");
    set_long(
        "Avanzas con dificultad por el camino serpenteante que se abre entre los matorrales y la maleza espesa. Las plantas enredadas y retorcidas parecen conspirar contra tu avance, como si se entrelazaran intencionadamente para mantener su secreto oculto. Cada paso que das es un desafío, con espinas afiladas rozando tu piel y lianas traicioneras que amenazan con enredarte.\n"
        "El aire se torna denso y húmedo, dificultando la respiración. A medida que sigues adelante, una sensación persistente de estar siendo vigilado te acecha desde las sombras. La maleza se cierra a tu alrededor, creando un laberinto claustrofóbico que parece querer desorientarte.\n"
        "La tenue luz que se filtra entre las ramas añade un efecto deslumbrante al paisaje, obligándote a entrecerrar los ojos repetidamente para enfocar tu visión. Los sonidos silenciosos de la naturaleza son interrumpidos por un crujido furtivo aquí y allá, como si fueras acompañado por miradas ocultas y acechantes.\n");
}
