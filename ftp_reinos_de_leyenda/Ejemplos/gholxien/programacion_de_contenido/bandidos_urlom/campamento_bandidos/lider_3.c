//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/room.c";

void setup() {
    add_exit(SO, CAMPBAN + "lider_2.c", "standard");
    set_light(60);
    set_short("Campamento de bandidos: Rincón de Ayrah");
    set_long("Te adentras en el rincón de Ayrah, un refugio tranquilo dentro de la tienda de campamento. Este espacio privado, discretamente ubicado entre las telas, refleja su personalidad enigmática.\n"
        "El suelo está cubierto por una suave alfombra en tonos oscuros, proporcionando una base cálida y confortable para los pies. Las paredes están decoradas con cortinas de tela fina, permitiendo que la luz natural penetre suavemente y cree un ambiente acogedor.\n"
        "En un rincón de la estancia, encuentras una cama con dosel, cubierta con sábanas de colores oscuros y suaves. Junto a la cama, pequeñas mesas de madera albergan velas encendidas que crean una luz suave y relajante. Un delicado aroma a incienso flota en el aire, proporcionando una atmósfera tranquila.\n"
        "En otra esquina, ves un escritorio de madera elegante y funcional, donde Ayrah dedica tiempo a sus estudios y prácticas. Sobre el escritorio, se encuentran pergaminos enrollados y libros de hechizos cuidadosamente colocados en orden. Un candelabro ilumina el espacio, brindando luz adecuada para la lectura y el trabajo.\n"
        "En el centro de la habitación, hay un espejo antiguo con un marco decorativo. Alrededor del espejo, estantes sostienen cristales y pequeños objetos de interés, cada uno con su propio propósito y significado.\n");
}
