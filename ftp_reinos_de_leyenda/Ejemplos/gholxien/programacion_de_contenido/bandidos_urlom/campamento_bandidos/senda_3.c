//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/bosque.c";

void setup() {
    add_exit(SO, CAMPBAN + "senda_2.c", "bosque");
    add_exit(S, CAMPBAN + "senda_4.c", "bosque");
    fijar_bosque(0);
    set_light(70);
    set_short("Bosque de Urlom: Senda serpenteante");
    set_long(
        "La senda se estrecha aún más, serpenteando entre la maleza densa. Aquí, las plantas se enredan y entrelazan formando un laberinto natural que dificulta la visibilidad.\n"
        "A medida que avanzas con cautela, puedes notar marcas y señales talladas en los árboles, probablemente dejadas por exploradores anteriores para indicar el camino seguro a través de este entramado de vegetación.\n");
}
