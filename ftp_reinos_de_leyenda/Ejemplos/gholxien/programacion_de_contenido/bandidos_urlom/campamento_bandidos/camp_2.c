//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/bosque.c";

void setup() {
    add_exit(N, CAMPBAN + "camp_5.c", "plain");
    add_exit(O, CAMPBAN + "camp_3.c", "plain");
    add_exit(SO, CAMPBAN + "camp_1.c", "plain");
    set_light(70);
    fijar_bosque(1);
    set_short("Campamento de bandidos: Sector suroriental");
    set_long("En la zona sureste del campamento, la naturaleza reclama su espacio. Árboles majestuosos se alzan imponentes, filtrando la luz solar y creando una penumbra cautivadora.\n"
        "A medida que avanzas entre las ramas y las sombras danzantes, te encuentras con refugios improvisados entre la maleza. Los susurros del viento entre las hojas y el crujido de las ramas secas bajo tus pies contribuyen a la atmósfera de misterio y ocultación.\n"
        "Entre la densa vegetación, se pueden apreciar pequeñas y llamativas manchas de color. Un festín visual se despliega ante tus ojos, ya que una variedad de setas, de distintos tamaños y formas, salpican el suelo y los troncos caídos. Algunas son diminutas y puntiagudas, mientras que otras crecen en forma de cúpula, como pequeños santuarios naturales.\n"
        "Los colores vibrantes de estas setas capturan la atención, ofreciendo una paleta cautivadora. Tonos intensos de rojo, violeta y amarillo se mezclan con matices más oscuros de negro y azul, creando una sinfonía cromática en medio del verdor circundante.\n");
set_zone("campamento_faucesombra");
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/faucesombras", 2 + random(3));
}
