//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/room.c";

void setup() {
    add_exit(SE, CAMPBAN + "lider_2.c", "standard");
    set_light(60);
    set_short("Campamento de bandidos: Rincón de Sylara");
    set_long("Te encuentras en el refugio personal de Sylara. La estancia está impregnada de una atmósfera inquietante. El suelo de lona resistente está cubierto por una discreta alfombra de piel de lobo, que añade un toque rústico a la estancia.\n"
        "Las paredes de lona blanca están adornadas con recortes de tela negra que ondean suavemente con la brisa, creando sombras danzantes en la tenue luz. En una esquina, una cama individual con sábanas negras y una manta de lana oscura se encuentra prolijamente dispuesta. Un dosel de tela negra se extiende sobre la cama, proporcionando una sensación de seguridad y aislamiento en medio de la naturaleza salvaje.\n"
        "Un pequeño escritorio de madera cruda descansa contra una pared, con un cuaderno abierto y un par de dagas afiladas sobre él. La luz de una lámpara de aceite ilumina el espacio, permitiendo a Sylara examinar con precisión sus herramientas letales y tomar notas meticulosas.\n"
        "Junto al escritorio, una silla de cuero desgastada invita a sentarse, mostrando marcas de uso y recordando los largos días de travesías y combates. A su lado, una mochila de cuero está cuidadosamente colocada, lista para ser cargada con provisiones y armas antes de aventurarse en el exterior. En otra esquina, una pequeña estantería de madera rústica exhibe una colección de frascos de vidrio oscuro, cada uno etiquetado con precisión y contenido con venenos letales.\n"
        "También se pueden ver rollos de vendajes y pequeños frascos de ungüentos curativos, mostrando la dualidad de Sylara como cazadora y sanadora de sí misma.\n");
}
