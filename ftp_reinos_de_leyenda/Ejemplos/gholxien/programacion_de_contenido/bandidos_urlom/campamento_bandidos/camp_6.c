//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/outside.c";

void setup() {
    object fogata;

	add_exit(E, CAMPBAN + "camp_5.c", "plain");
    add_exit(N, CAMPBAN + "camp_9.c", "plain");
    add_exit(O, CAMPBAN + "camp_7.c", "plain");
    add_exit(S, CAMPBAN + "camp_3.c", "plain");
    set_light(70);
    set_short("Campamento de bandidos: Sector central");
    set_long("En el corazón del campamento se extiende una amplia área abierta, rodeada por las imponentes tiendas de los bandidos. En cualquier momento del día o de la noche, esta zona central emana una atmósfera intrigante y cautivadora.\n"
        "El paisaje se transforma en un escenario pintoresco donde convergen las actividades de los forajidos. En el centro se alza una fogata imponente, cuyo fuego danza con incesante vitalidad. Alimentada por troncos gruesos y resinosos, las llamas proyectan su resplandor cálido y envolvente. El crepitar del fuego, como una melodía ancestral, rompe el silencio y llena el aire con su mágica sinfonía.\n"
        "Las chispas, pequeñas estrellas fugaces, ascienden hacia el cielo, disipándose en la vastedad. Alrededor de la fogata, dispersos en un patrón aparentemente improvisado, se encuentran rústicos asientos tallados en troncos y piedras. Estos lugares de descanso, desgastados por el paso del tiempo y la agitación constante, son testigos mudos de las reuniones y conspiraciones que se fraguan en las sombras.\n"
        "Es aquí donde los bandidos se agrupan en pequeñas facciones, compartiendo sus escasas provisiones y tejiendo planes en susurros cautelosos. El suelo bajo tus pies, marcado por los surcos y huellas de innumerables pasos, narra la historia de la vida agitada que pulsa en este punto focal del campamento. Cada pisada deja su impronta, cada marca revela la presencia constante de los forajidos. La arena y la tierra se entrelazan en una danza irregular, mostrando indicios de recientes actividades, como una obra de arte efímera que se renueva con cada paso.\n");

fogata= clone_object("/std/object");
	fogata->set_name("fogata");
	fogata->add_alias(({"fogata","fuego"}));
	fogata->add_plural(({"fogatas"}));
	fogata->set_short("Fogata humeante");
	fogata->set_main_plural("Fogatas humeantes");
	fogata->set_long("En el centro del campamento se levanta esta magestuosa fogata, cuyos leños arden con lentitud y parsimonia.\n"
	"Las chispas se elevan hasta perderse en el firmamento, cual estrellas fugaces, y el sonido chisporroteante de las llamas evoca en ti una calma imperecedera, una calma que solo puede ser rota por los sonidos furtivos y las voces maliciosas que llenan con su sonido este mágico ambiente.\n");
  fogata->fijar_genero(2);
	fogata->move(this_object());
	fogata->reset_get();
set_zone("campamento_faucesombra");
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/faucesombras", 2 + random(3));
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/cocinero", 2 + random(2));
}
