//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/bosque.c";

void setup() {
    add_exit(SO, CAMPBAN + "senda_4.c", "bosque");
    add_exit(N, CAMPBAN + "camp_1.c", "bosque");
    fijar_bosque(0);
    set_light(70);
    set_short("Bosque de Urlom: Umbral del campamento");
    set_long(
        "La senda finalmente se abre, revelando un espacio más amplio entre los matorrales. Aquí, ramas caídas y lianas estratégicamente colocadas crean una especie de barrera natural que oculta la entrada al campamento.\n"
        "Puedes ver una abertura cuidadosamente disimulada en la maleza, que parece conducir al interior de un lugar desconocido.\n"
        "El ambiente se torna más denso a medida que te acercas, sintiendo la proximidad de lo que aguarda más allá, una presencia latente que parece emerger de las sombras.\n");
}
