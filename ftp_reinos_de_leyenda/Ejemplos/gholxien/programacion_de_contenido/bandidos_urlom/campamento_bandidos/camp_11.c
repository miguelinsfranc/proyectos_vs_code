//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/outside.c";

void setup() {
    add_exit(N, CAMPBAN + "lider_1.c", "standard");
    add_exit(SE, CAMPBAN + "camp_8.c", "plain");
    add_exit(S, CAMPBAN + "camp_9.c", "plain");
    add_exit(SO, CAMPBAN + "camp_10.c", "plain");
    set_light(70);
    set_short("Campamento de bandidos: Ante una gran tienda");
    set_long("Desde la distancia, puedes divisar la majestuosa tienda del jefe en la parte norte del campamento. Su tamaño y diseño resaltan entre las demás estructuras, proclamando el estatus y la autoridad de Roderick Faucesombra.\n"
        "Aunque estás fuera de su alcance inmediato, puedes sentir la presencia opresiva de la tienda principal mientras te acercas. Una combinación de curiosidad y precaución te invade, ya que sabes que adentrarte más implicará enfrentar los peligros y desafíos que aguardan en el interior de este dominio poderoso.\n");
set_zone("campamento_faucesombra");
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/faucesombras", 2 + random(3));
}
