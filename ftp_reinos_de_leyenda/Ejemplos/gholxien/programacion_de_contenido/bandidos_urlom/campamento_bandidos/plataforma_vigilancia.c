#include "base_campamento.h"
inherit "/std/arbol_bosque.c";

void setup() {
set_short("Campamento de bandidos: Plataforma de vigilancia");
set_long("Desde la plataforma, rodeada por las ramas extendidas del árbol, se despliega una vista privilegiada del campamento y su entorno boscoso.\n"
"Incluso la construcción de la plataforma ha sido cuidadosamente camuflada, con ramas y hojas entrelazadas de manera sutil que ocultan la estructura desde abajo. La plataforma se convierte en un remanso de sombra, brindando protección adicional y ocultamiento para aquellos que la utilizan para mantener una vigilancia constante.\n"
"Este ingenioso ejemplo de ingeniería y adaptación a la naturaleza permite a los bandidos realizar sus tareas de vigilancia sin llamar la atención. Desde esta posición elevada y camuflada, tienen la ventaja de observar y proteger su campamento, manteniendo en secreto tanto su ubicación como la presencia de sus vigilantes.\n");
fijar_abajo(CAMPBAN + "camp_7");
}