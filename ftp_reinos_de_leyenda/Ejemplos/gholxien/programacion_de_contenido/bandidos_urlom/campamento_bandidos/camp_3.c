//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/outside.c";

void setup() {
    add_exit(E, CAMPBAN + "camp_2.c", "plain");
    add_exit(N, CAMPBAN + "camp_6.c", "plain");
    add_exit(O, CAMPBAN + "camp_4.c", "plain");
    add_exit(S, CAMPBAN + "camp_1.c", "plain");
    set_light(70);
    set_short("Campamento de bandidos: Sector meridional");
    set_long("El sector meridional del campamento está impregnado de actividad y organización. Barriles y cajas se encuentran apilados meticulosamente, listos para ser utilizados.\n"
        "Carpas de diversos tamaños están dispuestas en hileras ordenadas, creando un sentido de estructura en medio del caos. El sonido de voces enérgicas y el tintineo de herramientas metálicas llenan el aire, mientras los bandidos se mueven con propósito, llevando a cabo sus tareas asignadas.\n");
set_zone("campamento_faucesombra");
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/faucesombras", 2 + random(3));
}
