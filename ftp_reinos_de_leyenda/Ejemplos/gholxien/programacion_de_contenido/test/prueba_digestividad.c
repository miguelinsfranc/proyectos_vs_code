// Holemdolf Dic-'06
// Pan que pueden crear los mismos jugadores en la pastelería de Nimbor.

inherit "/obj/comestible.c";  

void setup()  {
	set_name("pan"); 
	set_short("Pan de Trigo de %^BOLD%^WHITE%^Nimbor%^RESET%^"); 
	set_main_plural("Panes de Trigo de %^BOLD%^WHITE%^Nimbor%^RESET%^"); 
 	set_long("Es un pan hecho a base de trigo y levadura. Tiene un color "
     "amarillento. Este en concreto ha sido manufacturado en Nimbor, donde se "
     "siembra el mejor trigo de todo Dalaensar. Su textura es mucho más "
     "esponjosa que el pan normal y su barra bastante robusta, casi parece un "
     "bizcocho.\n");
	add_alias(({"pan","trigo","pan de nimbor"}));
	add_plural(({"panes","trigo","panes de nimbor"}));
	fijar_peso(150); 
	fijar_genero(1);
	fijar_valor(0);
	fijar_tipo(2); /*Tipo de comestible: 1:lacteo, 2:pasteleria, 3:carne, 4:bebida, 5:fruta*/
	fijar_digestividad(1);
	fijar_alcohol(0);
	fijar_valor(500 * 15);
	////fijar_caducidad(150000); /*Hb en los que caducará el comestible*/
}
