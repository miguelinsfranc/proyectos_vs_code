//Rutseg 23-X-2002 Para controlar todo el tema de altura, peso, color del pelo, etc...

#define ALTURA complexion[0]
#define PESO complexion[1]
#define COLOR_PELO complexion[2]
#define RIZO_PELO complexion[3]
#define LONG_PELO complexion[4]
#define COLOR_OJOS complexion[5]
#define FORMA_OJOS complexion[6]
#define MUSCULATURA complexion[7]
#define PELUDO complexion[8]
#define MANO_HABIL complexion[9]
#define EDAD complexion[10]//Hace referencia a la edad modificada por procesos especiales como la magia

#define T_COLORES_PELO 17
#define COLORES_DE_PELO ({"blanco","plateado","azabache","pelirrojo","turquesa","zanahoria","rubio","castaño","negro","canoso","cobrizo","dorado","verde","azul", "gris", "marrón", "plateado"})
#define T_RIZOS_PELO 3
#define RIZOS_DE_PELO ({"rizado","ondulado","liso"})
#define T_LONGS_PELO 10
#define LONGS_DE_PELO ({"calvo","corto","melena","greñoso","largo","recogido","trenzado","escalado","rapado","cresta"})

#define T_COLORES_OJOS 14
#define COLORES_DE_OJOS ({"grises","negros","dorados","plateados","azules","turquesa","marrones","castaños","verdes","rojos","violetas","avellanados","ambarinos","blancos"})
#define T_FORMAS_OJOS 4
#define FORMAS_DE_OJOS ({"grandes","entrecerrados","rasgados","pequeños"})

#define T_MUSCULATURAS 6
#define MUSCULATURAS ({"hercúlea","fornida","atlética","enclenque","delgada","regordeta"})

#define T_PELUDOS 5
#define PELUDOS ({"imberbe","peluda","escamosa","viril","femenina"})

#define MANOS_HABILES ({"ninguna","diestro","zurdo","ambidiestro"})
#define TAMANYO_COMPLEXION 11//Tamaño del array
