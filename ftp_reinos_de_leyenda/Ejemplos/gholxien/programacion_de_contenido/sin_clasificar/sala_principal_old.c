//Rutseg 15-XI-2002 RL2 Actualizando... [Al inicio del código del objeto has de incluir un comentario diciendo tu nombre y la fecha]
//VALDOR '99. <- Esto que ves es un comentario. Si vas a hacer comentarios pon dos barras antes de la linea.
//Si quieres poner comentarios diversos en lineas diversas( o desabilitar una zona del fichero atento:
/*
	lalalalal
	esto no lo procesara (leera)
	por las barras y estrellas ;)
	aunque suena americano jejej
	yata
*/
/*Las sentencias que empiezan con un # se llaman opciones de compilación, ya que se ejecutan
antes de compilar el codigo de los objetos LPC.
Los básicos que ha de conocer todo aprendiz son #define e #include.
#define Se usa para substituir en todo el objeto una sentencia por algo ya prefijado, por ejemplo:

#define CREADOR "Rutseg"

Transcribirá literalmente allá donde vea de nuestro objeto la palabra CREADOR por "Rutseg", de ese
modo, si ponemos en una función:
	add_item("estatua","Es una estatua de "+CREADOR+".");
Al cargar en memoria el objeto, el driver substituirá CREADOR por "Rutseg" y lo que tendremos
escrito en esa línea será:
	add_item("estatua","Es una estatua de "+"Rutseg"+".");
Atención a ello, porque no es lo mismo que add_item("estatua","Es una estatua de Rutseg.");
aunque hay gente que todavía cree que funciona de esta última manera.
Un error muy común en los aprendices es poner:
#define CREADOR "Rutseg";
Eso daria:
	add_item("estatua","Es una estatua de "+"Rutseg";+".");
Lo cual es claramente incorrecto y produciría un parse error.

La opción de compilación #include coge el archivo que le estemos indicando y cuando se carga
este objeto en memoria escribe todo el archivo indicado en esta parte del codigo de nuestro objeto.
Es muy util para poner varios #define o cosas similares que luego vayas a usar en muchos
archivos. Recuerda que lo que ocupara en memoria este objeto incluira todo el codigo del
archivo incluido. Los archivos .h los llamaremos "bibliotecas" porque suelen contener recursos,
mientras los .c son "objetos" ya que pueden ser compilados.
Es recomendable que hagas un more de dicho archivo para saber que es lo que estás añadiendo
al objeto. En este caso una de las cosas que verás es el #define:
#define APRENDICES "/d/aprendizaje/"
Ello lo has de interpretar como si el define estubiera escrito en este archivo, en el punto
en que esta puesto el include.*/
#include "path.h"

/*La sentencia inherit nos permite hacer lo que llamamos "heredar" otro objeto.
Esta acción nos permite tener en nuestro objeto todo el código del objeto heredado, pero no
te confundas, no es como #include, no es una copia literal, sino que lo que hace es añadir
sus funciones y variables (poniendolas a 0).
Al heredar un objeto, este se carga también en memoria como un objeto maestro.
Ten en cuenta que heredar un archivo te permite definir una función que ya existia en el
objeto base, por ejemplo init()  ya existe en /std/room.c (hacer eso se conoce como "enmascarar la funcion").
Mira las explicaciones en la función init() si quieres saber más sobre enmascarar funciones ya existentes
en el archivo base.
Heredar nos permite tener ya un montón de funciones básicas para realizar nuestros objetos sin
tener que reescribirlas todas cada vez, y a la vez nos permite modificar ("enmascarar") aquellas
que queremos que hagan cosas adicionales.*/
inherit "/std/room.c";
/*En /std/ están los tipos básicos de room. Actualmente (15-XI-2002) tenemos las siguientes:

room.c -Tipo general de room, normalmente usada en interiores.
outside.c -Tipo de room en el exterior, incluye clima.
bosque.c -Tipo especial de outside situado en un bosque. La función set_bosque(x); nos permite
	fijar el espesor de la room: 0- Claro en el bosque sin arboles. 1- Arboles, baja densidad. 2- Bosque frondoso.
subterraneo.c -Tipo de room situada bajo tierra, normalmente cuevas, alcantarillas y similares.
suboscuridad.c -Tipo de room usada para el mundo existente bajo la superficie, donde habitan los duergars y drows.
underwater.c -Tipo de room usada para rooms que esten bajo el agua.
nieve.c -Tipo de room usada para zonas con nieves perpetuas.
pantano.c -Zonas empantanadas.
*/

//La función setup() se usa en las rooms y los objetos para inicializarlos dandoles unos valores básicos
void setup()
{
	/*Este es el "titulo" de la room, ha de dar una idea general de que es la room y su situación
	El color que le des es muy importante, ya que ha de ambientar, pero no ha de ser estresante,
	como norma general no uses en ningún sitio más de 2 colores, el uso de un único color es
	recomendable, pero solo usa colores en cosas como esta para resaltar partes importantes del
	juego*/
	set_short("%^BOLD%^Sala Principal%^RESET%^");
	/*Esta es la descripción detallada de la room, aquello que ve el jugador cuando mira en la
	room. Nunca uses colores en ella.
	Recuerda que algunos carácteres necesitan la barra de escape delante, ya que normalmente
	indicarian otra cosa, como el final del string en el caso de las dobles comillas:
		\" = "
		\\ = \
		\n = Salto de línea
		\t = Tabulación
	NOTA: Al final de la descripción usa solo un \n ya que el mirar ya incluye las separaciones adicionales,
	y al inicio nunca pongas un título, el mirar ya incluye el short. Piensa que algunas rooms
	que peudas ver por ahí tienen el código obsoleto y usan formas que ahora estarían mal.
	*/
	set_long("Te encuentras en la sala principal de aprendizaje. En esta zona aprenderás lo básico "
		"que hay que saber para  empezar a programar como aprendiz. Como no, antes de empezar "
		"tu trabajo como aprendiz deberás pasar una \"prueba\" para ver tu capacidad sobre el "
		"tema. Pon %^BOLD%^info%^RESET%^ para saber lo que tienes que hacer exactamente. Que "
		"hagas más no importa. Tienes una semana de plazo. Hasta que no hallas visitado todas las "
		"habitaciones del complejo no se te permitirá volver a tu home, joven aprendiz."
		"Te recomiendo encarecidamente que uses el %^BOLD%^more here%^RESET%^ siempre que acabes "
		"de ver una habitación con detalle para ver como se hace. Todo lo contenido "
		"en esta zona esta disponible para tu aprendizaje en /d/aprendices/.\nHay una campana "
		"colgada del techo.\n");
	/*La intensidad de la luz se fija con la siguiente función:
	0: Oscuridad total.
	100: Exposición directa al Sol.
	Cuando hagas una nueva room, usa el comando "man luz" para ver exactamente que quiere decir cada
	rango, y piensa que si es una room outside, esta luz ya se modifica sola segun se haga de dia o
	de noche, asi que normalmente sera 100 la luz fijada. En interiores dependera de la luz
	natural (ventanas, puertas) y artificial (velas, antorchas...).*/
	fijar_luz(60);

	/*Los add_item son una de las partes mas importantes de la room si quieres hacer que esta
	resulte "creible" e interesante de explorar. Piensa que si en tus rooms nunca pones items, o
	estos son escasos los jugadores perderán interés y se acostumbrarán a no mirarlos nunca,
	incluso dejarán de leer las descripciones, además las busquedas (quests o aventuras) serán
	mucho más sencillas de encontrar, ya que sabrán que si hay un item seguramente indica que
	es importante para alguna búsqueda.
	Aunque parezca pesado has de intentar que todo nombre ("sustantivo") en la descripción, tenga
	un item asociado al que mirar. Piensa que tendrás unos básicos que seguramente se irán repitiendo
	por toda la zona, con lo cuál el trabajo no es tan costoso como parece, y si muy enriquecedor
	para el juego. No hace falta que pongas el \n al final, lo añade el comando mirar el solo.*/
	add_item("campana","Una campana de oro finamente labrada con motivos de estrellas. Su badajo "
		"es enorme.");
	add_item("badajo","Es lo que hace que al mover la campana esta emita ruido.");
	/*Si quieres asociar más de un item a una misma descripción puedes hacerlo pasando un array
	de strings (tipo de dato: *string) en vez de un string (tipo de dato: string) como primer
	parámetro de la función:*/
	add_item(({"techo","pared","paredes","suelo"}),"Acabados con mármol de macael finamente pulido.");

	/*Para poner una señal en la room utiliza la función object add_sign(string long,string msj,string nombre,string alias)
	definida en la base heredada /std/room.c:
		-long: Descripción que verás al mirar el cartel.
		-msj: Mensaje que verá el jugador al leer el cartel.
		-nombre: Nombre con el que se verá el cartel en la room.
		-alias: Alias con el que podrán referirse los jugadores al cartel.
	*/
	add_sign("Un tablón de madera con algunas indicaciones adicionales.",
		"Existen dos acciones importantes en esta room que debes realizar:\n\n"
		"\t%^BOLD%^comandos%^RESET%^: Comandos basicos del unix para moverse entre ficheros.\n"
		"\t%^BOLD%^editor%^RESET%^: Editor del mud.\n\n"
		" Teclea una de las dos opciones.\n");

	/*Las salidas son uno de los puntos más importantes de una room. El color de estás es
	importantísimo para ambientar la zona, piensa que es una de las pocas sensaciones que
	recibirá el jugador. Los colores existentes actualmente son:
	
		"rojo_flojo": %^RED%^
		"amarillo_flojo": %^ORANGE%^
		"cian_flojo": %^CYAN%^
		"verde_flojo": %^GREEN%^
		"azul_flojo": %^BLUE%^
		"magenta_flojo": %^MAGENTA%^
		"blanco_flojo": %^WHITE%^
		"rojo": %^BOLD%^RED%^
		"amarillo": %^BOLD%^YELLOW%^
		"verde": %^BOLD%^GREEN%^
		"azul": %^BOLD%^BLUE%^
		"magenta": %^BOLD%^MAGENTA%^
		"blanco": %^BOLD%^WHITE%^
		"naranja": %^ORANGE%^
		"negro": %^BOLD%^BLACK%^
		"cian": %^BOLD%^CYAN%^ <- Este es el color usado por defecto
	*/
	set_exit_color("blanco");
	/*Para añadir una salida a la room usamos la función
	int add_exit(string direccion,string destino,string tipo)
	sus parametros son:
		direccion: El nombre de la salida, aquel que ves el la frase de las salidas.
		destino: La ruta del archivo del objeto de la room a la que lleva la salida.
		tipo: El tipo de salida. Existen varios actualmente (15-XI-2002):
			-standard: El valor por defecto o cuando no encaja en ningún otro.
			-corridor: Salidas en forma de pasillo.
			-plain: Cuando avanzas por un claro, un prado o similar.
			-door: Pone automáticamente una puerta en esa dirección (mirar "man puertas" para mas info)
			-stair: Cuando la salida son unas escaleras.
			-hidden: Cuando la salida no se ve a simple vista (no sale en la frase de salidas)
			-secret: Como hidden pero se supone que es una puerta.
			-gate: La entrada a un castillo o portalones similares.
			-road: La salida circula por un camino empedrado o bien cuidado.
			-path: La salida circula por un sendero.
			-bosque: La salida circula por el interior de un bosque (como path pero en un bosque).
			-pantano: La salida circula por tierras enfangadas.
			-muralla: La salida es una muralla que puede ser trepada mediante habilidades de escalada.

	NOTA: Para los destinos de la room es muy util usar defines establecidos en un archivo que luego
	incluyamos en toda la zona con la sentencia #include, de ese modo podemos cambiar de directorio la
	zona con bastante facilidad. En este caso usamos el #define APRENDICES definido en el archivo incluido
	"path.h" y que substituye APRENDICES por "/d/aprendizaje/"
	*/
	add_exit("sur",APRENDICES+"sala_principal","corridor");
}
/*init() es una función que se llama desde el driver a cualquier objeto (este tipo de funciones que
el driver espera que se encuentren en los objetos se llaman aplies).
La función es llamada en varios casos aunque de momento no es importante que comprendas exactamente
su funcionamiento, recuerda la explicación que hay aquí por si algún dia tienes que usarla así sabrás
donde buscar info sobre ella. ¿Cuando se llama init()?:
 - Cuando un objeto entra dentro de este objeto.
 - Cuando un objeto entra dentro del entorno de este objeto.
 - Cuando un objeto sale de este objeto.
 - Cuando un objeto sale del entorno de este objeto.
 - Cuando este objeto entra o sale de otro objeto.

En el caso de las rooms, init() ya está definido en el objeto base /std/room.c, pero resulta un lugar
ideal para poner un add_action, por ello normalmente enmascaramos la función para hacer cosas
adicionales que no están definidas en la función que hay en el objeto base. Recuerda que solo puedes
enmascarar una función que estés heredando, si la función existe en el objeto por un #include, entonces
se producirá un error por estar la función redefinida (la tienes escrita en el mismo archivo dos veces)
*/
void init()
{
	/*Como la función existe en el archivo base, y queremos que las cosas que hace allí siga haciendolas en
	nuestro objeto, tenemos que decirle que ejecute también esa función (la heredada). Si pusieramos únicamente
	init(); el driver entenderia que estás llamando a esta función y no a la heredada.
	Para ejecutar una función heredada existen dos formas de hacerlo:
	::init(); - Llama a la función init() heredada.
	room::init(); - Llama a la funciín init() heredada. En este caso estamos indicando el archivo base
		que heredamos en el que esta definida. Aquí no tiene importancia, pero en otros casos podemos
		estar heredando más de un objeto, y la función puede encontrarse en más de uno de ellos, con lo
		cual debemos especificar exactamente cual de las funciones heredadas queremos ejecutar (la de
		que objeto).*/
	::init();
	/*La función add_action añade a todos los objetos que hay dentro de este o en el entorno de este una acción.
	Más adelante me extendere más con ellas, de momento para no liarlo simplemente tienes que saber que
	el primer parametro es el nombre de la función que se llama al ejecutar la acción, y que el
	segundo es el comando que ha de introducir el jugador para activar dicha función.
	add_action es una función del driver, una de las más básicas, este tipo de funciones se llaman efuns*/
	add_action("funcion_info","info");
	add_action("editor","editor");
	add_action("comandos","comandos");
	/*Si quieres que más de un comando haga ejecutar una función, en vez de un string has de pasar
	un array de strings, al igual que haciamos con los add_item*/
	add_action("tocar_campana",({"tocar","golpear","mover"}));
	add_action("trampa",({"go","goto","goback","dest","home","ed"}));
}
/*Esta es la función que hemos puesto que debe ser llamada al usar el comando info
Delante de la función se pone el tipo de dato que devuelve con la sentencia return
Si quieres conocer más sobre los tipos de datos consulta el man o pregunta por la
página web del mud donde hay un pequeño manual de LPC básico.
En el caso de las funciones llamadas gracias a un add_action el tipo de dato
devuelto es un int, un 0 indica un fallo y un valor diferente (normalmente 1)
indica que la acción se realizó con exito.

El parámetro de la función es aquel texto que el jugador ponga detras del comando, en este
caso si el comando (a partir de ahora lo llamaremos verbo) es info, y el jugador escribe
info aqui, entonces el parámetro pasado a la función sera el string "aqui", por tanto en
ese caso tx seria "aqui" (tx=="aqui")*/
int funcion_info(string tx)
{
	/*tell_object(object receptor,string msj) es otra efun (funcion definida en el driver)
	Es otra de las funciones efuns importantes que tienes que conocer.
	Lo que hace es mostrarle por la pantalla al jugador que pasemos como parametro el mensaje
	que pongamos como segundo parametro de la funcion.

	this_player() es tal vez la efun más importante de todas.
	Lo que hace es devolver el objeto (jugador o npc) que está activo actualmente. Para ser
	más simple diré que es el jugador o npc que ha ejecutado la acción.

	Así pues la siguiente función lo que hace es mostrarle al jugador que ha realizado la
	acción el mensaje escrito como segundo parámetro.*/
	tell_object(this_player(),"Tu examen consistirá en crear 5 habitaciones distintas, una de "
		"las cuales se llamará workroom.c y sustituirás por la que tienes en tu directorio (esa room "
		"es a la que vas a parar cuando usas el comando home). Luego has de crear un "
		"npc como mínimo, capaz de hablar y luchar, y que esté equipado con almenos un arma y una "
		"armadura creadas por tí, y un objeto básico extraido de /baseobs/. En las rooms, en alguna de "
		"ellas, tendrás que poner un cartel, un objeto como un árbol, una fuente, una estatua o "
		"cualquier otra cosa que se te ocurra (es importante ser original).\n"
		"Para terminar tienes que crear una especie de mini-búsqueda, algo oculto en las rooms como un "
		"monstruo o una habitación secreta, cualquier cosa que se te ocurra que pueda ser divertida "
		"o interesante, y que se pueda descubrir explorando las diferentes salas.\n"
		"No queremos replicas de ningún tipo conocemos todos los examenes de los demás, por lo tanto "
		"cuidado con lo que haces. Todo lo que necesitas se encuentra en esta zona y puedes completar "
		"los conocimientos consultando el man, la web, la ayuda o los archivos existentes en /doc/. Si "
		"tienes alguna duda muy concreta puedes preguntar a los otros inmortales o por el canal cre, pero "
		"respeta su trabajo, así que si ves que están ocupados intenta no interrumpirles constantemente, o "
		"usa el comando mail para transmitirles tus necesidades.\nAtentamente,\n\n"
		+CREATOR+"\n");
	return 1;//Es el valor que devuelve la función... si es 0 quiere decir que fallo, 1 con éxito.
}
//Recuerda que el parámetro de la función es el texto adicional al verbo, por ejemplo, en 'tocar campana'
//'tocar' seria el verbo y 'campana' sería el string pasado como parámetro (tx=="campana")
int tocar_campana(string tx)
{
	//Si todavía no sabes que es un if, busca información en el man, la Web o algún manual de LPC o C/C++
	if(tx=="campana")
	{
		/*Ahora usaremos una nueva efun, también muy útil, llamada tell_room
		Esta función es similar a tell_object, pero se utiliza para mostrar el mensaje a todos
		los jugadores que tengan como entorno el objeto pasado como primer parámetro (por ejemplo
		si pasas como objeto una room, el mensaje lo verán todos los jugadores que estén en esa
		room en ese momento). La función es así:
		tell_room(object|string objeto_room,string mensaje,object|*object objetos_excluidos);
			-objeto_room: Es un string con la ruta de la room, o un objeto como el que devuelve
				clone_object o this_player() (Ten en cuenta que no es normal que un jugador este
				dentro de otro jugador, lo normal es que esté dentro de una room).
			-mensaje: Es el mensaje que ven todos los jugadores dentro de objeto_room.
			-objetos_excluidos: Es un objeto o un array de objetos que no deben ver el mensaje.
				Si no sabes que es un array todavía no te preocupes demasiado, de momento piensa
				que se puede excluir un grupo de objetos, si quieres saber que es un array
				visita la pagina Web o busca en el man la sección de estructuras.

		En este caso lo que queremos es mostrar un texto a todos los presentes para enseñar
		que el jugador esta realizando una acción, pero no queremos que el texto lo vea el jugador
		pues a el ya le salió el que le mandamos con el tell_object al principio de la función.

		La efun this_object() lo que devuelve es el objeto en el que es llamada, por tanto
		this_object() devuelve esta room (si la usaras en una esapada devolveria la espada esa).

		query_short() es una función con el nombre bonito del npc o jugador.
		*/
		tell_room(this_object(),this_player()->query_short()+" toca la campana.\n",this_player());
		tell_object(this_player(),"Tocas la campana.\n");
		//Ahora hacemos un ejemplo en el que el tell_room también ha de verlo el jugador que hace la accion
		tell_room(this_object(),"¡¡¡BOOOONGGG!!!\n");
		//La acción se completo con éxito, así que devolvemos un 1
		return 1;
	}
	else//else es la otra opcion del if, si lo del if no se cumple entonces se ejecuta lo que haya en el else.
	{
		/*Una efun muy importante en las acciones es notify_fail
		Su nomenclatura es void notify_fail(string msj,object objeto_avisado);
		- msj: Mensaje que ve objeto_avisado.
		- objeto_avisado: Quien está ejecutando la acción. Si este parámetro no se pone, el objeto
				avisado es this_player().

		notify_fail se usa cuando la acción ha sido fallida y vamos a realizar un return 0;
		Piensa que el jugador puede tener mas acciones que se llamen como esta (que tenga este mismo
		verbo) y por tanto alomejor el jugador está intentando hacer esa otra. Para manejar esto, la
		mudlib lo que hace es ir ejecutando todas las acciones que va encontrando hasta que una de ellas
		no devuelve 0, si todas devuelven 0 entonces sale el mensaje seleccionado con el notify_fail,
		que por defecto es 'La acción "nombre accion" no produjo ningún efecto.\n'. Con notify_fail
		podemos cambiar ese mensaje, con lo cual al personaje no le salen tell_room de acciones que no
		intenta hacer, ni tenemos que cortar el proceso de acciones metiendo un return 1;

		Si no lo has entendido de momento usa siempre return 0 y notify_fail cuando la acción falle y
		return 1 y tell_object cuando la acción surta efecto, y cuando te sientas preparado repasa
		este archivo.
		*/
		notify_fail("¿Qué quieres mover?\n",this_player());
		return 0;
		//Como notify_fail devuelve 0, una forma elegante de hacer el return seria return notify_fail("¿Qué quieres mover?\n",this_player());
	}
}
int editor(string tx)
{
	/*En esta acción aprenderemos los conceptos básicos de las propiedades:
	Las propiedades son una especie de recordatorios que se guardan en los jugadores o cualquier otro
	objeto. Hay tres tipos de propiedades:
		-static: Este tipo no se salva con el objeto, por tanto al salir del juego desaparecerá.
		-timed: Es una propiedad que se salva con el objeto, pero que desaparece cuando ha transcurrido
			un tiempo que le hayamos marcado.
		-old: Este es el tipo por defecto, es una propiedad que se salva con el jugador y que nunca
			desaparece. Este tipo has de procurar no usarlo nunca.
	Has de intentar usar la propiedad static siempre que puedas, si no te vale, entonces timed, y si esa
	tampoco te sirve, la property normal. Modo de empleo: (NOTA: objeto es un objeto como el que devuelve this_player())
		objeto->add_static_property("nombre_de_la_propiedad",Valor devuelto por la propiedad);
			Ej: this_player()->add_static_property("novato",1); (Esta propiedad desaparece al salir del juego)
		objeto->add_timed_property("nombre_de_la_propiedad",Valor devuelto por la propiedad,Tiempo en segundos/5);
			Ej: this_player()->add_timed_property("novato",1,2); (Esta propiedad duraria 2*5= 10 segundos)
		objeto->add_property("nombre_de_la_propiedad",Valor devuelto por la propiedad);
			Ej: this_player()->add_property("novato",1); (Esta propiedad queda guardada para siempre en el jugador, NO USAR)

	Para saber si un jugador/objeto tiene una propiedad tenemos las siguientes funciones:
		objeto->query_static_property("nombre");
		objeto->query_timed_property("nombre");
		objeto->query_old_property("nombre");
	Si nos da igual el tipo de propiedad y lo que queremos saber es simplemente si esta, usamos:
		objeto->query_property("nombre");
	Ten en cuenta que aquello que devuelve la función es el 'Valor devuelto por la propiedad'
	que fijamos al añadirle la propiedad.
	*/
	//Averiguamos si tenemos la propiedad
	if(this_player()->query_property("editor_dado"))
	{
		/*Ahora aprenderemos a clonar un objeto, la función necesaria para ello es clone_object("ruta.c");
		Esta función crea una copia en la memoria del objeto cuyo archivo es el indicado en la función.
		La copia es exactamente igual al objeto maestro, solo que tiene su propia copia de variables, y
		es totalmente independiente de la anterior.
		La función sería:
			clone_object(OBJ+"obj/papelito2");
		NOTA: OBJ es un #define de nuestro include path.h y que es: #define OBJ "/d/aprendizaje/obj/"
		El archivo de la copia es el mismo que el del objeto maestro pero con un # y un número al final.
		Eso es lo que obtenemos usando la efun file_name(objeto), pero por ahora lo importante que tienes
		que saber es que existen objetos clonados (los que son copia de uno maestro y cuyo archivo acaba
		con #número; son de este tipo todos los npcs, jugadores, armas y armaduras con los que interactuas
		durante el juego), el otro tipo de objetos son los maestros (los que ves en color lila cuando usas
		el comando 'ls', las rooms normalmente son de este tipo).

		Puede que ahora mismo te sientas un poco mareado, pero no te preocupes por ello.
		En algunos lugares veras la función add_clone, aunque sirven para cosas parecidas no son lo mismo
		y más adelante se explicará cuales son sus diferencias, muy importantes, y que no todos los
		creadores tienen en cuenta y ocasionan bugs por esa razón.

		Por último indicar que clone_object devuelve el objeto clonado, el mismo tipo de dato que devuelve
		this_player(), pero en este caso es el nuevo objeto que hemos creado.
		*/
		/*
		Piensa que al usar clone_object solo has creado un clon del objeto maestro, y para entendernos,
		de una forma abstracta esta en la 'nada' osea, no tiene entorno alguno; por tanto tienes
		que mover el objeto al entorno que tu deseas, en este caso a el jugador que realiza la
		funcion (this_player()). La función utilizada para ello es move(nuevo destino)
		Esa función está definida en cada objeto, por ello tienes que llamarla sobre el objeto clonado
		mediante una sentencia similar a esta objeto->move(destino);
		-> es una forma abreviada de usar la efun call_other, que sirve para llamar a una
		función definida en otro objeto que no es este.
		Observa como clonamos el objeto y aprovechando que clone_object devuelve el objeto,
		hacemos referencia a el mediante un call_other (->) para llamar a la función move y
		meter el objeto dentro del jugador que realiza la acción (this_player())
		*/
		clone_object(OBJ+"papelito2")->move(this_player());
		/*Si te has enterado perfectamente de este último paso ya tienes mucho ganado, pues
		has entendido como clonar objetos, como moverlos a un nuevo entorno y como llamar
		a una función definida en otro objeto.*/

		tell_object(this_player(),"¡Pero que has hecho con el anterior bestia!\nTe vuelve a aparecer un papel en las manos.\n");
		tell_room(this_object(),"Aparece de la nada un papel en las manos de "
			+this_player()->query_short()+".\n",this_player());
		return 1;//Devolvemos 1 porque la acción se realizó con éxito.
	}
	else//else es la otra opcion del if, si lo del if no se cumple entonces se ejecuta lo que haya en el else.
	{
		tell_object(this_player(),"Con cara de sorpresa contemplas como te aparece un papel extraño de la nada.\n");
		tell_room(this_object(),"Aparece de la nada un papel en las manos de "+this_player()->query_short()+".\n",this_player());
		this_player()->add_static_property("editor_dado",1);//Añadimos una propiedadd estatica
		clone_object(OBJ+"papelito2")->move(this_player());
		return 1;
	}
}
int comandos(string tx)
{
	if(this_player()->query_property("comandos_dados"))
	{
		tell_object(this_player(),"Siempre perdiéndolo todo leñe.\nUn papel se materializa en tus manos.\n");
		tell_room(this_object(),"Un papel se materializa en manos de "+this_player()->query_short()+".\n",this_player());
		clone_object(OBJ+"papelito")->move(this_player());
		return 1;
	}
	else
	{
		tell_object(this_player(),"Un papel se materializa en tus manos.\n");
		tell_room(this_object(),"Un papel se materializa en manos de "+this_player()->query_short()+".\n",this_player());
		clone_object(OBJ+"papelito")->move(this_player());
		this_player()->add_static_property("comandos_dados",1);
		return 1;
	}
}
int trampa(string tx)
{
	tell_object(this_player(),"No, saldrás cuando estés preparado para salir por tus propios medios.\n");
	return 1;
}