//Gholxien 06/2023: Campamento de bandidos

#include "base_campamento.h"

inherit "/std/room.c";

void setup() {
    add_exit(SE, CAMPBAN + "lider_2.c", "standard");
    set_light(60);
    set_short("Campamento de bandidos: Sala de emboscada");
    set_long("Esta pequeña y estratégica habitación, similar a un cubículo, se encuentra en la esquina noroeste de la tienda. Dispone de una abertura en la tela, ubicada a una altura adecuada para permitir disparos de proyectiles desde su interior.\n"
        "La abertura ha sido reforzada con parches de tela más gruesa para evitar cualquier rasgadura. En la parte trasera del cubículo, se encuentran cajas de madera que contienen una variada selección de proyectiles, como flechas, virotes y piedras para hondas, organizados de manera ordenada y fácilmente accesible.\n"
        "Las paredes de tela tensa están libres de adornos o decoraciones, enfocándose en la funcionalidad. La iluminación proviene de antorchas sujetas a soportes de metal fijados al suelo, brindando una luz tenue pero suficiente para mantener una buena visibilidad.\n");
}
