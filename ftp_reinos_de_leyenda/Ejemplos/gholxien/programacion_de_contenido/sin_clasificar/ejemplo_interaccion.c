// 2020.11.30 [Vhurkul]: Spooktober'20, A Fuego Lento

#include "../../include/path_anduar.h"

inherit "/obj/conversar_npc";

private void preparar_interacciones_fuego_lento();

void setup()
{
	set_name("sshukrok");
	set_short("Sshukrok");
	set_main_plural("Sshukroks");
    generar_alias_y_plurales();
    
    set_long("Un viejo chamán hombre-lagarto de ojos brillantes. Las escamas de su lomo han "
        "empezado a osificarse, y su cabeza, coronada por una brillante cresta rojiza, está "
        "cubierta de un extraño plumón blanquecino, pero su mirada aún es joven y está llena "
        "de curiosidad y sabiduría.\n"
        "Tiene una personalidad taciturna y pragmática, y pocas veces emplea más palabras "
        "que las estrictamente necesarias, aunque siempre se comporta con frialdad y "
        "aplomo.\n");
		
	fijar_raza("hombre-lagarto");
    fijar_clase("chaman");

    set_random_stats(12,17);
    fijar_nivel(15 + random(5));
	
    fijar_perfil_lenguajes("global");
    fijar_lenguaje_actual("lagarto");

    fijar_retardo_interacciones(4);
    preparar_interacciones_fuego_lento();
}

private nosave object jugador_dialogos = 0;
void capturar_jugador(object pj, string tema) { jugador_dialogos = pj; }

private void preparar_interacciones_fuego_lento()
{
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        "'Bienvenida, bella durmiente.",
        ":sonríe jovialmente.",
        "'¿Qué tal sienta ese cuerpo nuevo?",
        "'No nos ha salido precisamente barato... cuéntate bien, a ver si tienes todos los "
            "deditos.",
        (: tell_room(ETO, "Inesperadamente, Lésfora se abalanza sobre tí y aprieta uno de sus "
            "puñales contra tu garganta.\n") :),
        "'Más vale que hayas descubierto algo interesante, o te mandaré de vuelta al Limbo "
            "en un abrir y cerrar de ojos.",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", TO, ({
        (: do_say("Calma, calma, " +  jugador_dialogos->QCN + " ha sido " + 
            jugador_dialogos->dame_numeral() + " valios" + jugador_dialogos->dame_vocal() +
            " aliad" + jugador_dialogos->dame_vocal() + " hasta ahora, Lésfora.", 0) :),
        ":sujeta el brazo de su jefa y tira ligeramente de él, invitándola a apartarlo.",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        ":afloja su presa de mala gana.",
        "'Muy bien, veámos que información tienes para nosotros. Y esta vez no nos ocultes "
            "nada.",
        (: tell_room(ETO, "Cuentas la historia de Humo... las partes que entendiste, porque "
            "tampoco es que el gnomo fuera precisamente claro en sus explicaciones...\n") :),
        (: tell_room(ETO, "De hecho, agradeces haber estado muert" + 
            jugador_dialogos->dame_vocal() + ", porque de lo contrario seguro que ahora "
            "tendrías un impresionante dolor de cabeza.\n") :),
        ":te interrumpe bruscamente.",
        "'¿Sabes qué son esas semillas, Sshukrok?",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", TO, ({
        "'Sí. Los eruditos las llaman Ascuas de Verano.",
        ":se rasca la papada mientras observa las olas del Mar de Loereth.",
        "'Son como... cadáveres de elemental de fuego. Normalmente, cuando un elemental "
            "ve su vida en peligro, suele volver a su plano de origen.",
        "'Pero en algunas ocasiones, si la muerte les pilla por sorpresa, o si no tienen "
            "suficiente energía para el regreso, se encogen hasta quedar convertidos en "
            "unas bolitas pequeñas, del tamaño de perlas.",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        "'Entonces, ¿esta información no nos es nada útil?",
        ":aprieta de nuevo su puño en torno al mango del puñal.",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", TO, ({
        "'No, no. Sí que lo es.",
        "'Para empezar, normalmente son muy difíciles de conseguir, pero si eso de que "
            "las salamandras de la Corte de Verano las recolectan y atesoran es cierto... "
            "podríamos asegurarnos un suministro estable.",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        "'¿Para qué las reúnen, de todos modos?",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", TO, ({
        "'Según los libros de Olaphander, las Ascuas se abren si se las somete a temperaturas "
            "muy altas, y de ellas nace un nuevo elemental de fuego.",
        "'El maestro rúnico no dejaba claro si se trata del mismo elemental que murió, o de "
            "uno distinto...",
        "'Pero si es lo primero, quizás estén intentando encontrar las Ascuas del Rey Estival "
            "para poder resucitarle.",
        (: tell_room(ETO, "El viento cambia de dirección y trae el nauseabundo olor del "
            "pescado podrido de las lonjas, pero tanto Sshukrok como Lésfora parecen "
            "acostumbrados, pues no se inmutan.\n") :),
        "'Además esto de las flores es novedoso...",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        ":da un respingo y concentra toda su atención.",
        "'¿En qué sentido?",
        "'¿Son valiosas?",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", TO, ({
        "'No lo sé, nadie había descrito nunca algo así.",
        "'Parece como sí hubiese congelado las llamas del elemental recién nacido... Supongo "
            "que eso concuerda con otros usos conocidos del Khadul...",
        ":asiente.",
        "'Sí, creo que son valiosas. Un artesano experimentado podría construir poderosos "
            "objetos con ese nuevo material.",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        ":murmura con tono burlón:",
        "'¿Traficar con bebés elementales congelados? Sí, suena a algo que yo haría...",
        "'¿Crees que le mataron por eso? ¿Por congelar bebés y ahogarlos en agua?",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", TO, ({
        "'Es factible.",
        "'Debieron quemar la Ópera y descuartizarlo como escarmiento, o como advertencia para "
            "otros que quisieran imitarle.",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        ":vuelve a ponerte su puñal al cuello.",
        "'Si esto sale bien, puede ser muy gordo, Sshukrok.",
        "'Podríamos tener el monopolio de esas... Flores de Verano.",
        "'Por ahora solo conocemos el secreto nosotros tres...",
        (: $1->do_say("Confío en tí, pero deberíamos quitarnos a " + jugador_dialogos->QCN + 
            " de en medio.", 0) :),
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", TO, ({
        (: do_say("No merece la pena, Lésfora. Como ya te habrás fijado, nuestr" + 
            jugador_dialogos->dame_vocal() + " amig" + jugador_dialogos->dame_vocal() + 
            " no está por la labor de quedarse muert" + jugador_dialogos->dame_vocal() + ".", 0) :),
        (: do_say("Hagamos un pacto, " + jugador_dialogos->QCN + ". Tú te quedas los estuches "
            "de la Ópera, y nosotros los de las salamandras.", 0) :),
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        ":rueda los ojos con gesto de desesperación.",
        "'Por Khaol, Sshukrok... que blando eres.",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", TO, ({
        "'Estás mucho más susceptible de lo habitual, Lésfora...",
        "'¿Te ocurre algo? ¿A qué se debe ese ansia de sangre?",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        ":mira a Sshukrok, furiosa.",
        "'No me gusta venir al continente, especialmente a esta apestosa aldea de pescadores "
            "venida a más...",
        "'Tienen tanto de lo que nosotros carecemos... ¡Y no lo merecen!",
        ":frunce los ojos con odio.",
        "'Si yo mandase aquí...",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", TO, ({
        "'Nosotros tenemos lo más importante... Libertad.",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        "'Que topicazo más rancio, Sshukrok...",
        "'¿Y tú qué, lagarto? ¿Por qué tanto empeño en dejarle vivir?",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", TO, ({
        "'Yo... me pregunto cuales son los planes de los Ancestros...",
        "'¿Por qué nos ha desvelado Txolahini el complot de la Corte de Verano?",
        "'¿Quiere que les ayudemos a resucitar al Rey Estival? ¿Que lo impidamos?",
        (: do_say("Creo que " + jugador_dialogos->QCN + " jugará un papel crucial en esos "
            "planes, por eso deseo evitar su muerte.", 0) :),
        ":mira fijamente a Lésfora a los ojos, en silencio.",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        ":mantiene la mirada de Sshukrok durante unos segundos interminables, pero al final se "
            "rinde. Nadie puede ganar a un hombre-lagarto a ese juego.",
        ":aparta el puñal de mala gana y escupe al suelo.",
        (: $1->do_say("Si te dejo con vida, " + jugador_dialogos->QCN + ", es porque puedes "
            "servirme de conejillo de indias. Aún tenemos que comprobar el método para "
            "abrir los estuches.", 0) :),
        ":repite el procedimiento:",
        "'Hay que buscar los círculos de ceniza en los bosques.",
        "'Enterrar allí los Estuches, y esperar a que aparezcan los elementales a hacer su ritual.",
        "'Y cuando se hayan ido, desenterrar las Ascuas.",
        "'¿Algo más, Sshukrok?",
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", TO, ({
        "'Sí, hay que hacerlo a finales de la Estación de los Dones.",
        "'Y el bosque concreto cambia cada año.",
        ":se rasca la cabeza con su garra.",
        "'Bueno, claro, y los elementales descubrieron el truco de Humo, así que quizás "
            "ya no funcione...",
        "'En ese caso, puede que tengas que luchar con ellos para evitar que se lleven "
            "las Ascuas."
    }));
    nuevo_paso_interaccion("final_mision_fuego_lento", "lesfora", ({
        (: $1->do_say("Muy bien, entonces. Puedes marcharte, " + jugador_dialogos->QCN + ".", 0) :),
        "'Pero recuerda: si cuentas esto, lo sabré.",
        "'Si me robas mi parte, lo sabré.",
        ":se gira, dándote la espalda, mientras observa el puerto de Alandaen desde la barandilla.",
    }));
}
