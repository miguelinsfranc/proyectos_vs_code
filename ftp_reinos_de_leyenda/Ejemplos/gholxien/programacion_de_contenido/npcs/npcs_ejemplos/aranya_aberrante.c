//Inovercy 2007
// Modificado por Altaresh Feb19

inherit "/obj/monster.c";
#include "/d/eldor/path.h"

#define PROP_BLOQUEO_INV "Bloqueo_invocar_crias"
#define TIEMPO_BLOQUEO_INV 30
#define PROP_NUMERO_INVOCACIONES "numero_invocaciones"
#define NUMERO_INVOCACIONES 4 //  en realidad son 5 porque no estoy iniciando en =1
void setup()
{
	set_name("araña aberrante");
	set_short("Araña %^BOLD%^BLACK%^aberrante%^RESET%^");
	set_main_plural("Arañas %^BOLD%^BLACK%^aberrantes%^RESET%^");
	add_alias(({"araña", "aberrante"}));
	add_plural(({"arañas", "aberrantes"}));
	set_long("Te encuentras frente a una araña de grandes proporciones. Se trata de una aberración que solo ha podido ser creada a partir de oscuros "
		"rituales.\n"
		"Mide aproximadamente cuatro metros y su cuerpo se encuentra recubierto por un fino pelaje "
		"azabache de dura apariencia, tan duro que podría llegar a agujerearte si te acercaras lo suficiente. Sus rojos ojos "
		"parecen clavarse en ti mientras una serie de fluídos viscosos y pegajosos surgen de su mandíbula deteriorando el "
		"suelo mientras se va depositando en él. Sus grandes y fuertes patas no paran de moverse como si la gran araña estuviera buscando un punto débil "
		"para abalanzarse sobre ti. Te llaman la atención incontables esferas de gran tamaño en la parte inferior de su tórax.\n");

	fijar_raza("araña");
	fijar_nivel(70);
	adjust_tmp_damage_bon(150);
	add_res("acido", 100, 1);
	fijar_alineamiento(10000);
	fijar_genero(2);
	fijar_altura(400+random(2));
	fijar_peso(dame_altura()*1000);
	fijar_pvs_max(50000+random(10000));
	fijar_pe_max(100000);
	fijar_esferas( ([ "mayor": ({"evocacion"}) ]) );
	fijar_bo(320+random(100));
	fijar_be(100+random(80));
	fijar_bp(60+random(60));
	ajustar_carac_tmp("poder_magico",500);
	set_random_stats(15, 20);
	fijar_fue( 25);
	fijar_carac("con", 30);
	fijar_carac("int", 25);
	add_attack_spell(30, "proyectil magico mayor", 3);
	add_attack_spell(30, "zarpazo", 3);
	add_attack_spell(30, "tormenta acida", 4);
	//add_attack_spell(10, "telaraña", 3);
	fijar_estilo("/baseobs/estilos/razas/aracnido.c");

    set_aggressive(1,0);
	fijar_objetos_morir(({"tesoro",({"arma","armadura"}),({7,8}),2}));
	fijar_objetos_morir(({"tesoro","varios",({5,6}),1}));
}

void invocar_crias()
{	
	object ob;
	int i,j;
	int numero_invocaciones;
	numero_invocaciones=query_property(PROP_NUMERO_INVOCACIONES);
	j=random(2)+1;
	
	if (numero_invocaciones==NUMERO_INVOCACIONES) return;
	
	if (query_timed_property(PROP_BLOQUEO_INV)) return;
	
	tell_room(ENV(TO), "Del torax de la araña empiezan eclosionar huevos y aparecen de ellos "+j+" crias que se lanzan al combate.\n", "");
	for(i=0;i<j;i++)
	{
		ob=clone_object(NPCS"cria_aberrante.c");
		if (ob) ob->move(ENV(TO));
	}
	add_timed_property(PROP_BLOQUEO_INV,1,TIEMPO_BLOQUEO_INV);
	add_property(PROP_NUMERO_INVOCACIONES,numero_invocaciones+1);
	
}

int ajustar_pvs(int i, object causante){ 
	int r = ::ajustar_pvs(i,causante);
    
	__debug_x(sprintf("Daño: %d, Causante: %s, Vida actual: %d\n",intp(i)?i:to_int(i),causante?causante->query_name():"Nadie",dame_pvs()),"aranya_altaresh"); 
	
	if(dame_pvs()<=((dame_pvs_max()*90)/100) && //vida por debajo del 90%
	   dame_pvs()>=((dame_pvs_max()*20)/100) //vida por encima del 20%
		){
		//remove_attack_spell("tormenta acida");
		invocar_crias();
	}
return r;
}
void event_death(object muerto,object *atacantes,object asesino){
	//ECKOL: haz un if(!muerto !! !asesino) por si acaso.
	if(!muerto || !asesino)		return;
	
	__debug_x(sprintf(muerto->QCN+" muere a manos de "+asesino->QCN+" ayudado por "+query_multiple_short(atacantes)),"aranya_altaresh"); 
    _LOG(
        "estadisticas",
        "aberrante",
		muerto->QCN+" a muere a manos de "+asesino->QCN+" ayudado por "+query_multiple_short(atacantes)+".\n"
		);

}

