// Gholxien 20.03.2021
// Npcs de pruebas


inherit "/obj/monster";

void setup() {
	set_name("conejo");
     	set_short("Conejo");
     	add_alias("conejo");
     	set_gender(1);
     	set_main_plural("Conejos");
     	add_plural(({"conejos"}));
     	set_random_stats(8,12);
     	fijar_raza("roedor");
     	set_level(1+random(5));
     	set_long("Un conejo que corretea feliz por el bosque. Ocasionalmente se detiene para comer algún que otro brote de hierba, pero inmediatamente continúa con su interminable carrera. Sus largas orejas le advierten de depredadores cercanos.\n");
	load_chat(70,
     		({
		1, "'nyic nyic",
		1, ":mueve sus largos bigotes y ech a correr mientras da grandes saltos.",
		1, ":te mira mientras mueve sus bigotes, pensativo."
		}));
     	load_a_chat(50,
     		({
        	1, ":te mira con gran tristeza y decepción",
        	1, ":empieza a corretear mientras trata de esquivar tus ataques.",
     		}));
     	fijar_bando("mercenario");
     	fijar_religion("ateo");
     	fijar_ciudadania("vagabundo",0);
     	}