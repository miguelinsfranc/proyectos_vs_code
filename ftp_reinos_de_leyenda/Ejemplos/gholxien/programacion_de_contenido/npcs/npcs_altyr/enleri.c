// 2021 [Gholxien]: Ficha y conversaciones de Enleri

inherit "/obj/conversar_npc";

private void setup_conversacion();

void setup()
{
    set_name("enleri");
    generar_alias_y_plurales();
        set_short("Enleri");
    set_main_plural("imágenes de Enleri");

    set_long(
        "Lleva su cabello moreno, peinado en tres grandes trenzas que a diferencia del resto de las niñas, recoge en la nuca, atravesándolas con un colmillo de oso y dejando que cuelguen casi hasta alcanzar sus pies. En su ancha frente, tiene dibujado con color ocre, su totem de nacimiento, representado por dos líneas curvas que se entrelazan y van de una sien a la otra. Sus avellanados y vivaces ojos, poseen el brillo del conocimiento ancestral de su clan, contrastando claramente con su cuerpecito espigado y a todas luces aun infantil, aunque unas leves curvas, comienzan a tornearse en sus caderas. Recoge su túnica al estilo de las mujeres de la tribu, recogiendo y plegando la prenda en la cintura con una tira de cuero blando, procurándose así pliegues y bolsillos donde poder llevar la carga, que en su caso parece ser sobre todo, plantas, semillas y raíces. A la altura de la cadera, la joven lleva colgada una libre que ella misma deshuesó y evisceró, tratando después el cuero del animal para que fuera impermeable. La cabeza del animal se encuentra cosida al cuerpo con un tendón, dejando una apertura del tamaño de una mano, para poder introducir y sacar su contenido.\n"
    );

	fijar_clase("druida");
	fijar_fue(15);
	fijar_des(17);
	fijar_con(18);
	fijar_int(11);
	fijar_sab(20);
	fijar_car(11);
    fijar_genero(2);
    fijar_alineamiento(0);
    fijar_nivel(28);
    fijar_pvs_max(dame_pvs_max() * 2);
fijar_peso_corporal(60000);
fijar_altura(158);
fijar_raza("humano");
nuevo_lenguaje("dendrita",100);
nuevo_lenguaje("negra",100);
nuevo_lenguaje("adurn",100);
nuevo_lenguaje("lagarto",100);

add_clone("/baseobs/armaduras/tunica.c", 1);
add_clone("/baseobs/armaduras/cinturon.c", 1);
add_clone("/baseobs/armaduras/botas.c", 1);
init_equip();

load_chat(20, ({
        ":remueve con fuerza el contenido burbujeante de un caldero, mientras surge un vapor anaranjado.",
        "'Necesito urgentemente algunas plantas, pero Alug no deja que baje al bosque para recogerlas. ¡No entiende que son necesarias para bajar la fiebre! ¡Solo sabe preocuparse!",
        
    }));
setup_conversacion();
}

private void setup_conversacion()
{
nuevo_dialogo("marcas", ({
function(object pj) { tell_object(pj, "Señalas la frente de la niña mientras preguntas su significado.\n"); 
tell_room(ETO, pj->QCN + " señala los símbolos de la frente de la niña, mientras pregunta su significado con un gesto.\n", pj); },
":se lleva la mano a la frente y desliza delicadamente su dedo índice por las marcas, sin llegar a tocar la piel.",
"'Estas marcas son mi totem de nacimiento.",
"'Cuando un niño nacido en el clan alcanza el mes de vida, Broursag consulta a los espíritus ancestrales del oso pálido, sobre el totem que protegerá durante toda su vida a la criatura.",
"'Este será revelado al resto de la tribu en una ceremonia, donde Broursag dibujará por primera vez el totem en la frente de la criatura.",
"'A partir de entonces, será misión de los padres hacerlo, hasta que el niño o la niña pueda hacerlo por si mismo.",
}));

nuevo_dialogo("broursag", ({
"'Es el elegido por el espíritu superior del Oso Pálido para hacernos llegar sus designios.",
":comienza a remover con más ímpetu el contenido del caldero y un humo blanquísimo y espeso comienza a elevarse y arremolinarse formando formas y figuras según Enleri va desgranando su relato.",
"'Al principio estaba la Madre de todos, que nos protegía y cuidaba dándonos con generosidad todo lo que en la tierra estaba a nuestro alcance.",
"'Un día, con el transcurrir de los años, las tribus del clan se volvieron egoístas y no agradecían los bienes recibidos, así que la Madre retiró su abrazo protector y estuvimos desamparados hasta que una anciana del clan tuvo una visión en la que un espíritu sería enviado con aspecto de oso muy pálido, para protegernos.",
"'Ella fue la primera Broursag.",
(: tell_room(ETO, "El humo del caldero forma la figura de un oso aterrador que se alza en sus dos patas traseras.\n") :),
"'Este es el servidor del Superpredador, un espíritu superior que rige la muerte, la vida y la sabiduría del equilibrio con ambas y la naturaleza.",
(: tell_room(ETO, "El humo muestra ahora una suerte de garra que pareciera hecha de hojas enredadas.\n") :),
":sopla el humo que desaparece completamente sorprendiéndote.",
"'Cada tribu del clan tiene un Broursag que rinde cuentas al Gran Broursag del clan.",
"'Ellos nos guían y velan por nuestros espíritus.",
}), "marcas");

nuevo_dialogo("totem", ({
"'Se considera de mala educación entre los nuestros preguntar o mirar fijamente el totem de otro.",
":se encoge de hombros.",
"'Entiendo que sientas curiosidad, yo soy también muy curiosa, así que te explicaré algo más y te contaré que significa el mío.",
":sonríe.",
"'Los totems son espíritus de animales, plantas o elementos primarios.",
"'El mío en concreto simboliza las ondas del agua que fluye, ya que mi totem es el del río.",
"'Me lo pusieron por mi tenacidad, constancia y para simbolizar lo antiguo de mi linaje.",
}), "marcas"),

nuevo_dialogo("linaje", ({
	":remueve el contenido burbujeante del pequeño caldero.",
"'Mi linage se remonta a más de cincuenta generaciones de curanderas.",
"'Las mujeres de mi familia siempre han poseído el don de curar, y siempre han transmitido de madres a hijas todos los conocimientos necesarios para elaborar y preparar medicinas.",
"'Estamos rodeados de remedios, que si sabes como utilizarlos, pueden curar muchas dolencias.",
"'Pero me temo que mi preparación aun no es suficiente para curar a los enfermos del ataque.",
":mira apesadumbrada en dirección a la yurta de los enfermos.",
"'Espero que vengan pronto los refuerzos y que mi madre esté entre ellos.",
}), "totem");

nuevo_dialogo("enfermos", ({
":abre mucho los ojos mientras te explica.",
"'Los lobos rezumaban una baba de un tono verde que jamás había visto.",
"'Parecía refulgir como si una estrella brillara, pero en verde.",
"'Esa sustancia es la que les enferma y les produce la calentura, pero por más que pruebo remedios, no consigo bajarla ni expulsar los espíritus malignos del brillo verde.",
}), "linaje");

nuevo_dialogo("remedios", ({
":se mueve por su yurta, preocupada.",
"'No sé que voy a hacer con los enfermos.",
"'¡He probado con todo lo que tengo y nada surte efecto, nada!",
"'¡Soy una vergüenza para la antigua estirpe de curanderas de la cual provengo, he probado con todo y no consigo bajar la fiebre!",
"'Aunque espera... hay una cosa que aún no he probado, pero no dispongo de ella aquí.",
"'Se trata de una flor de pétalos blancos, con unas espinas rojizas.",
(: do_say("¿Crees que podrías traerme por ejemplo unas 10, " + $1->QCN + "?", 0) :),
"'Me parece haber oído a mi madre decir que son muy abundantes en el trecho de sendero que separan los bosques de Waret y Maragedom.",
"'Si consigues traerme por lo menos 10 de estas flores, mi gente y yo te estaremos eternamente agradecidos.",
"mision altyr_secundarias_plantas apuros_enleri",
}), "enfermos");

nuevo_dialogo("entregar", ({
(: do_say("¡Oh, " + $1->QCN + "!", 0) :),
"'Desde el primer momento en el que entraste aquí, supe que tú podrías ayudarme.",
"'Voy a contarle a Alug la buena noticia, ¡seguro que le encantará!",
"hito altyr_secundarias_plantas entregar_flores",
}));
prohibir("entregar", "hito altyr_secundarias_plantas obtencion_flores");
deshabilitar("entregar", "hito altyr_secundarias_plantas entregar_flores");

habilitar_conversacion();
}