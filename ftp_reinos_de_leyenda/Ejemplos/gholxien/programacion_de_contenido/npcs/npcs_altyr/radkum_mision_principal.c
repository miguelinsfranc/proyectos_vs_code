// 2021 [Gholxien]: Ficha y conversaciones de Radkum [mision altyr_principal]

inherit "/obj/conversar_npc";

private void setup_conversacion();

void setup()
{
    set_name("radkum");
    generar_alias_y_plurales();
        set_short("Radkum");
    set_main_plural("imágenes de Radkum");

    set_long(
        "Una fina pelusilla rubicunda comienza a mostrarse en las mejillas y mentón de este joven adolescente que con su mirada clára e inocente, observa cuanto le rodea con parsimonia e interés. Su cabello castaño claro, le cae suelto hasta los hombros. Por ahora no luce ningún adorno anudado entre los mechones o al final de estos, ya que aun no ha sido recibido en la tribu como hombre cazador. Cuando cobre su primera pieza, se realizarán los ritos de madurez que le darán la bienvenida al círculo de los hombres del clan y podrá mostrar a todos su estatus, mediante los trofeos de colmillos y garras con los que remate sus cabellos. En su alta frente, lleva dos círculos concéntricos en tonos ocre. Viste un manto de piel de oso, pero anudada al cuello en lugar de entre los omóplatos como hacen los cazadores y una pieza corta de cuero, anudada a la cintura.\n"
    );

	fijar_clase("druida");
	fijar_fue(16);
	fijar_des(18);
	fijar_con(18);
	fijar_int(10);
	fijar_sab(19);
	fijar_car(11);
    fijar_genero(1);
    fijar_alineamiento(0);
    fijar_nivel(32);
    fijar_pvs_max(dame_pvs_max() * 2);
fijar_peso_corporal(65000);
fijar_altura(160);
fijar_raza("humano");
nuevo_lenguaje("dendrita",100);
nuevo_lenguaje("negra",100);
nuevo_lenguaje("adurn",100);
nuevo_lenguaje("lagarto",100);

add_clone("/baseobs/armaduras/tunica.c", 1);
add_clone("/baseobs/armaduras/cinturon.c", 1);
add_clone("/baseobs/armaduras/botas.c", 1);
add_clone("/baseobs/armas/baston_a_dos_manos.c", 1);
init_equip();

setup_conversacion();
}

private void setup_conversacion()
{
nuevo_dialogo("nombre", ({
(: $1->do_say("Saludos, ¿eres Radkum?", 0) :),
"'Así es, soy Radkum del clan del Oso pálido. ¿Vienes de parte de Shihon?",
(: $1->do_say("Así es, me ha comentado que tenéis algunos problemas. Si me lo permitís, me gustaría ayudaros.", 0) :),
"'En estos tiempos que está pasando mi clan, cualquier ayuda es bienvenida.",
}));

nuevo_dialogo("ayuda", ({
(: do_say("Muy bien " + $1->QCN + ", sígueme en dirección a mi campamento. Si me pierdes el rastro, te estaré esperando de nuevo aquí mismo.", 0): ),
"hito altyr_principal conversacion_radkum_1",
}), "nombre");

habilitar_conversacion();
}