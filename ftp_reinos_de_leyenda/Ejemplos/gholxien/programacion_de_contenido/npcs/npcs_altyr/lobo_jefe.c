// 2021 [Gholxien]: Ficha de lobo jefe

inherit "/obj/monster";

void setup() {
    set_name("lobo");
    generar_alias_y_plurales();
    set_short("Gran lobo blanco");
set_main_plural("imágenes de Gran lobo blanco");
    set_long("Se trata de un lobo gigantesco, que mide mas de cinco pies y pesa alrededor de doscientos kilos. Su enorme corpulencia está tonificada por la fuerte masa muscular que se ven a simple vista. Está repleto de cicatrices antiguas, que se entrecruzan entre sí, formando una especie de laberinto. Te percatas de que solo tiene una oreja, que mantiene levantada en busca de algún peligro.\n");

            fijar_clase("aventurero");
            fijar_fue(19);
            fijar_des(20);
            fijar_con(22);
            fijar_int(3);
            fijar_sab(3);
            fijar_car(7);
            fijar_genero(1);
            fijar_alineamiento(0);
            fijar_nivel(30);
            fijar_raza("canino");
        fijar_bo(150);
        fijar_be(130);
        fijar_pvs_max(10000);
        fijar_carac("daño", 20);
        

        load_chat(60, ({
        ":gruñe mientras una saliva verduzca chorrea de entre sus colmillos.",
        ": aúlla hacia la entrada de la cueva.",
        ": te mira mientras contrae los labios, en un silencioso gruñido.",
        }));
        

fijar_objetos_morir(({"tesoro", "varios", ({1,2}), 1 }));
        fijar_objetos_morir("/d/dendra/armas/garra_lobo.c");
		}
