// 2021 [Gholxien]: Ficha y conversaciones de Radkum

inherit "/obj/conversar_npc";

private void setup_conversacion();

void setup() {
    set_name("radkum");
    generar_alias_y_plurales();
    set_short("Radkum");
    set_main_plural("imágenes de Radkum");

    set_long(
        "Una fina pelusilla rubicunda comienza a mostrarse en las mejillas y mentón de este joven adolescente que con su mirada clára e inocente, observa cuanto le rodea con parsimonia e interés. Su cabello castaño claro, le cae suelto hasta los hombros. Por ahora no luce ningún adorno anudado entre los mechones o al final de estos, ya que aun no ha sido recibido en la tribu como hombre cazador. Cuando cobre su primera pieza, se realizarán los ritos de madurez que le darán la bienvenida al círculo de los hombres del clan y podrá mostrar a todos su estatus, mediante los trofeos de colmillos y garras con los que remate sus cabellos. En su alta frente, lleva dos círculos concéntricos en tonos ocre. Viste un manto de piel de oso, pero anudada al cuello en lugar de entre los omóplatos como hacen los cazadores y una pieza corta de cuero, anudada a la cintura.\n");

    fijar_clase("druida");
    fijar_fue(16);
    fijar_des(18);
    fijar_con(18);
    fijar_int(10);
    fijar_sab(19);
    fijar_car(11);
    fijar_genero(1);
    fijar_alineamiento(0);
    fijar_nivel(32);
    fijar_pvs_max(dame_pvs_max() * 2);
    fijar_peso_corporal(65000);
    fijar_altura(160);
    fijar_raza("humano");
    nuevo_lenguaje("dendrita", 100);
    nuevo_lenguaje("negra", 100);
    nuevo_lenguaje("adurn", 100);
    nuevo_lenguaje("lagarto", 100);

    add_clone("/baseobs/armaduras/tunica.c", 1);
    add_clone("/baseobs/armaduras/cinturon.c", 1);
    add_clone("/baseobs/armaduras/botas.c", 1);
    add_clone("/baseobs/armas/baston_a_dos_manos.c", 1);
    init_equip();

    load_chat(20, ({
            ":agarra su cuchillo y comienza a tallar con mucho cuidado un trozo de madera.",
            "'Me gustaría que Alug deje de tratarme como a un niño.",

        }));
    setup_conversacion();
}

object * encuentra_piedras(object pj) {
    object * piedras = filter(all_inventory(pj), function (object ob) {
        return real_filename(ob) == "/baseobs/minerales/piedra";
    });
    if (sizeof(piedras) >= 10)
        return piedras[0..9];
	return 0; // TODO
}

private void setup_conversacion() {
    nuevo_dialogo("shihon", ({
            "'Sí, conocí a Shihon y él nos ayudó muchísimo.",
            "'La verdad es que hasta Alug tiene que admitir que es muy impresionante, con los brazos mutados y todo eso.",
            ":alza de manera grandilocuente los brazos con los ojos brillantes de admiración.",
            "'De mayor me encantaría unirme al círculo del simbionte y aprender a ser un druida.",
            "'De hecho ya le estoy preguntando cosas a Enleri sobre plantas, para que cuando llegue no me sienta un completo ignorante.",
            ":se pellizca el mentón y luego se lo frota distraídamente.",
            "'Lo difícil va a ser que mi hermana me ayude a entrenar, pues sé que los druidas no solo son sabios, sino también hábiles en el combate.",
            ":mira de reojo a su hermana.",
            "'No se lo digas, pero quiero que sea ella quien me entrene. Es muy buena luchando.",
        }));

    nuevo_dialogo("alug", ({
            "'Alug es mi hermana mayor.",
            "'Siempre tuvo muy mal genio, pero desde que cobró su primera pieza en una cacería, está insoportable.",
            ":mira a Alug que no le presta atención y le saca la lengua.",
            "'No te creas además que la mató ella.",
            ":niega socarronamente.",
            "'Únicamente le hizo la primera herida, y solo por eso va por ahí como un gallo de corral.",
            ":se pavonea estirando mucho el cuello y moviéndose con altanería.",
            (: tell_room(ETO, "Alug ve a su hermano y le pone la zancadilla, haciendo que este dé de bruces contra el suelo.\n"): ),
            ":escupe una bocanada de barro que ha tragado sin querer y mira a su hermana con fastidio.",
            "'¡Hueles peor que el culo de un orco!",
            (: tell_room(ETO, "Alug dice: ¡Pues tú eres más tonto que Tugh!"): ),
            ":mira a su hermana intentando contener la risa, pero al ver que esta hace lo mismo, ambos se sacan la lengua y comienzan a reir a carcajadas.",
        }), "shihon");

    nuevo_dialogo("horno", ({
            "'Vamos a tener un gran festín.",
            "'Bambo piensa que después de tantos sucesos malos nos vendrá bien tener las barrigas bien satisfechas, y es posible que tenga razón.",
            "'Está preparando un horno de piedra para cocinar, con ese montón de piedras que ha recogido.",
            "'Me da la sensación de que con esas piedras no va a ser suficiente para forrar todo ese agujero.",
            "'Bambo ha hecho un agujero bastante grande con la esperanza de meter un par de jabalíes, pero no ha calculado bien las piedras. Le faltan al menos diez.",
            function (object pj) {
                tell_object(pj, "Radkum te mira sopesándote y tras unos instantes decide.\n");
                tell_room(ETO, "Radkum sopesa con la mirada a " + pj->QCN + " y decide.\n", pj);
            },
            "'Tú podrías traer esas piedras. Son fáciles de encontrar, aunque necesitarás cavar en la orilla del lago para encontrarlas. Ten cuidado con los lobos que aún rondan por la zona.",
            (: do_say("¡¿Nos ayudarías con esta tarea, " + $1->QCN + "?", 0): ),
            "mision altyr_secundarias_piedras requerimiento_piedras",
        }));

    nuevo_dialogo("entregar", ({
            function (object pj) {
                encuentra_piedras(pj)->dest_me();
				tell_object(pj, "Apilas las piedras en el montón junto a las que ya había, mientras te limpias el sudor de la frente.\n");
                tell_room(ETO, pj->QCN + " apila las piedras en el montón junto a las que ya había, mientras se limpia el sudor de la frente.\n", pj);
            },
            function (object pj) {
                tell_object(pj, "Radkum te felicita efusivamente.\n");
                tell_room(ETO, "Radkum felicita efusivamente a " + pj->QCN + ".\n", pj);
            },
            (: do_say("¡Gracias amig" + $1->dame_vocal() + "! Has traído un buen montón de piedras. Ahora sí podremos terminar el horno.", 0) :),
            "hito altyr_secundarias_piedras entregar_piedras",
        		}));
    prohibir("entregar", "chequeo_entregar_piedras");
    deshabilitar("entregar", "hito altyr_secundarias_piedras entregar_piedras");

    habilitar_conversacion();
}

int chequeo_entregar_piedras(object pj) {
	return pj && pj->dame_hito_mision("altyr_secundarias_piedras", "requerimiento_piedras") && !pj->dame_hito_mision("altyr_secundarias_piedras", "entregar_piedras") && encuentra_piedras(pj);
}
