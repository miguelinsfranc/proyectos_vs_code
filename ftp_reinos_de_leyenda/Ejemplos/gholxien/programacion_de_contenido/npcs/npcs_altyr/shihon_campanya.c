// Satyr 27.06.2017
// Satyr 28.09.2017 -- cambio automático a ralder (AKA: dejad de rallar al consejo)
// 2021.03.06 [Vhurkul]: Limpieza de codigo viejo y pequeños cambios de diálogo

#include "/d/terra-nullius/path.h"
#include <baseobs_path.h>

inherit "/obj/pnjs/pnj-ia.c";
inherit "/obj/pnjs/conversador.c";

#define CANTIDAD 50

void setup() {
    set_name("shihon");
    set_short("Shihon, último defensor del Trono de Raíces");
    set_main_plural("imágenes de " + query_short());
    generar_alias_y_plurales();

    set_long(
        "Un elfo adusto cuyo rostro está surcado por las cicatrices y arrugas características "
        "de aquellos que han vivido más de una decena de batallas. Su pelo es una "
        "llamativa cresta anaranjada que huele a tierra mojada y tiene el brillo "
        "de la savia que la impregna. Dos trenzas, largas y muy cuidadas, recogen los "
        "pelos que no están en la cresta y los dejan caer sobre sus hombros.\n\n"

        "  Sus ojos son de un color azulado, salpicado con motas verdes brillantes que "
        "no parecen naturales y dan fe de su adhesión al credo de Naphra, diosa "
        "ya casi olvidada cuyos dominios de poder son la vida vegetal, el renacimiento"
        " y la dominación.\n\n"

        "  Sus brazos son aberrantes, pues uno de ellos es grueso, peludo y repleto de "
        "afiladas garras negras, mientras que el otro es quitinoso, segmentado y "
        "rematado por un aguijón que rezuma un veneno empalagoso que gotea "
        "sobre el suelo. Shihon es uno de los pocos que domina la "
        "técnica del polimorfismo parcial, lo que le permite convertir su cuerpo "
        "en una amalgama de extremidades animales con el fin de defender sus ideales.\n\n"

        "  Junto a algunos familiares y amigos, formó la última guardia del trono "
        "de Raíces, el lugar del plano material que usó su señora para esconderse "
        "cuando ya no tenía las energías suficientes para volver a su plano "
        "original, pero tras el despertar de Izgraull la cuadrilla se separó, puesto "
        "que muchos de ellos creían que la dama verde estaba a punto de morir.\n\n"

        "  Lejos de esto, Naphra se unió a Ralder en simbiosis, haciendo que el fuego "
        "interior de Shihon se avivase con el renacimiento de su dama, dándole "
        "la determinación necesaria para imponer la nueva fe de Ralder y Naphra "
        "en el mundo, sin importar quien se interponga en su camino.\n");

    fijar_religion("ralder");
    fijar_raza("elfo");
    fijar_clase("druida");

    fijar_genero(1);
    set_random_stats(18, 18);
    fijar_fue(24);
    fijar_extrema(100);
    fijar_nivel(80);

    fijar_pvs_max(80000);
    fijar_estilo("/d/terra-nullius/estilos/shihon.c");
    ajustar_carac("critico", 150);
    ajustar_bo(300);
    ajustar_be(300);
    fijar_maestrias((["Desarmado": 100]));

    add_hated("religion", ({
            "izgraull"
        }));
    set_aggressive(6, 25);
    set_move_after(30, 30);

    add_attack_spell(50, "zarpazo", 3);
    add_attack_spell(50, "destripar", 3);

    // TODO Equipo de druida
    add_clone(BSAGRADOS + "simbolo_ralder.c", 1);
    add_clone(BARMADURAS + "pendiente.c", 1);
    add_clone(BARMADURAS + "pantalones.c", 1);
    init_equip();

    nuevo_lenguaje("adurn", 100);
    nuevo_lenguaje("natural", 100);
    nuevo_lenguaje("negra", 100);
    nuevo_lenguaje("dendrita", 100);

    nuevo_dialogo("nombre", ({
            "'Soy Shihon, silvano de tribu ya olvidada y último defensor del trono de raíces.",
            "'Asumo que adoras al simbionte. Si necesitas un símbolo sagrado, házmelo saber.",
        }));

    nuevo_dialogo("simbionte", ({
            "'Los dioses Ralder y Naphra se unieron en enlace simbiótico.",
            "'Ya son muchos los creyentes que han empezado a dedicar su adoración a la nueva "
            "entidad: el Simbionte.",
            "'Aunque aún queda mucho trabajo por hacer, pues la mayoría de fieles persiste "
            "en las viejas creencias, y algunos de los que siguen los ideales de nuestro "
            "credo ni siquiera conocen su nombre.",
            "'Es nuestro deber enseñarles el nuevo camino. Algún día, todos los seguidores "
            "del Simbionte nos reuniremos en un nuevo Círculo.",
        }), "nombre");

    nuevo_dialogo("simbolo", ({
            "dar_simbolo"
        }));

    nuevo_dialogo("silvano", ({
            "'Mis orígenes son irrelevantes. Hace mucho tiempo que no soy esa persona.",
            "'Dejé atrás esa vida al dedicarme a Naphra en cuerpo y alma.",
        }), "nombre");

    nuevo_dialogo("naphra", ({
            "'La Dama Verde es la diosa de la vida vegetal, el renacimiento y la transformación.",
            "'El culto élfico terminó dándole la espalda tras adorar a Eralie, por lo que la falta "
            "de fe hizo que su poder menguase, quedándose atrapada en el primer plano material.",
            "'Solo unos pocos elfos, entre los que me incluyo, se decidió a defenderla hasta que "
            "reuniese el poder necesario para volver al panteón de los dioses, donde podría "
            "recuperarse.",
            ":parece taciturno.",
            "'Las cosas no salieron como pensábamos.",
        }), "silvano");

    nuevo_dialogo("cosas", ({
            "'La reina se ocultó bajo el suelo de Zylwynnör No Wareth. Creamos allí un círculo "
            "druídico y nos dedicamos a protegerla.",
            "'Sin embargo, el Imperio de Dendra se expandió hacia el oeste y junto a los Golthur-Hai "
            "pronto empezaron a talar el bosque, ralentizando la curación de Naphra.",
            "'Tuvimos que intervenir, defender el bosque. Pero no eramos suficientes para defenderlo "
            "y pronto empezamos a perder miembros.",
            "'Cuando solo quedábamos un puñado, Ralder nos salvó, a todos, incluyendo a mi padre "
            "y a mi cónyuge.",
            "'Pero ellos ya habían perdido la esperanza, y decidieron darle la espalda y unirse "
            "al círculo de Izgraull.",
            ":masculla una maldición.",
        }), "naphra");

    nuevo_dialogo("izgraull", ({
            "'Un dios de la vieja era que otrora fundó un círculo druídico en el continente de "
            "Naggrung.",
            "'El círculo encontró su final cuando se cristalizó el bosque. Sin embargo, el "
            "segundo despertar de Izgraull lo trajo de vuelta.",
            "'Mis aliados me abandonaron en aras de defender lo que consideraban 'una causa que "
            "tiene futuro'.",
            "'Terminaré con su osadía, sin importar el cariño que les profesara en el pasado.",
        }), "cosas");

    nuevo_dialogo("mision", ({
            (: do_say("Que la bendición del simbionte caiga sobre ti, " + $1->QCN + ".", 0): ),
            "'En efecto, he convocado a los seguidores del simbionte para ayudar a un clan en apuros y liberar a unos lobos de cierta sustancia maligna que los ha corrompido.",
            "'En fin, no entraré en detalles, busca a Radkum por la zona occidental del bosque y acompáñale a su campamento, él te contará todo.",
            "'Pero por lo que él me ha contado, sé mas o menos los detalles. Lo que requiero de ti, es que me traigas una muestra de la sangre de los enfermos, otra de la sangre de los lobos, y otra muestra del agua que contiene la poza plateada.",
            (: do_say("¿Crees que estas a la altura de esta misión, " + $1->QCN + "?", 0): ),
            "mision altyr_principal conversacion_shihon_1",
        }));

    habilitar_conversacion();
}

int dar_simbolo(object pj) {
    if (!pj->dame_bloqueo_combate(TO)) {
        do_say("Aquí tienes, " + pj->dame_nombre_completo() + ".", 0);
        pj->entregar_objeto(BSAGRADOS + "simbolo_ralder.c");
        pj->nuevo_bloqueo_combate(TO, 43200);
        return 1;
    }

    if (!pj->query_timed_property("comprar-simbolo-shihon")) {
        do_say(
            "Hace poco que te entregué un símbolo sagrado. He de reservar mis "
            "existencias para el resto de fieles.",
            0);

        do_say(
            "Si realmente necesitas uno, vuelve a pedírmelo y te cobraré algo "
            "de dinero por él. Concretamente, " + CANTIDAD + " platinos.",
            0);

        do_say(
            "No lo haré por codicia, pues no soy materialista, pero nuestros recursos "
            "son finitos y deben ser repartidos con imparcialidad. Si vas a tomar "
            "más de los que te corresponden, es justo que aportes una compensación "
            "para costear tales gastos.",
            0);

        pj->add_timed_property("comprar-simbolo-shihon", 1, 360);
        return 1;
    }

    if (!pj->pagar_dinero(CANTIDAD, "platino")) {
        do_say(
            "Si no tienes dinero tendrás que esperar. Los símbolos de Ralder "
            "son finitos y tengo que repartirlos o cobrarlos.",
            0);
        return 1;
    }

    do_say("Aquí tienes, " + pj->dame_nombre_completo() + ".", 0);
    pj->entregar_objeto(BSAGRADOS + "simbolo_ralder.c");
    pj->nuevo_bloqueo_combate(TO, 43200);
    pj->remove_timed_property("comprar-simbolo-shihon");
    return 1;
}

int chequeo_inicial_cv(object jugador) {
    if (-1 == member_array(jugador->dame_religion(), ({
                "ralder",
                "naphra"
            }))) {
        do_say(
            "No tengo nada que decir a los tuyos, " + jugador->dame_nombre_completo() + ".",
            0);
        return 0;
    }

    return 1;
}
