// 2021 [Gholxien]: Ficha y conversaciones de Bambo

#include "../../../include/zona_cyr.h"
inherit "/obj/conversar_npc";

private void setup_conversacion();

void setup() {
    set_name("bambo");
    generar_alias_y_plurales();
    set_short("Bambo");
    set_main_plural("imágenes de Bambo");

    set_long(
        "Por lo que puedes observar del dibujo ocre de la frente de este adolescente, su totem de nacimiento es un jabalí, y realmente no podría ser más adecuado. Todo en él te recuerda a este animal, desde sus piernas algo más cortas de lo normal, hasta su prominente corpulencia. Posee un grueso cuello de doble papada que sustenta una ancha cabeza de nariz aplastada. Su labio superior está ligeramente retraído hacia arriba, por lo que parece estar oliendo algo permanentemente. Su frente es algo abombada y la cantidad de líneas de su totem, así como unas anchas y pobladas cejas, hace parecer que tuviera una frente más estrecha de lo normal. Tiene el pelo muy corto, en mechones que por su fosquedad y color, asemejan a las cerdas del jabalí. Sin embargo, sus pequeños ojillos, reflejan bondad y alegría incluso aunque no esté sonriendo. Viste una capa de piel de oso blanco anudada bajo sus papadas y una pieza de cuero anudada alrededor de su prominente cintura que le llega hasta las rodillas.\n");

    fijar_clase("barbaro");
    fijar_fue(20);
    fijar_des(9);
    fijar_con(19);
    fijar_int(6);
    fijar_sab(9);
    fijar_car(12);
    fijar_genero(1);
    fijar_alineamiento(0);
    fijar_nivel(29);
    fijar_pvs_max(dame_pvs_max() * 2);
    fijar_peso_corporal(75000);
    fijar_altura(160);
    fijar_raza("humano");
    nuevo_lenguaje("dendrita", 100);
    nuevo_lenguaje("negra", 100);
    nuevo_lenguaje("adurn", 100);
    nuevo_lenguaje("lagarto", 100);

    add_clone(BARMADURAS "tunica", 1);
    add_clone(BARMADURAS "cinturon", 1);
    add_clone(BARMADURAS "botas", 1);
    init_equip();

    load_chat(20, ({
            ":se relame los labios mientras contempla un trozo de carne jugosa cocinándose sobre el fuego.",
            "'¡Qué ganas tengo de probar una suculenta tajada de la carne de un jabalí!",
        }));
    setup_conversacion();
}

private void setup_conversacion() {
    nuevo_dialogo("trabajo", ({
            ":señala con uno de sus rollizos dedos el caldero y a continuación el hoyo recubierto de piedras, mientras explica.",
            "'Estoy preparando un horno para un gran festín.",
            "'Una vez recubra todo el interior de piedras calientes, pondré las piezas y cubriré todo con esas ramas con hojas grandes y lo taparé con arena.",
            "'No va a ser el festín que yo esperaba, pues Alug no ha sido capaz de traer una pieza grande con la que deleitarnos.",
            function (object pj) {
                tell_object(pj, "Bambo se tapa la boca temblando de miedo mientras te mira con temor.\n");
                tell_room(ETO, "Bambo se tapa la boca temblando de miedo mientras mira a " + pj->QCN + "con temor.\n", pj);
            },
            "'Oh, por favor, por favor, ¡dime que Alug no me ha oído decir eso!",
            function (object pj) {
                tell_object(pj, "Bambo se relaja poco a poco mientras se da cuenta que nadie le ha oído, y te dedica una sonrisa cómplice.\n");
                tell_room(ETO, "Bambo se relaja poco a poco mientras se da cuenta que nadie le ha oído, y dedica una sonrisa cómplice a " + pj->QCN + ".\n", pj);
            },
            "'como iba diciendo, tendrá que bastar.",
            "'Seguramente Enleri podrá darme algunas bayas comestibles con las que aderezar los platos y así supliré la carencia.",
            ":se pasa nerviosamente la mano por la nuca de manera compulsiva mientras pone ojos soñadores.",
            "'Aunque me muero por saborear un delicioso jabalí.",
            ":se relame solo de pensarlo.",
        }));

    nuevo_dialogo("aderezar", ({
            "'Si, rellenaré algunas de las aves de frutos secos que he ido recogiendo por los alrededores y de las bayas que pueda darme Enleri, y estará todo delicioso.",
            "'Mi abuela me enseñó a cocinar y no se me da nada mal.",
            ":hincha con orgullo el pecho mientras emite un gorjeo gutural con la garganta, que hace que vibre su doble papada.",
        }), "trabajo");

    nuevo_dialogo("jabali", ({
            ":saca con unas grandes pinzas hechas con lo que parece ser un fémur de algún animal de tamaño medio "
            "una gran piedra humeante que amontona en una piel y después acarrea hasta el hoyo para seguir forrándolo.",
            "'Estaría muy bien si alguien pudiera cazar un par de esos deliciosos animales.",
            ":se frota con deleite su enorme barriga mientras se relame.",
            "'Si, con dos especímenes grandecitos será suficiente.",
            "'¿Tú serías capaz de cazar un par de jabalíes?",
            "mision altyr_secundarias_jabali requerimento_jabali",
        }), "trabajo");

    nuevo_dialogo("entregar", ({
            function (object pj) {
                tell_object(pj, "Dejas con cuidado ambos cuerpos a los pies de Bambo que lagriméa de felicidad y saca raudo un afilado cuchillo con el que se dispone a preparar las piezas.\n");
                tell_room(ETO, pj->QCN + "Deja con cuidado ambos cuerpos a los pies de Bambo que lagriméa de felicidad y saca raudo un afilado cuchillo con el que se dispone a preparar las piezas.\n", pj);
            },
            "'Todos se alegrarán con la tripa llena de este rico festín!",
            "' Sobre todo Alug... ella no se ha quejado una sola vez, pero sus tripas "
            "llevaban rugiendo de hambre varios días "
            "para que a los demás no nos faltase alimento.",
            "hito altyr_secundarias_jabali entregar_jabali",
        }));
    prohibir("entregar", "hito altyr_secundarias_jabali obtencion_jabali");
    deshabilitar("entregar", "hito altyr_secundarias_jabali entregar_jabali");

    habilitar_conversacion();
}
