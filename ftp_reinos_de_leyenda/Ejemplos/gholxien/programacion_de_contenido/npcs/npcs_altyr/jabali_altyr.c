#include <baseobs_path.h>
inherit BANIMALES + "jabali.c";

int do_death(object asesino) {
    int contador;
    if (!asesino) {
        return ::do_death(asesino);
    }

    if (!asesino->dame_mision_abierta("altyr_secundarias_jabali") || asesino->dame_hito_mision_actual("altyr_secundarias_jabali", "obtencion_jabali")) {
        return ::do_death(asesino);
    }

    tell_accion(ENV(asesino), asesino->QCN + " recoge del suelo el cuerpo del jabalí que acaba de matar y se lo echa a los hombros, con esfuerzo.\n", "", ({
            asesino
        }), asesino);
    contador = asesino->dame_propiedad_virtual("contador_jabalis_bambo");
    contador++;
    asesino->nueva_propiedad_virtual("contador_jabalis_bambo", contador);
    if (contador >= 2) {
        tell_object(asesino, "Recoges el segundo jabalí y te lo echas al hombro junto con el otro cuerpo, e intentas volver con dificultad con Bambo, para entregarle los jabalíes.\n");
        asesino->nuevo_hito_mision("altyr_secundarias_jabali", "obtencion_jabali", 0);
        asesino->borrar_propiedad_virtual("contador_jabalis_bambo");
    } else
        tell_object(asesino, "Recoges el cuerpo del jabalí con mucho esfuerzo y te lo echas a los hombros.\n");
    return ::do_death(asesino);
}
