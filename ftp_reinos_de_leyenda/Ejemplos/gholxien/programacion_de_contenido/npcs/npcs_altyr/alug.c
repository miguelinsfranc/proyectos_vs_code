// 2021 [Gholxien]: Ficha y conversaciones de Alug

#include <baseobs_path.h>
#include <combate.h>
#include <depuracion.h>

inherit "/obj/conversar_npc";
inherit "/baseobs/snippets/sparring";

private void setup_conversacion();
private void setup_interaccion_radkum();
private void setup_conversacion_sparring();
private void setup_conversacion_antes_confianza();
private void setup_conversacion_despues_confianza();


void setup()
{
    set_name("alug");
    generar_alias_y_plurales();
    set_short("Alug");
    set_main_plural("imágenes de Alug");

    set_long(
        "Las redondeces del cuerpo fibroso de esta joven, son suaves y delicadas, como si acabaran "
        "de despertar al mundo, así como un capullo se despereza lentamente ante el primer rayo de "
        "sol. Pero la delicadeza de su fisonomía, se traiciona con la ferocidad y determinación de "
        "sus ojos claros, que parecen apuñalar el alma de aquel al que mira.\n"
        "Lleva el oscuro cabello cortado a trasquilones a la altura de unos lóbulos que decora con "
        "varias perforaciones, en las que luce garras y colmillos de oso. Al igual que el resto de "
        "la tribu, en su frente despejada se puede apreciar su totem de nacimiento, representado "
        "por tres líneas ocres, que ascienden desde el ceño de la joven y se pierden en la raíz "
        "del pelo. Este dibujo, junto con sus labios con tendencia natural a estar fruncidos y su "
        "forma inquisitiva de arquear las cejas, le confieren un gesto duro y desconfiado.\n"
        "Viste con la túnica tradicional de las mujeres, aunque la recoge en la cintura de tal "
        "modo que no deja lugar a muchos pliegues para guardar carga, pero hace que la prenda le "
        "estorbe menos a la hora de moverse y dar grandes zancadas. También lleva un corto manto "
        "de piel de oso blanco cruzado en el pecho, al estilo de los cazadores del clan.\n");

    fijar_clase("cazador");
    fijar_fue(17);
    fijar_des(19);
    fijar_con(18);
    fijar_int(10);
    fijar_sab(10);
    fijar_car(15);
    fijar_genero(2);
    fijar_alineamiento(0);
    fijar_nivel(35);
    fijar_pvs_max(dame_pvs_max() * 2);
    fijar_peso_corporal(63000);
    fijar_altura(165);
    fijar_raza("humano");
    nuevo_lenguaje("dendrita", 100);
    nuevo_lenguaje("negra", 100);
    nuevo_lenguaje("adurn", 100);
    nuevo_lenguaje("lagarto", 100);

    add_clone(BARMAS "lanza_madera", 1);
    add_clone(BARMADURAS "tunica", 1);
    add_clone(BARMADURAS "cinturon", 1);
    add_clone(BARMADURAS "botas", 1);
    add_clone(BARMADURAS "anillo", 1);
    add_clone(BARMADURAS "collar", 1);
    add_clone(BARMADURAS "pendiente", 1);
    add_clone(BARMADURAS "manto", 1);
	init_equip();

    load_chat(20, ({
        ":mira en todas direcciones y frunce el ceño.",
        ":murmura en voz baja mientras mira con preocupación en dirección al bosque.",
    }));
    
    setup_conversacion();
    setup_interaccion_radkum();
    setup_conversacion_sparring();
setup_conversacion_antes_confianza();    
setup_conversacion_despues_confianza();
	habilitar_conversacion();
}

private void setup_conversacion()
{
    nuevo_dialogo("nombre", ({
        "|Alug se tensa y apunta con su lanza a ${a:qcn}."
        "|"
        "|Alug se tensa y te apunta con su lanza.",
        "'¿Quién va?",
        "'Yo soy Alug, la líder de este asentamiento del Clan del Oso Pálido.",
    }));

    nuevo_dialogo("asentamiento", ({
        "'Este es uno de nuestros asentamientos en la época estival.",
        "'Todos los años se reúnen aquí jóvenes y adultos, para que los más pequeños aprendan "
            "costumbres sobre el clan, técnicas de supervivencia y a cazar en caso de estar en "
            "edad de ello.",
    }), "nombre");

    nuevo_dialogo("clan", ({
        "'Somos del Clan del Oso Pálido.",
        "'Somos muchos más de los que ves aquí, por supuesto.",
        ":hace un gesto abarcando con el brazo todo el asentamiento.",
        "'Los refuerzos están a punto de llegar, pero hasta que lo hagan, yo soy la líder aquí.",
        ":subraya la afirmación con un golpe de su lanza en la tierra.",
        "'Así lo ordenó Nhagte después del ataque de los lobos.",
    }), "asentamiento");

    nuevo_dialogo("lobos", ({
        "'Siempre nos habíamos respetado...",
        ":frunce el ceño preocupada.",
        "'Hay una larga tradición tanto en mi clan como en esa manada de lobos por compartir "
            "la zona.",
        "'El gran espíritu del Oso Pálido nos acompaña siempre y eso es algo que los animales "
            "perciben y respetan, pero cuando llegamos a la poza este año, los lobos habían "
            "cambiado.",
        ":frunce el ceño mientras recuerda la escena.",
        "'Al principio todos pensamos que se trataba de una nueva manada, y eso nos desconcertó "
            "por unos instantes.",
        "&baja el volumen de su voz para decir&Instantes que nos costaron caros.",
        "&murmura&Quizá si hubiéramos reaccionado más rápido, se habrían salvado más vidas.",
        "|Alug se limpia con rapidez una lágrima que se le escapaba por el rabillo del ojo, y mira "
            "desafiante a ${a:qcn}, para ver si se atreve a juzgarla débil."
        "|"
        "|Alug se deshace rápidamente de una lágrima que se le escapaba por el rabillo del ojo y "
            "te mira desafiante, como para ver si te atreves a juzgarla débil."
    }), "clan");

    nuevo_dialogo("vidas", ({
        "'En el ataque murieron dos valientes mujeres, Lylain y Celien, y una niña: Zarendine.",
        "&traga saliva y dice con resignación&Radkum insiste en que Zarendine podría seguir viva, "
            "pero yo he perdido ya la esperanza.",
        "'Más tarde perdimos a un valiente guerrero por sus graves heridas, y aún ahora tenemos a "
            "un niño y otra mujer luchando entre la vida y la muerte.",
        "|Alug ladea ligeramente la cabeza como escuchando los gemidos de los enfermos del "
            "interior de la tienda y escudriña desconfiada a ${a:qcn}"
        "|"
        "|Alug ladea ligeramente la cabeza como escuchando los gemidos de los enfermos del "
            "interior de la tienda y escudriña tu rostro, desconfiada.",
        "|Alug salta frente a ${a:qcn}, haciendo que dé un par de pasos hacia atrás, "
            "trastabillando."
        "|"
        "|Alug salta frente a ti, haciéndote retroceder un par de pasos.",
        "|Alug coloca la lanza de modo amenazador ante ${a:qcn}."
        "|"
        "|Alug coloca la lanza de modo amenazador ante ti.",
        "'¡Ni se te ocurra acercarte!",
    }), "lobos");

    nuevo_dialogo("cazar", ({
        "'Vine aquí para continuar mi entrenamiento como cazadora. Es mi mayor aspiración.",
        "'En nuestro clan, no suele haber mujeres cazadoras, pero no hay normas que lo prohíban, "
            "ni mucho menos.",
        "'Muchas mujeres simplemente prefieren ocuparse de las tareas de recolección, tallado o "
            "preparación de prendas para arroparnos.",
        "'La verdad es que las mujeres del Oso Pálido, son maravillosas artesanas que son capaces "
            "tanto de hacerte una lanza muy afilada como una cuchara de madera.",
        "|Alug mira a ${a:qcn}, retándo${a:dame_le} a que la desmienta."
        "|"
        "|Alug te mira como retándote a que la desmientas."
    }), "asentamiento");
}

private void setup_interaccion_radkum()
{
    // esto es opcional, pero hace el intercambio más rápido.
    // fijar_retardo_interacciones(2);

    //Alug (to) empieza con la interacción.
    nuevo_paso_interaccion("cv_radkum", TO, ({
        "'¡Radkum, maldito zoquete! ¿Cómo te atreves a salir por ahí sin decírmelo?",
        function (object npc)
        {
            object pj = dame_propiedad_interaccion("iniciador");
            tell_object(pj, "Alug se da cuenta de tu presencia y entrecierra los ojos.\n");
            tell_room(ETO, "Alug se da cuenta de la presencia de " + pj->QCN + " y entrecierra "
                "los ojos\n", ({pj}));
        },
        "'Y encima, ¿traes forasteros? ¿Acaso no te enseñó padre a desconfiar de los desconocidos?",
    }));

    //radkum efectuará el segundo paso y dirá una frase
    nuevo_paso_interaccion("cv_radkum", "radkum", ({
        function (object npc)
        {
            object pj = dame_propiedad_interaccion("iniciador");
            npc->do_say("Mi querida hermana, ¿podrías tranquilizarte? Est" + pj->dame_ae() + 
                " extranjer" + pj->dame_vocal() + " viene en paz para ayudarnos. No quiere causar "
                "ningún perjuicio ni al campamento ni a nosotros.", 0);
        }
    }));

    // En el tercer paso Alug (to) toma de nuevo el control de la interacción y dirá otra frase
    nuevo_paso_interaccion("cv_radkum", TO, ({
        "'¿Que me tranquilice? ¡Que me tranquilice!",
        "'¿Tú sabes todas las escenas que me han venido a la cabeza?",
        "'¿Sabes cuantas horas llevo imaginándome tu cuerpo tirado en el bosque, desgarrado por "
            "los lobos?",
        ":aprieta la mandíbula, con su rostro contraído de dolor.",
        "&masculla mientras las lágrimas ruedan por sus mejillas&¿Te lo puedes imaginar?",
    }));

    // En el 4º paso ambos personajes se abrazan y finaliza la discusión.
    nuevo_paso_interaccion("cv_radkum", "radkum", ({
        ":mira a su hermana con una emoción patente, y ambos hermanos se funden en un fuerte "
            "abrazo fraternal."
    }));

    // En el quinto paso, Alug interpela al jugador.
    nuevo_paso_interaccion("cv_radkum", TO, ({
        ":aparta a su hermano tras unos instantes y agarra su arma con decisión.",
        function (object npc)
        {
            object pj = dame_propiedad_interaccion("iniciador");
            npc->do_say("En cuanto a tí, foraster" + pj->dame_vocal() + ", puedes volver por "
                "donde has venido. No necesitamos la ayuda de nadie, nuestro clan pronto vendrá "
                "a rescatarnos de este maldito lugar.",
                "señala amenazadoramente mientras dice");
        },
    }));

    // En el sexto paso, Radkum intercede ante su hermana en favor del jugador, con dos frases.
    nuevo_paso_interaccion("cv_radkum", "radkum", ({
        function (object npc)
        {
            object pj = dame_propiedad_interaccion("iniciador");
            tell_object(pj, "Radkum percibe la creciente hostilidad de Alug y se interpone entre "
                "vosotros.\n");
            tell_room(ETO, "Radkum percibe la creciente hostilidad de Alug y se interpone entre "
                "ella y " + pj->QCN + ".\n", ({pj}));
        },
        "'Hermana, no te enfurezcas. Dejemos que investigue por el bosque y la poza.",
        "'Ha venido a ayudarnos y quizá también pueda ser útil para algunas de las cosas que "
            "necesitamos en el asentamiento."
    }));

    // En el último paso, Alug acepta la proposición de Radkum, y se obtiene el hito correspondiente.
    nuevo_paso_interaccion("cv_radkum", TO, ({
        ":frunce el ceño, pensativa.",
        "'No sé, Radkum, por ahora solo ha llegado hasta aquí porque tú le has guiado.",
        "'Cuando haga algo realmente valeroso, me plantearé si es de confianza.",
        function (object npc)
        {
            object pj = dame_propiedad_interaccion("iniciador");
            pj->nuevo_hito_mision("altyr_principal", "acompañamiento_radkum");
        }
    }));
}

private void setup_conversacion_sparring()
{
    string _mision = "altyr_secundarias_sparring_alug";

    nuevo_dialogo("muñeco",
        ({
            ":se seca el sudor de la frente tras darle un puñetazo a su muñeco de entrenamiento.",
            "|Alug mira a ${a:qcn} de forma desafiante."
            "|"
            "|Alug te mira de forma desafiante.",
            "'¿Qué quieres ahora, foraster${a:v}?",
            "'¿Acaso no ves que estoy entrenando?",
            "'Tengo que luchar bien para poder defender el asentamiento de gente como tu."
        }),
        0,
        ({
            "parece que lo haces bien",
            "no lo haces muy bien"
        }),
        "¿Quieres responder algo a Alug?");

    nuevo_dialogo("parece que lo haces bien", ({
        "|Alug resopla con desdén ante la respuesta de ${a:dnc}."
        "|"
        "|Alug resopla con desdén ante tu respuesta.",
        "'¿Crees que necesito tu aprobación, extranjer${a:v}?",
        "'Métete en tus asuntos y déjame en paz.",
        ":se prepara para retomar su entrenamiento."
    }), "muñeco");

    nuevo_dialogo("no lo haces muy bien", ({
        ":te mira enfadada.",
        "'¿Y tú que demonios sabrás sobre cómo lo hago?",
        "'¿Tú te has visto? Tienes pinta de esmirriad${a:v}.",
        ":se cruza de brazos.",
        "'Cuando quieras te demuestro como lo hago de bien. Mano a mano. Tú y yo. Sin armas ni "
            "trucos.",
        "'Cuando te parta la nariz a ver si me dices que no lo hago bien, foraster${a:v}.",
        "'¿Y bien?, ¿qué va a ser?, ¿te a atreves a poner algo de acción tras tus palabras?",
        "mision " + _mision + " inicio"
    }), "muñeco");

    nuevo_dialogo("otra ronda", ({
        "'Tras tu última victoria te has venido arriba, ¿eh, ${a:qcn}?",
        "&sonríe con desdén y dice&Lo entiendo, créeme. No obstante, ahora no tengo ganas de "
            "entrenar contigo.",
        "'Sí, sí... ríete lo que quieras. ¡Pero no te despistes!, ¡pronto te exigiré la revancha!",
    }));

    nuevo_dialogo("revancha", ({
        "|Alug le dedica una sonrisa pícara a ${a:qcn}."
        "|"
        "|Alug te dedica una sonrisa pícara.",
        "'¿Quieres que te dé otra paliza tan pronto?",
        "'Quizás otro día, ${a:qcn}.",
        "'Será mejor que entrenes un poco más. No estás a mi nivel. No me gusta abusar de gente "
            "tan indefensa como tú.",
        ":se ríe a carcajada abierta."
    }));

    // Conversaciones deshabilitadas si la misión está hecha
    deshabilitar("muñeco", "mision " + _mision);
    deshabilitar("parece que lo haces bien", "mision " + _mision);
    deshabilitar("no lo haces muy bien", "mision " + _mision);

    // Diálogos para hablar del resultado del último evento
    prohibir("otra ronda", "hito " + _mision + " exito");
    prohibir("revancha", "hito " + _mision + " fallo");
}

private void setup_conversacion_antes_confianza()
{
nuevo_dialogo("poza", ({
"|${a:qcn} se acerca a Alug y le cuenta algo entre susurros."
"|"
"|Te acercas a Alug y le cuentas lo que ha sucedido en la poza plateada.",
"|Alug mira de arriba a abajo a ${a:qcn} mientras parece tomar una decisión."
"|"
"|Alug te mira de arriba abajo y tras pensarlo un rato, decide que quizá puedas ayudar.",
"'Está bien, puedes pasear y hablar con los demás, pero la yurta de los enfermos está totalmente prohibida, aún no confío lo suficientemente en ti.",
"hito altyr_principal conversacion_alug_1",
}));

prohibir("poza", "hito altyr_principal obtencion_muestras_agua_lobos");
deshabilitar("poza", "hito altyr_principal conversacion_alug_1");
}

private void setup_conversacion_despues_confianza()
{
nuevo_dialogo("confianza", ({
"|Alug dirige una sonrisa a ${a:qcn}, y te parece que es lo más precioso que hubieras visto jamás."
"|"
"|Alug muestra un asomo de sonrisa, nada propio de ella y te parece lo más hermoso que has visto en tu vida.",
"'Está bien extranjer${a:v}, has demostrado ser muy útil para nuestro asentamiento, así que te permitiré pasar unos instantes a ver a los enfermos, pero procura no cansarlos.",
"|Alug se retira de la entrada para permitirle el paso a ${a:qcn}."
"|"
"|Alug se retira de la entrada para permitirte el paso.",
"hito altyr_principal permiso_alug",
}));
prohibir("confianza", "hito altyr_principal hechas_altyr_secundarias");
deshabilitar("confianza", "hito altyr_principal obtencion_muestra_enfermos");
}

/**
 * Esto inicia el bucle de la misión de sparring.
 */
void respuesta_positiva_conversador(object pj, string mision, string hito, string tema)
{
    if (!pj || mision != "altyr_secundarias_sparring_alug")
    {
        respuesta_positiva_conversador(pj, mision, hito, tema);
        return;
    }

    ejecutar_monster_string_ya(
        "'¡Muy bien ${a:qcn}!, ¡veamos de qué pasta estás hech${a:v}!",
        (["actor":pj]));

    /**
     * Es necesario retrasarlo para que primero se limpie el menú pendiente
     * mostrado para confirmar la misión en el jugador.
     */
    call_out("iniciar_sparring", 0, pj, 0);
}

/*******************************************************************************
 * Sección: Sparring
 ******************************************************************************/
/**
 * Si vencemos terminamos la misión exitosamente
 */
void sparring_evento_fin_exito(object pj, class sparring sp)
{
    if (!pj)
        { return; }

    pj->nuevo_hito_mision("altyr_secundarias_sparring_alug", "exito", 0);

    ejecutar_monster_string_ya(({
        "'¡Suficiente, ${a:qcn}!",
        "'Parece que tenías razón y algo hacía mal. Es bueno ver que no eres ${a:num} pusilánime.",
        "'Esta vez me has ganado. Puedes estar orgullos${a:v}."
    }), (["actor":pj]));
}
/**
 * Si fallamos terminamos la misión con fallo
 */
void sparring_evento_fin_fracaso(object pj, class sparring sp)
{
    if (!pj)
        { return; }

    pj->nuevo_hito_mision("altyr_secundarias_sparring_alug", "fallo", 0);

    ejecutar_monster_string_ya(({
        ":se rie escandalosamente.",
        "'¿Qué pasa, ${a:qcn}? ¿Te cuesta mantenerte en pie?",
        "'Parece que al final no lo hago tan mal, ¿no?"
    }), (["actor":pj]));
}

varargs class sparring_turno *dame_turnos_sparring(string dificultad)
{
    string pregunta_turno = "¿Cómo vas a responder al ataque de Alug?";

    return ({
        new(class sparring_turno,
            msj_inicio     : "|" _C_PREPARACION_ALL "Alug adquiere una posición de combate que "
                                "deja su flanco derecho expuesto.",
            ataque         : "un contundente cabezazo",
            opcion_valida  : "puñetazo con la mano izquierda",
            msj_fin        : ":frunce el ceño, enfadada.",
            pregunta_turno : pregunta_turno
        ),
        new(class sparring_turno,
            msj_inicio     : "|" _C_PREPARACION_ALL "Alug se prepara para lanzar una patada.",
            ataque         : "una dura patada en la espinilla",
            opcion_valida  : "contraataque",
            msj_fin        : ":se tambalea ligeramente, pero logra recomponerse.",
            pregunta_turno : pregunta_turno
        ),
        new(class sparring_turno,
            msj_inicio     : "|" _C_PREPARACION_ALL "Alug retrocede unos pasos y deja su costado "
                                "izquierdo desprotegido.",
            ataque         : "una patada en la entrepierna (¡QUÉ DOLOR!)",
            opcion_valida  : "puñetazo con la mano derecha",
            msj_fin        : ({
                                ":se tambalea, trastabilla y cae al suelo sobre una rodilla.",
                                "|Alug dedica una sonrisa a ${a:qcn}."
                                "|"
                                "|Alug te sonrie con satisfacción."
                            }),
            pregunta_turno : pregunta_turno
        ),
    });
}

mixed dame_mensajes_sparring_fin_fracaso(object pj, class sparring sp)
{
    return (
        {sprintf(
             "Te tambaleas hasta caer de rodillas. Claramente Alug te ha " +
                 "derrotado. Haces acopio de la poca " +
                 "dignidad que te queda e intentas mantenerte erguid%s, " +
                 "a pesar de estar magullad%s y un poco avergonzad%s.",
             pj->dame_vocal(),
             pj->dame_vocal(),
             pj->dame_vocal()),
         "",
         sprintf(
             "Alug claramente ha derrotado a %s. %s, magullad%s y algo " +
                 "avergonzad%s, parece hacer un esfuerzo por mantenerse " +
                 "erguid%s.",
             pj->query_cap_name(),
             pj->dame_este(1),
             pj->dame_vocal(),
             pj->dame_vocal(),
             pj->dame_vocal())});
}

string *sparring_dame_mensajes_punyetazoi_exito(object pj)
{
    switch (random(0)) {
        case 0:
            return (
                {"Consigues pasar entre las defensas de la niña y la golpeas "
                 "en " +
                     "el costado con un rápido puñetazo.",
                 pj->query_cap_name() + " evade las defensas de Alug con un " +
                     "rápido movimiento que culmina en un puñetazo con su "
                     "mano " +
                     "izquierda que castiga el costado de la niña."});
    }
}
string *sparring_dame_mensajes_punyetazoi_fallo(object pj, string atk)
{
    switch (random(3)) {
        case 0:
            return (
                {"Lanzas tu puño con poca fuerza a la cara de la " +
                     "niña, pero esta es demasiado rápida y bloquea tu " +
                     "ataque atrapándote el bajo una firme presa de su axila, "
                     "lo que "
                     "le permite propinarte " +
                     atk + ".",
                 pj->query_cap_name() + " lanza su puño derecho contra la " +
                     "cara de Alug, pero ésta es demasiado rápida y consigue " +
                     "bloquear su ataque, atrapándo" + pj->dame_le() +
                     " el brazo " +
                     "bajo una firme presa de su axila, lo que le permite " +
                     "contraatacar con " + atk + "."});
        case 1:
            return (
                {"Alug se adelanta a tu puñetazo y bloquea tu brazo con una " +
                     "presa antes de golpearte " + atk + ".",
                 "Alug se adelanta al ataque de " + pj->query_cap_name() + " " +
                     "y bloquea su brazo con una presa antes de golpearle "
                     "con " +
                     atk + "."});
        default:
            return (
                {"Alug consigue evitar tu puño y te da " + atk + " que casi " +
                     "te hace caer.",
                 pj->query_cap_name() + " intenta alcanzar a Alug con un " +
                     "puñetazo, pero la niña es demasiado rápida y lo evita, " +
                     "contraatacando con " + atk + " que casi derrumba a su " +
                     "agresor" + pj->dame_a() + "."});
    }
}
string *sparring_dame_mensajes_punyetazod_exito(object pj, string atk)
{
    switch (random(0)) {
        case 0:
            return (
                {"Permaneces en el sitio y, con un rápido puñetazo, castigas " +
                     "el costado de la niña.",
                 pj->query_cap_name() + " permanece en su sitio y, con un " +
                     "rápido puñetazo, castiga el costado de Alug."});
    }
}
string *sparring_dame_mensajes_punyetazod_fallo(object pj, string atk)
{
    return sparring_dame_mensajes_punyetazoi_fallo(pj, atk);
}
string *sparring_dame_mensajes_bloquear_exito(object pj, string atk)
{
    switch (random(0)) {
        case 0:
            return (
                {// "Puñetazos normales consecutivos" es una referencia a
                 // 1punch-man, no lo cambien please
                 "Adquieres una posición defensiva y logras ver como Alug " +
                     "lanza " + atk +
                     " contra ti. A pesar de que tu postura no " +
                     "es la adecuada, permaneces en el sitio y solo cuando "
                     "su " +
                     "ataque está a punto de golpearte retrocedes dos pasos, " +
                     "desequilibrando a tu atacante. Aprovechándote de su " +
                     "desventaja, vuelves a tu posición inicial y le golpeas " +
                     "con dos puñetazos normales consecutivos que le "
                     "aciertan " +
                     "en el costado y en el mentón.",
                 pj->query_cap_name() + " adopta una posición defensiva y, " +
                     "con un rápido juego de piernas, logra retroceder a " +
                     "tiempo de evitar " + atk + " de Alug. Acto seguido, " +
                     pj->query_cap_name() +
                     " se adelanta de nuevo y golpea a su " +
                     "rival con dos puñetazos normales consecutivos que le " +
                     "aciertan en el costado y en el mentón."});
    }
}
string *sparring_dame_mensajes_bloquear_fallo(object pj, string atk)
{
    switch (random(2)) {
        case 0:
            return (
                {"Intentas colocar tus puños en posición de defensa, pero " +
                 "Alug se te adelanta y te propina " + atk + ". El dolor " +
                 "escuece, pero lo que más te afecta es la risa de la niña, " +
                 "que te mira como si fueras tont" + pj->dame_vocal() + "."});
        default:
            return (
                {"Adquieres una posición defensiva, pero Alug lo ve venir y " +
                     "te lanza un grueso esputo que te alcanza en toda la "
                     "cara. " +
                     "Instintivamente, abandonas tu posición de defensa para " +
                     "limpiarte, instante que aprovecha tu rival para "
                     "alcanzarte " +
                     "de improvisto con " + atk + ".",
                 pj->query_cap_name() + " adquiere una posición defensiva y " +
                     "Alug responde lanzándole un grueso esputo que alcanza "
                     "a " +
                     "su rival en toda la cara. " + pj->query_cap_name() +
                     ", de " +
                     "forma apresurada e instintiva, abandona su posición de " +
                     "defensa para limpiarse, momento que su rival aprovecha " +
                     "para alcanzarle de improvisto con " + atk + "."});
    }
}
