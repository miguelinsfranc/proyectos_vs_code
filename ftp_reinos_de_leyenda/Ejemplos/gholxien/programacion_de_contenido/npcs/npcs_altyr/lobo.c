// 2021 [Gholxien]: Ficha de lobo

inherit "/obj/monster";

void setup() {
    set_name("lobo");
    generar_alias_y_plurales();
    set_short("Lobo");

    set_long(
        "Un enorme animal de pelo plateado y gran musculatura. En sus ojos rojos como la sangre se aprecian pequeñas pinceladas de azul. Sus garras permanentemente "
        "contraídas para lanzarse al ataque te inspiran un gran temor. De su hocico entreabierto caen grandes hilos de lo que parece ser un líquido verduzco.\n");

    fijar_clase("aventurero");
    fijar_fue(18);
    fijar_des(19);
    fijar_con(18);
    fijar_int(2);
    fijar_sab(2);
    fijar_car(5);
    fijar_genero(1);
    fijar_alineamiento(0);
    fijar_nivel(12);
    fijar_raza("canino");
    fijar_bo(100);
    fijar_be(120);
    fijar_bp(1);
    fijar_pvs_max(dame_pvs_max() * 2);
    add_zone("altyr_bosque");
    set_move_after(15, 45);

    load_chat(60, ({
            ":gruñe mientras una saliva verduzca chorrea de entre sus colmillos.",
            ":aúlla hacia la espesura del bosque.",

        }));
}
