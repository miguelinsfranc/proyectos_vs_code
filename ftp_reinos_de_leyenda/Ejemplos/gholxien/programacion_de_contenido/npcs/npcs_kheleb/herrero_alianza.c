// Durmok 30/03/2003
//17.05.2021 [Gholxien]: Quito algunos temas de conversación por incompatibilidad con noticias nuevo 4430

#include "/d/kheleb/path.h"
inherit "/obj/conversar_npc.c";

void setup() {
    set_name("herrero");
    set_short("Kharmel, Herrero de la Alianza");
    add_alias(({"kharmel","herrero"}));
    set_main_plural("Herreros");
    add_plural("herreros");
    fijar_genero(1);
    fijar_raza("enano");
    fijar_religion("eralie");
    fijar_fe(20);
    set_random_stats(12,17);
    fijar_nivel(10+random(5));
    set_long("Es uno de los miembros más importantes de la comunidad de la Alianza, "
      "ya que en sus forjas de Kheleb Dum elabora fantásticos materiales que "
      "luego vende más baratos en su tienda de la Alianza.\n");
    load_chat(30,
      ({
	1, "'La Alianza puede estar orgullosa de lo bien que ha prosperado.",
	1, "'¿No te parecen preciosas las montañas que rodean Kattak?",
	1, ":saca brillo a una de sus armaduras.",
	

      }));
    load_a_chat(50,
      ({
	1, "'¡Muerte a los bastardos!",
      }));
    ajustar_dinero(random(3)+1,"khaldan");
    add_clone(BARMADURAS+"camiseta", 1);
    add_clone(BARMADURAS+"pantalones",1);
    add_clone(BARMADURAS+"botas",1);
    init_equip();
    
    nuevo_dialogo("nombre",({
      "'Me llamo Kharmel y soy el herrero oficial de la Alianza de Darin.","'Mi trabajo consiste en gestionar los puntos de los integrantes de la Alianza, y cambiarlos por equipamiento si ellos lo precisan.",":se rie."}));
    habilitar_conversacion();
}
void attack_by(object atacante)
{
    atacante->add_timed_property("perseguido_kd",1,90);
    ::attack_by(atacante);
}
