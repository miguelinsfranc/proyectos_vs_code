//Eckol Abr21

#include "/d/kheleb/path.h"
inherit BMONSTRUOS+"/bases/sacerdote_curandero";
void setup()
{
   set_name("sacerdotisa");
   set_short("Brinntha Girlhim, Voz de Eralie");
   set_main_plural("Imágenes de Brinntha Girlhim");
   generar_alias_y_plurales();
   set_long(
   "Brinntha es una de las más famosas sacerdotisas del poderoso clan Girlhim.\n" +
"Se trata de una doncella enana que parece joven, pues sus frondosas" +
" trenzas doradas no exhiben ni una pincelada de plata y sus facciones," +
" redondeadas y sin mácula alguna, son las típicas en una típica" +
" muchacha de su raza. Sus ojos castaños, cálidos y aterciopelados," +
" observan a quienes entran en el templo con serenidad y benevolencia" +
" pero cuando se fijan en ti sientes que hasta el más oculto" +
" sentimiento de tu corazón queda desnudo ante su inquisitiva mirada.\n" +
"La luz de las joyas luminosas distribuidas por la sala reluce en su" +
" rostro, o tal vez sea el poder de su dios, que la rodea como un aura.\n" +
"Famosa en toda Kheleb Dum, incluso entre los enanos que siguen a otros" +
" dioses, su voz es respetada en la toma de decisiones y todo enano que" +
" sienta que su corazón está cubierto por las brumas de la tristeza, la" +
" duda o el desencanto encontrará en ella sabias palabras y la paz que añora.\n");

   fijar_raza("enano");
   fijar_clase("eralie");
   
   anyadir_lenguaje("khadum",100);   
   fijar_lenguaje_actual("khadum");
   fijar_nivel(55);
   fijar_alineamiento(-50000);
   fijar_religion("eralie");
   fijar_fe(100);
   fijar_ciudadania("kheleb", 400);
   fijar_genero(2);
   fijar_bo(350);
   fijar_be(100);
   fijar_bp(400);
   fijar_pe_max(200);
     fijar_pe(200);
   set_max_social_points(20000);
   set_social_points(20000);

   set_random_stats(15,18);
   fijar_sab(18);
   fijar_fue(40);
   fijar_car(18);
   fijar_pvs_max(5000);
   set_heart_beat(1);
   
   crear_cartel("Cartel");
   
   add_clone(ARMADURAS+"cinturon_hortera", 1);
   add_clone(ARMADURAS+"tunica_clerigo", 1);
   add_clone(ARMADURAS+"alpargatas", 1);

   init_equip();

   load_chat(150, ({
       ":te mira y sientes que adivina tus más ocultos pensamientos.",
       ":desliza sus dedos por las runas del altar.",
       ":susurra una leve plegaria a su dios mientras observa los bajorrelieves de los muros.",
       "'¿Necesitas consejo?",
       "'La paz de Eralie sea contigo.",
       "'¡Ay de aquel que traiga el mal a este sagrado lugar!",
     }));



load_a_chat(150, ({
       ":alza las manos pidiendo el auxilio de Eralie.",
       ":clama el poder de su dios.",
       }));

}
