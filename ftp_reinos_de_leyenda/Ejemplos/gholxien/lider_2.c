//Gholxien 06/2023: Campamento de bandidos

#include "/w/gholxien/programacion_de_contenido/bandidos_urlom/campamento_bandidos/base_campamento.h"

inherit "/std/room.c";

void setup() {
    add_exit(S, CAMPBAN + "lider_1.c", "standard");
    add_exit(N, CAMPBAN + "tesoro.c", "standard");
    add_exit(NE, CAMPBAN + "lider_3.c", "standard");
    add_exit(NO, CAMPBAN + "lider_4.c", "standard");
    modify_exit(({N,NE,NO}),({"function","pasar"}));
modify_exit(S,({"function","pasar2"}));
	set_light(60);
    set_short("Campamento de bandidos: Estudio principal");
    set_long("Este es el lugar central de trabajo de Roderick, donde se dedica a planificar y trazar estrategias. Un sencillo y funcional escritorio de madera se encuentra apoyado contra una de las paredes de tela.\n"
        "Sobre él, reposan mapas enrollados, planos desplegados y otros documentos relacionados con los intrincados planes de la agrupación. Alrededor del escritorio, se han dispuesto sillas plegables y resistentes, proporcionando un espacio cómodo para trabajar.\n"
        "El suelo está cubierto con una lona gruesa y resistente que se extiende hasta los bordes de la habitación. Este estudio ha sido diseñado de manera funcional, sin adornos superfluos ni muebles innecesarios. Una pequeña lámpara de aceite colocada sobre el escritorio proporciona una luz tenue pero suficiente para realizar tareas de lectura y escritura.\n"
        "Las paredes de tela tensa están equipadas con ganchos donde se cuelgan prendas de vestir y objetos personales de Roderick y sus hijas. Estos elementos se mantienen ordenados, ocupando un espacio mínimo en la habitación.\n");

add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/ayrah", 1);
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/roderick", 1);
add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/sylara", 1);
}

int pasar(string salida, object caminante, string msj_especial) {
    if (secure_present("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/ayrah.c", TO) || secure_present("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/sylara.c", TO) || secure_present("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/roderick.c", TO)) {
        return notify_fail("No puedes pasar a las estancias privadas de Roderick y sus hijas hasta que no estén muertos.\n");
    }
    return 1;
}

int pasar2(string salida, object caminante, string msj_especial) {
    if (dame_peleando() > 0) {  // Verificar la condición adicional
        if ((secure_present("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/ayrah.c", TO) || secure_present("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/sylara.c", TO) || secure_present("/w/gholxien/programacion_de_contenido/bandidos_urlom/npcs/roderick.c", TO)) && !caminante->dame_ts("agilidad")) {
            return notify_fail("Los ocupantes de la tienda se coordinan a la perfección y logran cortarte el paso.\n");
        }
    }
    return 1;
}
