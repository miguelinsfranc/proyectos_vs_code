// Satyr 2k7
inherit "/clases/luchadores/personal/forjador";
void setup()
{

	add_static_property("crear_cartel", "Placa");
	set_name("forjador");
	set_short("Cruzado forjador");
	add_alias(({"cruzado","forjador","cruzado forjador"}));
	set_main_plural("Cruzados forjadores");
	fijar_genero(1);
	set_long(
		"\tEl honor de un buen cruzado soldado está en saber usar y proteger su arma -normalmente una lanza- "
		"para defender el honor y la monarquía de Takome, así como la fortaleza de Eralie en Eirea. "
		"Este forjador ha llegado más lejos y conoce los secretos para crear las armas personales, armas de los "
		"tiempos inmemoriables de los guerreros de Arturo. "
		"Su trabajo es el de proporcionar ese arma a cualquier Takomita honorable que se la pida e instruirle "
		"en el arte de hacer que ambos se conviertan en un único. "
		"Su mirada es amable y sus enormes ojos marrones te ojean entre los enormes rizos de sus alborotados cabellos "
		"negros, es tan fuerte como noble y sobre todo, es alguien orgulloso de ser un Takomita.\n");
		
	fijar_fue(18);
	fijar_carac("des", 18);
	fijar_carac("con", 18);
	fijar_carac("int", 15);
	fijar_carac("sab", 11);
	fijar_carac("car", 16);
	fijar_extrema(random(40)+60);
	
	
	quitar_lenguaje("dendrita");
	nuevo_lenguaje("adurn",100);
	fijar_lenguaje_actual("adurn");
	add_clone("/baseobs/herramientas/martillo_forja", 1);
	add_clone("/baseobs/armaduras/cuero.c",1);
	add_clone("/baseobs/armaduras/pantalones.c",1);
	add_clone("/baseobs/armaduras/botas.c",1);
	
	load_chat(20,({
		"'Las lanzas son el arma de un verdadero Takomita. Con sus puntas afiladas atravesamos el corazón del mal sin nisiquiera mancharnos.",
		"'Por el honor y la gloria del combate, cederé mis conocimientos a cualquier luchador que los necesite."
		}));
		
	init_equip();
}
