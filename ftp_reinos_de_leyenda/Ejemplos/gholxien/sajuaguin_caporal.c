// Sierephad Abr 2k18	-- Saguajin para los naufragios

#include "/d/oceano/path.h"

inherit NPCS + "oceano/sajuaguin.c";

string dame_ruta_esbirro()
{
    return NPCS + "oceano/sajuaguin"; 
}

void setup()
{
    ::setup();
    
    set_name("sajuaguin");
    set_short("Caporal Sajuaguín");
    set_main_plural("Caporales Sajuaguines");
	generar_alias_y_plurales();
	
    set_long(
        "Los Caporales Sajuaguines, son los más grandes y fieros de entre la estirpe de los "
		"Capataces Sajuaguines, llegando a conseguir un puesto de renombre y liderazgo entre "
		"todos ellos debido a la gran cantidad de esclavos conseguidos para la reina. \n\n "
		
		"Prácticamente todas las escamas que recubren su cuerpo son una mezcla entre colores "
		"rojizos y cobrizos, apenas siendo visibles algunos grupos de escamas azuladas, más "
		"habituales en los capataces Sajuaguines de menor renombre y tamaño. Se dice que este "
		"color es debido a la inconmensurable furia que circula por su interior y que demuestran "
		"a cada latigazo que dan, aunque otras historias cuentan que dicho color es debido a "
		"los cientos, o incluso miles, de víctimas esclavas que han caído bajo la fuerza de sus "
		"latigazos, tiñendo a lo largo de los años su cuerpo con su oscura sangre. \n\n"
		
		"Los Caporales Sajuaguines además de ser conocidos por su inagotable tiranía y "
		"crueldad, también son reconocidos por su inigualable dominio del látigo, armas "
		"que manejan con una feroz agilidad y una destreza letal, como si esta fuera un "
		"apéndice más de su escamoso cuerpo. \n\n"
		
		"Cuentan las leyendas del océano, que un Caporal Sajuaguín puede ser incluso capaz "
		"de someter a base de látigo y furia a algunos de los ejemplares más jóvenes de los "
		"dragones anguila... Aunque un combate así probablemente acabaría con la vida del "
		"Sajuaguín sesgada por las gigantescas fauces de la bestia oceánica, las leyendas "
		"siempre tienen ciertos matices de veracidad...\n\n"
    );

    set_random_stats(18, 19);
    fijar_con(45);
    fijar_des(25);
    fijar_fue(25);
    fijar_nivel(45);

	add_clone(ARMAS      + "latigo_anemonas.c"     , 1);
	add_clone(BESCUDOS	 + "gran_escudo_madera.c"  , 1);
	
	add_clone(BARMADURAS + "escamas.c"             , 1);
    add_clone(BARMADURAS + "brazalete.c"           , 1);
    add_clone(BARMADURAS + "brazalete_izq.c"       , 1);
    add_clone(BARMADURAS + "guante.c"              , 1);
    add_clone(BARMADURAS + "guante_izq.c"          , 1);
    add_clone(BARMADURAS + "yelmo.c"               , 1);
    add_clone(BARMADURAS + "grebas_mallas.c"       , 1);
    add_clone(BARMADURAS + "botas_campaña.c"       , 1);
    add_clone(BARMADURAS + "anilla.c"              , 1);
	add_clone(ARMADURAS  + "collar_caporal.c"      , 1);

    init_equip();
    
	ajustar_armadura_natural(30);
	fijar_maestrias(([
		"Lacerante ligera": 100,
	]));
	
	add_static_property("ts-agilidad"   	, 85);
    set_heart_beat(1);
    ajustar_bo(350);
    ajustar_be(150);
    ajustar_bp(150);
    
    load_chat(30,({
		"'¡Trabajad! ¡O no veréis otro día para servir a la emperatriz!",
		"'¡No hay descanso! ¡No hay piedad! ¡Trabajad o vuestra espalda conocerá mi látigo!",
		"'Comparados con las bestias que cacé en pasadas Slabyhä... ¡No sois nada! ¡TRABAJAD!",
		"'¡Trabajad! ¡O machare mis escamas con la sangre de vuestros cuerpos muertos!",
		":mantienen una postura inamovible, mirando con desprecio a todos los esclavos del lugar.",
        ":chasquea su látigo con un ágil movimiento insuflando un temor atroz a todos los que lo rodean.",
		":se mueve por la zona sin perder detalle del trabajo de los esclavos que hay a su alrededor.",
		":mueve rápidamente su látigo y lacera sádicamente la espalda de un esclavo.",
		":golpea con saña a un esclavo que se había cesado durante un instante en su trabajo.",
    }));
	
	load_a_chat(50,({
		"'¡Cuando acabe contigo no quedaran ni restos para alimentar a los tiburones!",
		"'¡No eres rival para los Sajuaguines! ¡Y menos para un Caporal!",
		"'¡Acabaras sirviendo como esclavo al Palacio de Coral!",
		"'¡Tus colmillos serán un trofeo más en mi collar!",
		"'¡Sométete y puede que tu agonía sea rápida!",
        ":arremete con una furia incontrolable.",	
		":mueve su látigo, hostigándote con furiosos ataques.",
		":se mueve a una increíble velocidad, rodeándote para atacarte desde el flanco.",
		":mueve su látigo para bloquear hábilmente tus ataques.",
		":chasquea sus mandíbulas con fuerza mientras te mira con su rostro contraído por la ira.",
    }));
	
	nueva_habilidad("coletazo");
	add_attack_spell(75, "golpecertero", 3);
	add_attack_spell(75, "ataquedoble", 3);
	add_attack_spell(90, "atrapar", 3);

    set_aggressive(1,15);
    set_heart_beat(1);
    
}

object *dame_esbirros()
{
    if ( ! environment() )
        return allocate(0);
    
    return filter(all_inventory(environment()), (: $1 && base_name($1) == dame_ruta_esbirro() :));
}


void heart_beat() 
{
	
	object target;
	object esbirro;
	string str="contra la pared";
	
    ::heart_beat();
	
	if (dame_pvs()<dame_pvs_max()/2)
		if ( !dame_protector() && sizeof(dame_esbirros()) ) {
			if (esbirro=element_of(dame_esbirros())) {
				do_emote("señala inquisitivamente a "+esbirro->query_short()+".");
				do_say("!Soldado! ¡Protégeme o acabaras "+
				element_of(({
					"convertido en un esclavo",
					"picando coral en las minas de esclavos",
					"sirviendo de comida para los tiburones",
					"descuartizado por mis propias manos",
					"limpiando los suelos del Palacio de Coral",
					"siendo mi presa en la próxima Slabyhä"
				}))
				+"!",0);
				esbirro->do_emote("asiente decididamente con la cabeza y se interpone delante de "+query_short()+", protegiéndolo.");
				fijar_protector(esbirro);
			}
		}

	if (!target=TO->query_timed_property("atrapado-quien"))
		return;
	
	//Para que no lo haga siempre y de tiempo a otros npcs de la sala a pegarle un poco
	tell_object("sierephad","Target: "+target+"\n");
	if (random(3))
		return;
	
	tell_object("sierephad","Locations: ENV(target)="+ENV(target)+" ENV(TO)"+ENV(TO)+"\n");
	//Si la victima no esta en nuestra room
	if (ENV(target)!=ENV(TO))
		return;

	tell_object("sierephad","3\n");
	foreach(object npc in filter(all_inventory(ENV(TO)),(:$1->query_npc() && $1->dame_invocacion():)))
		if (npc->dame_invocador()==target) {
			//if (!random(2))
				str="contra "+npc->query_name();
			break;
		}
	tell_object("sierephad","Str: "+str+"\n");
	"/habilidades/luchadores/desplomar.c"->do_command(str,TO);

}