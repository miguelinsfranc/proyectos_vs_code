// Satyr 2007
// Loredor el anciano, protector de thorin
// Guardia de thorin numero 1
// Eckol Sep17 Balance vida un poco al alza.

// Os parecerá raro, pero la druida que está a su lado va montada entre sus ramas
// No va a galopar ni hacer nada raro nunca jamás
inherit "/obj/montura.c";
//inherit "/obj/monster.c";

int dame_guardia() { return 1; }
//string dame_tipo_montura() { return "ent"; }

void setup()
{
	set_name("loredor");
	add_alias(({"loredor","anciano","ent", "guardia"}));
	add_plural(({"loredor","ancianos","ent"}));
	set_short("Loredor el Anciano Ent");
	set_main_plural("imagenes de Loredor, el Anciano Ent");
	set_long(
		"\tUn enorme roble, o por lo menos, eso es lo que le parecería a los ojos de aventureros inexpertos. "
		"Lo que tienes ante ti es una de las criaturas más ancestrales y benignas que el mundo jamás ha conocido: un ent. "
		"Los ents son espíritus del bosque con forma de árbol que se mueven mágicamente gracias al poder natural que los envuelve. "
		"Su aspecto es casi idéntico al de un ser humano normal -con un tamaño muchísimo superior, evidentemente-; su corteza "
		"emula la piel de una persona, el moho que en algunas partes de su tronco está adherido parece asemejarse a vello facial o corporal y "
		"las raíces que brotan de sus gruesas piernas parecen dedos; fuese quien fuese el que creó a esta mítica raza, está claro "
		"que tomó como modelo una de las muchas criaturas humanoides que puebla (o ha poblado) Eirea. "
		"Loredor es un ent anciano; puede que haya vivido en siglos en los que los que ninguna otra criatura poblase la tierra y seguramente "
		"haya visto el ir y venir de los dioses del mundo. "
		"Tras cientos de años vagando por el mundo en las sombras, las recientes masacres en el bosque de Thorin atrajeron su atención "
		"y decidió ofrecer su protección a los Nyathor, aquellos que lo dan todo por proteger su bosque. "
		"Su aspecto es el de un roble y su gran tamaño, fuerza y robustez dan fe del gran árbol del que proviene. "
		"Permanece estático, semi-enraizado en el rico suelo de Thorin mientras observa el cielo calmadamente, moviéndose tan sólo cada "
		"varios minutos cuando algún animal despistado trepa por él, confundiéndolo con un simple árbol.\n");

	fijar_tipo_montura("ent");
	fijar_religion("eralie");
	fijar_raza("ent");
	fijar_clase("barbaro");
	fijar_ciudadania("thorin",999);
	fijar_tamanyo_racial(7);
	nuevo_estilo("ent");
	fijar_estilo("ent");
	fijar_peso_max(dame_peso_max()+500000);  
	
	fijar_alineamiento(-100000);
	fijar_genero(1);
	
	set_random_stats(19,24);
	fijar_extrema(100);
	fijar_des(5);
	fijar_nivel(40+random(6));
	fijar_armadura_natural(dame_armadura_natural()+50);
	adjust_tmp_damage_bon(100);
	fijar_pvs_max(25000);
	fijar_bo(100);
	
	nuevo_lenguaje("adurn",100);
	fijar_lenguaje_actual("adurn");
	
	add_loved("ciudadania",({"thorin"}));
	add_hated("enemigo",({"Thorin"}));
	add_hated("relacion",({"thorin"}));
	add_hated("propiedad",({"enemigo_thorin"}));
	add_hated("propiedad",({"enemigo_nyathor"}));
	set_aggressive(6);
	
	add_property("sin miedo",1);
	add_property("no_lanzar",1);
	
	add_attack_spell(20,"lanzar",3);
	add_attack_spell(20,"enredar",3);
	
	load_chat(10,({
			":se sacude, luchando por librarse de una ardilla que se le subio a la cabeza.",
			":entierra sus raíces bajo tierra.",
			":canta algo en una lengua grave, gutural e incomprensible.",
			"#sonreir ardilla",
			":dice en ent: Grouuurraaarraarruuuuum.",
			":dice en ent: Mergrearou grreuarustragrum setrgritrgm.",
			":parece inmóvil durante minutos."}));
	quitar_lenguaje("adurn");			
}

varargs int do_say(string msj, mixed msj_literal)
{
	do_emote("dice en ent: Gurraarruuuum.");
	return 1;	
}


// Las armas no-hachas le hacen la mitad de daño, sin embargo las hachas le causan un 50% mas
int weapon_damage(int cantidad,object atacante,object arma,string tipo_ataque)
{
	switch(arma->dame_arma_base())
	{
		case "hacha a dos manos":
		case "hacha de mano":
		case "hacha de batalla":
		case "hacha doble":
			tell_object(atacante,"¡Tu "+arma->query_short()+" daña gravemente a "+query_short()+"!\n");
			cantidad=cantidad*1.5;
			break;
		
		default:
			tell_object(atacante,"Tu "+arma->query_short()+" no parece ser muy efectiva contra "+query_short()+".\n");
			cantidad=cantidad*0.5;
			break;
		break;
	
	}
	return ::weapon_damage(cantidad,atacante,arma,tipo_ataque);
}

// El daño "agua" le hace un 50% del daño, el daño "fuego" un 50% mas
int spell_damage(int pupa,string tipo,object lanzador,object hechizo)
{
	switch(tipo)
	{
		case "fuego":
			tell_object(lanzador,"¡"+query_short()+" grita de dolor con su voz atronadora al sentir el fuego devorándole!\n");
			pupa=pupa*1.5;	
		break;
		
		case "agua":
			tell_object(lanzador,"¡"+query_short()+" no parece verse muy afectado por tu hechizo!\n");
			pupa=pupa*0.5;
		break;
		
		default:
	}
	return ::spell_damage(pupa,tipo,lanzador,hechizo);
}

// Las tiradas de salvación de agilidad no las llevan muy bien =(
int dame_ts(string tipo,int modificador,string elemento,string nombre_efecto)
{
	if (tipo=="agilidad")
		modificador-=50;
		
	return ::dame_ts(tipo,modificador,elemento,nombre_efecto);
}

// Pega duro
void attack()
{
	::attack();
	::attack();
}

void event_inicio_pelea(object atacado,object atacante)
{
	object naiad=present("naiad",environment());
	
		
	if (atacado==this_object()&&naiad)
		naiad->attack_ob(atacante);
	
	
}
//Baelair 07/03/2014
//Al ser montura no ponía bien los despojos, ahora con esto sí.
object dame_despojos(int calidad, object pj, object herramienta, object cuerpo)
{
	return clone_object("/baseobs/despojos/manejadores/ent.c", cuerpo->dame_tamanyo(), calidad, pj, herramienta, cuerpo);
}
