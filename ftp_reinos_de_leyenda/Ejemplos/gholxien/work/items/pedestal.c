//28.03.2021 [Gholxien]:  Pedestal para el trono, para mi workroom

inherit "/std/item";

void setup() {
    set_name("pedestal");
    generar_alias_y_plurales();
    set_short("Pedestal");
    set_main_plural("Pedestales");
    set_long("Caes de rodillas, embargado por la emoción al contemplar tal uso del metal, que acabó inspirando el bellísimo pedestal que se alza imponente ante ti. Seguramente supere los 100 pies de altura, y te preguntas cómo es que la fortaleza no se viene abajo, soportando tal peso. Observas que justo en el centro hay una escalera ancha, tal vez deberías subir por ella y ver qué hay más arriba... o tal vez no deberías, no hay que jugar con los dioses en absoluto. Tal vez, habiendo llegado hasta aquí, debas retroceder en silencio y poner pies en polvorosa.\n");
    reset_get();
    fijar_genero(1);
}
