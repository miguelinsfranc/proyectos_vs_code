		//20.05.2023 [Gholxien]: Asiento de Gurthang para mi workroom

inherit "/std/item";

void setup() {
    set_name("asiento");
    generar_alias_y_plurales();
    set_short("Asiento de Gurthang");
    set_main_plural("Asientos");
    set_long("Este es el asiento de Gurthang el belicoso. Se trata de un gran bloque de madera que ha sido tallado burdamente por alguien inexperto en las artes del tallado de maderas. Por los bordes surgen astillas de madera y los antebrazos se hallan roídos, como si algún gigantesco animal tratase de arrancarlos con sus garras.\n");
    reset_get();
    fijar_genero(1);
}
