		//28.03.2021 [Gholxien]: Asiento de Draifeth para mi workroom

inherit "/std/item";

void setup() {
    set_name("asiento");
    generar_alias_y_plurales();
    set_short("Asiento de Draifeth");
    set_main_plural("Asientos");
    set_long("Este es el asiento de Draifeth, un burdo trono hecho de madera, que fue recubierta por algún tipo de pigmento, que le da esa tonalidad rosácea. El asiento es bastante alto, y a los pies del mismo reposa un reposapiés, para que la dama oscura pudiera apoyar sus extremidades y escuchar las palabras del emperador.\n");
    reset_get();
    fijar_genero(1);
}
