//28.03.2021 [Gholxien]: Trono de los dioses para mi workroom

inherit "/std/item";

void setup() {
    set_name("trono");
    generar_alias_y_plurales();
    set_short("Trono de los dioses");
    set_main_plural("Tronos");
    set_long("La gigantesca mole que se alza ante tí te produce admiración y un temor reverente. Está hecho de todos los minerales que existen en Eirea, siendo el más bajo la dura piedra, y el mas alto hecho de Mithril. Aquí es donde se sienta Gholxien, el emperador de los dioses, y es aquí donde rige sobre los dioses y Eirea.\n");
    reset_get();
    fijar_genero(1);
}
