		//28.03.2021 [Gholxien]: Asiento de Seldar para mi workroom

inherit "/std/item";

void setup() {
    set_name("asiento");
    generar_alias_y_plurales();
    set_short("Asiento de Seldar");
    set_main_plural("Asientos");
    set_long("Te hallas ante el corrupto asiento de Seldar, una pila de madera carcomida y podrida, que te quedas asombrado buscando una explicación, sobre cómo consigue mantenerse en pie. Luego te percatas de los huesos que entrelazan una madera con otra, dándole soporte. Su oscuro color negro deja entrever en el respaldo del mismo, dibujado con cuidado y con sangre, el símbolo de Seldar: un pentáculo envuelto en llamas. A los pies del trono yace una larga y ancha plataforma, echa de metal oxidado, donde el señor del mal reposa sus extremidades.\n");
    reset_get();
    fijar_genero(1);
}
