		//28.03.2021 [Gholxien]: Asiento de Gedeon para mi workroom

inherit "/std/item";

void setup() {
    set_name("asiento");
    generar_alias_y_plurales();
    set_short("Asiento de Gedeón");
    set_main_plural("Asientos");
    set_long("Observas lo que parece ser un trono, hecho con madera de roble. Carece de ostentosidadess pareciéndose así a cualquier trono de un monarca cualquiera. A diferencia del resto de los asientos, este no lleva una plataforma para descansar los pies.\n");
    reset_get();
    fijar_genero(1);
}
