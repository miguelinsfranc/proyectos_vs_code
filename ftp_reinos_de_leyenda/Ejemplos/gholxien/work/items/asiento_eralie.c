		//28.03.2021 [Gholxien]: Asiento de Eralie para mi workroom

inherit "/std/item";

void setup() {
    set_name("asiento");
    generar_alias_y_plurales();
    set_short("Asiento de Eralie");
    set_main_plural("Asientos");
    set_long("Este es el bendito trono de Eralie, dios del bien y defensor de la libertad y la esperanza. Está hecho de puro mithril y plata, excepto los antebrazos, que están hechos de madera de arce. A los pies del asiento yace una plataforma hecha de mármol, donde la esperanza personificada descansa sus extremidades.\n");
    reset_get();
    fijar_genero(1);
}
