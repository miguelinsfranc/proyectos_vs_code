//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Las escaleras de los dioses");
    set_long("Estás en unas escaleras largas y anchas, por donde podrían subir cinco elefantes sin rozarse. Están forjadas de hierro, extraído desde las profundas minas subterráneas que se hallan debajo del panteón de los dioses. Oyes un ruido extraño, como si fuera un leve traqueteo entre dos metales\n");
    set_light(80);
    add_sound(({
            "traqueteo",
            "metales"
        }), "Escuchas con atención el leve traqueteo que producen dos metales al entrechocarse por acción de la suave brisa, y descubres una finísima fisura en la parte norte, justo en la pared del pedestal");
    add_item(({
            "fisura",
            "pared"
        }), "Se trata de una fisura vertical en la pared del pedestal, tal vez podrías intentar hacer palanca y empujar la pared.");
    add_exit("arriba", "/w/gholxien/work/rooms/pedestal_escaleras_01.c", "stair");
    add_exit("abajo", "/w/gholxien/work/rooms/pedestal_escaleras_03.c", "stair");
}

void init() {
    ::init();
    add_action("empujar_pared", "empujar");
}

int empujar_pared(string str) {
    if (str == "pared") {
        if (this_object()->query_timed_property("pared_empujada_recientemente") == 0) {
            add_timed_property("pared_empujada_recientemente", 1,1000);
            modify_item("pared", "La pared parece haber sido empujada recientemente.\n");
            tell_object(this_player(), "Empujas con muchísimo esfuerzo la pared metálica,"
                " y esta se desplaza con un ruido atronador, dejando al descubierto una gigantesca puerta metálicaen dirección norte, incrustada en el gigantesco pedestal.\n");
            tell_room(this_object(), this_player()->query_cap_name() + " Empuja con esfuerzo la pared"
                " y deja al descubierto una puerta metálica en dirección norte, incrustada en el interior de este gigantesco pedestal.\n", this_player());
            add_exit("norte", "/w/gholxien/work/rooms/pedestal_interior_01.c", "door");
            adjust_open_door("norte", 1, 1);
			renew_exits();
            return 1;
        }

        tell_object(this_player(), "Ya has empujado la pared recientemente, y estás demasiado cansado como para seguir empujando.\n");
        return 1;
    }
    notify_fail("¿Empujar qué?\n");
    return 0;
}
