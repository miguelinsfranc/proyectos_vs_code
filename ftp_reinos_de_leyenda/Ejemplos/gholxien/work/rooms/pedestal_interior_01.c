//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Túnel estrecho");
    set_long("Te encuentras en el interior de un túnel estrecho y lóbrego, donde lo único que sientes es el debastador frío que esta gran mole metálica te produce. Sientes como si todo el peso estuviera sobre tus hombros, y te cuesta respirar. Al fondo puedes ver cómo este túnel continúa mas, introduciéndose cada vez mas en el interior de este gigantesco pedestal.\n");
    set_light(80);
    add_exit("norte", "/w/gholxien/work/rooms/pedestal_interior_02.c", "corridor");
    add_exit("sur", "/w/gholxien/work/rooms/pedestal_escaleras_02.c", "door");
}
