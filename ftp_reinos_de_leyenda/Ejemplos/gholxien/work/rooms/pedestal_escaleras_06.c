//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Las escaleras de los dioses");
    set_long("Estás en unas escaleras largas y anchas, por donde podrían subir cinco elefantes sin rozarse. Están forjadas de hierro, extraído desde las profundas minas subterráneas que se hallan debajo del panteón de los dioses. Al sur se extiende una sala de enormes y bellas proporciones, donde los dioses de alto renombre en Eirea toman las decisiones que rigen y guían el destino de Eirea y de los mortales, siempre con el consentimiento del emperador divino, por supuesto.\n");
    set_light(80);
    add_exit("arriba", "/w/gholxien/work/rooms/pedestal_escaleras_05.c", "stair");
    add_exit("sur", "/w/gholxien/work/rooms/asientos_dioses_03.c", "corridor");
}
