//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Entrada al recinto del trono");
    set_long("Te encuentras en lo alto de este precioso pedestal, cuya cima alberga uno de los mayores tronos que se han visto jamás sobre la faz de este mundo. El pedestal está surcado por varias líneas blancas horizontales, varias otras líneas negras, dispuestas en forma vertical, y dos líneas que van en diagonal, cruzando el pedestal de parte a parte y formando una especie de cruz, en cuyo centro está situado el trono. Como no podía ser menos, estas líneas son las representaciones geométricas de la continua lucha entre el bien y el mal, y el constante empeño de los neutrales para recuperar el equilibrio. Unas inmensas escaleras, forjadas con el hierro mas puro jamás visto, se extienden por debajo de tí. Cuidado con caerte, sería mortal.\n");
    set_light(80);
    add_exit("norte", "/w/gholxien/work/rooms/pedestal_07.c", "corridor");
    add_exit("abajo", "/w/gholxien/work/rooms/pedestal_escaleras_02.c", "stair");
}
