//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Panteón de los primogénitos: Estatua en honor a Gestur");
    set_long("Gestur, uno de los dioses primogénitos, uno de los primeros dioses que llegaro a nuestra dimensión tras el Baldrick. En esta parte de la sala, no hay tapices, pero un pedestal de obdirum se halla en la sala, que sostiene una gigantesca estatua de Gestur.\n");
    set_light(80);
    add_sign("Una estatua de un anciano sabio se alza ante ti. De cabellos largos y dorados, y con sus ojos grises centelleando, pareciera que va a tomar vida de un momento a otro. Esta es la estatua de Gestur, uno de los dioses primogénitos que llegó a Eirea junto con Astaroth, lumen, gedeon y Oneex.\n",
        "Gestur fue uno de los dioses primogénitos que llegaron a Eirea después del Baldrick.\n"
        "Junto con Lummen, creó los animales, y más adelante, los humanos.\n"
        "Gestur era un dios neutral, aunque durante la Guerra de los Dragones se alineó con Osucaru y Lummen para luchar contra fuerzas de Astaroth y Oskuro.\n"
        "La principal ambición de Gestur era mantener Eirea como un mundo justo.\n"
        "Como la mayoría de los dioses primogénitos, Gestur tenía la capacidad de manipular la energía, y de crear vida.\n"
        "Tenía una gran influencia sobre los animales, y un control excepcional sobre el fuego.\n"
        "Durante varias eras, los Clérigos de Gestur impartieron la justicia por Eirea.\n"
        "Cuando Gestur fue encerrado en la dimensión conocida como Iluminado, Paris siguió otorgando poderes a los seguidores de Gestur hasta El Cataclismo.\n\n",
"Gran estatua de Gestur",
		"estatua");
    add_exit("norte", "/w/gholxien/work/rooms/asientos_dioses_05.c", "standard");
    add_exit("sur", "/w/gholxien/work/rooms/asientos_dioses_15.c", "standard");
    add_exit("este", "/w/gholxien/work/rooms/asientos_dioses_09.c", "standard");
}
