//20.05.2023 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Panteón del Caos: Asiento de Gurthang");
    set_long("Te encuentras en una sala lóbrega, donde tienes la continua sensación de ser observado y medido por las oscuras sombras que se remueven en los rincones mas apartados de la enorme sala. Este es el rincón de Gurthang el belicoso, quien anteriormente fue un paladín de la luz, digno hijo de Paris, pero que posteriormente sucumbió a las artimañas y promesas vacías de Oskuro. Tras ser encerrado por los ancestros, sus siervos lo liberaron, y desde entonces la espada negra causa miedo y caos en Eirea. En el centro de la gran sala se halla un asiento de madera negra con los brazos de un color magenta, que descansa sobre una gran alfombra de color azul oscuro.\n");
    set_light(80);
    add_clone("/w/gholxien/work/items/asiento_gurthang.c", 1);
	add_exit("norte", "/w/gholxien/work/rooms/asientos_dioses_06.c", "standard");
	add_exit("sur", "/w/gholxien/work/rooms/asientos_dioses_16.c", "standard");
    add_exit("oeste", "/w/gholxien/work/rooms/asientos_dioses_012.c", "standard");
}
