//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Panteón del bien: Asiento de Eralie");
    set_long("Eralie, el creador de la vida y defensor de la libertad, esperanza y la naturaleza. Este es su asiento, de color blanco, que se yergue magestuoso sobre un magnífico tapiz azul. El tapiz ha sido tejido por un maestro magistral en su obra, pues ha logrado plasmar perfectamente las ondulaciones que se producen en el agua a causa del oleaje. Esta combinación de colores evoca en aquél que mira el conjunto con detenimiento, una noche cálida sobre el inmenso océano de color azul, y con la luna Argan, alzándose magestuosa en su belleza y dispuesta a zambullirse en el océano al salir el sol, como dos amantes que se reúnen después de mucho tiempo sin verse. Eh aquí el concepto de la creación que Eralie proclama y defiende, pues ningún mortal o dios puede deshacer y rehacer la creación a su antojo.\n");
    set_light(80);
    add_clone("/w/gholxien/work/items/asiento_eralie.c", 1);
	add_exit("norte", "/w/gholxien/work/rooms/asientos_dioses_04.c", "standard");
	add_exit("sur", "/w/gholxien/work/rooms/asientos_dioses_14.c", "standard");
    add_exit("oeste", "/w/gholxien/work/rooms/asientos_dioses_10.c", "standard");
add_exit("este", "/w/gholxien/work/rooms/asientos_dioses_08.c", "standard");
}
