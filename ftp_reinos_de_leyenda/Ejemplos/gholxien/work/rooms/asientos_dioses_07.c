//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Panteón del mal: Asiento de Seldar");
    set_long("Te hallas ante el asiento de Seldar, señor del mal, también conocido como el usurpador o el ángel caído. En el suelo hay un amplio tapiz de color rojo, sobre el cual se alza, imponente, el trono de Seldar, hecho de una madera corrupta, tan negra como el corazón de su dueño. Como no, esto solo es una metáfora, que hace alusión a los millares de litros que el mal está dispuesto a derramar, con el fin de hacerse con el mundo conocido.\n");
    set_light(80);
    add_clone("/w/gholxien/work/items/asiento_seldar.c", 1);
	add_exit("norte", "/w/gholxien/work/rooms/asientos_dioses_02.c", "standard");
	add_exit("sur", "/w/gholxien/work/rooms/asientos_dioses_12.c", "standard");
    add_exit("oeste", "/w/gholxien/work/rooms/asientos_dioses_08.c", "standard");
add_exit("este", "/w/gholxien/work/rooms/asientos_dioses_06.c", "standard");
}
