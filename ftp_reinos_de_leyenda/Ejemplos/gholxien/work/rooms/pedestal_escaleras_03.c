//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Las escaleras de los dioses");
    set_long("Estás en unas escaleras largas y anchas, por donde podrían subir cinco elefantes sin rozarse. Están forjadas de hierro, extraído desde las profundas minas subterráneas que se hallan debajo del panteón de los dioses.\n");
    set_light(80);
    add_exit("arriba", "/w/gholxien/work/rooms/pedestal_escaleras_02.c", "stair");
    add_exit("abajo", "/w/gholxien/work/rooms/pedestal_escaleras_04.c", "stair");
}
