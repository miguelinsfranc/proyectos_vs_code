//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("En lo alto del pedestal");
    set_long("Te encuentras en lo alto de este precioso pedestal, cuya cima alberga uno de los mayores tronos que se han visto jamás sobre la faz de este mundo. El pedestal está surcado por varias líneas blancas horizontales, varias otras líneas negras, dispuestas en forma vertical, y dos líneas que van en diagonal, cruzando el pedestal de parte a parte y formando una especie de cruz, en cuyo centro está situado el trono. Como no podía ser menos, estas líneas son las representaciones geométricas de la continua lucha entre el bien y el mal, y el constante empeño de los neutrales para recuperar el equilibrio.\n");
    set_light(80);
    add_item("suelo", "El suelo está a una considerable distancia. Seguro que si saltas desde aquí obtendrías una de las muertes más rápidas.\n");
    add_item("techo", "El techo se encuentra a una considerable altura desde donde estás. Dirías que desde el trono al techo son casi unos 50 pies de alto. Te preguntas cómo se verá el techo desde abajo.\n");
    add_item(({
            "pedestal"
        }), "Un precioso pedestal de plata y mithril, que da a la sala un aspecto grandioso. También sirve para realzar a los dioses que vienen aquí a rogar favores al dios emperador o a rendir su pleitesía. Observas en una de las esquinas lo que parece ser una diminuta mancha roja.\n");

    add_item(({
            "sangre",
            "mancha"
        }), "Te agachas y observas una mancha reseca de color rojizo, seguramente debe de ser sangre. Te da la sensación de que el aspecto grandioso de la sala solo sirve para tapar el verdadero aspecto de la misma, y te preguntas cuál será el verdadero fin de esta sala.\n");

    add_smell(({
            "sangre"
        }), "Sin duda, el leve olor a metal no te deja duda sobre qué es esta mancha.\n");

    add_feel(({
            "mancha",
            "sangre"
        }), "Tal y como esperabas, la mancha está reseca. Dirías que lleva ya mucho tiempo aquí.\n");

    add_exit("norte", "/w/gholxien/work/rooms/pedestal_01.c", "standard");
    add_exit("noroeste", "/w/gholxien/workroom.c", "standard");
    add_exit("sur", "/w/gholxien/work/rooms/pedestal_06.c", "standard");
    add_exit("sudoeste", "/w/gholxien/work/rooms/pedestal_07.c", "standard");
    add_exit("oeste", "/w/gholxien/work/rooms/pedestal_04.c", "standard");
}
