//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Ante los asientos de los dioses");
    set_long("Observas la magnífica vista que los dioses han construido aquí, en su morada. En frente de tí, un alto y gigantesco pedestal, hecho de plata y mithril, alberga en su cima una de las mayores obras jamás creadas. El trono de los dioses, ocupado por el emperador divino. De tras de ti están los asientos de los dioses del panteón, ordenados cada uno por sus creencias, de manera que ninguno esté sentado junto a su enemigo. Bajas tu mirada y observas una alfombra, cuyo centro es de color dorado, y mientras se va aproximando a su borde, poco a poco se transforma en un blanco puro. Al norte se extienden unas escaleras de hierro, que reptan sobre el inmenso pedestal hasta llegar a lo alto. Son bastante anchas, y desde aquí abajo resulta una estampa formidable. Supones que está hecho así a propósito, para atemorizar a los que vienen a rogar favores al emperador divino, el dios entre los dioses.\n");
    set_light(80);
    add_item("techo", "Alzas tu mirada hacia arriba, con el fin de poder contemplar el techo, pero quedas estupefacto cuando no llegas a verlo desde aquí.\n");
    add_clone("/w/gholxien/work/items/pedestal.c", 1);
    add_exit("norte", "/w/gholxien/work/rooms/pedestal_escaleras_06.c", "corridor");
	add_exit("sur", "/w/gholxien/work/rooms/asientos_dioses_08.c", "standard");
    add_exit("oeste", "/w/gholxien/work/rooms/asientos_dioses_04.c", "standard");
    add_exit("este", "/w/gholxien/work/rooms/asientos_dioses_02.c", "standard");
}
