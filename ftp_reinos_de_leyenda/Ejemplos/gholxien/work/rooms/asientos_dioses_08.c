//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Panteón del equilibrio: Asiento de Gedeón");
    set_long("Aquí yace el asiento de Gedeón, aquél que se encarga de mantener el equilibrio a toda costa. En el suelo hay un tapiz completamente marrón, sobre el cual se alza el magnífico asiento, de color amarillo. Esta rara combinación de colores simboliza la creación, pues el color marrón es una alusión a la tierra, y el asiento amarillo simboliza nada más y nada menos que los árboles otoñales, que se preparan para afrontar el duro invierno, para luego florecer en la primavera, con sus lustrosas hojas de colores y el suave olor que desprenden.\n");
    set_light(80);
    add_clone("/w/gholxien/work/items/asiento_gedeon.c", 1);
	add_exit("norte", "/w/gholxien/work/rooms/asientos_dioses_03.c", "standard");
	add_exit("sur", "/w/gholxien/work/rooms/asientos_dioses_13.c", "standard");
    add_exit("oeste", "/w/gholxien/work/rooms/asientos_dioses_09.c", "standard");
add_exit("este", "/w/gholxien/work/rooms/asientos_dioses_07.c", "standard");
}
