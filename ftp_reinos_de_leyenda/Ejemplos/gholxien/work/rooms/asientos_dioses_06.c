//27.03.2021 [Gholxien]: Rooms para la workroom de Gholxien

inherit "/std/room";

void setup() {
    set_short("Panteón del Caos: Asiento de Draifeth");
    set_long("He aquí el asiento de Draifeth, señora del caos, también conocida como la dama oscura. El suelo está alfombrado con un tapiz de color negro, y sobre él se alza un imponente trono de madera, de color rosa y con los antebrazos negros, conformando así el conjunto de colores que adopta Draifeth.\n");
    set_light(80);
    add_clone("/w/gholxien/work/items/asiento_draifeth.c", 1);
	add_exit("norte", "/w/gholxien/work/rooms/asientos_dioses_01.c", "standard");
	add_exit("sur", "/w/gholxien/work/rooms/asientos_dioses_11.c", "standard");
    add_exit("oeste", "/w/gholxien/work/rooms/asientos_dioses_07.c", "standard");
}
