#define CREADOR "gholxien"
/**
 * Workroom para inmortales sin experiencia.	Aqui intentare que
 * podais encontrar las cosas mas basicas que se pueden hacer con una
 * room.	Tambien incluye algunas cosas un poco mas avanzadas.
 * Fairiel : 7 agosto, 2000
 * Modificado por Kaitaka: 15 noviembre, 2002
 */

/**
 * Para hacer una room, siempre tienes que heredar de otro archivo.
 * Segun el tipo de room que quieras, deberas heredar de un archivo
 * o de otro.Los archivos de los cuales puedes heredar se encuentran
 * en /std/ , y son:
 * room.c				-> para hacer habitaciones de interior.
 * outside.c		-> para hacer habitaciones en el exterior.
 * bosque.c			-> para hacer habitaciones que se encuentran en un bosque.
 *									con set_bosque(int) en el setup() de la room podremos
 *									poner la densidad del bosque:
 *									0 -> hay matorrales, pero no arboles (Camino entre el
 *											 bosque o claro en al bosque).
 *									1 -> Hay pocos arboles, estan a los lados del camino
 *											(Adentrandose en el bosque).
 *									2 -> Bosque frondoso (Dentro del bosque).
 * underground.c	-> para hacer habitaciones que se encuentren bajo tierra,
 *									por ejemplo, debajo de una montaña, una cueva, una mina..
 * suboscuridad.c	-> para hacer habitaciones que esten muy por debajo de
 *									tierra (ciudad drow, duergars, etc).
 * underwater.c		-> para hacer habitaciones que se encuentran debajo del
 *									agua.
 * pantano.c			-> para pantanos
 * nieve.c				-> para zonas de nieves perpetuas
 */
inherit "/std/room.c";

void setup() {
    set_short("Trono de Gholxien, el emperador divino");
    set_long("Observas admirado el gigantesco trono que se alza ante tí. Mide mas de 7 pies de alto, y se encuentra sobre un pedestal hecho de Mithril y plata, que mide unos 20 metros de diámetro, y no sabrías decir cuantos metros mide de altura, ya que es tan alto que el suelo se divisa a mas de 50 metros debajo de tí. Eso sí, podrías asegurar que el techo de la sala se encuentra a una considerable altura. La sala del trono está magníficamente decorada con tapices que recuerdan a grandes batallas de Eirea, así como varias estatuas de los dioses más antiguos. He aquí donde se sienta Gholxien, el emperador de los dioses, aquél que rige sobre todos los dioses y el destino de Eirea.\n");
    //Selecciona el nivel de luz en la habitacion. 'man luz' para mas detalles.
    set_light(80);
    /*
     * add_item() es usado para describir todos los objetos que aparecen en
     * tu descripcion.
     */

    add_item("suelo", "El suelo está a una considerable distancia. Seguro que si saltas desde aquí obtendrías una de las muertes más rápidas.\n");
    add_item("techo", "El techo se encuentra a una considerable altura desde donde estás. Dirías que desde el trono al techo son casi unos 50 pies de alto. Te preguntas cómo se verá el techo desde abajo.\n");

    /** Puedes darle a varios objetos la misma descripcion */
    add_item(({
            "pedestal"
        }), "Un precioso pedestal de plata y mithril, que da a la sala un aspecto grandioso. También sirve para realzar a los dioses que vienen aquí a rogar favores al dios emperador o a rendir su pleitesía. Observas en una de las esquinas lo que parece ser una diminuta mancha roja.\n");

    add_item(({
            "sangre",
            "mancha"
        }), "Te agachas y observas una mancha reseca de color rojizo, seguramente debe de ser sangre. Te da la sensación de que el aspecto grandioso de la sala solo sirve para tapar el verdadero aspecto de la misma, y te preguntas cuál será el verdadero fin de esta sala.\n");
    /** Añade caracteristicas reales a tu habitacion */
    add_smell(({
            "sangre"
        }), "Sin duda, el leve olor a metal no te deja duda sobre qué es esta mancha.\n");

    add_feel(({
            "mancha",
            "sangre"
        }), "Tal y como esperabas, la mancha está reseca. Dirías que lleva ya mucho tiempo allí.\n");

    //En el futuro, tus habitaciones deben de poseer descripciones completas
    //sin que puedan quedarse a medias...

    /*
     * Para incluir objetos en las habitaciones se usara la funcion add_clone(),
     * para mas detalles man add_clone..
     * Estos objetos pueden ser armas, npcs o cualquier otro objeto.
     * En general se encontraran en ficheros diferentes a los que hacemos una
     * llamada con la funcion add_clone(dir,num).
     */
    //add_clone("/obj/misc/button.c",1);
    add_clone("/w/gholxien/work/items/trono.c", 1);

    //add_sign("Un cartel de madera.\n",
    //		"Bienvenido a "+mud_name()+"\n\n"
    //" Como ya deberían haberte informado, has de superar una pequeña prueba en el período de "
    //"una semana. Para ello vas a disponer de varias ayudas para aprender. La primera y más "
    //"importante de ellas es la Escuela de Aprendices, para llegar a ella has de entrar por "
    //"la salida aprendizaje, una vez dentro no podrás salir de ella hasta que no estés completamente "
    //"preparad@ para ello.\n"
    //"Otras fuentes de ayuda las encontrarás en el comando man, o en la web del mud. En el código "
    //"de esta room hay algunas ideas para iniciarse, pero están mejor explicadas en la escuela, ves "
    //"allí y a partir de ahora aprende a programar como allí se explica.\n"
    //"Recuerda que tu espacio de trabajo está en /w/"+lower_case(CREADOR)+"/.\nAtentamente,\n\n"
    //"Rutseg %^BOLD%^BLUE%^Martillo de %^RESET%^GREEN%^Piedra\n","Cartel","cartel");

    /*
     * Aqui se describe las salidas que pudiera tener tu habitcion
     * add_exit(direccion, destino, tipo)
     * direccion: Sera lo que aparezca en el rotulo de direcciones
     * destino: Direccion donde te movera esa salida
     * tipo: El tipo de salida tiene que ver con una escalera, puerta, etc..
     * Leer /doc/roomgen/exit_type_help para mas informacion, o mirad
     * man add_exits. Aqui vienen muy bien detalladas.
     * Las salidas comunes (norte, sur, etc) deben ponerse en castellano.
     */
    add_exit("comun", "/w/comun", "hidden");
    add_exit("campban", "/w/gholxien/programacion_de_contenido/bandidos_urlom/campamento_bandidos/senda_1", "hidden");
	//Como queremos que la puerta comun empiece estando abierta le ponemos esto
    //adjust_open_door("comun", 0, 0);

    //Esto es necesario para anyadir la salida de comun a tu habitacion
    "/w/comun.c"->add_exit(CREADOR, "/w/" + CREADOR + "/workroom", "door");
    "/w/comun.c"->renew_exits();
    add_exit("este", "/w/gholxien/work/rooms/pedestal_01.c", "standard");
    add_exit("oeste", "/w/gholxien/work/rooms/pedestal_02.c", "standard");
    add_exit("sur", "/w/gholxien/work/rooms/pedestal_04.c", "standard");
    add_exit("sudeste", "/w/gholxien/work/rooms/pedestal_03.c", "standard");
    add_exit("sudoeste", "/w/gholxien/work/rooms/pedestal_05.c", "standard");
}

/*
 * La funcion init se llama automaticamente cuando se carga
 * en memoria el objeto (room, npc o lo que sea)
 * Aqui es donde tienes que definir las funciones extra.
 * man init para mas ayuda.
 */
//void init()
//{
/*
 * Muy importante esta linea. Aqui llamais al init()
 * del archivo del que heredamos, y es importante.
 * No os olvideis de esta linea.
 */
//	::init();
/*
 * La funcion add_action ata un comando verbal a una
 * funcion local, definida en la room. El primer
 * parametro es el nombre de la funcion local,
 * y el segundo el nombre del comando verbal.
 * man add_action para mas ayuda.
 * Se pueden definir tantas funciones locales como se
 * deseen.
 */
//	add_action("limpiar_mesa","limpiar");
//	add_action("mover_mesa","mover");
//}
/*
 * Funcion local. El int delante indica que hemos de
 * devolver un entero.
 * El string str que se le pasa, es lo que escribimos
 * despues del comando verbal
 */
//int limpiar_mesa(string str)
//{
//	if(str=="mesa")
//	{
/*
 * Jugamos con las propiedades. Le preguntamos si ya
 * hemos limpiado la mesa
 */
//if( this_object()->query_timed_property("LIMPIA")==0 )
//{
/*
 * Cuando limpiamos la mesa, le anyadimos
 * una timed property para que le de tiempo
 * a ensuciarse y no estar limpiandola todo
 * el rato XD joer, que somos inmortales :P
 * Para mas ayuda:
 * man add_timed_property, query_timed_property
 * man add_property, query_property
 * man add_static_property, query_static_property
 */
//			this_object()->add_timed_property("LIMPIA",1,1000);
/**
 * Con esta funcion modificamos la descripcion de un item
 * ya existente.
 * man modify_item
 */
//modify_item("mesa","La mesa parece haber sido limpiada recientemente.\n");
//tell_object(this_player(),"Empiezas a limpiar la mesa y"
//				" encuentras un papel en ella.\n");
//tell_room(this_object(),this_player()->query_cap_name()+" limpia la mesa"
//				" encontrando en ella un papel.\n",this_player());
/*
 * Aqui usa una funcion distina para clonar cosas.
 * Clone object solo clona el objeto.	Para que salga aqui lo
 * tienes que traer con el	->move(this_object())
 */
//clone_object("/obj/misc/newcreator_paper.c")->move(this_object());
//Devolvemos 1 porque la accion ha sido hecha con exito
//return 1;
//}
//Lo que sale cuando ya has limpiado la mesa una vez
//tell_object(this_player(),"Ya has limpiado la mesa.\n");
//return 1;
//}
/*
 * Los mensajes de error siempre se dan con notify_fail
 */
//notify_fail("Nada en esta habitacion necesita limpiarse excepto la mesa.\n");
/*
 * Devolvemos 0 por que no hemos realizado la accion que
 * deseabamos
 */
//	return 0;
//}

//int mover_mesa(string str)
//{
//if(str=="mesa")
//{
//		tell_object(this_player(),"Mueves la mesa descubriendo una "
//"trampilla debajo de esta.\n");
//tell_room(this_object(),this_player()->query_cap_name()+" mueve la mesa.\n",this_player());
//add_exit("trampilla","/room/void.c","door");
///**
//* Como modificamos las salidas en una room ya cargada
//* hay que actualizar las salidas. Se hace con renew_exits()
//* man renew_exits para mas ayuda
//*/
//renew_exits();
/**
 * Introduccion para impedir el paso en salidas.
 * La funcion encargada de esto es modify_exit
 * En este caso, le indicamos que queremos
 * modificar la salida "trampilla" con una funcion
 * "function" que se llama "permiso" y que debemos
 * definir. Si devuelve 0, impide el paso.	En caso
 * contrario deja pasar. La funcion modify_exit
 * tiene muchas mas utilidades. Para verlas con mas
 * detalle, man modify_exit
 */
//modify_exit("trampilla",({"function","permiso"}));
//return 1;
//}
//return notify_fail("Mover que?.\n");
//}

/*
 * Funcion definida para ver quien puede pasar.
 * Solo si eres inmortal te deja pasar XD
 */
//int permiso()
//{
//	if(this_player()->query_creator())
//	{
//tell_object(this_player(),"¡Pasa, Oh inmortal!\n");
//return 1;
//}
//return notify_fail("¡¡¡¡Un mortal no puede entrar aqui!!!!\n");
//}

