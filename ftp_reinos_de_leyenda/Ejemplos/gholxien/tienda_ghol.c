//04.04.2021 [Gholxien]: Tienda para uso propio


inherit "/std/tienda";

void setup() {
    set_short("Tienda de %^BLUE%^BOLD%^Gholxien%^RESET%^");
    set_long("Al entrar, te encuentras con una amplia selección de objetos y cosas. Hay antorchas, palas, clavos, tuercas, odres y todo tipo de utensilios disponibles. Estos elementos son exhibidos en estanterías de madera robustas y resistentes, diseñadas para soportar el peso de los productos.\n"
        "Además, puedes observar burdos ganchos de metal en los que se cuelgan cuerdas, garfios y otros objetos similares. Estos ganchos añaden un toque rústico a la tienda.\n"
        "En el centro de la tienda, hay un sencillo y pequeño mostrador de madera de cerezo. A diferencia del resto de la estancia, este mostrador se mantiene ordenado y despejado, proporcionando un espacio agradable para realizar las transacciones.\n"
        "Detrás del mostrador, encontrarás al tendero de la tienda, un simpático conejo que está a cargo de atender a los clientes y ayudarlos en sus compras.\n"
        "Aunque la tienda no cuenta con una selección de objetos específicos en este momento, Gholxien se asegura de mantener una variedad de productos comunes a disposición de los clientes.\n");

    poner_tipo_tienda("general");
    fijar_tabla_objetos((["/baseobs/armas/daga.c":1]));
    //items a mirar
    add_item(({
            "objetos",
            "cosas"
        }), "Antorchas, palas, clavos, tuercas, odres, todo tipo de utensilios.");
    add_item("estanterias", "De madera, sencillas y resistentes para soportar bastante peso.");
    add_item("ganchos", "Burdos ganchos de metal de los que cuelgan cosas tales como cuerdas, garfios, etc.");
    add_item("mostrador", "Es un sencillo y pequeño mostrador de madera de cerezo que en contraste con la estancia atestada de cosas, está bastante ordenado y despejado.");
    poner_tendero("/w/gholxien/programacion_de_contenido/npcs/npcs_propios/conejo.c");
}

