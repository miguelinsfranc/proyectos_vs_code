#include "/d/takome/path.h"
inherit "/std/forja.c";
void setup()
{
    set_short("%^RED%^Altos Hornos de los dioses%^RESET%^");
    set_long(
        "Accedes a los altos hornos de los Dioses, sientes como los hornos "
        "que alimentan el calor aquí te sofocan, al entrar observas a los "
        "herreros trabajar, hombres martillando las armas que el gran emperador "
        "de los Dioses ha mandado construir, a tu izquierda observas un enorme "
        "escudo, es el emblema del Panteón de los Dioses, forjado en honor a los Dioses "
        "nobles del Panteón, a la derecha, cientos de armas, hierro, y "
        "minerales que serán utilizados en el proceso del forjado, sin "
        "duda alguna, el tamaño de los Altos Hornos refleja la "
        "capacidad metalúrgica del Panteón.\n"
    );

    fijar_luz(60);
    fijar_capacidad(20000);
  
    poner_cartel("grabado","Un cartel metálico seguramente creado con esta misma fragua.");
add_clone("/w/gholxien/forjador_takome.c", 1);
	}


