// Altaresh 19
#include "../path.h"
#include <tiempo.h>
inherit GENERADOR_BASE;

void poner_npcs(object sala, string zona, string subzona){
     switch(subzona) {
		case "camino":
			if(!CLIMA->query_noche())
			{
				if (!random(4)){
					sala->add_clone(NPCS+"forajido_hobgoblin.c", random(3));
				}
				if (!random(4)){
					sala->add_clone(NPCS+"gaviota.c",1);
				}
			}else{
				if(!random(4)) sala->add_clone(BMONSTRUOS+"jabali.c",1);
			}
		break;
		case "senda":
	
			if(!CLIMA->query_noche())
			{
				if (!random(4)){
					sala->add_clone(BMONSTRUOS+"raton", random(3));
				}
				if (!random(4)){
					sala->add_clone(BMONSTRUOS+"halcon",1);
				}
			}else{
				if(!random(4)){
					sala->add_clone(BMONSTRUOS+"gato_montes",1);
				}
			}
		break;
	 }
}

void poner_color_salidas(object sala, string zona, string subzona) {
    switch (subzona)
   {
     case "camino":
		sala->set_exit_color("naranja");
	break;
     case "senda": 
		sala->set_exit_color("negro");
	break;
   }

}
void poner_short(object sala, string zona, string subzona) {
	
    switch(subzona) {
    case "camino":
        subzona = "%^ORANGE%^Camino sobre el acantilado%^RESET%^";
        break;
    case "senda":
        subzona = "%^BOLD%^BLACK%^Senda Litoral%^RESET%^";
        break;
    }
	zona = "%^BOLD%^%^BLUE%^Acantilados%^RESET%^ de %^BOLD%^WHITE%^H%^YELLOW%^iros%^RESET%^";
    sala->set_short(zona+": "+subzona);    
}
void poner_especiales(object sala, string zona, string subzona) {
    sala->fijar_clima("eldor");
	switch(subzona) {
    case "camino":
        sala->fijar_frecuencia_sensaciones(30);
		sala->fijar_sensaciones(({
		"El olor del mar te reconforta",
		"Las olas golpean las rocas bajo tu posición con gran fuerza",
		"El mar levanta grandes cantidades de espuma al chocar contra las rocas",
		"Gaviotas se enfrentan al aire que viene de mar adentro",
		"Varias gaviotas sobrevuelan la zona meciéndose con el aire",
		"Una isla parece vislumbrarse mar adentro",
		"Esquivas una cagada de gaviota que choca contra el suelo a pocos centímetros de ti",
		"Muchos metros de roca te separan del mar que hay bajo tu posición",
		"El aire que proviene del mar es bastante fuerte en esta zona",
		"La vegetación de la zona se mece con el fuerte aire que viene del mar",
		"No te entra en la cabeza como Hiros pudo levantar este acantilado de una galopada",
		"El sonido del mar rompiendo contra el acantilado te recuerda a una tormenta",
		"La humedad de la zona se hace sudar",
		}));
        break;
    case "senda":
		sala->fijar_frecuencia_sensaciones(30);
		sala->fijar_sensaciones(({
		"Solo ves pendiente si miras hacia el este",
		"El sendero es cada vez más estrecho y las ramas arañan tus pertenencias",
		"Te parece oir a lo lejos el rugido de un felino",
		"Varios roedores te miran desde sus refugios entre la maleza",
		"Te preguntas cuanto desnivel tendrá el camino",
		"Escuchas el sonido de las plumas cortando el viento de un halcón en picado",
		"Un halcón que te sobrevolaba acaba de realizar un vuelo en picado hacia una presa",
		"Un hilo de fino de telaraña se te pega en el brazo",
		"Al pasar la mano por un matorral te da la sensación que algo fino se te ha pegado",
		"Una serpiente diminuta acaba de cruzarse en tu camino",
		"El brusco cambio de altura te hace coger más aire al respirar",
		"Finos hilos de telaraña cruzan el camino y al pasar se te pegan en tus pertenencias",
		"Escuchas pequeños desprendimientos a tu alrededor",
		"Pequeños roedores se cruzan en tu camino"
		}));
		break;
	}

}
                                                                               
