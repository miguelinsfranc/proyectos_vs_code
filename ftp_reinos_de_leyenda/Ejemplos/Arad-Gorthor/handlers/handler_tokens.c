// Satyr 08/11/2019 01:09
#include "../path.h"
#include <clean_up.h>

void create()
{
    mi_euid();
}

object cargar_token(string tipo, string nombre)
{
    return load_object(DIREXPLO + tipo + "/" + nombre + ".c");
}

int dar_token(object pj, string tipo, string nombre_token)
{
    int    err;
    object token;

    if (!pj || !tipo || !nombre_token) {
        return -1;
    } else if (!token = cargar_token(tipo, nombre_token)) {
        return -2;
    } else if (!token->dar_token(pj)) {
        return -3;
    }

    return 1;
}
