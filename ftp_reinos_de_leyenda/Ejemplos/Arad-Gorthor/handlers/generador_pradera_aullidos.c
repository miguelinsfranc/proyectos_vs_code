/* Zoilder 14-02-2011
* Camino a la Pradera de los Aullidos. Desvío de la Ruta Boreal

Eckol 11Oct14: quita "huargo" del add_item, para que no choque con los npcs.
*/

#include "../path.h"
#include <tiempo.h>

/*
*        Función que duerme los cachorros añadidos a la cueva si es de día
*/
void duerme_cachorro(object cachorro)
{
        if(cachorro && !CLIMA->query_noche())
                {
                        cachorro->dormir_cachorro();
                }
}

/*
*        Función que añade items, sonidos y demás elementos a las rooms para darle realismo
*        Por simplicidad, se ponen los elementos de cada habitación por separado, en lugar de mezclar los comunes, que complicaría
*        la lectura del código
*/
void anyade_elementos(object habitacion,string clave)
{
        //Se añaden items y sonidos para darle realismo a la zona
        //Inicio Camino y Zona Profunda
        switch(clave)
        {
                case "inicio_camino":
                        if(!CLIMA->query_noche()) //De día
                        {
                                habitacion->add_item(({"volcan","volcán","n'argh"}),
                                        "Ves como se erige imponente al norte, el volcán N'argh, el cual aún a la distancia a la que se encuentra, muestra su enorme 
porte.");
                                habitacion->add_sound(({"ruido","ruidos","el ruido","los ruidos","sonido","sonidos","el sonido","los sonidos"}),
                                        "Hay una ausencia casi total de cualquier tipo de sonido, a excepción de alguna ráfaga de viento o un aullido de huargo 
puntual.");
                                habitacion->add_sound(({"viento","vientos","el viento","los vientos"}),
                                        "Oyes el ruido provocado por el viento al pasar cerca de aquí.");
                                habitacion->add_sound(({"aullido","aullidos","el aullido","los aullidos","huargo","huargos","ulular","el ulular"}),
                                        "Escuchas algún que otro aullido de huargo que procede más al oeste de aquí.");
                        }
                        else //De noche
                        {
                                habitacion->add_item(({"volcan","volcán","n'argh"}),
                                        "Aún con la oscuridad reinante, ves al norte la silueta del volcán N'argh.");
                                habitacion->add_sound(({"ruido","ruidos","el ruido","los ruidos","sonido","sonidos","el sonido","los sonidos"}),
                                        "Escuchas una amalgama de sonidos de distinta índole, destacando entre ellos el sonido del rugir del viento y el de aullidos 
de huargos.");
                                habitacion->add_sound(({"viento","vientos","el viento","los vientos","rugir"}),
                                        "Oyes el rugir del viento a tu alrededor, aumentando por momentos.");
                                habitacion->add_sound(({"aullido","aullidos","el aullido","los aullidos","huargo","huargos","ulular","el ulular"}),
                                        "Escuchas innumerables aullidos provenientes de los huargos que habitan la pradera y sus alrededores.");
                        }
                        //Comunes
                        habitacion->add_item("camino","Te encuentras en un camino que sirve como nexo entre la Ruta Boreal, "
                                "que lleva al volcán N'argh; y la Pradera de los Aullidos. Está cubierto de maleza.");
                        habitacion->add_item("maleza","Cubre el camino a la Pradera de los Aullidos al completo, es lo único que se puede ver, "
                                "salvo alguna que otra huella.");
                        habitacion->add_item(({"huella","huellas","la huella","las huellas"}),
                                "Ves huellas salpicadas por algunas zonas del camino. La mayoría son de huargos, pero ves como hay alguna que otra que parece "
                                "ser de algún caballo. Seguramente el animal acabaría siendo comida de huargo.");
                        habitacion->add_item(({"ruta","boreal","ruta boreal"}),
                                "Este camino se dirige, bordeando la Meseta Oriental, hacia el norte, llegando hasta el volcán N'argh.");
                        habitacion->add_item(({"meseta","oriental","meseta oriental","la meseta","la meseta oriental"}),
                                "Al final del camino, se encuentra la impontente Meseta Oriental.");
                        habitacion->add_item(({"pradera","aullidos","pradera de los aullidos"}),
                                "El camino en el que te encuentras muere al oeste, en la denominada Pradera de los Aullidos, dominada como su nombre indica "
                                "por los aullidos de los huargos que en ella habitan.");
                        habitacion->add_item(({"cementerio","el cementerio"}),
                                "Un cementerio se erige al sur. Te parecen ver algunas sombras moviéndose.");
                        break;
                case "zona_profunda":
                        if(!CLIMA->query_noche()) //De día
                        {
  habitacion->add_item(({"suelo","túneles","tocas"}), "Junto a los huesos esparcidos, ves como se amontonan decenas de caminos y tocas, algo mucho comun en este 
tipo de vegetación.");
                                habitacion->add_item(({"volcan","volcán","n'argh"}),
                                        "Ves como se erige imponente al norte, el volcán N'argh, el cual aún a la distancia a la que se encuentra, muestra su enorme 
porte.");
                                habitacion->add_item(({"pared","paredes","escarpada","muro","muros"}),"Si te fijas con detalle, a lo largo de la ya visible pared 
de la meseta, puedes observar una serie de agujeros en la pared, pareciendo cuevas. "
                                        "Te da la sensación de que en alguna de ellas se ve una sombra moverse.");
                        }
                        else //De noche
                        {
                                habitacion->add_item(({"volcan","volcán","n'argh"}),
                                        "Aún con la oscuridad reinante, ves al norte la silueta del volcán N'argh.");
                        }
                        //Comunes
                        habitacion->add_item("camino","Te encuentras en un camino que sirve como nexo entre la Ruta Boreal, "
                                "que lleva al volcán N'argh; y la Pradera de los Aullidos. Está cubierto de maleza.");
                        habitacion->add_item("maleza","Cubre el camino a la Pradera de los Aullidos al completo, es lo único que se puede ver, "
                                "salvo alguna que otra pisada.");
                        habitacion->add_item(({"huella","huellas","la huella","las huellas"}),
                                "Ves huellas salpicadas por algunas zonas del camino. La mayoría son de huargos, pero ves como hay alguna que otra que parece "
                                "ser de algún caballo. Seguramente alguien pasó con su caballo por aquí y los huargos decidieron seguirle.");
                        habitacion->add_item(({"ruta","boreal","ruta boreal"}),
                                "Este camino se dirige, bordeando la Meseta Oriental, hacia el norte, llegando hasta el volcán N'argh.");
                        habitacion->add_item(({"pradera","aullidos","pradera de los aullidos"}),
                                "El camino en el que te encuentras muere al oeste, en la denominada Pradera de los Aullidos, dominada como su nombre indica "
                                "por los aullidos de los huargos que en ella habitan.");
                        habitacion->add_item(({"cementerio","el cementerio"}),
                                "Un cementerio se erige al sur. Te parecen ver algunas sombras moviéndose.");
                        habitacion->add_item(({"arbusto","arbustos"}), "Ves algún que otro arbusto de tamaño medio esparcidos a ambos lados del camino.");
                        habitacion->add_item(({"meseta","oriental","meseta oriental","la meseta","la meseta oriental"}),
                                "Al final del camino, se encuentra la impontente Meseta Oriental.");
                        habitacion->add_sound(({"ruido","ruidos","el ruido","los ruidos","sonido","sonidos","el sonido","los sonidos"}),
                                "Escuchas una amalgama de sonidos de distinta índole, destacando entre ellos el sonido del rugir del viento y el de aullidos de 
huargos.");
                        habitacion->add_sound(({"viento","vientos","el viento","los vientos","rugir"}),
                                "Oyes el rugir del viento a tu alrededor, aumentando por momentos.");
                        habitacion->add_sound(({"aullido","aullidos","el aullido","los aullidos","huargo","huargos","ulular","el ulular"}),
                                "Escuchas innumerables aullidos provenientes de los huargos que habitan la pradera y sus alrededores.");                        
                        break;
                case "pradera":
                        if(!CLIMA->query_noche()) //De día
                        {
                                habitacion->add_item(({"volcan","volcán","n'argh"}),
                                        "Ves como se erige imponente al norte, el volcán N'argh, el cual aún a la distancia a la que se encuentra, muestra su enorme 
porte.");
                                habitacion->add_item(({"pared","paredes","escarpada","muro","muros"}),"Si te fijas con detalle, a lo largo de la ya visible pared 
de la meseta, puedes observar una serie de agujeros en la pared, pareciendo cuevas. "
                                        "Te da la sensación de que en alguna de ellas se ve una sombra moverse.");
                        }
                        else //De noche
                        {
                                habitacion->add_item(({"volcan","volcán","n'argh"}),
                                        "Aún con la oscuridad reinante, ves al norte la silueta del volcán N'argh.");
                                habitacion->add_smell(({"olor","aire"}), "Predomina un olor a huargo mezclado con sus deposicionar bastante fuerte.");
                        }
                        //Comunes
                        habitacion->add_item(({"ruta","boreal","ruta boreal"}),
                                "Este camino se dirige, bordeando la Meseta Oriental, hacia el norte, llegando hasta el volcán N'argh.");
                        habitacion->add_item(({"arbusto","arbustos"}), "Ves unos arbustos esparcidos por toda la pradera.");
                        habitacion->add_item(({"maleza","malezas"}), "Observas como la pradera está dominada por la maleza.");
                        habitacion->add_item(({"hueso","huesos","restos"}), "Observas varios huesos diseminados por toda la pradera. No tienes forma de saber a qué 
tipo de ser pertenecen.");
                        habitacion->add_item(({"muro","pared","muros","paredes","meseta","oriental","la meseta","la meseta oriental"}), "Observas la imponente Meseta 
Oriental. Ves como su cara sudeste corta abruptamente la pradera. Te fijas en una serie de cuevas a lo largo de su superficie.");
                        habitacion->add_item(({"cueva","cuevas"}), "Observas a lo largo del muro escarpado una serie de cuevas lo suficientemente grandes para que 
puedas introducirte en ellas. Quizá puedas escalar a una y ver que hay.");
                        habitacion->add_sound(({"ruido","ruidos","el ruido","los ruidos","sonido","sonidos","el sonido","los sonidos"}),
                                "Escuchas una amalgama de sonidos de distinta índole, destacando entre ellos el sonido del rugir del viento y el de aullidos de 
huargos.");
                        habitacion->add_sound(({"viento","vientos","el viento","los vientos","rugir"}),
                                "Oyes el rugir del viento a tu alrededor, aumentando por momentos.");
                        habitacion->add_sound(({"aullido","aullidos","el aullido","los aullidos","huargo","huargos","ulular","el ulular"}),
                                "Escuchas innumerables aullidos provenientes de los huargos que habitan la pradera y sus alrededores.");                        
                        habitacion->add_smell(({"olor","aire"}), "Procedente de todo la pradera, pero en especial de las cuevas de la pared escarpada, procede un 
fuerte olor a deposiciones de huargos.");
                        break;
                case "cueva":
                        //Comunes
                        habitacion->add_item(({"hueso","huesos","restos","pila"}), 
                                "Observas varios huesos apilados al fondo de la cueva, dando la impresión de ocultar algo en su interior. Quizá puedas remover 
los huesos para ver si hay algo.");
                        habitacion->add_item(({"cueva","cuevas"}), "Observas a lo largo del muro escarpado una serie de cuevas lo suficientemente grandes para que 
puedas introducirte en ellas. Quizá puedas escalar a una y ver que hay.");
                        habitacion->add_item(({"pradera"}), "Observas desde la altura la pradera, llena de maleza y arbustos diseminados, junto a varios huesos descarnados.");
                        habitacion->add_smell(({"olor","aire"}), "El aire de la cueva huele a una mezcla de deshechos de huargo con restos de otras especies.");
                        habitacion->add_item(({"muro","pared","muros","paredes","meseta","oriental","la meseta","la meseta oriental"}), 
                                "Te encuentras en el interior de una de las cuevas que predominan por la cara sudeste de la Meseta Oriental.");
                        break;
        }
}

void inicio_camino(object habitacion, string *mon)
{
        habitacion->set_short("Pradera de los Aullidos: %^BOLD%^YELLOW%^Camino de acceso%^RESET%^");
        habitacion->set_long("Te encuentras en un desvío de la Ruta Boreal, que se dirige hacia el oeste, en dirección a la imponente Meseta Oriental. "
          "A primera vista, el camino parece bastante poco transitado, estando completamente inundado por la maleza. "
          "Hay alguna que otra huella marcada en el suelo, principalmente de huargos. "
                "A diferencia de lo que su nombre sugiere, el ambiente en esta zona se caracteriza por una casi total ausencia de cualquier tipo de ruido, "
                "solamente de vez en cuando se oye un aullido de alguno de los huargos que suelen encontrarse por la zona, sonido que te es traído "
                "por el viento. Si te fijas con atención, puedes observar como muy a lo lejos, al norte, se puede divisar el volcán N'argh, el destino "
                "al que se dirige la Ruta Boreal, bordeando desde el este la Meseta Oriental. "
                "Al sur observas como se extiende un amplio cementerio. Al oeste puedes ver la pradera situada al final del camino en el que te encuentras, "
                "pradera cortada abruptamente por la rocosa pared de roca maciza perteneciente a la zona sudeste de la Meseta Oriental.\n");
        habitacion->set_night_long("Te encuentras en un desvío de la Ruta Boreal. A diferencia del día, la noche en la zona representa perfectamente "
                "el nombre que a lo largo de los años se le ha dado a la pradera que se encuentra al final del camino que transitas, en su lado oeste, "
                "pues lo único que se llega a escuchar, son los aullidos de los huargos que en ella conviven, mezclados con el rugido del viento. "
                "Si escuchas con atención, tras los aullidos intensos que puedes escuchar, que parecen provenir de todas partes al mismo tiempo, localizas "
                "otra serie de sonidos, especialmente provenientes del este, de todo tipo de fauna nocturna que conviven en las proximidades del camino, en especial "
                "en la zona más próxima a la Ruta Boreal. Sin embargo, los aullidos, aún dándote la sensación de provenir desde cualquier parte "
                "son más fuertes desde el oeste, donde el camino en el que te encuentras muere en la Pradera de los Aullidos. "
                "Lo único que puedes ver es el camino en el que te encuentras, pues mires en la dirección que mires sólo ves oscuridad, a excepción de en "
"el norte, donde aún entre la oscuridad resalta el volcán N'argh, adormecido en mitad de la oscuridad.\n");
        if (CLIMA->query_noche()) {
                if (!CLIMA->query_nublado()) {
                        habitacion->fijar_sensaciones(({
                                "En el norte, a pesar de su lejanía, resalta imponente en la oscuridad la silueta del volcán N'argh",
                                "El sonido de los aullidos de los huargos te llegan claramente desde la pradera situada al oeste, notando como crecen en intensidad
por momentos",
                                "Sientes como eres observado desde la profunda oscuridad de tu alrededor"
                        }));
                } else {
                        habitacion->fijar_sensaciones(({
                                "Debido a la escasa luz, caminas torpemente, llegando a pisar cosas que no podrían considerarse maleza",
                                "El frío, en conjunto a los aullidos de los huargos, te hace temblar e ir con especial atención",
                                "Pisas los numerosos charcos que pueblan el camino, dificultándote el avance"
                        }));
                }
        }
        else {
                if (CLIMA->query_nublado()) {
                        habitacion->fijar_sensaciones(({
                                "Las nubes que dominan el cielo retienen la mayoría de los rayos de sol, haciendo que aún estando en pleno día, sea complicado
ver lo que hay más allá, por lo que fuerzas la vista para poder ver con más claridad",
                                "La lluvia que cae, encharca el camino, dificultándote el avance"
                        }));
                }
        }
        //Añadimos npcs
        if (!random(3))
                habitacion->add_clone(NPCS_BO+mon[random(sizeof(mon))], 1+random(3));
      habitacion->add_clone(NPCS_BO+"animales/aguila.c", 1+random(2));
}

void zona_profunda(object habitacion, string *mon)
{

        habitacion->set_short("Pradera de los Aullidos: %^BOLD%^ORANGE%^Zona Profunda del Camino%^RESET%^");
        habitacion->set_long("Te adentras en la zona más profunda del camino de acceso a la Pradera de los Aullidos. "
                "En comparación a ésta parte del camino, la zona que se encuentra más al este del mismo, "
 "parece de otra región lejana. El ambiente está dominado por extraños sonidos "
                "procedentes de todas las direcciones posibles. De entre la cantidad inmensa de sonidos, puedes distinguir aullidos de huargos, algunos "
                "incluso provienen de cerca, dándote la sensación de estar siendo vigilado. Aquí la maleza aumenta bastante en comparación a la que se puede "
                "encontrar al comienzo de este mismo camino, salpicada por algún que otro arbusto a los lados del camino. "
                "Si te fijas con atención, puedes observar como muy a lo lejos, al norte, se puede divisar el volcán N'argh, en donde muere el camino que puedes "
                "ver cómo nace al este de tu posición, conocido como la Ruta Boreal. "
                "Al sur observas como se extiende un amplio cementerio. Al oeste puedes ver la pradera situado al final del camino en el que te encuentras, "
                "cortada abruptamente por la parte sudeste de la imponente Meseta Oriental, con sus escarpadas paredes. "
      "El suelo está lleno de túneles y pequeñas tocas, muy probablemente sirben de guarida a los animales y algunos reptiles.\n");
        habitacion->set_night_long("Te adentras en la zona más profunda del camino de acceso a la Pradera de los Aullidos. "
                "El ambiente de noche es tremendamente ruidoso. A los potentes y tenebrosos aullidos, de lo que parecen ser una enorme cantidad de huargos,  "
                "se le unen otros extraños sonidos que no llegas a distinguir. La maleza aumenta bastante en comparación a la que se puede encontrar "
                "al comienzo de este mismo camino. A ambos lados del camino, se encuentra disperso algún que otro arbusto, "
                "de cuyo interior parece provenir la respiración de algún animal, al parecer vigilándote. "
                "Desde esta zona, sólo te rodea oscuridad, quedándote sólo a la vista la pradera que se sitúa justo delante de tu posición, al oeste, y "
                "la imponente silueta al norte del volcán N'arhg.\n");
        if (CLIMA->query_noche()) {
                if (!CLIMA->query_nublado()) {
                        habitacion->fijar_sensaciones(({
                                "En el norte resalta en la oscuridad la silueta del volcán N'argh",
                                "Ves como el camino en el que te encuentras muere al oeste en una pradera",
                                "El sonido de los aullidos de los huargos se hace algo insoportable",
                        }));
                } else {
                        habitacion->fijar_sensaciones(({
                                "Caminas con cuidado de no tropezar, debido a la escasa luz",
                                "Evitas unas deposiciones en el suelo, posiblemente de huargos",
                                "El olor del ambiente se hace algo insoportable, debido a la mezcla de deposiciones de huargos con el agua de los charcos que dominan
el suelo"
                        }));
                }
                if (!random(2)) /*De noche pueden haber huargos jóvenes en esta parte del camino. Si idlea un jugador aquí y no se resetea pues habrá huargos
de día, pero no merece la pena controlarlo) */
                        habitacion->add_clone(NPCS+"huargo_joven.c",random(3));
        }
        else {
                if (CLIMA->query_nublado()) {
                        habitacion->fijar_sensaciones(({
                                "Ves como sólo logra pasar algún que otro rayo de sol, quedando el resto retenidos en las nubes que pueblan el cielo",
                                "La lluvia que cae encharca el camino, dificultándote el avance"
                        }));
                }
        }
        //Añadimos npcs
    if( random(3) ) {
        if( !CLIMA->query_noche() ) {
        habitacion->add_clone(NPCS_BO+"animales/aguila.c", random(3));
             habitacion->add_clone(NPCS_BO+mon[random(sizeof(mon))], 1+random(4));
        }
    else
                habitacion->add_clone(NPCS_BO+mon[random(sizeof(mon))], 1+random(2));
    }
}

void pradera(object habitacion)
{
        habitacion->set_short("%^BOLD%^ORANGE%^Pradera de los Aullidos%^RESET%^");
        habitacion->set_long("Te encuentras en una extensa pradera al pie de la Meseta Oriental, "
"en la que observas la gran presencia de arbustos y maleza en el terreno. "
                "En la parte oeste de la misma observas la escarpada pared rocosa de dicha meseta, totalmente vertical, cortando abruptamente la pradera. "
                "Si te fijas en ella, puedes observar una serie de cuevas a lo largo de su superficie. "
                "Si miras hacia el norte puedes observar el imponente volcán de N'argh y hacia el este puedes ver la Ruta Boreal que conduce hasta él y da inicio "
                "al camino que lleva hasta tu posición actual. "
                "Fijándote a lo largo de la pradera, puedes observar varios huesos diseminados, de los cuales algunos tienen aún restos de carne fresca. "
                "El ambiente se caracteriza por los aullidos de los huargos, que dominan toda la zona, "
    "aullidos que parecen provenir de todas direcciones al mismo tiempo.\n");
        habitacion->set_night_long("Te encuentras en una pradera en el que lo primero que destaca es la gran cantidad de arbustos que observas a tu alrededor. "
                "El ambiente está dominado al completo por aullidos, que parecen miles, de huargos en todas las direcciones posibles. Si miras fijamente a tu "
                "alrededor, dentro del escaso ángulo de visión que te permite la oscuridad, puedes ver una serie de figuras moverse rápidamente en la oscuridad, "
                "dejando escapar de vez en cuando un potente aullido. La profunda oscuridad hace que no puedas ver mucho más allá de un palmo a tu alrededor, pero "
                "suficiente para ver la gran cantidad de huesos diseminados por el suelo.\n");
        if (CLIMA->query_noche()) {
                if (!CLIMA->query_nublado()) {
                        habitacion->fijar_sensaciones(({
                                "Observas varias figuras moverse a tu alrededor, en círculos y no puedes evitar que un espasmo recorra tu piel",
                                "Oyes pisadas de la maleza a tu alrededor",
                                "Sientes cómo eres observado desde la profunda oscuridad por varios ojos rojos",
                                "Te parece sentir el aliento de algún ser en tu nuca",
                                "Pisas un hueso que casi te hace caer"
                        }));
                } else {
                        habitacion->fijar_sensaciones(({
                                "Caminas lentamente debido a la escasa visibilidad para evitar chocarte con algo no deseado",
                                "Oyes el chapotear del agua de lluvia y pisadas ajenas en los charcos que se han formado",
                                "Oyes ruidos a tu alrededor, pero no alcanzas a ver nada",
                                "Escuchas multitud de aullidos justo detrás de tu posición"
                        }));
                }
                if (!random(2)) //De noche pueden haber huargos jóvenes
                        habitacion->add_clone(NPCS+"huargo_joven.c",random(3));
                if(!random(3)) //Puede llegar a haber un huargo endemoniado
                        habitacion->add_clone(NPCS+"huargo_endemoniado.c",1);
        }
        else {
                if (CLIMA->query_nublado()) {
                        habitacion->fijar_sensaciones(({
                                "Las nubes retienen gran parte de los rayos solares, dándole a la pradera una apariencia fantasmal",
                                "La lluvia que cae, forma varios charcos a lo largo de la pradera",
                                "Pisas un hueso y te desequilibras un momento"
                        }));
                }
        }
}

void cueva(object habitacion)
{
        habitacion->set_short("Pradera de los Aullidos: Cueva de Huargo");
        habitacion->set_long("Te encuentras en el interior de una de las cuevas existente en la Pradera de los Aullidos. "
                "El interior está dominado por un olor muy potente que mezcla deposiciones de huargo junto al de los restos que les sirven de alimento. "
                "Puedes ver como hay varios huesos diseminados por la estancia.\n");
        habitacion->fijar_sensaciones(({
                        "Sin darte cuenta pisas un hueso del suelo que casi te hace caer",
                        "Oyes los aullidos procedentes del exterior de la cueva creciendo en intensidad"
        }));
        habitacion->fijar_luz(50); //Habrá menos luz en su interior, como es lógico
        habitacion->fijar_altura(5);
        /*habitacion->add_property("no_clean_up", 1); //Si hubiera algún abuso en conseguir cachorros y no se quiere limitar el random del add_clone, activar esto
para que no se descargue la room de memoria
*/
        if(!random(2)) //Habrá un 50% de posibilidades de que salga un cachorro
        {
                habitacion->add_clone(NPCS+"huargo_cachorro.c",1,(:duerme_cachorro:));
                CLIMA->anyadir_event_room(habitacion); //Se le añade gestión del clima a la room si se añade un cachorro
        }
}

varargs void describir(object habitacion, string tipo, int tiempo_reset)
{
        string * mon;
        mon = ({"hormiga_roja","serpiente", "escorpion", "araña_enorme"});
        habitacion->set_exit_color("amarillo_flojo");
        habitacion->fijar_luz(100);
        habitacion->fijar_frecuencia_sensaciones(33);
        switch (tipo)
        {
                case "inicio_camino":
                        inicio_camino(habitacion,mon);
                        break;
                case "zona_profunda":
                        zona_profunda(habitacion,mon);
                        break;
                case "pradera":
                        pradera(habitacion);
                        break;
                case "cueva":
                        cueva(habitacion);
                        break;
                default:
                        habitacion->set_short("Pradera de los Aullidos: Perdido");
                        habitacion->set_long("Te encuentras perdido en la Pradera de los Aullidos.");
                        habitacion->set_night_long("Te encuentras perdido en la Pradera de los Aullidos y es de noche y tienes miedo.");
        }
        //Se añaden elementos para dar realismo
        anyade_elementos(habitacion,tipo);
        //Si se indica un tiempo para el reset
        if(tiempo_reset>0)
                set_reset(habitacion,tiempo_reset);
}
