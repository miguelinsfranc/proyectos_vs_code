// Nohur 26/01/2019 - Handler para la zona "Ruta Boreal"
// Altaresh 19, retomo el trabajo

#include <tiempo.h>
#include "../path.h"
inherit GENERADOR_BASE;


void poner_short(object sala, string zona, string subzona)
{
    switch (zona) {
        case "ruta_boreal":
            zona = "Ruta Boreal";
            break;
    }
    switch (subzona) {
        case "cordillera":
            subzona = ": %^BOLD%^BLACK%^Sendero hacia la Cordillera de Bûrzum%^RESET%^";
            break;
		case "puerto":
            subzona = ": Proximidades del Puerto de Golthur%^RESET%^";
            break;
        default:
            subzona = "";
            break;
    }

    sala->set_short(zona + subzona);
}

void poner_fronteras(object sala, string zona, string subzona) {
	sala->fijar_fronteras(({0, 0, 0, "Golthur", "Ruta Boreal", "Caminos"}));  
}
void poner_color_salidas(object sala, string zona, string subzona) {
     sala->set_exit_color("cian");
}
void poner_npcs(object sala, string zona, string subzona) {
	switch (subzona) {
        case "cordillera":
            if (!random(3)) sala->add_clone(BMONSTRUOS+"raton",random(2));
            break;
		case "puerto":
            sala->add_clone(BANIMALES+"gaviota",random(2));
            break;
    }
}
void poner_especiales(object sala, string zona, string subzona) {
	switch (subzona) {
        case "cordillera":
            sala->fijar_clima("golthur");
			sala->set_night_long("La oscura y silenciosa noche que te rodea, sólo se ve interumpida por el"
								 " chocar de las olas contra los acantilados cercanos. Algunos roedores aprovechan "
								 "esta oscuridad para salir de sus madrigueras y comer lo poco que pueden encontrar.\n");
			sala->fijar_frecuencia_sensaciones(33);
			if (CLIMA->query_noche()) {
				sala->fijar_sensaciones(({
					"El salitre inunda tus pulmones de vigor y energía con su fuerte y salado olor",
					"Los pequeños pasos de un pequeño roedor se acrecentan por momentos",
					"Unas olas rompen fuertemente contra los acantilados, creando un enorme estruendo"
				  }));
			} else {
				sala->fijar_sensaciones(({
					"Con frecuencia ves a las gaviotas que sobrevuelan la zona costera en busca de alimentos",
					"Escuchas algún graznido provenientes de las gaviotas que sobrevuelan los acantilados cercanos",
					"La proximidad de la costa se percibe en la abundante vegetación a ambos lados de la vía"
				  }));
			}
            break;
		case "puerto":
            sala->fijar_clima("mar_de_los_dioses");
             sala->set_night_long("La noche templada por la proximidad de la costa augura una próspera pesca en la "
								  "madrugada, lo que hace aún más ruidosos los cánticos provenientes de las tabernas en el Puerto de Golthur. "
								  "El camino, regular y en constante mantenimiento por esta zona, se muestra perfectamente liso y perfecto "
								  "para ser cruzado por carros, orcos o animales de carga.\n");
			sala->fijar_frecuencia_sensaciones(33);
			if (CLIMA->query_noche()) {
				sala->fijar_sensaciones(({
					"El salitre inunda tus pulmones de vigor y energía con su fuerte y salado olor",
					"Las tabernas resuenan ruidosas, seguramente otra confrontación entre orcos y goblins",
					"Las luces de la zona portuaria al norte sirven como faro en tu camino"
				  }));
			} else {
				sala->fijar_sensaciones(({
					"Con frecuencia ves a las gaviotas que sobrevuelan la zona costera en busca de alimentos",
					"Los ruidos de profuso trabajo en el muelle llegan claramente traídos por la brisa marina",
					"La proximidad de la costa se percibe en la abundante vegetación a ambos lados de la vía"
				  }));
			}
            break;
    }
}

