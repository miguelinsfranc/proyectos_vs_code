// Satyr 10.11.2016
/**
 * Este handler aglutina varias funciones útiles para toda Eradia por motivos de rendimiento.
 *
 * Por norma general, habrá una macro eh el path para usarlas.
 */

#include "../path.h"
#include <clean_up.h>
#include <tiempo.h>
#include <depuracion.h>

void create() {
    mi_euid();
}
// Devuelve el nombre de la estación
varargs string dame_estacion(object room) {
    switch(CLIMA->nombre_estacion(room)) {
        case PRIMAVERA: return "dones";
        case VERANO   : return "sacrificio";
        case OTONYO   : return "lluvias";
        case INVIERNO : return "invierno";
        default       : return CLIMA->nombre_estacion(room) + "";
    }
}
// Dada una zona y subzona devuelve el fichero de descripciones
string buscar_descripciones(object sala, string zona, string subzona) {
    string tx, ruta, noche, estacion;

    if ( file_size(ruta = DESCRIPCIONES + zona) != -2 )
        return 0;

    noche    = CLIMA->query_noche() ? "noche" : 0;
    estacion = dame_estacion(sala);

    if ( noche && file_size(tx = ruta + "/" + subzona + "-" + estacion + "-" + noche + ".txt") > 0 )
        return tx;

    if ( noche && file_size(tx = ruta + "/" + subzona + "-" + noche + "-" + estacion + ".txt") > 0 )
        return tx;

    if ( noche && file_size(tx = ruta + "/" + subzona + "-" + estacion + ".txt") > 0 )
        return tx;

    if ( noche && file_size(tx = ruta + "/" + subzona + "-" + noche + ".txt") > 0 )
        return tx;

    if ( file_size(tx = ruta + "/" + subzona + "-" + estacion + ".txt") > 0 )
        return tx;

    return DESCRIPCIONES + zona + "/" + subzona + ".txt";
}
// Dada una ruta, intenta extraer la zona y la subzona
// El equivalente a la posición "ciudad" y "zona" de una frontera que se intentará averiguar
// en base a un directorio
string *dame_zonas_ruta(string ruta) {
    string zona, subzona;

    // zona/subzona/sala
    if ( 2 == sscanf(ruta, ROOMS + "%s/%s/%*s", zona, subzona) )
        return ({zona, subzona});

    // zona/subzona_x
    if ( 3 == sscanf(ruta, ROOMS + "%s/%([a-zA-Z_-]*[a-zA-Z])_%*d", zona, subzona) )
        return ({zona, subzona});

    // zona/subzona
    if ( 2 == sscanf(ruta, ROOMS + "%s/%([a-zA-Z_-]*[a-zA-Z])", zona, subzona) )
        return ({zona, subzona});

    return 0;
}
// Como dame_zonas_ruta, pero usa una sala
string *dame_zonas_sala(object sala) {
    return dame_zonas_ruta(base_name(sala));
}
// Usa el estándar de generadores de eradia para generar una sala
void autogenerar_sala(object sala) {
    string *slices;
	
    if ( ! sizeof(slices = dame_zonas_sala(sala)) ){
		__debug_x(sprintf("autogenerar " + base_name() + ": " + slices[0] + "|" + slices[1]),"arad-gorthor"); 
        return;
	}
    //DEBUG("autogenerar " + base_name() + ": " + slices[0] + "|" + slices[1]);
	
    GENERAR_X(sala, slices[0], slices[1]);
}
