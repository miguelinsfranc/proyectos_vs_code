// Rezzah 10-11-2009

#include "../path.h"

void iniciar(object habitacion, string tipo)
{
	habitacion->fijar_frecuencia_sensaciones(40);
	habitacion->set_zone(tipo);
	switch (tipo)
	{
		case "camino":
			habitacion->set_short("%^ORANGE%^Camino Subterráneo%^RESET%^");
			habitacion->set_long("La estrecha senda por la que caminas se hunde en las entrañas de la Montaña del Jabalí."
			" No puedes identificar que fuerzas han horadado la tierra de esta manera, pero parece una hendidura natural "
			"que profundiza en la tierra o, mejor dicho, como si emergiera hacia el exterior.\n");
			habitacion->set_exit_color("amarillo_flojo");
			habitacion->fijar_luz(40);
			habitacion->add_item(({"suelo","pared","paredes", "roca", "rocas"}), "Tras un examen exhaustivo adviertes dos detalles inusuales, "
			"la roca está extrañamente caliente y las formas redondeadas y suaves de la roca son reflejo de una gran erosión.");
			habitacion->fijar_sensaciones(({"El calor es más palpable en el interior de la gruta que hacia el exterior", 
			"Las redondeadas y suaves formas de la roca erosionada evitan que te hagas daño al resbalar inocentemente"}));
			break;
		case "poblado":
			habitacion->set_short("%^GREEN%^Gran Caverna%^RESET%^");
			habitacion->set_long("La altura de la espaciosa caverna en la que te has adentrado es descomunal, la luz que emana "
			"las antorchas dispuestas por las paredes y diferentes postes de la caverna apenas llegan a iluminar las zonas más "
			"bajas del techo. Por todos lados se ven restos de actividad.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(50);
			habitacion->add_item(({"antorcha","antorchas"}), "Estacas sin llamas brillan con luz propia, invocada mágicamente, "
			"en distintos lugares repartidos por toda la caverna, en el perímetro colgando de las paredes y en el interior en "
			"postes de piedra edificados a tal efecto.");
			habitacion->add_item(({"postes","poste"}), "Un grupo de piedras bien seleccionadas están amontonadas formando una "
			"pequeña torre, de cerca de un metro de altura, en cuya cúspide hay encajada una antorcha.");
			habitacion->fijar_sensaciones(({"Sientes una inquietante presencia próxima a tí, que te observa", "Te parece ver "
			"por el rabillo del ojo una figura esconderse de tu vista", "Un escalofrío recorre tu espalda paralizándote por "
			"un segundo"}));
			break;
		case "camino_oculto":
			habitacion->set_short("%^RED%^BOLD%^Camino Oculto%^RESET%^");
			habitacion->set_long("Las paredes de este estrecho camino palpitan con una vida rojiza, brillante, que permite avanzar "
			"sin tropiezos aunque la luz que emanan apenas te permite ver más allá del alcance de tu mano. El camino crece hacia el "
			"sur desviándose ligeramente hacia el este donde la oscuridad es aún mayor.\n");
			habitacion->add_item(({"pared","paredes"}), "Las paredes relucen como si untaras con sangre un escudo bronce y jugaras a "
			"reflejar la luz del sol. En tonos rojizos, anaranjados y tostados ves diferentes vetas que dibujan imágenes sin sentido "
			"sobre la roca.");
			habitacion->add_item(({"imagen","imagenes","tono","tonos"}), "Si te fijas con detenimiento en las imágenes éstas parecen "
			"fluctuar, como si estuvieras en el estómago de un dragón y vieras la sangre recorrer las venas que te rodean, una sensación"
			" muy inquietante pero muy adecuada si tenemos en cuenta el calor que desprenden las paredes.");
			if (random(4)) habitacion->add_clone(BMONSTRUOS+"araña_enorme.c", random(4)+2);
			habitacion->set_exit_color("rojo");
			habitacion->fijar_luz(30);
			habitacion->fijar_sensaciones(({"Sientes un soplo de aire caliente proveniente del sureste que casi abrasa tu piel"}));
			break;
		case "portal":
			habitacion->set_short("%^CYAN%^BOLD%^Sala del Portal%^RESET%^");
			habitacion->set_long("Estás en una habitación cuadrada de un tamaño impresionante, la intensa luz que inunda todo el espacio "
			"proviene de una llama azulada situada en el rincón noreste tan grande como un carromato. Toda la habitación tiene las paredes "
			"marcadas con infinitas inscripciones metálicas que parecen nacer de la misma piedra y que reflejan continuamente el brillo "
			"azur del portal.\n");	
			habitacion->add_item(({"peculiar","marca","techo"}), "Hay una marca especial en el techo de la estancia, está un poco lejos, pero "
			"aguzando la vista podrías conseguir descifrarlo.");
			habitacion->add_item(({"pared","paredes","runa","runas","marcas","metal","inscripcion","inscripciones","metalicas", "metálicas"}), "Podrías tardar "
			"una vida completa de un enano en analizar todo el texto que hay inscrito en las paredes. Parece que el autor talló la forma precisa "
			"de cada símbolo en la pared y luego volcó plata líquida o un metal similar en cada hendidura. No sabes como lo hizo para que no "
			"gotearan pero tan finamente han sido grabadas que eso es lo que parece. En el techo una marca peculiar carece de metal.");
			habitacion->set_exit_color("verde");
			habitacion->fijar_luz(70);
			break;
		case "camino_caverna":
			habitacion->set_short("%^BLACK%^BOLD%^Camino Subterráneo%^RESET%^");
			//La room 26 tiene el portal de regreso, similar al de ida, teleporta a las afueras, al poblado goblin (room 10), hay que
			//ponerlo añadido al set_long y con add_item y add_action
            habitacion->set_long("Te encuentras en un pequeño pasillo de piedra similar a los que has cruzado con anterioridad, en sus "
			"paredes la misma piedra de aspecto suave y redondeado emana aún más calor si cabe. Al menos su brillo rojizo te permite vislumbrar "
			"el camino.\n");
            habitacion->set_exit_color("blanco_flojo");
            habitacion->fijar_luz(30);
			habitacion->fijar_sensaciones(({"Tu corazón late acelerado por la certeza de que hay un ser de poder considerable en las proximidades",
			"Un sombra fugaz atraviesa el techo a tal velocidad que te es imposible identificarla.", "El mal flota en el aire como un humo espeso que "
			"entra por tu boca y tu nariz inundándote"}));
			//La room 29 tiene un dragón de sombras, está oculto, si alguien busca aparece y sonríe, no es agresivo, se puede hablar con él para que
			//abra el acceso al laberinto, sino se habla con él pero se abre igualmente (room 28) impide cruzar la puerta (hay que matarlo o sobornarlo)
            break;
		case "laberinto":
			habitacion->set_short("%^MAGENTA%^BOLD%^Laberinto de Cavernas%^RESET%^");
			switch (random(3)) {
			case 1:
	            habitacion->set_long("Las paredes lisas, talladas por manos de verdaderos maestros de la piedra, son uniformes, no presentan ninguna "
				"variación que permita distinguir una pared de otra, una estancia de otra, resultando muy complicado orientarse en el complejo entramado "
				"de pasillos, vueltas y giros que conforman los laberínticos túneles de la caverna.\n");
				break;
			case 2:
				habitacion->set_long("No estás seguro de si estás andando cuesta arriba o cuesta abajo, lo que está claro es que resulta agotador caminar "
				"por estos interminables pasillos, idénticos entre sí, que parecen no llevar a ninguna parte. Cada recodo te resulta familiar a estas alturas"
				" y podrías jurar, o no, que estás al principio otra vez, o quizá en el extremo opuesto, cerca de la salida.\n");
				break;
			default:
				habitacion->set_long("Debes haber errado el camino por completo, te alejas de tu objetivo, las paredes parecen combarse sobre tí, te asfixian "
				"esos pasillos interminablemente repetitivos, sin aparente orden, ni sentido, ni dirección, estás en el corazón de un infierno de piedra.\n");
				break;
			}
			habitacion->fijar_sensaciones(({"Parece que este pasillo ya lo has recorrido con anterioridad. ¿Has dado la vuelta sin darte cuenta?",
			"Cuando crees que estás llegando a la salida algo te dice que te has equivocado por completo en tu camino",
			"Crees...crees que ya has pasado por aquí, al menos un par de veces.", "Una ráfaga de aire fresco parece indicarte la proximidad de una salida"}));
			habitacion->add_item("pasillo","Un interminable pasillo de piedra con un cruce al final, ¿quién sabe a dónde llevará?");
			habitacion->add_item("piedra", "Piedra, piedra, y más piedra, gris, extrañamente cálida, pero lisa e uniforme, igual en todos y cada uno de los malditos "
			"puntos de este infernal laberinto.");
			habitacion->add_item(({"pared","paredes"}), "Como mudos testigos de tu desesperación las paredes se yerguen altas, lisas y grises como la piedra que las "
			"forma por encima de tu cabeza, con un punto de sarcasmo... ¡¿Se están riendo de tí?!");
			habitacion->add_item(({"tunel","tuneles","túnel","túneles"}), "Son largos y agotadores, imposible de distinguir uno de otro, y parecen no acabarse nunca.");
			habitacion->add_item("recodo", "Quizá tengas suerte y el siguiente recodo del camino sea el último, solo quizá.");
			habitacion->add_item("salida", "Por más que miras no ves la salida por ninguna parte.");
			
            habitacion->set_exit_color("azul");
            habitacion->fijar_luz(50);
			if (!random(3)) habitacion->add_clone(NPCS+"golem_de_lava.c", 1+random(10)/9);
			if (random(2)) habitacion->add_clone(BMONSTRUOS+"cucaracha.c", 1+random(4));
			else if (random(2)) habitacion->add_clone(BMONSTRUOS+"araña_enorme.c", 1+random(3));
			else if (random(2)) habitacion->add_clone(BMONSTRUOS+"muertos_vivientes/esqueleto_luchador.c", 1+random(2));
            break;
	    case "camino_conclave":
			habitacion->set_short("%^RED%^BOLD%^Camino hacia el Cónclave%^RESET%^");
            habitacion->set_long("Tras la pesadilla pétrea que has dejado a tus espaldas el tono anaranjado de las paredes, a pesar de su inquietante fluctuación, resulta "
			"algo reconfortante. La roca es similar a la que dejaste hace tiempo atrás pero algo diferente menos redondeada, como si una explosión de fuego hubiera sacudido "
			"el corazón de la Montaña del Jabalí y te hallaras peligrosamente cerca del epicentro.\n");
            habitacion->set_exit_color("rojo_flojo");
            habitacion->fijar_luz(30);
			habitacion->fijar_sensaciones(({"La piedra parece latir con vida propia, los colores fluctúan por ella como sangre recorriendo las venas de un enorme ser",
			"Siente la presión de toneladas de roca por encima de tu cabeza", "El camino es un contínuo descenso a 'nadie sabe donde'"}));
            break;
		case "conclave":
			habitacion->set_short("%^BLACK%^BOLD%^Cónclave de Lava%^RESET%^");
			//Una de las rooms tiene la estatua de todos los chamanes alrededor de la prisión de Glach'Davir (la 89) y otra el pozo de lava (90) cambiar set_long
			//Toda la zona emana gases tóxicos
            habitacion->set_long("Te encuentras en una zona completamente sepultada por la lava, enfríada casi instantáneamente y que debe ser la causante de la erosión "
			"con sentido de extrusión que has visto en los pasillos que bajaban hasta aquí, el calor es infernal y agotador y hay pequeños charcos con magma repartidos por "
			"el suelo, así que es necesario andar con mucho cuidado.\n");
            habitacion->set_exit_color("rojo");
            habitacion->fijar_luz(30);
			habitacion->add_item(({"charco","magma", "lava"}), "Como si se tratara del mismo infierno pequeños charcos de magma aún hirviente escupen gases tóxicos infestando de muerte "
			"la caverna.");
			habitacion->fijar_sensaciones(({"El calor abrasador hace insoportable permanecer por mucho más tiempo aquí"}));
            break;
		default://Espero que no lleguen a esta parte nunca XD
			habitacion->set_short("%^BLACK%^BOLD%^Camino Apartado%^RESET%^");
            habitacion->set_long("Te has introducido en la maraña de cavernas tan profundamente que has perdido por completo la noción de dónde te encuentras.\n");
            habitacion->set_exit_color("blanco_flojo");
            habitacion->fijar_luz(30);
            break;
	}
}
