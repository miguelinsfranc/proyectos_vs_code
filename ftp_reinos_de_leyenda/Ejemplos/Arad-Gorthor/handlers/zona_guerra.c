// Nohur 26/01/2019 - Handler para la zona "zona de guerra"
// Altaresh, se retoma el trabajo
#include <tiempo.h>
#include <conquistas.h>
#include <organizaciones.h>
#include <depuracion.h>

#include "../path.h"
inherit GENERADOR_BASE;

#define RANDOM_GUARDIAS 2
#define RANDOM_APARACION_GUARDIAS 4

void fijar_npcs_organizacion(object sala);

void poner_short(object sala, string zona, string subzona)
{
    switch (subzona) {
        case "malos":
            subzona = ": %^BOLD%^BLACK%^Zona en Conflicto Occidental%^RESET%^";
			zona = "Ruta Boreal";
            break;
		case "buenos":
            subzona = ": %^BOLD%^Zona en Conflicto Oriental%^RESET%^";
			zona = "Ruta Boreal";
            break;
		case "cabanya":
            subzona = ": Cabaña de %^BOLD%^Mando%^RESET%^";
			zona = "%^BOLD%^RED%^Campamento Boreal%^RESET%^";
            break;
		case "entrada":
            subzona = ": Entrada al Campamento";
			zona = "%^BOLD%^RED%^Campamento Boreal%^RESET%^";
            break;
		case "empalizada":
            subzona = ": Empalizada";
			zona = "%^BOLD%^RED%^Campamento Boreal%^RESET%^";
            break;
		case "campamento":
            subzona = ": Interior";
			zona = "%^BOLD%^RED%^Campamento Boreal%^RESET%^";
            break;
        default:
            subzona = "";
            break;
    }

    sala->set_short(zona+subzona);
}

void poner_fronteras(object sala, string zona, string subzona) {
	switch (subzona) {
        case "malos":
			sala->fijar_fronteras(({0, 0, 0, "Golthur", "Zona de Guerra", "Alrededores"}));  
            break;
		case "buenos":
			sala->fijar_fronteras(({0, 0, 0, "Golthur", "Zona de Guerra", "Alrededores"}));  
            break;
        default:
            sala->fijar_fronteras(({0, 0, 0, "Golthur", "Zona de Guerra", "Campamento Boreal"}));  
            break;
    } 
}
void poner_color_salidas(object sala, string zona, string subzona) {
	switch (subzona) {
        case "malos":
			sala->set_exit_color("rojo");
            break;
		case "buenos":
			sala->set_exit_color("cian"); 
            break;
        default:
            sala->set_exit_color("amarillo"); 
            break;
    } 
	
}
void poner_items(object sala, string zona, string subzona) {
	 
	 mapping objetos_subzonas;

    /**
     * Antes había un if (!random(4)) en cada switch.
     *
     * Poniéndolo aquí y abortando conseguimos lo mismo y ganamos:
     *
     * - Legibilidad (el código no se va tan a la derecha)
     * - Facilidad de entender (no hay que ver randoms anidados)
     */
    if (random(4)) {
        return;
    }

    /**
     * En lugar de mantener un switch complicado con varios randoms para cada
     * cosa, mantengo un único mapping donde cada key es la subzona (p.ej.: 
     * "malos", "buenos", etc.) y el valor de cada key es un array que contiene
     * los elementos que puede haber en esa zona.
     */
    objetos_subzonas = ([
        "malos": ({
            OBJETOS + "catapulta",
            OBJETOS + "onagro",
            OBJETOS + "ariete",
            OBJETOS + "trabuquete"
        }),
        "buenos": ({
            OBJETOS + "balista",
            OBJETOS + "cañon",
        }),
        "campamento": ({
            OBJETOS + "restos",
            OBJETOS + "escombros"
        })
    ]);

    // Como son iguales las subonas, evito repetirlo
    objetos_subzonas["empalizada"] = objetos_subzonas["campamento"];

    // Chequeo de que la subzona es correcta
    if ( -1 == member_array(subzona, keys(objetos_subzonas)) ) {
        return;
    }

    // objetos_subzonas[subzonas] es un array de objetos
    // sabiendo eso, podemos usar element_of para sacarlo listo
    sala->add_clone(element_of(objetos_subzonas[subzona]), 1);


/* master class de Satyr dejo lo viejo para acordarme de la diferencia

	switch(subzona){
		case "malos":	
						switch (random(4)) {
							case 3:
								sala->add_clone(OBJETOS+"trabuquete", 1);
								break;
							case 2:
								sala->add_clone(OBJETOS+"ariete", 1);
								break;
							case 1:
								sala->add_clone(OBJETOS+"onagro", 1);
								break;
							case 0:
								sala->add_clone(OBJETOS+"catapulta", 1);
								break;
							}
		break;
		case "buenos":
			switch (random(2))
			{
				case 1:
					sala->add_clone(OBJETOS+"balista", 1);
				break;
				case 0:
					sala->add_clone(OBJETOS+"cañon", 1);
				break;
			}
						
		break;
		case "campamento":
		case "empalizada": 
			switch (random(2)) 
			{
				case 1:
					sala->add_clone(OBJETOS+"restos", 1);
								
				break;
				case 0:
					sala->add_clone(OBJETOS+"escombros", 1);
								
				break;
			}
						
		break;
	}
}
*/
}
void poner_npcs(object sala, string zona, string subzona) {
	switch (subzona)
	{
		case "buenos":  
			if (!random(5)){
				sala->add_clone(BMONSTRUOS+"animales/buitre", random(3));
			}
			if (!random(5)){
				sala->add_clone(BMONSTRUOS+"rata", 1);
			}
			fijar_npcs_organizacion(sala);
			break;
		case "malos": 
			if (!random(5)){
				sala->add_clone(BMONSTRUOS+"cuervo", random(3));
			}
			if (!random(5)){
				sala->add_clone(BMONSTRUOS+"rata", 1);
			}
			fijar_npcs_organizacion(sala);
			break;
	    case "entrada":
			//sala->add_clone(NPCS+"/zona_guerra/guardia_bueno", 2);
			//sala->add_clone(NPCS+"/zona_guerra/clerigo_bueno", 1);
			
			break;
		case "cabanya":
			sala->add_clone(NPCS+"/zona_guerra/mando_sacerdote", 1);
			sala->add_clone(NPCS+"/zona_guerra/mando_soldado", 1);
			break;
	    case "campamento":
		case "empalizada":
			fijar_npcs_organizacion(sala);
			break;
	}
}


void poner_especiales(object sala, string zona, string subzona) {
	switch (subzona) {
        case "buenos":
            sala->fijar_clima("eldor");
			sala->fijar_frecuencia_sensaciones(33);
			if (CLIMA->query_noche()) {
				sala->fijar_sensaciones(({
					"Debes andar con cuidado entre los cráteres producidos por las armas de asedio para no caerte",
					"El silencio de la zona de noche te parece inquietante",
					"Te sorprende la cantidad de armas de asedio destruidas de la zona"
					"La luz de las lunas se refleja en las protecciones de armas de asedio, dándoles brillos inquietantes",
					"Escuchas el corretear de pequeños roedores bajo los restos de un cañón",
					"El aire que proviene del mar es bastante intenso en la zona"					
				  }));
			} else {
				sala->fijar_sensaciones(({
					"El sol se refleja en algunos momentos en las protecciones de las armas de asedio cegándote",
					"Escuchas algún aleteo proveniente de los cuervos de la zona",
					"Te sorprende la cantidad de armas de asedio destruidas de la zona",
					"Esquivas una gran roca que está dentro de un crater, sin duda munición de una gran catapulta",
					"Ves un cañón que ha sido partido por la mitad por la munición de una catapulta",
					"Ves símbolos recurrentes en las armas de la zona, todas parecen portar un caballo",
					"Una rata se cruza en tu camino proveniente de los bajos de un arma de asedio"
				  }));
			}
            break;
		case "malos":
            sala->fijar_clima("golthur");
			sala->fijar_frecuencia_sensaciones(33);
			if (CLIMA->query_noche()) {
				sala->fijar_sensaciones(({
					"El salitre inunda tus pulmones de vigor y energía con su fuerte y salado olor",
					"Los pequeños pasos de un pequeño roedor se acrecentan por momentos",
					"Unas olas rompen fuertemente contra los acantilados, creando un enorme estruendo",
					"Te fijas en pequeñas guaridas que han creado los roedores de la zona en las máquinas de asedio",
					"Varios cuervos están posados en la cazoleta de una catapulta",
				  }));
			} else {
				sala->fijar_sensaciones(({
					"Con frecuencia ves a las gaviotas que sobrevuelan la zona costera en busca de alimentos",
					"Escuchas algún graznido provenientes de las gaviotas que sobrevuelan los acantilados cercanos",
					"La proximidad de la costa se percibe en la abundante vegetación a ambos lados de la vía"
				  }));
			}
            break;
			case "empalizada":
			case "campamento":
			case "entrada":
            sala->fijar_clima("golthur");
			sala->fijar_frecuencia_sensaciones(33);
			if (CLIMA->query_noche()) {
				sala->fijar_sensaciones(({
					"Un olor a muerte inunda tus fosas nasales al que no te acabas de acostumbrar.",
					"Sonidos de pequeños animales te sobresaltan en la noche",
					"Se acaba de desprender uno de los postes que sujetaban la puerta",
					"Varias flechas se encuentran clavadas en los alrededores de la entrada",
					"Escuchas como la madera de la empalizada cruje cuando el aire la golpea",
					"La noche convierte el campamento en un lugar tétrico",
					"Tropiezas con uno de los restos de armadura del suelo", 
					"Caminas con cuidado para no tropezarte con algún resto"
				  }));
			} else {
				sala->fijar_sensaciones(({
					"Ves la desolación del campamento por todos los sitios",
					"Mires donde mires ves destrucción y restos de una cruenta batalla",
					"Una rata parece tener su madriguera debajo de un resto de armadura",
					"Ves en el cielo varios buitres sobrevolando en círculos la zona",
					"El graznido de un cuervo te sobresalta",
					"Varios cuervos están posados en el borde de los restos de empalizada y no te quitan la vista de encima",
					"Rachas de aire levantan grandes polvaredas en la zona",
					"Escuchas como la madera de la empalizada cruje cuando el aire la golpea",
					"Con un fuerte estruendo, uno de los troncos de la empalizada cae contra el suelo",
					"El fuerte aire de la zona hace estremecerse los restos de la empalizada"
				  }));
			}
            break;
    }
}
void fijar_npcs_organizacion(object sala)
{
	string organizacion = DAME_CONQUISTADOR_ZONA("boreal");
	string bando = ORG_BANDO(organizacion);
	
	__debug_x(sprintf("organizacion: %s, bando: %s\n",organizacion,bando),"arad-gorthor");
	
	if (!bando)
	{
		switch (random(3))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone(GUARDIAS+"mercenario_caballero.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone(GUARDIAS+"mercenario_sacerdote.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 2:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone(GUARDIAS+"mercenario_soldado.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
			
		return;
	}
	switch (organizacion)
	{
		// BUENOS
		case "eldor":
			switch (random(3))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/eldor/npcs/guardias/patrullero_lancero.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/eldor/npcs/guardias/patrullero_monje.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 2:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/eldor/npcs/guardias/patrullero_soldado.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		case "takome":
			switch (random(3))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/takome/npcs/cruzada/cruzado_templario.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
					sala->add_clone("/d/takome/npcs/cruzada/cruzado_sacerdote.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 2:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/takome/npcs/cruzada/cruzado_soldado.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		case "thorin":
			switch (random(3))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/takome/npcs/thorin/ranger.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/baseobs/monstruos/ent.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 2:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/takome/npcs/thorin/druida.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		case "poldarn":
			switch (random(2))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/takome/npcs/poldarn/aprendiz_caballero.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/takome/npcs/poldarn/guardia_templario.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		case "kattak":
			switch (random(2))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/kheleb/npcs/clerigo_alianza.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){ sala->add_clone("/d/kheleb/npcs/soldado_alianza.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		case "kheleb":
		
			if (!random(RANDOM_APARACION_GUARDIAS)){
				sala->add_clone("/d/kheleb/npcs/khazad_dum.c", random(RANDOM_GUARDIAS));
			}				
		break;
		case "eloras":
		
			if (!random(RANDOM_APARACION_GUARDIAS)){
				sala->add_clone("/d/anduar/npcs/miliciano.c", random(RANDOM_GUARDIAS));
			}
		break;
		case "ak'anon":
		
			if (!random(RANDOM_APARACION_GUARDIAS)){
				sala->add_clone("/d/urlom/npcs/ciudadano_aguerrido.c", random(RANDOM_GUARDIAS));
			}
		
		break;
		case "veleiron":
			switch (random(2))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
					sala->add_clone("/d/orgoth/npcs/guardia_puerta_veleiron.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
					sala->add_clone("/d/orgoth/npcs/guardia_veleiron.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		
		// MALOS
		case "dendra":
			switch (random(3))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/dendra/npcs/galador/galador_soldado.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/dendra/npcs/soldados/soldado_imperial.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 2:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/dendra/npcs/galador/guardia_galador.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		case "grimoszk":
			switch (random(2))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/zulk/npcs/soldado.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/zulk/npcs/guardia.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		case "ar'kaindia":
			switch (random(2))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/al-qualanda/npcs/soldado_orgo.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/al-qualanda/npcs/sacerdote_velian.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		// ANARQUICOS
		case "golthur":
			switch (random(3))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/golthur/npcs/empalador.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/golthur/npcs/shaman_orco.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 2:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("[/d/golthur/npcs/uruk-hai.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		case "mor_groddur":
			switch (random(3))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/mor-groddûr/npcs/lider_horda.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/mor-groddûr/npcs/shaman.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 2:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/mor-groddûr/npcs/guardia.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		case "ancarak":
			switch (random(2))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/ancarak/npcs/kobold_shaman_aprendiz.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/ancarak/npcs/urd_patrullero.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		// MERCENARIOS
		case "anduar":
		
			if (!random(RANDOM_APARACION_GUARDIAS)){
				sala->add_clone("/d/anduar/npcs/cruzado_anduar.c", random(RANDOM_GUARDIAS));
			}

		break;
		case "keel":
		
			if (!random(RANDOM_APARACION_GUARDIAS)){
				sala->add_clone("/d/naggrung/npcs/keel_corsario_recluta.c", random(RANDOM_GUARDIAS));
			}
			
		break;
		case "agnur":
			switch (random(2))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/naggrung/npcs/agnur_patrullero_cruzado.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/naggrung/npcs/agnur_guardian_cruzado.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
		case "andlief":
			switch (random(2))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone("/d/naggrung/npcs/guardias/guardia_elfo.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){ 
					sala->add_clone("/d/naggrung/npcs/guardias/arquero_elfo.c", random(RANDOM_GUARDIAS));
					}
				break;
			}	
		break;
		case "vagabundo":
		
			if (!random(5)){
				sala->add_clone(NPCS+"/zona_guerra/mercenarios.c", random(3));
			}
			
		break;
		
		// NEUTRAL
		case "nalaghar":
				
			if (!random(RANDOM_APARACION_GUARDIAS)){
				sala->add_clone("/d/naggrung/npcs/nalaghar_barbaro.c", random(RANDOM_GUARDIAS));
			}
				
		break;	
		
		// DEFAULt
		
		default:
		switch (random(3))
			{
				case 0:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone(GUARDIAS+"mercenario_caballero.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 1:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone(GUARDIAS+"mercenario_sacerdote.c", random(RANDOM_GUARDIAS));
					}
				break;
				case 2:
					if (!random(RANDOM_APARACION_GUARDIAS)){
						sala->add_clone(GUARDIAS+"mercenario_soldado.c", random(RANDOM_GUARDIAS));
					}
				break;
			}
		break;
	}
}
	
	
