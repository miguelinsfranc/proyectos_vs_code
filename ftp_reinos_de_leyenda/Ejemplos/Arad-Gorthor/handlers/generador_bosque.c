// Dherion; Mayo 2017; Handler para el bosque

#include "../path.h"
#include <tiempo.h>
void describir(object habitacion, string tipo)
{
    habitacion->set_exit_color("verde");
    habitacion->fijar_luz(40);
    habitacion->add_smell("default","El olor de la vegetación inunda tus
fosas nasales.\n");
        habitacion->add_item("vegetacion","La vegetación es avundante y te
impide andar con normalidad.\n");
        habitacion->add_sound(({"bosque","default"}),"Oyes el sonido que
producen tus pasos al andar, el producido por algún animal huyendo al
verte...\n");
        habitacion->set_zone("bosque");
        habitacion->fijar_frecuencia_sensaciones(33);
    switch (tipo)
    {
    case "bosque":
        habitacion->fijar_bosque(4);
        habitacion->fijar_tipo_arbol("roble");
        habitacion->set_short("Un pequeño pero tupido bosquecillo");
        habitacion->set_long("Las copas de los árboles sobre tu cabeza apenas
deja pasar la luz del sol. la numerosa vegetación se enreda en tus piernas,
buscando los pocos rayos de luz que los altos árboles que componen este
bosquecillo dejan pasar entre sus ramas.\n");
        habitacion->set_night_long("Los altos y gruesos árboles que componen
este bosque ocultan con sus ramas la luz de la luna casi en su totalidad,
convirtiendo este lugar por la noche en casi intransitable. la numerosa
vegetación se enreda en tus piernas impidiéndote avanzar con normalidad.\n");
        if (CLIMA->query_noche()) {
            if (!CLIMA->query_nublado()) {
                habitacion->fijar_sensaciones(({
                    "Tus pasos rompen el silencio y la quietud del lugar",
                    "Ves un animalillo corretear hacia lo que posiblemente sea
su escondite al verte",
                    "La luz de la luna ilumina tu paso"
                  }));
            } else {
                habitacion->fijar_sensaciones(({
                    "Las nubes apenas dejan pasar la luz de la luna, reduciendo
tu visibilidad",
                    "Caminas con cuidado debido a la poca visibilidad",
                    "Los ruídos del bosque a tu alrededor consiguen
sobresaltarte"
                  }));
            }
        } else {
            if (CLIMA->query_nublado()) {
                habitacion->fijar_sensaciones(({
                    "Las nubes ocultan la luz del sol casi en su totalidad
haciendo difícil el caminar",
                    "La lluvia que cae te impide ver con claridad"
                  }));
            }
if(random(2))
habitacion->add_clone(BMONSTRUOS+"animales/lobo", 1);
if(random(2))
habitacion->add_clone(BMONSTRUOS+"animales/jabali", 1);
if(random(2))
habitacion->add_clone(BMONSTRUOS+"animales/ardilla", 1);
        }
        break;
    default:
        break;
    }
}
