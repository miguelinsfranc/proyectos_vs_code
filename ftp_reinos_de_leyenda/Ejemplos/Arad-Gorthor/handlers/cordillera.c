// Nohur 26/04/2019 - Handler para la zona "Cordillera"

#include <tiempo.h>
#include "../path.h"
inherit GENERADOR_BASE;

void poner_short(object sala, string zona, string subzona) {
    switch(subzona) {
		case "desvio":
			zona = "%^RESET%^Desvío hacia la Cordillera de Bûrzum: %^BOLD%^BLACK%^Rodeo del Bosque de Taubûrz%^RESET%^";
			break;
		case "paso":
			zona = "%^BOLD%^BLACK%^Cordillera de Bûrzum: %^RESET%^ORANGE%^Paso de Gargroth%^RESET%^";
			break;
		case "guarida":
			zona = "%^BOLD%^BLACK%^Cordillera de Bûrzum: %^RESET%^ORANGE%^Guarida%^RESET%^";
			break;
		case "caverna":
			zona = "%^BOLD%^BLACK%^Cordillera de Bûrzum: %^RESET%^ORANGE%^Caverna oculta%^RESET%^";
	}
    sala->set_short(zona);
}
void poner_fronteras(object sala, string zona, string subzona) {
	sala->fijar_fronteras(({0, 0, 0, "Golthur","Cordillera de Bûrzum","Caminos"}));  
}
void poner_color_salidas(object sala, string zona, string subzona) {
	switch (subzona)
	{
		case "desvio":  sala->set_exit_color("amarillo");break;
		case "paso":
		case "guarida":
		case "caverna":
		sala->set_exit_color("negro");break;
	}
    
}
void poner_npcs(object sala, string zona, string subzona) {
	
	switch (subzona)
	{
		case "desvio":  
			if (!random(4)) sala->add_clone(BMONSTRUOS+"cuervo", random(3));
			if (!random(4)) sala->add_clone(NPCS_GOLTHUR"serpiente_ebano",1);
			break;
		case "paso": 
			if (!random(6)) sala->add_clone(NPCS+"salteador_goblin", 1,(:$1->escondete():));
			if (!random(4)) sala->add_clone(NPCS+"culebra_ceniza",1);
			break;
		case "caverna":
			sala->add_clone(NPCS+"karghul",1);
	}
}
void poner_especiales(object sala, string zona, string subzona) {
    sala->fijar_clima("golthur");
	switch (subzona) {
        case "desvio":
			sala->set_night_long("La oscura noche junto a los macabros sonidos provenientes del Bosque de Taubûrz hacen de este desvío "
								 "un lugar que temer. No consigues observar ninguna luz por ningún lado, y los graznidos de los cuervos "
								 "te hacen sentir que puede que tú seas la carroña que esperan devorar.\n");
			sala->fijar_frecuencia_sensaciones(33);
			sala->fijar_sensaciones(({
					"Escuchas numerosos graznidos de la multitud de cuervos que se asientan sobre tu cabeza",
					"Los pequeños pasos de un pequeño roedor se acrecentan por momentos",
					"Un escalofrío recorre tu cuerpo al oir voces guturales que vienen sin duda del bosque cercano",
					"Una serpiente se enfrenta a un cuervo en un lado del camino",
					"Del Bosque de Taubûrz surgen murmullos que acaban con un grito sobrenatural"
				  }));
            break;
		case "paso":
			sala->fijar_frecuencia_sensaciones(33);
			sala->fijar_sensaciones(({
					"Un poco de ceniza proveniente del volcán te acaricia la cara",
					"Pequeños desprendimientos de roca cruzan el paso",
					"En un paso en falso pisas a una pequeña culebra, que tras un pequeño siseo desaparece entre las rocas",
					"El sonido de una pequeña roca cayendo te despista por momentos"
				  }));
            break;
		case "guarida":
			sala->fijar_frecuencia_sensaciones(50);
			sala->fijar_sensaciones(({
					"El olor a podredumbre te inunda las fosas nasales",
					"Un fuerte olor a heces te asalta",
					"El suelo esta lleno de charcos secos rojizos"
				  }));
		case "caverna":
			sala->fijar_luz(40);
            break;
    }
}

