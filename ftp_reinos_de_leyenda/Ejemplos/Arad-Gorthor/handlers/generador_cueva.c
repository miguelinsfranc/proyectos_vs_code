// Dherion; Mayo 2017; Handler para la cueva

#include "../path.h"
#include <tiempo.h>
void describir(object habitacion, string tipo)
{
    habitacion->set_exit_color("verde");
    habitacion->fijar_luz(40);
    habitacion->add_smell("default","No hueles nada especial.\n");
    habitacion->add_item("musgo","Ves que las paredes están cubiertas de
musgo.\n");
        habitacion->add_sound(({"cueva","default"}),"Oyes el sonido que
producen tus pasos al andar y el de tu propia respiración.\n");
        habitacion->set_zone("cueva");
        habitacion->fijar_frecuencia_sensaciones(33);
    switch (tipo)
    {
    case "cueva":
        habitacion->set_short("Una cueva natural");
        habitacion->set_long("Tus ojos no son capaces de dislumbrar el final de
esta cueva natural. "
          "Alguien ha colgado en las paredes de piedra, llenas de musgo y demás
vegetación variada de este tipo de lugares algunas antorchas que iluminan
levemente tu paso, "
          "lo que te hace pensar que alguna raza inteligente utiliza esta cueva
para algo, ya sea esconderse o cualquier otra cuestión.\n");
      habitacion->fijar_sensaciones(({
                    "El eco de tus propios pasos te sobresalta",
"La luz de las antorchas apenas es suficiente para iluminar el lugar",
                    "Caminas con cuidado, tratando de no perderte"
                  }));
habitacion->add_clone(NPCS+"/goblin_cueva", 1+random(4));
        break;
    default:
        break;
    }
}
