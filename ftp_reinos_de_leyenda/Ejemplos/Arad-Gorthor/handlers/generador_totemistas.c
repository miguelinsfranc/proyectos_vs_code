// Dunkelheit 05-11-2009

#include "../path.h"

void describir(object habitacion, string tipo)
{
	switch (tipo)
	{
		default:
		case "cavernas":
			habitacion->set_short("%^ORANGE%^Caverna de los Totemistas%^RESET%^");
			habitacion->set_long("Estas angostas cavernas, apropiadamente ocultas bajo toneladas y toneladas de basura "
			"golthur-hai, sirven de guarida para la mayor comunidad hobgoblin de Arad Gorthor. Delante y detrás de ti "
			"sólo hay oscuridad que las erráticas antorchas no pueden despejar, ¿quién sabe a dónde "
			"conducirán estas serpenteantes galerías? Por lo menos no hay encrucijadas ni "
			"otras desgracias laberínticas por el estilo, sino que el pasillo se retuerce de "
			"un lado a otro sin bifurcación que te complique. Pero eso también puede jugar "
			"contra ti: esta guarida puede ser una trampa mortal. A tus lados, y apoyados "
			"junto a las paredes, hay numerosos camastros y utensilios personales; es obvio "
			"que estas bestias ni siquiera conocen el concepto de habitación, y conviven "
			"hacinados por los pasillos cual cochiquera. Buena suerte, y no llegues tan "
			"lejos que te sea imposible regresar con vida.\n");
			habitacion->add_item("antorchas", "Son antorchas que iluminan muy poco. Los "
			"hobgoblins no son conocidos precisamente por su amor a la luz.\n");
			habitacion->add_item("camastros", "Tienen un aspecto de lo más incómodo. Eso por no decir que seguramente estén "
			"infestados de piojos. ¡Ugh!\n");
			habitacion->add_item("utensilios", "Cazuelas, cuerdas, algún que otro barril... ninguno de ellos tiene nada de "
			"interés.\n");
			habitacion->set_exit_color("negro");
			habitacion->set_zone("cavernas");
			habitacion->fijar_luz(30);
			if (random(4)) {
				habitacion->add_clone(NPCS+"hobgoblin", roll(1,3));
			}
			if (!random(4)) {
				habitacion->add_clone(NPCS+"guerrero_hobgoblin", 1);
			}
			if (!random(4)) {
				habitacion->add_clone(NPCS+"totemista_hobgoblin", 1);
			}
			if (!random(6)) {
				habitacion->add_clone(NPCS+"campeon_hobgoblin", 1);
			}
			break;
	}
}
