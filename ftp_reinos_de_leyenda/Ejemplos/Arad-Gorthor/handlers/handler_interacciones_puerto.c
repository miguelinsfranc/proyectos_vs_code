//Altaresh 19

#include <clean_up.h>
#include <depuracion.h>
#include "../path.h";

void create() {
    mi_euid();
}

void marcharse(object principal, string secundario, int tipo);
void atacar_secundario(object principal, string secundario);
object dame_objeto_secundario(object principal,string secundario);
void dar_exploracion(string id,object npc);

void fijar_interaciones(object principal, string secundario, string id)
{
  
	if(!principal) return;
	
	switch (secundario)
	{
		case "borracho":
                
				principal->nuevo_paso_interaccion("conversacion_borrachos", principal,
				({	":da trompicones y se sostiene como puede.",
					"'Viva este puerto, las mejores bebidas que un "+principal->dame_raza()+" puede tomar.",
					})
				);
				principal->nuevo_paso_interaccion("conversacion_borrachos", secundario,
				({	":asiente con la cabeza mientras mira a "+principal->QCN+".",
					"'Y que lo digas hermano, no puedo parar de beber.",
					":se lanza con los brazos abiertos hacia "+principal->QCN+"."})
				);
				principal->nuevo_paso_interaccion("conversacion_borrachos", principal,
				({	":levanta la cabeza como puede y abre los brazos aceptando el abrazo de "+secundario+".",
					"'Ven hermano, cantemos juntos.",
					"'Golthur... PATRIA QUERIDAAAAAAA!"})
				);
				principal->nuevo_paso_interaccion("conversacion_borrachos", secundario,
				({	":se apoya en su nuevo colega y sigue a "+principal->QCN+" con su interpretación.",
					"' Golthur DE MIS AMORES!",
					":junto a "+principal->QCN+" abandonan la zona sujetándose el uno al otro para no caerse mientras siguen cantando.",
                    (:dar_exploracion,id,principal:),
                    (:marcharse,principal,secundario,3:),
                    }),
				);
		break;
		
		
		case "cangrejo":
				principal->nuevo_paso_interaccion("capturar_cangrejo", secundario,
				({	":Castañea sus pinzas sin importarle ninguno de los presentes.",
				})
				);
				principal->nuevo_paso_interaccion("capturar_cangrejo", principal,
				({	":Se fija en un "+secundario+" de la zona.",
					"'Mmmmm, es hora de cambiar el menú...",
					"'Ven aquí "+secundario+", tu serás el plato estrella hoy."})
				);
				principal->nuevo_paso_interaccion("capturar_cangrejo", secundario,
				({	":se lleva sus pinzas a la boca después de recoger desperdicios de la zona.",
					":ignora completamente a "+principal->QCN+"."})
				);
				principal->nuevo_paso_interaccion("capturar_cangrejo", principal,
				({	":recoge un garrote de detras del mostrador y se encamina hacia "+secundario+".",
					"'Ven, merienda con patas.",
					":lanza un ataque con su garrote en direccion al "+secundario+"."})
				);
				principal->nuevo_paso_interaccion("capturar_cangrejo", secundario,
				({	":esquiva velozmente el ataque de "+principal->QCN+".",
					":gira sobre si mismo y ataca con sus pinzas a la mano del "+principal->QCN+"."})
				);
				principal->nuevo_paso_interaccion("capturar_cangrejo", principal,
				({	":grita de dolor cuando la pinza de  "+secundario+" lastima su mano.",
					"'¡Maldito seas!, ¡ser del abismo!.",
					":empieza a sacudir su mano como un loco para liberarse de las pinzas de "+secundario+"."})
				);
				principal->nuevo_paso_interaccion("capturar_cangrejo", secundario,
				({	":libera a "+principal->QCN+" de su presa y abandona el lugar con un porte chulesco.",
                    (:dar_exploracion,id,principal:),
					(:marcharse,principal,secundario,2:)
				})
				);
		break;
						
		case "humano":
				principal->nuevo_paso_interaccion("oler_humano", principal,
				({	":huele el aire a su alrededor",
					"'Por aquí huele a humano.",
                    (:dar_exploracion,id,principal:)})
				);
		
		break;
		
		case "prostituta":
				principal->nuevo_paso_interaccion("piropear_prostituta", secundario,
				({	":se fija en "+principal->QCN+".",
					"'Mmmmm, Hola marinero.",
					"'¿Quieres pasar un buen rato?"})
				);
				principal->nuevo_paso_interaccion("piropear_prostituta", principal,
				({	":mira con los ojos desorbitados a la prostituta.",
					"'Tus verrugas me ponen como un bárbaro en furia."})
				);
				principal->nuevo_paso_interaccion("piropear_prostituta", secundario,
				({	":mira al "+principal->QCN+" de arriba a abajo.",
					"'Eso se lo diras a todas "+principal->QCN+" golfo.",
					"'Invitarme a un trago es lo que tienes que hacer"})
				);
				principal->nuevo_paso_interaccion("piropear_prostituta", principal,
				({	":mira al "+secundario+" babeando.",
					"'Te invitaré después de pasar por el callejon monada.",
					":señala con la mirada un callejón cercano."})
				);
				principal->nuevo_paso_interaccion("piropear_prostituta", secundario,
				({	":empuja a "+principal->QCN+" con la mano.",
					"'Primero las monedas, luego el placer."})
				);
				principal->nuevo_paso_interaccion("piropear_prostituta", principal,
				({	":introduce las manos en los bolsillos buscando dinero.",
					"'Toma y espero que esta vez merezca la pena.",
					":le arroja una moneda a "+secundario+" que recoge al vuelo"})
				);
				principal->nuevo_paso_interaccion("piropear_prostituta", secundario,
				({	":Agarra de la pechera a "+principal->QCN+".",
					"'Por aquí, guapo, sígueme al palacio de los deseos.",
                    (:dar_exploracion,id,principal:),
					(:marcharse,principal,secundario,1:)		
					})
				);
		break;
		
		case "goblin":
				principal->nuevo_paso_interaccion("deuda_goblin", secundario,
				({	":se da cuenta de la presencia de "+principal->QCN+".",
					":intenta pasar desapercibido."})
				);
				principal->nuevo_paso_interaccion("deuda_goblin", principal,
				({	":se da cuenta de que "+secundario+" esta en la zona.",
					"'¡Te he visto gusano!."})
				);
				principal->nuevo_paso_interaccion("deuda_goblin", secundario,
				({	":da un respingo al haber sido descubierto.",
					"'Hola "+principal->QCN+", cuanto tiempo.",
					":sonrie inocentemente mostrando su dentadura amarilla."})
				);
				principal->nuevo_paso_interaccion("deuda_goblin", principal,
				({	":cruje sus nudillos mirando a "+secundario+".",
					"'Me debes dinero, canijo.",
					"'Vete soltando el dinero o te vas con los peces."})
				);
				principal->nuevo_paso_interaccion("deuda_goblin", secundario,
				({	":se le apaga la sonrisa de la cara.",
					"'Ahora mismo me pillas que estoy sin blanca."})
				);
				principal->nuevo_paso_interaccion("deuda_goblin", principal,
				({	":agarra a "+secundario+" y lo levanta sin esfuerzo.",
					"'Espero que sepas nadar.",
					":lanza por los aires a "+secundario+" con dirección al agua."})
				);
				principal->nuevo_paso_interaccion("deuda_goblin", secundario,
				({	"'NOOOOOOOOOOOOOOOOOO.",
					":vuela por los aires hasta llegar al mar.",
                    (:dar_exploracion,id,principal:),
					(:marcharse,principal,secundario,2:)
					
					})
				);
		break;
		
				case "orco":
				principal->nuevo_paso_interaccion("pelea_orcos", secundario,
				({	":mira con cara de odio a "+principal->QCN+".",})
				);
				principal->nuevo_paso_interaccion("pelea_orcos", principal,
				({	":se da cuenta de que "+secundario+" le esta mirando.",
					"'¡Que miras!."})
				);
				principal->nuevo_paso_interaccion("pelea_orcos", secundario,
				({	":cruje sus nudillos.",
					"'Eres un despojo, "+principal->QCN+", tu me robaste ayer.",
					":se encamina hacia el otro marinero."})
				);
				principal->nuevo_paso_interaccion("pelea_orcos", principal,
				({	":hace varios gestos de fuerza, intentando intimidar a "+secundario+".",
					"'A mi nadie me acusa de ladrón",
					"'Vas a saber lo que es bueno...",
                    (:dar_exploracion,id,principal:),
					(:atacar_secundario,principal,secundario:)
					})
				);
				
		break;
		
		
				
		
	}
	
	principal->fijar_retardo_interacciones(2);
}

void poner_interacciones(object npc,string secundario,string *id_interacciones)
{
	if (!npc || !sizeof(id_interacciones)) return;
	
	if (secundario=="") return;
	
	foreach(string id in id_interacciones)
	{
		fijar_interaciones(npc, secundario, id);
	}

}

int test_iniciar_interaccion(object npc,string id)
{
	//Variuables generales
	int res=1; //variable que guardara el resultado de la funcion padre

	//Comprobaciones iniciales
	//Si no tenemos ide... fuera
	if (!id)
		return 0;	
	
	//En este punto el resultado de RES debe de ser 1, osea que si pasa las restricciones BASICAS
	
	//Apartir de aqui, comprobamos nuestras restricciones particulares a las posibles interaccioens
	switch(id){
		
		//Para la iteraccion
		case "oler_humano":
			{
				object *humanos=({});
				
				if (ENV(npc) && ENV(npc)->query_room())
					humanos=filter(all_inventory(ENV(npc)),(: $1->query_player() && $1->dame_raza()=="humano" :));

				//En este punto, RES sigue valiendo uno, asik si la iteraccion no cumple nuestra restriccion particular. la ponemos a 0
				if (!sizeof(humanos)) 
					res=0;
			}
			break;
		
		//Para la iteraccion
		case "deuda_goblin":
		{
			object *goblins=({});
				
			if (ENV(npc) && ENV(npc)->query_room())
				goblins=filter(all_inventory(ENV(npc)),(: $1->query_short()=="Marinero goblin":));

			//En este punto, RES sigue valiendo uno, asik si la iteraccion no cumple nuestra restriccion particular. la ponemos a 0
			if (!sizeof(goblins)) 
				res=0;
			if(npc->dame_raza()=="goblin")
				res=0;
		}
			break;
			
		case "pelea_orcos":
		{
			object *orcos=({});
			
			if (ENV(npc) && ENV(npc)->query_room())
				orcos=filter(all_inventory(ENV(npc)),(: $1->query_short()=="Marinero orco" || $1->query_short()=="Marinera orco" :));

			//En este punto, RES sigue valiendo uno, asik si la iteraccion no cumple nuestra restriccion particular. la ponemos a 0
			if (!sizeof(orcos)) 
				res=0;
			if(npc->query_short()!="Marinero orco")
				res=0;
		}
			break;
		case "piropear_prostituta":
			
			//Aki no comprobamos nada y el resultado en este punto debe de ser 1
			break;
			
		//Para cualkeir otra intreaccion no definida previamente dentro del switch
		default:
			//Aki no comprobamos nada y el resultado en este punto debe de ser 1
			break;
		
			
	}
	
	
	return res;
}

object dame_objeto_secundario(object principal, string secundario)
{	
	object ob_secundario;

	if(!environment(principal)) return 0;
	
    ob_secundario = present(secundario, environment(principal));

	return ob_secundario;
}

void marcharse(object principal, string secundario, int tipo)
{
	object ob_secundario;	
	
	if (!principal) return ;
	
		switch(tipo)
		{
			case 1:
			
				ob_secundario=dame_objeto_secundario(principal,secundario);
				
				if (!ob_secundario) return notify_fail("Error en las interacciones no esta el secundario en la room");
				
				tell_room(ENV(principal), principal->QCN+" y "+ob_secundario->QCN+" abandonan juntos la zona haciendose carantoñas.\n", "");
				
				ob_secundario->dest_me();
				principal->dest_me();			
				
				return;
				break;
				
			case 2:
			
				ob_secundario=dame_objeto_secundario(principal,secundario);
				
				if (!ob_secundario) return notify_fail("Error en las interacciones no esta el secundario en la room");
				
				ob_secundario->dest_me();
                
                return;
                break;
				
			 case 3:
			 
				ob_secundario=dame_objeto_secundario(principal,secundario);
				
				if (!ob_secundario) return notify_fail("Error en las interacciones no esta el secundario en la room");
				
				ob_secundario->dest_me();
				principal->dest_me();			
				
				return;
				break;
							 
				
		}
		return;
}

void atacar_secundario(object principal,string secundario) {
	
    object ob_secundario = dame_objeto_secundario(principal,secundario);
    
    __debug_x(sprintf("antes del if de ataca"),"arad-gorthor"); 
    
    if ( ob_secundario && principal){
        __debug_x(sprintf("dentro del if de ataca"),"arad-gorthor"); 
        principal->attack_ob(ob_secundario);
    }
}
void dar_exploracion(string id,object npc)
{
    object *jugadores=({});
    
    if(!npc)
    {
        return notify_fail("Error en la entrega del Token de exploracion");
    }
    if (!ENV(npc) || !ENV(npc)->query_room())
    {
        return notify_fail("Error en la entrega del Token de exploracion");
    }
    jugadores=filter(all_inventory(ENV(npc)),(: $1->query_player()==1:));
    
    foreach(object jugador in jugadores){
        if(!jugador)
        {
        continue;
        }
        DAR_TOKEN(jugador, "boreal", id);
    }
}
    
    