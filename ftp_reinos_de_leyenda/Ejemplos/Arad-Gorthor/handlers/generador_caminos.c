// Dunkelheit 19-11-2009
/*
 * Caminos de Arad-Gorthor
        Eckol 11Oct14: Aumento la frecuencia de NPCS. Ahora hay animales en 2
de cada 3 salas, y en las que no,
                1 de cada 5 tiene un salteador goblin, y 1 de cada 3 patrullas.
 */
// Dherion Abril 2017: Añado el camino hacia la costa
// Dherion Mayo 2017: Añado el camino hacia el bosque
// Grimaek Abril 2023: Añado camino entre cementerio y linde sur para cuadrar con el mapa general
// Grimaek Junio 2023: Añado logros a las firmas del Arcon del Volcan

#include "../path.h"
#include <tiempo.h>
void describir(object habitacion, string tipo)
{
    object * mon;
    mon = ({"hormiga_roja","serpiente", "rata", "raton", "buho", "cuervo",
      "escorpion", "araña_enorme", "cucaracha"});
    switch (tipo)
    {
    case "camino_costa":
        habitacion->set_short("Ruta Boreal: Camino hacia la Costa de los Dioses");
        habitacion->set_long("Al igual que el camino a Eldor, este pequeño sendero parece no haber sido mantenido desde hace tiempo por el estado que presenta."
          " El suelo está cubierto de maleza y pequeñas piedrecitas, dificultando el avanzar. A lo lejos consigues ver la Costa de los Dioses."
          "Al mirar hacia un lado te percatas que caer de la ruta natural que han formado las rocas de este acantilado podría no ser muy agradable.\n");
        habitacion->set_night_long("Al igual que el camino a Eldor, este pequeño sendero parece no haber sido mantenido desde hace tiempo por el estado que presenta."
          " El suelo está cubierto de maleza y pequeñas piedrecitas que dificultan el avance, a lo que se le une la oscuridad de la noche. Tratas de mirar hacia abajo, pero la falta de visibilidad te lo impide.\n");
        habitacion->set_exit_color("amarillo");
        habitacion->fijar_luz(70);
        habitacion->add_item(({"suelo","piedras","piedrecitas"}),"El suelo está cubierto por pequeñas piedras que llegan desde el acantilado.");
        habitacion->add_item("maleza","La maleza te impide avanzar con normalidad. Te da la impresión de que nadie se ha preocupado por mantener este camino.");
        habitacion->fijar_frecuencia_sensaciones(33);
        if (CLIMA->query_noche()) {
            if (!CLIMA->query_nublado()) {
                habitacion->fijar_sensaciones(({
                    "El silencio, roto solo por el ruído que ocasionan tus pies sobre el suelo te sobrecoge",
                    "La quietud del ambiente a tu alrededor pesa como una losa sobre tu tranquilidad",
                    "Caminas con mucho cuidado para no caer por el acantilado"
                  }));
            } else {
                habitacion->fijar_sensaciones(({
                    "Las nubes apenas dejan pasar la luz de la luna, por lo que tropiezas con una piedra y caes al suelo",
                    "La brisa helada del mar que sopla desde la costa te hace temblar de frío",
                    "El agua ha provocado que el suelo esté resvaladizo, por lo que pones un pie mal al intentar avanzar y caes irremediablemente al suelo"
                  }));
            }
        } else {
            if (CLIMA->query_nublado()) {
                habitacion->fijar_sensaciones(({
                    "Las nubes ocultan la luz del sol casi en su totalidad, haciéndote aminorar el paso por seguridad",
                    "La lluvia que cae te impide ver con claridad"
                  }));
            }
        }
        break;
    case "camino_bosque":
        habitacion->fijar_bosque(1);
        habitacion->fijar_tipo_arbol("roble");
        habitacion->set_short("Ruta Boreal: Sendero natural");
        habitacion->set_long("La presencia de algún que otro árbol y de algún que otro arbusto marcan un sendero que parte desde el camino de eldor hasta lo que parece ser un pequeño bosquecillo."
        " A juzgar por el aspecto del suelo, cuvierto de hojas, ramas y malas hierbas sospechas que nadie en mucho, mucho tiempo ha transitado por este sendero natural.\n");
        habitacion->set_exit_color("verde");
        habitacion->fijar_luz(70);
        habitacion->add_item(({"suelo","hojas","ramas"}),"El suelo está cubierto por una gran variedad de hojas y ramas transportadas por el viento.");
        habitacion->add_item("maleza","La maleza te impide avanzar con normalidad. Te da la impresión de que poca gente ha transitado por este sendero.");
        habitacion->fijar_frecuencia_sensaciones(33);
        if (CLIMA->query_noche()) {
            if (!CLIMA->query_nublado()) {
                habitacion->fijar_sensaciones(({
                    "El silencio, roto solo por el ruído que ocasionan tus pies sobre el suelo te sobrecoge",
                    "La quietud del ambiente a tu alrededor pesa como una losa sobre tu tranquilidad",
                    "Caminas con lentitud, pisando y apartando ramas a tu paso"
                  }));
            } else {
                habitacion->fijar_sensaciones(({
                    "Las nubes apenas dejan pasar la luz de la luna, por lo que tropiezas con una rama y caes al suelo",
                    "El aullido de un lobo te sobresalta",
                    "El agua ha provocado que el suelo esté resvaladizo, por lo que pones un pie mal al intentar avanzar y caes irremediablemente al suelo"
                  }));
            }
        } else {
            if (CLIMA->query_nublado()) {
                habitacion->fijar_sensaciones(({
                    "Las nubes ocultan la luz del sol casi en su totalidad reduciendo tu visibilidad, lo que ocasiona que tropieces con una rama y caigas al suelo",
                    "La lluvia te empapa.",
"El frío cala tus huesos"
                  }));
            }
        }
        break;
    case "prox_bastion":
        habitacion->set_short("Ruta Boreal: Proximidades del Bastión de Arad-Gorthor");
        habitacion->set_long("La Ruta Boreal es un camino que bordeando la meseta oriental se dirige directo hacia el volcán N'argh. "
          "Aunque el camino es pedregoso las pesadas botas de las criaturas que lo transitan han convertido el camino en una sencilla "
          "y llana carretera por la que caben perfectamente tres carromatos. Al este una extensa llanura se adentra en el reino de Celiath "
          "mientras que al oeste se alza, con sobrecogedora presencia, la Meseta Oriental y un poco más al norte la fortaleza de Golthur Orod.\n");
        habitacion->set_night_long("La vida nocturna en la Ruta Boreal resulta inquietante, la quietud arrullada por los sonidos de "
          "distintas criaturas nocturnas hacen temblar todos tus músculos como si un frío helado soplara por tu espalda. Las únicas luces "
          "disponibles provienen del norte, donde el volcán N'argh palpita adormecido y del Bastión de Arad-Gorthor, la formidable y "
          "bulliciosa fortaleza pentagonal que a estas horas rezuma actividad.\n");
        habitacion->set_exit_color("amarillo");
        habitacion->fijar_luz(70);
        habitacion->fijar_frecuencia_sensaciones(33);
        if (CLIMA->query_noche()) {
            if (!CLIMA->query_nublado()) {
                habitacion->fijar_sensaciones(({
                    "Gritos de alguna violenta reyerta entre los habitantes de Arad-Gorthor llegan claramente hasta tí",
                    "La quietud del ambiente a tu alrededor pesa como una losa sobre tu tranquilidad",
                    "Un pequeño chillido, infrahumano, te hiela la sangre"
                  }));
            } else {
                habitacion->fijar_sensaciones(({
                    "Las lunas ocultas por las nubes arrojan escasa luz sobre el camino haciéndote torpezar",
                    "El frío empapa tus huesos en esta noche lluviosa haciéndote sentir débil y sin defensa ante lo desconocido",
                    "Numerosos charcos poblan el camino que ha quedado enfangado dificultando el avance"
                  }));
            }
        } else {
            if (CLIMA->query_nublado()) {
                habitacion->fijar_sensaciones(({
                    "El sol brilla con mortecina luz atravesando las nubes que pueblan el cielo",
                    "La fina lluvia en la lontananza te permite ver poco más que las proximidades del camino, sucio y lleno de barro",
                    "Los surcos de los carros llenos de agua por las recientes lluvias son una trampa en la que tropiezas continuamente",                
                  }));
            }
        }
        break;
    case "prox_volcan":
        habitacion->set_short("Ruta Boreal: Proximidades del volcán N'argh");
        habitacion->set_long("El camino de piedra triturada está compuesto en esta zona por una roca negruzca, de aspecto poroso "
          "seguramente proviniente del volcan, que se alza amenazador al oeste obligando al camino a realizar un pequeño rodeo "
          "en su largo camino hacia la costa.\n");
        habitacion->set_night_long("La calidez del volcán se hace palpable en las horas nocturnas, seguramente bajo tus pies hay "
          "lava fundiendo la roca aunque la carretera no muestra ninguna grieta y parece una vía segura por la que circular.\n");
        habitacion->add_item(({"volcan","volcán"}),"Imponente al oeste se alza el volcán N'argh, "
          "una pared escarpada de roca volcánica y aspecto inexpugnable.\n");
        habitacion->set_exit_color("amarillo");
        habitacion->fijar_luz(80);
        habitacion->fijar_frecuencia_sensaciones(33);
        if (CLIMA->query_noche()) {
            habitacion->fijar_sensaciones(({
                "Una diminuta bola de fuego se alza desde el volcán disipándose en su ascensión",
                "En la oscuridad de la noche la cima del volcán resplandece como las ascuas de una fragua",
                "La abundancia de flora y fauna revela la riqueza del suelo en esta zona"
              }));
        } else {
            habitacion->fijar_sensaciones(({
                "El calor sofocante de la ladera oriental del volcán hace este tramo del camino agotador",
                "Un continuo tráfico de carros y máquinas de asedio ha creado unos surcos descomunales en el duro suelo",
                "Gritos alarmantes de criaturas infernales llegan desde el volcán"
              }));
        }
        break;
    case "arco_volcan":
        habitacion->set_short("Ruta Boreal: Arco bajo el volcán N'argh");
        habitacion->set_long("El coste de la construcción de la Ruta Boreal llegó a tal extremo que por no rodear el poderoso "
          "volcán N'argh los constructores prefieron atravesar la dura roca del camino de ascenso hasta el mismo. Tal obra "
          "fue fruto tanto de la genialidad kobold como de la locura goblin, quienes fueron usados como carnaza para atravesar "
          "la roca por si llegaba el caso en que cruzaban un río de lava o se producía algo similar. Afortunadamente no fue así, "
          "desgraciadamente pensaron algunos orcos, puesto que habían apostado por que muchos goblins morirían en el intento, y un "
          "poderoso arco de roca apuntalado con descomunales columnas de sólida piedra permite atravesar bajo el camino de ascenso "
          "al volcán sin peligro alguno para los viandantes.\n");
        habitacion->add_item(({"columna","columnas"}), "Aunque no sabes que tipo de piedra es su impasibilidad ante el descomunal peso "
          "que soportan te hace imaginar que no un material frágil.\n");
        habitacion->add_item("arco", "La roca está trabajada brutal pero eficientemente, con un dibujo irregular por la falta de precisión "
          "pero con recurrentes firmas de los distintos obreros que lo pusieron ahí.\n");
        switch(random(10)) {
        case 0: habitacion->add_item(({"firma","firmas"}), "Xolik, el magnífico cavó esta piedra.\n");habitacion->add_property("firma","Xolik");break;
        case 1: habitacion->add_item(({"firma","firmas"}), "¡Gurthang! Bendice este arco y escúpenos tu gloria.\n");habitacion->add_property("firma","Anonimo1");break;
        case 2: habitacion->add_item(({"firma","firmas"}), "Elfo el que lo lea.\n");habitacion->add_property("firma","Anonimo2");break;
        case 3: habitacion->add_item(({"firma","firmas"}), "Klil y Burpe, amor para siem...\n");habitacion->add_property("firma","Klil1");break;
        case 4: habitacion->add_item(({"firma","firmas"}), "Burpe es una zorra. Klil.\n");habitacion->add_property("firma","Klil2");break;
        case 5: habitacion->add_item(({"firma","firmas"}), "Kavdmbas, estuvo aquí\n");habitacion->add_property("firma","Kavdmbas");break;
        case 6: habitacion->add_item(({"firma","firmas"}), "¿Porqué el elfo cruzó la carretera? Porque un humano se agachó al otro lado\n");habitacion->add_property("firma","Anonimo3");break;
        case 7: habitacion->add_item(({"firma","firmas"}), "Con las tres de antes y las dos de ahora ya son seis putas piedras colocadas. Tuz.\n");habitacion->add_property("firma","Tuz");break;
        case 8: habitacion->add_item(({"firma","firmas"}), "¿Como se llama el hijo de un orgo y una gnoma? Desafío total.\n");habitacion->add_property("firma","Anonimo4");break;
        case 9: habitacion->add_item(({"firma","firmas"}), "Me gustan los orcos bien cachondos, búscame en el sector tres. Burpe.\n");habitacion->add_property("firma","Burpe");break;
        }
        habitacion->set_exit_color("rojo");
        habitacion->fijar_luz(50);
        break;
    case "prox_dircin":
        habitacion->set_short("Ruta Boreal: Proximidades de las ruinas de Dircin'Gah");
        habitacion->set_long("Conforme avanzas por la ancha ruta que une Arad-Gorthor con la costa ves cómo la amplia explanada "
          "que se extiende al oeste acaba abruptamente en una zona boscosa contra la que se recorta siniestramente la figura retorcida "
          "de unas ruinas. Por el este la planicie que separa Arad-Gorthor de Eldor y Celiath se muestra inhóspita y desolada.\n");
        habitacion->set_night_long("El camino es monótono y te hace mirar a un lado y otro constantemente intentando distraer tu "
          "aburrimiento, eso hace que te fijes en la pálida luz verdosa que recorta la figura cadavérica de lo que otrora fue "
          "Dircin'Gah, ruinas de la ciudad llena de rumores, fábulas y cuentos para no dormir, donde desaparece algunos de los "
          "más valiente y aguerridos luchadores sin regresar jamás.\n");
        habitacion->set_exit_color("verde");
        habitacion->fijar_luz(80);
        habitacion->fijar_frecuencia_sensaciones(33);
        if (CLIMA->query_noche()) {
            habitacion->fijar_sensaciones(({
                "Un tenebroso aullido, agónico más allá de la muerte, cruza el vacío hiriendo tus tímpanos dolorosamente",
                "El camino en esta zona se llena de una niebla luminosa, como iluminada por fuegos fatuos",
                "La podredumbe llega a tu olfato como un dulce reclamo desde las ruinas de Dircin'Gah"
              }));
        } else {
            habitacion->fijar_sensaciones(({
                "Los viandantes con los que te cruzas en este tramo miran al suelo preocupados, avanzando con paso rápido",
                "En esta zona las flores, pálidas y moribundas, apenas resisten la agonía que flota en el ambiente",
                "Solo aves carroñeras sobrevuelan la zona, como un mal augurio"
              }));
        }
        break;
    case "prox_cerro":
        habitacion->set_short("Ruta Boreal: Proximidades del Cerro de los Gigantes");
        habitacion->set_long("El camino de la Ruta Boreal, siempre despejado con grandes explanadas resulta en comparación "
          "claustrofóbico en este tramo, a pesar de que mantiene la amplitud, por la proximidad del nublado Cerro de los "
          "Gigantes que se alza amenazador en el oriente, y la terrible cercanía de Tauburz, el bosque de los No-Muertos, "
          "que crece tenebroso al oeste.\n");
        habitacion->set_night_long("La luz de las lunas apenas se atreve a brillar aquí, difuminada por la densa niebla "
          "que rodea el Cerro de los Gigantes que durante las frías noches crece inundando la planicie adyacente. Todo "
          "a tu alrededor adquiere un fantasmagórico aire de irrealidad cuando apenas puedes ver unos metros más adelante.\n");
        habitacion->set_exit_color("amarillo");
        habitacion->fijar_frecuencia_sensaciones(33);
        if (CLIMA->query_noche()) {
            habitacion->fijar_luz(5);
            habitacion->fijar_sensaciones(({
                "La niebla te cala en los huesos, pero su frío llega más allá de lo físico ¿es el miedo?",
                "Solo la efectividad con la que se construyó esta vía te permite darte cuenta de que sigues pisándola y no te has perdido",
                "Un gruñido lejano, de algún tipo de bestia, es respondido por otro siniestro y lúgrube en la lejanía"
              }));
        } else {
            habitacion->fijar_luz(80);
            habitacion->fijar_sensaciones(({
                "El camino está marcado a veces por profundas huellas, mucho mayores que las de un orco",
                "Tienes la impresión de que es el peor momento para equivocarse de camino"
              }));
        }
        break;
    case "prox_puerto":
        habitacion->set_short("Ruta Boreal: Proximidades del Puerto de Golthur");
        habitacion->set_long("La poderosa brisa que inunda tus pulmones de salitre resulta refrescante y energética, "
          "llena tu cuerpo de gozo y dinamismo. El rumor de las olas llega a tus oidos desde la costa, acompañado por el "
          "insistente graznido de las gaviotas y los gritos habituales de las zonas portuarias.\n");
        habitacion->set_night_long("La noche templada por la proximidad de la costa augura una próspera pesca en la "
          "madrugada, lo que hace aún más ruidosos los cánticos provenientes de las tabernas en el Puerto de Golthur. "
          "El camino, regular y en constante mantenimiento por esta zona, se muestra perfectamente liso y perfecto "
          "para ser cruzado por carros, orcos o animales de carga.\n");
        habitacion->set_exit_color("azul");
        habitacion->fijar_luz(90);
        habitacion->fijar_frecuencia_sensaciones(33);
        if (CLIMA->query_noche()) {
            habitacion->fijar_sensaciones(({
                "El salitre inunda tus pulmones de vigor y energía con su fuerte y salado olor",
                "Las tabernas resuenan ruidosas, seguramente otra confrontación entre orcos y goblins",
                "Las luces de la zona portuaria al norte sirven como faro en tu camino"
              }));
        } else {
            habitacion->fijar_sensaciones(({
                "Con frecuencia ves a las gaviotas que sobrevuelan la zona costera en busca de alimentos",
                "Los ruidos de profuso trabajo en el muelle llegan claramente traídos por la brisa marina",
                "La proximidad de la costa se percibe en la abundante vegetación a ambos lados de la vía"
              }));
        }
        break;
    case "camino_eldor":
        habitacion->set_short("Ruta Boreal: Camino a Eldor");
        habitacion->set_long("El camino que une Eldor y Arad-Gorthor hace tiempo que dejó de ser mantenido "
          "y eso se percibe con claridad en como se ha ido degradando poco a poco, la maleza cubre el suelo "
          "dificultando el avance y el estado que presenta es lamentable.\n");
        //habitacion->set_night_long("");
        habitacion->set_exit_color("rojo");
        habitacion->add_sound("default","El silencio de este lugar solo es roto ocasionalmente por el ruído de algún animal.");
        habitacion->add_item("maleza","La maleza te impide avanzar con normalidad. Te da la impresión de que nadie se ha preocupado por mantener este camino.");
        habitacion->fijar_luz(70);
        habitacion->fijar_frecuencia_sensaciones(33);
        if (CLIMA->query_noche()) {
            if (CLIMA->query_lloviendo()) {
                habitacion->fijar_sensaciones(({
                    "El agua disminuye tu visibilidad, haciéndote tropazar con la maleza y relentizando tu paso",
                    "Caes al suelo al tropezar con una rama, manchándote de barro"
                  }));
            }
            else {
                habitacion->fijar_sensaciones(({
                    "Una rata cruza por delante de tí ignorándote por completo como perseguida por la propia muerte",
                    "El salitre inunda tus pulmones de vigor y energía con su fuerte y salado olor",
                    "Las plantas se enredan entre tus piernas haciéndote trastabillar"
                  }));
            }
        } else {
            habitacion->fijar_sensaciones(({
                "Apenas consigues ver donde pones el pie y terminas cayendo torpemente al suelo",
                "Debes haber pasado por una zona de ortigas pues la irritación que sientes por las piernas es horrible"
              }));
        }
        break;
    case "cemen_lindesur":
        habitacion->set_short("Linde Sudeste de la Meseta Oriental");
        habitacion->set_long("Te encuentras en un largo camino de tierra bastante ancho,"
	" que une la linde sur del volcán N'argh con el Cementerio de Armas de Asedio. "
	"El terreno es abrupto y pedregoso lleno de matas secas y malas hierbas. Hacia el sur "
	"se extiende el dominio del reino de Dendra, y su imponente fortaleza Galador. "
	"Observas en algunos lados del camino material volcánico, aún candente. El camino "
	"circunda los últimos metros de la ladera sur de la meseta y se adentra cada vez más y "
	"más en la cara este.\n");
        habitacion->set_exit_color("amarillo");
        habitacion->add_sound("default","El silencio de este lugar solo es roto ocasionalmente por el ruído de algún animal.");
        habitacion->add_item("maleza","La maleza te impide avanzar con normalidad. Te da la impresión de que nadie se ha preocupado por mantener este camino.");
        habitacion->fijar_luz(70);
        habitacion->fijar_frecuencia_sensaciones(33);
        if (CLIMA->query_noche()) {
            if (CLIMA->query_lloviendo()) {
                habitacion->fijar_sensaciones(({
                    "El agua disminuye tu visibilidad, haciéndote tropazar con la maleza y relentizando tu paso",
                    "Caes al suelo al tropezar con una rama, manchándote de barro"
                  }));
            }
            else {
                habitacion->fijar_sensaciones(({
                    "Una rata cruza por delante de tí ignorándote por completo como perseguida por la propia muerte",
                    "El salitre inunda tus pulmones de vigor y energía con su fuerte y salado olor",
                    "Las plantas se enredan entre tus piernas haciéndote trastabillar"
                  }));
            }
        } else {
            habitacion->fijar_sensaciones(({
                "Apenas consigues ver donde pones el pie y terminas cayendo torpemente al suelo",
                "Debes haber pasado por una zona de ortigas pues la irritación que sientes por las piernas es horrible"
              }));
        }
        break;	
////////////////////ZONAESTE
    case "caminoeste":
        habitacion->set_short("CAMINO ESTE");
        habitacion->set_long("PENDIENTE.\n");
        habitacion->set_exit_color("amarillo");
        habitacion->add_sound("default","El silencio de este lugar solo es roto ocasionalmente por el ruído de algún animal.");
        habitacion->add_item("maleza","La maleza te impide avanzar con normalidad. Te da la impresión de que nadie se ha preocupado por mantener este camino.");
        habitacion->fijar_luz(70);
        habitacion->fijar_frecuencia_sensaciones(33);
        if (CLIMA->query_noche()) {
            if (CLIMA->query_lloviendo()) {
                habitacion->fijar_sensaciones(({
                    "El agua disminuye tu visibilidad, haciéndote tropazar con la maleza y relentizando tu paso",
                    "Caes al suelo al tropezar con una rama, manchándote de barro"
                  }));
            }
            else {
                habitacion->fijar_sensaciones(({
                    "Una rata cruza por delante de tí ignorándote por completo como perseguida por la propia muerte",
                    "El salitre inunda tus pulmones de vigor y energía con su fuerte y salado olor",
                    "Las plantas se enredan entre tus piernas haciéndote trastabillar"
                  }));
            }
        } else {
            habitacion->fijar_sensaciones(({
                "Apenas consigues ver donde pones el pie y terminas cayendo torpemente al suelo",
                "Debes haber pasado por una zona de ortigas pues la irritación que sientes por las piernas es horrible"
              }));
        }
        break;	



///FIN ZONAESTE		
    }
    //añadimos npcs
    switch(tipo){
case "camino_costa":
if (!random(3)) habitacion->add_clone("/baseobs/monstruos/animales/gaviota",
1);
break;
    case "prox_puerto":
        switch(random(4)){
        case 0:
            habitacion->add_clone(NPCS_BO+mon[random(sizeof(mon))],
1+random(3));
            break;
        case 1:
            habitacion->add_clone(NPCS_BO+"animales/gaviota", 1+random(2));
            break;
        case 2:
            if (!random(5)) habitacion->add_clone(NPCS+"salteador_goblin", 1);
            if (!random(3)) habitacion->add_clone(NPCS_G+"patrulla_vigilancia",
1);
            break;
        default:
            break;
        }
    default:
        if (random(3)) {
            habitacion->add_clone(NPCS_BO+mon[random(sizeof(mon))],
1+random(3));
        } else {
            if (!random(5)) habitacion->add_clone(NPCS+"salteador_goblin", 1);
            if (!random(3)) habitacion->add_clone(NPCS_G+"patrulla_vigilancia",
1);
        }
        break;
    }
}

