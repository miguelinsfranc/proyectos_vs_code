// Dherion, 03/04/2017: Handler para la Costa de los Dioses
#include "../path.h"
#include <tiempo.h>
#include <depuracion.h>
void describir(object habitacion, string tipo)
{
	//__debug_x(tipo,"dherion");
    habitacion->set_exit_color("amarillo");
    habitacion->fijar_luz(70);
    habitacion->add_smell("default","Hueles la sal proveniente del mar en
el aire.\n");
        habitacion->add_item("mar","Te percatas de que el agua está bastante
limpia, podrías ver el fondo del mar si entrases a él sin ningún tipo de
problema.\n");
habitacion->add_sound(({"default","mar","ola","olas"}),"Oyes el sonido de las
olas rompiendo contra la orilla.\n");
        habitacion->set_zone("playa");
        habitacion->fijar_frecuencia_sensaciones(33);
        if (CLIMA->query_noche()) {
            if (!CLIMA->query_nublado()) {
                habitacion->fijar_sensaciones(({
                    "El sonido de las olas del mar acompaña tu paso",
                    "La tranquilidad que se respira en este lugar te abruma",
                    "Ves un pájaro caer en picado hacia el mar y ascender poco
después con algo en el pico"
                  }));
            } else {
                habitacion->fijar_sensaciones(({
                    "Las nubes apenas dejan pasar la luz de la luna, reduciendo
tu visibilidad",
                    "La brisa helada del mar que sopla te hace temblar de
frío",
                    "Inspiras lentamente, disfrutando de la tranquilidad del
lugar"
                  }));
            }
        } else {
            if (CLIMA->query_nublado()) {
                habitacion->fijar_sensaciones(({
                    "Las nubes ocultan la luz del sol casi en su totalidad",
                    "La lluvia que cae te impide ver con claridad"
                  }));
            }
    switch (tipo)
    {
    case "orilla":
	//__debug_x("Case orilla","dherion");
        habitacion->set_short("Costa de los dioses: Orilla");
        switch(random(2)) {
        case 0:
            habitacion->set_long("La playa se extiende a tu alrededor. Cerca
ves el mar con sus olas, que llegan una y otra vez a la orilla, coronadas por 
espuma blanca. "
              "El suelo, mojado por las frías aguas del Mard de los dioses que
viene y va, de fina arena también blanca, está adornado por numerosas conchas
de animales que han sido arrastradas por el océano.\n");
            break;
        case 1:
            habitacion->set_long("Tus pies pisan la fina y húmeda arena que
forma este suelo. Ante tí están las frías y misteriosas aguas del Mar de los
Dioses, más allá el inmenso océano. Ves alguna que otra concha de algún animal
que ya ha muerto bajo tus pies, seguramente traída por estas frías aguas.\n");
            break;
        default:
            break;
        }
        //habitacion->set_night_long(".\n");
                break;
    case "playa":
	//__debug_x("Case playa","dherion");
        habitacion->set_short("Costa de los dioses: Playa");
        switch(random(3)) {
        case 0:
            habitacion->set_long("La playa se extiende a tu alrededor. Cerca
ves el mar con sus olas, que llegan una y otra vez a la orilla, coronadas por 
espuma blanca. "
              "El suelo, de fina arena también blanca, está adornado por alguna
que otra roca que ha caído del acantilado, al igual que por numerosas conchas
de animales que han sido arrastrados por el océano y por pequeñas dunas que el
viento ha ido formando al amontonar la arena.\n");
            break;
        case 1:
            habitacion->set_long("Tus pies pisan una fina y blanca arena que se
mueve guiada por el viento, formando pequeñas dunas a tu alrededor. Ves alguna
que otra roca que se ha desprendido del acantilado y ha caído rodando por el
sendero que da acceso a este lugar. Puedes ver alguna que otra concha en la
arena, seguramente traída por el mar.\n");
            break;
        case 2:
            habitacion->set_long("En esta playa, que se extiende a tu
alrededor, ves una arena fina y blanca que forma pequeñas dunas al ser
amontonada por el viento. Mirando a tu alrededor, ves cerca el mar, rocas que
han caído del acantilado... También te encuentras con alguna que otra concha
que la marea ha traído en algún momento.\n");
            break;
        default:
            break;
        }
        //habitacion->set_night_long(".\n");
if(random(2))
habitacion->add_clone(BMONSTRUOS+"animales/gaviota", 1);
if(random(2))
habitacion->add_clone(NPCS+"cangrejo_moteado", 1);
        break;
    default:
        break;
    }
}
}
