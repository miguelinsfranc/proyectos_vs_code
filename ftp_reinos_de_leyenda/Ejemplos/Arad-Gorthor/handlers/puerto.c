// Nohur 26/01/2019 - Handler para la zona "Puerto"

#include "../path.h"
inherit GENERADOR_BASE;
#define NUM_MAX_NPCS 3

void poner_short(object sala, string zona, string subzona)
{

    switch (subzona) {
        case "entrada":
            subzona = "%^BLUE%^BOLD%^Juggash Bur%^RESET%^, Puerto de Golthur Orod";
            break;
		case "puerto":
            subzona = "%^BLUE%^BOLD%^Juggash Bur%^RESET%^, Puerto de Golthur Orod";
            break;
		case "muelle":
            subzona = "%^BLUE%^BOLD%^Juggash Bur%^RESET%^, Muelle";
            break;
		case "taberna":
            subzona = "%^GREEN%^BOLD%^Taberna de Juggash Bur%^RESET%^";
            break;
		case "tienda":
            subzona = "%^CYAN%^Tienda de Abastecimientos Marítimos de Juggash Bur%^RESET%^";
            break;
		case "astillero":
            subzona = "%^CYAN%^Astillero Juggashi%^RESET%^";
            break;
    }

    sala->set_short(subzona);
}

void poner_fronteras(object sala, string zona, string subzona) {
	sala->fijar_fronteras(({0, 0, 0, "Golthur", "Puerto", "Juggash Bur"})); 
 
}
void poner_color_salidas(object sala, string zona, string subzona) {
     sala->set_exit_color("cian");
}
// Añade items de la sala
void poner_items(object sala, string zona, string subzona) {
	switch (subzona) {
        case "entrada":
			sala->add_item(({"escala", "escalera","escalinata","escalas","escaleras","escalinatas"}), 
							"Una pared de dura roca ha sido golpeada brutalmente hasta ceder a los deseos de los "
							"orcos de adquirir la forma aproximada de una escalera.");
			break;
		case "puerto":
            break;
		case "muelle":
            break;
		case "taberna":
			sala->poner_cartel("piel");
            break;
		case "tienda":
			sala->crear_cartel("grabado");
            break;
		case "astillero":
            break;
    }
}
void rellenar_npcs_puerto(object sala, string *npcs_subzonas)
{
			//Puede haber hasta 3 NPCS en la room 
			//Puede haber 1 de cada tipo, 2 de uno, etc. No serán los 3 del mismo tipo. (O sí, eso lo decide el aleatorio).
			//Se meterán tantos NPCS como el random siguiente indique
	int random;
			
	for(int num=0;num<random(NUM_MAX_NPCS)+1;)
	{
		//Número de NPCS a meter (nunca sobrepasará el número Máximo definido)
		random=random(NUM_MAX_NPCS-num)+1;
		//Al numero de NPCs añadidos se le incrementa el random
		num+=random;
		//Se meten tantos NPCS aleatorios como diga el Random
		sala->add_clone(element_of(npcs_subzonas), random);
	}
}


// PNJs
void poner_npcs(object sala, string zona, string subzona) {
	
	
	mapping npcs_subzonas;
	
	 npcs_subzonas = ([
        "entrada": ({
            NPCS + "prostitutos",
			NPCS + "marineros",
			NPCS + "borrachos"         
        }),
        "puerto":({
            NPCS + "prostitutos",
			NPCS + "marineros",
			NPCS + "borrachos",
			//NPCS + "putita", la dejo para mas adelante
        }),
        "muelle": ({
            NPCS + "pescadores.c"
        }),
		 
		
    ]);
	npcs_subzonas["taberna"] = npcs_subzonas["entrada"];
	
	switch (subzona) {
        case "entrada":
            sala->add_clone(BMONSTRUOS+"animales/cangrejo.c", random(4));
			// ver arriba funcion rellenar_npcs_puerto	
			rellenar_npcs_puerto(sala, npcs_subzonas["entrada"] );
            break;
		case "puerto":
			sala->add_clone(BMONSTRUOS+"animales/cangrejo.c", random(4));
			rellenar_npcs_puerto(sala, npcs_subzonas["puerto"] );
            break;
		case "muelle":
            sala->add_clone(NPCS + "pescadores.c", random(NUM_MAX_NPCS-1)+1);
            break;
		case "taberna":
           sala->add_clone(BMONSTRUOS+"animales/cangrejo.c", random(2));
           rellenar_npcs_puerto(sala, npcs_subzonas["taberna"] );
            break;
		case "tienda":
            sala->poner_tendero(NPCS+"dependiente_tienda.c");
            break;
		case "astillero":
            sala->poner_tendero(NPCS + "dependiente_astillero.c");
            break;
    }
}
// Cualquier otra cosa
void poner_especiales(object sala, string zona, string subzona) {
    sala->fijar_clima("mar_de_los_dioses");
	switch (subzona) {
        case "entrada":
		case "puerto":
			sala->fijar_frecuencia_sensaciones(33);
			sala->fijar_sensaciones(({
					"Te sorprende que alguna partícula de ceniza llegue hasta la zona desde el volcán",
					"Acabas de pisar algo que eres incapaz de reconocer, buagg!",
					"Sorteas despojos de algún ser en tu camino",
					"Deberías oler a mar pero el hedor a podredumbre es demasiado fuerte en la zona",
					"Un goblin persigue a un cangrejo con un tenedor y un cuchillo en las manos",
					"Un gnoll acaba de lanzar a un orco al mar, parece que le esta recriminando algo relacionado con una deuda",
					"Varios goblins están sentados a un lado del muelle jugando a los dados",
					"Que olor, que olor... no puedes dejar de pensar en ello",
					"Si estás pensado en buscar pelea crees que no hay sitio mejor para encontrarla",
					"Varios orcos sujetan a gnoll mientras un goblin le depila el pecho con unas tenazas",
					"Un marinero vomita al mar toda la bebida que tenia en el cuerpo",
					"Varios prostitutos rodean a un joven marinero insinuándose, mientras uno de ellos le esta metiendo la mano en la bolsa",
					"Casi pierdes el equilibrio al pisar un líquido que no te atreves ni a pensar en que es",
					"Una gaviota y un cangrejo luchan a muerte a tu lado"
				  }));
            break;
		case "muelle":
			sala->fijar_frecuencia_sensaciones(33);
			sala->fijar_sensaciones(({
					"Te sorprende que alguna particula de ceniza llegue hasta la zona desde el volcán",
					"Acabas de pisar algo que eres incapaz de reconocer, buagg!",
					"Sorteas despojos de algún ser en tu camino",
					"Deberías oler a mar pero el hedor a podredumbre es demasiado fuerte en la zona",
					"Un goblin persigue a un cangrejo con un tenedor y un cuchillo en las manos",
					"Un gnoll acaba de lanzar a un orco al mar, parece que le esta recriminando algo relacionado con una deuda",
					"Varios goblins están sentados a un lado del muelle jugando a los dados",
					"Que olor, que olor... no puedes dejar de pensar en ello",
					"Si estás pensado en buscar pelea crees que no hay sitio mejor para encontrarla",
					"Varios orcos sujetan a gnoll mientras un goblin le depila el pecho con unas tenazas",
					"Un marinero vomita al mar toda la bebida que tenia en el cuerpo",
					"Varios prostitutos rodean a un joven marinero insinuándose, mientras uno de ellos le esta metiendo la mano en la bolsa",
					"Casi pierdes el equilibrio al pisar un líquido que no te atreves ni a pensar en que es",
					"Una gaviota y un cangrejo luchan a muerte a tu lado"
				  }));
            sala->fijar_coordenadas( ({33, 14, 0}) );//33 Oeste 14 Sur
			sala->fijar_clima("mar_de_los_dioses");
            break;
		case "taberna":
			sala->fijar_frecuencia_sensaciones(33);
			sala->fijar_sensaciones(({
					"Uno de los presentes acaba de tirarse el pedo más sonoro que has oído en tu vida",
					"Un goblin acaba de salir despedido de su silla, parece que el orco de su lado quería ese sitio",
					"El cocinero parece rebosar el pescado en un barreño que parece estar sacado de alguna letrina",
					"Varios marineros se afanan en un concurso de pulsos",
					"Uno de los goblin que sirve en la taberna ha tropezado derramando varias bebidas en un orco con cara de pocos amigos, no quieres ni mirar...",
					"No sabes que olor es peor, el de los marineros o el que sale de la cocina",
					"Ves uno de los tatuajes de un marinero gnoll, parece un muestrario de posturas eróticas",
					"Un orco se dirige a la esquina que todos llaman el vomitero y echa hasta la última papilla",
					"Varios gnolls comienzan una lucha de aullidos, que termina cuando uno de ellos cae al suelo inconsciente por el esfuerzo",
					"Desde la cocina no paran de salir platos cocinados de cosas que no sabrías identificar a simple vista"
				  }));
			sala->fijar_organizacion("golthur");
            sala->poner_tabernero(NPCS+"tabernero_puerto");
			sala->poner_carisma(3);
			sala->poner_plato("Pescado variado a la plancha", "comida", 30);
			sala->poner_alias("pescado", "variado");
			sala->poner_plato("Otra cosa a la plancha", "comida", 60);
			sala->poner_alias("otra","cosa");
			sala->poner_plato("Aguardiente", "alcohol",8);
			sala->poner_alias("Aguardiente","aguardiente");
            break;
		case "tienda":
			sala->fijar_organizacion("golthur");
            sala->fijar_tipo_tienda("general");
			sala->poner_tabla_objetos(([
				"/baseobs/misc/sextante":roll(2,4),
				"/baseobs/misc/catalejo":roll(1,3),
				]));
            break;
		case "astillero":
			sala->fijar_organizacion("golthur");
            sala->fijar_tipo_tienda("astillero");
			sala->fijar_tabla_objetos(([
				"/d/oceano/barcos/escrituras/escritura_ballesta_ligera": roll(1,5),
				"/d/oceano/barcos/escrituras/escritura_ballesta": roll(1,4),
				"/d/oceano/barcos/escrituras/escritura_ballesta_pesada": 1,
				"/d/oceano/barcos/escrituras/escritura_cañon_ligero": roll(1,3),
				"/d/oceano/barcos/escrituras/escritura_cañon": roll(1,2),
				"/d/oceano/barcos/escrituras/escritura_cañon_pesado": 1,
				"/d/oceano/barcos/escrituras/escritura_catapulta_ligera": roll(1,2),
				"/d/oceano/barcos/escrituras/escritura_catapulta": roll(1,2),
				"/d/oceano/barcos/escrituras/escritura_catapulta_pesada": 1,
				"/d/oceano/barcos/municiones/bala": roll(2,10),
				"/d/oceano/barcos/municiones/obus": roll(2,8),
				"/d/oceano/barcos/municiones/saeta": roll(2,25),
				"/baseobs/misc/arpeo_abordaje": roll(1,4),
				"/baseobs/misc/catalejo": roll(1,3),
				"/baseobs/misc/sextante": roll(1,3),
				"/d/oceano/barcos/escrituras/escritura_bote": 3,
				"/d/oceano/barcos/escrituras/escritura_barco": 1,
				"/d/oceano/barcos/escrituras/escritura_cañonero": 1,
				"/d/oceano/barcos/escrituras/escritura_crucero": 1,
				"/d/oceano/barcos/escrituras/escritura_barco": 1,
				"/d/oceano/barcos/escrituras/escritura_destructor": 1,
				"/d/oceano/barcos/escrituras/escritura_fragata": 1,
				"/d/oceano/barcos/escrituras/escritura_goleta": 1,
				// "/d/oceano/barcos/escrituras/escritura_goleta_arcana": 1,
				"/d/oceano/barcos/escrituras/escritura_velero": 1,
				"/d/oceano/barcos/pinturas/bote_pintura_amarillo": !random(3),
				"/d/oceano/barcos/pinturas/bote_pintura_azul": !random(3),
				"/d/oceano/barcos/pinturas/bote_pintura_azul_intenso": !random(3),
				"/d/oceano/barcos/pinturas/bote_pintura_blanco": random(2),
				"/d/oceano/barcos/pinturas/bote_pintura_cian": random(2),
				"/d/oceano/barcos/pinturas/bote_pintura_cian_intenso": random(2),
				"/d/oceano/barcos/pinturas/bote_pintura_gris": random(2),
				"/d/oceano/barcos/pinturas/bote_pintura_magenta": !random(5),
				"/d/oceano/barcos/pinturas/bote_pintura_magenta_intenso": !random(5),
				"/d/oceano/barcos/pinturas/bote_pintura_marron": random(2),
				"/d/oceano/barcos/pinturas/bote_pintura_negro": !random(5),
				"/d/oceano/barcos/pinturas/bote_pintura_rojo": !random(3),
				"/d/oceano/barcos/pinturas/bote_pintura_rojo_intenso": !random(3),
				"/d/oceano/barcos/pinturas/bote_pintura_verde": !random(4),
				"/d/oceano/barcos/pinturas/bote_pintura_verde_intenso": !random(4),
				]));
            break;
    }

}

