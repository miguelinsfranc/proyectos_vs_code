//Eckol Ago19
//Eckol May20: modo continuo (sin temporadas)

#include <conquistas.h>

inherit H_CONQUISTAS_BASE;

void setup(){
    fijar_nombre("boreal");
    fijar_corto("Campamento Boreal");

    puntos_por_pk();
    //fijar_periodo_conquista("semana_real");

    fijar_resucitar( "Campamento Boreal", "/d/arad-gorthor/zonas/golthur_eldor/zona_guerra/campamento_00" );

    fijar_ayuda_conquista(sprintf("La influencia en %s se gana con muertes de jugadores en la zona, y derrotando al líder del campamento. "
	"La zona la dominara por defecto la ciudadania de Golthur Orod. "
    "Las conquistas de la zona se rigen por temporadas que duraran una semana de tiempo real.",dame_corto()));

    fijar_ayuda_beneficios(sprintf("Conquistar %s permite ir a la zona al resucitar, y poder acceder y cruzar el campamento libremente.",dame_corto()));
	
	fijar_conquistador_defecto("golthur");
}
