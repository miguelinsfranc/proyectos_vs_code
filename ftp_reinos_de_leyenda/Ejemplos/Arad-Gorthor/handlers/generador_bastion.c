// Dunkelheit 19-11-2009
/*
 * Matones, pordioseros, antorchero
 * Cazarrecompensas
 * Boticario 
 * Flagelador
 * Doctor de Plagas
 * 
 */

#include "../path.h"
#include <tiempo.h>

void describir(object habitacion, string tipo)
{
	switch (tipo)
	{
		case "puerta_oeste":
			habitacion->set_short("%^BLACK%^BOLD%^Las Puertas de la Guerra%^RESET%^");
			habitacion->set_long("¡Siempre abiertas a la guerra! Así permanecen estas dos "
			"titánicas puertas de más de veinte metros de altura y casi dos metros de "
			"grosor. La tierra se ha amontonado alrededor de ellas puesto que es algo "
			"muy excepcional que se cierren, y es que la feroz guardia que las custodia "
			"no necesita escudarse tras ellas, ¡se bastan ellos sólos para doblegar "
			"cualquier ataque! Con la misma vertiginosa altura, el muro de Arad Gorthor "
			"se extiende hasta perderse en polvoriento ambiente de estas tierras yermas. "
			"Dos orgullosas atalayas se levantan entre las puertas y el muro, y tras sus "
			"estrechas ventanas acechan docenas de letales ballesteros goblinoides. El "
			"terreno está allanado por las pisadas de cientos de miles de tropas a lo "
			"largo de muchos años, aunque esta ciudad fortificada es relativamente joven "
			"comparada con otras, pues data de la Era 4ª. Pero no sólo tropas ve pasar "
			"el metal gris de estas puertas, sino también la ingente cantidad de mercaderes "
			"y peregrinos que llegan de todos puntos de Dalaensar, pues Arad Gorthor es "
			"un importante núcleo comercial y, sobre todo, religioso: aquí se supone "
			"enterrada la Espada Negra de Lord Gurthang.\n");
			habitacion->set_night_long("La noche está dominada por el fuego que crepita "
			"en lo alto de las atalayas que vigilan el acceso a Arad Gorthor. Por debajo "
			"de sendas hogueras, decenas de estrechas ventanas dejan asomarse tímidamente "
			"a la luz de las antorchas de las misteriosas entrañas de esta mole de piedra y "
			"metal. Arad Gorthor es una ciudad oscura, y más todavía durante las gélidas "
			"noches de estas tierras yermas. Apenas pueden distiguirse las colosales "
			"puertas metálicas, tan oscuras que se funden con el cielo.\n");
			habitacion->add_item(({"manivela", "manivelas"}), "Desde aquí no las puedes "
			"ver bien, pues se encuentran en lo alto del muro.\n");
			habitacion->add_item(({"puerta", "puertas"}), "Son dos puertas grisáceas de "
			"metal tan grandes que sólo un dios puede haberlas puesto aquí: miden más de "
			"veinte metros de altura, un metro de grosor y casi quince de ancho. Para "
			"abrirlas, dos ogros del desierto han de tirar de una descomunal manivela. "
			"¡Tal proceso puede llegar a durar hasta diez minutos!\n");
			habitacion->add_item(({"atalayas", "atalaya"}), "Dos atalayas hexagonales "
			"que se levantan entre las puertas y el muro, y que sobresalen para dar más "
			"angulo de disparo a los ballesteros que las infestan. Decenas de ventanas "
			"estrechas se reparten erráticamente hasta en... ¡nueve niveles de altura!\n");
			habitacion->add_item("muro", "El muro de Arad Gorthor es un puzzle interminable "
			"de ladrillos grises que llegan hasta donde se pierde la vista. Pero antes de "
			"llegar a ese punto puedes cómo el terreno se hunde en lo que antaño debió ser "
			"un foso.\n");
			habitacion->add_item("foso", "Un hendimiento del terreno que se divisa en el "
			"lejano norte te hace intuir que en el pasado debió exitir un foso. ¿Estará "
			"seco?\n");
			habitacion->add_item("suelo", "Cientos de huellas se extienden en todas "
			"direcciones. Algunas de ellas son tan grandes que te da miedo pensar qué "
			"clase de criatura las dejó aquí.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			habitacion->fijar_frecuencia_sensaciones(33);
			if (CLIMA->query_noche()) {
				habitacion->fijar_sensaciones(({
					"Percibes decenas de ojos que te escudriñan desde la oscuridad, enviolentándote",
					"La gélida brisa nocturna de las tierras yermas bate tu rostro y lo llena de arena",
					"Una crepitante luz sale de entre las puertas de Arad Gorthor. La ciudad parece tener una gran actividad nocturna",
				}));
			} else {
				habitacion->fijar_sensaciones(({
					"Te invade el vértigo al admirar la inmensidad de estas puertas",
					"Docenas de ojos te vigilan atentos desde los cientos de rendijas del muro y las atalayas",
					"Desde aquí ya se puede escuchar el bullicio de las calles de Arad Gorthor",
				}));
			}
			break;
		case "puerta_este":
			habitacion->set_short("%^BLACK%^BOLD%^Las Puertas de los Reinos Goblinoides%^RESET%^");
			habitacion->set_long("A diferencia de sus hermanas occidentales, estas puertas "
			"rara vez se abren. Su inactividad es tal que hasta parecen haberse fundido con "
			"el muro gris. Tampoco tienen atalayas defensivas, puesto que los ataques "
			"provinientes de Celiath son prácticamente inexistentes a día de hoy. Tan sólo "
			"los tiradores apostados en el muro dan algo de vida a este aburrido sector "
			"de la ciudad. La calzada hacia Celiath se extiende en dirección este, y está "
			"sembrada por barricadas y alambres de espino, que aguardan impacientes a un "
			"enemigo inexistente. Todos los reinos libres de Eirea conocen a estas puertas "
			"como las de los reinos goblinoides, ya que dan pie a todo el horror que se "
			"esconde en sus cientos de miles de hectáreas.\n");
			habitacion->set_night_long("La oscuridad de la noche envuelve las puertas "
			"orientales de la ciudad. Y a diferencia de sus hermanas occidentales, "
			"rara vez se abren. Su inactividad es tal que hasta parecen haberse fundido con "
			"el muro gris. Tampoco tienen atalayas defensivas, puesto que los ataques "
			"provinientes de Celiath son prácticamente inexistentes a día de hoy. Tan sólo "
			"los tiradores apostados en el muro dan algo de vida a este aburrido sector "
			"de la ciudad. La calzada hacia Celiath se extiende en dirección este, sumida "
			"en la más absoluta oscuridad.\n");
			habitacion->add_item(({"puerta", "puertas"}), "Son idénticas a las puertas del "
			"oeste de la ciudad, pero a diferencia de ellas rara vez se abren.\n");
			habitacion->add_item("muro", "No tiene nada de sorprendente. Es un muro de "
			"piedras grises, de los más altos que hay construidos en Eirea.\n");
			habitacion->add_item(({"calzada", "celiath"}), "Una calzada sembrada con "
			"trampas contra armas de asedio que se extiende hasta tierras de Celiath.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "explanada_oeste":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Explanada de Contención");
			habitacion->set_long("Son escasas las incursiones que logran sobrevivir a la "
			"Puerta de la Guerra y llegar hasta este punto. En esta explanada, escalonada "
			"en parapeto de forma casi inapreciable para dar ventaja al defensor, cualquier "
			"enemigo se verá asediado por los ataques provinientes de las atalayas, de las "
			"almenas del muro y de la propias tropas desplegadas en la explanada, lo "
			"suficientemente amplia como para que en los días festivos de Lord Gurthang "
			"se puedan celebrar aquí majestuosos desfiles militares. En periodos de paz "
			"la explanada es testigo del denso tráfico de orcos, goblins y otras bestias "
			"goblinoides. Las Puertas de la Guerra, situadas al oeste de la explanada, "
			"proyectan su sombra la tierra. Una calzada de baldosas de piedra se extiende "
			"en dirección este. En dirección sur se puede ver la Torre de Brujería, lugar "
			"sagrado de chamanes y hechiceros; en dirección noreste puedes ver una "
			"depresión del terreno en la que se amontonan cientos de pequeñas casas "
			"de formas retorcidas.\n");
			habitacion->add_item(({"puertas", "puerta"}), "Desde aquí se ven las enormes "
			"cadenas de su sistema de apertura. En sus eslabones bien podría dar seis pasos "
			"un halfling.\n");
			habitacion->add_item("suelo", "El suelo es de tierra, bastante limpio para tratarse de "
			"una ciudad goblinoide. Una calzada empieza a unos cincuenta metros de las "
			"puertas y se extiende hacia el centro de la ciudad.\n");
			habitacion->add_item("calzada", "Una calzada de baldosas de piedra.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "explanada_este":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Explanada de Contención");
			habitacion->set_long("Es una explanada similar a la que se encuentra en la "
			"puerta occidental, con la salvedad de que rara vez se ha derramado sangre "
			"enemiga en su tierra. A escasos cincuenta metros de las puertas que hay "
			"al este, empieza una calzada que se extiende en dirección al centro de la "
			"ciudad. Desde esta explanada se ven con total claridad el bastión de la "
			"Horda Negra, ubicado al noroeste, y el Templo de la Metalurgia al sudoeste, "
			"con una de sus cuatro chimeneas expulsando humo negro de forma ininterrumpida.\n");
			habitacion->add_item(({"puertas", "puerta"}), "Desde aquí se ven las enormes "
			"puertas que abren Arad Gorthor a las tierras del este, concretamente hacia "
			"Celiath. Pero como los goblinoides no quieren que estén abiertas... pues no "
			"lo están. Además, ¿para qué abrirlas si nadie quiere ir allí?\n");
			habitacion->add_item("suelo", "El suelo es de tierra, bastante limpio para tratarse de "
			"una ciudad goblinoide. Una calzada empieza a unos cincuenta metros de las "
			"puertas y se extiende hacia el centro de la ciudad.\n");
			habitacion->add_item("calzada", "Una calzada de baldosas de piedra.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "exterior_atalaya_norte":
			habitacion->set_short("Pie de la Atalaya Boreal de %^BLACK%^BOLD%^Arad Gorthor%^RESET%^");
			habitacion->set_long("Estás en el nacimiento del extenso muro que protege la "
			"ciudad fortificada de Arad Gorthor. Hay numerosos restos de asedios fallidos, "
			"pues el campo de batalla está fuera de la jurisdicción del servicio de "
			"limpieza de la ciudad. Será mejor andar con cuidado por aquí, no sea que "
			"todavía quede algún artefacto o trampa activada. El camino se extiende de "
			"norte a sur a lo largo de toda la cara occidental del muro.\n"
			"Sobre el muro, te parece apreciar algún tipo de plataforma.\n");
			habitacion->add_item("restos", "Son escombros sin utilidad alguna.\n");
			habitacion->add_item("plataforma","No consigues ver a nadie ahí, aunque esa plataforma "
												"sería el sitio ideal para que un tirador defendiera "
												"la ciudad.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "interior_atalaya_norte":
			habitacion->set_short("%^BLACK%^BOLD%^Saetera Boreal de Arad Gorthor%^RESET%^");
			habitacion->set_long("Estás es una pequeña plataforma que en vez de techo tiene dispuesto "
			"un parapeto que permite una total visibilidad de Las Puertas de la Guerra y las cercanías."
			" La astucia con la que está construido permite a un ballestero experimentado disparar sin "
			"ser visto para poder proteger el poderoso bastión.\n");
			habitacion->add_item("parapeto", "Gruesas tablas dispuestas alrededor de la plataforma con placas "
			"metálicas ancladas a la altura del rostro con diferentes perforaciones.\n");
			habitacion->add_item(({"TOKEN","placas"}), "Ves que las placas cubren amplios huecos del parapeto y gracias a "
			"los agujeros que tiene practicados se ve perfectamente la entrada al bastión. Un agujero más grande "
			"que el resto te hace suponer que sirve para apuntalar una ballesta y disparar en dirección a las "
			"puertas.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "exterior_atalaya_sur":
			habitacion->set_short("Pie de la Atalaya Austral de %^BLACK%^BOLD%^Arad Gorthor%^RESET%^");
			habitacion->set_long("Estás en el nacimiento del extenso muro que protege la "
			"ciudad fortificada de Arad Gorthor. Hay numerosos restos de asedios fallidos, "
			"pues el campo de batalla está fuera de la jurisdicción del servicio de "
			"limpieza de la ciudad. Será mejor andar con cuidado por aquí, no sea que "
			"todavía quede algún artefacto o trampa activada. El camino se extiende de "
			"norte a sur a lo largo de toda la cara occidental del muro.\n"
			"Sobre el muro, te parece apreciar algún tipo de plataforma.\n");
			habitacion->add_item("restos", "Son escombros sin utilidad alguna.\n");
			habitacion->add_item("plataforma","No consigues ver a nadie ahí, aunque esa plataforma "
												"sería el sitio ideal para que un tirador defendiera "
												"la ciudad.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "interior_atalaya_sur":
			habitacion->set_short("%^BLACK%^BOLD%^Saetera Austral de Arad Gorthor%^RESET%^");
			habitacion->set_long("Estás es una pequeña plataforma que en vez de techo tiene dispuesto "
			"un parapeto que permite una total visibilidad de Las Puertas de la Guerra y las cercanías."
			" La astucia con la que está construido permite a un ballestero experimentado disparar sin "
			"ser visto para poder proteger el poderoso bastión.\n");
			habitacion->add_item("parapeto", "Gruesas tablas dispuestas alrededor de la plataforma con placas "
			"metálicas ancladas a la altura del rostro con diferentes perforaciones.\n");
			habitacion->add_item(({"TOKEN","placas"}), "Ves que las placas cubren amplios huecos del parapeto y gracias a "
			"los agujeros que tiene practicados se ve perfectamente la entrada al bastión. Un agujero más grande "
			"que el resto te hace suponer que sirve para apuntalar una ballesta y disparar en dirección a las "
			"puertas.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "exterior_murallas":
			habitacion->set_short("Base de la Muralla de %^BLACK%^BOLD%^Arad Gorthor%^RESET%^");
			habitacion->set_long("Estás en el nacimiento del extenso muro que protege la "
			"ciudad fortificada de Arad Gorthor. Hay numerosos restos de asedios fallidos, "
			"pues el campo de batalla está fuera de la jurisdicción del servicio de "
			"limpieza de la ciudad. Será mejor andar con cuidado por aquí, no sea que "
			"todavía quede algún artefacto o trampa activada. El camino se extiende de "
			"norte a sur a lo largo de toda la cara occidental del muro.\n");
			habitacion->add_item("restos", "Son escombros sin utilidad alguna.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "murallas_norte":
			habitacion->set_short("Gran Muralla de %^BLACK%^BOLD%^Arad Gorthor%^RESET%^");
			habitacion->set_long("Te encuentras en lo alto del gran muro de Arad Gorthor. "
			"Las vistas aquí son espectaculares... si eres un amante de los panoramas "
			"apocalípticos. El muro tiene una amplitud suficiente como para que una "
			"fila de doce orcos pueda andar con soltura. Las enormes almenas están hechas "
			"de bloques de piedra gris y son la protección ideal para los tiradores que "
			"escudriñan el horizonte en busca de enemigos. Desde este sector del muro "
			"se puede ver toda la mitad norte del dominio goblinoide: las ruinas de "
			"Dircin'Gah en el noroeste, más al noreste el Cerro de los Gigantes, y en "
			"los días despejados hasta se puede ver el Mar de los Dioses.\n");
			habitacion->set_night_long("Las escasas antorchas dispuestas en la cara "
			"interior de las almenas sólo te permite ver el suelo de baldosas grises "
			"de lo alto de este muro. El horizonte, que en el sector norte del dominio "
			"no debería mostrar señales de vida, es tan oscuro que se funde con el cielo "
			"nocturno. Y esperas que así siga.\n");
			habitacion->add_item("almenas", "Largas filas de almenas que se pierden en el "
			"horizonte.\n");
			habitacion->add_item(({"ruinas", "dircin'gah", "Dircin'Gah"}), "Son las ruinas "
			"del antiguo poblado de goblins de la Era 2ª. Dicen que ahora son el hogar de "
			"numerosos muertos-vivientes.\n");
			habitacion->add_item("cerro", "El cerro de los gigantes está oculto entre nubes "
			"de origen divino. O eso dicen.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			habitacion->set_zone("muro");
			habitacion->fijar_frecuencia_sensaciones(10);
			if (CLIMA->query_noche()) {
				habitacion->fijar_sensaciones(({
					"Te pareció escuchar un escalofriante gemido proviniente de las lejanas ruinas de Dircin'Gah",
					"A esta altura sólo se escucha el fuerte viento de las tierras yermas",
					"Numerosas luces titilean en la malvada Arad Gorthor",
				}));
			} else {
				habitacion->fijar_sensaciones(({
					"Te pareció escuchar un escalofriante gemido proviniente de las lejanas ruinas de Dircin'Gah",
					"Observas con recelo el misterioso Cerro de los Gigantes, cuyas cumbres permanecen ocultas por nubes divinas",
					"El Mar de los Dioses ruge en el lejano horizonte septentrional",
				}));
			}
			
			//NPCS
			switch(random(6)){
				case 0..2: //De 1 a 3 snagas.
					habitacion->add_clone(NPCS + "snaga",random(3)+1);
					break;
				case 3..4: //De 1 a 2 soldados
					habitacion->add_clone(NPCS + "soldado_horda",random(2)+1);
					break;
				default:
					break;
			}
			
			break;
		case "murallas_sur":
			habitacion->set_short("Gran Muralla de %^BLACK%^BOLD%^Arad Gorthor%^RESET%^");
			habitacion->set_long("Te encuentras en lo alto del gran muro de Arad Gorthor. "
			"Las vistas aquí son espectaculares... si eres un amante de los panoramas "
			"apocalípticos. El muro tiene una amplitud suficiente como para que una "
			"fila de doce orcos pueda andar con soltura. Las enormes almenas están hechas "
			"de bloques de piedra gris y son la protección ideal para los tiradores que "
			"escudriñan el horizonte en busca de enemigos. Desde este sector del muro "
			"se puede ven los confines del imperio goblinoide: las torres de Golthur "
			"Orod asoman sobre la elevada meseta oriental y el horizonte macabro del "
			"cementerio de armas de asedio oculta prácticamente las escasas porciones "
			"de terreno dendrita visibles desde aquí.\n");
			habitacion->set_night_long("Las escasas antorchas dispuestas en la cara "
			"interior de las almenas sólo te permite ver el suelo de baldosas grises "
			"de lo alto de este muro. Desde aquí puede verse el ténue resplandor del "
			"volcán de N'argh y de la torres de Golthur Orod. La noche oculta el resto.\n");
			habitacion->add_item("almenas", "Largas filas de almenas que se pierden en el "
			"horizonte.\n");
			habitacion->add_item(({"torres", "golthur"}), "Del dominio de Golthur Orod "
			"sólo se distinguen las torres de su oscura fortaleza.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			habitacion->set_zone("muro");
			habitacion->fijar_frecuencia_sensaciones(10);
			if (CLIMA->query_noche()) {
				habitacion->fijar_sensaciones(({
					"Te pareció escuchar un escalofriante gemido proviniente de las lejanas ruinas de Dircin'Gah",
					"Esforzándote bien, eres capaz hasta de ver las luces de la lejana ciudadela de Galador",
					"El fuego del volcán de N'argh parece iluminar incluso más que las lunas",
					"Numerosas luces titilean en la malvada Arad Gorthor",
				}));
			} else {
				habitacion->fijar_sensaciones(({
					"Las cumbres de la Fortaleza Negra se elevan majestuosas en el lejano oeste",
					"Tus ojos se pierden en la vasta extensión del desierto que separa Arad Gorthor de Dendra",
					"Desde aquí tienes una increíble persectiva del cementerio de armas de asedio que se extiende en el sudoeste",
					"Puedes ver toda la actividad de Arad Gorthor con total claridad. Sin duda este es un lugar idóneo para patrullar",
				}));
			}
			//NPCS
			switch(random(6)){
				case 0..2: //De 1 a 3 snagas.
					habitacion->add_clone(NPCS + "snaga",random(3)+1);
					break;
				case 3..4: //De 1 a 2 soldados
					habitacion->add_clone(NPCS + "soldado_horda",random(2)+1);
					break;
				default:
					break;
			}
			
			break;
		case "atalaya_norte":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Atalaya Boreal");
			habitacion->set_long("La vieja piedra que se ha usado para construir la Atalaya Boreal "
			"todavía se muestra firme y bien asentada a pesar de las innumerables marcas de batalla "
			"que puedes apreciar. Los escuadrones que protegían tanto la atalaya Boreal como la "
			"Austral tenían dispuesto un lugar fijo lo que llevó a crear una carnicera competición "
			"entre ambas atalayas para ver qué escuadra conseguía mayor gloria durante un asalto. "
			"A día de hoy esta rivalidad todavía se mantiene alimentada por los dirigentes de la "
			"Horda Negra pues llena de coraje los corazones de los soldados.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "atalaya_norte_base":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Base de la Atalaya Boreal");
			habitacion->set_long("La poderosa Atalaya Boreal se asienta con la firmeza de un martillo "
			"sobre esta base de piedra grabada con innumerables inscripciones hechas en su mayoría con "
			"sangre.\n");
			habitacion->add_item(({"TOKEN","inscripciones"}), "Lees varias inscripciones al azar y los mensajes se "
			"repiten como consignas: Guerreros Boreales Invictos, Donde Falla El Sur Nosotros Arrasamos, "
			"Gurthang Ruge Con La Atalaya Boreal, La Atalaya Austral: Nido De Cobardes, y cosas similares, "
			"en un tamaño superior al resto aparece su grito de guerra: ¡Arrancavenas! derivado de su "
			"legendaria y brutal costumbre de arrancar las venas del cuello de sus víctimas.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "atalaya_sur":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Atalaya Austral");
			habitacion->set_long("La Atalaya Austral es la digna rival de la Boreal, una fortificacion robusta "
			"como un monolito que se yergue magullada pero orgullosa contra el cielo cenizo de Arad Gorthor. El escuadrón "
			"que protegía este edificio se formaba por soldados que nada más terminar su adiestramiento "
			"se incorporaban a la Escuadra Austral y por siempre se mantenían fieles a su posición, defendiéndolo con "
			"furia y sin miedo hasta la muerte.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "atalaya_sur_base":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Base de la Atalaya Austral");
			habitacion->set_long("La poderosa Atalaya Boreal se asienta con la firmeza de un martillo "
			"sobre esta base de piedra grabada con innumerables inscripciones hechas en su mayoría con "
			"sangre.\n");
			habitacion->add_item(({"TOKEN","inscripciones"}), "Lees varias inscripciones al azar y los mensajes se "
			"repiten como consignas: La Escuadra Boreal Está Plagada De Elfos Y Gnomos, Campeones Australes De Gurthang, "
			"Sangre y Dolor, y cosas similares, en un tamaño superior al resto aparece su grito de guerra: ¡Tronchapiernas! "
			"derivado de su salvaje afán de retorcer las piernas de sus enemigos hasta arrancarlas de cuajo.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;

		case "plaza":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Plaza del Lord Caído");
			habitacion->set_long("Podría decirse que el nacimiento de la ciudad fortificada que "
			"te rodea aconteció en esta plaza, cuando no era más que tierra yerma. Lord Gurthang "
			"se manifestó portando su legendaria Espada Negra, cuyo filo hendió la estéril roca "
			"que hasta entonces rara vez había sido pisada por un mortal. El tajo que infligió "
			"supuró vapores primero y demonios después, y la tierra se levantó en capas hasta "
			"formar un montículo virulento. Alrededor de esta manifestación divina empezó la "
			"pronta construcción de Arad Gorthor, y lo que quedóse como un accidente en el monótono "
			"terreno se convirtió en un templo en honor a Lord Gurthang, alimentado por los vapores "
			"que él mismo descubrió. Para acceder a su cima dispusieron escalones que crean una "
			"serie de capas concéntricas, y en su cumbre erigieron un santuario de porte mortuorio. "
			"Y hoy en día esta plaza, llamada del Lord Caído en honor a la historia personal de "
			"Gurthang, se ha convertido en el centro neurálgico, social y religioso de la ciudad "
			"fortificada.\n");
			habitacion->set_night_long("Podría decirse que el nacimiento de la ciudad fortificada que "
			"te rodea aconteció en esta plaza, cuando no era más que tierra yerma. Lord Gurthang "
			"se manifestó portando su legendaria Espada Negra, cuyo filo hendió la estéril roca "
			"que hasta entonces rara vez había sido pisada por un mortal. El tajo que infligió "
			"supuró vapores primero y demonios después, y la tierra se levantó en capas hasta "
			"formar un montículo virulento. Alrededor de esta manifestación divina empezó la "
			"pronta construcción de Arad Gorthor, y lo que quedóse como un accidente en el monótono "
			"terreno se convirtió en un templo en honor a Lord Gurthang, alimentado por los vapores "
			"que él mismo descubrió. Para acceder a su cima dispusieron escalones que crean una "
			"serie de capas concéntricas, y en su cumbre erigieron un santuario de porte mortuorio. "
			"Y hoy en día esta plaza, llamada del Lord Caído en honor a la historia personal de "
			"Gurthang, se ha convertido en el centro neurálgico, social y religioso de la ciudad "
			"fortificada. Por las noches, antorchas de fuego glauco, alimentadas por la magia de "
			"la Torre de Brujería, iluminan este recinto sagrado.\n");
			habitacion->add_item(({"templo", "santuario"}), "El templo de Lord Gurthang guarda un "
			"escalofriante parecido con un mausoleo: seis ornamentadas columnas corintias dan la "
			"bienvenida al recinto sagrado en el que Lord Gurthang hendió su famoso filo. En el "
			"entablamiento del templo se pueden ver numerosos relieves demoníacos con una imagen "
			"de Gurthang en el centro, armado hasta los dientes y con un enorme yelmo de cuernos "
			"varios. La cubierta es bastante elevada y da la impresión de albergar un nivel en "
			"ella.\n");
			habitacion->add_item("suelo", "Las plañideras se encargan de limpiar el suelo de "
			"forma accidental al arrastrase en sus rezos. Un trabajo impecable.\n");
			habitacion->add_item(({"escalones", "escaleras"}), "Hay que subir más de veinte "
			"escalones para llegar a la entrada del templo. En ellos es habitual ver a orcos y "
			"goblins rezando.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			habitacion->fijar_frecuencia_sensaciones(75);
			if (CLIMA->query_noche()) {
				habitacion->fijar_sensaciones(({
					"Por la noche, las letanías de los fieles de Lord Gurthang se convierten en un funesto murmullo que acompaña los sueños de los habitantes de Arad Gorthor",
					"Las antorchas glaucas iluminan la fachada del templo",
					"Andas con ojo para no tropezar con uno de los muchos fanáticos de Gurthang han peregrinado hasta aquí",
					"Avanzas con lentitud entre la turba de fieles congregados alrededor del templo",
					"El templo de Lord Gurthang se alza con esplendor en el centro de esta plaza",
				}));
			} else {
				habitacion->fijar_sensaciones(({
					"Andas con ojo para no tropezar con uno de los muchos fanáticos de Gurthang han peregrinado hasta aquí",
					"Las letanías de los fieles de Lord Gurthang pueden llegar a ser ensordecedoras, especialmente las proferidas por las plañideras",
					"Avanzas con lentitud entre la turba de fieles congregados alrededor del templo",
					"El templo de Lord Gurthang se alza con esplendor en el centro de esta plaza",
				}));
			}
			habitacion->add_clone(NPCS + "planidera", 2 + roll(1, 2));
			habitacion->add_clone(NPCS + "penitente", 2 + roll(1, 2));
			habitacion->add_clone(NPCS + "mortificado", 2 + roll(1, 2));
			break;
		case "templo":
			habitacion->set_short("%^WHITE%^BOLD%^Templo del %^BLUE%^Lord Caído%^RESET%^");
			habitacion->set_long("Aunque lo parezca, el templo no está a punto de derrumbarse. "
			"La enorme grieta que se extiende ante tus pies es en realidad el objeto de "
			"culto que dio lugar a la ciudad en la que te encuentras. Representando el "
			"momento -y causa- de su nacimiento, una enorme estatua de Lord Gurthang alza "
			"su mítica Espada Negra dispuesta a hundirla en el suelo. Pero esta estatua es "
			"lo de menos, ya que toda la atención de los fieles va dirigida a la humeante "
			"hendidura. Los amplios ventanales, en cuyos grabados predominan los colores "
			"blanco y azul, iluminan la estancia de una forma muy particular que no parece "
			"importunar a los goblinoides, más acostumbrados a estancias lúgubres. Y es que "
			"hay algo que pasa desapercibido para la mayoría de sus fieles, y es la realidad "
			"de que Gurthang fue, hace muchos muchos años, un noble paladín, hijo del dios "
			"del bien, Paris, que traicionó y sucumbió trágicamente a la llamada del odio y "
			"el terror. Este templo honra precisamente ese radical giro en su vida mortal.\n");
			habitacion->add_item(({"grieta", "suelo"}), "Una sinuosa grieta de unos seis "
			"metros de largo. Emite constantemente un vapor tóxico de penetrante olor "
			"sulfuroso. Está más que comprobado que, eventualmente, demonios y avatares "
			"del Plano Abismal hacen aparición a través de ella.\n");
			habitacion->add_item(({"cristales", "ventanas", "ventanales"}), "Representan "
			"escenas de la traición de Lord Gurthang a su padre Paris, la desesperación "
			"de éste, la formación del Lago de Cristal con sus lágrimas, su adopción por "
			"Oskuro, y su ascenso a deidad.\n");			
			habitacion->set_exit_color("blanco");
			habitacion->fijar_luz(65);
			
			habitacion->add_clone(NPCS + "cabildo", 1);
			break;
		
		case "akbath":
		case "akbath_centro":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Vía de Akbath");
			habitacion->set_long("Esta calzada, nombrada tras uno de los más famosos "
			"señores de la fortaleza de Golthur Orod en la Era 2ª, es la arteria principal "
			"de la ciudad fortificada de Arad Gorthor. Se extiende de oeste a este y comunica "
			"las dos puertas de la ciudad, además de dar paso a sus edificios más "
			"importantes. Su anchura es acorde con la ingente cantidad de goblins y orcos "
			"que la transitan día a día: no sólo mercaderes y peregrinos, sino también las "
			"henchidas tropas de la Horda Negra, que han de desfilar por las calles de la "
			"ciudad antes de lanzarse a sus largas incursiones. En la ribera de la calzada "
			"puedes observar una señal de civilización: ¡farolas! La sociedad goblinoide "
			"está encantada con este nuevo \"invento\". A esta altura de la vía puedes "
			"ver el alboroto habitual de la plaza del Lord Caído, que se vislumbra a "
			"escasos metros.\n");
			habitacion->add_item(({"farolas", "farola"}), "Farolas de diseño exclusivo para "
			"Arad Gorthor firmadas por el gabinete de herreros de Helldamm. Estas farolas "
			"han llevado casi diez años de desarrollo intenso, y en esta extenuante "
			"empresa se han perdido decenas y decenas de vidas. Así que en nombre de los "
			"ciudadanos de Arad Gorthor, esperamos que las disfrutes. Lo más importante es "
			"que por las noches se encienden e iluminan. ¡¿No es increíble?!\n");
			habitacion->add_item(({"suelo", "via", "calzada"}), "Sería entretenido jugar "
			"a que las piedras de la calzada son un lugar seguro mientras que las rendijas "
			"que se forman entre ellas suponen perder el juego. Pero si lo hicieras acabarías "
			"chocando contra alguno de los enfurecidos goblinoides que transitan por ella, "
			"y eso es algo que tú no quieres.\n");
			habitacion->add_item("plaza", "Es la plaza central de la ciudad fortificada, "
			"en la que se levanta un templo en honor a Lord Gurthang.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			habitacion->set_zone("akbath");
			if (!random(8)) {
				habitacion->add_clone(NPCS + "antorchero", 1);
			}
			if (!random(9)) {
				habitacion->add_clone(NPCS + "doctor", 1);
			}
			break;
		case "akbath_oeste":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Vía de Akbath");
			habitacion->set_long("Esta calzada, nombrada tras uno de los más famosos "
			"señores de la fortaleza de Golthur Orod en la Era 2ª, es la arteria principal "
			"de la ciudad fortificada de Arad Gorthor. Se extiende de oeste a este y comunica "
			"las dos puertas de la ciudad, además de dar paso a sus edificios más "
			"importantes. Su anchura es acorde con la ingente cantidad de goblins y orcos "
			"que la transitan día a día: no sólo mercaderes y peregrinos, sino también las "
			"henchidas tropas de la Horda Negra, que han de desfilar por las calles de la "
			"ciudad antes de lanzarse a sus largas incursiones. En la ribera de la calzada "
			"puedes observar una señal de civilización: ¡farolas! La sociedad goblinoide "
			"está encantada con este nuevo \"invento\". A esta altura de la vía puedes "
			"ver la majestuosa Torre de Brujería, que se yergue en el sur de la ciudad "
			"con sus fuegos mágicos de color verde. Al norte el terreno se hunde y se "
			"levantan las docenas de casas de los suburbios, cuyas formas te recuerdan, "
			"por algún motivo, a la nariz ganchuda y arrugada de una vieja nigromante.\n");
			habitacion->add_item(({"farolas", "farola"}), "Farolas de diseño exclusivo para "
			"Arad Gorthor firmadas por el gabinete de herreros de Helldamm. Estas farolas "
			"han llevado casi diez años de desarrollo intenso, y en esta extenuante "
			"empresa se han perdido decenas y decenas de vidas. Así que en nombre de los "
			"ciudadanos de Arad Gorthor, esperamos que las disfrutes. Lo más importante es "
			"que por las noches se encienden e iluminan. ¡¿No es increíble?!\n");
			habitacion->add_item(({"suelo", "via", "calzada"}), "Sería entretenido jugar "
			"a que las piedras de la calzada son un lugar seguro mientras que las rendijas "
			"que se forman entre ellas suponen perder el juego. Pero si lo hicieras acabarías "
			"chocando contra alguno de los enfurecidos goblinoides que transitan por ella, "
			"y eso es algo que tú no quieres.\n");
			habitacion->add_item(({"suburbios", "norte"}), "Es una de las zonas más "
			"peligrosas de la ciudad, así que ten cuidado si te aventuras por allí.\n");
			habitacion->add_item(({"torre", "sur"}), "La Torre de Brujería es uno de los "
			"lugares más exclusivos y secretos de la ciudad, pues sólo chamanes y hechiceros "
			"de cierto renombre tienen acceso a él.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			habitacion->set_zone("akbath");
			if (!random(8)) {
				habitacion->add_clone(NPCS + "antorchero", 1);
			}
			if (!random(9)) {
				habitacion->add_clone(NPCS + "doctor", 1);
			}
			break;
		case "akbath_este":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Vía de Akbath");
			habitacion->set_long("Esta calzada, nombrada tras uno de los más famosos "
			"señores de la fortaleza de Golthur Orod en la Era 2ª, es la arteria principal "
			"de la ciudad fortificada de Arad Gorthor. Se extiende de oeste a este y comunica "
			"las dos puertas de la ciudad, además de dar paso a sus edificios más "
			"importantes. Su anchura es acorde con la ingente cantidad de goblins y orcos "
			"que la transitan día a día: no sólo mercaderes y peregrinos, sino también las "
			"henchidas tropas de la Horda Negra, que han de desfilar por las calles de la "
			"ciudad antes de lanzarse a sus largas incursiones. En la ribera de la calzada "
			"puedes observar una señal de civilización: ¡farolas! La sociedad goblinoide "
			"está encantada con este nuevo \"invento\". A esta altura de la vía puedes ver "
			"el grandioso bastión de la Horda Negra, hogar del ejército de Lord Gurthang. Su "
			"edificio se levanta en el lado norte de la calzada. Frente a él, en el sur, se "
			"encuentra el Templo de la Metalurgia, y desde aquí puedes respirar el humo "
			"negro que vomita uno de sus cuatro chimeneas.\n");
			habitacion->add_item(({"farolas", "farola"}), "Farolas de diseño exclusivo para "
			"Arad Gorthor firmadas por el gabinete de herreros de Helldamm. Estas farolas "
			"han llevado casi diez años de desarrollo intenso, y en esta extenuante "
			"empresa se han perdido decenas y decenas de vidas. Así que en nombre de los "
			"ciudadanos de Arad Gorthor, esperamos que las disfrutes. Lo más importante es "
			"que por las noches se encienden e iluminan. ¡¿No es increíble?!\n");
			habitacion->add_item(({"suelo", "via", "calzada"}), "Sería entretenido jugar "
			"a que las piedras de la calzada son un lugar seguro mientras que las rendijas "
			"que se forman entre ellas suponen perder el juego. Pero si lo hicieras acabarías "
			"chocando contra alguno de los enfurecidos goblinoides que transitan por ella, "
			"y eso es algo que tú no quieres.\n");
			habitacion->add_item(({"norte", "bastion"}), "Es el enorme bastión de la Horda "
			"Negra, un edificio fortificado, un segundo nivel de protección en esta ciudad "
			"ya blindada de por sí.\n");
			habitacion->add_item(({"sur", "templo"}), "Puedes sentir el calor que emana del "
			"Templo de la Metalurgia, además de oler el humo tóxico que emite una de sus cuatro "
			"chimeneas negras. De este edificio sale todo el arsenal de los ejércitos de "
			"Lord Gurthang.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			habitacion->set_zone("akbath");
			if (!random(8)) {
				habitacion->add_clone(NPCS + "antorchero", 1);
			}
			if (!random(9)) {
				habitacion->add_clone(NPCS + "doctor", 1);
			}
			break;
		case "suburbios":
			habitacion->set_short("%^RED%^Suburbios de %^BLACK%^BOLD%^Arad Gorthor%^RESET%^");
			habitacion->set_long("Entrar en los suburbios de la ciudad fortificada de "
			"Arad Gorthor es como entrar en otro mundo... dentro de un mundo de por sí "
			"espantoso y hostil. Las calles aquí son angostas y claustrofóbicas, sensación "
			"agudizada por la anárquica arquitectura de los edificios, que parecen "
			"amontonarse y pelearse entre ellos por cada centímetro de espacio. En sus "
			"oscuras fachadas, oscurecidas por la ceniza de los humos tóxicos del Templo "
			"de la Metalurgia, hay ventanas que nunca jamás serán abiertas, pues la "
			"mayoría están tapiadas para evitar robos y, sobre todo, para ocultar a los "
			"mirones las barbaridades que puedan acontecer en su interior. No olvidemos que "
			"esto no deja de ser una mera parodia de lo que una ciudad debería ser, y que "
			"nada aquí es lo que parece. Las casas son todo menos un hogar, y no es de los "
			"callejones oscuros de lo que uno debe cuidarse, puesto que aquí el peligro "
			"suele andar al descubierto y disfrazado de lo que uno menos se espera.\n");
			habitacion->set_night_long("¡¿Pero qué haces aquí, insensato?! ¡Tan sólo un "
			"tarado se atrevería a circular por los suburbios en plena noche! Este es sin "
			"duda el lugar más peligroso de Arad Gorthor; aquí la guardia hace la vista "
			"gorda de todos los despropósitos que aquí acontecen. Estas calles están "
			"infestadas de vividores, bandidos y pícaros a los que la boca se les hace "
			"agua con tan sólo oir el tintineo de tu monedero. Las casas, ataviadas con "
			"antorchas colgantes para iluminar la calle, tienen todas las ventanas tapiadas, "
			"y por las noches no verás entrar o salir ni un alma de ellas.\n");
			habitacion->add_item(({"casas", "casa", "edificios"}), "Los edificios están "
			"torcidos en todos los sentidos: echados hacia delante, inclinados, y algunos "
			"hasta están como \"estrujados\". Sus paredes están negras por la ceniza de "
			"la polución que causa el Templo de la Metalurgia.\n");
			habitacion->add_item(({"ventanas", "ventana"}), "La mayoría están tapiadas para "
			"resguardar a sus habitantes... o para protegerte de ellos.\n");
			habitacion->add_item("suelo", "El suelo está bastante limpio para tratarse de "
			"una ciudad goblinoide. Pese a ello, los habituales riachuelos de porquería "
			"circulan a ambos lados de la vía. Y eso sucede todos los días del año, llueva "
			"o no. Por lo tanto no quieres preguntarte qué clase de líquidos son los que "
			"viajan por ellos hasta las alcantarillas.\n");
			habitacion->add_item("antorchas", "¿Qué sería de un lugar de Eirea sin sus "
			"pertinentes antorchas?\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(80);
			habitacion->set_zone("suburbios");
			habitacion->fijar_frecuencia_sensaciones(10);
			if (CLIMA->query_noche()) {
				habitacion->fijar_sensaciones(({
					"Caminas con preocupación por las peligrosas calles de los suburbios",
					"Escuchas griterío proviniente de algún lugar cercano; las peleas y los asesinatos son comunes en estos lares",
					"Un goblin borracho canturrea viejas canciones de guerra mientras lucha por no ahogarse en su propio vómito",
					"Una rata se da un chapuzón con jolgorio en los riachuelos de aguas negras que surcan esta calzada",
				}));
			} else {
				habitacion->fijar_sensaciones(({
					"Caminas con preocupación por las peligrosas calles de los suburbios",
					"Una rata se da un chapuzón con jolgorio en los riachuelos de aguas negras que surcan esta calzada",
					"Presencias una pelea entre varios goblins, a uno de ellos le están dando la paliza de su vida",
					"Los guardias ignoran por completo las atrocidades que se cometen en estas barriadas",
					"Una patrulla orca avanza con arrogancia entre los plebeyos, que se apartan atemorizados",
					"Escuchas cristalres romperse en la lejanía",
				}));
			}
			
			if (!random(4)) {
				habitacion->add_clone(NPCS + "maton_orco", 1 + random(3));
			}
			if (!random(4)) {
				habitacion->add_clone(NPCS + "pordiosero", 1);
			}
			if (!random(5)) {
				habitacion->add_clone(NPCS + "cazarrecompensas", 1);
			}
			if (!random(8)) {
				habitacion->add_clone(NPCS + "antorchero", 1);
			}
			if (!random(9)) {
				habitacion->add_clone(NPCS + "doctor", 1);
			}
			if (!random(9)) {
				habitacion->add_clone("/baseobs/monstruos/versionados/rata_negra", 1);
			}
			break;
		case "ejecuciones":
			habitacion->set_short("%^RED%^Suburbios de %^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Pared de Ejecuciones");
			habitacion->set_long("Estás en un lugar de los suburbios que recibe su nombre "
			"de la larga pared que bordea toda la cara oriental de la barriada. Esta pared, "
			"conocida por ser el \"paredón\" habitual en las ejecuciones sumarias de la "
			"Horda Negra, es parte del muro que delimita el bastión de la misma "
			"organización. Aquí la vía es más ancha que en el resto de los suburbios para "
			"que los verdugos tengan espacio suficiente como para desplegar sus "
			"maravillosas artes. Hay numerosas bocas de alcantarilla para que los ríos "
			"de sangre puedan seguir su curso natural hasta las entrañas de la fortaleza. "
			"Cabe resaltar que las ejecuciones que se desempeñan aquí son sobre víctimas "
			"de segunda categoría. Las ejecuciones más importantes se dan siempre en "
			"el bastión de la Horda Negra.\n");
			habitacion->add_item("pared", "Aunque esta pared (o, mejor dicho, muro) da nombre "
			"al lugar, no es en la pared donde se producen las ejecuciones, sino que suelen "
			"acontecer en mitad de la calle.\n");
			habitacion->add_item(({"boca", "bocas", "alcantarillas", "alcantarilla"}), "Son "
			"numerosas y están repartidas a lo largo y ancho de esta calle. Tendrás que ir "
			"mirándolas una por una si acaso esperas descubrir una \"entrada secreta\" (por "
			"supuesto, no la hay).\n");
			habitacion->add_item("suelo", "A diferencia del suelo del resto de los "
			"suburbios, aquí no circula la porquería sino la sangre.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(80);
			habitacion->set_zone("suburbios");
			habitacion->fijar_frecuencia_sensaciones(10);
			if (CLIMA->query_noche()) {
				habitacion->fijar_sensaciones(({
					"Caminas con preocupación por las peligrosas calles de los suburbios",
					"Escuchas griterío proviniente de algún lugar cercano; las peleas y los asesinatos son comunes en estos lares",
					"Un goblin borracho canturrea viejas canciones de guerra mientras lucha por no ahogarse en su propio vómito",
					"Una rata se da un chapuzón con jolgorio en los riachuelos de aguas negras que surcan esta calzada",
					"A estas horas es mejor pasar desapercibido. Procuras no llamar mucho la atención",
				}));
			} else {
				habitacion->fijar_sensaciones(({
					"Caminas con preocupación por las peligrosas calles de los suburbios",
					"Una rata se da un chapuzón con jolgorio en los riachuelos de aguas negras que surcan esta calzada",
					"Presencias una pelea entre varios goblins, a uno de ellos le están dando la paliza de su vida",
					"Los guardias ignoran por completo las atrocidades que se cometen en estas barriadas",
					"Una patrulla orca avanza con arrogancia entre los plebeyos, que se apartan atemorizados",
					"Escuchas cristales romperse en la lejanía",
					"Un orco de aspecto amenazador te mantiene la mirada desafiantes",
				}));
			}
			if (!random(4)) {
				habitacion->add_clone(NPCS + "pordiosero", 1);
			}
			if (!random(3)) {
				habitacion->add_clone(NPCS + "cazarrecompensas", 1);
			}
			if (!random(8)) {
				habitacion->add_clone(NPCS + "antorchero", 1);
			}
			if (!random(9)) {
				habitacion->add_clone(NPCS + "doctor", 1);
			}
			break;
		case "bazar":
			habitacion->set_short("%^RED%^Suburbios de %^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Gran Bazar Trasgo");
			habitacion->set_long("Si hay un lugar donde las criaturas de Lord Gurthang ponen "
			"de lado sus diferencias y se unen para perseguir un bien común... ese lugar "
			"sería sin duda el bazar. Y el bien perseguido, ¡sería el dinero! Incluso a "
			"veces se han llegado a ver humanos vagabundos comerciando por estos lares. "
			"De apariencia menos amenazadora que el resto de los suburbios, el bazar hace "
			"gala de un repertorio de colores y variedad que choca con la monotonía gris y "
			"marrón de las tierras yermas de Arad Gorthor. Aquí, más que en edificios, los "
			"comerciantes prefieren usar pequeños tenderetes para vender sus mercancías.\n");
			habitacion->set_night_long("¿No es maravillosa la anarquía inherente a estas "
			"razas? A diferencia del mundo civilizado, aquí puedes ir a comprar incluso "
			"en mitad de la noche. El bazar rebosa actividad incluso bien entrada la "
			"madrugada, aunque los clientes de entonces tienen una reputación bastante "
			"más... dudosa. Los tenderetes están iluminados por coloridas lámparas que "
			"penden de cuerdas que van atadas desde cada uno de los edificios hasta el "
			"que se encuentra enfrente.\n");
			habitacion->add_item("tenderete", "Son tenderetes destartalados donde los "
			"mercaderes venden su género. Muchos de ellos no son más que carromatos "
			"adaptados para la función.\n");
			habitacion->add_item("lamparas", "Son lamparas pendientes que se mueven de "
			"un lado a otro al más mínimo soplido de viento.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(80);
			habitacion->set_zone("bazar");
			habitacion->fijar_frecuencia_sensaciones(50);
			if (CLIMA->query_noche()) {
				habitacion->fijar_sensaciones(({
					"Rechazas la fruta podrida que un goblin de dientes podridos intenta venderte",
					"Apartas a golpes a la chusma que se amontona en este bazar",
					"Un orco mantiene una acalorada discusión con un cliente, pero se termina rápidamente cuando le abre la cabeza con un pedrolo",
					"Un orco bastante pesado te muestra su género, pero haces como si no lo hubieras visto",
					"Alguien te reclama a gritos que le compres unas túnicas de muy dudosa calidad",
					"Notas una mano palpando tus pantalones. Ándate con ojo, este bazar está lleno de pícaros y ladronzuelos",
					"Este bazar es una especie de competición por ver quién grita más. La cabeza te empieza a doler",
					"Una vieja y chiflada goblin te tira de la ropa para llamar tu atención, pero la apartas de un empujón y cae rodando al suelo",
					"Un transeunte te mira con muy malos ojos, parece andar buscando pelea",
					"Un goblin sale corriendo entre la muchedumbre con una sandía bajo el brazo, si no le pillan será su día de suerte",
					"Unas campanas provinientes de uno de los tenderetes anuncian una jugosa oferta",
					"Por las noches, despunta la venta de bebidas alcohólicas. Lo puedes notar en el ambiente",
				}));
			} else {
				habitacion->fijar_sensaciones(({
					"Rechazas la fruta podrida que un goblin de dientes podridos intenta venderte",
					"Apartas a golpes a la chusma que se amontona en este bazar",
					"Un orco mantiene una acalorada discusión con un cliente, pero se termina rápidamente cuando le abre la cabeza con un pedrolo",
					"Un orco bastante pesado te muestra su género, pero haces como si no lo hubieras visto",
					"Alguien te reclama a gritos que le compres unas túnicas de muy dudosa calidad",
					"Notas una mano palpando tus pantalones. Ándate con ojo, este bazar está lleno de pícaros y ladronzuelos",
					"Este bazar es una especie de competición por ver quién grita más. La cabeza te empieza a doler",
					"Una vieja y chiflada goblin te tira de la ropa para llamar tu atención, pero la apartas de un empujón y cae rodando al suelo",
					"Un transeunte te mira con muy malos ojos, parece andar buscando pelea",
					"Un goblin sale corriendo entre la muchedumbre con una sandía bajo el brazo, si no le pillan será su día de suerte",
					"Unas campanas provinientes de uno de los tenderetes anuncian una jugosa oferta",
				}));
			}
			if (!random(4)) {
				habitacion->add_clone(NPCS + "pordiosero", 1);
			}
			if (!random(6)) {
				habitacion->add_clone(NPCS + "cazarrecompensas", 1);
			}
			if (!random(8)) {
				habitacion->add_clone(NPCS + "antorchero", 1);
			}
			if (!random(9)) {
				habitacion->add_clone(NPCS + "doctor", 1);
			}
			break;
		case "herejes":
			habitacion->set_short("%^RED%^Suburbios de %^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Altar de los Herejes");
			habitacion->set_long("En este estrecho callejón, rodeado por las tiendas del Gran Bazar Trasgo, se alza "
			"un diminuto altar rodeado de pequeñas ofrendas un tanto atípicas para Gurthang, flores, frutos del campo "
			"y del bosque, y algunas viandas propias de viajeros. Resulta sorprendente y fuera de lugar un altar así "
			"en un sitio como éste. Quizá se trate de un acto de acercamiento a los extraños visitantes que en ocasiones "
			"acuden al bazar, una actitud inédita entre los trasgos.\n");
			habitacion->add_item(({"ofrenda","ofrendas","fruto","frutos","flor","flores","vianda","viandas"}), 
			"Ves muy distintos objetos, propios de un culto menos sangriento que la servidumbre a Gurthang, resulta "
			"sospechoso y fuera de lugar.");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(60);
			break;
		case "metalurgia_entrada":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Umbral del Templo de la Metalurgia");
			habitacion->set_long("La entrada al Templo de la Metalurgia es una espectacular arcada de doble hoja de bronce envejecido "
				"que mantiene sus puertas constantemente abiertas y de las que emana el acre olor del azufre con la intensidad "
				"suficiente como para hacerte estornudar. Está flanqueada por enormes antorchas forjadas con pie que se sujetan tanto en "
				"el quicio de la impresionante entrada como en la pared que en la acompaña.\n");
			habitacion->add_item(({"antorcha","antorchas"}), "A izquierda y derecha de la puerta, ves dos grupos de tres lanzas "
				"gigantescas de hierro, ancladas al suelo que se cruzan dibujando "
				"un trípode, hundido en la tierra en la parte inferior y que sustenta un caldero casi plano de aceite ardiendo. El punto central "
				"donde se tocan las tres pilastras de hierro, una figura de forma draconiana las ata entre sí para evitar que se "
				"desprendan.");
			habitacion->add_item(({"figura","dragon","draconiana"}), "La figura que une las pilastras de la gigantesca antorcha tiene la forma "
				"de un dragón gusano, alargado y sin alas pero con una fiereza inusual en su rostro, brota desde el quicio de la puerta, como si "
				"hubiera atravesado el muro y saliera de él. Sus garras inferiores se agarran al trípode como si fueran un único metal y tras "
				"rodear por dos veces el tronco superior de la figura se entrecruza con las lanzas para apoyarse, como si intentara separar dos de ellas, "
				"y asomar su rostro maligno hacia la pared, contra la cual una llama férrea choca con furia tras brotar de las fauces de la escultura.");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "metalurgia":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Templo de la Metalurgia");
			habitacion->set_long("Te encuentras en el Templo de la Metalurgia, corazón industrial del Bastión de Arad Gorthor. "
				"Durante cada minuto del día durante años estas fraguas han alimentado las manos desnudas del ejército de Golthur con hojas mortales "
				"y aceradas. El rítmico repiqueteo de los martillos llenaba con su estruendo las proximidades y, según dicen, cuando pasaban las horas "
				"los herreros que comenzaban compitiendo entre sí por tener un ritmo más rápido se unían todos a la misma velocidad. Las decenas de "
				"martillos golpeando el metal al unísono parecía el latir del corazón de un gigante terrorífico. Aún hoy algunos de los testigos "
				"de la época más gloriosa del bastión creen oir los el choque del metal contra el metal cuando pasan por las fraguas.\n");
      habitacion->fijar_frecuencia_sensaciones(10);
      habitacion->fijar_sensaciones(({
        "Como si fuera el eco de otra época parece que oyes el trabajo de la forja",
        "Parece que alguien te llama silbando, como si estuviera hacia el sur",
        "El silencio que se ha hecho de repente hace resonar tus pasos como si un ejército entero cruzase a tu lado"
      }));

			habitacion->set_exit_color("rojo");
			habitacion->fijar_luz(90);
			break;
    case "metalurgia_forja":
      habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Forja Agotada del Templo de la Metalurgia");
      habitacion->set_long("Lo que antes debió ser una gigantesca fragua capaz de albergar el trabajo de una docena de herreros a la vez "
				"ahora mismo no es más que un montón de rocas caídas por el puro abandono sobre el foso que antes alimentaba la fragua, todavía "
				"se puede ver cómo a través de los huecos entre ellas sale la luz del río de lava subterráneo. Desgraciadamente el descomunal tamaño "
				"de las rocas que lo taponan hacen imposible considerar volver a colocarlas en su sitio.\n");
			habitacion->fijar_frecuencia_sensaciones(10);
			habitacion->fijar_sensaciones(({
				"Al escapar los gases generados por el río subterráneo de lava entre los resquicios de las rocas que lo taponan producen un curioso silbido",
				"El extremo calor de la estancia hacen brotar profusos ríos de sudor por tu frente y mejillas",
			}));
      habitacion->set_exit_color("rojo");
      habitacion->fijar_luz(90);
      break;
	case "metalurgia_forja_activa":
      habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Forja del Templo de la Metalurgia");
      habitacion->set_long("Ante ti tienes una enorme fragua, donde una docena de herreros pueden trabajar comodamente. Puedes apreciar "
							"que fue construida hace mucho tiempo, pero al no haber cesado su actividad se conserva bien. "
							"Cerca de la fragua hay un foso, donde puedes ver el rio de lava subterráneo de donde obtiene "
							"el calor necesario para funcionar.\n");
			habitacion->fijar_frecuencia_sensaciones(10);
			habitacion->add_item(({"foso","fragua"}), "No puedes hacerte una idea de qué clase de criatura acciona el mecanismo que permite activar la fragua para alcanzar "
				"la temperatura adecuada aumentando el flujo de la lava en el foso, pero tienes claro que no te gustaría que se te sentara encima.");
			habitacion->fijar_sensaciones(({
				"Los gases generados por el río subterráneo producen un extraño olor en la estancia.",
				"El extremo calor de la estancia hacen brotar profusos ríos de sudor por tu frente y mejillas",
			}));
      habitacion->set_exit_color("rojo");
      habitacion->fijar_luz(90);
      break;
		case "brujeria_entrada":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Portal de la Torre de Brujería");
			habitacion->set_long("La más tétrica de tus pesadillas es un juego de niños en comparación con la "
			"retorcida mente que ha ingeniado el esperpéntico portal que se alza frente a tí custodiando el "
			"acceso a la Torre de Brujería. Rocas sobre rocas, amontonadas en un caótico orden junto con cráneos "
			"y otros huesos mal triturados de todos los tamaños y razas como pasta para unirlas, fibras musculares, nervios y venas "
			"animadas por una aberrante magia forman una red alrededor para sustentar en precario equilibrio la "
			"amalgama de carne y piedra. La forma que se ha conseguido poco tiene que ver con una torre sino más bien una "
			"calabaza bombacha, casi esférica, de dura roca, hueso, músculo, vena y ligamento que casi parece respirar, "
			"de tanta vida como palpita en sus venas. La \"torre\" está coronada por un techo picudo de ramas grisáceas "
			"que se entretejen formando un conjunto impenetrable.\n");
			habitacion->add_item(({"vena","venas","hueso","huesos","musculo","musculos","ligamento","ligamentos","nervio","nervios"}),
			"La torre de brujería asemeja una bola viviente de carne, roca y vísceras que se hincha y deshincha con pausada "
			"respiración. Se dice que la magia que alimenta este engendro proviene de pactos con demonios que "
			"establecieron los chamanes y magos para beneficio mutuo, unos aprendieron, otros se cobraron sus servicios con "
			"las almas de los muertos en los combates de la Puerta Oeste del bastión.\n");
			habitacion->add_item(({"roca","rocas"}), "La mayor parte de la torre está formada por rocas, pero se unen y entrelazan "
			"mediante el uso de venas, músculos y nervios. Los huesos mal triturados hacen el papel de argamasa rellenando las "
			"juntas entre las rocas para proteger el interior.\n");
			habitacion->fijar_frecuencia_sensaciones(10); 
			habitacion->fijar_sensaciones(({
				"Un suspiro agónico parece brotar de la torre poniéndote la piel de gallina",
				"La torre parece sacudirse, como si se agitara incómoda sobre su asiento",
				"Un quejumbroso quejido brota de las piedras congelando tu alma"
			}));
			habitacion->set_exit_color("verde");
			habitacion->fijar_luz(90);
			break;
		case "brujeria":
			habitacion->set_short("%^BLACK%^BOLD%^Torre de Brujería de Arad Gorthor%^RESET%^");
			habitacion->set_long("La torre de brujería por dentro es sin duda muy diferente al exterior, sólamente "
			"se aprecia roca en sus paredes y el techo, abierto en su centro como un observatorio, permite la total "
			"influencia de Velian sobre los brujos que circulan en silenciosos murmullos de un estante a otro, cuando "
			"no se sepultan bajo innumerables volúmenes en los toscos escritorios que poblan las paredes.\n");
			habitacion->add_item(({"escritorio", "escritorios"}), 
				"La mayoría están ocupados por montañas de libros que debe estar consultando algún aprendiz y encarados "
				"hacia la pared en los distintos puntos donde las velas que pueblan la pared iluminan lo suficiente para "
				"la lectura. La forma esférica de la torre permite estar situados en un espacio abierto y la vez protegidos "
				"de las inclemencias del tiempo.\n");
			habitacion->add_item(({"roca", "rocas"}), "Las paredes interiores de la torre son cóncavas pero completamente "
				"lisas, gracia conseguida sin duda por la magia pues no hay mano orca, gnoll, goblinoide o kobold capaz de "
				"trabajar tan finamente la piedra.\n");
			habitacion->add_item("techo", "Una enorme abertura circular en el techo permite ver el cielo y la disposición de "
				"las lunas cuando llega la noche.\n");
			habitacion->add_item(({"volumenes","volumen","libro","libros","estante","estantes","estanteria","estanterias"}),
				"Las paredes, allá donde no hay escritorios, están pobladas por estanterías completamente abarrotadas por "
				"incontables libros, de todos los tamaños, colores, libros viejos, libros aún más viejos y libros tan viejos"
				" que da miedo tocarlos por temor a que no se deshagan en tus manos como polvo.\n");
			habitacion->set_exit_color("verde");
			habitacion->fijar_luz(90);
			break;
		/*	
		case "horda_entrada":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Entrada al Bastión de la Horda Negra");
			habitacion->set_long("Dos colmillos gigantescos, de la altura de dos trolls, trabajados con intrincados "
			"dibujos de guerra y muerte y atados por la punta el uno al otro, forman el esplendoroso portal que da "
			"acceso al Bastión de la Horda Negra. Ampliamente pueden pasar bajo el arco hasta tres grandes orcos sin "
			"necesidad de tocarse de no ser por el tosco tocón de algún árbol milenario ya muerto que se encuentra en "
			"el centro del paso. Clavadas en su centro se ven varias armas de lujosa apariencia. Si miras hacia el interior "
			"del bastión un largo pasillo de considerables dimensiones crece hacia el norte.\n");
			habitacion->add_item(({"colmillo","colmillos"}), 
				"Cada colmillo representa a una de las escuadras que se dedican a proteger las atalayas de la Puerta Oeste, "
				"en ellos están grabados las respectivas glorias obtenidas en combate convenientemente exageradas, por lo que "
				"puedes apreciar escenas en las que un ballestero atraviesa cuatro ojos de cuatro víctimas distintas de un sólo "
				"disparo o cómo un goblin de diminuto tamaño arroja por encima de su cabeza dos caballos con sendos paladines "
				"montados. Sin duda representaciones totalmente fiables de lo ocurrido en el campo de batalla.\n");
			habitacion->add_item(({"tocon","tronco","arbol","armas"}), 
				"Las armas clavadas en el tocón que custodia la entrada al bastión son consideradas casi sagradas en el ámbito "
				"militar de la Horda Negra, aunque frecuentemente ridiculizadas por sacerdotes y brujos. Cada una de ellas "
				"perteneció a un soldado tan hábil que una vez muerto el Maestro de Armas, que entrena "
				"las fuerzas armadas, fue relevado por este soldado que \"clava su arma\" para tener las manos libres para formar a los recién llegados, generalmente en forma de tremendos tortazos capaces de erradicar la indisciplina durante largas temporadas.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;*/
		case "horda_entrada":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Entrada al Bastión de la Horda Negra");
			habitacion->set_long("Al norte de aquí se encontraba antiguamente el Bastión de la Horda Negra. El bastión "
			"de la Horda Negra era un sencillo campamento a cielo abierto, donde los miembros de la Horda realizaban sus "
			"entrenamientos. "
			"Puedes ver dos colmillos gigantescos, de la altura de dos trolls, trabajados con intrincados "
			"dibujos de guerra y muerte, que probablemente formaban un arco que daba entrada al campamento. "
			"Sin duda tres grandes orcos podían pasar bajo el arco sin "
			"necesidad de tocarse de no ser por el tosco tocón de algún árbol milenario ya muerto que se encuentra en "
			"el centro del paso. Hacia el interior del bastión ves lo que probablemente fuera un amplio pasillo que se "
			"dirigía hacia el norte.\n");
			habitacion->add_item(({"colmillo","colmillos"}), 
				"Cada colmillo representaba a una de las escuadras que se dedican a proteger las atalayas de la Puerta Oeste, "
				"en ellos están grabados las respectivas glorias obtenidas en combate convenientemente exageradas, por lo que "
				"puedes apreciar escenas en las que un ballestero atraviesa cuatro ojos de cuatro víctimas distintas de un sólo "
				"disparo o cómo un goblin de diminuto tamaño arroja por encima de su cabeza dos caballos con sendos paladines "
				"montados. Sin duda representaciones totalmente fiables de lo ocurrido en el campo de batalla.\n");
			habitacion->add_item(({"TOKEN","tocon","tronco","arbol"}), 
				"Un viejo tocón, que custodiaba la entrada al bastión, y aún conserva las marcas de las armas que se clavaban en él. "
				"Estas armas eran consideradas casi sagradas en el ámbito "
				"militar de la Horda Negra, aunque frecuentemente ridiculizadas por sacerdotes y brujos. Cada una de ellas "
				"perteneció a un soldado tan hábil que llegó a ocupar el puesto de Maestro de Armas, el encargado del entrenamiento de "
				"las fuerzas armadas. El Maestro de Armas clavaba aquí su arma para tener las manos libres para formar "
				"a los recién llegados, generalmente en forma de tremendos tortazos capaces de erradicar la indisciplina durante "
				"largas temporadas.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
		case "horda":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor:%^RESET%^ Bastión de la Horda Negra%^RESET%^");
			habitacion->set_long("El bastión de la Horda Negra es un sencillo campamento de cielo abierto dónde los "
				"Golthur-Hai habían llevado a cabo los primeros entrenamientos en el ejercicio bélico. Está rodeado por "
				"una enredada maraña de hierros y plantas tan efectiva para evitar la entrada como la salida. No es raro "
				"ver manchas de sangre o miembros cercenados tirados en un rincón, generalmente roídos por algún oportunista "
				"hambriento. El frío y el sol, cayendo alternativamente sin protección ninguna durante años en los suelos "
				"del bastión han dejado su huella con rabia, dejando un suelo horadado por las grietas y cuarteado por la sequedad.\n");
			habitacion->set_exit_color("rojo");
			habitacion->fijar_luz(90);
			break;
		default:
		case "bastion":
			habitacion->set_short("%^BLACK%^BOLD%^Arad Gorthor%^RESET%^");
			habitacion->set_long("Arad Gorthor es una ciudad principalmente militar, su estructura, metódica y orientada a la defensa "
				"revela un trabajo mejorado con el paso de los años y el aprendizaje a través de grandes victorias y penosas derrotas, "
				"sobretodo de éstas últimas. Tiene una forma casi cuadrada si se pudiera ver desde el aire y unos altos muros que protegen "
				"su interior, dos gigantescas puertas, de este a oeste, protegen del las invasiones y sirven como puesto avanzado contra "
				"el reino de Eldor.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			break;
	}
}
