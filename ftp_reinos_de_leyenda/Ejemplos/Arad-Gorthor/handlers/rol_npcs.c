// Dunkelheit 10-11-2009

#include "../path.h"

varargs void definir(object quien, string tipo)
{
	tipo = tipo ? tipo : "general";
	
	switch (tipo)
	{
		case "general":
		default:
			quien->add_loved("religion", "gurthang");
			quien->set_aggressive(1);
			break;
	}
}
