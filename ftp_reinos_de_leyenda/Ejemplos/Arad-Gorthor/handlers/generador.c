// Nohur 26/01/2019
#include "../path.h"
#include <clean_up.h>
#include <tiempo.h>
void create() {
    mi_euid();
}
// Dado un texto "hola_amigos" devuelve "Hola amigos"
string shortify(string tx) {
    return capitalize(trim(replace(tx, "_", " ")));
}
// Dado un nombre de zona lo convierte en el short correspondiente
string dame_short(string subzona) {
    return shortify(subzona);
}
string dame_short_zona(string zona, string subzona)  {
    return capitalize(zona);
}
// Dado un nombre de zona, devuelvezonael nombre de la frontera asociada a la misma
string dame_zona_frontera(string tipo) {
    return dame_short(tipo);
}
// Devuelve el nombre de estación. Permito enmascararla para depurar
varargs string dame_estacion(object room) {
    return DAME_ESTACION(room);
}
// Establece la luz de la sala
void poner_luz(object sala, string zona, string subzona) {
    sala->fijar_luz(sala->query_outside() ? 100 : 50);
}
// Establece la frontera de la sala (puede que las iniciales ya estén en /d/eradia/fronteras.c)
void poner_fronteras(object sala, string zona, string subzona) {
}
// Establece el color de las salidas
void poner_color_salidas(object sala, string zona, string subzona) {
}
// Establece el short de la sala
void poner_short(object sala, string zona, string subzona) {
    // Para evitar que coja por defecto el short "en el agua" de /std/agua.c
    if ( sala->dame_agua() && sala->query_short() == "En el agua" )
        sala->set_short(0);

    if ( sala->query_short() )
        sala->set_short(sala->query_short() + ": " + dame_short(subzona));
    else if ( dame_short(subzona) )
        sala->set_short(dame_short_zona(zona, subzona) + ": " + dame_short(subzona));
    else
        sala->set_short(dame_short_zona(zona, subzona));
}
// Establece la descripción de la sala
void poner_descripciones(object sala, string zona, string subzona) {
    sala->fijar_fichero_descripciones(RUTA_DESCRIPCIONES(sala, zona, subzona));
}


// Añade items de la sala
void poner_items(object sala, string zona, string subzona) {
}
// PNJs
void poner_npcs(object sala, string zona, string subzona) {
}
// Cualquier otra cosa
void poner_especiales(object sala, string zona, string subzona) {

}

// Genera una sala
void generar_sala(object sala, string zona, string subzona)
{
    sala->set_zone(subzona);

    poner_color_salidas(sala, zona, subzona);
    poner_luz(sala, zona, subzona);
    poner_fronteras(sala, zona, subzona);

    poner_short(sala, zona,subzona);
    poner_descripciones(sala, zona, subzona);

    poner_items(sala, zona, subzona);
    poner_npcs(sala, zona, subzona);

    poner_especiales(sala, zona, subzona);
}
// Dado una sala, intenta cargar un handler para la zona a la que pertenece.
// Si existe, genera la sala con ese handler. Si no, usará este propio.
// Es el sistema de "punto de entrada único" para la generación de rooms en eradia.
void generar_sala_especifica(object sala, string zona, string subzona) {
    object handler;

    if ( ! handler = load_object(GENERADORES + zona + ".c") ) {
        generar_sala(sala, zona, subzona);
        return;
    }

    return handler->generar_sala(sala, zona, subzona);
}
