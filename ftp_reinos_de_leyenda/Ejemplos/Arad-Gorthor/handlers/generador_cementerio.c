// Dunkelheit 05-11-2009

#include "../path.h"

void set_zones(string *zonas, object npc)
{
	npc->set_zones(zonas);
}

void describir(object habitacion, string tipo)
{
	switch (tipo)
	{
		case "cienaga":
			habitacion->set_short("%^GREEN%^Ciénaga Séptica%^RESET%^");
			habitacion->set_long("Ante tus pies se encuentran las aguas fecales de todo un "
			"imperio. Un mar de heces y orín se extiende hasta perderse de vista entre sus "
			"propios vapores tóxicos, que reducen la visibilidad drásticamente y dan una "
			"tonalidad verdosa-marrón a todo cuanto rodean. La profundidad de esta ciénaga... "
			"prefieres no saberla; pero por suerte es lo suficientemente sólida como para "
			"poder andar sin problema alguno, siempre y cuando estar de mierda hasta la "
			"cintura no sea un problema para ti. En dirección sudeste, fuera ya de este "
			"insalubre trampal, atinas a ver las montañas de basura del vertedero de "
			"Golthur Orod. En algún lugar del centro de la ciénaga se distingue una "
			"pequeña estructura.\n");
			habitacion->add_smell(({"cienaga", "aguas", "heces", "caca", "mierda"}), "¡Puag! "
			"¡es la mierda de más de un millón de orcos!\n");
			habitacion->add_item(({"cienaga", "aguas", "heces"}), "Metes un dedo en la ciénaga "
			"y acto seguido te lo llevas a la boca. No hay duda: es mierda de orco.\n");
			habitacion->add_item(({"montañas", "basura", "vertedero"}), "Es el vertedero de "
			"Golthur, una montaña de basura no tan desagradable como la ciénaga en la que te "
			"encuentras.\n");
			habitacion->add_item("estructura", "Desde aquí parece una choza, pero con esta "
			"niebla fétida no podrías asegurarlo. ¡Quizá sea un Dragón Fecal!\n");
			habitacion->add_item(({"niebla", "vapor", "vapores"}), "Una densa niebla del "
			"mismo color que la ciénaga lo inunda todo. Seguramente sea producto de la "
			"descomposición de las heces.\n");
			habitacion->set_exit_color("azul");
			habitacion->fijar_luz(90);
			habitacion->fijar_frecuencia_sensaciones(40);
			habitacion->fijar_sensaciones(({
				"Las heces de la ciénaga burbujean como si de un guiso a fuego lento se tratasen",
				"El hedor es sencillamente inhumano, maldices el día en que se te ocurrió aventurarte en esta ciénaga",
				"Los mosquitos se vuelven locos ante tu presencia, sin duda eres el plato fuerte del día",
				"Moverse por una ciénaga de excrementos es agotador; estás deseando salir de aquí cuanto antes",
				"No puedes evitar preguntarte qué se te ha perdido entre varias toneladas de mierda",
			}));
			habitacion->set_zone("cienaga"); //Para evitar que los npcs se muevan fuera de la ciénaga
			habitacion->add_clone(BMONSTRUOS + "mosquito", 8 + random(6),(:set_zones,({"cienaga"}):));
			break;
		case "vertedero":
			habitacion->set_short("%^GREEN%^Vertedero Golthur-hai%^RESET%^");
			habitacion->set_long("Ya puedes morir con la tranquilidad de haber contemplado la "
			"mayor catástrofe ecológica que Eirea ha sufrido en su historia. El camino en el que "
			"te encuentras parte en dos una cordillera de desperdicios de todo tipo, procedentes "
			"de la infame Fortaleza Negra de Golthur Orod. Los millones de orcos que allí habitan "
			"generan basura a un ritmo frenético y, por supuesto, esa basura tiene que ir a parar "
			"a algún lado. La desértica llanura noroccidental de Arad Gorthor -próxima a la meseta "
			"oriental del Erial de los Condenados- fue el lugar idóneo.\n");
			habitacion->add_item("camino", "El camino serpentea a través de las montañas de porquería. "
			"Es muy habitual que haya desprendimientos, así que ándate con ojo. Las enfermedades que "
			"puedes coger aquí no están escritas todavía.\n");
			habitacion->add_item(({"cordillera", "basura", "vertedero"}), "Enormes colinas de basura, "
			"tan altas que desde aquí es imposible ver la cima. Sirven de alimento para todo tipo "
			"de aves y roedores. Su contenido es lo más singular, y créeme, no quieras saber más.\n");
			habitacion->set_exit_color("amarillo_flojo");
			habitacion->fijar_luz(90);
			habitacion->fijar_frecuencia_sensaciones(15);
			habitacion->fijar_sensaciones(({
				"Hambrientas y sucias aves carroñeras sobrevuelan las colinas de basura",
				"Percibes un movimiento entre la basura con el rabillo del ojo. Confías en que sólo sea una rata",
				"Ráfagas de hediondo aire caliente llegan del norte; nada bueno debe suceder en esa dirección",
			}));
			if (random(2)) {
				habitacion->add_clone(BMONSTRUOS + "mosquito", 1,(:set_zones,({"cienaga"}):));
			}
			break;
		case "monticulo":
			habitacion->set_short("%^ORANGE%^Cementerio de Armas de Asedio: Montículo%^RESET%^");
			habitacion->set_long("Te encuentras en una privilegiada posición dentro de "
			"este monumental cementerio de armas de asedio. Desde este pequeño collado "
			"puedes disfrutar del apocalíptico paisaje que te rodea: torres de asedio, "
			"trabuquetes, catapultas y onagros, todos ellos en ruinas. Por desgracia, "
			"aquí eres un blanco perfecto para cualquiera de las muchas criaturas que "
			"merodean por aquí buscando fortuna.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			if (!random(6)) {
				habitacion->add_clone(NPCS+"salteador_goblin", 1);
			} else if (!random(8)) {
				habitacion->add_clone(NPCS+"galeb_duhr", 1);
			} else if (!random(8)) {
				habitacion->add_clone("/baseobs/monstruos/animales/coyote", 1);
			}
			if (!random(4)) {
				switch (random(4)) {
					case 3:
						habitacion->add_clone(OBJETOS+"trabuquete", 1);
						break;
					case 2:
						habitacion->add_clone(OBJETOS+"ariete", 1);
						break;
					case 1:
						habitacion->add_clone(OBJETOS+"onagro", 1);
						break;
					default:
					case 0:
						habitacion->add_clone(OBJETOS+"catapulta", 1);
						break;
				}
			}
			break;

		case "norte":
			habitacion->set_short("%^ORANGE%^Cementerio de Armas de Asedio%^RESET%^");
			habitacion->set_long("En este extenso y árido llano han encontrado el "
			"descanso eterno las variopintas máquinas de asedio del los golthur-hai. "
			"En tiempos más nobles, esta zona estaba ocupada por las Montañas del "
			"Jabalí; pero el Cataclismo de la Era 2ª, que culminó con una violenta "
			"erupción del volcán de N'argh, inundó de magma las montañas y el valle "
			"que las cruzaba, el -ya de por sí desértico- Valle de la Desolación. "
			"Los orcos encontraron en la llanura formada el lugar idóneo para "
			"deshacerse de sus pesadas armas de asedio: catapultas, onagros, "
			"arietes, trabuquetes y torres de asalto. Sus ruinosas siluetas "
			"ya no dibujan un horizonte propio del de una incipiente batalla, sino más "
			"bien un panorama apocalíptico. Desde aquí ya puede verse el infame bastión "
			"de Arad Gorthor, la fortaleza construída por Lord Gurthang y que sirve de "
			"cuartel general para la Horda Negra alzándose temible por el este. En dirección noroeste hay todavía "
			"más ruinas, aunque éstas no son de máquinas de asedio...\n");
			habitacion->add_item(({"maquinas", "armas"}), "Hay desde pequeños arietes "
			"hasta gigantescos trabuquetes, descomunales máquinas de derribar castillos. "
			"Todas ellas se encuentran en ruinas.\n");
			habitacion->add_item("suelo", "El suelo es de tierra y bastante irregular, con "
			"numerosas depresiones y montículos. Parece haber sido removido por aquí y "
			"por allá.\n");
			habitacion->add_item(({"siluetas", "horizonte"}), "El horizonte está dominado "
			"por el bastión de Arad Gorthor.\n");
			habitacion->add_item(({"terreno", "monticulos", "depresiones"}), "Es un "
			"terreno irregular en el que puede esconderse cualquier tipo de peligros.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			if (!random(6)) {
				habitacion->add_clone(NPCS+"salteador_goblin", 1);
			} else if (!random(8)) {
				habitacion->add_clone(NPCS+"galeb_duhr", 1);
			} else if (!random(6)) {
				habitacion->add_clone("/baseobs/monstruos/animales/coyote", 1);
			}
			if (!random(4)) {
				switch (random(4)) {
					case 3:
						habitacion->add_clone(OBJETOS+"trabuquete", 1);
						break;
					case 2:
						habitacion->add_clone(OBJETOS+"ariete", 1);
						break;
					case 1:
						habitacion->add_clone(OBJETOS+"onagro", 1);
						break;
					default:
					case 0:
						habitacion->add_clone(OBJETOS+"catapulta", 1);
						break;
				}
			}
			habitacion->fijar_frecuencia_sensaciones(10);
			habitacion->fijar_sensaciones(({
				"El cálido aire empuja las pesadas máquinas de asedio, que tiemblan y parecen estar a punto de derrumbarse",
				"Escuchas risas pícaras y malvadas a lo lejos; sólo esperas que no sea una bestia predadora riéndose de tu desamparo",
				"El viento arrastra una ola de polvo que choca contra los restos de una de las máquinas",
			}));
			break;

		case "centro":
			habitacion->set_short("%^ORANGE%^Cementerio de Armas de Asedio%^RESET%^");
			habitacion->set_long("En este extenso y árido llano han encontrado el "
			"descanso eterno las variopintas máquinas de asedio del los golthur-hai. "
			"En tiempos más nobles, esta zona estaba ocupada por las Montañas del "
			"Jabalí; pero el Cataclismo de sla Era 2ª, que culminó con una violenta "
			"erupción del volcán de N'argh, inundó de magma las montañas y el valle "
			"que las cruzaba, el -ya de por sí desértico- Valle de la Desolación. "
			"Los orcos encontraron en la llanura formada el lugar idóneo para "
			"deshacerse de sus pesadas armas de asedio: catapultas, onagros, "
			"arietes, trabuquetes y torres de asalto. Sus ruinosas siluetas "
			"ya no dibujan un horizonte propio del de una incipiente batalla, sino más "
			"bien un panorama apocalíptico. A esta altura sólo hay escombros y ruinas a "
			"tu alrededor. No parece haber ningún camino distinguible, sólo puedes "
			"aspirar a no romperte la crisma en este irregular terreno.\n");
			habitacion->add_item(({"maquinas", "armas"}), "Hay desde pequeños arietes "
			"hasta gigantescos trabuquetes, descomunales máquinas de derribar castillos. "
			"Todas ellas se encuentran en ruinas.\n");
			habitacion->add_item("suelo", "El suelo es de tierra y bastante irregular, con "
			"numerosas depresiones y montículos. Parece haber sido removido por aquí y "
			"por allá.\n");
			habitacion->add_item(({"siluetas", "horizonte"}), "Es una visión dramática, "
			"el horizonte parece el de una ciudad reducida a escombros. Desde aquí puedes "
			"ver las zonas adyacentes al cementerio: la meseta oriental "
			"al oeste, el bastión de Arad Gorthor al noreste, y más ruinas al norte.\n");
			habitacion->add_item(({"terreno", "monticulos", "depresiones"}), "Es un "
			"terreno irregular en el que puede esconderse cualquier tipo de peligros.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			if (!random(6)) {
				habitacion->add_clone(NPCS+"salteador_goblin", 1);
			} else if (!random(8)) {
				habitacion->add_clone(NPCS+"galeb_duhr", 1);
			} else if (!random(6)) {
				habitacion->add_clone("/baseobs/monstruos/animales/coyote", 1);
			}
			if (!random(4)) {
				switch (random(4)) {
					case 3:
						habitacion->add_clone(OBJETOS+"trabuquete", 1);
						break;
					case 2:
						habitacion->add_clone(OBJETOS+"ariete", 1);
						break;
					case 1:
						habitacion->add_clone(OBJETOS+"onagro", 1);
						break;
					default:
					case 0:
						habitacion->add_clone(OBJETOS+"catapulta", 1);
						break;
				}
			}
			break;

		case "sur":
			habitacion->set_short("%^ORANGE%^Cementerio de Armas de Asedio%^RESET%^");
			habitacion->set_long("En este extenso y árido llano han encontrado el "
			"descanso eterno las variopintas máquinas de asedio del los golthur-hai. "
			"En tiempos más nobles, esta zona estaba ocupada por las Montañas del "
			"Jabalí; pero el Cataclismo de la Era 2ª, que culminó con una violenta "
			"erupción del volcán de N'argh, inundó de magma las montañas y el valle "
			"que las cruzaba, el -ya de por sí desértico- Valle de la Desolación. "
			"Los orcos encontraron en la llanura formada el lugar idóneo para "
			"deshacerse de sus pesadas armas de asedio: catapultas, onagros, "
			"arietes, trabuquetes y torres de asalto. Sus ruinosas siluetas "
			"ya no dibujan un horizonte propio del de una incipiente batalla, sino más "
			"bien un panorama apocalíptico. Este improvisado cementerio de armas de "
			"asedio se extiende hacia el norte con sus irregulares montículos y "
			"depresiones; un terreno así es ideal para las emboscadas así que será "
			"mejor andarse con ojo. Tras de ti, en dirección sur, la superficie es más "
			"regular y desértica, así hasta llegar a la frontera con Dendra.\n");
			habitacion->add_item(({"maquinas", "armas"}), "Hay desde pequeños arietes "
			"hasta gigantescos trabuquetes, descomunales máquinas de derribar castillos. "
			"Todas ellas se encuentran en ruinas.\n");
			habitacion->add_item("suelo", "El suelo es de tierra y bastante irregular, con "
			"numerosas depresiones y montículos. Parece haber sido removido por aquí y "
			"por allá.\n");
			habitacion->add_item("horizonte", "Entre las siluetas de las máquinas de "
			"asedio pueden verse las zonas adyacentes al cementerio: la meseta oriental "
			"al oeste, el bastión de Arad Gorthor al noreste, y más ruinas al norte.\n");
			habitacion->add_item("siluetas", "Es una visión dramática, el horizonte parece "
			"el de una ciudad reducida a escombros.\n");
			habitacion->add_item(({"terreno", "monticulos", "depresiones"}), "Es un "
			"terreno irregular en el que puede esconderse cualquier tipo de peligros.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			if (!random(6)) {
				habitacion->add_clone(NPCS+"salteador_goblin", 1);
			} else if (!random(8)) {
				habitacion->add_clone(NPCS+"galeb_duhr", 1);
			} else if (!random(6)) {
				habitacion->add_clone("/baseobs/monstruos/animales/coyote", 1);
			}
			if (!random(4)) {
				switch (random(4)) {
					case 3:
						habitacion->add_clone(OBJETOS+"trabuquete", 1);
						break;
					case 2:
						habitacion->add_clone(OBJETOS+"ariete", 1);
						break;
					case 1:
						habitacion->add_clone(OBJETOS+"onagro", 1);
						break;
					default:
					case 0:
						habitacion->add_clone(OBJETOS+"catapulta", 1);
						break;
				}
			}
			break;

		case "general":
		default:
			habitacion->set_short("%^ORANGE%^Cementerio de Armas de Asedio%^RESET%^");
			habitacion->set_long("En este extenso y árido llano han encontrado el "
			"descanso eterno las variopintas máquinas de asedio del los golthur-hai. "
			"En tiempos más nobles, esta zona estaba ocupada por las Montañas del "
			"Jabalí; pero el Cataclismo de la Era 2ª, que culminó con una violenta "
			"erupción del volcán de N'argh, inundó de magma las montañas y el valle "
			"que las cruzaba, el -ya de por sí desértico- Valle de la Desolación. "
			"Los orcos encontraron en la llanura formada el lugar idóneo para "
			"deshacerse de sus pesadas armas de asedio: catapultas, onagros, "
			"arietes, trabuquetes y torres de asalto. Sus ruinosas siluetas "
			"en el horizonte un panorama apocalíptico.\n");
			habitacion->add_item(({"maquinas", "armas"}), "Hay desde pequeños arietes "
			"hasta gigantescos trabuquetes, descomunales máquinas de derribar castillos. "
			"Todas ellas se encuentran en ruinas.\n");
			habitacion->add_item("suelo", "El suelo es de tierra y bastante irregular, con "
			"numerosas depresiones y montículos. Parece haber sido removido por aquí y "
			"por allá.\n");
			habitacion->add_item("horizonte", "Entre las siluetas de las máquinas de "
			"asedio pueden verse las zonas adyacentes al cementerio: la meseta oriental "
			"al oeste, el bastión de Arad Gorthor al noreste, y más ruinas al norte.\n");
			habitacion->set_exit_color("amarillo");
			habitacion->fijar_luz(90);
			if (!random(6)) {
				habitacion->add_clone(NPCS+"salteador_goblin", 1);
			} else if (!random(8)) {
				habitacion->add_clone(NPCS+"galeb_duhr", 1);
			} else if (!random(6)) {
				habitacion->add_clone("/baseobs/monstruos/animales/coyote", 1);
			}
			if (!random(4)) {
				switch (random(4)) {
					case 3:
						habitacion->add_clone(OBJETOS+"trabuquete", 1);
						break;
					case 2:
						habitacion->add_clone(OBJETOS+"ariete", 1);
						break;
					case 1:
						habitacion->add_clone(OBJETOS+"onagro", 1);
						break;
					default:
					case 0:
						habitacion->add_clone(OBJETOS+"catapulta", 1);
						break;
				}
			}
			break;
	}
}
