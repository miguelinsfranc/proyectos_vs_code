// Dunkelheit 27-10-2009

inherit "/obj/arma";

void setup()
{
	fijar_arma_base("baston");
	set_name("baston");
	set_short("Bastón Caprino");
	add_alias("caprino");
	set_main_plural("Bastones Caprinos");
	add_plural(({"bastones", "caprinos"}));
	set_long("Un bastón de madera grisácea culminado con un puño de madera que representa "
	"la cabeza de un malhumorado macho cabrío. En las cuencas de sus ojos hay dos rubíes "
	"engastados que parecen titilear con vida propia.\n");
	
	fijar_encantamiento(5);
}
