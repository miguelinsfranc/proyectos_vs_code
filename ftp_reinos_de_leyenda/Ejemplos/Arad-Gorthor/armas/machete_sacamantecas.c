// Dunkelheit 27-10-2009

inherit "/obj/arma";

void setup()
{
	fijar_arma_base("hacha de batalla");
	set_name("machete");
	set_short("Machete del Sacamantecas");
	add_alias("sacamantecas");
	set_main_plural("Machetes del Sacamantecas");
	add_plural(({"machetes", "sacamantecas"}));
	set_long("Un machete tan grande que bien podría pasar por un hacha ante los ojos profanos de "
	"quienes no han seguido el sendero de la guerra, o portada por las manos de un halfling, por "
	"aquello de las proporciones. Tiene una alargada hoja de un único filo, con la peculiaridad de "
	"que el reverso es lo suficientemente ancho como para aplastar la carne de las reses a "
	"desmantecar. El mango de madera tiene pegotes secos de grasa ensangrentada en su unión con "
	"el filo. Si fueras un orco, no tardarías en lanzarte a lamerlos.\n");
	
	remove_attack("tabla");
	add_attack("tabla", "cortante", 1, 100, 20);
	
	fijar_encantamiento(5);
}
