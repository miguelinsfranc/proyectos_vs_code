// Dunkelheit 29-07-2004
// Dunkelheit 13-03-2007 -- revisando -- un arma desproporcionada que nunca podrá ser empuñada por un jugador, sólamente por el Balrog
// Grimaek 18-05-2023 Basado en el latigo del Balrog
inherit "/obj/arma.c";
int no_catalogar_armeria() {
    return 1;
}
void setup()
{
    fijar_arma_base("latigo");
    set_name("latigo");
    set_short("Látigo %^RED%^Flamígero%^RESET%^");
    set_long("Observas el látigo flamígero de Glach'Davir, Señor de Mae'Ree. Una enorme empuñadura de un metal negro con forma de "
    "garra; y una larga tira de fuego eterno.\n");
    add_alias(({"látigo", "flamígero", "flamigero"}));
    set_main_plural("Látigos %^RED%^Flamígeros%^RESET%^");
    add_plural(({"latigos", "látigos", "flamígeros", "flamigeros"}));
    fijar_genero(1);
    fijar_encantamiento(60);
	remove_attack("tabla");
	add_attack("tabla", "lacerante", 3, 45, 36, 15, 0, 0, 0);
    add_attack("magico_fuego", "fuego", 3, 60, 42, 10, 0, 0, 0);
	add_static_property("jugador", "glachdavir");
	nuevo_efecto_basico("barrido", 5);
	nuevo_efecto_basico("antimagia", 20);
	nuevo_efecto_basico("sorbemagia", 20);
	nuevo_efecto_basico("ataques_rapidos", 5);
	
}
