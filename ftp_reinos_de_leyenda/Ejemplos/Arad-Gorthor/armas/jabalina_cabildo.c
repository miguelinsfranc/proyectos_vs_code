// Dunkelheit 05-12-2009

inherit "/obj/arma";

void setup()
{
	fijar_arma_base("jabalina");
	set_name("jabalina");
	set_short("Jabalina del Cabildo");
	add_alias("cabildo");
	set_main_plural("Jabalinas del Cabildo");
	add_plural(({"jabalinas", "cabildos"}));
	set_long("Una jabalina de madera bendita para el cabildo del templo del Lord "
	"Caído. Tiene una función más ceremonial que práctica, pues su cabeza no es de "
	"metal como la mayoría de las jabalinas modernas, sino que es de piedra. Está "
	"enganchada a la madera con intestinos resecos y plumas de buitre.\n");
	
	fijar_encantamiento(3);
}
