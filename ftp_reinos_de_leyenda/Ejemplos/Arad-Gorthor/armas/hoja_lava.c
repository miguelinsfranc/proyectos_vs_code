inherit "/obj/arma";

void setup()
{
	fijar_arma_base("espada ancha");
	set_name("hoja lava");
	set_short("Hoja de %^RED%^Lava%^RESET%^");
	set_long("Una hoja de piedra rojiza, de aproximadamente un metro de longitud, "
	"que brilla con un fulgor intenso como si estuviera incandescente. Largas gotas "
	"de piedra fundida recorren su filo hacia la empuñadura desprendiendo un calor "
	"intenso como el del propio fuego.\n");
	set_main_plural("Hojas de %^RED%^Lava%^RESET%^");
	add_alias(({"hoja","lava"}));
	add_plural(({"hojas"}));
	fijar_genero(2);
	fijar_material(6); //Piedra
	fijar_encantamiento(2);
	add_static_property("fuego", 10);
	add_static_property("agua", -10);
	add_attack("secundario", "fuego", 2, 50, 50, 5, 0, 1, 1); //2D38 +5 -> 2D50 +50 (5 de bo )
}

int set_in_use(int i) {
	int equipado;
	if (i) {
		if (real_filename(environment()) != "/d/arad-gorthor/npcs/guardian_conclave"){//Tus manos se queman
			environment()->spell_damage(-10-random(10), "fuego", TO, 0);
			tell_object(environment(), "Te resulta imposible soportar el abrasador calor de tu "+query_short()+" y te quemas al intentar empuñarla.\n");
			return 0;
		}
	}
	equipado = ::set_in_use(i);
	if (i && equipado) {
		set_heart_beat(12);
		add_timed_property("quemo_no_hace_mucho",1,5);
	} else if (!i && !equipado) {
		set_heart_beat(0);
	}
	return equipado;
}

void heart_beat() {
	if (!query_timed_property("quemo_no_hace_mucho")) {
		if (random(3)) {
			tell_object(environment(), "El calor que emana de tu "+query_short()+" es una tortura y aprietas fuertemente la empuñadura para soportar el dolor.\n");
			environment()->ajustar_pe(-random(5)-5);
			add_timed_property("quemo_no_hace_mucho",1,10+random(10));
		}
	}
}
void query_message(int pupa,string tipo,string localizacion,object atacante,object defensor) {
	string defname= defensor->query_short(),atname= atacante->query_short(),msj_room, msj_def, msj_at;

	tell_object(atacante, "\n%^CYAN%^BOLD%^Pupa: "+pupa+"%^RESET%^\n");
	//El daño va de 52 a 150 (creo que se suma el BO y la fuerza 
	switch (pupa) {
	case 0..100:
		msj_def = "%^RED%^*%^RESET%^ "+atname+" te golpea con su "+query_short()+" "+localizacion+" quemándote ligeramente.\n";
        msj_at = "%^GREEN%^#%^RESET%^ Golpeas a "+defname+" con tu "+query_short()+" "+localizacion+" quemándole ligeramente.\n";
        msj_room = atname+" golpea a "+defname+" con su "+query_short()+" quemándole ligeramente.\n";
		break;
	case 101..140:
		msj_def = "%^RED%^*%^RESET%^ "+atname+" te causa quemaduras graves "+localizacion+" al acertarte con su "+query_short()+".\n";
        msj_at = "%^GREEN%^#%^RESET%^ Causas quemaduras graves "+localizacion+" a "+defname+" al acertarle con tu "+query_short()+".\n";
        msj_room = atname+" acierta a "+defname+" con su "+query_short()+" causándole graves quemaduras.\n";
		break;
	case 141..149:
		msj_def = "%^RED%^*%^RESET%^ "+atname+" te lanza un tajo atroz que supera tus defensas dejándote severas marcas de carne carbonizada "+localizacion+".\n";
		msj_at = "%^GREEN%^#%^RESET%^ Lanzas un tajo atroz hacia "+defname+" que supera sus defensas dejándole severas marcas de carne carbonizada "+localizacion+".\n";
		msj_room = atname+" lanza un tajo atroz hacia "+defname+" que supera sus defensas dejándole severas marcas de carne carbonizada "+localizacion+".\n";
		break;
	default:
        msj_def = "%^RED%^*%^RESET%^ "+atname+" te corta con brutalidad "+localizacion+" con un hábil giro de su "+query_short()+" inundando tu cerebro con infinito dolor.\n";
        msj_at = "%^GREEN%^#%^RESET%^ Cortas con brutalidad "+localizacion+" a "+defname+" con un hábil giro de tu "+query_short()+" que le quema con tortuoso dolor.\n";
        msj_room = atname+" corta con brutalidad a "+defname+" con un hábil giro de su "+query_short()+" que le quema con un dolor indescriptible.\n";
	}
    tell_object(atacante,msj_at);
    tell_object(defensor,msj_def);
    tell_room(environment(defensor),msj_room,({defensor,atacante}));
}
