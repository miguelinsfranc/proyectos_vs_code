// Dunkelheit 27-10-2009

inherit "/obj/arma";

string descripcion_encantamiento()
{
	return "Entre los brillantes fogonazos mágicos que emite la Diezmadora "
	"se asoman imágenes de un legendario soldado, blindado de los pies a la "
	"cabeza con una hermosa armadura de diseño jamás visto en Eirea.";
}

void setup()
{
	fijar_arma_base("cimitarra larga");
	set_name("diezmadora");
	set_short("%^RED%^BOLD%^Diezmadora%^RESET%^");
	set_main_plural("%^RED%^BOLD%^Diezmadoras%^RESET%^");
	fijar_genero(2);
	add_plural("diezmadoras");
	set_long("Es la poco habitual cimitarra portada por Morkûm Thule, el que un día "
	"fue Mariscal del Ejército Negro de Golthur Orod. Su alargada hoja todavía podría "
	"sacar una cabeza al más alto de los enanos; y eso sin contar su pronunciada "
	"curvatura, acentuada en el extremo y dividida en tres arcos diferentes, en "
	"cuyos puntos de encuentro se alzan sendos punzones, cuya función en el combate "
	"es poder apresar a las víctimas por el cuello contra una superficie capaz de "
	"se atravesada por ellos. Este magnífico metal aleado tiene un ligero resplando "
	"carmesí, y la base de la empuñadura tiene tallado un rostro humano con una larga "
	"barba. Esto, junto con su cuidada forja, te hace dudar que su manuufactura sea "
	"orca.\n");
	
	fijar_encantamiento(31);
}
