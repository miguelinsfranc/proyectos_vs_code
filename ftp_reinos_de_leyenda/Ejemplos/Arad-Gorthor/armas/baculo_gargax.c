//Rezzah 17/01/2011
inherit "/obj/arma";

void setup()
{
	fijar_arma_base("baston");
	set_name("baculo");
	set_short("%^BLUE%^Báculo del %^BOLD%^RED%^Profeta%^RESET%^");
	set_main_plural("%^BLUE%^Báculos del %^BOLD%^RED%^Profeta%^RESET%^");
	add_plural("baculos");
	fijar_material(0);
	set_long("El báculo del profeta es un bastón de más de metro y medio de altura tallado sobra "
		"una extraña aleación metálica de tacto cálido y poroso, como si fuera madera, de hecho su "
		"color y los intrincados trazados de su hechura recuerdan los retorcidos nudos de un árbol. "
		"Hacia lo más álto del báculo y de forma progresiva el extraño material que lo compone va "
		"cambiando volviéndose poco a poco más metálico y brillante, como si fuera oro negro por lo "
		"fino y delicado de sus superficie y lo oscuro de su color. En la punta superior el metal "
		"es tan reluciente que más recuerda una gema negra y esférica, un hermoso y enorme azabache, que el metal "
		"del que obviamente se compone, pues el repiqueteo metálico contra el suelo a cada peso es "
		"inconfundible. El orbe superior tiene grabadas unas diminutas letras en tono dorado que "
		"lo embellecen como si una valiosa joya fuera. Su extraordinario peso lo convierten en un "
		"arma difícil de manejar.\n");
	fijar_genero(1);
	fijar_encantamiento(9);
	add_static_property("divino",60);
	add_static_property("bien",-60);
	add_static_property("mal", 10);
	ajustar_BO(10);
	ajustar_BP(15);
	ajustar_BE(-20);
	add_attack("golpe_divino", "mal", 3, 20, 10,0,0,0,1);  
	fijar_mensaje_leer("En la noche más oscura Gurthang extenderá su sombra sobre Eirea.\n","negra");
}

int set_in_use(int i) {
	object pj;
	pj = environment();
	if (i) {
		if (pj->dame_clase() != "chaman" || pj->dame_religion() != "gurthang") {
			tell_object(pj, "El "+query_short()+" no está hecho para tí.\n");
			return 0;
		}
		if (pj->dame_nombre_alineamiento() != "diabólico") {
			tell_object(pj, "La perturbadora maldad del "+query_short()+" te supera incluso a tí.\nSolo un ser de espíritu endemoniado sería capaz de empuñar un báculo con tanto odio en su interior.\n");
			return 0;
		}
	}
	return ::set_in_use(i);
}

string descripcion_encantamiento() {
	return "Un poder divino más allá de tu comprensión late con fuerza dentro del "+query_short()+".\n";
}
