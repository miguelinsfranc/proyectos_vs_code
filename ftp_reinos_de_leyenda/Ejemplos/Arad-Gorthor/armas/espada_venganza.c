// Dherion 05/10/2017
inherit "/obj/arma.c";
void setup()
{
    fijar_arma_base("espada larga");
    set_name("espada de la venganza");
    set_short("Espada de la Venganza");
    set_main_plural("Espadas de la Venganza");
    generar_alias_y_plurales();
    set_long("Es una espada de doble filo, cuya  larga hoja de unos noventa centímetros de longitud "
      "se va estrechando desde la empuñadura hasta formar la aguzada punta. La empuñadura "
      "envuelta en cuero y tejido de fina cuerda posee un pomo metálico de forma "
      "redondeada y una guarda de hierro adornada "
      "con unos sencillos grabados geométricos que parecen dar un extraño poder al arma.\n");
    fijar_manos_necesarias(1);
    fijar_BO(20);
    add_static_property("messon","Sientes ansias de sangre al empuñar la
espada.\n");
    add_static_property("messoff","Tus ansias de sangre disminuyen cuando
guardas la espada.\n");
}
