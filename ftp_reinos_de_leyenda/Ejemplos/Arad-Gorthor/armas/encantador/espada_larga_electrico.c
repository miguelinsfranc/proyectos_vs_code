// Dunkelheit 02-06-2007 -- Rehaciendo descripciones
inherit "/obj/arma.c";

void setup()
{
  fijar_arma_base("espada larga");
  add_attack("ataque_encantado", "electrico", 3,20,15,0,0,0,1);
set_name("espada larga");
  set_short("Espada Larga");
  add_alias(({"espada", "larga"}));
  set_main_plural("Espadas Largas");
  add_plural(({"espadas largas", "espadas", "largas"}));
  set_long("Se trata de una de las armas de guerra más comunes en Eirea. Una espada de hoja larga y doble "
  "filo, con un canal que se extiende hasta casi el extremo de la hoja y una cruz más amplia de lo habitual. "
  "El metal de su filo es de una brillante tonalidad plateada y éste alcanza más o menos los ciento diez "
  "centímetros de longitud. Su empuñadura, tradicionalmente de una mano, es del mismo metal que la hoja pero "
  "ha sido cuidadosamente labrado con patrones elípticos. No posee guarda alguna, pero presenta dos poderosos "
  "gavilanes de hierro en cruz, totalmente rectos y perpendiculares a la hoja. La punta de este tipo de espadas "
  "ha ido evolucionando con el tiempo: en la presente Edad, son romboides y punzantes, ideales para propinar "
  "dolorosas estocadas al contrincante.\n");
  fijar_genero(2);
}
