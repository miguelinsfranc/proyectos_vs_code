// Durmok 16/01/2005 Poniendo imagenes en MXP
 
inherit "/obj/proyectiles/arrojadiza";

void setup()
{
  fijar_arma_base("puñal");
  add_attack("ataque_encantado", "fuego", 3,20,15,0,0,0,1);
set_name("puñal");
  set_short("%^RED%^Puñal%^RESET%^");
  set_long("Es una arma parecida a una daga en tamaño, aunque su "
	"punta esta bastante mas afilada, pensada para ser clavada en el torso. Su "
	"pequenyo tamanyo la hace especialmente util para ser arrojada.\n");
  set_main_plural("%^RED%^Puñal%^RESET%^es");
  add_plural("punyales"); // parece que sino no funciona
  add_alias("punyal");
  add_alias("puñal");
  add_plural("puñales");
  fijar_genero(1);
}

