inherit "/obj/arma";

void setup()
{
   fijar_arma_base("sable");
   add_attack("ataque_encantado", "electrico", 3,20,15,0,0,0,1);
set_name("sable");
   set_short("%^CYAN%^Sable%^RESET%^");
   set_main_plural("%^CYAN%^Sable%^RESET%^s");
   set_long("Es un sable con un tercio filo curvado de color gris y resplandeciente al la luz del sol. "
      "La empuñadura esta provista de una pequeña proteccion para la mano. El mango mas recio que la hoja "
      "tiene un acabado curvado en contradireccion a la hoja.\n");
   add_alias("sable");
   add_plural("sables");
   fijar_genero(1);
}
