// Dunkelheit 18-12-2009

inherit "/obj/arma";

void setup()
{
	fijar_arma_base("luz del alba");
	set_name("estrella");
	set_short("%^BLUE%^BOLD%^Estrella del Norte%^RESET%^");
	set_main_plural("%^BLUE%^BOLD%^Estrellas del Norte%^RESET%^");
        generar_alias_y_plurales();
	set_long("La Estrella del Norte es la luz del alba personal del cancerbero de "
	"las Puertas de la Guerra, antesala de la polvorienta y calurosa Arad Gorthor. "
	"Su manufactura es un tanto basta y primitiva; las púas de su cabeza están "
	"repartidas de cualquier manera y tienen una base piramidal tan estrecha que "
	"parecen poder quebrarse con facilidad, lo cual no está mal si la mitad de la "
	"púa se ha quedado albergada en la cabeza de tu víctima...\n");
	
	fijar_encantamiento(5);
}
