La Horda Negra es el movimiento religioso y militar que persigue la absoluta dominación de Eirea y sus continentes, reinos y habitantes bajo el yugo de Lord Gurthang, el Dios de la Guerra. Este objetivo ulterior es conocido como la Voluntad Final, y el camino que conduce a su consecución es la Guerra.

Historia
********

Desde mediados del año 15 hasta finales del año 17 de la Era 4ª, el Ejército Negro de Golthur Orod sufrió una sucesión de humillantes derrotas que menguó drásticamente la cantidad de efectivos y, lo que fue todavía más devastador, decapitó en varias ocasiones a su estado mayor. La incapacidad de los múltiples caudillos para evitar sublevaciones e impartir disciplina acortó todavía más sus breves mandatos.

La que un día fue la mayor potencia septentrional de Eirea se sumió en el pesimismo y la más extrema de las anarquías. Entre los círculos internos más sectarios de la Fortaleza Negra volvía a planear la sombra del golpe de estado, auspiciada especialmente por dos figuras: el comandante Glorbaugh y el mariscal de campo Morkûm Thule.

Probablemente fue este último quien instigó con más vehemencia el golpe de estado como única salida, incitado por una fe jamás vista en un militar y el insoportable rosario de derrotas padecidas por sus tropas. Para alcanzar el ansiado cambio, y con insólita meticulosidad, concibieron una sociedad secreta cuyos tentáculos pronto se extendieron entre los otros dos reinos que veneraban al Dios de la Guerra: Mor Groddûr y Ancarak.

Esta sociedad fue conocida como la Orden Thule, y el halo de intencionado misticismo que la rodeaba atrajo a las mentes más sagaces e inquientas de las razas goblinoides. Como muchas sociedades secretas incipientes, su actividad primordial fue la conspiración. De sus primeras reuniones, que tuvieron lugar entre las inhabitadas mazmorras de la Fortaleza Negra, salió como conclusión que la anarquía, que un día fue inferida como instrumento de limpieza racial, había sido llevada demasiado lejos: no sólo estaba erradicando a los miembros más débiles de sus sociedades sino que además estaba llevándose consigo a componentes válidos y necesarios en las filas de sus ejércitos tanto directa como indirectamente.

Pero este viento de cambio pasaba desapercibido para los miembros más brutos y descerebrados de las filas verdes -¡la inmensa mayoría!-, cuya moral no estaba tan eclipsada y siempre encontraban presas, tanto extranjeras como nativas, para pasar el rato entre incursión e incursión.

Los primeros años de la Orden Thule fueron un fracaso estrepitoso. Su veloz desarrollo inicial se frenó tan pronto como la voz de su existencia se diseminó entre las mentes más despiertas del ejército. Una vez alcanzado este apogeo, fue la propia y elevada mortalidad natural de los goblinoides la que se encargó de menguar las filas de la Orden hasta casi la mitad. Para colmo, la borrachera de presunción, egocentrismo y ambición de algunos co-líderes de la Orden causó varios cismas que derivaron en nuevas sociedades secretas obsesionadas con tomar las riendas del Ejército Negro. Por fortuna, ninguna de ellas sobrevivió.

El curso de la historia cambió tras la pérfida alianza entre el Imperio de Dendra y el reino lagarto de Grimoszk, ambos grandes enemigos de la causa de Lord Gurthang. Juntos, desolaron los reinos del dios de la Guerra y minaron la moral de sus tropas hasta puntos inimaginables tras una cadena de ominosas derrotas.

La Voluntad Final
*****************

La Voluntad Final y la Guerra estarán siempre presentes en la mente de los miembros de la Horda. Todo cuanto hagan en sus vidas estará destinado a la consecución de la Voluntad Final; cualquier acción que busque cualquier otro propósito será considerada como alta traición y su castigo será la muerte.

La Voluntad Final está por encima de cualquier interés personal o colectivo. Cualquier intento, intencionado o involuntario, de boicotear, minar o desviarse del sendero de la Voluntad Final será considerado como alta traición y su castigo será la muerte.

Jerarquía
*********

La jerarquía de la Horda Negra es la siguiente:
			
La máxima autoridad de la Horda Negra es el Caudillo. Él es el máximo representante de Lord Gurthang en el Primer Plano Material; y sólo a Lord Gurthang -¡y a nadie más!- debe rendirle cuentas. Es su obligación dirigir y dictar con puño de acero y no hesitar en su mandato, cuyo objetivo será la Voluntad Final por la gloria de Lord Gurthang. El Caudillo será el miembro de la Horda Negra que tenga más puntos. Para descancar a Caudillo será necesario superar su puntuación en 200 puntos.

Directamente a continuación, e igualados en poder, se encuentran el Arconte y el Hierofante. 

El Arconte es la mano derecha militar del Caudillo y bajo su mandato se encuentra la totalidad de las tropas de la Horda Negra. Tendrá la última palabra en todas las decisiones bélicas, siempre que goce con el beneplácito del Caudillo. El Arconte será aquel miembro no-chamán de la Horda que, exceptuando al Caudillo, tenga mayor puntuación. Para desbancar al Arconte, será necesario superar su puntuación en 150 puntos.

El Hierofante es la mano derecha espiritual del Caudillo, y la máxima entidad religiosa por detrás del Caudillo. Representa el lado más extremista y vil del ya por sí infame evangelio de Lord Gurthang. El Hierofante es una figura trata constantemente con el plano Abismal y las entidades afines a Lord Gurthang que en él habitan. Gozará de este puesto el chamán de la Horda que, exceptuando al Caudillo, tenga mayor puntuación. Para desbancar al Hierofante, será necesario superar su puntuación en 150 puntos.

El grupo compuesto por el Caudillo, el Arconte y el Hierofante se denomina Estado Mayor. Es el núcleo y pilar primordial de la Horda Negra.

Por debajo del Arconte y el Hierofante se encuentra el Mariscal de Campo. El Mariscal de Campo es un experto estratega y consejero personal del Caudillo, el Arconte y el Hierofante. Su puesto de batalla, sin embargo, se encuentra en las más altas estancias de Arad Gorthor, entre planos y mapas de maniobras bélicas. Es el líder de facto del Estandarte Segundo, pues es un eficaz vínculo entre éste y el Estado Mayor.

El resto de guerreros se distribuyen en tres estamentos jerarquizados. Primero los Oficiales: apróx. un 10% de la Horda; por debajo de ellos los Suboficiales: apróx. un 20% de la Horda; y por debajo de ellos se encuentra el resto de huestes de la Horda: el 70% restante. La política de ascensos y descensos será tal que permita garantizar la proporcion 10-20-70.

Política y Diplomacia
*********************

La Horda Negra impone una clara separación del brazo político-diplomático. El Estandarte Segundo acoge a todos los guerreros de la horda cuyo campo de batalla está en las intendencias de castillos y fortalezas. Estos guerreros no podrán ocupar ningún cargo militar mientras desempeñen roles diplomáticos, aunque sí podrán seguir obteniendo puntos y méritos por combate.

Existen tantas facciones político-diplomáticas como reinos bajo el dominio de Lord Gurthang. Cada Reino tendrá un único mandatario, llamado Tirano, que tomará todas las decisiones pertinentes para proteger y contener a sus ciudadanos, adoctrinarlos en la Voluntad Final y movilizarlos cuando los dominios de Lord Gurthang se encuentren en peligro o la Horda Negra así lo estime oportuno. Su jurisdicción quedará limitada a su reino.

Los Tiranos estarán al mismo nivel jerárquico que el General y el Pontífice Brujo, aunque cada cual pertenece a un estandarte diferente. Deberán mantener una relación cordial ente sí. En caso de problema irresoluble entre ellos, será el Caudillo el encargado de resolverlo.

Todos los reinos de Lord Gurthang deben seguir una política común en sus diplomacias. El Caudillo podrá intervenir en caso de que un reino que diverja de los otros y tome decisiones sin consensuarlas. No obstante, se permitirán las llamadas "diplomacias técnicas", mediante las cuales un reino podrá mantener una diplomacia diferente a la de los otros, única y exclusivamente cuando ayude a conquista de la Voluntad Final. Las diplomacias técnicas deberán ser consensuadas.

Disciplina
**********

La Horda Negra profesa una disciplina militar extrema. Cada miembro de la Horda será referido siempre con el título que le corresponda, de acuerdo a las siguientes directrices:
* El Caudillo será siempre llamado por su título. Bajo ningún concepto se le llamará por su nombre real.
* El General y el Pontífice Brujo serán llamados por sus respectivos títulos. Bajo ningún concepto se les llamará por su nombre real.
* Los Tiranos serán llamados por su título. Bajo ningún concepto se les llamará por su nombre real.
* Dentro de los tres estamentos inferiores, cada miembro se referirá a un cargo superior por su título, seguido opcionalmente del nombre.
* Cualquier integrante de la Horda puede utilizarse el término "Snaga" para referirse a alguien de rango inferior.

La Reserva
**********

Los miembros de la Horda que no orbiten a Eirea durante más de dos semanas serán desplazados a la Reserva. Perderan cualquier cargo que ostentasen, pero no su puntuación. No podrán volver a ser elegibles para ningún cargo hasta que no regresen de forma activa durante un mínimo de tiempo.

Los miembros de la Horda que tengan prohibido orbitar a Eirea (ban o suspensión) serán automáticamente desplazados a la Reserva. Perderán cualquier cargo que ostentasen y, si Lord Gurthang lo estima, también su puntuación.

Bastión de Arad Gorthor
***********************

Snagas - Suboficiales - Oficiales
Chamán - Maestro Chamán - Brujo 
Soldado - Soldado de Élite - Centurión
Gragbadûr - Gragbadûr de Élite - Portaestandarte
Bárbaro - Bárbaro de Élite - Brutalista
Cazador - Cazador de Élite - Asesino
Ladrón - Ladrón de Élite - Umbrío
Hechicero - Maestro Hechicero - Arcano
Adivino - Maestro Adivino - Augur
Necromante - Maestro Necromante - Arúspice
Tirador - Tirador de Élite - Vigilante
Bardo - Maestro Bardo - Rapsoda

Vertical Militar
Vertical Civil
