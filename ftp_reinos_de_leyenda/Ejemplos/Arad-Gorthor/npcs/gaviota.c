//Npc gaviota.c creado por Sierephad 08-octubre-2007
//Eckol Ago20: Pongo el path del dominio (antes path de altaresh). Arreglo y simplifico de irse (ahora simplemente run away)
#include "../path.h";
inherit "obj/monster";
int rnd;
int i;
string *rooms = ({});

void attack_by(object pj){
    if(!random(3))
        run_away(0);
    ::attack_by(pj);        
}

void setup() {
  set_name("gaviota");
  add_alias(({"gaviota","ave"}));
  set_short("Gaviota");
  set_long("Este ave se encuentra generalmente en las regiones cercanas a la costa "
"pues su mayor fuente de alimento es el pescado. Su cuerpo esta recubierto por "
"alargadas y brillantes plumas blancas, aunque solo en el periodo adulto, pues "
"cuando aun son poyuelos, su plumaje es grisáceo como las nubes de tormenta. "
"Sus anaranjadas patas son membranosas, permitiéndoles nadar sobre la superficie "
"del agua mientras busca nuevas presas. Posee un alargado y afilado pico que suele "
"utilizar para abrir y partir su alimento con rápidos picotazos.\n");
  set_main_plural("Gaviotas");
  add_plural(({"gaviotas","aves"}));
  fijar_altura(30+random(15));
  fijar_peso_corporal(100*dame_altura()+random(13));
  fijar_nivel(7+random(3));
  set_wimpy(0);
  set_random_stats(9,13);
  set_move_after(10,5);   
  fijar_genero(2);
  fijar_raza("ave");
  anyadir_estilo_combate_desarmado("razas/ave");
  poner_estilo_combate_desarmado("razas/ave");
  fijar_tamanyo_racial(1);
  set_align(0);
  load_chat(20,
           ({
             ":mira de un lado a otro.\n",
             ":se limpia su alargado y afilado pico contra su blanco plumaje.\n",
 ":picotea algo en el suelo.\n",
 ":duerme tranquila sobre acurrucandose sobre si misma."
           }));
}
