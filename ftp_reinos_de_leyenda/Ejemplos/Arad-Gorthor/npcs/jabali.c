//Golthiryus VI-08. Reahago los jabalies de arilven, que daba vergüenza eso de "cuerpo de Jtrufas"
inherit "/baseobs/monstruos/animales/jabali.c";

#include "../path.h"
#include <alimentacion.h>

int escarbando; //nos dice si esta buscando trufas o no
int mosqueado;

void fijar_escarbando(int);

void setup()
{
	::setup();
  set_name("jabali");
  add_alias(({"bicho","animal","mamifero","jabali"}));
  set_short("Jabalí");
  set_main_plural("Jabalíes");
  add_plural(({"bichos","animales","mamiferos","jabalies"}));
  set_long("Es un grasiento jabalí de pelaje no excesivamente largo y de color"
           " gris, bastante ocre. Posee una mirada salvaje en sus negros"
           " ojos, y por su constitución apuestas a que podría embestirte "
           "y dejarte anclado en cualquier árbol con facilidad.\n");

  fijar_raza("rumiante");

  fijar_genero(random(2)+1);
  
  load_chat(10,
  ({
  	":mueve el morro olfateando el ambiente.\n",
    ":escarba con las patas traseras en un trozo de tierra.\n",
 		":da un par de vueltas moviendo rápidamente sus cortas patas.",
 		":gruñe: Oiink Oinnq.",
 		":remueve la tierra mientras olisquea.",
   }));
   
   fijar_escarbando(1);
}

void fijar_escarbando(int i) {escarbando = i;}
int dame_escarbando() {return escarbando;}

void do_move_after()
{
	//no quiero que se mueva mientras escarba. Podria poner la frecuencia_movimiento a 0, pero me parece mu cutre
	if(escarbando) 
		return 0;
	else 
		return ::do_move_after();
}

//enmascaramos el event_enter para que busque trufas
void event_enter(object quien_entra,string mensaje,object procedencia,object *seguidores)
{
	object raiz;
	if (!escarbando || !living(quien_entra)) 
		return ::event_enter(quien_entra,mensaje,procedencia,seguidores); //si no escarba no hace nada especial
 	if (!raiz = present("raices_escarbables",ENV(TO)))
 		return ::event_enter(quien_entra,mensaje,procedencia,seguidores);
 	if(load_object(AHANDLERS+"handler_general.c"))
 		load_object(AHANDLERS+"handler_general.c")->crear_tierra(ENV(TO));
 	tell_accion(ENV(TO),query_short()+" se afana en remover la tierra junto a las raices de un arbol.\n");
 	//call_out(real_filename(TO),random(10)+20,"busca_trufa",TO,raiz);
 	call_out("busca_trufa",2,TO,raiz);
 	return ::event_enter(quien_entra,mensaje,procedencia,seguidores);
}

void busca_trufa(object jabali,object raiz)
{
	if(!jabali) return 0;
	if(!raiz || ENV(TO)!= ENV(raiz)) //se ha movido mientras escarbaba, no hacemos nada
		return 0;
	if (jabali->dame_peleando()) return 0;
	if (!raiz->quedan_trufas()) {
		tell_accion(ENV(TO),jabali->query_short()+" parece no encontrar mas trufas entre las raices y "
			"deja de excavar.\n");
		return 0;
	}
	if (random(2)) {
		tell_accion(ENV(TO),jabali->query_short()+" encuentra una trufa entre las raíces de un árbol y se "
			"la come.\n","Oyes movimiento a tu alrededor.\n"); 
		jabali->ajustar_volumen(D_COMIDA,50+random(50));
		jabali->fijar_xp_max(jabali->dame_xp_max()+(random(250)+250));
		raiz->menos_trufa();
		call_out("busca_trufa",random(10)+20,jabali,raiz);
		return 0;
	}
	tell_accion(ENV(TO),jabali->query_short()+" parece no encontrar nada entre las raíces y cambia de "
		"posición excavando en otro lugar.\n","Oyes movimiento a tu alrededor.\n"); 
	call_out("busca_trufa",random(10)+20,jabali,raiz);
	return 0;
}

int dame_mosqueado()
{
	return mosqueado;
}

int mosquear()
{
	mosqueado = 1;
}
