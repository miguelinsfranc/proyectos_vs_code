// Dunkelheit 10-11-2009
/*
*	Zoilder - 24/03/2011: Ampliación de la misión hobgoblins.
*/
//Grimaek 14/06/2023 añado logro quema de brujas

#include "../path.h";
#include <mxp.h>
#include <alimentacion.h>


inherit "/obj/conversar_npc.c";

void setup_conversacion();
void entregar_alcohol(mixed alcohol, object jugador, string respuesta);

void setup()
{
	set_name("bruja");
	set_short("Bruja del Pantano");
	add_alias(({"bruja","goblin","anciana"}));
	set_long("Una anciana bruja medio calva y llena de mugre, ataviada con una ropa destartalada y empuñando un bastón. "
		"Por la forma de mirar a su alrededor, dirías que está como una puta regadera.\n");
	set_main_plural("Brujas del Pantano");
	add_plural(({"brujas","goblins","ancianas"}));
	fijar_raza("goblin");
	fijar_clase("adivino_negro");
	fijar_altura(120);
	fijar_peso_corporal(38500);	
	fijar_fue(5);
	fijar_con(16);
	fijar_des(8);
	fijar_int(17);
	fijar_sab(21);
	fijar_car(4);
	fijar_genero(2);
	fijar_nivel(48);
	fijar_alineamiento(0);
	fijar_pvs_max(8000);
	fijar_perfil_lenguajes("seldar");
	fijar_lenguaje_actual("negra");
	add_clone(BARMAS + "baston", 1);
	add_clone(BARMADURAS + "capa",1);
	add_clone(BARMADURAS + "camiseta",1);
	add_clone(BARMADURAS + "pantalones",1);
	add_clone(BARMADURAS + "zapatillas",1);
	init_equip();
	setup_conversacion();
}

void setup_conversacion()
{
	nuevo_dialogo("molestar", ({
		":se queda parada con la mirada perdida durante un incómodo rato.",
		":pega una cabezada y se despierta.",
		"'¡¿Todavía estás aquí, cabestro?!",
		"'¡Malditos hobgoblins, macarras!",
		"'¿Quién es tu madre? ¡Seguro que la conozco!",
		"'¡Te dejará el trasero en carne viva de la azotaina que te va a dar!",
		"'Bahhhgggg...",
		":pega un sorbo al cuenco que hay junto a la marmita.",
		"'Sí... ¿Cómo te llamabas?",
		"'Ahhh...",
		":pone de repente una voz suave y melosa.",
		":vuelve a encabritarse. Te temes lo peor.",
		"'¡¡¡Pues ya va siendo hora de que vuelvas!!!",
		"'¡¡¡Pipas de río!!!",
		"'¡¡¡Animal \"perdío\"!!!",
		"'¡¡¡Cara de buey!!! ¡¡¡Cara de buey!!!",
	}));
	nuevo_dialogo("hobgoblins", ({
		":se lleva las manos al rostro y empieza a balancearse de un lado a otro. Deduces que no está muy bien de la cabeza.",
		"'Estos malditos piojos rojos, desgraciados... ¡Bandidos! ¡¡¡Truhanes!!!",
		"'Se meten... ¡En mi jardín!",
		"'¡Destrozan mis macetas! ¡Apedrean mis ventanas! ¡Roban mi comida! ¡Molestan a mis ratas!",
		"'Mis pequeñas ratitas...",
		":empieza a acariciar una rata imaginaria en su regazo.",
		"'¿Te han incordiado, cuchufletina mía?",
		":se detiene por completo. Ahora parece que le ha dado por hacerse la estatua.",
		"'No hay remedio...",
		"ya_completada_mision"
	}), "molestar");
	nuevo_dialogo("remedio", ({
		":se incorpora y te observa de arriba a abajo con detenimiento.",
		"decidir",
	}),"hobgoblins");
	nuevo_dialogo("macarras", ({
		"'Si no fuera una pobre y anciana inválida, les daría una lección que jamás olvidarían.",
		"'Pero los años han hecho mella en mí.",
		"'El señor de mi orden me aconsejó venir a estas tierras.",
		"'Respirar el aire fresco y puro de la ciénaga.",
		"'Descansar, descansar, descansar.",
		"'¡Pero no hay descanso posible con esos macarras!",
		":se acerca a ti como para contarte un secreto.",
		"'¿Quieres un poco de diversión? ¿Te gustaría hacer feliz a una pobre vieja?",
	}), "remedio");
	nuevo_dialogo("feliz", ({
		":se frota las manos.",
		"'Tráeme la cabeza del líder hobgoblin...",
		"'...y te revelaré uno de los mayores secretos de estas tierras.",
		"'Supongo que sabrás entrar a su guarida. ¿Supongo bien? ¿Eh?",
	}), "macarras");
	nuevo_dialogo("guarida", ({
		":se ríe a carcajadas, enseñando sus dientes podridos.",
		"'¡Lo sabía, lo sabía, lo sabía!",
		"'¡Eunuco mental!",
		"'¿Pensar? ¡¿Pensar?!",
		"'¡¡¡Pensar!!",
		"revelar_guarida",
		"'¡No te digo más!",
	}), "feliz");
	nuevo_dialogo("alcohol",({
		"'Así que me vas a dar alcohol, ¿eh?",
		"dar_alcohol"
	}),"guarida");
	nuevo_dialogo("secreto",({
		":se te acerca y te susurra al oído.",
		":eructa.",
		":se descojona de ti.",
		"'Jejeje, basta de bromas, seriedad, déjame contarte el secreto.",
		"'Hace mucho, mucho tiempo, un terrible cataclismo sacudió estas tierras.",
		":acaricia a su rata imaginaria.",
		"'El fuego y la lava arrasaron con toda forma de vida, convirtiendo el fructífero bosque de la Montaña del Jabalí, en un desolado desierto de roca volcánica.",
		"'El Destino, caprichoso y juguetón, quiso que en ese preciso momento, un Cónclave se encontrara invocando a un poderoso demonio y quedara atrapado por los siglos de los siglos.",
		"'¡Qué miedo! ¿Verdad, cuchufletina mía?",
		"'Un día, ese demonio surgió y destruyó todo el mundo conocido y por conocer, y tú y yo somos espíritus que vagamos sin rumbo.",
		":se ríe de forma exagerada.",
		"'¡Espíritu, espíritu! ¡Tu madre cagó un espíritu!",
		":tose.",
		"'Con odio grita esperando a través de las rocas, quien venga a liberarle, para que pueda saciar sus ansias de venganza contra todos aquellos que le encerraron y sus descendientes.",
		"'¡Venganza! ¡Dolor! ¡Sangre! ¡\"Pesaos\", siempre con las mismas tonterías! ¡Lo que de verdad deben temer es el poder de las ratas, que dominarán el mundo!",
		":traza círculos en el aire con su dedo índice.",
		"'Bueno, a lo que iba, que no dejas de cambiarme de tema.",
		"'Hace muchos años, un sabio chamán decidió que era mejor bloquear el acceso que lleva hasta el Cónclave.",
		"'Y sólo yo sé cómo activar la palanca que lleva hasta los peligros bajo la tierra.",
		":te mira pensativa.",
		"'En agradecimiento por haberme traído la cabeza de Rassoodock, te diré cómo, pero sólo tú lo sabrás; y date prisa, pues añado a mis palabras un encantamiento para que desaparezcan de tu cabeza.",
		"funcion_finalizacion"
	}));
	nuevo_dialogo("cabeza",({"entregar_cabeza"}));
	prohibir("macarras", "hito hobgoblins borrar-altar-piedra"); //No se hablará de los hobgoblins hasta haber descifrado el mapa
	deshabilitar("remedio","hito hobgoblins finalizada_mision"); //Una vez completa la misión ya no hablará sobre todo el tema de matar al líder, etc.
	deshabilitar("alcohol","hito hobgoblins conversacion-bruja"); //Si ya se entregó el alcohol, no pide más
	deshabilitar("cabeza","no_puede_entregar_cabeza"); //Si no se le pidió la cabeza aún, o ya la entregó la cabeza, no podrá hablar de esto
	prohibir("secreto","hito hobgoblins finalizada_mision"); //Sólo hablará del secreto una vez finalizada la misión
	habilitar_conversacion();
}

int frases_personalizables(string tipo, object npc, object pj)
{
	switch (tipo)
	{
		case "saludo":
			pj->do_say("¿Cómo sabes quién soy?", 0);
			break;
		case "bienvenida-pj":
			npc->do_emote("se encarama al trono que preside la cochambrosa mesa, frente a la burbujeante marmita.");
			npc->do_say("¡Me lo ha dicho la meretriz de tu difunta! ¡¿Qué haces aquí, zoquete?! ¡¿Tú también vienes a "+SEND("molestar","conversar molestar",0)+"?!", 0);
			break;
		case "bienvenida-npc":
			break;
		case "despedida-pj":
			pj->do_say("No me siento muy bien, creo que voy a vomitar, será mejor que me vaya.", 0);
			break;
		case "despedida-npc":
			npc->do_say("Si vas a vomitar hazlo aquí dentro, ¡no he dado de comer a mis ratas desde hace días!", 0);
			break;
		default:
			return 0;
	}

	return 1;
}

void decidir(object quien)
{
	if(environment()!=ENV(quien)) return;
	if (quien->dame_hito_mision("hobgoblins", "borrar-altar-piedra"))
		do_say("Ahhh... Veo que ya has investigado por tu cuenta a esos "+SEND("macarras","conversar macarras",0)+", a los "+SEND("hobgoblins","conversar hobgoblins",0)+", ¡pilluel"+quien->dame_vocal()+"!", 0);
	else {
		do_say("¡Ni siquiera sabes lo que es un hobgoblin!", 0);
		do_say("¡Desaparece de mi vista, zoquete!", 0);
		do_say("¡Y cierra la puerta al salir!", 0);
	}
}

void revelar_guarida(object quien)
{
	if(environment()!=ENV(quien)) return;
	if(!quien->dame_hito_mision("hobgoblins","conversacion-bruja")) //Si no se le ha entregado alcohol
	{
		do_say("¿Te crees que te diría algo así sin que hicieras algo por mí? "
			"¡Lo llevas claro pilluel"+quien->dame_vocal()+", así que te aviso de que a mí me encanta el "+SEND("alcohol","conversar alcohol",0)+"!",0);
		if (!quien->dame_hito_mision("hobgoblins", "entregar_alcohol"))
			quien->nuevo_hito_mision("hobgoblins", "entregar_alcohol", 1);
		return;
	}
	do_say("Busca un tótem entre la basura del vertedero, por la zona central del mismo, removiendo la mierda.", 0);
	do_emote("sonríe mostrando su fea dentadura.");
	do_say("¡Y no olvides traer algo de ese rico material para mis amigas!",0);
}

void dar_alcohol(object quien, string tema)
{
	object *alcohol=({}); //Objetos de alcohol del inventario
	string *lista_alcohol=({}); //Listado de nombres de los objetos con alcohol (para usar en el dialogo)
	if(environment()!=ENV(quien)) return;
	//Se recorre el inventario tomando todos los objetos que den alcohol
	foreach(object obj in all_inventory(quien))
		if(obj->dame_alcohol())
		{
			alcohol+=({obj});
			lista_alcohol+=({obj->query_short()});
		}
	if(!sizeof(alcohol)) //No llevamos nada de alcohol
	{
		do_say("No veo que lleves nada con alcohol encima, así que no me molestes, ¡¡¡o mandaré a mi caballero andante a que te pegue un \"bocao\"!!!",0);
		return;
	}
	do_say("Mmmm, veo que tienes alcohol, ¿por qué no me das un poco, precios"+quien->dame_vocal()+" mí"+quien->dame_vocal()+"?",0);
	if(sizeof(alcohol)==1) //Si sólo lleva el jugador un objeto con alcohol
	{
		quien->mostrar_dialogo("¿Entregas "+alcohol[0]->dame_articulo()+" "+alcohol[0]->query_short()+" a "+query_short()+"?",
			({"Sí","No"}),(:entregar_alcohol,alcohol[0]:));
		return;
	}
	//Si lleva más de un objeto de alcohol
	quien->mostrar_dialogo("¿Cuál de los siguientes objetos quieres entregar a "+query_short()+"?",
		lista_alcohol+({"No dar nada"}),(:entregar_alcohol,alcohol:));
}

void entregar_alcohol(mixed alcohol, object jugador, string respuesta)
{
	if(respuesta=="No" || respuesta=="No dar nada")
	{
		do_say("No ardieras en la hoguera y luego tus cenizas se las comiera un mediano vicioso.",0);
		return;
	}
	if(arrayp(alcohol)) //Si se tiene más de un objeto de alcohol
		foreach(object obj in alcohol) //Se obtiene el seleccionado por el jugador
		{
			if(obj->query_short()==respuesta) //Si la descripción del objeto coincide con la respuesta del usuario
			{
				alcohol=obj;
				break;
			}
		}
	tell_object(jugador,"Le das "+alcohol->dame_numeral()+" "+alcohol->query_short()+" a "+query_short()+".\n");
	tell_accion(environment(),jugador->query_short()+" le da "+alcohol->dame_numeral()+" "+alcohol->query_short()+" a "+query_short()+".\n",
		"Alguien entrega "+alcohol->dame_numeral()+" "+alcohol->query_short()+" a "+query_short()+".\n",({jugador}),jugador);
	alcohol->consumir_dosis();
	do_say("¡Muchas gracias jovencit"+jugador->dame_vocal()+"!",0);
	do_say("¡Ahora sí que te daré información sobre la "+SEND("guarida","conversar guarida",0)+" de esos macarras!",0);
	do_say("Ratita mía de mis amores, nos ha dado algo sabroso, ¿le ayudamos?",0);
	do_emote("hace gestos al aire.");
	do_say("Vale, maj"+jugador->dame_vocal()+". Busca un tótem entre la basura del vertedero, por la zona central del mismo, removiendo la mierda.", 0);
	do_emote("sonríe mostrando su fea dentadura.");
	do_say("¡Y no olvides traer algo de ese rico material para mis amigas!",0);
	if (!jugador->dame_hito_mision("hobgoblins", "conversacion-bruja"))
		jugador->nuevo_hito_mision("hobgoblins", "conversacion-bruja", 1);
	//ajustar_volumen(D_ALCOHOL,alcohol->dame_alcohol()); //Vamos a ponerla algo bebida, así es más gracioso hablar con ella                      GRIMAEK COMENTADO POR DAR ERROR
	tell_room(environment(),query_short()+" se bebe el alcohol en un plis-plas, acabando "+descripcion_volumen()+".\n",({TO}));
	return;
}

int no_puede_entregar_cabeza(object jugador)
{
	return !jugador->dame_hito_mision("hobgoblins","entregar_alcohol") || //No le han pedido aún la cabeza
					jugador->dame_mision_completada("hobgoblins"); //Ya ha completado la misión, entregándola
}

void respuesta_dialogo(object cabeza, object jugador, string respuesta)
{
	if(!cabeza)
	{
		do_say("No veo que tengas la cabeza de ese apestoso hobgoblin.",0);
		return;
	}
	if(respuesta=="Sí")
	{
		tell_object(jugador,"Le das la "+cabeza->query_short()+" a "+query_short()+".\n");
		tell_accion(environment(),jugador->query_short()+" le da la "+cabeza->query_short()+" a "+query_short()+".\n",
			"Alguien entrega la "+cabeza->query_short()+" a "+query_short()+".\n",({jugador}),jugador);
		do_say("¡Zaratrusta, zaratrusta! ¡Gracias, gracias, a la marmita que va!",0);
		tell_room(environment(),query_short()+" introduce la "+cabeza->query_short()+" en la marmita.\n",0);
		cabeza->dest_me();
		jugador->nuevo_hito_mision("hobgoblins","finalizada_mision");
		dialogar("secreto",jugador); //Forzamos a hablar del secreto
		return;
	}
	do_say("No reventaras junto a la cabeza de ese apestoso.",0);
}

void entregar_cabeza(object jugador, string tema)
{
	object cabeza, cabellera;
	if(environment()!=ENV(jugador)) return;
	foreach (object ob in all_inventory(jugador))
	{
		if(ob->dame_parte_cuerpo())
		{
			if(ob->dame_parte()=="cabeza" && ob->dame_duenyo()=="rassoodock") //Si tiene la cabeza
			{
				cabeza=ob;
				break;
			}
			else if(ob->dame_parte()=="cabellera" && ob->dame_duenyo()=="rassoodock") //Si tiene la cabellera
				cabellera=ob;
		}
	}
	if(!cabeza)
	{
		if(!cabellera)
			do_say("No veo que tengas la cabeza de ese apestoso hobgoblin.",0);
		else
			do_say("Lo que me interesa es la cabeza de ese apestoso hobgoblin, no su cabellera.",0);	
		return;
	}
	jugador->mostrar_dialogo("¿Me entregas la cabeza de ese apestoso?",({"Sí","No"}),(:respuesta_dialogo,cabeza:));
}

void ya_completada_mision(object jugador,string tema)
{
	if(environment()!=ENV(jugador)) return;
	if(jugador->dame_mision_completada("hobgoblins"))
	{
		do_say("¡Tú ya me ayudaste una vez, no olvido a la buena gente que me ayuda!",0);
		return;
	}
	do_say("Mmmm, quizá podrías ayudarme.",0);
}

void funcion_finalizacion(object jugador, string tema)
{
	if(environment()!=ENV(jugador))
	{
		tell_object(jugador,query_short()+" te exclama: ¡Desagradecido! ¡Sucia rata apestosa! ¡¿A dónde te crees que vas tan rápido?! ¡Qué aún no te he contado cómo mover la palanca!\n");
		return;
	}
	jugador->add_static_property("glachdavir_palanca", 1);
	do_emote("te explica cómo mover la palanca para poder abrir el acceso a las cavernas del cónclave.");
}


void event_enter(object quien_entra,string mensaje,object procedencia,object *seguidores)
{
	::event_enter(quien_entra,mensaje,procedencia,seguidores);
	if(quien_entra->query_player() && !quien_entra->query_hidden()) //Si es un jugador y no está oculto
		if(find_call_out("llegada")==-1) //Si se orbita aquí, se ejecuta el event enter 2 veces, así que controlamos esto
			call_out("llegada",2,quien_entra);
}

void llegada(object quien)
{
	if(environment()!=ENV(quien)) return;
	if (quien->dame_ciudadania() != "vagabundo") {
		do_say("Has viajado mucho desde que saliste por primera vez de "+quien->dame_nombre_ciudadania()+", ¿acaso me equivoco?", 0);
	} else {
		do_say("La vida del vagabundo es dura, y has viajado mucho intentando encontrar tu lugar en el mundo, ¿acaso me equivoco?", 0);
	}
}


void seapaga(object lanzador) {
	object entorno = environment();
	if(lanzador) tell_accion(entorno,"El chorro de agua que ha lanzado "+lanzador->query_short()+" apaga las llamas de la "+query_short()+".\n","Dejas de oir un fuego arder.\n");
	else tell_accion(entorno,"El fuego que cubria a la "+query_short()+" se apaga lentamente.\n","Dejas de oir un fuego arder.\n");
	set_short("Bruja del Pantano");
	remove_timed_property("ardiendo");
		
}

void seenciende(int pupa, string tipo, object lanzador) {
	int tiempo;
	if(query_timed_property("ardiendo")==1) {
		tell_object(lanzador,"Tu ataque de fuego aviva las llamas que envolvían a la "+query_short()+".\n");
		tell_accion(environment(lanzador),"El ataque de fuego de "+lanzador->query_short()+" aviva las llamas que envolvían a la "+query_short()+".\n","Oyes el crepitar de un fuego.\n",({lanzador}),lanzador);
		tiempo=((pupa*-1)/10)+query_timed_property("ardiendo");
		add_timed_property("ardiendo",1,tiempo);
	} else {
		tell_object(lanzador,"Tu ataque de fuego provoca que la "+query_short()+" comienze a arder.\n");
		tell_accion(environment(lanzador),"El ataque de fuego de "+lanzador->query_short()+" provoca que la "+query_short()+" comienze a arder.\n","Oyes el crepitar de un fuego.\n",({lanzador}),lanzador);
		set_short("Bruja del Pantano (%^BOLD%^RED%^ardiendo%^RESET%^)");
		tiempo=((pupa*-1)/10);
		add_timed_property("ardiendo",1,tiempo);
	}
	

}


int spell_damage(int pupa, string tipo, object lanzador, object hechizo, object arma,string nombre_ataque, mapping cfg_ataque) {//SE recupera igual que antes y los elementos que le dañan le pican mucho más (muere más rapido pero hace más daño)

	if (tipo == "fuego") { //Se quema con fuego
		seenciende(pupa, tipo, lanzador);
	} 
	
	if (tipo == "agua" && query_timed_property("ardiendo")==1) { //Se apaga con agua
		seapaga(lanzador);
	}
	return ::spell_damage(pupa,tipo,lanzador,hechizo,arma,nombre_ataque,cfg_ataque);
}

int do_death(object asesino)
{ 
	if(!asesino) {
		return ::do_death();
	}
	
    if(query_timed_property("ardiendo")==1) {
		asesino->comprobar_logro("quema_bruja", "desafios", 1, "quema_bruja_aradgorthor");
		tell_object(asesino,"Cuando propinas el golpe mortal a "+query_short()+" se forma una gran pira de fuego que consume los restos corporales.\n");
		tell_accion(ENV(TO),asesino->query_short()+" propina el golpe mortal a "+query_short()+" y se forma una gran pira de fuego que consume los restos corporales de la misma.\n",
			asesino->query_short()+" propina el golpe mortal a "+query_short()+" y se forma una gran pira de fuego que consume los restos corporales de la misma.\n",({asesino}),asesino);		
	}

	return ::do_death(asesino); 
}

void heart_beat()
{
	if(query_timed_property("ardiendo")==1 && query_time_remaining("ardiendo")<5) {
		seapaga(0);
	}
    ::heart_beat();
}

object make_corpse() {
    //Como arde no deja cuerpo
	if(query_timed_property("ardiendo")==1) return 0; //Como no creamos cuerpo, en este caso hay que devolver un 0

	return ::make_corpse(); 
  }