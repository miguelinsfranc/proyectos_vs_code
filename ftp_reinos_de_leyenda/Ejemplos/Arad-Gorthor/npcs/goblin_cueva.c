// Dherion 08/05/2017; Goblin para la cueva, basado en el fichero salteador_goblin.c.
// Grimaek 14/06/2023 arregladas maestrias

#include "../path.h";
inherit "/obj/monster.c";
#define SHADOW_ESCONDERSE "/habilidades/shadows/esconderse_sh.c"
void setup()
{
    set_name("goblin vigilante");
    set_short("Goblin vigilante");
    set_long("Este sucio y apestoso soldado desertor de la Horda negra te mira con malicia empuñando sus armas. "
      "Según su aspecto dirías que no se ha alimentado bien en meses, aunque sospechas que su hambre le da, si eso es posible, más peligrosidad.\n");
    set_main_plural("Goblins vigilantes");
    generar_alias_y_plurales();
    fijar_altura(80);
    fijar_peso_corporal(40000);
    fijar_raza("goblin");
    fijar_clase("soldado");
    fijar_religion("gurthang");
    fijar_fue(18 + !random(4));
    fijar_con(18);
    fijar_des(18);
    fijar_int(4);
    fijar_sab(4);
    fijar_car(5);
    fijar_nivel(16 + random(4) + !random(10));
    fijar_alineamiento(8500);
    load_chat(10, ({
        ":permanece atento a su alrededor, esperando que ocurra algo anormal para no aburrirse.",
        ":te mira bostezando",
      }));
    load_a_chat(50, ({
        "'¡No debiste haber venido!",
        ":se lanza sobre ti con no muy buenas intenciones.",
        "'¡Vas a morir! ¡Morir! ¡Morir!",
        "'¡No te resistas! ¡Será más doloroso para tí y tendré que esforzarme más!",
      }));
	fijar_maestria("Penetrantes ligeras", 40+random(11));
    switch (random(4)) {
    case 0:
        add_clone(BARMAS + "daga", 1);
        break;
    case 1:
        add_clone(BARMAS + "puñal", 1);
        break;
    case 2:
        add_clone(BARMAS + "estilete", 1);
        break;
    case 3:
        add_clone(BARMAS + "cuchillo", 1);
        break;
    }
    add_clone(BARMADURAS + "zapatillas", 1);
    add_clone(BARMADURAS + "pantalones", 1);
    if (!random(5)) {
        add_clone(BARMADURAS + "cuero", 1);
    }
    init_equip();
    set_aggressive(1);
    if (!random(10)) {
        ajustar_dinero(1 + random(2), "oro");
    } 
    ajustar_dinero(roll(2, 10), "plata");
}
