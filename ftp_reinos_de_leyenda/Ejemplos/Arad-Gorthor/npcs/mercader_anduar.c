// Dunkelheit 14-11-2009

#include "../path.h";
#include <depuracion.h>
inherit "/obj/conversar_npc.c";

#define DESTINO "/d/anduar/rooms/anduar/murallas/ruinmurnor_8"

status atado = 1;

string short(int oscuridad)
{
    return ::short(oscuridad) + (atado ? " (atado al poste)" : "");
}

void setup()
{
    set_name("mercader");
    set_short("Mercader de Anduar");
    add_alias("anduar");
    set_long("Un aterrorizado mercader de Anduar. El pobre ha acabado en manos "
             "de un grupo de osgos, "
             "atado en un poste de madera de la inhóspita Arad Gorthor. Su "
             "lujosa ropa está "
             "hecha jirones y llena de polvo y restos de sangre.\n");
    set_main_plural("Mercaderes de Anduar");
    add_plural("mercaderes");

    fijar_altura(170 + random(11));
    fijar_peso_corporal(75000);

    fijar_raza("humano");
    fijar_subraza("bheniense");
    fijar_clase("aventurero");
    fijar_religion("ateo");

    fijar_fue(15);
    fijar_con(18);
    fijar_des(13);
    fijar_int(14);
    fijar_sab(12);
    fijar_car(13);

    fijar_nivel(20);

    add_clone(BARMADURAS + "tunica", 1);
    add_clone(BARMADURAS + "botas", 1);
    add_clone(BARMADURAS + "guante", 1);
    add_clone(BARMADURAS + "guante_izq", 1);
    init_equip();

    nuevo_dialogo(
        "apresado",
        ({
            "'Qué necio, pero qué necio he sido...",
            "'Ya me lo dijo mi cliente, ¡no se te ocurra llevar mis obras de "
            "arte más allá de Dendra!",
            "'Menos mal que no viajaba con ninguna pieza de valor",
            "'Esos tres osgos me apresaron cuando intentaba volver al camino "
            "de Mnenoic",
        }));
    nuevo_dialogo(
        "osgos",
        ({
            ":rebaña la sangre que discurre por su arrugada frente.",
            "'Te juro que sólo los había visto en grabados",
            "'¡No pensé que existieran todavía, creía que se extinguieron en "
            "la Era 2ª!",
            "'Pero ahora tú también los has visto... por Comellas, ¡son más "
            "horribles que un goblin!",
            "'En fin... ¿y ahora qué?",
        }),
        "apresado");
    nuevo_dialogo(
        "ahora",
        ({
            "'Si no te importaría me gustaría formar parte de tu grupo de "
            "aventuras",
            "'Llévame a la puerta norte de Anduar, por favor...",
            "'Estoy muy débil como para hacer el viaje de vuelta yo sólo",
            "'¡Te recompensaré!",
            "'Vamos, vamos, marchémonos...",
        }),
        "osgos");
    habilitar_conversacion();

    fijar_ciudadania("anduar", 601);

    load_chat(
        100,
        ({
            "'¡Socorro! ¡auxilio!",
            "'¡Salvadme por favor, os lo ruego!",
            "'¡Ayudadme, os lo suplico! ¡os dare todo cuanto tengo!",
            ":se revuelve contra las cuerdas que lo mantienen atado al poste.",
        }));

    nuevo_lenguaje("adurn", 100);
    nuevo_lenguaje("elfico", 65);
    nuevo_lenguaje("negra", 65);
    nuevo_lenguaje("gnomo", 65);
    nuevo_lenguaje("lagarto", 65);
}

void init()
{
    ::init();
    add_action("liberar", ({"liberar", "soltar", "desatar", "rescatar"}));
    add_action("agruparse", "agruparse");
}

int puede_conversar()
{
    if (atado) {
        tell_accion(
            environment(),
            "El mercader está maniatado y demasiado nervioso como para "
            "conversar.\n");
        return 0;
    }
    return 1;
}

int liberar(string str)
{
    object quien;

    quien = this_player();
    if (!str || !regexp(str, "mercader")) {
        return notify_fail(
            "¿" + capitalize(query_verb()) + " a quién?\n", this_player());
    }

    if (!atado) {
        return notify_fail(
            "El mercader ya es libre, no le marees más.\n", this_player());
    }

    if (secure_present(NPCS + "picaro_osgo", environment())) {
        return notify_fail(
            "Resulta imposible acercarse al mercader con todos esos osgos "
            "custodiándolo.\n",
            this_player());
    }
	DAR_TOKEN(this_player(), "cementerio", "mercader_anduar");
    atado = 0;

    tell_object(
        this_player(),
        "Desatas las cuerdas que mantenían preso al mercader.\n");
    tell_accion(
        environment(),
        this_player()->query_short() +
            " desata las cuerdas que mantenían preso al mercader.\n",
        "",
        ({this_player()}),
        this_player());

    if (!quien->dame_hito_mision("mercader-secuestrado", "mercader-liberado")) {
        quien->nuevo_hito_mision(
            "mercader-secuestrado", "mercader-liberado", 1);
    }

    quien->consumir_hb();

    do_say(
        "¡No sé cómo darte las gracias, " +
            this_player()->dame_nombre_completo(2) + "!",
        0);
    do_say(
        "Por favor, tened la bondad de agruparme y llevarme a la puerta norte "
        "de Anduar. ¡Por piedad! ¡os daré una jugosa recompensa!",
        0);

    set_long("Este mercader de Anduar ha sido liberado de las viles garras de "
             "un grupo "
             "de osgos, y ahora espera que alguien le agrupe para llevarlo de "
             "vuelta a su hogar.\n");

    load_chat(
        10,
        ({"'¡Qué ganas tengo de llegar a la taberna y refrescar un poco mi "
          "gaznate!",
          ":limpia la sangre de sus heridas.",
          "'Esos miserables osgos... ¡no volveré a cruzar la frontera norte de "
          "Dendra!"}));

    remove_chat_string(-1);

    return 1;
}

int agruparse(string str)
{
    if (str && regexp(str, "(invitar)*(mercader)")) {
        if (-1 != member_array(
                      this_player()->dame_raza(),
                      ({"orco", "goblin", "uruk-hai", "gnoll", "kobold"}))) {
            tell_object(
                this_player(),
                "Tu raza no le inspira ninguna confianza al mercader. Parece "
                "que prefiere morir de hambre antes que seguirte.\n");
            return 1;
        }
        if (atado) {
            tell_object(
                this_player(),
                "Atiende, lumbrera. Mientras esté atado al poste, difícilmente "
                "podrá unirse a tu grupo.\n");
            return 1;
        }
        if (dame_grupo_seguir()) {
            tell_object(
                this_player(),
                "El mercader ya está en un grupo. No le marees más.\n");
            return 1;
        }
        do_say("¡Marchemos rápido a Anduar la bella!", 0);
        this_player()->nuevo_seguidor(this_object());
        prohibir("ahora", "LalalalaPedorretasXD");
    }
    return 0;
}

void premio(object quien)
{
    do_say(
        "¡Muchas gracias, " + quien->query_short() +
            "! ¡no sé cómo darte las gracias por salvarme la vida!",
        0);
    do_say(
        "Ten, aquí tienes todo mi dinero. Además, le contaré a todo el mundo "
        "lo que has hecho por mí. ¡Hasta pronto!",
        0);

    quien->ajustar_dinero(75 + random(26), "platino");
    quien->ajustar_estatus("Anduar", 10);
    quien->ajustar_xp(10000 + random(5000));

    if (!quien->dame_hito_mision("mercader-secuestrado", "llegada-anduar")) {
        quien->nuevo_hito_mision("mercader-secuestrado", "llegada-anduar", 1);
    }

    write_file(
        LOGS + "mercader_anduar",
        ctime() + " " + quien->query_short() + " termina la quest.\n");

    quien->quitar_seguidor(this_object());
    "/cmds/mortal/agruparse.c"->abandonar_grupo(quien, this_object());

    do_emote("desaparece entre el gentío resplandeciente de alegría.");
    dest_me();
    return;
}

varargs mixed move_player(
    string dir, mixed dest, mixed message, object ob_que_sigo, string enter)
{
    string path_dest;
    __debug_x(dest, "arad-gorthor");
    if (objectp(dest)) {
        path_dest = base_name(dest);
    } else {
        path_dest = dest;
    }
	__debug_x(file_name(dest), "arad-gorthor");
    if (file_name(dest)== DESTINO) {
		__debug_x("destino1", "arad-gorthor");
        ::move_player(dir, dest, message, ob_que_sigo, enter);
		__debug_x("destino2", "arad-gorthor");
        if (dame_grupo_seguir()) {
            premio(dame_grupo_seguir());
            return 0;
        }
    }

    return ::move_player(dir, dest, message, ob_que_sigo, enter);
}

int do_death(object asesino)
{
    if (asesino) {
        if (asesino->dame_mision_abierta("mercader-secuestrado")) {
            asesino->nuevo_hito_mision(
                "mercader-secuestrado", "mercader-asesinado", 1);
        } else {
            asesino->nuevo_hito_mision(
                "mercader-secuestrado", "mercader-muerto", 1);
        }
    }

    return ::do_death(asesino);
}
