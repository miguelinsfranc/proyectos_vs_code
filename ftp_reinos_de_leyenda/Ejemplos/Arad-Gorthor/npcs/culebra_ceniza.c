//Nohur 03/02/2019
//Altaresh 19 modificando despues de qc de Satyr

inherit "/obj/monster";
#include <combate.h>


void setup() 
{
    object estilo;
	set_name("culebra ceniza");
	set_short("Culebra de ceniza");
	set_main_plural("Culebras de ceniza");
	add_alias(({"culebra","ofidio","reptil","animal","ceniza"}));
	add_plural(({"culebras","ofidios","reptiles","animales","cenizas"}));
	set_long("Esta pequeña culebra se encuentra completamente mimetizada en el entorno que la rodea, el color de sus toscas"
	         " escamas es exactamente igual al de la ceniza en torno a ella, sólo eres capaz de divisarla por sus particulares"
			 " ojos oscuros. Aunque la mayoría suelen encontrarse entre las rocas, muchas se entierran en el suelo para atacar "
			 "a sus víctimas apareciendo desde la propia ceniza.\n");
	// fijar_imagen("monstruos/serpiente.gif");

	fijar_raza("ofidio");
	set_random_stats(3,12);
	fijar_tamanyo_racial(1);
	fijar_peso(1000+random(600));

	fijar_nivel(8+random(5));
	fijar_genero(2);
	
    load_chat(30,({
    ":se enrosca amenazante.",
    "$animales/serpiente2.wav$sisea mostrando su lengua."
    }));
    //masterclass de satyr
    
    // Clonamos el estilo desarmado que use.
    // Esto sirve para que podamos hacer cambios en él sin afectar al resto de PNJS
    // del mud que usen este estilo.
    clonar_estilo();

    // Ahora le metemos un efecto para envenenar, así ya se usa el sistema de 
    // combate normal y no hay que preocuparse de errores, "lo hace todo la mudlib"
    // Esto es un efecto, no efecto básico, permite personalizar un poco las cosas
    estilo = dame_ob_estilo();
    estilo->nuevo_efecto(
    // Un nombre para el efecto
    "envenenar",
    // Qué ataque envenena
    estilo->dame_ataque_principal(),
    // Ayuda. No es necesaria
    "",
    // Al impactar
    ({_TRIGGER_IMPACTO}),
    // un 30% en base 1000
    300,
    // si especificamos un texto así mete una incapacidad. Hay más opciones
    "venenos/circulatorio_daño",
    // "nivel" de la incapacidad
    50,
    // "duración" de la incapacidad
    600
    );


}
