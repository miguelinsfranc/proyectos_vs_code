// Dherion Marzo 2017: NPC soldado que avisa que el área es chunga... A su manera
#include "../path.h";
inherit "/obj/conversar_npc.c";
int comprobar_bando(object pj) {
    if(pj->dame_bando()=="malo")
        return 0;
    else
        return 1;
}
void avisar_peligro() {
    object* pjs= filter(all_inventory(ENV(TO)),(:$1->query_player() && !$1->query_hidden():));
    object pj;
    if (!ENV(TO)) {
        return;
    }
    if (sizeof(pjs)) 
        pj=element_of(pjs);
    if(!pj) return 0;
    do_say(pj->query_cap_name()+", escúchame!",0);
    do_say("Este camino es peligroso. No deberías ir sol"+pj->dame_vocal()+", y"+
      "si lo haces...",0);
    do_say("¡Ve con cuidado!",0);
}
void setup_conversacion() {
    nuevo_dialogo("nombre","Soy un soldado de Endor, enviado para patrullar y
controlar este camino.");
    nuevo_dialogo("camino",({
        ":asiente afirmativamente",
        "'Este camino, como puedes ver lleva mucho tiempo abandonado.",
        "'A Eldor no le interesa mantenerlo para evitar que los orcos de
Golthur puedan llegar con facilidad.",
        "'Y a los orcos supongo que no les interesa para evitar que los
ataquemos.",
        "'Aún así los soldados Eldorenses patrullamos en ocasiones para evitar
problemas.",
        "'Si nos encontramos con un orco, goblin o similar lo matamos. O al
menos lo intentamos...",
        "'Pero ahora hay un problema más del que preocuparse.",
        "'En ocasiones nos viene bien, pero...",
        "'Perdona, no me explico bien...",
        "'últimamente se ha visto por estos caminos a varios goblins desertores
de la Horda Negra.",
        "'Atacan indiscriminadamente a todo lo que se les cruce por medio para
sobrevivir."
      }),"nombre");
    nuevo_dialogo("problema",({
        "'Se están convirtiendo en una pesadilla",
        "'A la mínima que te descuidas aparece un goblin para asaltarte",
        "'Si lo unes a lo mal que está el camino y a las patrullas de
orcos...",
        "'Sospechamos que estos goblins se esconden en el bosque que hay hacia
el oeste de aquí",
        "'Pero no hemos tenido tiempo de ir a investigar",
        "'Si alguien pudiera ayudarnos...",
        "hito arad_plaga_goblin pista_inicial"
      }),"camino");
    nuevo_dialogo("ayudarnos",({
        "'Las grandes azañas se esparcen rápido...",
        "'Algo había llegado a mis oídos, pero no quise darle mucha
credibilidad.",
        "'No durará mucho, pues volverán a reagruparse, pero aún así es un
alivio.",
        ":sonríe agradecido.",
        "'Muchas gracias por tu ayuda. permíteme recompensarte.",
        "hito arad_plaga_goblin recompensa_final"
      }),"problema");
    prohibir("ayudarnos","hito arad_plaga_goblin lider_muerto");
    prohibir("camino","consultar_bando");
    habilitar_conversacion();
}
void setup()
{
    setup_conversacion();
    fijar_lenguajes((["adurn":100,"eldorense":100]));
    fijar_lenguaje_actual("adurn");
    set_name("soldado");
    set_short("Soldado");
    set_main_plural("Soldados");
    generar_alias_y_plurales();
    set_long("Este soldado parece fatigado. Desaliñado y sucio, camina
lentamente, con una mano en la empuñadura de su arma y una mirada alerta en sus
ojos grises, listo para entrar en combate pese a su aparente cansancio.\n");
    fijar_religion("hiros");
    fijar_raza("humano");
    fijar_clase("luchador");
    fijar_alineamiento(-10000);
    fijar_genero(1);
    set_random_stats(17,18);
    fijar_nivel(40);
    add_clone(BARMAS + "espada_larga.c",1);
    add_attack_spell(40,"golpecertero",3);
    init_equip();
    // Creamos el primer paso.
    nuevo_paso_interaccion("cv_soldado", TO,   
      ({"'¡Por fin!",
        "'Pronto llegaré a Eldor...",
        "#avisar_peligro"})
    );
    nuevo_paso_interaccion("cv_soldado", "goblin",
      ({":fija la mirada en el soldado, saliendo de entre unos matorrales
mientras se relame.",
        "'¡Vas a morir!"})
    );
    nuevo_paso_interaccion("cv_soldado", TO, 
      ({":se detiene en seco y se gira, encarando al goblin.",
        "'Ya decía yo que estaba resultando sencillo este último tramo..."})
    );
    nuevo_paso_interaccion("cv_soldado", "goblin",
      ({":empuña con fuerza y rabia su arma, lanzándose contra el soldado."})
    );
    nuevo_paso_interaccion("cv_soldado", "goblin",
      ({"'¡No saldrás vivo de aquí!"}),
    );
    nuevo_paso_interaccion("cv_soldado", TO, 
      ({":salta a un lado esquivando la acometida del goblin.",
        "#atacar_soldado"})
    );
    // esto es opcional, pero hace el intercambio más rápido.
    fijar_retardo_interacciones(2);
    // se inicia la interacción
    load_chat(300,({
        "#iniciar_cualquier_interaccion",
      }));
}
void atacar_soldado() {
object goblin = secure_present(NPCS+"goblin_interaccion.c", environment());
    if ( goblin )
        attack_ob(goblin);
}
