// Dunkelheit 22-11-2009

#include "../path.h";
inherit "/obj/conversar_npc.c";

void setup()
{
	set_name("arauko");
	set_short("Arauko el Trinchaelfas");
	add_alias("trinchaelfas");
	set_long("Parece que el requisito indispensable para regentar una taberna o una "
	"cantina goblinoide es ser mórbidamente obeso, y Arauko no es una excepción. "
	"Este descomunal orco de alrededor de un metro ochenta de altura es fácilmente "
	"reconocible por sus enormes, hirsutas y pobladas patillas, tan largas que alcanzan "
	"casi su clavícula. Se mueve con notable agilidad, sorteando taburetes caídos, "
	"borrachos y vasos rotos en el suelo, con la confianza y paciencia que otorgan "
	"dos decenios de experiencia tras una barra, sirviendo a energúmenos. En días "
	"anteriores, Arauko era un aguerrido soldado del Ejército Negro que se licenció "
	"tras ser descubierto -demasiadas veces- manteniendo relaciones sexuales con "
	"las elfas del harén de Angra, escarceo que no gustó nada a la susodicha. Por "
	"éste y otros sucesos similares, Arauko recibió el apodo de \"el trinchaelfas\". "
	"Es por todos sabido que Arauko se las arregla para tener siempre al menos a "
	"una elfa encadenada en su sótano, con la que se despecha agusto tras los largos "
	"días de trabajo.\n");
	set_main_plural("Mordgrimm el Trinchaelfas");
	add_plural("mordgrimms");
	
	fijar_altura(188);
	fijar_peso_corporal(123800);
	
	fijar_raza("orco");
	fijar_clase("soldado");
	fijar_religion("gurthang");
	
	fijar_fue(18);
	fijar_extrema(100);
	fijar_con(18);
	fijar_des(10);
	fijar_int(9);
	fijar_sab(8);
	fijar_car(13);
	
	fijar_nivel(39);
	fijar_alineamiento(18500);
	
	add_clone(BARMAS + "cuchillo", 1);
	add_clone("/d/golthur/items/delantal_sucio", 1);
	add_clone(BARMADURAS + "botas", 1);
	init_equip();

	ajustar_dinero(roll(1,4), "platino");
	
	ROL_NPCS->definir(this_object());
	
	load_chat(30, ({
		":arrastra ruidosamente las flemas de su garganta y lanza un denso escupitajo sobre la barra, pasando el trapo a continuación para limpiarla.",
		":rebaña en un paño la sangre acumulada en su cuchillo.",
		":apila media docena de jarras de cerveza y las lleva a la mesa de unos clientes.",
		"'¡Qué corra la cerveza, zánganos!",
		"'¡Eh, tú! ¡Si no vas a consumir, te aconsejo que te largues!",
		":da varios pisotones en el suelo para azuzar a los de cocina, que trabajan sin parar en el sótano.",
		":lleva platos de un sitio a otro con encomiable agilidad.",
	}));
	
	load_a_chat(50, ({
		"'¡¿Una pelea en MI cantina?! ¡Vais a ver lo que es bueno!",
		"'¡Serás el menú del día, zoquete!",
		":lanza varios taburetes por los aires.",
	}));
	
	habilitar_conversacion();
	
	fijar_pvs_max(7500);
	fijar_pe_max(2000);
	
	nuevo_dialogo("recomendaciones", ({
		":rebaña el sudor de su frente y pone cara de estar \"pensando\".",
		"'Nuestro surtido de ratas hace las delicias de los gourmets más exigentes",
		"'Yo te recomiendo la rata con ajos",
		"'Queda una carne muy tierna y jugosa",
		"'Y los ajos te ayudarán con esa halitosis que padeces",
		":mesa sus gigantescas patillas y sonríe.",
		"'Y para beber, sin lugar a duda, la cerveza roja de Arad Gorthor",
		"'Para bebidas más fuertes te recomiendo que vayas a la licorería de Cirrax",
		"'¡Todo el mundo acude allí entrada la madrugada!",
		"'Bueno, ¿te decides o no?",
	}));
	
	nuevo_dialogo("cirrax", ({
		":parece importunado ante tal pregunta."
		"'¡Oye! ¡No sé por quién me has tomado, si lo que quieres es cotillear...",
		":se relaja y sonríe.",
		"'...mejor pregunta a Korlash",
		"'Él conoce a todos los tipos importantes de la ciudad, a los huargos gordos",
		"'Bueno... más que conocer...",
		"'¡No les quita el ojo de encima! ¡menudo cuervo está hecho!",
	}), "recomendaciones");
	
	nuevo_dialogo("clientela", ({
		"'No suele haber muchos problemas con la gentuza que viene aquí a pastar",
		":ríe a carcajadas.",
		"'Mira, peleas y muerte hay todos los días",
		"'Pero al menos aquí no me paso tanto tiempo sacando cadáveres de la cantina como en Golthur",
		"'Ser aprendiz de camarero allí fue un suplicio",
		"'Menos mal que me alisté en el extinto Ejército Negro",
	}), "recomendaciones");
	
	nuevo_dialogo("ejercito", ({
		"'Ahhh qué tiempos aquéllos...",
		"'Cuando todavía se dirigía incursiones contra la asquerosa Orgoth",
		":escupe al suelo al nombrar el reino élfico.",
		":empieza a hablar como para sí mismo.",
		"'Esas elfas indefensas, con esa carne rolliza ensangrentada y amoratada...",
		":empieza a salivar considerablemente.",
		"'Les cortaba pedacitos triangulares de carne con este cuchillo que tengo entre mis manos",
		"'Cada bocadito era una explosión de sabor y placer en mis fauces",
		"'Cuando regresé a la fortaleza, ya no había más elfas con las que jugar...",
		"'...salvo las de Angra",
		"'Esa vieja bruja me pilló en mitad de una de mis... sesiones",
		"'¡Hizo que me licenciasen! ¡Nunca me lo perdonó!",
		"'Me encantaría poder hacer negocios con ella, me ahorraría mucho dinero, no te creas que es fácil conseguir elfas frescas hoy en día",
		":sonríe tristemente.",
	}), "clientela");
}

int frases_personalizables(string tipo, object npc, object pj)
{
	switch (tipo)
	{
		case "bienvenida-pj":
			if (HORDA_NEGRA->es_oficial(pj)) {
				npc->do_emote("clava su cuchillo en la barra y alza los brazos con alegría.");
				npc->do_say("¡Salve, "+HORDA_NEGRA->dame_titulo_corto(pj)+"! ¿qué se le antoja hoy para beber?", 0);
			} else {
				tell_object(pj, npc->query_short() + " escupe en un vaso (tu vaso) y lo planta con desdén frente a ti.\n");
				tell_accion(environment(pj), npc->query_short() + " escupe en el vaso de "+pj->query_short()+" y se lo planta en las narices.\n", "", ({ pj, npc }), npc);
				npc->do_say("¿Qué vas a tomar?", 0);
			}
			break;
		case "bienvenida-npc":
			break;
		case "despedida-pj":
			pj->do_say("Gracias por la conversación; ahora me gustaría beber con tranquilidad", 0);
			break;
		case "despedida-npc":
			npc->do_say("Cómo quieras, oye... ¿no tendrás alguna hermana elfa, no? ¡HAHAHA!", 0);
			break;
		default:
			return 0;
	}

	return 1;
}
