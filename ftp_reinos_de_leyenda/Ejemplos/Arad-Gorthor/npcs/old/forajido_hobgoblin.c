// Dunkelheit 07-11-2009

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	set_name("forajido");
	set_short("Forajido Hobgoblin");
	add_alias("hobgoblin");
	set_long("Un hobgoblin de piel rojiza y ropas de camuflaje ideales para pasar "
	"desapercibido por las desérticas tierras de Arad Gorthor. Los hobgoblins son "
	"una raza mucho más inteligente que los goblins y esto, sorprendentemente, les "
	"ha pasado factura al ser una minoría racial. La mayoría de hobgoblins vive "
	"como saltadores, forajidos y bandoleros, atacando a cualquier criatura que "
	"se cruza en sus caminos. Y este ejemplar no es una excepción.\n");
	set_main_plural("Forajidos Hobgoblin");
	add_plural(({"forajidos","hobgoblins"}));
	
	fijar_altura(80);
	fijar_peso_corporal(40000);
	
	fijar_subraza("hobgoblin");
	fijar_clase("soldado");
	fijar_religion("gurthang");
	
	fijar_fue(18);
	fijar_extrema(100);
	fijar_con(18);
	fijar_des(13);
	fijar_int(9);
	fijar_sab(3);
	fijar_car(3);
	
	fijar_nivel(19 + random(2));
	fijar_alineamiento(9000);
	
	load_a_chat(50, ({
		"'¡Espero que lleves algo de valor por lo que merezca la pena matarte!",
		":se ríe a carcajadas, parece que no le importa mucho morir.",
	}));

	switch (random(3)) {
		case 0:
			add_clone(BARMAS + "espada_corta", 1);
			add_clone(BESCUDOS + "escudo_madera", 1);
			fijar_maestria("espada corta", 30);
			add_attack_spell(40, "herir", 3);
			break;
		case 1:
			add_clone(BARMAS + "lanza", 1);
			fijar_maestria("lanza", 30);
			break;
		case 2:
			add_clone(BARMAS + "cadena", 1);
			fijar_maestria("cadena", 50);
			add_attack_spell(40, "atrapar", 3);
			break;
	}
	
	add_attack_spell(50, "golpecertero", 3);
	
	add_clone(BARMADURAS + "botas", 1);
	add_clone(BARMADURAS + "cinturon", 1);
	add_clone(BARMADURAS + "cuero_tachonado", 1);

	init_equip();
	
	set_aggressive(1);
	set_join_fight_mess("emboscada");
	
	if (!random(5)) {
		ajustar_dinero(1 + random(2), "oro");
	} 
	ajustar_dinero(roll(2, 10), "plata");

}

void emboscada(object victima)
{
	if (query_static_property("emboscada_iniciada")) {
		return;
	}
	add_static_property("emboscada_iniciada", 1);
	
	do_say("¡Despertad muchachos, acaba de llegar la comida! ¡JAJAJA!", 0);
}
