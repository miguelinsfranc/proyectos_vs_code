	
#include "../path.h";
inherit "/obj/monster";

void setup() {
    set_name("tendero");
    set_short("Tendero goblin");
    add_alias("goblin");
    set_main_plural("Tenderos goblin");
    fijar_raza("goblin");
    set_long("El tendero es un goblin cojo, feo, bajo y bizco, sus ojos miran a su nariz y su nariz a "
		"su barbilla, su barbilla... prefieres no seguir la dirección de su barbilla. Su cuerpo esquelético "
		"se mueve con torpeza entre los escasos bártulos y sus manos, nerviosas y nervudas, manosean algunas "
		"monedas de su bolsillo con una avaricia enfermiza.\n");
    set_random_stats(14,18);
	nuevo_lenguaje("negra",100);
    fijar_nivel(10+random(10));
    ajustar_dinero(1+random(10),"bela");
    
    
}
