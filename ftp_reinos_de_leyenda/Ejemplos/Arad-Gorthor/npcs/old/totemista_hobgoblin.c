// Dunkelheit 15-11-2009

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	set_name("hobgoblin");
	set_short("Totemista Hobgoblin");
	add_alias("totemista");
	set_long("Los totemistas son una orden chamánica extremadamente fanática y anárquica. Se toman "
	"la palabra de Gurthang completamente por su mano y a su antojo. Su avaricia y su ambición no "
	"conocen límites, y eso no sólo complace a su deidad, sino hace que contemple con condescendecia "
	"cualquier exceso a la hora de ponerla en práctica. Los totemistas visten largas túnicas negras "
	"colmadas con una capucha que sumerge su rostro en sombras.\n");
	set_main_plural("Totemistas Hobgoblins");
	add_plural(({"totemistas", "hobgoblins"}));
	
	fijar_altura(100 + random(20));
	fijar_peso_corporal(60000 + random(20000));
	
	fijar_subraza("hobgoblin");
	fijar_clase("chaman");
	fijar_religion("gurthang");
	
	fijar_fue(13);
	fijar_con(17);
	fijar_des(15);
	fijar_int(6);
	fijar_sab(18);
	fijar_car(12);
	
	fijar_nivel(26);
	fijar_alineamiento(9000);
	
	add_loved("raza", "goblin");
	set_aggressive(1);
	
	switch (random(2)) {
		case 0:
			add_clone(BARMAS + "jabalina", 1);
			break;
		case 1:
			add_clone(BARMAS + "garrote", 1);
			break;
	}
	add_clone(BARMADURAS + "tunica", 1);
	add_clone(BARMADURAS + "capucha", 1);
	init_equip();
		
	ajustar_dinero(roll(1, 5), "oro");
	
	add_attack_spell(10, "columna de fuego", 3);
	add_attack_spell(40, "curar heridas moderadas", 2);
}
