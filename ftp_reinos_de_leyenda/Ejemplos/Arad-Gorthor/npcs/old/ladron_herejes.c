// Rezzah 15/05/2011

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	string raza = "goblin";
	string clase = "ladron";
	
	set_name("ladron");
	set_short("Ladronzuelo");
	set_main_plural("Ladronzuelos");
	add_plural(({"ladronzuelos","ladrones"}));
	fijar_genero(1);

	fijar_altura(100 + random(30));
	fijar_peso_corporal(75000);

	fijar_raza(raza);
	fijar_clase(clase);
	fijar_religion("gurthang");
	fijar_bando("malo");
	set_long("Son la carne de cañón de la Horda Negra. La primera línea de batalla. "
	"El escalón más bajo; los últimos en enterarse de que van a morir: los snagas. "
	"Este "+dame_clase()+" "+dame_raza()+" está deseando escalar peldaños en la larga jerarquía "
	"de la horda y no dudes que estará dispuesto a hacer lo que sea. Su indumentaria "
	"es acorde a su escalafón: chatarra. Dañina, pero chatarra. Pero la función "
	"de los snagas no sólo se limita a ser un muro de carne en las incursiones, "
	"sino que también patrullan el bastión de Arad Gorthor, actividad que "
	"compaginan con sus intensos entrenamientos en la ciudad goblinoide.\n");
	
	fijar_fue(18);
	fijar_con(18);
	fijar_des(19);
	fijar_int(17);
	fijar_sab(14);
	fijar_car(14);
	
	fijar_nivel(30 + random(6));
	fijar_alineamiento(2501 + random(2000));
	
	if (!random(5)) {
		add_clone(BARMADURAS + "anillo", 1);
	}
	if (!random(5)) {
		add_clone(BARMADURAS + "amuleto", 1);
	}
	if (!random(8)) {
		add_clone(BARMADURAS + "collar", 1);
	}
	add_clone(BARMAS + "daga", 1);
	
	init_equip();
	
	ajustar_dinero(roll(1, 10), "plata");
	
	ajustar_bo(90);
	fijar_cobardia(99);
	add_zone("bazar");
}
