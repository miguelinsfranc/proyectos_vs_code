// Dunkelheit 10-11-2009

#include "../path.h";
inherit "/obj/conversar_npc.c";

void setup()
{
	string raza = element_of(({"orco", "goblin" }));
	
	set_name("pordiosero");
	set_short("Pordiosero " + capitalize(raza));
	add_alias(raza);
	set_long("Este miserable "+raza+" no ha sabido encontrar su lugar en la "
	"sociedad de Arad Gorthor -ni en Golthur ni Mor Groddûr-, así que se ve "
	"obligado a ejercer la mendicidad. Su aspecto es desaliñado y muy descuidado, "
	"y hasta dirías que huele un poco al viejo licor de la abuela goblin. Dada "
	"su condición, es algo de lo más comprensible.\n");
	set_main_plural("Pordioseros "+capitalize(raza)+"s");
	add_plural(({"matones", raza+"s"}));
	
	if (raza == "orco") {
		fijar_altura(150 + random(20));
		fijar_peso_corporal(70000 + random(20000));
	} else {
		fijar_altura(120 + random(20));
		fijar_peso_corporal(40000 + random(20000));
	}
	
	fijar_raza(raza);
	fijar_clase("aventurero");
	fijar_religion("gurthang");
	
	fijar_fue(10 + random(4));
	fijar_con(14 + random(2));
	fijar_des(10 + random(6));
	fijar_int(4);
	fijar_sab(4);
	fijar_car(5);
	
	fijar_nivel(5 + random(6));
	fijar_alineamiento(501);
	
	ajustar_dinero(1 + random(2), "cobre");
	
	ROL_NPCS->definir(this_object());
	
	nuevo_dialogo("limosna", ({
		"'¿Me puedes dar unas monedas, por favor?",
		"'La vida en Arad Gorthor es muy dura",
		"'Si no tengo dinero ni para comer yo...",
		"'...¿cómo alimentaré a mis "+query_num(roll(2,3), 10, 1)+" hijos?",
	}));
	nuevo_dialogo("dura", ({
		"'Aquí imperan la vida militar y la práctica religiosa",
		"'No hay lugar para nadie más",
		"'Emigraría a alguna otra ciudad... ¡pero son todavía peores!",
		"'¡Y en Anduar no somos bien recibidos!",
		"'¿Qué puedo hacer?",
	}), "limosna");
	nuevo_dialogo("suicidio", ({
		":se ríe sarcásticamente.",
		"adios",
	}), "dura");
	
	habilitar_conversacion();
	
	load_chat(30, ({
		"'¿Alguien podría darme unas monedillas con las que pasar la noche?",
		":extiende su mano ante todos los que se cruzan con él.",
		"'Unas moneditas, por favor...",
	}));
	load_a_chat(50, ({
		"'¡Mátame! ¡acaba ya con mi miseria!",
	}));
}

void adios(object quien)
{
	if (quien->dame_genero() == 2) {
		abandonar_conversacion(quien, "¡Déjame en paz, vieja bruja... sucia prostituta!");
	} else {
		abandonar_conversacion(quien, "¡Piérdete, tú que seguro que haces favores... eróticos a los dendritas!");
	}
}

int frases_personalizables(string tipo, object npc, object pj)
{
	switch (tipo)
	{
		case "saludo":
			pj->do_say("Saludos, "+npc->dame_nombre_raza()+". ¿Qué tal va la vida?", 0);
			break;
		case "bienvenida-pj":
			if (HORDA_NEGRA->es_oficial(pj)) {
				npc->do_say("¡Oh, por Lord Gurthang! ¡qué honor poder hablar con Usted!", 0);
				npc->do_emote("se arrodilla ante ti.");
			} else {
				npc->do_say(element_of(({
					"Mal, muy mal... no tengo dinero ni para comprar una hogaza de pan",
					"Podría irme mejor con unas monedillas",
					"Fatal, hoy no he conseguido hacer ni un mísero estaño",
				})), 0);
			}
			break;
		case "bienvenida-npc":
			break;
		case "despedida-pj":
			pj->do_say("Adiós, tengo que irme", 0);
			break;
		case "despedida-npc":
			if (HORDA_NEGRA->es_oficial(pj)) {
				npc->do_say("Gracias por esta conversación, gran "+pj->query_short(), 0);
			} else {
				npc->do_say("Bueno, hasta la vista", 0);
			}
			break;
		default:
			return 0;
	}

	return 1;
}

int recibir_pago(int cobres, object quien, mapping monedas)
{
	if (HORDA_NEGRA->es_oficial(quien)) {
		do_say("¡Su Eminencia es harto generosa, que Lord Gurthang se lo tenga en cuenta!", 0);
	} else {
		switch (cobres)
		{
			case 1:
				do_say("Bueno... un cobre... en fin... ¿gracias?", 0);
				do_emote("te llama algo en voz baja que prefieres no entender.");
				break;
			case 2..10:
				do_say("¡Muchas gracias, muy agradecido!", 0);
				break;
			case 11..100:
				do_say("¡Esto es mucho dinero, mil gracias!", 0);
				do_emote("hace una profunda reverencia.");
				break;
			default:
				do_say("¡Con este dineral mi hija podrá pagarse sus estudios de magia en la academia de Anduar! ¡No sé cómo darte las gracias!", 0);
				do_emote("te da un sentido abrazo.");
				break;
		}
	}
	
	if (cobres >= 5000) {
		do_say("¡Lupanar de Madam Zhariagh, allá voy!", 0);
		do_emote("piensa en voz alta:");
		do_say("¿Cómo era el santo y seña?", 0);
		do_say("¡Ah, sí! \"No me tires de la lengua\"", 0);
		run_away();
	}
	
	return 1;
}
