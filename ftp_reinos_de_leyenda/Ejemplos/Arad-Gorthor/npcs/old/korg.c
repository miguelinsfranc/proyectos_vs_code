// Dunkelheit 18-12-2009

#include "../path.h";
inherit "/obj/monster.c";

#include "../include/inteligencia.h"

int dame_guardia() { return 1; }

int check_anyone_here() { return 1; } // Para no perder el HB

void setup()
{
	set_name("korg");
	set_short("Korg, el Cancerbero");
	add_alias(({"korg", "cancerbero", "orco", "guardia_arad"}));
	set_long("¿Quién será el insensato que mandó delegar la custodia de unas puertas "
	"descomunales a un orco fibroso y menudo, de rostro chupado, casi calavérico? Con "
	"cuatro pelos blancos mal repartidos, más parece una momia recién levantada de su "
	"descanso eterno. Pero si acaso es éste tu primer encuentro con la traicionera "
	"Arad Gorthor, has de saber que nada aquí es lo que parece. Si este inquieto y "
	"agitado orco está al mando de la defensa de las Puertas de la Guerra... por algo "
	"será. Porta dos armas: una estrella del alba personalizada y una espada corta "
	"común. La primera tiene un aspecto grande y desproporcionado, haciendo que Korg "
	"tenga que andar ligeramente inclinado a la derecha.\n");
	set_main_plural("Korgs, los Cancerberos");
	add_plural(({"korgs", "orcos", "cancerberos"}));
	
	fijar_altura(173);
	fijar_peso_corporal(72500);
	
	fijar_raza("orco");
	fijar_clase("soldado");
	fijar_religion("gurthang");
	
	fijar_fue(18);
	fijar_extrema(100);
	fijar_con(18);
	fijar_des(18);
	fijar_int(14);
	fijar_sab(12);
	fijar_car(16);
	
	fijar_nivel(60);
	fijar_alineamiento(4500);
	
	add_clone(ARMAS + "estrella_norte", 1);
	add_clone(BARMAS + "espada_corta", 1);
	add_clone(BARMADURAS + "yelmo", 1);
	add_clone(BARMADURAS + "amuleto", 1);
	add_clone(BARMADURAS + "anillo", 1);
	add_clone(BARMADURAS + "cinturon", 1);
	add_clone(BARMADURAS + "talabarte", 1);
	add_clone(BARMADURAS + "bandas", 1);
	add_clone(BARMADURAS + "brazalete", 1);
	add_clone(BARMADURAS + "brazalete_izq", 1);
	add_clone(BARMADURAS + "guantelete", 1);
	add_clone(BARMADURAS + "guantelete_izq", 1);
	add_clone(BARMADURAS + "botas_campaña", 1);
	add_clone(BARMADURAS + "grebas", 1);
	init_equip();
	
	ajustar_dinero(5 + random(6), "platino");
	
	ROL_NPCS->definir(this_object());
	
	add_property("no_desarmar", 1);
	add_property("sin miedo", 1);
	add_property("no_desfondable", 1);
	add_property("abrigado", 1);
	add_property("libre-accion", 1);
	add_property("no_lanzar", 1);

	load_chat(15, ({
		":otea el horizonte polvoriento y apocalíptico.",
		":hace un gesto a los tiradores de las atalayas. Parece haber divisado algo.",
		"'Aquí recibimos las incursiones con los brazos y las puertas bien abiertos, ¡pues de aquí nunca se sale con vida!",
		"'Observa las Puertas de la Guerra, ¡son el preámbulo de la gran Arad Gorthor!, ¡ciudad de Lord Gurthang!",
		"'Recuerdo cuando las máquinas de guerra cruzaban estas puertas en dirección a Dendra, ¡qué grandes eran las guerras de antaño!",
		"'Ten cuidado con lo que haces, sólo tengo que chascar los dedos para que los tiradores te atraviesen la cabeza de oreja a oreja",
		":se rebaña el sudor de la frente, ya convertido en barro por el ambiente desértico.",
		"'¿A dónde vas? ¡cabezas de halfling traigo!",
	}));
	
	load_a_chat(50, ({
		"'¡¡¡Poned en marcha la maquinaria de guerra!!!",
		"'¡¡¡Todas las tropas, a las Puertas de la Guerra!!!",
	}));
		
	fijar_pvs_max(18000);
	fijar_pe_max(2000);
	
	fijar_maestria("luz del alba", 100);
	fijar_maestria("estrella corta", 100);

	ajustar_bo(150);
	adjust_tmp_damage_bon(60);
}

string dame_noconversar_personalizado()
{
	return "Korg no levanta la mirada del horizonte. Parece tomarse su trabajo muy en serio.\n";
}

void event_enter(object quien, string mensaje, object procedencia, object *seguidores)
{
	string *jugadores_odiados = query_hated()["jugador"];
	// Qué bien que funcionan los set_aggressive
	if (-1 != member_array(quien->query_name(), jugadores_odiados)) {
		quien->attack_by(this_object());
		attack_ob(quien);
	}	
	// Primera ráfaga de ataques contra los que abandonen la habitación antes de tiempo
	::event_enter(quien, mensaje, procedencia, seguidores);
	if (-1 != member_array(quien, query_call_outed() + query_attacker_list())) {
		habilidad("ataquedoble", quien);
	}
}

void bateria_basica(object quien)
{
	if (puedo_ejecutar("golpecertero_reciente")) {
		habilidad("golpecertero", quien);
	} else if (puedo_ejecutar("bloqueo-Aplastar")) {
		habilidad("aplastar", quien);
	} else if (random(2) && puedo_ejecutar("bloqueo-Herir")) {
		habilidad("herir", quien);
	} else if (puedo_ejecutar("bloqueo-luchador")) {
		habilidad("ataquedoble", quien);
	}
}

void heart_beat()
{
	if (tengo_enemigos()) {
		bateria_basica(victima_limpia());
	}
	
	if (dame_pvs() < dame_pvs_max() / 3 && base_name(environment()) == BASTION + "puerta_oeste" && !environment()->defensa_activada()) {
		tell_accion(environment(), "%^YELLOW%^"+query_short()+", da la voz de alarma y los engranajes de defensa de las Puertas de la Guerra entran en acción.\n", query_short()+", da la voz de alarma y los engranajes de defensa de las Puertas de la Guerra entran en acción.%^RESET%^\n");
		environment()->activar_defensa();
	}

	::heart_beat();
}
