//Rezzah 16-01-2011

inherit "/obj/conversar_npc";
#include "../path.h";

string desc = "La figura figura perruna que tienes delante irradia un poder descomunal, en cada uno de sus gestos, "
        "de sus pausas al hablar, en cada sílaba de su hocico perruno hay una portentosa fuerza, cómo nacida en el mismo corazón de "
        "Eirea que te avasalla y hunde en la más ignominiosa reverencia. Sientes la necesidad de rendirte a su sabiduría, "
        "a su voluntad, a su portentosa esencia. Serías capaz de entregar tu vida sin pensarlo si él te lo pidiera.\n";
void debug(mixed str) {
	tell_object(find_player("rezzah"), "\n%^CYAN%^BOLD%^"+str+"%^RESET%^\n");
}

int extraer_peso(string linea) {
	int peso;
	mixed salida, s1, s2;

	salida = reg_assoc(linea, ({"[0-9]+"}), ({1}), "no coincide");
	s1 = salida[0];
	s2 = salida[1];
	for (int i = 0; i < sizeof(s1);i++) {
		if (s2[i] != "no coincide") {
			peso = to_int(s1[i]);
			break;
		}
	}
	return peso;
}

int calcular_peso() {
	int stop,peso_total,posicion,peso;
	string linea;
	peso = 0;
	posicion = 1;
	peso_total = 0;
	stop=1;
	
	while (linea = read_file(LOGS+"cabezas.txt", posicion, 1)) {
		if (!linea || linea == "" || linea == 0) {
			break;
		}
		posicion++;
		if (!regexp(linea, "^[0-9]+#.*$")) {
			debug("La línea "+linea+" no está bien construida.");
		} else {
			peso = extraer_peso(linea);
			peso_total = peso + peso_total;
		}
	}	
	return peso_total;
}

void setup_conversacion() {
	nuevo_dialogo("nombre", "Soy Gargax, Profeta de Gurthang, su enviado para dar palabra a su voluntad.");
	nuevo_dialogo("profeta","Soy la voz de Gurthang en Eirea, su yunque en la forja, su lanza en la caza, ¡su profeta!","nombre");
	nuevo_dialogo("gurthang",({"'¡Imbécil! ¿Cómo te atreves a decir SU nombre sin la reverencia adecuada?",
								":te propina tal bofetada que te tiemblan las piernas.",
								"'Cuando llegue el cambio los impuros como tú caerán bajo su poder."}), "profeta");
	nuevo_dialogo("cambio", ({"'Así es, soy su enviado para transmitiros su llamada ¡GURTHANG OS CONVOCA!",
								"'¡Arrodillaos fieles! ¡Y clamar la gloria de Gurthang pues quienes oigan su llamada serán salvados!",
								"'La hora final por fin se acerca y ÉL se alzará para unir a sus discípulos y derrotar al infecto Eralie."
								"'¡Sí! Es la hora de los goblins, de los gnoll, de kobolds y orcos... ¡ES NUESTRA HORA!"}), "gurthang");
	nuevo_dialogo("llamada", ({"'¡Es alta y clara! ¿Acaso no la oyes en tu cabeza hueca?", "ver_religion"}), "cambio");
	nuevo_dialogo("aceptarla", ({"'¡Haces bien en aceptar su llamada!¡Sino morirías sin más a su llegada!",
									"'Así que escúchame bien, Gurthang clama por la unión de sus predilectos contra la escoria Eralie, por ello debemos poner nuestro máximo esfuerzo en fortalecerle para el enfrentamiento, Gurthang nos necesita ahora, y nos premiará generosamente cuando llegue el momento.",
									"'Y ese día, está próximo, muy próximo... pero tú quieres saber cómo, ¿verdad?",
									":sonríe y una fína hilera de perfectamente blancos colmillos aparece en su boca con siniestra apariencia.",
									"'Sé que te gustará, para fortalecerlo ¡Gurthang pide las cabezas de sus enemigos!",
									"'Así es, %^BOLD%^RED%^entregármelas%^RESET%^ y yo las acumularé en mí, soy el canal para ofrendarle el poder."}), 
									"llamada");
	nuevo_dialogo("ofrendar", ({"'Entregarme las cabezas, las devoraré y absorberé su poder.",
								"'Contra más poderosos fueran las criaturas que llevaran la cabeza sobre los hombros mejor, por supuesto.",
								"'Cuando esté listo al fin tendréis que matarme, es mi gustoso sacrificio, pero sin lucha no habrá ofrenda.",
								"'Gustosamente mataré a muchos de vosotros para mayor gloria de Gurthang y con mi muerte llegará su momento.",
								":sonríe maliciosamente.",
								"'Pero habreis de esperar hasta el momento adecuado, y sabreis sin duda alguna cuándo habrá llegado.",
								":te mira con dureza.",
								"'Recuerda que hay que esperar el momento adecuado."}), "aceptarla");
	
	prohibir("aceptarla", "es_gurthang");
	habilitar_conversacion();
}
void setup()
{
	if (read_file(LOGS+"gargax_ha_muerto.txt",1,1)) {//Ya no clona más (por si lo engordan lo suficiente cuando yo no esté)
		destruct(this_object());
	}
	set_name("gargax");
	fijar_raza("kobold");
	fijar_religion("gurthang");
	fijar_bando("malo");
	fijar_genero(1);
	nuevo_lenguaje("negra",100);
	fijar_lenguaje_actual("negra");
  	set_short("Profeta Gargax");
	add_alias("profeta");
    add_plural("profetas");
    set_main_plural("Profetas Gargax");
	fijar_clase("chaman");
	fijar_ciudadania("vagabundo");
    fijar_alineamiento(100000);
	fijar_carac("fue", 20);
	fijar_carac("int", 30);
	fijar_carac("sab", 50);
	fijar_carac("con", 30);
	fijar_carac("car", 50);
	fijar_carac("des", 20);
	fijar_peso_corporal(50000+calcular_peso());
	fijar_altura(130);
	fijar_nivel(100);
	fijar_fe(220);
	fijar_pvs_max(40000);
	fijar_pe_max(2000);
	ajustar_bo(200);
	ajustar_be(200);
	ajustar_bp(200);
	add_clone(ARMAS+"baculo_gargax.c",1);
	add_clone(ARMADURAS+"tunica_gargax.c",1);
	init_equip();

	add_attack_spell(60, "curar heridas serias", 1); //self
	add_attack_spell(60, "tormenta ignea", 4); //many enemies
	add_attack_spell(60, "causar heridas mayores", 3);//one enemy
	add_attack_spell(60, "enmudecer", 3);
	add_attack_spell(60, "curar heridas ligeras", 2); //many loved
	add_attack_spell(60, "tormenta arcana", 3);
	add_attack_spell(60, "gran plegaria sagrada", 2);
	add_attack_spell(60, "relampago", 3);
	add_attack_spell(60, "carne a piedra", 3);
	add_attack_spell(60, "gritos ancestrales",4);
	add_attack_spell(60, "favor de los antiguos",2);
	
//	add_loved("religion","gurthang");
	set_aggressive(4);
	setup_conversacion();

	add_property("libre-movimiento",1);
	add_property("sin miedo",1);
	add_property("no_desarmar",1);

	load_chat(30, ({
		"'¡Gurthang se alzará contra el mundo entero y nos guiará a la gloria!",
		"'Escuchadme fieles de Gurthang, ¡Escuchad mi mensaje!",
		"'¡Conseguir reunir todo el poder para Gurthang! ¡Él nos recompensará!",
		":se mueve con dificultad."
	}));
	if (dame_peso_corporal() > 250000) {
		load_a_chat(30, ({
			"¡GLORIA A GURTHANG! ¡Matar y morir, esa es su gloria!",
			"¡Vamos alfeñiques! ¡Matadme!",
			":no es más que un grotesca bola parlante de grasa con muy mala leche."
		}));
	} else {
		load_chat(30, ({
			"'¡¡Desgraciados!! Acabaré con todos vosotros... ¡AÚN NO ES EL MOMENTO!",
			"'¡Pagaréis con la muerte vuestra estupidez!",
		}));
	}
	
	call_out("descripcion_peso", 1);
}

void init() {
	::init();
	add_action("entregar_cabeza","entregar");
}
int llega_la_ayuda() {
	tell_room(environment(this_object()), "%^RED%^BOLD%^Cuatro llamas de fuego aparecen al instante rodeando a "+query_short()+".%^RESET%^\n");
	call_out("llega_la_ayuda2", 2);
}
int llega_la_ayuda2() {
	tell_room(environment(this_object()), "%^RED%^BOLD%^Las llamas cobran forma humanoide y dos de ellas cogen al dolorido profeta y desaparecen con él.%^RESET%^\n");
	call_out("llega_la_ayuda3", 3);
}
int llega_la_ayuda3() {
	string ob1,ob2;
	tell_room(environment(this_object()), "%^RED%^BOLD%^Las dos llamas restantes terminan su transformación convirtiéndose en dos demonios de fuego sedientos de sangre.%^RESET%^\n");
	ob1 = clone_object(NPCS+"demonio_gargax");
	ob2 = clone_object(NPCS+"demonio_gargax");
	ob1->move(environment(this_object()));
	ob2->move(environment(this_object()));
	foreach (object ob in dame_lista_atacantes()) {
		ob1->attack_ob(ob);
		ob2->atrack_ob(ob);
	}
	destruct(this_object());
}
void heart_beat() {
	if (dame_pvs() * 4 < dame_pvs_max() && !query_property("ayuda_invocada") && dame_peso_corporal() < 250000) {//Invoca ayuda porque aún no está para morir
		add_property("ayuda_invocada", 1);
		tell_room(environment(this_object()), "%^RED%^BOLD%^"+query_short()+" lanza un agónico grito atravesando aire, materia y los propios planos.%^RESET%^\n");
		call_out("llega_la_ayuda", 2);
	}
	::heart_beat();
}

int descripcion_peso() {//Añadimos a la descripcion del npc una idea sobre cómo ha engordado
	int peso;
	peso = dame_peso_corporal();
	if (peso < 60000) {
		set_long(desc+"Presenta un aspecto deplorable, de extrema delgadez.\n");
	} else if (peso < 80000) {
		set_long(desc+"Está muy delgado, debería alimentarse mejor.\n");
	} else if (peso < 130000) {
		set_long(desc+"Presenta un aspecto saludable, bien alimentado.\n");
	} else if (peso < 150000) {
		set_long(desc+"Está gordo para ser un kobold, su aspecto es desagradable.\n");
	} else if (peso < 180000) {
		set_long(desc+"Está tan gordo que apenas se mueve, su extremidades parecen morcillas.\n");
	} else if (peso < 220000) {
		set_long(desc+"Es una enorme mole de grasa, tiene tanta que apenas puedes distinguir su cuerpo, es casi una bola.\n");
	} else if (peso > 250000) {
		set_long(desc+"%^RED%^BOLD%^Está tan gordo que podría estallar, literalmente. ES EL MOMENTO.%^RESET%^\n");
	}
	return peso;
}
int es_gurthang(object pj) {
	if (pj->dame_religion() == "gurthang") return 1;
	return 0;
}
void ver_religion(object pj) {
	if (pj->dame_religion() != "gurthang") {
		abandonar_conversacion(pj, "¡Maldit"+pj->dame_vocal()+" bastard"+pj->dame_vocal()+"! No me molestes más con tus tonterías y lárgo de aquí!");
	} else {
		tell_object(pj, "Notas como si una gigantesca ola chocara contra tu mente dejándote completamente desnud"+pj->dame_vocal()+".\n");
		do_say("Veo a Gurthang en tí, sé que has oído la llamada, tienes que %^BOLD%^aceptarla%^RESET%^ en tí.",0);
	}
}

void sumar_cabeza(object pj, object ob) {
	int peso,fe;
	string nombre,victima;

	nombre = pj->query_name();
	victima = ob->dame_duenyo();
	fe = ob->dame_fe();
	if (fe > 100) fe = 100;
	peso = fe + ob->dame_nivel();
	peso = peso * 10;//Maximo 2000
	tell_object(pj, "Entregas ceremoniosamente la "+ob->query_short()+" a "+query_short()+" quien la devora mascullando rezos entre dentelladas.\n");
    tell_accion(ENV(TO), pj->query_short()+" da algo a "+query_short()+" quien lo devora mascullando rezos entre bocado y bocado.\n",
                        "Oyes ruidos de huesos crujiendo y triturándose entre los dientes de alguien.\n", ({pj}),0);
   	destruct(ob);
	pj->ajustar_xp(peso+random(1000));//para que no deduzcan valores :S
	write_file(LOGS+"cabezas.txt", peso+"#"+nombre+"#"+victima+"\n");
	fijar_peso_corporal(50000+calcular_peso());
	descripcion_peso();
}

int entregar_cabeza(string txt) {
	object pj;
	pj = this_player();
	if (!txt || txt == "") {
		return notify_fail("¿Entregar qué?\n");
	}
	if (txt == "cabezas") {
		return notify_fail("Mejor de una en una, ¿no querrás que se atragante verdad?\n");
	}
	if (txt != "cabeza") {
		return notify_fail("A Gargax sólo le gusta devorar cabezas decapitadas.\n");
	}
	foreach(object ob in all_inventory(pj)) {
		if (ob->dame_parte_cuerpo()) { //Es una parte de un cuerpo
			if (ob->dame_parte() != "cabeza") {
				return notify_fail("Gargax sólo quiere cabezas ¡¡Cabezas decapitadas!! ¿Tan difícil es de entender?\n");
			}
			if (ob->dame_cortador() != pj->query_name()) continue;//Solo cabezas que hayan cortado ellos
			if (ob->dame_nivel() < 15) { //La cabeza de una criatura insignificante
				tell_object(pj, query_short()+" te dice: ¿Qué demonios pretendes? ¿Para qué quiero yo la cabeza de una criatura tan insignificante?\n");
				return 1;
			}
			if (ob->dame_nivel() < 30) {//Cabezas decentillas pero ...
				if (ob->dame_fe() <= 0) {//Cabezas decentillas sin fe
					tell_object(pj, query_short()+" te dice: ¿Esto es todo lo que puedes conseguir? Trae, trae... pero me estás decepcionando.\n");
				} else {
					if (ob->dame_fe() > 100) { //Con fe molan, pero no contaremos má de 100 nunca
						tell_object(pj, query_short()+" te dice: ¡Excelente captura "+pj->query_short()+"! Sigue así para agradar a Gurthang.\n");
					} else {
                        tell_object(pj, query_short()+" te dice: ¡Buen trabajo "+pj->query_short()+"! Pero aún puedes mejorarlo.\n");
					}
				} 
			} else { //Mas de nivel 30
				if (ob->dame_fe() < 100) {
					tell_object(pj, "Los ojos de "+query_short()+" brillan de emoción cuando ve lo que le traes.\n"+query_short()+" te dice: ¡Así sí!¡Ésta es la forma de fortalecer a Gurthang!\n");
				} else {
					tell_object(pj, "La cara descompuesta por el puro placer de "+query_short()+" te revela que ésta vez has dado en el clavo.\n"+
					query_short()+" te dice: Tú, entre todos los fieles serás visto con los mejores ojos por Gurthang.\n"+query_short()+
					" te hace una solemne reverencia.\n");
					tell_accion(ENV(TO), query_short()+" hace una solemne reverencia a "+pj->query_short()+"\n","",({pj}),0);
				}
			}
			call_out("sumar_cabeza", 2, pj, ob);
			break;
		}
	}
	return 1;
}


int do_death(object ob)
{
	if (dame_peso_corporal() < 250000) { //aún nodebe morir
		tell_object(ob, "¡Maldición! Jurarías que lo habías matado pero... ¡No ha muerto!\n");
		return 1;//No se muere
	} else {
		foreach (object pj in users() ) {
			if (pj->query_player() && pj->query_name() == "rezzah") {
				tell_object(pj, "\n%^BOLD%^RED%^¡EL PROFETA GARGAX HA MUERTO! ¡GURTHANG RECIBIRÁ SU PODER PARA TERROR DE EIREA!\n");
			}
			write_file(LOGS+"gargax_ha_muerto.txt","ya no clona más");//este fichero se comprueba al inicio de setup() para que no clone más hasta que lo modifique y sea un esbozo de sí mismo xD
		}	
		return ::do_death(ob);
	}
}
int resistencia_magica(int nivel_lanzador,string caracteristica,string escuela,object hechizo) {
	string nombre;
	
	nombre = hechizo->dame_nombre_hechizo();
	if (nombre == "Enmudecer") return 1;
	if (nombre == "Retener persona") return 1;
	if (nombre == "Miedo") return 1;
	return 0;
}
