//Rezzah 09-02-2011
// Modificado por Dherion 20/07/2017

#include "../path.h";
inherit "/obj/conversar_npc.c";
void setup_conversacion() {
    nuevo_dialogo("nombre","'Soy un soldado dedicado a protejer este camino de los ataques de Eldor.");
    nuevo_dialogo("camino",({
        ":mira a su alrededor buscando algún peligro. Al no encontrarlo comienza a hablar.",
        "'sí, protejo el camino.",
        "'Mi misión es detener los ataques provenientes de Eldor antes de que lleguen más lejos.",
        "'Formo parte de los protectores del camino.",
        "'suele ser aburridísimo, aquí casi nunca pasa nada",
        "'Aunque ahora que lo pienso... Tenemos un problema con un arquero al que no conseguimos dar caza",
        "'Podrías ayudar, en lugar de estar preguntando cosas obvias"
      }),"nombre");
    nuevo_dialogo("ayudar",({
        "'Lleva unos días molestando con sus flechas",
        "'Ataca desde lejos y luego busca otro lugar para esconderse",
        "'Tarde o temprano morirá de sed o de hambre, pero mientras que se muere y no nos seguirá atacando",
        "'La cuestión es que sabe donde esconderse y como disparar si nos acercamos mucho",
        "'La última vez que lo vimos se dirigía hacia la playa que hay cerca de aquí. Intentamos darle caza, pero...",
        "'Ve rápidamente, con suerte nos haces el favor de quitarlo del medio",
        "'o de quitarte del medio...",
        "hito arad_arquero_escurridizo pista_inicial"
      }),"camino");
    nuevo_dialogo("muerto",({
        "'Así que le has conseguido dar caza...",
        "'Perfecto. Y no pareces tener muchas heridas.",
        ":sonríe con malicia",
        "'Toma esto, ¡y no molestes más!",
        "hito arad_arquero_escurridizo recompensa_final"
      }),"ayudar");
    prohibir("muerto","hito arad_arquero_escurridizo arquero_muerto");
    habilitar_conversacion();
}
void setup()
{
    setup_conversacion();
    fijar_lenguajes((["negra":100,"ogro":100]));
    fijar_lenguaje_actual("negra");
    add_zone("camino_eldor");
    set_move_after(50,10);
    set_name("protector");
    set_short("Protector de la Frontera");
    set_long("Los protectores de la frontera son unos rudos guardias. su misión es la de vigilar este camino para evitar incursiones por parte de Eldor.\n");
    set_main_plural("Protectores de la Frontera");
    generar_alias_y_plurales();
    fijar_altura(190);
    fijar_peso_corporal(130000);
    fijar_raza("orco");
    fijar_subraza("uruk-hai");
    fijar_bando("malo");
    fijar_clase("soldado");
    fijar_religion("gurthang");
    fijar_fue(20);
    fijar_extrema(100);
    fijar_con(18);
    fijar_des(15);
    fijar_int(15);
    fijar_sab(5);
    fijar_car(10);
    fijar_nivel(15);
    fijar_alineamiento(4000);
    add_loved("bando","malo");
    set_aggressive(1);
    fijar_estatus("Golthur Orod", 300);
    ajustar_dinero(roll(2, 3), "platino");
    fijar_maestria("cimitarra", 81);
    add_attack_spell(40, "herir", 3);
    add_attack_spell(40, "atizar",3);
    add_clone(BARMADURAS + "escudo", 1);
    add_clone(BARMADURAS + "botas", 1);
    add_clone(BARMADURAS + "yelmo", 1);
    add_clone(BARMADURAS + "mallas", 1);
    add_clone(BARMAS + "cimitarra", 1);
    init_equip();
}
