// Dunkelheit 19-01-2010

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	set_name("cazador");
	set_short("Cazador de la Horda");
	set_main_plural("Cazadores de la Horda");
	add_plural(({"cazadores", "hordas"}));
	fijar_genero(1);

	fijar_altura(140 + random(30));
	fijar_peso_corporal(60000 + random(10000));

	fijar_raza("goblin");
	fijar_clase("cazador");
	fijar_religion("gurthang");

	set_long(".\n");
	
	fijar_fue(18);
	fijar_con(18);
	fijar_des(18);
	fijar_int(15);
	fijar_sab(8);
	fijar_car(8);
	
	fijar_nivel(30 + random(6));
	fijar_alineamiento(5000 + random(2000));
	
	if (random(2)) {
		add_clone(BARMAS + "cimitarra", 2);
		ajustar_maestria("cimitarra", 60);
	} else {
		add_clone(BARMAS + "sable", 2);
		ajustar_maestria("sable", 60);
	}
	
	add_clone(BARMADURAS + "cuero", 1);
	add_clone(BARMADURAS + "capa", 1);

	if (!random(4)) {
		add_clone(BARMADURAS + "cinturon", 1);
	}

	if (!random(4)) {
		add_clone(BARMADURAS + "botas", 1);
	}
	
	init_equip();
	
	ajustar_dinero(roll(1, 8), "estaño");
	
	ajustar_bo(80);
	
	ROL_NPCS->definir(this_object());
	
	load_chat(30, ({
	}));
	
	load_a_chat(50, ({
	}));
	
	add_attack_spell(60, "torbellino", 3);
}

string dame_noconversar_personalizado()
{
	return "El soldado está concentrado en su tarea y no te presta la más mínima atención.\n";
}
