// Dunkelheit 19-01-2010

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	status hacha;
	string raza = element_of(({"orco", "gnoll"}));
	
	set_name("barbaro");
	set_short("Bárbaro de la Horda");
	set_main_plural("Bárbaros de la Horda");
	add_plural(({"barbaros", "bárbaros", "hordas"}));
	fijar_genero(1);

	fijar_altura(180 + random(21));
	fijar_peso_corporal(120000 + random(10000));

	fijar_raza(raza);
	fijar_clase("barbaro");
	fijar_religion("gurthang");

	set_long("Este gigantesco y fornido bárbaro "+raza+" camina pisando con "
	"fuerza, levantando una considerable polvareda por el arrastrar de sus "
	"cortas pero poderosas piernas. Apenas lleva ropa que le identifique como "
	"miembro de la Horda Negra, pues sin lugar a duda prefiere mostrar sus "
	"enormes y marcados músculos, con cuya mera muestra ya tiene ganada la "
	"parte más importante de la batalla: el factor psicológico. Esta criatura "
	"de aspecto necio no parece tener ningún miedo a la muerte, ni tampoco "
	"parece darle valor alguno a la vida, a juzgar por cómo trata a sus "
	"semejantes y, por qué no, su mirada perdida de asesino sin escrúpulos.\n");
	
	fijar_fue(18);
	fijar_extrema(100);
	fijar_con(18);
	fijar_des(18);
	fijar_int(4);
	fijar_sab(4);
	fijar_car(6);
	
	fijar_nivel(30 + random(6));
	fijar_alineamiento(5000 + random(2000));
	
	if (random(2)) {
		add_clone(BARMAS + "hacha_a_dos_manos", 1);
		ajustar_maestria("hacha a dos manos", 40);
		hacha = 1;
	} else {
		add_clone(BARMAS + "maza_a_dos_manos", 1);
		ajustar_maestria("maza a dos manos", 40);
	}
	
	add_clone(BARMADURAS + "piel", 1);
	add_clone(BARMADURAS + "botas", 1);
	
	if (!random(4)) {
		add_clone(BARMADURAS + "guante", 1);
		add_clone(BARMADURAS + "guante_izq", 1);
	}
	
	init_equip();
	
	ajustar_dinero(roll(1, 8), "estaño");
	
	ajustar_bo(80);
	
	ROL_NPCS->definir(this_object());
	
	load_chat(15, ({
		":no musita palabra alguna, pero su rechinar de dientes se oye a varias leguas de distancia.",
		":pone cara de muy pocos amigos. No parece una criatura muy inteligente.",
		":emite un ligero murmullo gutural, semejante al de un perro rabioso.",
		":anda de un lado a otro olisqueando como si estuviera rastreando a un presa.",
		"'He escuchado las leyendas de los antiguos bárbaros cazadores de osos grizzly de Golthur Orod, ¿qué habrá sido de esa especie?",
		"'¡¡¡Llevas armas dignas de elfo amanerado!!! ¡Observa la majestuosidad de mi "+(hacha ? "hacha":"maza")+" a dos manos!",
		":alza su "+(hacha ? "hacha":"maza")+" y sale corriendo detrás de una rata negra hasta que le da alcance y la aplasta una y otra vez.",
		"'Quien piense que los bárbaros sólo habitamos en tundras heladas... ¡es que es un idiota supino!",
	}));
	
	load_a_chat(50, ({
		"'¡¡¡WAAAAAAAAAAARRRGGGH!!!",
		":echa saliva espumosa por la boca, encolerizado.",
		":ha perdido todo control sobre sí mismo, ¡está poseído por un ira divina!",
	}));
	
	add_attack_spell(33, "cabezazo", 3);
}

string dame_noconversar_personalizado()
{
	return "Los bárbaros no se caracterizan por ser muy habladores. Prefieren liarse a guantazos.\n";
}
