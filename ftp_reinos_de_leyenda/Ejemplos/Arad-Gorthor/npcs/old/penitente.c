// Dunkelheit 24-11-2009

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	set_name("penitente");
	set_short("Penitente");
	set_long("Este goblin cree que la única vía para calmar la ira de Lord Gurthang es autoflagelarse "
	"hasta caer desangrado. Y parece que lo está haciendo muy bien. Su cuerpo maltrecho y lleno de sangre "
	"apenas conserva fuerzas ya, y está tan delgado que puedes hasta notar los latidos acelerados de su "
	"corazón.\n");
	set_main_plural("Penitentes");
	add_plural("penitentes");
	
	fijar_altura(130+random(20));
	fijar_peso_corporal(30000+random(20000));
	
	fijar_raza("goblin");
	fijar_clase("aventurero");
	fijar_religion("gurthang");
	fijar_ciudadania("mor_groddur");
	
	fijar_fue(6);
	fijar_con(14);
	fijar_des(9);
	fijar_int(4);
	fijar_sab(4);
	fijar_car(3);
	
	fijar_nivel(10 + random(6));
	fijar_alineamiento(900);
	
	ajustar_dinero(roll(1, 2), "cobre");
	
	ROL_NPCS->definir(this_object());
	
	load_chat(30, ({
		":se fustiga a sí mismo con el látigo.",
		":se arrastra por los escalones del templo medio desangrado, parece estar a punto de desfallecer.",
		"'¡Perdonad mis errores, oh mi señor Lord Gurthang!",
		":gime con lástima mientras continua azotándose con el látigo.",
		":parece sobrellevar muy bien el dolor, sin duda está bajo los efectos de alguna droga.",
	}));
	
	load_a_chat(50, ({
		"'Pon fin a mi castigo, ¡oh Lord Gurthang!",
		"'¡Acepto tus designios, mi Señor!",
	}));
	
	fijar_pvs(dame_pvs_max() / 10, this_object());
	
	add_clone(BARMAS + "latigo", 1);
	init_equip();
}

string dame_noconversar_personalizado()
{
		return "Pasas la mano ante el rostro del penitente... y ni se entera. Debe estar en algún estado de trance provocado.\n";
}
