// Zoilder 20/01/2011
// Clase NPC basica para prostitutos
#include "../path.h";
inherit "/obj/monster.c";

/*
 *	Función que devuelve una descripción aleatoria para el NPC.
 */
string dame_descripcion()
{
    string base_raza = capitalize(dame_numeral()) + " " + dame_raza() +
                       " prostitut" + dame_vocal() + ".";
    string *descripciones =
        ({base_raza +
              " De cuerpo más bien generoso, estás ante un ser de las calles "
              "que viene aquí buscando presas que llevarse a la cama. "
              "Tiene una mirada penetrante que parece decirte que si te "
              "acercas no te arrepentirás. Aunque no parece que los "
              "transeúntes "
              "piensen eso cuando pasan a su lado, pues notas como todos se "
              "alejan con cara de asco.\n",
          base_raza +
              " Con las ropas bien ajustadas y enseñando el máximo posible de "
              "carne, notas como todos los transeúntes tienen "
              "puesta sus miradas sobre dicha figura, que parece atraerles uno "
              "a uno, hasta el punto de tener a varios a su alrededor. "
              "De pelo largo recogido en una cola de caballo, con unos ojos "
              "azules que desprenden un brillo particular. "
              "Observas como se toca demasiado su pecho izquierdo. Parece que "
              "es su rito particular para atraer gente.\n",
          base_raza +
              " De cuerpo algo más menudo de lo normal en su raza, observas "
              "una figura llena de cicatrices a lo largo del cuerpo. "
              "Sin embargo, en lugar de taparse dichas zonas, las muestra con "
              "orgullo, y ves como aún así varios transeúntes, con "
              " mirada algo obscena, se acercan con ganas de pasar un rato "
              "jugueteando. Bajo su camiseta, ves como lleva una daga "
              "escondida.\n"});
    return element_of(descripciones);
}

/*
 *	Configuración del NPC
 */
void setup()
{
    string my_raza = element_of(({"orco", "goblin", "gnoll", "kobold"}));
    // Se establece en primer lugar el genero del NPC, pues a partir de eso se
    // sacan las descripciones y nombres
    fijar_genero(random(2) + 1);

    set_name("prostitut" + dame_vocal());
    add_alias(({"prostitut" + dame_vocal(), my_raza}));
    add_plural(({"prostitut" + dame_vocal() + "s", my_raza + "s"}));
    set_short("Prostitut" + dame_vocal() + " " + my_raza);
    set_main_plural("Prostitut" + dame_vocal() + "s " + my_raza + "s");
    fijar_raza(my_raza);
    // Se obtiene una descripción aleatoria
    set_long(dame_descripcion());
    fijar_religion("gurthang");
    fijar_clase("aventurero");

    nuevo_lenguaje("negra", 100);

    set_random_stats(10, 18);
    fijar_nivel(3 + random(16));
    fijar_altura(150 + random(50));
    fijar_peso_corporal(40000 + random(50) * 1000);

    load_chat(
        30,
        ({1,
          ":canta: Me lo como todo, me lo como todo.",
          1,
          ":enseña sus muslos desnudos a los transeúntes.",
          1,
          "'Macizos y macizas venid a mí, que vamos a disfrutar.",
          1,
          "#mirada"}));

    add_clone(BARMAS + "daga.c", 1);
    add_clone(BARMADURAS + "collar.c", 1);
    add_clone(BARMADURAS + "pendiente.c", 1);
    add_clone(BARMADURAS + "pulsera.c", 1);
    add_clone(BARMADURAS + "camiseta.c", 1);
    add_clone(BARMADURAS + "pantalones.c", 1);
    add_clone(BARMADURAS + "sandalias.c", 1);
    init_equip();
}
