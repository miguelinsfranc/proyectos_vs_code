// Dherion Mayo 2017: NPC orco que asigna la misión para malos
#include "../path.h";
inherit "/obj/conversar_npc.c";
int comprobar_bando(object pj) {
    if(pj->dame_bando()=="malo")
        return 1;
    else
        return 0;
}

void setup_conversacion() {
    nuevo_dialogo("nombre","'¡Mi nombre no te importa! ¡Déjame vigilar el
camino en paz!");
    nuevo_dialogo("camino",({
        ":gruñe por tu insistencia de hablar.",
        "'Este camino, como puedes ver lleva mucho tiempo abandonado.",
        "'A esos idiotas de Eldor no les interesa mantenerlo para evitar que
los ataquemos.",
        "'Aunque podemos hacerlo igualmente.",
        "'De hecho, seguro te has cruzado ya con alguna patrulla orca.",
        "'Si no fuera por el problema de los goblins renegados...",
        "'En ocasiones nos viene bien, pero...",
        "'Son una molestia.",
        "'últimamente se ha visto por estos caminos a varios goblins desertores
de la Horda Negra.",
        "'Atacan indiscriminadamente a todo lo que se les cruce por medio para
sobrevivir."
      }),"nombre");
    nuevo_dialogo("problema",({
        "'Se están convirtiendo en una pesadilla",
        "'A la mínima que te descuidas aparece un goblin para asaltarte",
        "'Hay veces que nos ayudan sin quererlo, pero como nos descuidemos...
¡Zas!",
        "'Sospechamos que estos goblins se esconden en el bosque que hay hacia
el este de aquí",
        "'Pero no hemos tenido tiempo de ir a investigar",
        "'Podrías hacer algo útil e ir a investigar en lugar de molestarme, ¿no
crees?",
":te lanza una mirada asesina",
        "hito arad_plaga_goblin pista_inicial"
      }),"camino");
    nuevo_dialogo("ayudarnos",({
        "'Bien, bien.",
        "'Con razón hay menos de esos apestosos renegados rondando por
aquí...",
        "'No durará mucho, pues volverán a reagruparse, pero aún así es un
alivio.",
        "'Toma esto, ¡y no molestes más!",
        "hito arad_plaga_goblin recompensa_final"
      }),"problema");
    prohibir("ayudarnos","hito arad_plaga_goblin lider_muerto");
    prohibir("camino","consultar_bando");
    habilitar_conversacion();
}
void setup()
{
    setup_conversacion();
    fijar_lenguajes((["negra":100,"ogro":100]));
    fijar_lenguaje_actual("negra");
    set_name("soldado orco");
    set_short("Soldado orco");
    set_main_plural("Soldados orcos");
    generar_alias_y_plurales();
    set_long("Este orco hace como que vigila el camino para que nadie lo use
como método para atacar Golthur Orod. Parece estar furioso por algo, a juzgar
por su rostro.\n");
    fijar_religion("gurthang");
    fijar_raza("orco");
    fijar_clase("luchador");
    fijar_alineamiento(10000);
    fijar_genero(1);
    set_random_stats(17,18);
    fijar_nivel(50);
    add_clone(BARMAS + "espada_larga.c",1);
    add_attack_spell(40,"golpecertero",3);
    init_equip();
    load_chat(300,({
"'Matar, matar, matar... ¡Te voy a matar!",
":mira a su alrededor, molesto.",
"'Odio estar aquí...",
"'¡Malditos goblins!",
"'¡Malditos elfos!",
"'Eh, tú, ¿qué miras?",
      }));
}
