//Rezzah 13/02/2011
#include "../path.h";
inherit "/obj/montura";
//Un cachorro huargo, una montura normal pero empieza siendo demasiado joven para ser montado
//no puede andar solo, hay que cogerlo y alimentarlo para que suba niveles
//si se alimenta sólo con cabezas de eralie, hiros o ralder se convertirá en una criatura más poderosa
void debug(mixed str) {
	string aux;
	if (typeof(str) != "string") tell_object(find_player("rezzah"), "\n"+typeof(str)+"\n");
	if (typeof(str) == "object") {
			aux = real_filename(str);
			tell_object(find_player("rezzah"), "\n%^CYAN%^BOLD%^"+aux+"%^RESET%^\n");
	} else {
		if (typeof(str) == "array") {
			tell_object(find_player("rezzah"), "\n%^CYAN%^BOLD%^({"+implode(str, ", ")+"})\n");	
		} else {
			tell_object(find_player("rezzah"), "\n%^CYAN%^BOLD%^"+str+"%^RESET%^\n");
		}
	}
}
void setup()
{
	set_name("huargo");
	add_alias(({"huargo","wargo", "cachorro"}));
	add_plural(({"huargos","wargos", "cachorros"}));
	set_short("Cachorro de huargo");
	set_main_plural("Cachorros de huargos");
	set_long("Un peludo y juguetón cachorro de huargo, muestra los dientes apenas salientes "
		"en ademán fiero intentando morder todo lo que está a su alcance por que sus cortas "
		"y titubeantes patas no le dan para moverse hacia donde quiere ir.\n");
	fijar_genero(random(2)+1);
	fijar_raza("mamifero");
	set_random_stats(13,18);
	//Los stats siempre antes de fijar el tipo de montura, la inteligencia y la fuerza
	//son preestablecidas según el tipo de montura
	fijar_tipo_montura("huargo");
	set_level(1);
	set_wimpy(60);//es joven y asustadizo
	set_get();
}

void init() {
	::init();
	add_action("alimentar_criatura", "alimentar");
	add_action("do_agruparse", "agruparse");
}

int do_agruparse(string tx) {
	if (!tx) return 0;
	if (tx == "invitar huargo" || tx == "invitar cachorro") 
		return notify_fail("Gira la cabeza hacia un lado moviendo la cola sin comprender absolutamente nada.\n");
	return 0;
}

int iniciar_montar(string que)
{
    if(que && !id(que) && !id_plural(que))
        return 0;
 	return notify_fail(query_short()+" es demasiado pequeñ"+dame_vocal()+" aún para ser montado.\n");
}
int ensillar(string montura) 
{
	return notify_fail(query_short()+" es demasiado pequeñ"+dame_vocal()+" y se queda escondid"+dame_vocal()+" bajo la silla.\n");
}
int do_atacar(string obj)
{
	return notify_fail(query_short()+" es demasiado pequeñ"+dame_vocal()+" y gimotea asustad"+dame_vocal()+".\n");
}
int alimentar_criatura(string txt) {
	string * pals;
	object alimento,nuevo;
	object * fm;
	int xp;

	xp = 0;
	if (!txt) {
		return notify_fail("Para alimentar tu cachorro pon: 'alimentar cachorro con <alimento>'.\n");
    }
	pals = explode(txt, " ");
	if (sizeof(pals) != 3) { //La sintaxis es alimentar [huargo|cachorro] con [alimento]
		return notify_fail("Para alimentar tu cachorro pon: 'alimentar cachorro con <alimento>'.\n");
	}
	if (pals[0] != "huargo" && pals[0] != "cachorro") { 
		return notify_fail("Para alimentar tu cachorro pon: 'alimentar cachorro con <alimento>'.\n");
    }
	if (pals[1] != "con") {
		return notify_fail("Para alimentar tu cachorro pon: 'alimentar cachorro con <alimento>'.\n");
    }
  if(query_property("dormido")) return notify_fail("El cachorro de huargo está dormido, así que mejor dejarlo descansar.\n"); //Si está dormido no podrá alimentarse;
	//Todo es correcto, hay que revisar el inventario buscando el item
	fm = find_match(pals[2], this_player(), 1);
	if (!sizeof(fm)) {
		return notify_fail("No encuentras "+pals[2]+" para alimentarle.\n");
	}
	alimento = fm[0];
	//Solo aceptas cabezas y comestibles
	if (alimento->dame_comestible() != 1) { //ni es algo comestible ni es una cabeza
		if (alimento->dame_parte_cuerpo() == 1) {
        	if (alimento->dame_parte() != "cabeza") {
            	return notify_fail("A "+query_short()+" no parece gustarle "+alimento->dame_articulo()+" "+alimento->dame_parte()+" que le ofreces.\n");
        	}
    	} else {
			return notify_fail("No conseguirás alimentarle con eso.\n");
		}
	}
	//Si es salvaje se convierte en su amo, si es su amo acepta la comida, sino la rechaza
	if (dame_amo() == "salvaje") {//Es salvaje y el que lo alimenta se convierte en su amo
		tell_object(this_player(), query_short()+" te mira con ojos suspicaces y poco a poco se acerca a la comida que le ofreces, la olisquea con pasión mirándote con ojos nuevos.\n");
		fijar_amo(this_player()->query_name());
	}//mensaje que solo sale la primera vez que es alimentado 
	if (dame_amo() == this_player()->query_name()) {//Es su amo y acepta la comida
		//Si es una cabeza de eralie hiros o ralder se alimenta de su fe y su peso, sino, solo de su peso
		if (typeof(alimento->dame_peso())) xp = alimento->dame_peso();
		else xp = alimento->dame_peso();
		if (query_timed_property("alimentado") > 10000 && !this_player()->query_creator()) {//hace menos de dos horas le alimentó, le ponemos el límite de 10kilos en un día (2horas)
			return notify_fail(query_short()+" olisquea la comida pero su barriga hinchada demuestra que no necesita más por ahora.\n");
		} else {
			add_timed_property("alimentado", xp + query_timed_property("alimentado"), query_time_remaining("alimentado")?query_time_remaining("alimentado"):7200);
		}
		if (-1 != member_array(alimento->dame_religion(), ({"hiros","ralder","eralie"}))) {
			if (alimento->dame_fe()>0) xp = alimento->dame_fe()+xp;
		}else {
			add_property("mal_alimentado",1);
		}	
		//Subimos la xp, si pasa de nivel 5 se convierte en una criatura distinta
		
		ajustar_xp(xp);
		if (dame_nivel() >= 5) {
			nuevo = clone_object(NPCS+"huargo_joven");
			if (!nuevo) return notify_fail("Se ha producido un error en el crecimiento de tu cachorro. Por favor, notifícalo al consejo de jugadores.\n");
			nuevo->fijar_amo(dame_amo());
			nuevo->fijar_genero(dame_genero());
			if (query_property("mal_alimentado")) nuevo->add_property("mal_alimentado",1);
			if (environment() == this_player()) tell_object(this_player(), "Dejas tu cachorro en el suelo porque su peso ya es insoportable, bien mirado, ya no es tan cachorro aunque sigue siendo demasiado pequeño para ser montado.\n");
			else tell_object(this_player(), "Miras tu cachorro y descubres que se mantiene firme en el suelo, parece haber crecido y parece capaz de andar ya por sí mismo, aunque es demasiado pequeño para ser montado.\n");
			if (nuevo->move(environment(this_player()))) {
				return notify_fail("Hubo un error en la evolucion de tu cachorro. Por favor, notifícalo al consejo de jugadores.\n");
			}
			destruct(this_object());
			return 1; //no debería hacer falta
		}
		tell_object(this_player(), query_short()+" se lanza sobre "+alimento->dame_articulo()+" "+alimento->query_name()+" hambriento y lo devora en un santiamén.\n");
		tell_accion(environment(this_player()), this_player()->query_short()+" ofrece "+alimento->dame_numeral()+" "+alimento->query_name()+" a su "+query_short()+" y lo devora con un ansia cómica.\n", "Oyes el ruido de alguien masticando muy deprisa.\n", ({this_player()}), this_player());
		if (!this_player()->query_creator()) destruct(alimento);
	} else {
		tell_object(this_player(), query_short()+" olisquea "+alimento->dame_articulo()+" "+alimento->dame_parte()+" que le ofreces pero lo rechazan mirándote con ojos desconfiados.\n");
		tell_accion(environment(this_player()), this_player()->query_short()+" tienta a "+query_short()+" con un pedazo de comida, pero lo rechaza desconfiado.\n", "", ({this_player()}), this_player());
	}
	return 1;
}
/*
*	Función que se encarga de dormir al cachorro, haciendo que no sea posible ni alimentarle, ni nada por el estilo
*/
void dormir_cachorro()
{
	add_alias(({"dormido"}));
	add_plural(({"dormidos"}));
	set_short("Cachorro de huargo dormido");
	set_main_plural("Cachorros de huargos dormidos");
	add_property("dormido",1);
	reset_get();
	tell_accion(ENV(TO),"El cachorro de huargo se acomoda y se queda dormido.\n","",({TO}),TO);
}

/*
*	Función que se encarga de despertar al cachorro, devolviéndolo a su estado normal
*/
void despertar_cachorro()
{
	remove_alias(({"dormido"}));
	remove_plural(({"dormidos"}));
	set_short("Cachorro de huargo");
	set_main_plural("Cachorros de huargos");
	remove_property("dormido");
	set_get();
	tell_accion(ENV(TO),"El cachorro de huargo se despereza.\n","",({TO}),TO);
}

/*
*	Función que controla si se intenta entrar en luchas con el cachorro. 
*/
void attack_by(object atacante)
{
	if(dame_amo()=="salvaje" && function_exists("huir",ENV(TO))) //Si está definida la función de huir en la room y el cachorro no tiene amo
	{
		environment()->huir(TO,atacante);
		return;
	}
	::attack_by(atacante);
}