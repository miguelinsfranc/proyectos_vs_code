// Dherion 15/05/2017; Líder goblin

#include "../path.h";
inherit "/obj/monster.c";
void setup()
{
    set_name("líder goblin");
    set_short("Líder goblin");
    set_long("Este sucio y apestoso soldado desertor de la Horda negra te mira desde su improvisado trono con malicia empuñando sus armas. "
      "Se distingue del resto de goblins que viven en esta cueva por su aspecto sano y bien alimentado.\n");
    set_main_plural("líderes goblin");
    generar_alias_y_plurales();
    fijar_altura(80);
    fijar_peso_corporal(50000);
    fijar_raza("goblin");
    fijar_clase("soldado");
    fijar_religion("gurthang");
    fijar_fue(20 + !random(4));
    fijar_con(20);
    fijar_des(18);
    fijar_int(4);
    fijar_sab(4);
    fijar_car(5);
    fijar_nivel(26 + !random(15));
    fijar_alineamiento(8500);
    load_chat(10, ({
        ":Da una orden a uno de los vigilantes en un bramido",
        ":te dedica una mirada de superioridad",
      }));
    load_a_chat(50, ({
        "'¡No debiste haber venido!",
        ":se lanza sobre ti con no muy buenas intenciones.",
        "'¡En este mundo no hay sitio para los dos!",
        "'¡Nunca lograrás matarme!",
      }));
    switch (random(4)) {
    case 0:
        add_clone(BARMAS + "daga", 1);
        fijar_maestria("daga", 50+random(11));
        break;
    case 1:
        add_clone(BARMAS + "puñal", 1);
        fijar_maestria("puñal", 50+random(11));
        break;
    case 2:
        add_clone(BARMAS + "estilete", 1);
        fijar_maestria("estilete", 50+random(11));
        break;
    case 3:
        add_clone(BARMAS + "cuchillo", 1);
        fijar_maestria("cuchillo", 50+random(11));
        break;
    }
    add_clone(BARMADURAS + "zapatillas", 1);
    add_clone(BARMADURAS + "pantalones", 1);
    if (!random(5)) {
        add_clone(BARMADURAS + "cuero", 1);
    }
    init_equip();
    set_aggressive(1);
    add_attack_spell(40,"rodear",3);
    if (!random(10)) {
        ajustar_dinero(1 + random(7), "oro");
    } 
    ajustar_dinero(roll(4, 10), "plata");
}
// Enmascaro la función do_death para la misión
int do_death(object asesino)
{
    if(asesino->dame_mision_abierta("arad_plaga_goblin"))
    {
        asesino->nuevo_hito_mision("arad_plaga_goblin","lider_muerto",1);
        tell_object(asesino,"tu último golpe termina con la vida del líder goblin. Aunque no puedes asegurarte de que no surja otro, al menos sospechas que los asaltos se detendrán temporalmente.\n");
    }
    return ::do_death(asesino);
}
