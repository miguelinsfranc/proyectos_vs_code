#include "../path.h";
inherit "/obj/conversar_npc";

void setup()
{
    set_name("tabernero");
    set_short("Tabernero goblin");
    add_alias("goblin");
    set_main_plural("Taberneros goblin");
    fijar_raza("goblin");
    set_long(
        "Un sucio tabernero goblin, con las manos quemadas por la plancha,"
        " sudoroso y maloliente que escupe con frecuencia sobre "
        "la plancha o la comida, acto que algunos corean con risotadas.\n");
    set_random_stats(14, 18);
    load_chat(
        30,
        ({
            ":deja con tranquilidad que el asqueroso moco que cae de su nariz "
            "se pose sobre la comidad.",
            "'Aquí pasamos cualquier cosa a la plancha... o a cualquiera",
            "'¡Última ronda y os vais todos desgraciados!",
        }));
    load_a_chat(
        50,
        ({"'¡Cuando te aplaste la cara serás el primer plato de la cena!",
          "'¡Hoy serás la sorpresa del menú!"}));
    nuevo_lenguaje("negra", 100);
    fijar_nivel(20 + random(10));
    ajustar_dinero(1 + random(10), "bela");
}
