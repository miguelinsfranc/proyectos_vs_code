// Dunkelheit 23-11-2009

#include "../path.h";
inherit "/obj/conversar_npc.c";

void setup()
{
	set_name("sacamantecas");
	set_short("El Sacamantecas");
	add_alias("orco");
	set_long("Jamás en Eirea ha existido una profesión tan repugnante y malvada como la "
	"de sacamantecas. Este obeso y gigantesco orco, que más que orco parece un ogro-mago, "
	"es el infame encargado de despiezar y posteriormente extraer la grasa de los cuerpos "
	"que llegan hasta su tienda. Está calvo y bajo el interminable mapa de arrugas de "
	"su frente se derraman las gotas de grasa que allí fueron a condensarse. Su mirada "
	"está perdida en un infinito no muy lejano al de su mostrador, en el que opera con "
	"suma delicadeza a golpe de machete. Su color de piel es marrón claro, una condición "
	"muy rara en la raza orca, y que muchos atribuyen a estar inmerso en grasa durante "
	"las veinticuatro horas del día. Mantente a raya.\n");
	set_main_plural("Sacamantecas");
	add_plural("orcos");
	
	fijar_altura(190);
	fijar_peso_corporal(185000);
	
	fijar_raza("orco");
	fijar_clase("khazad");
	fijar_religion("gurthang");
	fijar_ciudadania("golthur");
	
	fijar_fue(18);
	fijar_extrema(100);
	fijar_con(18);
	fijar_des(6);
	fijar_int(13);
	fijar_sab(3);
	fijar_car(19);
	
	fijar_nivel(31);
	fijar_alineamiento(3500);
	
	add_clone(ARMAS + "machete_sacamantecas", 1);
	
	add_clone(BARMADURAS + "piel", 1);
	add_clone(BARMADURAS + "cinturon", 1);
	add_clone(BARMADURAS + "botas", 1);

	init_equip();
	
	fijar_maestria("hacha de batalla", 100);
	
	ajustar_dinero(roll(2, 10), "oro");
	
	ROL_NPCS->definir(this_object());
	
	load_chat(30, ({
		":corta los brazos de un cuerpo con unos precisos tajos de su machete.",
		":levanta el cuerpo de un enano y le rompe los huesos de las rodillas para cortarle las piernas mejor.",
		":desmembra como el sacamantecas profesional que es, su habilidad no tiene parangón en los reinos.",
		"'Lord Gurthang me confirió una habilidad y una exquisitez sublimes en el arte del buen sacamantecar",
		":chupa un pegote de grasa ensangrentada de su dedo pulgar.",
		":se rebaña el sudor de la frente con el antebrazo. Buena idea: ¡sus manos están llenas de sangre y grasa!",
		"'¿Vas a quedarte ahí todo el día? ¿no piensas ni saludar?",
		":lleva un torso hasta la trastienda y lo cuelga en un gancho del techo.",
		"'En el paso de Bûrzum hay una galería con ganchos como estos... los aventureros que van allí suelen acabar colgando de ellos, lo sé porque yo forjé esos ganchos",
		"'La grasa es un manjar exquisito, es el fluído más valioso, mucho más que la sangre",
		":tira a la basura un par de brazos usados.",
		"'Mi hija tampoco podrá pagarse la escuela de sacamantequería como sigas ahí parado sin comprar",
		":parece estar mascando la grasa volatilizada en el ambiente. Y le gusta.",
	}));
	
	load_a_chat(50, ({
		"'¡¡¡Tengo un gancho con tu nombre!!!",
		"'¡Me pregunto cuántos litros de grasa podré sacar de tu cuerpo!",
		":parece decidido a sacarte toda la manteca. Deberías salir corriendo de su tienda cuanto antes.",
		":maneja su machete con letal precisión.",
		":está imaginándose ahora mismo líneas imaginarias sobre tu cuerpo para cortarte en pedazos.",
	}));
	
	nuevo_dialogo("trabajo", ({
		":te habla mientras decapita el cadáver de un pequeño humanoide con gran precisión. Ni siquiera levanta la cabeza.",
		"'No es un trabajo",
		":interrumpe el desmembrado para mirarte fijamente a los ojos.",
		"'Esto, y escúchame con atención, es un arte",
		"'Y este arte consiste en sacar la grasa de los recién muertos, cuando todavía conservan algo de calor",
		"'La grasa de humano está muy demandada por los chamanes y hechiceros de la Torre de Brujería",
		"'Aunque algunas grasas son más valiosas que otras",
	}));
	nuevo_dialogo("algunas", ({
		":termina de desmembrar el cuerpo y cuelga el torso en un gancho del techo.",
		"'¡Ciertas razas tienen grasas con propiedades especiales!",
		"'O al menos, eso dicen los sabios, a mí me da igual, yo les entrego la grasa en unas vasijas y a partir de ahí me desentiendo",
		"'Mi objetivo es sacar la mayor cantidad de grasa pura de un cuerpo, nada más",
	}), "trabajo");
	nuevo_dialogo("sacar", ({
		"'¡Ah! ¿quieres saber más?",
		"'Veo que estás interesado en mi negocio, ¿eh?",
		"'Esto te va a gustar más que llevar paquetes de un lado a otro",
		":coge un cuerpo de pruebas de debajo del mostrador.",
		"'Mira, primero hay que desmembrar el cuerpo",
		":traza unas líneas imaginarias sobre el torso putrefacto.",
		"'Se le corta la cabeza, los brazos y las piernas por debajo de las rodillas",
		"'Nos interesan los muslos, tienen mucha grasa",
		":sonríe satisfecho, sin duda ama los muslos.",
		"'Después colgamos el torso en uno de estos ganchos",
		":señala unos ganchos que penden en la trastienda.",
		"'Bajo el torso ponemos una vasija, y alrededor de él están esos enormes cirios que ves",
		"'El calor de los cirios hace que la grasa se derrita y caiga al barreño",
		"'Cuando el barreño está lleno, tienes a un sacamantecas satisfecho",
		":sonríe, y de fondo escuchas el goteo de la grasa cayendo a las vasijas.",
	}), "algunas");
	nuevo_dialogo("colaborar", ({
		"'Vaya, ¿pero qué tenemos aquí?",
		":clava su machete en la mesa sin mucho esfuerzo.",
		"'¡Pues no me vendría nada mal que me %^YELLOW%^entregar%^RESET%^as cuerpos frescos!",
		"'A ser posible en una pieza",
		"'Lógicamente te pagaré por ellos",
		"'Por los cuerpos de semi-elfos, elfos y enanos te podré dar algo más de dinero",
		":empuña su machete con un ademán amenazador.",
		"'¡Y no se te ocurra traerme cuerpos de animales!",
		"'Lo menos que acepto en mi tienda es un humano, a partir de ellos, ¡lo que quieras!",
	}), "sacar");
	
	fijar_pvs_max(10000);
	fijar_pe_max(8000);
	
	add_attack_spell(100, "tajar", 3);
	
	adjust_tmp_damage_bon(200);
	
	ajustar_bo(250);
}

void init()
{
	::init();
	add_action("entregar", "entregar");
}

int entregar(string str)
{
	int platinos = 0, xp = 0;
	object cuerpo;

	if (!str || str == "") {
		return notify_fail("¿Qué es lo que quieres entregar?\n", this_player());
	}
	
	this_player()->consumir_hb();
	
	cuerpo = present(str, this_player());
	
	if (!cuerpo) {
		return notify_fail("No hay nada que atienda a ese nombre en tu inventario.\n", this_player());
	}
	
	if (!cuerpo->dame_cuerpo()) {
		return notify_fail("Recuerdas vívidamente la conversación con el sacamantecas: si le entregas algo que no "
		"sea un cuerpo te meterá el machete por donde tú ya sabes.\n", this_player());
	}
	
	if (sizeof(cuerpo->dame_partes_cortadas())) {
		do_say("¡Este cuerpo ya está desmembrado! ¡y además, mal! ¡no me sirve!", 0);
		return notify_fail("Parece que no se la has colado al sacamantecas.\n", this_player());
	}
	
	if (cuerpo->dame_pudredumbre() <= 5) {
		do_say("¡Este cuerpo apesta a podrido! ¡no me sirve!", 0);
		return notify_fail("Parece que tu entrega no está lo suficientemente fresca.\n", this_player());
	}
	
	switch (cuerpo->dame_nombre_raza()) {
		case "Humano":
			platinos = 10;
			break;
		case "Halfling":
			platinos = 15;
			break;
		case "Enano":
			platinos = 20;
			break;
		case "Elfo":
		case "Semi-elfo":
		case "Semi-drow":
			platinos = 25;
			break;
		case "Orgo":
			platinos = 30;
			break;
		default:
			do_say("Esta raza no me sirve, de aquí no se puede sacar grasa pura y de calidad", 0);
			return notify_fail("Parece que tendrás que probar con el cadáver de alguna otra raza.\n", this_player());
	}
	
	if (cuerpo->dame_cuerpo_jugador()) {
		platinos *= 2;
	}
	
	if (this_player()->query_timed_property("bloqueo-sacamantecas")) {
		platinos /= 5;
	}
	this_player()->add_timed_property("bloqueo-sacamantecas", 1, 300);
	
	xp = platinos * 100 + random(500);

	do_emote("se relame de placer.");
	do_say("Vaya vaya vaya, ¿qué tenemos aquí? ¡un buen ejemplar, sí señor!", 0);
	if (cuerpo->dame_cuerpo_jugador()) {
		do_say("¡Además es nada más y nada menos que el cuerpo de "+cuerpo->dame_nombre_cuerpo()+"!", 0);
	}  
	do_say("Aquí tienes unas monedas por tu esfuerzo...", 0);
	
	this_player()->adjust_money(platinos, "platino");
	this_player()->adjust_xp(xp);
	tell_object(this_player(), "Recibes "+platinos+" moneda"+(platinos > 1 ? "s":"")+" de platino.\n");
	
	write_file(LOGS + "sacamantecas", ctime()+" "+this_player()->query_cap_name()+" entrega "+cuerpo->query_short()+" y recibe "+xp+" XP y "+platinos+" platinos.\n");
	
	cuerpo->move("/room/objetos_perdidos");
	cuerpo->dest_me();
	
	return 1;
}

int frases_personalizables(string tipo, object npc, object pj)
{
	switch (tipo)
	{
		case "bienvenida-pj":
			if (HORDA_NEGRA->es_oficial(pj)) {
				npc->do_emote("hace una profunda reverencia.");
				npc->do_say("¡Salve, "+HORDA_NEGRA->dame_titulo_corto(pj)+"! ¡es un honor tener su presencia en mi humilde tienda!", 0);
			} else {
				npc->do_emote("continua desmembrando como si nada.");
				npc->do_say("¿Sí?", 0);
			}
			break;
		case "bienvenida-npc":
			break;
		case "despedida-pj":
			pj->do_say("Adiós, tengo que irme", 0);
			break;
		case "despedida-npc":
			npc->do_emote("se despide sin siquiera levantar la mirada de la mesa ensangrentada.");
			npc->do_say("Sí, muy bien, adiós", 0);
			break;
		default:
			return 0;
	}

	return 1;
}
