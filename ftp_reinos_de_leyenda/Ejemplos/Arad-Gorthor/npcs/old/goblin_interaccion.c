// Dherion 31/03/2017 npc goblin para la interacción
#include "../path.h";
inherit "/obj/monster.c";
#define SHADOW_ESCONDERSE "/habilidades/shadows/esconderse_sh.c"
void setup()
{
    set_name("goblin");
    set_short("Salteador Goblin");
    set_long("Un escuálido y aparentemente desesperado goblin que merodea por los inhóspitos y despoblados "
      "caminos de Arad Gorthor en busca de víctimas a las que robar y, si procede, comerse. Las penurias que "
      "estos desertores de la Horda Negra sufren les impulsan a atacar a todo ser viviente que se cruce por "
      "su camino, sea enemigo o aliado.\n");
    set_main_plural("Salteadores Goblin");
    generar_alias_y_plurales();
    fijar_altura(80);
    fijar_peso_corporal(40000);
    fijar_raza("goblin");
    fijar_clase("ladron");
    fijar_religion("gurthang");
    fijar_fue(14 + !random(2));
    fijar_con(17);
    fijar_des(18);
    fijar_int(4);
    fijar_sab(4);
    fijar_car(3);
    fijar_nivel(10);
    fijar_alineamiento(8500);
    load_chat(10, ({
        ":permanece agazapado al borde de la senda, esperando que pase algún incauto que emboscar.",
        ":relame sus resquebrajados labios. ¡El calor en estos caminos es insoportable!",
      }));
    load_a_chat(50, ({
        "'¡Dame todo lo que tengas, zoquete!",
        ":agita su arma mientras berrea, intentando asustar.",
        "'¡¿Nunca te han dicho que es peligroso andar por estos caminos?!",
        "'¿Ves lo que me obligas a hacerte? ¡cualquier cosa es mejor antes que morir en las filas de la Horda!",
      }));
    switch (random(4)) {
    case 0:
        add_clone(BARMAS + "daga", 1);
        fijar_maestria("daga", 10+random(11));
        break;
    case 1:
        add_clone(BARMAS + "puñal", 1);
        fijar_maestria("puñal", 10+random(11));
        break;
    case 2:
        add_clone(BARMAS + "estilete", 1);
        fijar_maestria("estilete", 10+random(11));
        break;
    case 3:
        add_clone(BARMAS + "cuchillo", 1);
        fijar_maestria("cuchillo", 10+random(11));
        break;
    }
    add_clone(BARMADURAS + "zapatillas", 1);
    add_clone(BARMADURAS + "pantalones", 1);
    if (!random(5)) {
        add_clone(BARMADURAS + "cuero", 1);
    }
    init_equip();
    if (!random(10)) {
        ajustar_dinero(1 + random(2), "oro");
    } 
    ajustar_dinero(roll(2, 10), "plata");
}
void init()
{
    ::init();
    if (query_static_property("esconderse_iniciado")) {
        return;
    }
    add_static_property("esconderse_iniciado", 1);
    clone_object(SHADOW_ESCONDERSE)->setup_sombra(this_object(), 300, 0);
    set_hidden(1);
}
