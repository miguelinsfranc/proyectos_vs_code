// Dunkelheit 07-11-2009

#include "../path.h";
inherit "/obj/monster.c";

int check_anyone_here() { return 1; }

void setup()
{
	set_name("tirador");
	set_short("Tirador Hobgoblin");
	add_alias("hobgoblin");
	set_long("Este escurridizo tirador hobgoblin es uno de los mayores peligros "
	"de los desiertos de Arad Gorthor. Desde lo alto de las torres de asedio se "
	"dedica a disparar a todo cuanto se mueve, dando la voz de alarma al resto "
	"de hobgoblins para que se lancen en masa a rematar a su infeliz víctima.\n");
	set_main_plural("Tiradores Hobgoblin");
	add_plural(({"tiradores","hobgoblins"}));
	
	fijar_altura(80);
	fijar_peso_corporal(40000);
	
	fijar_subraza("hobgoblin");
	fijar_clase("tirador");
	fijar_religion("gurthang");
	
	fijar_fue(18);
	fijar_con(18);
	fijar_des(18);
	fijar_int(6);
	fijar_sab(3);
	fijar_car(6);
	
	fijar_nivel(23);
	fijar_alineamiento(9000);
	
	add_clone("/baseobs/proyectiles/arco_largo", 1);
	add_clone("/baseobs/proyectiles/flecha", 20 + random(6));
	add_clone(BARMADURAS + "botas", 1);
	add_clone(BARMADURAS + "cinturon", 1);
	add_clone(BARMADURAS + "capucha", 1);
	
	fijar_maestria("arco largo", 40);

	init_equip();
	
	set_aggressive(1);
	
	ajustar_dinero(3 + random(3), "oro");
}

object buscar_victima()
{
	if (sizeof(query_attacker_list())) {
		return element_of(query_attacker_list());
	}
	if (sizeof(query_call_outed())) {
		return element_of(query_call_outed());
	}
}

void attack_ob(object ob)
{
	if (!query_heart_beat(this_object())) {
		set_heart_beat(1);
	}
	::attack_ob(ob);
}

void heart_beat()
{
	object victima;

	if (!query_static_property("pasivo")) {
		add_static_property("pasivo", 1);
		habilidad("disparopasivo", 0);
	}
	
	if (!query_time_remaining("bloqueo-Disparocertero") && victima = buscar_victima()) {
		habilidad("disparocertero", victima);
	}

	::heart_beat();
}
