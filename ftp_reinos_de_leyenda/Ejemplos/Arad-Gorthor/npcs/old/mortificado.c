// Dunkelheit 24-11-2009

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	set_name("mortificado");
	set_short("Mortificado");
	set_long("Los mortificados llevan su fé por su dios, Lord Gurthang, hasta unos "
	"extremos que escapan a tu comprensión. Puedes entender la autoflagelación, pero... "
	"¿la autoamputación de miembros hasta morir desangrado? Este goblin está tendido "
	"en el suelo, sumido en el charco de su propia sangre. Una de sus extremidades "
	"yace junto a él, y un círculo de plañideras entonan oraciones alrededor suyo. "
	"Para ellas, esta moribunda es un héroe.\n");
	set_main_plural("Mortificados");
	add_plural("mortificados");
	
	fijar_altura(130+random(20));
	fijar_peso_corporal(30000+random(20000));
	
	fijar_raza("goblin");
	fijar_clase("aventurero");
	fijar_religion("gurthang");
	fijar_ciudadania("mor_groddur");
	
	fijar_fue(6);
	fijar_con(14);
	fijar_des(9);
	fijar_int(4);
	fijar_sab(4);
	fijar_car(3);
	
	fijar_nivel(10 + random(6));
	fijar_alineamiento(900);
	
	ajustar_dinero(roll(1, 2), "cobre");
	
	ROL_NPCS->definir(this_object());
	
	fijar_pvs(dame_pvs_max() / 10, this_object());
}

string dame_noconversar_personalizado()
{
		return "El mortificado no puede responderte. Está inconsciente, o tal vez ya muerto.\n";
}
