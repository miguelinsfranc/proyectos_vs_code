//Rezzah 18/01/2010

inherit "/obj/monster";
void debug(mixed txt) {
	tell_object(find_player("rezzah"), "\n"+txt+"\n");
}
void setup(){
	set_name("llama");
	set_short("%^BLACK%^BOLD%^Llama de Gurthang%^RESET%^");
	set_main_plural("%^BLACK%^BOLD%^Llamas de Gurthang%^RESET%^");
	add_alias(({"demonio"}));
	add_plural(({"demonios"}));
	set_long("La gigantesca criatura humanoide que tienes ante tí está formada principalmente por una llama negra que arde con una "
		"fuerza que parece inextinguible. Es un brote de pura maldad, nacido de la voluntad de Gurthang, para proteger "
		"a su siervo más destacado, el profeta Gargax.\n");
	fijar_bando("malo");
	fijar_religion("gurthang");
	fijar_raza("demonio");
	fijar_clase("barbaro");

	fijar_tamanyo_racial(9);

	fijar_estilo("puñetazo");

	fijar_alineamiento(80000);
	fijar_fe(100);
	fijar_genero(1);

	fijar_fue(40);
	fijar_extrema(100);
	fijar_des(6);
	fijar_con(30);
	fijar_int(3);
	fijar_sab(3);
	fijar_car(25);

	fijar_nivel(40+random(5));
	add_loved("jugador","gargax");
	set_aggressive(4);
	ajustar_carac_tmp("daño",50,0,0,0);
	ajustar_bo(250);
	ajustar_bp(160);
	add_attack_spell(30,"bola de fuego",4);
	add_static_property("libre-movimiento", 1);
	add_static_property("sin miedo", 1);
	add_attack_spell(30, "lanzar", 3);
	add_attack_spell(50, "cabezazo", 3);
}

int spell_damage(int pupa,string tipo,object lanzador,object hechizo) {
	int danio;
	if (tipo == "fuego" ) {
		danio = to_int(floor(pupa*-0.3));
		ajustar_pvs(danio, lanzador);
		return danio;//teóricamente si les curan les baja xp 
	} 
	if (tipo == "divino" ||tipo == "bien") {
		danio = to_int(ceil(pupa*1.2));
		return ::spell_damage(danio, tipo, lanzador, hechizo);
	}
	return danio;
}

int volver(object room) {
	string salida;

	foreach (string str in environment()->query_dest_dir()) {
		if (-1 == strsrch(str, "/")) {
			salida = str;//sino contiene / es la salida, si la contiene es el path de la salida
		} else {
			if (str == file_name(room)) { 
				queue_action(salida); //vuelve
			}
		}
	}
}
void event_exit(object quien_sale, string mensaje, object entorno_destino, object * seguidores) {
    string salida;

	if (query_timed_property("bloqueo-arremeter")) {
		//remove_timed_property("bloqueo-arremeter");
		::event_exit(quien_sale, mensaje, entorno_destino, seguidores);
		return;
	}
    if (-1 != member_array(quien_sale, dame_lista_atacantes())) {// || -1 != member_array(quien_sale, dame_lista_perseguidos())) {
        foreach (string str in environment()->query_dest_dir() ) {
            if (-1 == strsrch(str, "/")) {
				salida = str;//sino contiene / es la salida, si la contiene es el path de la salida
			} else {
            	if (str == file_name(entorno_destino)) {
                	call_out("habilidad", 1, "arremeter", salida+" "+quien_sale->query_name());
					if (sizeof(dame_lista_atacantes()) > 1) call_out("volver", 3, environment());//regresará a pegarse con el resto
				}
            }
        }
    }
	::event_exit(quien_sale, mensaje, entorno_destino, seguidores);
}

int unarmed_damage(int pupa,string estilo,object atacante) { 
	tell_object(atacante, "Tu golpe no parece dañar a "+query_short()+" pero el contacto con su cuerpo ¡TE ABRASA!\n");
	ajustar_pvs(10+random(30), atacante);
	return 0; 
}
