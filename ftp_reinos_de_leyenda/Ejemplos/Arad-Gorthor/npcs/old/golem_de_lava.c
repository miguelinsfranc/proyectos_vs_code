//Rezzah 17/11/2009 (basado en el golem de piedra)

inherit "/obj/monster";


int dame_golem() { return 1; }

void setup(){
	set_name("golem");
	set_short("Golem de Lava");
	set_main_plural("Golems de Lava");
	add_alias(({"golem","lava"}));
	add_plural(({"golems","lavas"}));
	set_long("Una gigantesca mole de roca y fuego que emana gases tóxicos por toda sus superfice, está formado completamente por magma y duras rocas flotan por su superfice moviéndse a lo largo del torso de esta gigantesca mole de fuego. Sus movimientos son torpes y mecánicos, pero la fuerza que debe poseer es aterradora.\n");

	add_zone("laberinto");
	set_move_after(1,10);

	fijar_bando("malo");
	fijar_religion("gurthang");
	fijar_raza("golem");
	fijar_clase("barbaro");

	fijar_tamanyo_racial(9);

	fijar_estilo("puñetazo");

	fijar_alineamiento(0);
	fijar_genero(1);

	fijar_fue(40);
	fijar_des(6);
	fijar_con(30);
	fijar_int(3);
	fijar_sab(3);
	fijar_car(25);

	fijar_nivel(40+random(5));
	set_aggressive(3);
	ajustar_carac_tmp("daño",50,0,0,0);
	ajustar_bo(250);
	ajustar_bp(160);
	add_attack_spell(30,"bola de fuego",4);
	add_attack_spell(20, "tormenta acida", 4);
	add_static_property("libre-movimiento", 1);
	add_static_property("sin miedo", 1);
}
int weapon_damage(int cantidad,object atacante,object arma,string ataque){
	if( !arma || arma->dame_encantamiento()<5)
	{
		string nom= arma ? arma->query_short(): "arma";
		
		tell_object(atacante,"Tu "+nom+" no parece dañar lo más mínimo a "+this_object()->query_short()+".\n");
		tell_accion(environment(atacante),"El ataque de "+atacante->query_short()+" con su "+nom+" no parece dañar a "+this_object()->query_short()+".\n","Oyes ruidos de golpes.\n",({atacante}),atacante);
		return 0;
	}
	return ::weapon_damage(cantidad,atacante,arma,ataque);
}

int unarmed_damage(int pupa,string estilo,object atacante) { return 0; }
int spell_damage(int pupa,string tipo,object lanzador,object hechizo) {
	int danio;
	if (tipo == "fuego" || tipo == "tierra" || tipo == "acido") { //Se cura con tierra, fuego y acido XD es lava!
		danio = to_int(floor(pupa*-0.5));
		ajustar_pvs(danio, lanzador);
		return danio;//teóricamente si les curan les baja xp 
	} 
	if (tipo == "agua" ||tipo == "frio") {//Le hacen un 20% más de daño si es agua o frio
		danio = to_int(ceil(pupa*1.2));
		::spell_damage(danio, tipo, lanzador, hechizo);
		return danio;
	}
	//En cualquier otro caso suda de los ataques mágicos
	return 0;
}
