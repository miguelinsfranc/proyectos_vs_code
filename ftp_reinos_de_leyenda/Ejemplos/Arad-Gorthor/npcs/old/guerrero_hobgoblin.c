// Dunkelheit 15-11-2009

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	set_name("hobgoblin");
	set_short("Guerrero Hobgoblin");
	add_alias("guerrero");
	set_long("Un salvaje guerrero hobgoblin cuya rojiza piel está llena de pinturas de camuflaje. "
	"Dos manchurrones de pintura negra cruzan sus afilados y delgados pómulos, y en su frente hay "
	"tres puntos del mismo color. Su cabellera negra está recogida por una sencilla cinta. Su cuerpo "
	"es mucho más musculoso que el del resto de sus congéneres, y es bastante notable para tratarse "
	"de una criatura que apenas llega al metro veinte de altura.\n");
	set_main_plural("Guerreros Hobgoblins");
	add_plural(({"guerreros", "hobgoblins"}));
	
	fijar_altura(100 + random(20));
	fijar_peso_corporal(60000 + random(20000));
	
	fijar_subraza("hobgoblin");
	fijar_clase("soldado");
	fijar_religion("gurthang");
	
	fijar_fue(18);
	fijar_extrema(50 + random(20));
	fijar_con(17);
	fijar_des(13);
	fijar_int(5);
	fijar_sab(3);
	fijar_car(3);
	
	fijar_nivel(26);
	fijar_alineamiento(2000);
	
	set_aggressive(1);
	
	switch (random(3)) {
		case 0:
			add_clone(BARMAS + "daga", 1);
			break;
		case 1:
			add_clone(BARMAS + "puñal", 1);
			break;
		case 2:
			add_clone(BARMAS + "espada_corta", 1);
			break;
	}
	if (random(4)) {
		add_clone(BARMADURAS + "cuero", 1);
	} else {
		add_clone(BARMADURAS + "cuero_tachonado", 1);
	}
	add_clone(BARMADURAS + "cinta", 1);
	init_equip();
		
	load_a_chat(30, ({
		":se revuelve como un perro rabioso.",
		"'¡No saldrás de nuestra guarida con vida!",
		"'¡La curiosidad te llevará a la tumba, intruso!",
		"'¡Hoy tenemos carne en el menú! ¡hahahaha!",
		":blande sus armas con gran agilidad; es, sin duda, un enemigo formidable.",
	}));
	
	ajustar_dinero(roll(1, 5), "oro");
}
