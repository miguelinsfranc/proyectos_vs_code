// Zoilder 20/01/2011
// Clase NPC basica para pescadores
#include "../path.h";
inherit "/obj/monster.c";

/*
 *	Función que devuelve una descripción aleatoria para el NPC.
 */
string dame_descripcion()
{
    string base_raza = capitalize(dame_numeral()) + " " + dame_raza() +
                       (dame_genero() == 1 ? " pescador." : " pescadora.");
    string *descripciones =
        ({base_raza + " Por su forma de tratar su caña de pescar, parece que "
                      "aún no controla demasiado bien su oficio. "
                      "De rostro juvenil y ojos azules, parece estar más "
                      "interesado en el sexo opuesto, a quienes no deja de "
                      "mirar cuando pasan a su lado, "
                      "que en el arte de pescar.\n",
          base_raza +
              " Lleva encima una caña de pescar que por lo reluciente que "
              "está, denota que la cuida mucho; "
              "podría decirse que casi más que a su propio cuerpo, el cual "
              "presenta varias cicatrices a lo largo de su rostro y brazos.\n",
          base_raza +
              " Te fijas en la fuerza con la que sujeta su caña de pescar, "
              "parece que se deja la vida en ello. "
              "Notas como no aparta su mirada del agua, como si estuviera "
              "estudiando el movimiento de los peces bajo ella.\n"});
    return element_of(descripciones);
}

/*
 *	Configuración del NPC
 */
void setup()
{
    string my_raza = element_of(({"orco", "goblin", "gnoll", "kobold"}));
    string letra_genero;
    // Se establece en primer lugar el genero del NPC, pues a partir de eso se
    // sacan las descripciones y nombres
    fijar_genero(random(2) + 1);
    letra_genero = (dame_genero() == 1 ? "" : "a");

    set_name("pescador" + letra_genero);
    add_alias(({"pescador" + letra_genero, my_raza}));
    add_plural(
        ({"pescador" + (dame_genero() == 1 ? "es" : "as"), my_raza + "s"}));
    set_short("Pescador" + letra_genero + " " + my_raza);
    set_main_plural(
        "Pescador" + (dame_genero() == 1 ? "es" : "as") + " " + my_raza + "s");
    fijar_raza(my_raza);
    // Se obtiene una descripción aleatoria
    set_long(dame_descripcion());
    fijar_religion("gurthang");
    fijar_clase("aventurero");
    fijar_oficio("pescador");

    nuevo_lenguaje("negra", 100);

    set_random_stats(10, 18);
    fijar_nivel(3 + random(16));
    fijar_altura(150 + random(50));
    fijar_peso_corporal(40000 + random(50) * 1000);

    load_chat(
        30,
        ({1,
          ":observa su caña de pescar.",
          1,
          ":se seca el sudor de la frente.",
          1,
          "'Cada vez me cuesta más pescar buenas piezas.",
          1,
          ":otea el horizonte."}));

    add_clone(BASEOBS + "herramientas/pesca/canya.c", 1);
    add_clone(BARMADURAS + "capucha.c", 1);
    add_clone(BARMADURAS + "camiseta.c", 1);
    add_clone(BARMADURAS + "pantalones.c", 1);
    add_clone(BARMADURAS + "botas.c", 1);
    init_equip();
}
