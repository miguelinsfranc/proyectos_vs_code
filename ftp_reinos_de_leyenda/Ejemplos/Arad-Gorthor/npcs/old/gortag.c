inherit "/obj/conversar_npc.c";

void setup_dialogo() {
	
	nuevo_dialogo("nombre", "Soy el Chamán Gortag, o mejor dicho, lo poco que queda de él.");
	nuevo_dialogo("queda", ({
		":suspira tristemente.",
		"'Tuve un pequeño accidente hace un tiempo, ya no recuerdo ni cuánto..",
		":se estremece al recordar el accidente."}), "nombre");
	nuevo_dialogo("accidente", ({
		":se ha sonrojado levemente al hacer memoria, a pesar de no poseer sangre en sus venas intangibles.",
		"'En realidad más que un accidente fue un ligero despiste, nos reunimos varios chamanes en cónclave para invocar un poderoso demonio, pero algo salíó mal.",
		":mira hacia otro lado, hacia la roca que le rodea, y suspira.",
		"'Es que no sé que pintaba yo en el maldito cónclave."
	}), "queda");
	nuevo_dialogo("conclave", ({
		"'Burz, mi mentor, me reclamó a su presencia, había profetizado una terrible catástrofe y como solución reunió a cuatro chamanes más, pero uno no apareció y me tocó sustituirle, los cincos partimos al Pozo Rojo para intentar salvar nuestras tribus invocando un demonio.",
		":mira al suelo diciendo 'no' repetidamente con la cabeza.",
		"'Resultó completamente inútil, más nos valdría haber huído, ahora todos están muertos."
	}), "accidente");
	nuevo_dialogo("catastrofe", ({
		"'Nos dijo que en poco tiempo, como un río de fuego, la lava brotaría desde el Pozo y arrasaría todo a su paso, que la ira divina impulsaría tal destrucción y que sería imposible evitarlo, por lo que intentaríamos salvar nuestras vidas ya que no había escapatoria posible...",
		":se estremece al imaginarlo.",
		"'¡Se trataba de un Cataclismo a nivel global!",
		"'Encima tuvimos que atravesar aquel extraño laberinto, custodiado por Uil'ahzn... aún tiemblo al recordarlo..."
	}), "conclave");
	nuevo_dialogo("demonio", ({
		"'Supuestamente podríamos invocar a un poderoso Demonio en un cónclave de cinco chamanes, pero...",
		"'¡¿Porqué tuvo que llevarme a mí?!", 
		"'Burz sabía que yo no soy muy diestro con las invocaciones, ahora están todos muertos, se lo tienen merecido... ¿pero yo? ¡Yo no merecía morir!",
		":eleva la vista clamando al cielo amargado.",
		"'¡¡ Maldito seas Burz !!"
	}), "catastrofe");
	nuevo_dialogo("uil'ahzn", ({
		"'¿No sabes quién es? Jajaja... pues mucha suerte si te lo cruzas...",
		":sonríe ocultando algo."
	}), "catastrofe");
	nuevo_dialogo("laberinto", ({
		"'Un enrevesado laberinto plagado de golems horribles, por fortuna bastante torpes.",
		"'Recuerdo que para encontrar la salida tuvimos que mirar por todas partes cada pocos metros, y encima para salir había una trampa, había que pulsar más abajo del oculto botón que nos abrió la puerta.",
		"'Pero lo que no entiendo qué salió mal...",
		"puede_salir_laberinto"
	}), "catastrofe");
	nuevo_dialogo("mal", ({
		"'Bueno, creo que fue un ataque mental del demonio, consiguió derribar mis defensas y fallé la concentración...",
		":tiembla sólo con recordarlo.",
		"'Al fallar durante la fase de dominio del demonio una parte del círculo rúnico con el que pretendíamos atarlo se resquebrajó...",
		"'Con un demonio cualquiera no habría pasado así, pero el círculo no pudo contener todo su poder, nos tocó iniciar de nuevo el salmo de prisión.",
	}), "demonio");
	nuevo_dialogo("salmo", ({
		"'Ese salmo nos permitió contenerlo en una jaula de energía mística, pero no podíamos movernos para recomponer el círculo.",
		":se muestra apesadumbrado.",
		"'Si lo hubiéramos podido recomponer habría quedado firmemente sellado en la jaula y habríamos podido volver al salmo de dominio.",
		"'Entonces se habría puesto a nuestras órdenes y nos habría salvado de la catástrofe, pero desgraciadamente no fue así.",
		"'Quedamos en un empate técnico, sin poder dejar de salmodiar para que no acabara con todos nosotros y sin poder movernos para reconstruir el círculo."
	}), "mal");
	nuevo_dialogo("empate", ({ 
		"'Estuvimos tan concentrados buscando una solución a la vez que repitiendo el cántico que lo mantenía inofensivo que ...",
		"'Cuando quisimos darnos cuenta era tarde, la catástrofe se cernía sobre nosotros, un río de lava nos abrasó al instante y creo que todos murieron excepto yo.",
		"'Yo me salvé, bueno... 'me salvé'... porque prudentemente había conjurado un hechizo que me devolvería a mi hogar si mi cuerpo sufría daño."
	}), "salmo");
	nuevo_dialogo("hechizo", ({
		"'Todo habría funcionado correctamente pero no esperaba morir instantáneamente así que solo mi espíritu regresó a mi hogar.",
		":aprieta los puños enrabietado.",
		"'Como no esperaba regresar muerto no pensé en retirar los conjuros que protegen mi rincón de los muertos vivientes, ahora soy uno de ellos y no puedo entrar."	
	}), "empate");
	nuevo_dialogo("rincon", ({
		"'Está un poco más al sur, quizá ... ",
		":te mira con gesto de duda.",
		"'¿Podrías ayudarme? Si tú entraras en mi hogar y desactivaras el conjuro, podría entrar a mi rincón y quizá recuperar mi cuerpo del pozo para descansar enternamente de una vez por todas."
	}), "hechizo");
	nuevo_dialogo("pozo", ({
		"'¿Te interesa el pozo? Es un sitio realmente peligroso, no creo que debas ir pero puedes encontrar cómo llegar en mi diario.",
		":mira hacia el sur y estira el brazo señalando en esa dirección.",
		"'Allá, junto a las rocas, está mi rincón, y tallada en la piedra hay una caja, si la giras desprotegerá el rincón permitiéndome acceder y además en ella encontrarás mi diario",
		":sonríe ampliamente.",
		"'Todos ganamos, ¿verdad?",
		"sabe_girar"
	}), "rincon");
	habilitar_conversacion();
}

void sabe_girar(object jugador) {
	if (jugador->dame_religion() == "gurthang") {
		do_say("Por cierto, ten cuidado al girar la caja, un gas venenoso saldrá disparado a tu rostro si no lo haces extremadamente lento.", 0);
	}
	jugador->add_static_property("glachdavir_sabe_girar", 1);
}
void puede_salir_laberinto(object jugador) {
	jugador->add_static_property("glachdavir_salir_laberinto",1);
}
void setup() {
	set_short("Gortag, El Espíritu");
	set_name("gortag");
	set_long("Tienes ante tí el reflejo de lo que otrora fue un chamán goblin, su etéreo cuerpo parece arrastrarse unos centrímetos por encima del suelo y muestra signos de profundas quemaduras, desde el rostro, en menor medida, hasta las manos, casi calcinadas. Su grisácea apariencia parece brillar aleatoriamente cada cierto tiempo como si aún tuviera algún poder remanente en él.\n");
	fijar_clase("sacerdote");
	fijar_especializacion("chaman");
	add_alias(({"espíritu","espiritu"}));
	set_main_plural("Gortags, Los Espíritus");
	add_plural(({"gortags","espiritu","espíritus"}));
	fijar_raza("npc/muerto-viviente");
	fijar_alineamiento(15000);
	fijar_carac("int",12);
	fijar_carac("sab",18);
	fijar_carac("des",15);
	fijar_carac("fue",10);
	fijar_carac("con",15);
	fijar_nivel(25);
	nuevo_lenguaje("negra",100);
	nuevo_lenguaje("dendrita",60);
	fijar_lenguaje_actual("negra");
	setup_dialogo();
	fijar_pe_max(300);
	fijar_pe(300);
	fijar_pvs_max(4000);
	add_attack_spell(80, "hervor de la sangre", 3);
	add_attack_spell(40, "relampago", 3);
	add_attack_spell(10, "tormenta ignea",4);
}
