//Rezzah 16/11/2009	

inherit "/baseobs/monstruos/bases/dragon_conversar.c";
#include "../path.h";

void nube_de_sombras(object*);
void rafaga_deshidratacion(object*);
void furia_desatada(object*);
void inicia_lucha();
void setup() {
	fijar_dragon("gema", "topacio");
	fijar_edad("adolescente");
	fijar_nombre("Uil'ahzn");
	fijar_sexo(1);
	poner_en_guarida();

	set_name("uilahzn");
	
	
	nuevo_lenguaje("dwerg",100);
	nuevo_lenguaje("dendrita",100);
	nuevo_lenguaje("adurn",100);
	nuevo_lenguaje("negra",100);
	nuevo_lenguaje("drow",100);
	nuevo_lenguaje("lagarto",100);

   add_static_property("sin miedo", 1);
   add_static_property("abrigado", 1);
   add_static_property("no_robable", 1);
   add_static_property("no_desarmar", 1);

	nuevo_dialogo("dragon", ({
		"'¡Oh! Que perspicaz. Sí, yo soy el dragón al que me refería.",
		":extiende ligeramente sus alas y las agita con gracia, una espesa bruma parece brotar de ellas.",
		"'Soy el Guardián del %^WHITE%^BOLD%^Laberinto%^RESET%^, y tú, ¿eres un %^WHITE%^BOLD%^visitante%^RESET%^ o un %^WHITE%^BOLD%^intruso%^RESET%^?"
	}));
	nuevo_dialogo("visitante", ({
		"' ¡Todos los que están tras estas puertas, están muertos o nunca tuvieron una vida! ¿Acaso quieres unirte a ellos?",
		":eleva su cabeza a la vez que el tono de su voz resultando atronador, llenándote de miedo.",
		"' ¡Contesta escoria! ¿Por qué habría de darte paso al %^WHITE%^BOLD%^Laberinto%^RESET%^?"
	}), "dragon");
	nuevo_dialogo("intruso", ({
		":sonríe con franqueza mientras te evalúa decidiendo si tanta franqueza es un desafío o si sencillamente estás mal de la cabeza.",
		":ríe con ganas, dejando atrás la tensión del momento."
		"'¿Y qué te trae a mi cubil intruso?¿Vienes a buscar mis %^BOLD%^WHITE%^tesoros%^RESET%^ o estás de %^BOLD%^WHITE%^paso%^RESET%^?"
	}), "dragon");

	nuevo_dialogo("tesoros", ({
		"' ¡Jajajaja! Admiro tu franqueza, pero ese tema es algo delicado para jugar con él, ¡sea pues!",
		":despliega sus alas ocupando todo el ancho del pasillo en una espectacular exhibición de su musculatura.",
		":te mira y en la más cruel sonrisa que jamás imaginaste exhibe unas hileras mortíferas de dientes.",
		"inicia_lucha"
	}), "intruso");
	
	nuevo_dialogo("paso", ({
		"' ¿Y vas a paso a donde? Ya te dicho que yo soy el Guardián del %^WHITE%^BOLD%^Laberinto%*^. No debo dejarte pasar.",
		":te mira con expectación, esperando una nueva sorpresa."
	}), "intruso");
	nuevo_dialogo("laberinto", ({
		"' Tras estas puerta hay un laberinto del que pocos, por no decir casi nadie, ha salido con vida.",
		"' Lo que buscan tras esas paredes no es mi asunto, el mío es solo permitir el entrar a quienes lo merecen...",
		"' ¿Acaso crees que lo mereces?"
	}), "dragon");
	nuevo_dialogo("merecer", ({
		":clava sus ojos en los tuyos impidiéndote reaccionar, notas como desgrana tu alma capa por capa sin que puedas esconde nada.",
		"inicia_trato"
	}), "laberinto");
	//Resiste agua
	add_static_property("agua", 40);

	add_attack_spell(40, "nube_de_sombras", (:nube_de_sombras:));
	add_attack_spell(40, "rafaga_deshidratación", (:rafaga_deshidratacion:));
	
//	add_attack_spell(10, "furia_desatada", (:furia_desatada:));
	habilitar_conversacion();
	crear_dragon();
}
//Crea una nube de brumas hace daño de frío aunque no mucho pero debilita las bo's y be's excepto a hombres-lagarto q resisten la bruma (no el frio)
void nube_de_sombras(object * atacantes) {//Paso uno, avisamos y damos un segundo
	tell_accion(environment(), query_short()+" inhala con fuerza llenando sus pulmones de aire conteniéndolos en pecho por unos segundos.\n", "Oyes una profunda inspiración.\n", ({TO}), 0);
	call_out("nube_de_sombras2",2);
}
void nube_de_sombras2() {
	int pupa, tiempo;
	object * atacantes;
	//metemos el daño
	atacantes = dame_lista_atacantes();//los aún presentes
	pupa = dame_carac("int")*4+dame_carac("sab")*2+dame_carac("con");
	if (dame_carac("car") > 25) { //el máximo es 25+stats[PODER]+multiplicador_edad*5+8, daño bonificado
		pupa =  to_int(ceil(1.5 * pupa));
	}
	tiempo = random(15)+10;
	foreach (object ob in atacantes) {
		if (ob->dame_raza() == "hombre-lagarto") {//más daño aún
			pupa = to_int(ceil(1.2 * pupa));
		} else {//no encaja más daño pero pierde be's, bp's y bo's
			ob->ajustar_be_tmp(-dame_carac("int"), tiempo, 0, 0);
			ob->ajustar_bo_tmp(-dame_carac("int"), tiempo, 0, 0);
			ob->ajustar_bp_tmp(-dame_carac("int"), tiempo, 0, 0);
		}
		ob->spell_damage(-pupa, "frio", TO, 0);
	}
	
	tell_accion(environment(), query_short()+" exhala una nube de sombras y vapores húmedos que como una bruma se extiende por todas partes dificultando la visión.\n","Oyes un profundo soplido.\n", ({TO}), 0);

}
void rafaga_deshidratacion(object * atacantes) {
	object victima;
	victima = atacantes[random(sizeof(atacantes))];
	
	tell_object(victima, query_short()+ " te mira con intenso odio mientras un %^BLACK%^BOLD%^hálito grisáceo%^RESET%^ se acumula en sus fosas nasales.\n");
	tell_accion(environment(), query_short()+" mira a "+victima->query_short()+" con intenso odio mientras un %^BLACK%^BOLD%^hálito grisáceo%^RESET%^ se acumula en sus fosas nasales.\n", "", ({victima}), 0);
	
	call_out("rafaga_deshidratacion2", 2, victima);
}

void rafaga_deshidratacion2(object victima) {
	int pupa;
	if (present(victima, environment())) {//no se ha ido ÑAM
		if (victima->dame_ts("agilidad", (multiplicador_poder * -4) - dame_carac("des"))) { //Se salva! Maldito bastardo
			tell_object(victima, "%^MAGENTA%^*%^RESET%^ Consigues evitar en el último segundo el mortífero vapor que exhalaba "+query_short()+" contra tí.\n");
			tell_accion(environment(), victima->query_short()+" consigue evitar en el último segundo el mortífero vapor que exhalaba "+query_short()+" sobre "+victima->dame_pronombre()+".\n");
		} else {	//Ñam
			tell_object(victima, "%^RED%^*%^RESET%^ Sientes tu piel agrietarse y resquebrajarse como si estuvieras envejeciendo instantáneamente cuando los fluidos de tu cuerpo de secan.\n");
			victima->nueva_incapacidad("deshidratacion", "incapacidades", multiplicador_poder*2, multiplicador_poder + multiplicador_edad);
			pupa = 200+multiplicador_poder*100+random(500); //DOLOR!
			victima->spell_damage(-pupa, "enfermedad", TO, 0);
			tell_accion(environment(), query_short()+" sopla con furia sobre "+victima->query_short()+" que se reseca instantáneamente quedando como una pasa podrida.\n", "", ({victima}), 0);
		}
	}	else {
		tell_accion(environment(), query_short()+" mira contrariado como su víctima ha escapado corriendo.\n");
	}
}

void init() {
	::init();
	add_action("entregar", ({"entregar", "pagar"}));
}

void abrir() {
	do_emote("se gira hacia la puerta y tras pronunciar tan rápidamente como el vuelo de un mosquito las palabras de algún tipo de encantamiento deshace la protección de la salida sur.");
	environment()->add_static_property("abierta_por_dragon",1);
}
int entregar(string tx) {
	if (!query_static_property("precio")) return notify_fail("Parece que %^GREEN%^BOLD%^entregar%^RESET%^ no produjo efecto alguno.\n");
	if (tx) return notify_fail("Si quieres entregar el precio que "+query_short()+" te ha pedio pon '"+query_verb()+"' a secas.\n");
	foreach (object ob in deep_inventory(this_player())) {
		if (real_filename(ob) == query_property("precio")) {
			tell_accion(environment(), this_player()->query_short()+" saca su "+ob->query_short()+" y lo entrega a "+query_short()+" que lo coge con profunda codicia.\n", "", ({this_player()}), 0);
			tell_object(this_player(), "Sacas tu "+ob->query_short()+" y se lo entregas con cierta reticencia a "+query_short()+" que lo coge con profunda avaricia.\n");
			if (ob->move(this_object())) destruct(ob); //Si falla el moverlo al menos lo destruimos y se lo quitamos al otro
			do_say("Así me gusta, jajaja",0);
			do_emote("está exultante de alegría con su nuevo juguete.");
			write_file(LOGS+"premios_dragon.txt", ctime()+" "+this_player()->query_name()+" le entregó "+ob->query_short()+".\n");
			abrir();
			return 1;
		}
	} 
	//Si llega aquí es que le han intentado tomar el pelo 
	do_say("¿Me estais intentando engañar? No tienes el pago que estipulamos, estoy cansado ya de esto.", 0);
	do_emote("se alza sobre sus cuartos traseros dispuesto a matar.");
	inicia_lucha();
	return 1;
}
status dame_ver_realmente() { return 1; }

int frases_personalizables(string tipo, object npc, object pj)
{
	switch (tipo) {
		case "saludo":
			pj->do_say("Hola infraser... soy "+npc->query_short(),0);
			break;
		case "bienvenida-pj":
			npc->do_emote("se posa sobre sus cuatro patas para dejar su rostro a al altura del tuyo y sonríe con mezquino desprecio.");
			npc->do_say("¿Qué puede hacer un humilde %^WHITE%^BOLD%^dragón%^RESET%^ como yo por tí?", 0);
			break;
		case "bienvenida-npc":
			break;
		case "despedida-pj":
			pj->do_say("¡Vaya!, pues nada, me espera otra eternidad de aburrimiento...", 0);
			break;
		case "despedida-npc":
			npc->do_emote("asiente lacónicamente con su gigantesca testuz dando por finalizada la cháchara.", 0);
			break;
		default:
			return 0;
	}

	return 1;
}

void inicia_lucha() {
	foreach(object ob in all_inventory(environment())) {
		if (living(ob) && ob != this_object()) attack_ob(ob);
	}
}


void inicia_trato(object jugador) {
	object * premios;
	object precio;

	if (jugador->dame_panteon() != "seldar") {
		do_say("¿Qué? Maldito siervo de la tontería ¿Cómo has osado llegar hasta aquí?",0);
		do_emote("se alza sobre sus cuartos traseros preparándose para lanzarse contra tí.");
		call_out("inicia_lucha", 2, "jugador");
	} else {
		if (jugador->dame_religion() == "gurthang") {	
			if (sizeof(all_inventory(environment())) > 2) {
				do_say("Sin duda, sois los adecuados, tener cuidado más adelante, no acudiré en vuestra ayuda aunque oiga vuestros gritos de agonía bajo mis propias fauces.",0);
			} else {
				do_say("Sin duda, eres el adecuado, ten cuidado más adelante, no acudiré en tu ayuda aunque te oiga agonizar bajo mis propias fauces.",0);
			}
			abrir();
		} else {//seldar, ozomatli, velians...
			do_say("Bueno, en realidad no debería permitiros cruzar pero... todo tiene un precio...", 0);
		 	if (sizeof(all_inventory(environment())) > 2) {
				do_emote("os mira uno por uno examinándoos con un brillo codicioso en la mirada.");
			} else {
				do_emote("te mira con tremenda avaricia deseoso por encontrar encontrar algo en tí que le resulte interesante.");
			}		
			premios = ({});
			foreach(object ob in deep_inventory(environment())) {	
				if (living(ob)) continue; //no queremos vivos, solo objetos
				if (ob->dame_valor() > 500*300) { //de 300 platis en adelante
					if (environment(ob) == this_object()) continue;//es un objeto que ya carga el dragon
					if (ob->query_property("propietario")) continue; //es una personal y no nos interesan
					premios = premios + ({ob});
				}
			}
			if (sizeof(premios) > 0) {
				do_emote("sonríe al descubrir que hay algo que le interesa.");
				precio = premios[random(sizeof(premios))];
				if (!living(environment(precio))) { //Está en una mochila o algo así
					do_say("Quiero "+precio->dame_articulo()+" "+precio->query_name()+" "+environment(precio)->dame_del()+" "+environment(precio)->query_name()+" de "+environment(environment(precio))->query_short()+", y ninguna otra cosa más. Entregarme lo que os pido y os abriré el camino.", 0);
				} else {
					do_say("Quiero "+precio->dame_articulo()+" "+precio->query_name()+" que lleva "+environment(precio)->query_short()+" y ninguna otra cosa más. Entregarme lo que os pido y os abriré el camino.", 0);
				}
				add_static_property("precio", real_filename(precio));
				do_emote("espera impaciente su pago.");
			} else {
				do_say("Bah... dejar de hacerme perder el tío, fuera de aquí sino me quereis ver cabreado.", 0);
			}
		}
	}
}		

mixed do_death(object asesino) { //Clonaremos el cofre de OBJETOS+"cofre_dragon.c" y escribimos log
	object cofre;
	tell_room(environment(this_object()), "Un %^BOLD%^YELLOW%^cofre%^RESET%^ cae pesadamente de entre los pliegues de las alas de "+query_short()+" al caer herido mortalmente.\n");
	if (!environment() || !cofre = entregar_objeto(OBJETOS+"cofre_dragon.c")) {
		tell_object(asesino, "No ha sido posible clonar el cofre. Repórtalo con el comando 'error' por favor.\n");
	} else {
		if (cofre->move(environment())) 
			tell_object(asesino, "No ha sido posible traer el cofre. Repórtalo con el comando 'error' por favor.\n");
	}
	//ya está el cofre en la room
	write_file(LOGS+"muertes_dragon.txt", (asesino?asesino->query_name():"No hay asesino")+" propinó a Uil'ahzn. El grupo era "+implode(dame_lista_atacantes(), ","));
	if (sizeof(dame_lista_perseguidos()) > 0) write_file(LOGS+"muertes_dragon.txt", " pero hizo huir a "+implode(dame_lista_perseguidos(), ",")+".\n");
	else write_file(LOGS+"muertes_dragon.txt", "\n");
	return ::do_death(asesino);
}	
