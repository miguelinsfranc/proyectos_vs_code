// Dunkelheit 26-09-2004
// Dunkelheit 07-04-2007 -- revisión
// Dunkelheit 05-11-2009 -- creando salteadores goblin a partir de mis queridos esclavos

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	set_name("picaro");
	set_short("Pícaro Osgo");
	set_long("Los osgos, también llamados trasgos gigantes y -en días remotos- \"bugbears\", son "
	"los primos mayores de los goblins. Físicamente guardan un gran parecido a ellos, salvo que "
	"son más altos y corpulentos. La mayor y más desagradable diferencia es la cantidad de pelo "
	"que brota hasta de los lugares más insospechados, escondiendo su piel de color aceituna. "
	"Este osgo, como la mayoría de su especie, es un oportunista ladrón con dotes respetables "
	"en el combate, lo que tradicionalmente se conoce como \"pícaro\".\n");
	set_main_plural("Pícaros Osgos");
	add_plural(({"picaros", "osgos"}));
	
	fijar_altura(140);
	fijar_peso_corporal(65000);
	
	fijar_raza("osgo");
	fijar_clase("ladron");
	fijar_religion("gurthang");
	
	fijar_fue(18);
	fijar_con(18);
	fijar_des(18);
	fijar_int(3);
	fijar_sab(3);
	fijar_car(3);
	
	fijar_nivel(31);
	fijar_alineamiento(4000);
	
	load_a_chat(50, ({
		":está tan alterado que se arranca pelo del pecho, ¡parece un simio gigante!",
		":alza su garrote y lo descarga amenazador en un amplio círculo.",
		"'¡Waaaaargh!",
	}));

	add_clone(BARMAS + "garrote", 1);
	add_clone(BARMAS + "puñal", 1);
	add_clone(BARMADURAS + "cuero", 1);
	
	fijar_maestria("garrote", 50);
	fijar_maestria("puñal", 50);
	
	init_equip();
	
	set_aggressive(1);
	
	ajustar_dinero(1 + random(2), "platino");
	ajustar_dinero(roll(2, 10), "oro");
	
	fijar_sigilar(1);
	
	ajustar_bo(125);
	
	fijar_pvs_max(2 * dame_pvs_max());
	fijar_pe_max(3 * dame_pe_max());

	add_attack_spell(50, "ataquedoble", 3);
	add_attack_spell(100, "rodear", 3);
}
