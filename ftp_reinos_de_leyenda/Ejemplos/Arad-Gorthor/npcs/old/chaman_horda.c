// Dunkelheit 22-01-2010

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	string raza = element_of(({"orco", "goblin", "semi-orco"}));
	
	set_name("chaman");
	set_short("Chamán de la Horda");
	set_main_plural("Chamanes de la Horda");
	add_plural(({"chamanes", "hordas"}));
	fijar_genero(1);

	fijar_altura(140 + random(41));
	fijar_peso_corporal(60000 + random(40000));

	fijar_raza(raza);
	fijar_clase("chaman");
	fijar_religion("gurthang");

	set_long("Observas a uno de los muchos y muy fanáticos chamanes de la Horda Negra, recién "
	"salido del horno de odio que es la Torre de Brujería de Arad Gorthor. Este "+raza+" ha "
	"estado desde que apenas tenía unos años de vida enclaustrado en dicha torre, sometido a "
	"un entrenamiento físico y mental al que sólo los más fuertes y astutos consiguen "
	"sobrevivir, pues las ambiciones que burbujean tras sus bloques de pizarra negra son "
	"dignas de la mismísima sociedad drow. Muchos de estos chamanes todavía no han participado "
	"en ninguna gran guerra, pero cuentan con innumerables víctimas a sus espaldas, algo de "
	"lo que no pueden presumir los soldados o los bárbaros que terminan los entrenamientos "
	"iniciales de la Horda.\n");
	
	fijar_fue(15);
	fijar_con(17);
	fijar_des(18);
	fijar_int(10);
	fijar_sab(18);
	fijar_car(14);
	
	fijar_nivel(30 + random(6));
	fijar_alineamiento(5000 + random(2000));
	
	add_clone(BARMAS + "vara", 1);
	add_clone(BARMADURAS + "amuleto", 1);
	add_clone(BARMADURAS + "tunica", 1);
	add_clone(BARMADURAS + "capucha", 1);
	
	if (!random(4)) {
		add_clone(BARMADURAS + "guante", 1);
		add_clone(BARMADURAS + "guante_izq", 1);
	}

	if (!random(4)) {
		add_clone(BARMADURAS + "cinturon", 1);
	}
	
	init_equip();
	
	ajustar_dinero(roll(1, 8), "estaño");
	
	ajustar_be(70);
	
	ROL_NPCS->definir(this_object());
	
	load_chat(15, ({
	}));
	
	load_a_chat(50, ({
	}));
	
	add_attack_spell(40, "mordisco de la vibora", 3);
}

string dame_noconversar_personalizado()
{
	return "Cuando ve que intentas darle conversación, el chamán se apresura a iniciar unas oraciones "
	"antes de que puedas terminar de decir \"hola\". Gracias a tu inteligencia sin parangón, deduces "
	"que no tiene el más mínimo interés en hablar contigo.\n";
}
