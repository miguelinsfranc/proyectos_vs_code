//Rezzah 13/02/2011
#include "../path.h";
inherit "/obj/montura";
//Un cachorro huargo, una montura normal pero empieza siendo demasiado joven para ser montado
//no puede andar solo, hay que cogerlo y alimentarlo para que suba niveles
//si se alimenta sólo con cabezas de eralie, hiros o ralder se convertirá en una criatura más poderosa
void debug(mixed str) {
	tell_object(find_player("rezzah"), "\n%^CYAN%^BOLD%^"+str+"%^RESET%^\n");
}
void setup()
{
	set_name("huargo");
	add_alias(({"huargo","wargo", "cachorro"}));
	add_plural(({"huargos","wargos", "cachorros"}));
	set_short("Huargo joven");
	set_main_plural("Huargos jóvenes");
	set_long("Un huargo joven, de músculos incipientes y actitud agresiva. Su suave pelaje está mudando "
		"por otro más duro dándole un aspecto lamentable. En sus ojos ambarinos se ve el ansía por la caza "
		"latir con fuerza.\n");
	fijar_genero(random(2)+1);
	fijar_raza("mamifero");
	set_random_stats(13,18);
	//Los stats siempre antes de fijar el tipo de montura, la inteligencia y la fuerza
	//son preestablecidas según el tipo de montura
	fijar_tipo_montura("huargo");
	set_level(5);
	set_wimpy(40);//es joven y asustadizo
}

void init() {
	::init();
	add_action("alimentar_criatura", "alimentar");
}

int iniciar_montar(string que)
{
    if(que && !id(que) && !id_plural(que))
        return 0;
 	return notify_fail(query_short()+" todavía es demasiado pequeño para ser montado.\n");
}

int alimentar_criatura(string txt) {
	string * pals;
	object alimento;

	if (!txt) {
		return notify_fail("Para alimentar tu cachorro pon: 'alimentar cachorro con <alimento>'.\n");
    }
	pals = explode(txt, " ");
	if (sizeof(pals) != 3) { //La sintaxis es alimentar [huargo|cachorro] con [alimento]
		return notify_fail("Para alimentar tu cachorro pon: 'alimentar cachorro con <alimento>'.\n");
	}
	if (pals[0] != "huargo" && pals[0] != "cachorro") { 
		return notify_fail("Para alimentar tu cachorro pon: 'alimentar cachorro con <alimento>'.\n");
    }
	if (pals[1] != "con") {
		return notify_fail("Para alimentar tu cachorro pon: 'alimentar cachorro con <alimento>'.\n");
    }
	//Todo es correcto, hay que revisar el inventario buscando el item
	alimento = find_match(pals[2], this_player());
	if (!alimento) {
		return notify_fail("No encuentras "+pals[2]+" para alimentarle.\n");
	}

	return notify_fail(query_short()+" ya es capaz de alimentarse por sí mismo, ahora lo mejor es entrenarlo cazando y luchando.\n");
}

int ajustar_xp(int i) {
	object nuevo, amo;
	int xp;
	xp = ::ajustar_xp(i);
	if (dame_nivel() >= 10) {
//A nivel 10 evoluciona en huargo, si ha sido alimentado únicamente con cabezas no tendrá la property mal_alimentado y se convierte en un huargo endemoniado
		amo = find_player(dame_amo());
		if (!amo) {
			return xp;//Ya lo haremos cuando esté el amo conectado
		}
		if (environment(amo) != environment() ) {
			return xp;//Ya lo haremos cuando el amo esté delante
		}
		if (query_property("mal_alimentado")) {//El nuevo no es endemoniado
			nuevo = clone_object("/baseobs/monturas/huargo.c");
			nuevo->fijar_amo(this_player()->query_name());
			nuevo->fijar_nivel(dame_nivel());
			nuevo->fijar_xp(dame_xp());
			nuevo->fijar_genero(dame_genero());
		} else {
			nuevo = clone_object(NPCS+"huargo_endemoniado.c");
            nuevo->fijar_amo(this_player()->query_name());
            nuevo->fijar_nivel(dame_nivel());
            nuevo->fijar_xp(dame_xp());
			nuevo->fijar_genero(dame_genero());
		}

		if (nuevo->move(environment(amo))) 
			return notify_fail("Hubo un error en la evolucion de tu huargo. Por favor, notifícalo al consejo de jugadores.\n");
		destruct(this_object());
	}	
	return xp;
}
