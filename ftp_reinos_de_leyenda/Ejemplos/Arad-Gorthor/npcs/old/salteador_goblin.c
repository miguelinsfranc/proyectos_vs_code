// Dunkelheit 26-09-2004
// Dunkelheit 07-04-2007 -- revisión
// Dunkelheit 05-11-2009 -- creando salteadores goblin a partir de mis queridos esclavos
// Dherion Marzo 2017 quitando abhrojos y haciendo que sigilen.


#include "../path.h"
inherit "/obj/monster.c";

#define SHADOW_ESCONDERSE "/habilidades/shadows/esconderse_sh.c"

void setup()
{
    set_name("salteador");
    set_short("Salteador Goblin");
    set_long("Un escuálido y aparentemente desesperado goblin que merodea por los inhóspitos y despoblados "
      "caminos de Arad Gorthor en busca de víctimas a las que robar y, si procede, comerse. Las penurias que "
      "estos desertores de la Horda Negra sufren les impulsan a atacar a todo ser viviente que se cruce por "
      "su camino, sea enemigo o aliado.\n");
    set_main_plural("Salteadores Goblin");
    generar_alias_y_plurales();
    fijar_sigilar(1);
    set_move_after(20,10);

    fijar_altura(80);
    fijar_peso_corporal(40000);

    fijar_raza("goblin");
    fijar_clase("ladron");
    fijar_religion("gurthang");

    fijar_fue(16 + !random(4));
    fijar_con(17);
    fijar_des(18);
    fijar_int(4);
    fijar_sab(4);
    fijar_car(3);

    fijar_nivel(14 + random(4) + !random(10));
    fijar_alineamiento(8500);

    load_chat(10, ({
        ":permanece agazapado al borde de la senda, esperando que pase algún incauto que emboscar.",
        ":relame sus resquebrajados labios. ¡El calor en estos caminos es insoportable!",
      }));

    load_a_chat(50, ({
        "'¡Dame todo lo que tengas, zoquete!",
        ":agita su puñal mientras berrea, intentando asustarte.",
        "'¡¿Nunca te han dicho que es peligroso andar por estos caminos?!",
        "'¿Ves lo que me obligas a hacerte? ¡cualquier cosa es mejor antes que morir en las filas de la Horda!",
      }));

    switch (random(4)) {
    case 0:
        add_clone(BARMAS + "daga", 1);
        fijar_maestria("daga", 10+random(11));
        break;
    case 1:
        add_clone(BARMAS + "puñal", 1);
        fijar_maestria("puñal", 10+random(11));
        break;
    case 2:
        add_clone(BARMAS + "estilete", 1);
        fijar_maestria("estilete", 10+random(11));
        break;
    case 3:
        add_clone(BARMAS + "cuchillo", 1);
        fijar_maestria("cuchillo", 10+random(11));
        break;
    }

    add_clone(BARMADURAS + "zapatillas", 1);
    add_clone(BARMADURAS + "pantalones", 1);

    if (!random(5)) {
        add_clone(BARMADURAS + "cuero", 1);
    }

    init_equip();

    set_aggressive(1);
    set_join_fight_mess("emboscada");

    if (!random(10)) {
        ajustar_dinero(1 + random(2), "oro");
    } 
    ajustar_dinero(roll(2, 10), "plata");

    fijar_sigilar(1);
    fijar_cobardia(10);
}

void escondete()
{
    if ( TO->dame_shadow_esconderse() ) {
        return;
    }

    clone_object(SHADOW_ESCONDERSE)->setup_sombra(this_object(),60,0);
    set_hidden(1);
}

void emboscada(object victima)
{
    if (query_static_property("emboscada_iniciada")) {
        return;
    }
    add_static_property("emboscada_iniciada", 1);

    if (random(2)) {
        do_say("¡No escaparás de este camino con vida, "+lower_case(victima->dame_nombre_raza())+"!", 0);
    } else {
        do_say("¡Dame todo lo que lleves encima, miserable "+lower_case(victima->dame_nombre_raza())+"!", 0);
    }

}
