// Dherion 24/06/2017; Arquero

#include "../path.h";
inherit "/obj/monster.c";
void setup()
{
    set_name("arquero herido");
    set_short("Arquero herido");
    set_long("Este hombre se encuentra casi en los huesos, señal de que hace días que no tiene una buena comida. sus ropajes están manchados de sangre seca y su rostro presenta varios cortes y magulladuras. Descansa sentado en la arena.\n");
    set_main_plural("arqueros heridos");
    generar_alias_y_plurales();
    fijar_altura(120);
    fijar_peso_corporal(70000);
    fijar_raza("humano");
    fijar_clase("tirador");
    fijar_fue(20 + !random(4));
    fijar_con(20);
    fijar_des(20);
    fijar_int(4);
    fijar_sab(4);
    fijar_car(5);
    fijar_nivel(15);
    fijar_alineamiento(-8500);
    load_chat(10, ({
        ":te mira con miedo",
        "'¿Has venido a matarme?",
        "'Ayúdame, por favor. Esos orcos me están buscando para matarme",
      }));
    load_a_chat(50, ({
        "'Estoy herido, ¡pero todavía puedo disparar!",
        ":Te apunta con su arma buscando un tiro perfecto",
        "'¡Te arrepentirás de esto!",
        "'¡Nunca lograrás matarme!",
      }));
    add_clone("/baseobs/proyectiles/arco", 1);
    fijar_maestria("arco", 50+random(11));
    add_clone(BARMADURAS + "zapatillas", 1);
    add_clone(BARMADURAS + "pantalones", 1);
    add_clone(BARMADURAS + "cuero", 1);
    init_equip();
    add_attack_spell(40,"disparocertero",3);
    ajustar_dinero(roll(4, 10), "plata");
}
// Enmascaro la función do_death para la misión
int do_death(object asesino)
{
    if(asesino->dame_mision_abierta("arad_arquero_escurridizo"))
    {
        asesino->nuevo_hito_mision("arad_arquero_escurridizo","arquero_muerto",1);
        tell_object(asesino,"tu último golpe termina con la vida del arquero. ¡Una molestia menos!\n");
    }
    return ::do_death(asesino);
}
