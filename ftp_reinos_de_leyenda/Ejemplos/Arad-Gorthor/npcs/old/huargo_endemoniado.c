//Rezzah 13/02/2011
#include "../path.h";
inherit "/obj/montura";
//Un joven huargo, una montura normal pero empieza siendo demasiado joven para ser montado
//no puede andar solo, hay que cogerlo y alimentarlo para que suba niveles
//si se alimenta sólo con cabezas de eralie, hiros o ralder se convertirá en una criatura más poderosa
void debug(mixed str) {
	tell_object(find_player("rezzah"), "\n%^CYAN%^BOLD%^"+str+"%^RESET%^\n");
}

void setup() {
	set_name("huargo");
	add_alias(({"huargo","wargo", "endemoniado"}));
	add_plural(({"huargos","wargos", "endemoniados"}));
	set_short("Huargo endemoniado");
	set_main_plural("Huargos endemoniados");
	set_long("Esta criatura de aspecto lobuno parece nacida en el mismo infierno. "
		"Sus ojos están desencajados y la mandíbula, entreabierta, chorrea saliva y sangre. "
		"Su cuerpo descomunal es musculoso, mucho más que la mayoría de su congéneres y "
		"junto al resto de su apariencia, le dan un aspecto horroroso y amenazador.\n");
	fijar_genero(random(2)+1);
	fijar_raza("mamifero");
	set_random_stats(18,20);
	//Los stats siempre antes de fijar el tipo de montura, la inteligencia y la fuerza
	//son preestablecidas según el tipo de montura
	fijar_tipo_montura("huargo");
	fijar_nivel(10);
}

void heart_beat() {
	object objetivo;

	::heart_beat();
	//Cada 60 segundos intenta lanzar un ataque
	if (dame_peleando() > 0 && !query_timed_property("lanzo_ataque")) {//Está peleando y no tiene bloqueo
		add_timed_property("lanzo_ataque",1,60);
		objetivo = element_of(dame_lista_atacantes());
		habilidad("mordisco", objetivo);	
	} else {
		if (dame_peleando() < 0 && !query_timed_property("lanzo_aullido")) { //Se le escapan?
			add_timed_property("lanzo_aullido", 1, 7200);
			tell_accion(query_short()+" aúlla salvajemente haciéndote temblar por su fiereza por los enemigos escapados.\n");
		} else { 
			if (!random(20)) tell_accion(query_short()+" gruñe con el deseo terrible de matar.\n");
		}
	}
}
