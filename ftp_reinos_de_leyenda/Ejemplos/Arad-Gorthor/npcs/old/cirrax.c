// Dunkelheit 22-11-2009
// Liberar ratones y reconducirlos hasta algun lado para asustar a alguien LOL XD

#include "../path.h";
inherit "/obj/conversar_npc.c";

void setup()
{
	set_name("cirrax");
	set_short("Cirrax");
	set_long("¡Enhorabuena! Has vivido lo suficiente como para ver un goblin con "
	"anteojos. Y por su aspecto pausado y la forma que tiene de mirarte, dirías que "
	"hasta tiene algo más que inteligencia... ¡sentido común! Pero, ¿tal vez no sea "
	"esto una mera aparencia, consecuencia de su profesión? Pues Cirrax es un "
	"hombre de negocios con mucha solera, que advirtió la necesidad de las poblaciones "
	"goblinoides no sólo en el campo del \"alcohol a mansalva\", sino también en el "
	"de \"bebidas elitistas de importación para orcos pretenciosos\". Este goblin de "
	"medianas edad y estatura permanece sentado gran parte del día tras el mostrador "
	"de madera de su negocio, enriqueciéndose a costa incluso de enemigos que se "
	"arrastran hasta su negocio, saltándose todas las vías diplomáticas.\n");
	set_main_plural("Cirrax");
	add_plural(({"cirraxes", "cirraxs"}));
	
	fijar_altura(142);
	fijar_peso_corporal(53200);
	
	fijar_raza("goblin");
	fijar_clase("mago_negro");
	fijar_religion("gurthang");
	
	fijar_fue(3);
	fijar_con(18);
	fijar_des(18);
	fijar_int(18);
	fijar_sab(15);
	fijar_car(9);
	
	fijar_nivel(42);
	fijar_alineamiento(12800);
	
	ajustar_dinero(roll(1, 4), "platino");
	
	ROL_NPCS->definir(this_object());
	
	add_clone(BARMAS + "vara", 1);
	add_clone(BARMADURAS + "capa", 1);
	add_clone(BARMADURAS + "cinturon", 1);
	add_clone(BARMADURAS + "sandalias", 1);
	init_equip();
	
	load_chat(30, ({
		":destapa un frasquito, todo él de cristal, y pasea el tapón por debajo de su nariz aguileña, embriagánose con los perfumes etílicos del mismo.",
		"'Aquí podrás encontrar los caldos alcohólicos más exquisitos, dignos del mayor de los gourmets. Pero... ¿crees que podrás pagarlos? ¡jaja!",
		":baja sus anteojos y te examina de abajo a arriba. No parece tomarte muy en serio.",
		"'Conozco a más de un enano que traicionaría a su sucio dios Eralie con tal de llevarse una gota de mis elixires a los labios... ¡menuda panda de borrachos!",
		"'No dejes de probar el exquisito Áspex que importo de Golthur Orod",
	}));
	
	load_a_chat(50, ({
		"'¡¡¡Hay que ver lo que sois capaces de hacer los borrachos por un sorbo de alcohol!!!",
		"'¡Guardias, guardias! ¡Intentan atracar mi licorería!",
	}));
	
	fijar_pvs_max(5250);
	fijar_pe_max(3500);
	
	nuevo_dialogo("licores", ({
		":sonríe con orgullo. ¡Menudo vanidoso!",
		"'Son el objeto de deseo de los mundanos que deambulan por los suburbios",
		"'¡Nada que ver con vulgares sopas como la cerveza o el vino!",
		"'Aunque de estos dos también tengo botellas importadas, de incalculable valor",
		"'¿Quieres saber algo más acerca mis licores más exóticos?",
	}));
	
	nuevo_dialogo("gluhwein", ({
		"'¡¡¡No, no, no!!!",
		"'¡No se pronuncia así!",
		"'¡Se pronuncia glühwein!",
		"'No sé qué tengo que hacer para %^CURSIVA%^llegar%^RESET%^ al vulgo...",
		"'El glühwein es un vino especiado",
		"'Tanto el vino como la calidad y origen de las especias son un secreto que jamás te desvelaré",
		"'Te diré que no vale cualquier vino -ni cualquier especia- para producir un buen glühwein",
		"'Normalmente se toma caliente en enormes vasijas de arcilla",
		"'Pero no estamos en los valles neblinosos... sino en el desierto",
		"'Así que la gente se lo toma tal cual",
		"'Lo cual, a mi entender, es una aberración",
	}), "licores");
	nuevo_dialogo("aspex", ({
		"'No me tomes el pelo, ¿cómo no vas a saber lo que es el áspex?",
		"'¡Es el licor más famoso de Golthur Orod! ¡la bebida oficial del reino!",
		"'Es tan fuerte que hasta lo puedes usar para quitar el óxido de tus armas",
		"'¡Imagínate lo que hará en tu estómago!",
	}), "licores");
	nuevo_dialogo("grog", ({
		"'No es una bebida muy común a estas latitudes...",
		"'Proviene de allende los mares, de ambientes húmedos y tropicales",
		"'Los mejores están destilados en la Isla del Bucanero",
		"'Es todo un rival para el Áspex en lo que a capacidad de limpiar metales se refiere",
	}), "licores");

	habilitar_conversacion();	
	
	add_attack_spell(50, "defenestrar", 3);
	add_attack_spell(50, "proyectil magico mayor", 3);
	add_attack_spell(50, "cono de frio", 3);
	add_attack_spell(100, "llamarada electrica", 4);
}

int frases_personalizables(string tipo, object npc, object pj)
{
	switch (tipo)
	{
		case "bienvenida-pj":
			if (HORDA_NEGRA->es_oficial(pj)) {
				npc->do_emote("esboza una sonrisa forzada.");
				npc->do_say("¡Salve, "+HORDA_NEGRA->dame_titulo_corto(pj)+"! ¡mis preciados caldos están a su entera disposición!", 0);
			} else {
				tell_object(pj, "Te acercas a la encorvada figura de "+npc->query_short()+", quien te escudriña por encima de sus anteojos.\n");
				npc->do_say("Dime, ¿de qué manera podrías hacerme rico?", 0);
			}
			break;
		case "bienvenida-npc":
			break;
		case "despedida-pj":
			pj->do_say("He de marcharme, gracias por tu conversación", 0);
			break;
		case "despedida-npc":
			npc->do_say("Sí, sí... muy bien", 0);
			npc->do_emote(":vuelve a incrustarse los anteojos en la cara.");
			break;
		default:
			return 0;
	}

	return 1;
}
