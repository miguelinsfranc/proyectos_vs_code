// Dunkelheit 27-10-2009

#include "../path.h";
inherit "/obj/conversar_npc";

#include "../include/inteligencia.h"

// Para que nunca se quede sin heartbeat
int check_anyone_here() { return 1; }

void setup()
{
	set_name("morkûm");
	add_alias(({"morkum", "thule"}));
	if (HORDA_NEGRA->dame_caudillo() == "morkûm") {
		set_short("Caudillo Morkûm Thule");
		set_main_plural("Caudillos Morkûm Thule");
		add_alias("caudillo");
	} else {
		set_short("Mariscal Morkûm Thule");
		set_main_plural("Mariscales Morkûm Thule");
		add_alias("mariscal");
	}
	add_plural(({"morkums", "morkûms"}));
	set_long("Morkûm Thule es un uruk que pese a su tamaño atroz no tendría "
	"piel suficiente para colgar las incontables condecoraciones que ha ido "
	"ganando desde su ingreso en el antiguo Ejército Negro, allá por la Era 3ª. "
	"Este magnífico estratega destacó desde sus inicios por su mentalidad "
	"calmada y sagaz, lo cual le propulsó hasta las más altas esferas del "
	"ejército, que buscaba ese tipo de cualidades frente a la habitual "
	"brutalidad descerebrada de los golthur-hai. En este metro noventa de "
	"carne fibrosa, tallada con todo tipo de cicatrices de guerra y venas "
	"a punto de explotar, se empotran dos brazos como troncos de árbol que "
	"sostienen su arma predilecta, la legendaria Diezmadora. Su tronco "
	"desproporcionado y sus potentes piernas hacen que su figura se asemeje "
	"a la de una avispa.\n");
	
	fijar_subraza("uruk-hai");
	fijar_clase("gragbadûr");
	fijar_religion("gurthang");
	fijar_ciudadania("golthur", 999);
	fijar_gremio("horda_negra");
	
	fijar_altura(189);
	fijar_peso_corporal(115000);
	
	fijar_fue(23);
	fijar_extrema(100);
	fijar_con(19);
	fijar_des(18);
	fijar_int(17);
	fijar_sab(15);
	fijar_car(19);
	
	fijar_nivel(80);
	
	fijar_pvs_max(49000);
	fijar_pe_max(31000);
	
	ajustar_maestria("cimitarra", 100);
	ajustar_maestria("cimitarra larga", 100);

	add_clone(ARMAS+"diezmadora", 1);
	add_clone(BARMADURAS+"anillo.c",1);
	add_clone(BARMADURAS+"amuleto.c",1);
	add_clone(BARMADURAS+"pendiente.c",1);
	add_clone(BARMADURAS+"collar.c",1);
	add_clone(BARMADURAS+"talabarte.c",1);
	add_clone(ARMADURAS+"nudillos_mariscal", 1);
	add_clone(BARMADURAS+"guantelete_izq.c",1);
	add_clone(BARMADURAS+"brazal.c",1);
	add_clone(BARMADURAS+"cinturon.c",1);
	add_clone(BARMADURAS+"vaina.c",1);
	add_clone(BARMADURAS+"brazal_izq.c",1);
	add_clone(BARMADURAS+"grebas_metalicas.c",1);
	add_clone(BARMADURAS+"completa.c",1);
	add_clone(BARMADURAS+"gran_yelmo.c",1);
	add_clone("/d/golthur/items/botas_punzantes.c",1);
	init_equip();
	
	add_static_property("no_desarmar", 1);
	
	habilitar_conversacion();
	
	load_a_chat(35, ({
		":se dispone a golpear a un enemigo, pero todo resultó ser un amago.",
		":alza su Diezmadora por encima de la cabeza y vira violentamente.",
		":retrocede unos pasos para cambiar su arma de mano.",
		":se ríe a carcajadas, realmente parece disfrutar del combate.",
		"'Te lo aviso, ¡no me iré sólo a la tumba!",
	}));
	
	nuevo_dialogo("thule", ({
	}));
}

void bateria_basica(object quien)
{
	if (puedo_ejecutar("cansado_de_golpear")) {
		habilidad("golpear", quien);
	} else if (!quien->query_time_remaining("jugarreta-recien-sufrida") && puedo_ejecutar("jugarreta-recien-ejecutada")) {
		habilidad("jugarreta", quien);
	} else if (puedo_ejecutar("ansia_de_sangre")) {
		habilidad("ansia", this_object());
	} else if (puedo_ejecutar("bloqueo-Tajar")) {
		habilidad("tajar", quien);
	}
}

void event_enter(object quien, string mensaje, object procedencia, object *seguidores)
{
	::event_enter(quien, mensaje, procedencia, seguidores);
	if (-1 != member_array(quien, query_call_outed() + query_attacker_list())) {
		if (random(2) && puedo_ejecutar("cansado_de_golpear")) {
			habilidad("golpear", quien);
			if (!random(10)) {
				::heart_beat();
			}
		}
	}
}

void heart_beat()
{
	object quien, candidato;
	
	if (dame_manos_libres() == 1) {
		do_cmd("mano");
	}

	// Comportamiento normal con enemigos
	if (tengo_enemigos()) {
		quien = victima_limpia();
		if (candidato = rival_mas_debil(busca_afectados("retenido"))) {
			quien = candidato;
		}
		bateria_basica(quien);

	} else if (hay_peligro() && puedo_ejecutar("ansia_de_sangre")) {
		habilidad("ansia", this_object());
	} 

	::heart_beat();
}

int frases_personalizables(string tipo, object npc, object pj)
{
	status caudillo, hierofante, arconte, oficial, paleto;
	
	if (HORDA_NEGRA->es_caudillo(pj)) {
		caudillo = 1;
	} else if (HORDA_NEGRA->es_hierofante(pj)) {
		hierofante = 1;
	} else if (HORDA_NEGRA->es_arconte(pj)) {
		arconte = 1;
	} else if (HORDA_NEGRA->es_oficial(pj)) {
		oficial = 1;
	} else {
		paleto = 1;
	}

	switch (tipo)
	{
		case "despedida-pj":
			if (paleto) {
				tell_object(pj, "Morkûm Thule te hace un gesto obsceno y te manda salir de la estancia.\n");
			} else {
				tell_object(pj, "Morkûm Thule te saluda militarmente.\n");
			}
			break;
		case "despedida-npc":
			if (paleto) {
				tell_object(pj, "¡Desaparece de mi vista, snaga!\n");
			} else if (oficial) {
				tell_object(pj, "¡Ashdautas vrasubatlat!\n");
			} else {
				tell_object(pj, "¡Salve, Caudill"+pj->dame_vocal()+"! ¡ashdautas vrasubatlat!\n");
			}
			break;
		case "noentiende-pj":
			if (paleto) {
				do_emote("no entiende el idioma en que le estás hablando, lo cual no parece hacerle mucha gracia.");
			} else {
				do_emote("frunce el entrecejo intentando entender tus palabras.");
			}
			break;
		case "noentiende-npc":
			if (paleto) {
				this_object()->attack_ob(npc);
				npc->attack_by(this_object());
			} else if (oficial) {
				do_say("¡No me hables en ese sucio lenguaje!", 0);
			} else {
				do_say("Le ruego que uses nuestra gloriosa lengua negra para poder entendernos mejor...", 0);
			}
			break;
		case "bienvenida-pj":
			tell_object(pj, "Saludas militarmente a Morkûm Thule.\n");
			break;
		case "bienvenida-npc":
			if (paleto) {
				do_say("¡¿Qué haces aquí, snaga?!", 0);
			} else if (oficial) {
				do_say("¡Saludos, oficial "+pj->dame_nombre_completo(2)+"!", 0);
			} else if (arconte) {
				do_say("¡Salve, Arconte! ¡Ashdautas vrasubatlat! ¿cuántos enemigos han probado hoy el filo de tu espada?", 0);
			} else if (hierofante) {
				do_say("¡Salve, Hierofante! ¡Ashdautas vrasubatlat! ¿qué malévolos conjuros habéis urdido hoy?", 0);
			} else if (caudillo) {
				do_say("¡Salve, Caudill"+pj->dame_vocal()+"! ¡¡¡mi Diezmadora está siempre afilada y preparada para servirle en la batalla!!!", 0);
			}
			break;
	}
	return 1;
}
