// Dunkelheit 15-11-2009

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	set_name("hobgoblin");
	set_short("Campeón Hobgoblin");
	add_alias("campeon");
	set_long("Es el más temible guerrero del menguado ejército hobgoblin. Los campeones tienen casi el tamaño y la "
	"corpulencia de un osgo, y portan orgullosos una cantidad semejante de vello corporal, lo cual les da un aspecto "
	"todavía más amenazador. Los méritos que le han convertido en campeón no son sólo físicos: su fiereza en combate "
	"no tiene parangón, y su dominio de la lanza es mortífero en zonas estrechas como las cavernas en que habitan. "
	"Con este historial es fácil que hayan podido conservar un yelmo y una cota de mallas durante tanto tiempo: los "
	"hobgoblins son muy envidiosos, y matarían a su madre por un equipo de este calibre.\n");
	set_main_plural("Campeones Hobgoblins");
	add_plural(({"campeones", "hobgoblins"}));
	
	fijar_altura(150);
	fijar_peso_corporal(70000);
	
	fijar_subraza("hobgoblin");
	fijar_clase("soldado");
	fijar_religion("gurthang");
	
	fijar_fue(18);
	fijar_extrema(100);
	fijar_con(18);
	fijar_des(15);
	fijar_int(9);
	fijar_sab(5);
	fijar_car(5);
	
	fijar_nivel(30);
	fijar_alineamiento(4000);
	
	set_aggressive(1);
	
	add_clone(BARMAS + "lanza", 1);
	add_clone(BARMADURAS + "mallas", 1);
	add_clone(BARMADURAS + "yelmo", 1);
	add_clone(BARMADURAS + "botas", 1);
	init_equip();
	
	fijar_maestria("lanza", 51);
		
	load_a_chat(30, ({
		":se revuelve como un perro rabioso.",
		":ataca con su lanza con mortífera precisión.",
		":hace gala en combate de su poderosa musculatura y su agilidad.",
		"'¡Dejádmelo a mí, yo acabaré con este infeliz!",
		"'¡La curiosidad te llevará a la tumba, intruso!",
		"'¡Ahora podrás medirte con alguien de tu tamaño!",
		"'¡Jamás he perdido una batalla, y hoy no será ese día!",
	}));
	
	ajustar_dinero(roll(2, 3), "platino");
}
