// Dunkelheit 10-11-2009
/*
*	Zoilder - 24/03/2011: Retoques de misión hobgoblins.
*		- Ahora no atacará a todos los jugadores que no sean gurthang, para dar la posibilidad a obtener el hito a cualquier jugador,
*			requiriendo solamente que sepan los idiomas correctos (los de seldars)
*/

#include "../path.h";
inherit "/obj/conversar_npc.c";

void setup()
{
	set_name("glotrub");
	set_short("Glotrub Guardabasuras");
	add_alias(({"orco", "guardabasuras"}));
	set_long("¿Por qué gastar material construyendo una muralla cuando puedes poner al "
	"orco más obeso de la fortaleza para taponar la entrada? Eso debieron pensar los "
	"superiores de Glotrub cuando se les preguntó cómo proteger el vertedero de Golthur "
	"Orod. Pero, ¿de quién hay que proteger tantas toneladas de basura? ¿acaso fue una "
	"estratagema para deshacerse de este oblongo estorbo? Sea como fuere, Glotrub fue "
	"destinado a una garita -en la que irónicamente no cabe- para impedir el paso de "
	"curiosos al vertedero.\n");
	set_main_plural("Glotrubs Guardabasuras");
	add_plural("orcos");
	
	fijar_altura(165);
	fijar_peso_corporal(350000);
	
	fijar_raza("orco");
	fijar_clase("soldado");
	fijar_religion("gurthang");
	fijar_ciudadania("golthur");
	
	fijar_fue(18);
	fijar_extrema(100);
	fijar_con(18);
	fijar_des(6);
	fijar_int(13);
	fijar_sab(3);
	fijar_car(3);
	
	fijar_nivel(35);
	fijar_alineamiento(3500);
	
	load_chat(50, ({
		"'Nnnnghhh, ¿me has traído algo de comer? ¡da igual, tú serás mi aperitivo!",
		":lucha denonadadamente por respirar.",
		":rebaña el sudor que hay entre las docenas de pliegues de su piel.",
		"'En Golthur Orod nadie tira la comida, yo la filtro antes de que llegue al vertedero, ¡hahaha!",
	}));
	
	load_a_chat(50, ({
		"'Esas piernecillas escuálidas... ¡ME LAS COMERÉ!",
		"'Esos ojitos crujientes... ¡ME LOS COMERÉ!",
		"'Esa cabeza rellena de serrín... ¡ME LA COMERÉ!",
	}));

	add_clone(BARMAS + "alabarda", 1);
	
	add_clone(BARMADURAS + "varillas", 1);
	add_clone(BARMADURAS + "yelmo", 1);
	add_clone(BARMADURAS + "brazal", 1);
	add_clone(BARMADURAS + "brazal_izq", 1);
	add_clone(BARMADURAS + "guantelete", 1);
	add_clone(BARMADURAS + "guantelete_izq", 1);
	add_clone(BARMADURAS + "botas_campaña", 1);

	init_equip();
	
	fijar_maestria("alabarda", 58);
	
	add_attack_spell(10, "tajar", 3);
	add_attack_spell(50, "golpecertero", 3);
	
	//set_aggressive(1);
	fijar_perfil_lenguajes("seldar");
	fijar_lenguaje_actual("negra");
	
	adjust_tmp_damage_bon(100);

	ajustar_dinero(roll(2, 10), "plata");
	
	fijar_pvs_max(15000);
	
	//ROL_NPCS->definir(this_object());
	
	nuevo_dialogo("vertedero", ({
		"'Es muy importante vigilar el vertedero...",
		"'Hay muchos interés depositado en él, ¿sabes?",
		"'Y también hay mucha gente... interesada en él",
		"'Por eso hay que protegerlo",
	}));
	nuevo_dialogo("gente", ({
		"'Sí, ya sabes... quieren acceder a él",
		"'A sus secretos",
		"'Por eso me eligieron a mí entre cientos de aspirantes",
		"'Este es un puesto de gran responsabilidad",
	}), "vertedero");
	nuevo_dialogo("secretos", ({
		"'Uhmmm... nnnno puedo decírtelos",
		":se rasca nervioso una oreja.",
		"'¡Pero que sepas que los hay!",
		":suspira tristemente.",
		"'¡Baaaah! ¡a quién quiero engañar!",
		"'¡Esta misión apesta!",
		"'Pero por lo menos puedo comerme toda la comida que llega aquí, ¡hahaha!",
	}), "gente");
	nuevo_dialogo("mapa", ({
		"'¡¿Uh?! ¿un mapa de los hobgoblins dice que hay algo en mi vertedero?",
		":alza su alabarda en posición de combate, visiblemente enfurecido. Parece que le has pillado.",
		"mamporrazo"
	}));
	prohibir("mapa", "funcion_mapa");
	habilitar_conversacion();
}

void mamporrazo(object quien)
{
	abandonar_conversacion(quien, "¡Me parece que sabes demasiado! ¡¡¡TE COMERÉ!!!");
	deshabilitar_conversacion();
	
	if (!quien->dame_hito_mision("hobgoblins", "descubrir-glotrub")) {
		quien->nuevo_hito_mision("hobgoblins", "descubrir-glotrub", 1);
	}

	quien->attack_by(this_object());
	attack_ob(quien);
	habilidad("tajar", quien);
}

int funcion_mapa(object quien)
{
	if (quien->dame_hito_mision("hobgoblins", "borrar-altar-piedra")) {
		return 1;
	}
	return 0;
}

int frases_personalizables(string tipo, object npc, object pj)
{
	switch (tipo)
	{
		case "despedida-pj":
			pj->do_say("Bueno, tengo que irme, buena suerte vigilando estas montañas de basura.", 0);
			break;
		case "despedida-npc":
			npc->do_say("Eso es, déjame seguir tranquilo con mi misión.", 0);
			tell_accion(environment(npc), npc->query_short()+" se lleva un trozo de carne podrida a la boca y lo mastica ruidosamente.\n", "Escuchas a alguien masticar.\n", ({ npc }), npc);
			break;
		default:
			return 0;
	}

	return 1;
}
