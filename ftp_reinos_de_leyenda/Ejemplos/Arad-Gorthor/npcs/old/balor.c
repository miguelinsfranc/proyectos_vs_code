// Dunkelheit 18-12-2009

#include "../path.h";
inherit "/obj/monster.c";

#include "../include/inteligencia.h"

int dame_guardia() { return 1; }

int check_anyone_here() { return 1; } // Para no perder el HB

void setup()
{
	set_name("balor");
	set_short("%^RED%^BOLD%^Demonio Balor%^RESET%^");
	add_alias("demonio");
	set_long("No tienes ni idea de qué clase de pacto han hecho los señores de Arad-Gorthor "
	"con esta formidable criatura del Abismo para merecer sus favores pero no cabe duda de que "
	"hay mucha sangre por medio, tanta como poder emana de este demonio de casi tres metros de "
	"hediondo pelaje y ojos desbordantes de odio, coronados por una crin encrespada que recorre "
	"toda su espalda. A tal tamaño hay que sumarle sus alas membranosas que, a pesar de estar "
	"recogidas, aumentan su altura en más de medio metro.\n");
	set_main_plural("%^RED%^BOLD%^Demonios Balor%^RESET%^");
	add_plural(({"demonios", "balors", "balores", "guardia_arad"}));
	
	fijar_altura(280);
	fijar_peso_corporal(500000);
	
	fijar_raza("demonio");
	fijar_clase("soldado");
	fijar_religion("gurthang");
	
	fijar_fue(36);
	fijar_extrema(100);
	fijar_con(26);
	fijar_des(20);
	fijar_int(23);
	fijar_sab(22);
	fijar_car(29);
	
	fijar_nivel(90);
	fijar_alineamiento(50000);
	
	poner_estilo_combate_desarmado("pelea");
	add_clone(BARMAS + "maza_a_dos_manos", 1);
	init_equip();
	
	ROL_NPCS->definir(this_object());
	
	add_property("no_desarmar", 1);
	add_property("sin miedo", 1);
	add_property("no_desfondable", 1);
	add_property("abrigado", 1);
	add_property("libre-accion", 1);
	add_property("no_lanzar", 1);

	load_chat(15, ({
		":otea el horizonte con atención, su aguda vista parece alcanzar una distancia inimaginable.",
		":balancea peligrosamente cerca de tí su maza para calentar sus músculos.",
		":aletea brevemente y una ola de calor te invade por completo."
	}));
	
	load_a_chat(50, ({
	}));
		
	fijar_pvs_max(40000);
	fijar_pe_max(25000);
	
	fijar_maestria("maza a dos manos", 100);

	ajustar_bo(300);
	adjust_tmp_damage_bon(120);
	
	add_attack_spell(30, "aplastar", 3);
	add_attack_spell(10, "barrido", 3);
	//add_loved("raza", ({"orco", "gnoll", "goblin", "kobold"}));
	//add_loved("ciudadania", ({"ancarak","mor_groddur", "golthur","arad-gorthor"}));
	add_loved("religion", ({"gurthang"}));
	set_aggressive(4);//players y npcs salvo loved
}

string dame_noconversar_personalizado()
{
	return "Con irreverente desprecio una brusca mueca del demonio te sirve como toda contestación.\n";
}
