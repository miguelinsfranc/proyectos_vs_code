#include "../path.h";
inherit "/obj/monster.c";
void setup()
{
	set_name("galin");
	set_short("Galin");
	set_long("Galin es un pequeño goblin de ojos tan finos como agujas que brillan con negra codicia cuando los abre al hacer una venta. Sus manos se retuercen constantemente "
	"inquieto por su negocio cada vez más pobre y descuidado.\n");
	set_main_plural("Galins");
	add_plural(({"galines", "galinss"}));
	
	fijar_altura(102);
	fijar_peso_corporal(30200);
	
	fijar_raza("goblin");
	fijar_religion("gurthang");
	
	fijar_nivel(22);
	ajustar_dinero(roll(1, 4), "platino");
	
	ROL_NPCS->definir(this_object());
	
	add_clone(BARMADURAS + "capa", 1);
	add_clone(BARMADURAS + "sandalias", 1);
	init_equip();
	
	load_chat(30, ({
		":retuerce sus manos mirando tu bolsa con avidez.",
		"'Cómo os vea meteros algo en el bolsillo intentando estafarme granujas ¡Os arrancaré la piel tirando de las pestañas!",
		":parece darte la espalda y de repente ¡BOING! De un gran salto se gira y te mira abriendo al máximo sus negros ojos para ver si te pilla robando.",
		":olfatea el aire con su fea nariz apuntando a tu bolsa y una mueca de disgusto cubre su rostro. O eres pobre, o hueles fatal.",
	}));
	
	load_a_chat(50, ({
		":corre por los pasillos maltrechos de su tienda intentando huir de tí mientras busca tu espalda.",
		"'¡Como rompas algo te mato! ¡Yo por mi tienda MA-TO!",
	}));
}
