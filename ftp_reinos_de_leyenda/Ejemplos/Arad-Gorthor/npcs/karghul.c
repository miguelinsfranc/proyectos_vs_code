// Grimaek 14/06/2023 arregladas maestrias
#include "../path.h";
inherit "/obj/conversar_npc.c";


void presentarse(object pj) {
	
	if (!pj || !ENV(pj)) return;
	
	pj->do_say("Soy "+pj->dame_nombre_completo(),0);
	pj->do_say(capitalize(pj->dame_clase())+" y "+capitalize(pj->dame_oficio()),0);
	pj->do_say(capitalize(pj->query_title()),0);
	return;
}

void es_horda(object pj)
{
	if(!pj ) return ;
	// si no es horda_negra, pero ha llegado hasta aquí por diplomacias, nos da igual solo es fiel a la horda.
	if("horda_negra"==pj->dame_gremio())
    {
        do_emote("te mira de arriba a abajo.");
        do_say("Veo que perteneces a la horda como yo.",0);
        do_emote("sonrie con complicidad.");
        do_say("¿Sabes cual es mi nombre?",0);
        
        return;
			
	}
        do_emote("te mira con los ojos desorbitados de ira.");
        do_say("¿Como te atreves a molestarme basura?",0);
        do_emote("aprieta los dedos contra su daga.");
        do_say("solo hablare con mis hermanos de la horda negra,",0);
        do_emote("se acerca hacia ti a toda velocidad con su arma apuntandote.");
        
        attack_ob(pj);

        do_say("¡Largate de aquí!.",0);
        
        return;
}
		

void titulo_horda(object pj) {
	
	if (!pj || !ENV(pj)) return;
	
	
	
	if ( ! "/grupos/gremios/horda_negra.c"->es_mandatario(pj))
	{
        do_emote("se muestra considerablemente enfadado.");
        do_say("Como que no me conoces pedazo de basura, soy Karghul el manitas, ",0);
        do_emote("aprieta los dientes durante unos segundos mientras te mira con los ojos llenos de cólera.");
        do_say("el gran trampero del Ejército negro,",0);
        do_say("mis facultades son conocidas por todos en la Horda.",0);
        do_say("Tanto, que he sido enviado para acabar con esos salteadores del exterior.",0);
        do_say("¿Y tu gusano?... ¿Quien eres?",0);
        presentarse(pj);
        do_say("Muy bien "+pj->dame_nombre_completo()+"...¿Que diablos quieres?",0);
        return;
	}

        do_say("Mi señor, soy Karghul el manitas, ",0);
        do_say("me enviasteis para eliminar a todos los salteadores existentes.",0);
        do_emote("sonrie como un maniaco homicida.");
        do_say("Haciendo uso de mis...facultades.",0);
        do_say("Mi señor "+pj->dame_nombre_completo()+"...",0);
        do_say("¿Que os ha traído hasta aquí?",0);

	return;
}

int soy_trampero(object player) {
	
	if (!player)
		return 1;
	
    if(player->pertenece_oficio("trampero"))
		return 0;
	
	return 1;
}

void puede_trampero(object pj)
{
	int valido;
	if (!pj) return;
	
	valido = "/grupos/oficios/trampero.c"->chequea_caracteristicas(pj); 
	
	if (!soy_trampero(pj))
	{
			do_say("Tú ya eres trampero no me hagas perder mi tiempo",0);
			return;
	}
		
	
	if(valido) 
	{
        do_emote("te observa detenidamente.");
        do_say("¿Quieres que te enseñe como crear trampas?",0);
        do_emote("aprieta los dientes durante unos segundos mientras te mira con los ojos llenos de cólera.");
        do_say("No tengo tiempo suficiente para perderlo contigo… ",0);
        do_say("Aunque bueno…",0);
        do_say("Si me traes tres cabezas de esos salteadores que hay en el exterior podemos llegar a algún acuerdo…",0);
        
        if(!pj->dame_mision_abierta("arad_trampero_horda") )
        pj->nuevo_hito_mision("arad_trampero_horda", "buscar_cabezas", 1);
        
        return;
	}
	
        do_emote("te observa detenidamente.");
        do_say("Anda ya, vete por ahí,",0);
        do_emote("niega con la cabeza.");
        do_say("no pienso perder mi tiempo con un inútil, estúpido y asqueroso como tú.",0);
        do_say("Aunque bueno…",0);
        do_say("Tú no serías capaz ni de crear una trampa para matar una mosca.",0);
        do_emote("levanta su daga indicandote la entrada de la caverna.");
        return;
			
}
void entregar_cabezas(object pj){
	
	object *cabezas;
	int num_cabezas = 0;
	
	if (!pj) return;
	
	cabezas = filter(all_inventory(pj),(:$1->query_short() == "Cabeza de salteador":) );
	num_cabezas = sizeof(cabezas);
		
    do_emote("te observa detenidamente.");
    do_say("Veamos si ya tienes las cabezas...",0);
    
	
	if (num_cabezas<3)
	{
        do_emote("niega con la cabeza.");
        do_say("no me hagas perder el tiempo lombriz.",0);
        do_say("vuelve cuando tengas las tres cabezas",0);
        return;
	}

    tell_object(pj,"Buscas en tu inventario y le lanzas las cabezas a los pies del goblin.\n");

    tell_accion(ENV(TO), pj->QCN+" rebusca algo en su inventario y saca unas cabezas que arroja a los pies del goblin.\n",
    "Escuchas el ruido de varios objetos golpear contra el suelo.\n",({pj}),pj);
    cabezas->move(ENV(TO));
    do_emote("te mira con los muy abiertos.");
    do_say("No me imaginaba que lo conseguirias.",0);
    do_say("Yo me quedare con las cabezas.",0);
    cabezas->dest_me();
    do_emote("ríe como un maniaco homicida.");
    do_say("Esta bien, para que luego digan que los goblins somos traicioneros...",0);
    do_say("Ven aquí, simplemente hay que poner las cosas puntiagudas apuntado hacia lo que quieras matar. ",0);

    pj->nuevo_oficio("trampero");
    tell_object(pj,"Aprendes el oficio de Trampero.\n");
		
	if(!pj->dame_hito_mision("arad_trampero_horda", "ser_trampero") )
        pj->nuevo_hito_mision("arad_trampero_horda", "ser_trampero", 1);
}	


void iniciar_dialogos()
{
	
	nuevo_dialogo("presentarse",({
       "es_horda"
    }));
	nuevo_dialogo("nombre",({
       "titulo_horda"
    }),"presentarse"
	);

	
	nuevo_dialogo("facultades", ({
		":te mira con cierto aire de desprecio.",
		"'¡Soy un experto trampero!",
		"'¡El mejor de todos en la horda negra!",
		":parece más grande cuando sus gritos resuenan en las paredes de la caverna.",
		"'¡el mejor de todos los goblins que existen!",
		"'Además, se crear trampas perfectas en las que atrapar a los enemigos de Gurthang.",
		":mira con orgullo a las creaciones que tiene a su alrededor."
    }),"nombre");
	
	nuevo_dialogo("trampero", ({
        "'Aunque la mayoría de goblins sepamos colocar trampas en nuestro alrededor, ",
		"'sólo unos pocos somos las que sabemos crear estos artefactos dignos"
		" de los seres más asquerosos de Eirea.",
		":hace una pequeña pausa.",
		":se ríe maniacamente."		
    }),"facultades");
	
	nuevo_dialogo("crear", ({
        "puede_trampero"	
    }),"trampero");
	deshabilitar("crear","hito arad_trampero_horda buscar_cabezas");
	
	prohibir("entregar","hito arad_trampero_horda buscar_cabezas");
	nuevo_dialogo("entregar", ({
        "entregar_cabezas"	
    }));

	habilitar_conversacion();
	
	
}
void setup()
{
    set_name("Karghul");
    set_short("Karghul, el manitas");
    set_long("Karghul es un pequeño y extremadamente delgado goblin enviado por la horda negra a estos lares. "
        "Es uno de los mejores y más conocidos tramperos de las filas de Gurthang, su habilidad para crear, colocar y desmantelar "
        "trampas es colosal. Aunque seguramente, esto no fue así siempre, pues el goblin posee numerosos cortes por todo el cuerpo, "
        "sobre todo por brazos, piernas y manos, en la derecha faltan la mitad de tres dedos y en la izquierda le faltan dos completos, "
        "el meñique y el anular. En su cabeza puedes ver como su ojo derecho parece estar reemplazado por una bola de cristal empañada y sucia. "
        "Tanto sus ropajes como su piel tienen un color negro amarronado, que le den un grotesco aspecto antihigiénico\n");
    set_main_plural("Karghuls");
    generar_alias_y_plurales();
    fijar_altura(80);
    fijar_peso_corporal(40000);
    fijar_raza("goblin");
    fijar_clase("ladron");
    fijar_religion("gurthang");
    fijar_fue(18 + !random(4));
    fijar_con(18);
    fijar_des(18);
    fijar_int(4);
    fijar_sab(4);
    fijar_car(5);
    fijar_nivel(16 + random(4) + !random(10));
    fijar_alineamiento(8500);
    load_chat(10, ({
        ":remueve sus cachivaches en busca de algún utensilio que usar en la trampa que está construyendo.",
        ":te mira sonriendo, mientras juguetea con su daga.",
      }));
    load_a_chat(50, ({
        "'¡No debiste haber venido!",
        ":se lanza sobre ti con no muy buenas intenciones.",
        "'¡Vas a morir! ¡Morir! ¡Morir!",
        "'¡No te resistas! ¡Será más doloroso para tí y tendré que esforzarme más!",
      }));
   
    add_clone("/d/golthur/items/daga_curva.c", 1);
    fijar_maestria("Penetrantes ligeras", 40+random(11));
		
    add_clone(BARMADURAS + "zapatillas", 1);
    add_clone(BARMADURAS + "pantalones", 1);
    add_clone(BARMADURAS + "cuero", 1);
    
    init_equip();
	add_loved("gremio","horda_negra");
    set_aggressive(1);
	
	iniciar_dialogos();
	
	
 
}
