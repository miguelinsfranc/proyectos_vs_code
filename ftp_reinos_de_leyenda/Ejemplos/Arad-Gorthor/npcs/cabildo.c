// Dunkelheit 05-12-2009
// Grimaek 13/06/2013 Sustituido add_attack_spell por anyadir_ataque_pnj

#include "../path.h";
inherit "/obj/conversar_npc.c";

void setup()
{
	set_name("cabildo");
	set_short("Cabildo de la Guerra");
	add_alias(({"goblin", "guerra"}));
	set_long("Los chamanes de Arad Gorthor que -siempre extraoficialmente- prefieren la "
	"vida monástica al campo de batalla, tienen una única aspiración en la vida: "
	"integrarse en la orden de cabildos de la guerra. Este goblin es la actual cabeza "
	"visible de esta orden, cuya única determinación es el mantenimiento de los "
	"templos erigidos en honor a Lord Gurthang y el oficio de misas y otra suerte "
	"de ritos. Ataviado con la humilde y minimalista toga de cabildo y una jabalina "
	"ceremonial, este goblin de aspecto traicionero limpia y ordena aquí por allá sin "
	"quitarte un ojo de encima.\n");
	set_main_plural("Cabildos de la Guerra");
	add_plural(({"cabildos", "goblins"}));
	
	fijar_altura(145);
	fijar_peso_corporal(65000);
	
	fijar_raza("goblin");
	fijar_clase("chaman");
	fijar_religion("gurthang");
	
	fijar_fue(18);
	fijar_con(18);
	fijar_des(18);
	fijar_int(15);
	fijar_sab(19);
	fijar_car(17);
	
	fijar_nivel(36);
	fijar_alineamiento(15250);
	
	add_clone(ARMAS + "jabalina_cabildo", 1);
	add_clone(ARMADURAS + "toga_cabildo", 1);
	add_clone(BARMADURAS + "capucha", 1);
	init_equip();

	ajustar_dinero(3 + random(4), "platino");
	
	ROL_NPCS->definir(this_object());
	
	load_chat(30, ({
		":saca una pequeña vasija con un ungüento y lo extiende en los pies de la estatua.",
		":enciende unas velas alrededor del altar.",
		":se mueve lentamente de un lado a otro del templo, pero siempre se detiente ante la estatua para proferir una oración.",
		":lanza unos polvos a la humeante grieta, que responde lanzando unas lenguas de fuego.",
		"'Hoy es un gran día para la guerra, no desaproveches la oportunidad que nos ha dado Lord Gurthang para sembrar el caos y el horror",
	}));
	
	load_a_chat(50, ({
		"'¡Infiel, tu carne alimentará el fuego de la grieta!",
	}));
	
	nuevo_dialogo("templo", ({
		"'A todo snaga se le llena a boca hablando del templo, ¿verdad?",
		"'Yo estaba aquí mucho antes de que esos zánganos pudieran erguirse",
		"'¿A quién hacen caso en la Torre de Brujería en cuanto al templo se refiere?",
		"'¡A mí!",
		"'¿A quién recurren para invocar el impío poder de los vapores avernales de la grieta?",
		"'¡¡¡A mí!!!",
	}));
	nuevo_dialogo("humildad", ({
		":se rebota y frunce el ceño.",
		"'¡No necesito que me den lecciones de humildad, entérate!",
		"'¡Para ser el custodio del templo hace falta algo más que tener una buena posición!",
	}), "templo");
	nuevo_dialogo("custodio", ({
		":sopla con enfado uno de los cirios, salpicándo el suelo de cera.",
		"'¡Dicen que estoy viejo ya para este cargo!",
		"'¡Ni que hubiera que desempeñar un gran esfuerzo físico!",
		"'¡Por Lord Gurthang! ¡hay tanto snaga que ambiciona mi posición!",
		"'Pero estoy bien prevenido",
		"'Pienso regar la grieta con la sangre de quien ose pisar el templo con intención de destituirme",
	}), "humildad");
	nuevo_dialogo("ayudar", ({
		":pone cara de asombro.",
		"'¿Tú querrías ayudarme?"
		":se frota las huesudas y uñosas manos.",
		"'Aniquila a los aprendices de la Orden de Cabildos... ¡sí!",
		"'Aquí tienes la llave que da acceso a las cámaras donde se reunen",
		"'¡Sólo tengo esta copia, así que guárdala bien!",
		"'Y recuerda, ¡sólo hay concilios las noches en que no hay luna alguna!",
		"dar_llave"
	}), "custodio");
	
	habilitar_conversacion();
	
	fijar_pvs_max(9000);
	fijar_pe_max(4500);
	
	anyadir_ataque_pnj(100, "retener persona");
	anyadir_ataque_pnj(100, "mordisco de la vibora");
	anyadir_ataque_pnj(100, "columna de fuego");

}

int frases_personalizables(string tipo, object npc, object pj)
{
	switch (tipo)
	{
		case "bienvenida-pj":
			if (HORDA_NEGRA->es_oficial(pj)) {
				tell_object(pj, npc->query_short()+" te recibe con los brazos abiertos y te imparte su bendición.\n");
				npc->do_say("¡Salve, "+HORDA_NEGRA->dame_titulo(pj)+"! ¿a qué debemos el honor de su presencia?", 0);
			} else {
				tell_object(pj, npc->query_short()+" asiente discretamente mientras continua acicalando el santuario.\n");
				npc->do_say("Saludos, "+HORDA_NEGRA->dame_titulo(pj), 0);
			}
			break;
		default:
			return 0;
	}

	return 1;
}

int dar_llave(object quien) {
	if (quien->query_timed_property("AG_llave_totemistas")) {
		TO->do_say("¡Hace poco que te entregué la llave! ¿La has perdido?");
		TO->do_emote("apoya la cara en la mano consternado mientras susurra algo sobre la madre del cordero.. o la tuya.. no te ha quedado claro.");
	} else if (TO->query_property("AG_llave_entregada") == 0 && quien->entregar_objeto(OBJETOS+"llave_cueva_totemistas.c", "", 1)) {
		tell_object(quien, "Te entrega una frágil llave de madera.\n");
		tell_accion(environment(quien), TO->query_short() + " entrega algo a " + quien->query_short() + ".\n", "", ({quien}), quien);
		quien->add_timed_property("AG_llave_totemistas", 1, 86400);
		TO->add_property("AG_llave_entregada", quien->query_short());
	}
	else {
		TO->do_say("¡Qué memoria la mía, hace poco que se la dí a " + TO->query_property("AG_llave_entregada") + "! ¿En qué estaría pensando?"); 
		TO->do_emote("se marcha murmurando entre dientes como un anciano algo sobre el exceso de setas en su juventud.");
	}
	return 1;
}
