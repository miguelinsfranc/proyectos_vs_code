// Dunkelheit 10-11-2009
// Grimaek 13/06/2013 Sustituido add_attack_spell por anyadir_ataque_pnj

#include "../path.h";
inherit "/obj/conversar_npc.c";

void setup()
{
	set_name("cazarrecompensas");
	set_short("Cazarrecompensas");
	add_alias("goblin");
	set_long("Al tener el rostro cubierto por una capucha-máscara de tela, es "
	"difícil determinar la raza de este avezado cazarrecompensas. Pero por su "
	"tamaño y andares dirías que es un goblin, lo cual tiene sentido, ya que "
	"estas escurridizas y sigilosas criaturas son los asesinos perfectos. Los "
	"cazarrecompensas encuentran en los suburbios de Arad Gorthor el lugar "
	"ideal para sus \"negocios\": aquí es sencillo toparse con alguien que "
	"necesite deshacerse de algún enemigo, o encontrarlos para reclamarles "
	"la vida en monedas de platino. Los cazarrecompensas pueden llegar a ser "
	"magníficos adversarios, así que mejor andarse con ojo.\n");
	set_main_plural("Cazarrecompensas");
	add_plural(({"cazarrecompensas", "goblins"}));
	
	fijar_altura(130 + random(40));
	fijar_peso_corporal(50000 + random(25000));
	
	fijar_raza("goblin");
	fijar_clase("yver'adhras");
	fijar_religion("gurthang");
	
	fijar_fue(18);
	fijar_con(18);
	fijar_des(18);
	fijar_int(4);
	fijar_sab(4);
	fijar_car(11);
	
	fijar_nivel(30 + random(4));
	fijar_alineamiento(5000);
	fijar_maestria("Cortantes medias", 60);
	
	switch (random(2)) {
		case 1:
			add_clone(BARMAS + "cimitarra", 2);
			break;
		default:
		case 0:
			add_clone(BARMAS + "sable", 2);
			break;
	}

	add_clone(BARMADURAS + "cuero", 1);
	add_clone(BARMADURAS + "capa", 1);
	add_clone(BARMADURAS + "capucha", 1);
	add_clone(BARMADURAS + "botas", 1);
	
	init_equip();
	
	ajustar_dinero(5 + random(5), "oro");
	
	ROL_NPCS->definir(this_object());
	
	nuevo_dialogo("trabajo", ({
		"'¿Me tomas el pelo?",
		"'Creo que sabes muy bien a qué me dedico",
		"'De lo contrario creo que deberías salir de este barrio",
		":se ríe.",
	}));
	nuevo_dialogo("explicamelo", ({
		":refunfuña.",
		"'Mira, si tienes que deshacerte de alguien...",
		"'Yo soy tu tipo",
		":sonríe con confianza.",
		"'Solo tienes que hacer el %^YELLOW%^encargo%^RESET%^",
	}), "trabajo");
	
	load_chat(25, ({
		":permanece apoyado en la pared, observando a la gente pasar.",
		":se ajusta la capucha para ocultar mejor su rostro.",
		":sigue con la mirada a un transeunte. ¿Será este el comienzo de una pelea?",
	}));
	
	habilitar_conversacion();
	
	anyadir_ataque_pnj(100, "torbellino");
}

void init()
{
	::init();
	add_action("encargar", ({"encargo", "encargar"}));
}

int encargar(string str)
{
	str = lower_case(str);

	this_player()->consumir_hb();
	
	if (dame_peleando()) {
		return notify_fail("El cazarrecompensas está en mitad de una pelea. Quizá ahora no sea el mejor momento.\n", this_player());
	}

	if (!str || str == "") {
		return notify_fail("Si lo que quieres es hacerle un \"encargo\" al cazarrecompensas, "
		"tendrás que decirle al menos el nombre de tu víctima.\n", this_player());
	}
	
	if (str == this_player()->query_name()) {
		return notify_fail("Eso no sería muy inteligente.\n", this_player());
	}
	
	if (!user_exists(str)) {
		do_say("Lo siento, pero no conozco a nadie con ese nombre", 0);
		return notify_fail("Vaya. Parece que tendrás que buscarte otra víctima.\n", this_player());
	}
	
	if (sizeof(query_hated()["jugador"])) {
		do_say("Lo siento, pero ya me han hecho un encargo hoy... así que tendrás que esperar", 0);
		return notify_fail("¡Pues vaya faena! Parece que serás TÚ quien tenga que encargarse de "+capitalize(str)+"\n", this_player());
	}

	do_say("Esto te costará 15 monedas de platino", 0);

	if (!this_player()->pagar_dinero(15, "platino")) {
		do_say("¡Y no parece que las tengas!", 0);
		return notify_fail("Vaya. Parece que tendrás que conseguir algo de dinero antes.\n", this_player());
	}
	
	set_aggressive(6);
	add_hated("jugador", ({ str }));
	
	write_file(LOGS+"cazarrecompensas", ctime()+" "+this_player()->query_cap_name()+" encargó matar a "+capitalize(str)+".\n");
	
	do_say("Tenemos un trato, "+this_player()->dame_nombre_completo(2)+". Acabaré con la miserable vida de "+capitalize(str)+" si se cruza en mi camino", 0);
	return 1;
}

void heart_beat()
{
	if (!query_timed_property("lockout_concentracion") && dame_peleando()) {
		habilidad("concentracion", "");
	}
	::heart_beat();
}

int frases_personalizables(string tipo, object npc, object pj)
{
	switch (tipo)
	{
		case "bienvenida-pj":
			if (HORDA_NEGRA->es_lider(pj)) {
				npc->do_emote("reconoce tu posición en la Horda Negra y realiza una humilde reverencia antes de empezar la conversación.");
			} else {
				npc->do_emote("asiente con la cabeza, aceptando tu invitación para conversar.");
			}
			break;
		case "bienvenida-npc":
			break;
		case "despedida-pj":
			pj->do_say("Adiós, tengo que irme", 0);
			break;
		case "despedida-npc":
			npc->do_say("Hasta la vista... y ten cuidado", 0);
			break;
		default:
			return 0;
	}

	return 1;
}
