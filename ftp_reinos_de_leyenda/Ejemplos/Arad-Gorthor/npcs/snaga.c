// Dunkelheit 19-01-2010

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	string raza = element_of(({"orco", "semi-orco", "goblin", "gnoll"}));
	string clase = element_of(({"soldado", "barbaro"}));
	
	set_name("snaga");
	set_short("Snaga");
	set_main_plural("Snagas");
	add_plural("snagas");
	fijar_genero(1);

	fijar_altura(150 + random(30));
	fijar_peso_corporal(75000 + random(15000));

	fijar_raza(raza);
	fijar_clase(clase);
	fijar_religion("gurthang");

	set_long("Son la carne de cañón de la Horda Negra. La primera línea de batalla. "
	"El escalón más bajo; los últimos en enterarse de que van a morir: los snagas. "
	"Este "+dame_clase()+" "+dame_raza()+" está deseando escalar peldaños en la larga jerarquía "
	"de la horda y no dudes que estará dispuesto a hacer lo que sea. Su indumentaria "
	"es acorde a su escalafón: chatarra. Dañina, pero chatarra. Pero la función "
	"de los snagas no sólo se limita a ser un muro de carne en las incursiones, "
	"sino que también patrullan el bastión de Arad Gorthor, actividad que "
	"compaginan con sus intensos entrenamientos en la ciudad goblinoide.\n");
	
	fijar_fue(18);
	fijar_extrema(90 + (random(3) ? 10 : 0));
	fijar_con(18);
	fijar_des(14);
	fijar_int(14);
	fijar_sab(4);
	fijar_car(4);
	
	fijar_nivel(20 + random(6));
	fijar_alineamiento(2501 + random(2000));
	
	if (clase == "soldado") {
		switch (random(10)) {
			case 0..2:
				add_clone(BARMADURAS + "bandas", 1);
				break;
			default:
			case 3..8:
				add_clone(BARMADURAS + "mallas", 1);
				break;
			case 9:
				add_clone(BARMADURAS + "escamas", 1);
				break;
		}
		switch (random(10)) {
			case 0..8:
				add_clone(BARMADURAS + "yelmo", 1);
				break;
			default:
			case 9:
				add_clone(BARMADURAS + "bacinete", 1);
				break;
		}
		switch (random(8)) {
			default:
			case 0:
				add_clone(BARMAS + "cimitarra", 1);
				add_clone(BESCUDOS + "escudo", 1);
				ajustar_maestria("cimitarra", 50);
				break;
			case 1:
				add_clone(BARMAS + "hacha_de_batalla", 1);
				add_clone(BESCUDOS + "escudo", 1);
				ajustar_maestria("hacha de batalla", 50);
				break;
			case 2:
				add_clone(BARMAS + "jabalina", 1);
				add_clone(BESCUDOS + "escudo", 1);
				ajustar_maestria("jabalina", 50);
				break;
			case 3:
				add_clone(BARMAS + "martillo_de_guerra", 1);
				add_clone(BESCUDOS + "escudo", 1);
				ajustar_maestria("martillo de guerra", 50);
				break;
			case 4:
				add_clone(BARMAS + "horca_militar", 1);
				ajustar_maestria("horca militar", 50);
				break;
			case 5:
				add_clone(BARMAS + "guja", 1);
				ajustar_maestria("guja", 50);
				break;
			case 6:
				add_clone(BARMAS + "bardiche", 1);
				ajustar_maestria("bardiche", 50);
				break;
			case 7:
				add_clone(BARMAS + "mayal", 1);
				add_clone(BESCUDOS + "gran_escudo_madera", 1);
				ajustar_maestria("mayal", 50);
				break;
		}
	} else {
		if (random(2)) {
			add_clone(BARMAS + "hacha_a_dos_manos", 1);
		} else {
			add_clone(BARMAS + "maza_a_dos_manos", 1);
		}
		add_clone(BARMADURAS + "piel", 1);
		add_clone(BARMADURAS + "botas", 1);
		if (!random(5)) {
			add_clone(BARMADURAS + "pulsera", 1);
			add_clone(BARMADURAS + "pulsera_izq", 1);
		}
		if (!random(4)) {
			add_clone(BARMADURAS + "cinturon", 1);
		}
	}
	
	if (!random(5)) {
		add_clone(BARMADURAS + "anillo", 1);
	}
	if (!random(5)) {
		add_clone(BARMADURAS + "amuleto", 1);
	}
	if (!random(8)) {
		add_clone(BARMADURAS + "collar", 1);
	}
	
	init_equip();
	
	ajustar_dinero(roll(1, 10), "plata");
	
	ajustar_bo(90);
	
	ROL_NPCS->definir(this_object());
	
	load_chat(30, ({
		":escudriña de un lado a otro con los ojos entrecerrados. No parece tener un pelo de tonto.",
		":se saca un pedazo de resina del bolsillo y empieza a mascarlo.",
		"'¡Mi sangre no termina de acostumbrarse al calor del desierto!",
		"'Únete a la Horda Negra, vivirás entre los guerreros más aguerridos y morirás como un héroe... ¡irás directo al seno de Lord Gurthang!",
		"'¡Lord Gurthang es grandioso y nos regala batallas a diario en las que podemos demostrar nuestro poder!",
		"'¡Guerra! ¡Guerra! ¡Guerra! ¡Lo es todo para nosotros!",
		"'Vivimos y morimos por la Voluntad Final de nuestro dios: ¡la conquista de todos los reinos libres del mundo!",
		"'No me fío de esos kobolds; son una raza más cercana al ratón que al goblinoide",
		"'La historia pronto hablará de mí, quédate con mi cara... o mejor, quédate con mis pies, ¡para cuando tengas que besarlos!",
		":le da un empujón a uno de sus compañeros, haciendo que pierda el paso.",
		"'Qué ganas tengo de que termine mi guardia, ¡me muero por echar un trago en la cantina!",
		"'¿No tendrás a mano un trago de Áspex? ¿No? Vaya, es una lástima",
		"'¡Qué pasa! ¡¿Es que no nos van a mandar de una vez a acabar con la lacra hobgoblin del cementerio?!",
		"'Tengo un sueño... no, espera, ¡lo que tengo hambre! ¡HAHAHA!",
		":apoya su arma en el hombro para descansar, pero le reprimen con un bofetón en la cara y vuelve rápidamente a marcar el paso como manda la Horda.",
	}));
	
	load_a_chat(50, ({
		"'¡Bendito seas, Lord Gurthang! ¡Gracias por esta ración de Guerra!",
		"'¡Te despellejaré y venderé tu carne al sacamantecas, así me sacaré unas monedas!",
		":profiere gritos de guerra y te dedica gestos obscenos.",
		"'¡Con tu muerte tengo el ascenso a suboficial asegurado! ¡Así que no te hagas de rogar!",
		"'¡Maldita basura, te arrancaré los ojos y te los meteré por el trasero!",
		":requiere inmediatamente la presencia de guerreros de la Horda Negra.",
		"'¡Me da igual quién seas, cuando te mate nadie se acordará de ti!",
	}));
}

string dame_noconversar_personalizado()
{
	return "Este snaga está muy ocupado en sus estrictos quehaceres y no te prestará "
	"atención a menos que sea para abrirte la cabeza a mamporros; nunca serás capaz "
	"de doblear su férrea disciplina militar.\n";
}
