	

inherit "/obj/monster";

void setup() {
    set_name("tendero");
    set_short("Tendero kobold");
    add_alias("kobold");
    set_main_plural("Tenderos kobold");
    fijar_raza("kobold");
	set_long("Este kobold de porte digno, mira a su alrededor como si fuera el señor de todo Eirea, sólo con "
		"su soberbia mirada es capaz de transmitir tanto desprecio que enfurece al más humilde de los clientes. "
		"Muchos comentan que esta actitud altanera le permite negociar mucho mejor los contratos.\n");
    set_random_stats(14,18);
	nuevo_lenguaje("negra",100);
    fijar_nivel(10+random(10));
    ajustar_dinero(1+random(10),"bela");
}
