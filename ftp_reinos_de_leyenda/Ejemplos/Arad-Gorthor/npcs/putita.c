// Dunkelheit 10-11-2009

#include "../path.h";
inherit "/obj/conversar_npc.c";

void setup()
{
    string raza = element_of(({"orca", "goblin" }));
    
    set_name("ramera");
    set_short("Ramera " + capitalize(raza));
    add_alias(raza);
    set_long("Camina dando pasos cortos con las manos apoyadas a la altura de "
    "los riñones; dudas que sea a causa de un largo viaje a caballo. Las rameras "
    "de Arad Gorthor han despertado incluso la curiosidad de los matasanos de "
    "reinos lejanos como Kattak o Takome, atraídos por la cantidad ingente de "
    "enfermedades -venéreas, aunque tú eso no lo sabes- que albergan sus "
    "maltrechos y manidos cuerpos. Seguramente no te cueste mucho conseguir "
    "sus servicios, tal vez unas míseras monedas de oro y un paseo hasta algún "
    "callejón oscuro, lejos de otros posibles clientes que no dudarían un "
    "segundo en unirse a tu fiesta privada... sin pagar.\n");
    set_main_plural("Rameras "+capitalize(raza)+"s");
    add_plural(({"rameras", raza+"s"}));
    
    if (raza == "orco") {
        fijar_altura(150 + random(20));
        fijar_peso_corporal(70000 + random(20000));
    } else {
        fijar_altura(120 + random(20));
        fijar_peso_corporal(40000 + random(20000));
    }
    
    fijar_raza(raza);
    fijar_clase("aventurero");
    fijar_religion("gurthang");
    
    fijar_fue(17); // Para aguantar tanta metida
    fijar_con(19); // Para aguantar tanta enfermedad
    fijar_des(6);  // Su repertorio de posturas no es muy amplio
    fijar_int(4);
    fijar_sab(4);
    fijar_car(18); // De algo tienen que vivir
    
    fijar_nivel(5 + random(6));
    fijar_alineamiento(501);
    
    ajustar_dinero(3 + random(5), "oro");
    
    //ROL_NPCS->definir(this_object());
    
    nuevo_dialogo("callejon", ({
        "'No podemos ponernos a hacerlo en mitad de la ciudad, podrían meternos en el calabozo",
        "'Agrúpame y llévame al callejón oscuro de los suburbios, allí tendremos suficiente intimidad",
    }));
    
    nuevo_dialogo("hacerlo", ({
        "chikichiki"
    }));
    
    habilitar_conversacion();
    
    load_chat(30, ({
        ":restriega sus senos el uno contra el otro.",
        ":saca uno de sus pechos y empieza a dar lametones a su pezón encostrado.",
        ":te dedica un gesto obsceno con la mano, parece invitarte a poner \"tu lengua\" en sus \"genitales\".",
        "'¿Alguien quiere pasar un buen rato?",
        "'He sido una "+raza+" muy mala, y necesito que alguien me dé mi merecido",
    }));
    load_a_chat(50, ({
        "'¡Sabía que eras de los que disfruta azotando a las rameras!",
        "'¡Siempre guardo una daga en el liguero, prepárate para descubrir mis verdaderas armas de mujer!",
    }));
}

void chikichiki(object quien)
{
	do_say("Ayayayay", 0);
}

int frases_personalizables(string tipo, object npc, object pj)
{
    switch (tipo)
    {
        case "saludo":
            if (pj->dame_genero() == 1) {
            	if (HORDA_NEGRA->es_oficial(pj)) {
	                pj->do_say("Supongo que sabrás que soy alguien importante en la Horda Negra", 0);
	                pj->do_say("Así que harás lo que te ordene o te segaré el pescuezo", 0);
            	} else {
	                pj->do_say("¿Y tú qué vendes, guapa?", 0);
            	}
            } else {
            	if (HORDA_NEGRA->es_oficial(pj)) {
	                pj->do_say("Supongo que sabrás que soy alguien importante en la Horda Negra", 0);
	                pj->do_say("Creo que quiero conocer nuevas experiencias... esta vez, con una sucia hembra como tú", 0);
	            } else {
	            	pj->do_say("No sé qué hago hablando contigo... puede que tal vez quiera probar las mieles de otra mujer", 0);
	            }
            }
            break;
        case "bienvenida-pj":
            if (pj->dame_genero() == 1) {
		        if (HORDA_NEGRA->es_oficial(pj)) {
		            npc->do_emote("se somete dócilmente ante tu indiscutible autoridad, y aparta a un lado "
		            "su raída faja para mostrarte los genitales. Parece estar más que dispuesta a satisfacerte "
		            "sin cobrar...");
		            npc->do_say("Llévame al callejón oscuro y empálame con tu descomunal verga, ¡ahora mismo!", 0);
		        } else {
		            npc->do_say(element_of(({
		                "Depende de lo que puedas pagar, pazguato",
		                "Uf... mira que tengo que acostarme con tipos desagradables, pero tú... ¡puag!",
		                "Vendo higos chumbos muy jugosos, ¿a ti qué te parece?",
		            })), 0);
		            npc->do_say("Qué, ¿te apetece dar un paseo hasta el callejón oscuro?", 0);
		        }
	    	} else {
		        if (HORDA_NEGRA->es_oficial(pj)) {
		            npc->do_say("No suelo hacerlo con hembras, pero... me dejaré llevar por la erótica del poder", 0);
		            npc->do_say("¿Qué me dices? ¿nos vamos al callejón oscuro y chocamos las pelucas?", 0);
		        } else {
		            npc->do_say(element_of(({
		                "Pues no se yo si quiero chocar pelucas contigo...",
		                "Ugh, ¿eres de las que disfruta peinando el oso?",
		                "Si puedes pagarlo, encontrarás en el tribadismo uno de los placeres más salvajes, te lo aseguro",
		            })), 0);
		            npc->do_say("Qué remedio, ¿me llevas al callejón oscuro o qué?", 0);
		        }
	    	}
            break;
        case "bienvenida-npc":
            break;
        case "despedida-pj":
        	if (query_property("fornicada")) {
        		string palomo = query_property("fornicada");
        		if (pj->query_name() == palomo) {
            		pj->do_say("Hasta la vista ramera, espero que lo hayas gozado tanto como yo", 0);
        		} else {
            		pj->do_say("Adiós ramera, recupérate pronto de tu último negocio, que necesito desahogarme", 0);
        		}
        	} else {
            	pj->do_say("Bueno, dame un poco de tiempo para pensar, ahora no sé si me apetece que me pegues la lepra", 0);
        	}
            break;
        case "despedida-npc":
        	if (query_property("fornicada")) {
        		string palomo = query_property("fornicada");
        		if (pj->query_name() == palomo) {
				    if (HORDA_NEGRA->es_oficial(pj)) {
				        npc->do_say("¡Oh sí, gran "+pj->query_short()+"! ¡por favor, vuelve pronto a darme leña!", 0);
				    } else {
				        npc->do_say("Psche, eres un poco \"rápido\" y... sinceramente, apenas la he notado", 0);
				    }
        		} else {
            		pj->do_say("Adiós, adiós, déjame respirar al menos", 0);
        		}
        	} else {
            	pj->do_say("Pues nada, otr"+pj->dame_vocal()+" que se va sin catarme. ¡Tú te lo pierdes!", 0);
        	}
            break;
        default:
            return 0;
    }

    return 1;
}

