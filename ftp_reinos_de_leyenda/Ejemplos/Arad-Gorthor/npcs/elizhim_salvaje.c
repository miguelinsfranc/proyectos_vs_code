//npc caballo.c creada por Sierephad el 09-octubre-2007
inherit "obj/monster.c";


void setup() {
	add_static_property("abrigado",1);
    add_static_property("cruzar_nieve",10);
    set_name("elizhim salvaje");
    set_short("Elizhim salvaje");
    set_long("Ves un caballo majestuoso con un increible porte, sus ojos brillan de tal manera" 
		"que hasta crees que posee una inteligencia superior al resto de los caballos que conoces,"
		"Su crin crece larga y salvaje.\n");
    set_main_plural("elizhim salvajes");
    add_alias(({"caballo","elizhim","mamifero","equino","animal","salvaje"}));
    add_plural(({"caballos","elizhims","mamiferos","equinos","animales","salvajes"}));
    fijar_raza("rumiante");
    fijar_altura(170+random(50));
    fijar_peso_corporal(100*dame_altura()+random(80));
    fijar_nivel(10+random(5));
    set_random_stats(10, 20);
    fijar_con(20);
	fijar_fue(20);
	fijar_extrema(100);
    fijar_alineamiento(-200);
    set_gender(1);
    load_chat(20,({
            ":relincha fuertemente.",
            ":remueve el suelo con sus cascos.",
	    ":mueve su robusto cuello haciendo que su crin se mueva elegantemente por el aire."
      }));
}

