// Altaresh 19
// Grimaek 13/06/2013 Sustituido add_attack_spell por anyadir_ataque_pnj

#include "../path.h"
#include <baseobs_path.h>
#include <conquistas.h>

#define PUNTOS_CONQUISTA 15
inherit "/obj/monster.c";
inherit "/baseobs/snippets/codigo_pnj_organizacion.c";



void setup()
{	
  
	string organizacion = DAME_CONQUISTADOR_ZONA("boreal");


	_setup_personalizacion_organizacion(organizacion);

}
void setup_descripciones(object org)
{
	string organizacion = DAME_CONQUISTADOR_ZONA("boreal");
	string corto; 
    string name  = "Guerrer" + dame_a() =="" ? "o" : "a";
	
    string donde = " Elite de " + capitalize(organizacion);
	
	fijar_genero(random(2)+1);
	
	corto = dame_genero() == 2 ? "campeona" : "campeón";

    set_name(name);
    set_short(capitalize(corto) + donde);
    set_main_plural(capitalize(corto) + donde);
    generar_alias_y_plurales();
	
	set_join_fight_mess(capitalize(corto)+" exclama: ¡Por "+capitalize(organizacion));
}

void setup_poder(object org)
{
	::setup_poder(org);
	
	fijar_maestria("Cortantes pesadas", 100);
    fijar_nivel(60);
    fijar_pvs_max(65000);
	fijar_clase("soldado");
	set_random_stats(17,18);
	fijar_carac("fue",19);
	
	anyadir_ataque_pnj(50, "tajar");
	anyadir_ataque_pnj(50, "herir");
	anyadir_ataque_pnj(50, "golpecertero");
}

void setup_equipo(object org)
{
	::setup_equipo(org);
	

    add_clone(BARMAS + "espada_a_dos_manos.c", 1);

    add_clone(BARMADURAS + "yelmo.c", 1);
    add_clone(BARMADURAS + "manopla.c", 1);
    add_clone(BARMADURAS + "manopla_izq.c", 1);
    add_clone(BARMADURAS + "grebas.c", 1);
    add_clone(BARMADURAS + "brazalete_mallas.c", 1);
    add_clone(BARMADURAS + "brazalete_mallas_izq.c", 1);
    add_clone(BARMADURAS + "botas_campaña.c", 1);
    add_clone(BARMADURAS + "completa.c", 1);
	
    init_equip();
	
}

// comprueba si el pj pertenece a la organización que conquista boreal
int pj_valido(object pj)
{
	
	return member_array(DAME_CONQUISTADOR_ZONA("boreal"), ({pj->dame_ciudadania(),pj->dame_gremio(),pj->dame_familia()}));
}

int es_aliado_valido(object b)
{

    if (!b || do_aggressive_check(b)) {
        return 0;
    }
	
	if (-1!=member_array(b,children(GUARDIAS+"mando_sacerdote.c"))){
		return 1;
	}

    if (b == TO || pj_valido(b)>-1) {
        return 1;
    }

    return 0;
}
/**
 * Devuelve la lista de "aliados validos".
 *
 * @see es_aliado_valido
 */
object *dame_aliados_validos()
{
    return filter(all_inventory(environment()), ( : es_aliado_valido($1) :));
}
void try_defender()
{
    object * obj = dame_aliados_validos() - ({TO});
    function f   = function(object b)
    {
        return b && b->dame_protector() != TO &&
               b->query_hidden() <= TO->dame_ver_realmente();
    };

    if (sizeof(obj = filter(obj, f))) {
        obj->fijar_protector(TO);
        tell_accion(
            environment(),
            query_short() + " protege a " + query_multiple_short(obj) + ".\n",
            "",
            ({TO}),
            TO);
    }
}
void heart_beat()
{
    if (!(query_hb_counter() % 10)) {
        try_defender();
    }
    ::heart_beat();
}
int do_death(object b)
{
	string *agresores = dame_pjs_involucrados_muerte();
	
	if (!b) return ::do_death(b);
	
	if (!sizeof(agresores)) return ::do_death(b);
	
    write_file(
        LOGS + "lider_boreal.txt",
        ctime() + " Muero a manos de " + (b ? b->query_cap_name() : "Alguien") + ", Los atacantes fueron... " +
        query_multiple_short(query_attacker_list() + query_call_outed()) + ".\n"
    );
	
	
	
	if (sizeof(agresores)==1)
	{
		__debug_x(sprintf("unico asesino: %s, puntos: %d\n",agresores[0],PUNTOS_CONQUISTA),"arad-gorthor");
		AJUSTAR_CONQUISTAS(find_player(agresores[0]),"boreal",PUNTOS_CONQUISTA);
		
		return ::do_death(b);
	}
	
	foreach(string quien in agresores) {

        if ( !quien ){
            continue;
		}
		if (-1!=member_array(DAME_CONQUISTADOR_ZONA("boreal"), ({quien->dame_ciudadania(),quien->dame_gremio(),quien->dame_familia()})))
		{
			continue;
		}
		
		__debug_x(sprintf("varios asesinos: %s, puntos: %d\n",quien,PUNTOS_CONQUISTA/sizeof(agresores)),"arad-gorthor");
		AJUSTAR_CONQUISTAS(find_player(quien),"boreal",PUNTOS_CONQUISTA/sizeof(agresores));
    }
	

    return ::do_death(b);
}
