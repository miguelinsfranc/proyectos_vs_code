//Rezzah 17/11/2009 (basado en el golem de piedra)
// Grimaek 13/06/2013 Sustituido add_attack_spell por anyadir_ataque_pnj. arregladas maestrias

#include "../path.h";

inherit "/obj/conversar_npc";

//Está controlado directamente por glachdavir, es más rápido e inteligente que un golem normal
int dame_golem() { return 1; }

void setup(){
	set_name("guardian");
	set_short("Guardián del %^RED%^Cónclave de Lava%^RESET%^");
	set_main_plural("Guardianes del %^RED%^Cónclave de Lava%^RESET%^");
	add_alias(({"golem","lava", "guardian", "guardián", "conclave"}));
	add_plural(({"golems","lavas", "guardianes", "conclave"}));
	set_long("Estás ante un artefacto elaborado por un verdadero maestro artificiero, su corpulencia y fortaleza son como un muro a la vista, palpables e infranqueables. Es un golem de lava como el resto de los que pueblan el Laberinto pero con muchas menos rocas que los otros, casí como un ente de magma palpitante hirviendo en mortal movimiento, esto le da mayor soltura y ligereza en sus movimientos aunque parece más desprotegido que el resto.\n");

	fijar_bando("malo");
	fijar_religion("gurthang");
	fijar_raza("golem");
	fijar_clase("soldado");

	fijar_tamanyo_racial(9);

	fijar_estilo("pelea");

	fijar_alineamiento(0);
	fijar_genero(1);

	fijar_fue(40);
	fijar_des(16);
	fijar_con(30);
	fijar_int(16);
	fijar_sab(3);
	fijar_car(25);

	fijar_nivel(50);
	add_loved("religion", ({"gurthang"})); //Solo a los gurthang no les pega
	set_aggressive(3);
	ajustar_carac_tmp("daño",50,0,0,0);
	ajustar_bo(350);
	ajustar_bp(100);
	add_clone(ARMAS+"hoja_lava.c", 1);
	fijar_maestria("Cortantes medias", 100);

	
	anyadir_ataque_pnj(40, "ataquedoble");
	anyadir_ataque_pnj(30, "golpecertero");
	anyadir_ataque_pnj(40, "oleada");
	anyadir_ataque_pnj(30, "barrido");
	anyadir_ataque_pnj(30, "estallido de fuego");
	
	add_static_property("libre-movimiento", 1);
	add_static_property("sin miedo", 1);
	add_static_property("fuego",50);
	nuevo_lenguaje("negra",100);
	nuevo_lenguaje("dendrita",100);
	fijar_lenguaje_actual("negra");
	nuevo_dialogo("nombre", ({
		"'¿Qué infiernos importa mi nombre? Lo único importante es que me libereis cuánto antes.",
		":te apremia para que te acerques."
	}));
	nuevo_dialogo("libereis", ({
		":te mira entrecerrando los ojos, con gesto sospechoso.",
		"'¿No habéis venido a eso? ¿No habeis sentido mi llamada?"
	}), "nombre");
	nuevo_dialogo("llamada", ({
		"'Llevo milenios buscándoos para que resolvais el maldito entuerto en el que me metisteis."
	}), "libereis");
	nuevo_dialogo("entuerto",({
		"'No entiendo como te permiten vivir con tanta estupidez como exhibes...",
		":se muestra enormemente irritado.",
		"'Tus ancestros me intentaron invocar con tanta incompetencia que me dejaron encerrado por siempre, muriendo todos..."
	}), "llamada");
	nuevo_dialogo("invocar", ({
		"'¿Aún no lo has entendido? Soy Glach'Davir.",
		":deja melodramáticamente que el pánico que provoca su nombre flote en el aire unos instante.",
		"' Glach'Davir, Señor de Mae'Ree, invocado por Burz el Estúpido Antes del Cataclismo, y tú..." //Ahora tendré que hacer Mae'Ree la torre del demonio ..  cachis en la mar
	}), "entuerto");
	nuevo_dialogo("yo", ({
		"'Tú, has venido a liberarme o a morir. Decídelo ya.",
	}), "invocar");
	nuevo_dialogo("liberar", ({
		":sonríe exhibiendo una magnífica e ígnea hilera de colmillos.",
		"'Eso pensaba..., solo aquel que hable primero conmigo podría pasar, te enseñaré al verdadero Glach'Davir",
		"abrir"
	}), "yo");
	nuevo_dialogo("morir", ({
		":se muestra perplejo.",
		"'¿Has venido a morir?",
		"'¡Maldita sea! PUES QUE ASÍ SEA  ¡¡¡ M U E R E !!!",
		"inicia_luchas"
	}), "yo");
	
	habilitar_conversacion();
	init_equip();
}

void abrir(object pj) {
	tell_accion(environment(), query_short()+" se gira hacia la pared a sus espaldas y al apróximarse a ella se convierte en una puerta que se abre a su voluntad, con un gesto indica a "+pj->query_short()+" y sólo a él que se adentre en la cueva.\n", "", ({pj}), 0);
	tell_object(pj, query_short()+" se gira hacia la pared a sus espaldas y al apróximarse a ella se convierte en una puerta que se abre a su voluntad, con un gesto te indica que te adentres en la cueva.\n");
	environment()->add_exit(E, CAVERNAS+"conclave89.c", "corridor");
	environment()->renew_exits();
	pj->add_static_property("glachdavir_paso_autorizado",1);
}

int weapon_damage(int cantidad,object atacante,object arma,string ataque){
	if( !arma || arma->dame_encantamiento()<5)
	{
		string nom= arma ? arma->query_short(): "arma";
		
		tell_object(atacante,"Tu "+nom+" no parece dañar lo más mínimo a "+this_object()->query_short()+".\n");
		tell_accion(environment(atacante),"El ataque de "+atacante->query_short()+" con su "+nom+" no parece dañar a "+this_object()->query_short()+".\n","Oyes ruidos de golpes.\n",({atacante}),atacante);
		return 0;
	}
	return ::weapon_damage(cantidad,atacante,arma,ataque);
}

int unarmed_damage(int pupa,string estilo,object atacante) { return 0; }
int spell_damage(int pupa,string tipo,object lanzador,object hechizo) {//SE recupera igual que antes y los elementos que le dañan le pican mucho más (muere más rapido pero hace más daño)
	int danio;
	if (tipo == "fuego" || tipo == "tierra" || tipo == "acido") { //Se cura con tierra, fuego y acido XD es lava!
		danio = to_int(floor(-pupa/2));
		ajustar_pvs(danio, lanzador);
		return danio;//teóricamente si les curan les baja xp 
	} 
	if (tipo == "agua" ||tipo == "frio") {//Le hacen un 200% más de daño si es agua o frio
		danio = to_int(ceil(pupa*2));
		::spell_damage(danio, tipo, lanzador, hechizo);
		return danio;
	}
	//En cualquier otro caso suda de los ataques mágicos
	return 0;
}

void inicia_luchas() {
    foreach(object ob in all_inventory(environment())) {
        if (living(ob) && ob != this_object()) attack_ob(ob);
    }
}

int do_death(object asesino) {
	tell_room(environment(), "Cuando "+query_short()+" muere una neblina gris se forma en la pared este disolviendo la pared y abriendo la entrada.\n");
	environment()->add_exit(E, CAVERNAS+"conclave89.c", "corridor");	
	environment()->renew_exits();
	return ::do_death(asesino);
}
