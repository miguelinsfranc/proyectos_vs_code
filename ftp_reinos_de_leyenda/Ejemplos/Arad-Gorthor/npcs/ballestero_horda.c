// Dunkelheit 10-11-2009

#include "../path.h";
inherit "/obj/monster.c";

string *rooms_apuntables = ({ });

int check_anyone_here() { return 1; }

void fijar_rooms_apuntables(string *str) { rooms_apuntables = str; }
string *dame_rooms_apuntables() { return rooms_apuntables; }

void setup()
{
	set_name("ballestero");
	set_short("Ballestero de la Horda");
	add_alias("orco");
	set_long("Este orco de excepcionales características pertenece a la élite de tiradores de Arad-Gorthor."
	" Su innata puntería junto a un duro entrenamiento lo convierten en un certero asesino capaz de sacarle "
	"un ojo a un halfling a más de doscientos pasos, acto que, por la dura expresión de sus ojos, le produciría "
	"un placer abrumador.\n");
	set_main_plural("Ballesteros de la Horda");
	add_plural(({"ballesteros", "orcos"}));
	
	fijar_altura(180);
	fijar_peso_corporal(96500);
	
	fijar_raza("orco");
	fijar_clase("tirador");
	fijar_religion("gurthang");
	
	fijar_fue(18);
	fijar_con(18);
	fijar_des(18);
	fijar_int(4);
	fijar_sab(4);
	fijar_car(15);
	
	fijar_nivel(30);
	fijar_alineamiento(18500);
	
	add_clone("/baseobs/proyectiles/ballesta", 1);
	add_clone("/baseobs/proyectiles/saeta", 20 + random(6));
	add_clone(BARMADURAS + "botas", 1);
	add_clone(BARMADURAS + "cuero", 1);
	add_clone(BARMADURAS + "cinturon", 1);
	add_clone(BARMADURAS + "capucha", 1);
	
	fijar_maestria("Armas de proyectiles", 60);

	init_equip();
	
	ajustar_dinero(3 + random(3), "platino");
	
	ajustar_bo(90);
	
	ROL_NPCS->definir(this_object());
}

int comprobar_entorno(object victima)
{
	return victima && environment(victima) && member_array(base_name(environment(victima)), dame_rooms_apuntables()) != -1;
}

object *limpiar_victimas(object *victimas)
{
	return filter_array(victimas, (: comprobar_entorno :));
}

object buscar_victima()
{
	if (sizeof(query_attacker_list())) {
		return element_of(query_attacker_list());
	}
	if (sizeof(query_call_outed())) {
		object *outeds = limpiar_victimas(query_call_outed());
		if (sizeof(outeds)) {
			return element_of(outeds);
		}
	}
	return 0;
}

void attack_ob(object ob)
{
	if (!query_heart_beat(this_object())) {
		set_heart_beat(1);
	}
	::attack_ob(ob);
}

void heart_beat()
{
	object victima;

	if (!query_static_property("pasivo")) {
		add_static_property("pasivo", 1);
		habilidad("disparopasivo", 0);
	}
	
	if (!query_time_remaining("bloqueo-Disparocertero") && victima = buscar_victima()) {
		habilidad("disparocertero", victima);
	}

	::heart_beat();
}
