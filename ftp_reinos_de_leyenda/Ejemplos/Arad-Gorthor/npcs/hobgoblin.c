// Dunkelheit 15-11-2009

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	set_name("hobgoblin");
	set_short("Hobgoblin");
	set_long("Los hobgoblins son criaturas de aspecto casi idéntico al de un goblin. La única "
	"diferencia visible es el color de su piel, que suele ser de un rojo ceniza. Además de esta "
	"peculiaridad, los hobgoblins esconden una mente más aguda e inteligente que la de sus "
	"paisanos verdosos, y su cuerpo goza de una mayor fuerza y agilidad. Pese a estas ventajas "
	"se han convertido en una raza marginal, al borde de la extinción.\n");
	set_main_plural("Hobgoblins");
	add_plural("hobgoblins");
	
	fijar_altura(80 + random(20));
	fijar_peso_corporal(40000 + random(20000));
	
	fijar_subraza("hobgoblin");
	fijar_clase("aventurero");
	fijar_religion("gurthang");
	
	fijar_fue(14);
	fijar_con(17);
	fijar_des(10);
	fijar_int(5);
	fijar_sab(3);
	fijar_car(3);
	
	fijar_nivel(20 + random(5));
	fijar_alineamiento(1000);
	
	set_aggressive(1);
	
	load_chat(10, ({
            ":da vueltas de un lado al otro sin saber muy bien qué hacer.",
		":traza unos garabatos con su pie en la tierra del suelo.",
	}));
	
	load_a_chat(30, ({
		":se revuelve como un perro rabioso.",
		"'¡Han descubierto el campamento! ¡avisad a los guerreros!",
		"'¡Intrusos! ¡dad la voz de alarma!",
		"'¡Waaaargh! ¡esto no quedará así!",
		":se defiende con uñas y dientes.",
	}));
	
	ajustar_dinero(roll(2, 10), "plata");
}
