// Dunkelheit 19-01-2010
// Grimaek 13/06/2013 Sustituido add_attack_spell por anyadir_ataque_pnj

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	string raza = element_of(({"orco", "semi-orco", "goblin", "gnoll"}));
	
	set_name("soldado");
	set_short("Soldado de la Horda");
	set_main_plural("Soldados de la Horda");
	add_plural(({"soldados", "hordas"}));
	fijar_genero(1);

	fijar_altura(150 + random(30));
	fijar_peso_corporal(75000 + random(15000));

	fijar_raza(raza);
	fijar_clase("soldado");
	fijar_religion("gurthang");

	set_long("Observas con detenimiento a un orgulloso soldado "+raza+" de la horda. "
	"Embutido en una indumentaria militar modesta, este guerrero de la Horda Negra "
	"puede presumir de haber salido con vida de más de una batalla, acumulando así "
	"méritos suficientes como para ascender al rango de soldado. Rango que, por otra "
	"parte, es el que predomina en la horda, pues casi ocho de cada diez integrantes "
	"lo ostentan. Pese a ello, cualquiera de estas anónimas bestias puede convertirse "
	"en un enemigo letal, incluso para el más laureado de los héroes; nunca hay que "
	"bajar la guardia frente a estos escurridizos, traicioneros e ingeniosos "
	"goblinoides.\n");
	
	fijar_fue(18);
	fijar_extrema(100);
	fijar_con(18);
	fijar_des(14);
	fijar_int(14);
	fijar_sab(8);
	fijar_car(8);
	
	fijar_nivel(30 + random(6));
	fijar_alineamiento(5000 + random(2000));
	
	switch (random(10)) {
		case 0..2:
			add_clone(BARMADURAS + "mallas", 1);
			break;
		default:
		case 3..8:
			add_clone(BARMADURAS + "placas", 1);
			break;
		case 9:
			add_clone(BARMADURAS + "campaña", 1);
			break;
	}

	add_clone(BARMADURAS + "yelmo", 1);

	switch (random(8)) {
		default:
		case 0:
			add_clone(BARMAS + "espada_bastarda", 1);
			add_clone(BESCUDOS + "gran_escudo", 1);
			ajustar_maestria("espada bastarda", 60);
			break;
		case 1:
			add_clone(BARMAS + "tridente", 1);
			add_clone(BESCUDOS + "gran_escudo", 1);
			ajustar_maestria("tridente", 60);
			break;
		case 2:
			add_clone(BARMAS + "latigo", 1);
			add_clone(BESCUDOS + "gran_escudo", 1);
			ajustar_maestria("latigo", 60);
			break;
		case 3:
			add_clone(BARMAS + "luz_del_alba", 1);
			add_clone(BESCUDOS + "gran_escudo", 1);
			ajustar_maestria("luz del alba", 50);
			break;
		case 4:
			add_clone(BARMAS + "hacha_doble", 1);
			ajustar_maestria("hacha doble", 60);
			break;
		case 5:
			add_clone(BARMAS + "alabarda", 1);
			ajustar_maestria("alabarda", 60);
			break;
		case 6:
			add_clone(BARMAS + "martillo_a_dos_manos", 1);
			ajustar_maestria("martillo a dos manos", 60);
			break;
		case 7:
			add_clone(BARMAS + "cadena_larga", 1);
			ajustar_maestria("cadena larga", 60);
			break;
	}
	
	if (random(2)) {
		add_clone(BARMADURAS + "brazalete", 1);
		add_clone(BARMADURAS + "brazalete_izq", 1);
	}
	if (!random(5)) {
		add_clone(BARMADURAS + "anillo", 1);
	}
	if (!random(5)) {
		add_clone(BARMADURAS + "amuleto", 1);
	}
	if (!random(8)) {
		add_clone(BARMADURAS + "collar", 1);
	}
	
	switch (random(10)) {
		case 0..2:
			add_clone(BARMADURAS + "botas", 1);
			break;
		case 9:
			add_clone(BARMADURAS + "botas_campaña", 1);
			break;
	}
	
	init_equip();
	
	ajustar_dinero(roll(1, 8), "estaño");
	
	ajustar_bo(80);
	
	ROL_NPCS->definir(this_object());
	
	load_chat(30, ({
		"'Espero sacar un buen botín de la próxima incursión... ¡tengo ganas de tener un arma nueva!",
		":patrulla inmerso en un profundo estado de concentración, analizando cada metro del terreno que le rodea.",
		"'Los guerreros de la Horda Negra no tenemos nada que ver con los de Ejército Negro... ellos no sabían ni ubicarse el trasero en un dibujo",
		"'Tras la unión de nuestras fuerzas, goblins, orcos, semi-orcos y gnolls somos la mayor fuerza del mal en Eirea",
		"'¡Qué se preparen nuestros enemigos, pues ninguno de nosotros teme a la muerte!",
		"'¡Eh! ¡a ver si mira por dónde vas!",
		":rebaña el sudor de su frente con sus sucias garras.",
		"'Es fácil ver visiones en el desierto, el cementerio de máquinas de asedio me pone los pelos de punta",
		"'Esperemos que mañana nos visiten menos peregrinos que hoy, me pone de los nervios tanta gente, ¡me da ganas de matar!",
		"'Lord Gurthang nos ha bendecido con una fuerza sin parangón y una determinación inquebrantable, ¡no podemos fallarle!",
	}));
	
	load_a_chat(50, ({
		"'¡Prepárate para conocer a tu creador!",
		"'¡Te enseñaré cómo combate un soldado de verdad!",
		"'¡Te despellejaré y venderé tu carne al sacamantecas, así me sacaré unas monedas!",
		":profiere gritos de guerra y te dedica gestos obscenos.",
		"'¡Con tu muerte tengo el ascenso a suboficial asegurado! ¡Así que no te hagas de rogar!",
		"'¡Maldita basura, te arrancaré los ojos y te los meteré por el trasero!",
		":requiere inmediatamente la presencia de más soldados de la Horda Negra.",
		"'¡Me da igual quién seas, cuando te mate nadie se acordará de ti!",
		"'¡Waaaaaaargh! ¡Por Lord Gurthang! ¡Por la Guerra!",
	}));
	
	anyadir_ataque_pnj(15, "golpecertero");
}

string dame_noconversar_personalizado()
{
	return "El soldado está concentrado en su tarea y no te presta la más mínima atención.\n";
}
