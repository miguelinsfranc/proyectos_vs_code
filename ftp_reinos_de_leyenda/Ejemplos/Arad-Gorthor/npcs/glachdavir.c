//Grimaek 17/05/2023 Testeado por Gholxien y Hazrakh 
#include "../path.h";
#define SALAS ({"conclave89","conclave90","conclave91","conclave92","conclave93","conclave94","conclave95","conclave96"})
inherit "/obj/monster.c";


int dame_no_limpiable() { return query_timed_property("no_limpiable"); } //Esto evita el reset de la sala

//void conjuracion_infernal()
function conjuracion_infernal(object *objetivos)
{
 	do_emote("sonríe malignamente mientras chasquea con su Látigo %^RED%^Flamígero%^RESET%^ el suelo y comienza a aparecer una burbuja de lava.\n");
	
	call_out("conjuracion_infernal2", 2);
}

void conjuracion_infernal2()
{
	int i=random(3)+1,x;
 
	tell_accion(ENV(TO),
		"Ves como la burbuja de lava creada por "+query_short()+" explota, arrojando "+i+" golems de lava a su alrededor.\n",
		"Escuchas un ruido burbujeante.\n");	
	for(x=i;x--;) {
		clone_object(NPCS+"golem_glachdavir.c",1)->move(ENV(TO));
	}
}

void estalactitas_de_piedra2()
{
	object *todos, *objetivos = ({});
	int i,x, cuantos;
 
	//Buscamos un objetivo...
	cuantos = 0;
	for(x=89; x<=96; x++) {//89 a 96  
        for(i = sizeof( todos = all_inventory( load_object(CAVERNAS+"conclave"+x+".c") ) ); i--;) {
            if(living(todos[i]) && !todos[i]->query_hidden() && todos[i]->query_player() && todos[i] != this_object()) {
                objetivos += ({todos[i]});
                cuantos++;
            }
        }
    }
	
	if(cuantos = 0) {
       tell_room(ENV(TO),query_short()+", desconcertado, observa que no pasa nada a su alrededor.\n");
       return;
    }
	
	tell_accion(ENV(TO),
		"Ves como el terremoto provocado por "+query_short()+" crea desprendimientos de tierra que caen sobre los presentes.\n",
		"Escuchas un ruido estruendoso.\n");	
		
	for(i = sizeof(objetivos); i--;) {

	
		if(!objetivos[i]->dame_ts("agilidad", 0, "tierra", 0)) {
			//Falla la tirada de salvacion...


			if(objetivos[i]->spell_damage(-(500 + random(300)),"tierra",TO,0) == -300 && objetivos[i]->spell_damage(-(700 + random(300)),"magico",TO,0) == -300 ) {
				return; 
			}

			tell_object(objetivos[i],"¡Una %^ORANGE%^estalactita%^RESET%^ te alcanza de lleno y te provoca daños internos!\n"); 
			tell_room(ENV(objetivos[i]),"¡Escuchas el grito agónico de "+objetivos[i]->query_short()+" mientras una %^ORANGE%^estalactita%^RESET%^ le golpea de lleno!\n",objetivos[i]);
			if(!objetivos[i]->dame_ts("fuerza bruta", 0)) {
				tell_object(objetivos[i],"¡La fuerza del impacto de la %^ORANGE%^estalactita%^RESET%^ te ha tirado por el suelo! ¡Deberás levantarte!\n"); 
				load_object("/cmds/npc/agacharse.c")->agacharse(objetivos[i]);
			}
		}
		else {
			tell_object(objetivos[i],"¡Una %^ORANGE%^estalactita%^RESET%^ choca contra el suelo a tu lado! Lo has esquivado por poco.\n");
			tell_room(ENV(objetivos[i]),query_short()+" mira ofuscado a "+objetivos[i]->query_short()+" después que esquivara la %^ORANGE%^estalactita%^RESET%^ que ha golpeado el suelo a su lado.\n",objetivos[i]);
		}
	}
}

//void estalactitas_de_piedra()
function estalactitas_de_piedra(object *objetivos)
{
	do_emote("suelta una carcajada mientras golpea con su Látigo %^RED%^Flamígero%^RESET%^ una columna de piedra.\n");
	//estalactitas_de_piedra2();
	call_out("estalactitas_de_piedra2", 2);
}

void setup(){
	set_name("glachdavir");
	set_short("%^RED%^Glach'Davir, Señor de Mae'Ree%^RESET%^");
	set_main_plural("Dos %^RED%^Glach'Davir, Señores de Mae'Ree%^RESET%^");
	add_alias(({"glach","glachdavir", "señor"}));
	add_plural(({"glachs","glachdavirs", "señores"}));
	set_long("Los grandes ojos rojos de este demonio desprenden rabia y dolor hacia todo lo que se van encontrando. Su arrugada piel,"
			" desnuda de cintura para arriba, es negra como el carbón y curtida como el cuero. Dos cuernos apuntan al cielo "
			"retorciéndose desde su frente hacia arriba. No modifica el semblante siniestro en ningún momento y de vez en cuando su "
			"aliento deja salir un vaho caliente de su interior que le hace humedecer los pelos de la barba cana.\n");

	fijar_raza("demonio");
	fijar_clase("soldado");
    fijar_altura(780);
    fijar_carac("tamaño", 19); 
	
	fijar_fe(200);
	fijar_alineamiento(25000);
	
	fijar_tamanyo_racial(9);
	fijar_bando("mercenario");
	fijar_estilo("pelea");

	fijar_genero(1);

	fijar_fue(40);
	fijar_des(16);
	fijar_con(30);
	fijar_int(40);
	fijar_sab(3);
	fijar_car(25);
    add_zone("conclave");
    set_move_after(10, 6);
	
	fijar_nivel(65 + random(15));
	set_aggressive(3);
	ajustar_carac_tmp("daño",250,0,0,0);
	ajustar_carac_tmp("poder_magico",650,0,0,0);
	ajustar_bo(350);
	ajustar_bp(100);
	fijar_maestrias((["Lacerantes ligeras": 100]));
	add_spheres((["mayor":({"evocacion","conjuracion"})]));
	anyadir_ataque_pnj(30, "conjuracion_infernal");
	anyadir_ataque_pnj(30, "estalactitas_de_piedra");
	anyadir_ataque_pnj(65, "bola encadenada de fuego");
	anyadir_ataque_pnj(65, "flecha de llamas");	
	anyadir_ataque_pnj(40, "ataquedoble");
	anyadir_ataque_pnj(30, "golpecertero");

	add_static_property("libre-movimiento", 1);
	add_static_property("sin miedo", 1);
	add_static_property("fuego",50);
	
	fijar_pvs_max(105000);
	fijar_pe_max(8500);
    fijar_memoria_jugador(1);

    quitar_lenguaje("dendrita");
    nuevo_lenguaje("negra", 100);
    fijar_lenguaje_actual("negra");

    fijar_armadura_natural(90);
    fijar_armadura_magica(90);

    add_property("sin miedo", 1);
    add_property("no_desarmar", 1);
	add_loved("raza","golem");
	
	add_clone(ARMAS      + "latigo_flamigero.c" , 1);
	add_clone("/d/golthur/items/escudo_inmaterial.c" , 1);
	add_clone(BARMADURAS + "completa.c"        , 1);
	add_clone(BARMADURAS + "gran_yelmo.c"      , 1);
	add_clone(BARMADURAS + "guantelete.c"      , 1);
	add_clone(BARMADURAS + "guantelete_izq.c"  , 1);
	add_clone(BARMADURAS + "botas_guerra.c"    , 1);
	add_clone(BARMADURAS + "grebas_metalicas.c", 1);
	add_clone(BARMADURAS + "brazal.c"          , 1);
	add_clone(BARMADURAS + "brazal_izq.c"      , 1);
	
    fijar_objetos_morir(ARMADURAS + "capa_maeree.c");//PENDIENTE
    fijar_objetos_morir(({"tesoro", ({"arma", "armadura"}), ({9, 10}), 1}));	
	init_equip();
	
    load_a_chat(50, ({
        ":extiende sus alas ígneas, lanzando restos de lava que se funden con el suelo.",  
        ":ruge con fiereza, expulsando una nube de acido incandescente.", 
        ":envuelve la estancia con un calor abismal. Se hace insoportable mantenerse cerca del demonio.", 
        ":exhala hálito incandescente que hiede a azufre y a muerte.", 
        "'¡No tenéis escapatoria! ¡Permanecereís atrapados conmigo!", 
        "'¡Mi poder procede del abismo de Mae'Ree! ¡Preparáos para sentir el verdadero significado de fuego!", 
    }));

	add_timed_property("no_limpiable",1,4*3600);
	add_timed_property("no_clean_up",1,4*3600);	
}

// No gasta energía
int ajustar_pe(int i) { return i < 0 ? dame_pe() : ::ajustar_pe(i); }

int weapon_damage(int cantidad,object atacante,object arma,string ataque){
	if( !arma || arma->dame_encantamiento()<10)
	{
		string nom= arma ? arma->query_short(): "arma";
		
		tell_object(atacante,"Tu "+nom+" no parece dañar lo más mínimo a "+query_short()+".\n");
		tell_accion(environment(atacante),"El ataque de "+atacante->query_short()+" con su "+nom+" no parece dañar a "+query_short()+".\n","Oyes ruidos de golpes.\n",({atacante}),atacante);
		return 0;
	}
	return ::weapon_damage(cantidad,atacante,arma,ataque);
}

int unarmed_damage(int pupa,string estilo,object atacante) { return 0; }
int spell_damage(int pupa,string tipo,object lanzador,object hechizo,object arma,string nombre_ataque,mapping cfg_ataque) {//SE recupera igual que antes y los elementos que le dañan le pican mucho más (muere más rapido pero hace más daño)
	int danio;
	if (tipo == "fuego" || tipo == "tierra" || tipo == "acido") { //Se cura con tierra, fuego y acido XD es lava!
		danio = to_int(floor(-pupa/2));
		ajustar_pvs(danio, lanzador);
		tell_object(lanzador,"El hechizo tipo "+tipo+" lanzado parece revitalizar a "+query_short()+".\n");
		return danio;//teóricamente si les curan les baja xp 
	} 
	if (tipo == "agua" ||tipo == "frio") {//Le hacen un 200% más de daño si es agua o frio
		danio = to_int(ceil(pupa*2));
		::spell_damage(danio, tipo, lanzador, hechizo, arma, nombre_ataque, cfg_ataque);
		return danio;
	}
	//En cualquier otro caso suda de los ataques mágicos
	return 0;
}

void inicia_luchas() {
    foreach(object ob in all_inventory(environment())) {
        if (living(ob) && ob != this_object()) attack_ob(ob);
    }
}

int do_death(object asesino)
{ 
	object sala_ob,b;
	if(!asesino) {
		return ::do_death();
	}
	
	tell_object(asesino,"Cuando propinas el golpe mortal a "+query_short()+" este cae dolorido y sus llamas se apagan.\n");
	tell_accion(ENV(TO),asesino->query_short()+" propina el golpe mortal a "+query_short()+" y este cae al suelo apagándose sus llamas.\n",
		asesino->query_short()+" propina el golpe mortal a "+query_short()+" y este cae al suelo apagándose sus llamas.\n",({asesino}),asesino);

    if (asesino)
		write_file(LOGS+"glachdavir.log",ctime() + " " + asesino->query_name()+" mato a "+query_short()+" ["+asesino->dame_nivel()+"].\n");

    else
        write_file(LOGS+"glachdavir.c", ctime()+" - "+query_short()+" matado por la madre del topo\n");
	
	if ( b = secure_present(ARMAS+"latigo_flamigero.c", TO) )
            b->dest_me();	
	//Limpiamos las salas:
	foreach(string sala in SALAS){
		sala_ob=load_object(CAVERNAS+sala);
		if(sala_ob){
			sala_ob->remove_timed_property("no_limpiable");
			sala_ob->remove_timed_property("no_clean_up");
		}
	}	
	return ::do_death(asesino); 
}

void heart_beat()
{
    object entorno = environment();

    // Quitamos la lacra del silencio
    if (entorno->query_silencio())
    {
        tell_object(this_object(), "¡Gritas con todas tus fuerzas, destrozando los hechizos de silencio que afectaban a
tu entorno!\n");
        tell_accion(entorno, "¡"+query_short()+" grita con todas sus fuerzas, destrozando el hechizo de silencio!\n", 
			"Un grito atronador destroza tus tímpanos.\n", ({this_object()}), this_object());
        entorno->destruir_silencio();
    }

    ::heart_beat();
}