// Altaresh 19
#include "../../path.h"
#include <guardias.h>
#include <conquistas.h>
#include <estatus.h>
inherit _GUARDIAS_PATH + "guardia_soldado.c";

/**
 * Funciones nativas
 */
void setup()
{	
    fijar_organizacion_guardia(DAME_CONQUISTADOR_ZONA("boreal"));
}
/**
 * Funciones de guardias
 */
void setup_personalizacion(object org)
{
    string name  = "gerrer" + dame_a();
    string corto = dame_genero() == 2 ? "campeona" : "campeón";
    string donde = org ? " mercenario de " + org->dame_corto() : "";
	
	::setup_personalizacion(org);

    set_name(name);
    set_short(capitalize(corto) + donde);
    set_main_plural(capitalize(corto) + donde);
    generar_alias_y_plurales();
}
/* los guardias no tunean
void setup_poder(object org)
{
   ::setup_poder(org);
    fijar_nivel(35);
    fijar_pvs_max(4500);
	aprender_mejora_ataque("golpecertero");
}
*/
