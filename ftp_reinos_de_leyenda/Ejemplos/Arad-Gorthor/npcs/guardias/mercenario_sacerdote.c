// Satyr 16.04.2015
// Satyr 08/05/2019 02:09
#include "../../path.h"
#include <guardias.h>
#include <conquistas.h>
#include <estatus.h>
inherit _GUARDIAS_PATH + "guardia_sacerdote.c";

/**
 * Funciones nativas
 */
void setup()
{
	
    fijar_organizacion_guardia(DAME_CONQUISTADOR_ZONA("boreal"));
}
/**
 * Funciones de guardias
 */
void setup_personalizacion(object org)
{
	
	
    string a     = dame_vocal();
    string name  = a == "a" ? "cleriga" : "clerigo";
    string corto = dame_genero() == 2 ? "clériga" : "clérigo";
    string donde = org ? " mercenario de " + org->dame_corto() : "";
  
		
	::setup_personalizacion(org);

    set_name(name);
    set_short(capitalize(corto) + donde);
    set_main_plural(capitalize(corto) + donde);
    generar_alias_y_plurales();



}
/* los guardias no se tunean
void setup_poder(object org)
{
	::setup_poder(org);
    fijar_nivel(30);
    fijar_pvs_max(3000);
	add_spell("curar heridas moderadas");
   
}
*/
void setup_equipo(object org)
{
	::setup_equipo(org);
	init_equip();
}
