//Altaresh 19 continuando el curro de Nohur
#include "../path.h";
#include INCLUDES+"interaciones.h"
inherit "/obj/monster.c";

void setup() {
    set_name("tabernero");
    set_short("Groghug, el Tabernero");
    set_main_plural("Groghug, los taberneros");
    generar_alias_y_plurales();
    fijar_raza("goblin");
    set_long("Un sucio tabernero goblin, con las manos quemadas por la plancha, sudoroso "
    "y maloliente que se escupe con frecuencia sobre la plancha o la comida, acto que "
    "algunos corean con risotadas.\n");
    set_random_stats(14,18);

    fijar_religion("gurthang");
    fijar_clase("aventurero");
    fijar_ciudadania("golthur");
    nuevo_lenguaje("negra", 100);
    fijar_nivel(10 + random(16));
    fijar_altura(100+random(50));
    fijar_peso_corporal(40000+random(20)*1000);


    load_chat(30, ({
    ":deja con tranquilidad que el asqueroso moco que cae de su nariz se pose sobre la comidad.",
    "'Aquí pasamos cualquier cosa a la plancha... o a cualquiera",
    "'¡Última ronda y os vais todos desgraciados!",
    "#iniciar_cualquier_interaccion",

    }));
    load_a_chat(50, ({
    "'¡Cuando te aplaste la cara serás el primer plato de la cena!",
    "'¡Hoy serás la sorpresa del menú!"
    }));
    nuevo_lenguaje("negra",100);
    fijar_nivel(20+random(10));
    ajustar_dinero(1+random(10),"bela");

    PONER_INTERACCIONES(TO, "cangrejo",({"capturar_cangrejo"}));
}
 int puedo_iniciar_interaccion(string id){
	 
	int res; //variable que guardara el resultado de la funcion padre

	//Comprobaciones iniciales
	//Si no tenemos ide... fuera
	if (!id)
		return 0;	
	
	//Si la funcion padre devuelve 0.... fuera
	if (!res=::puedo_iniciar_interaccion(id)){
		__debug_x(sprintf("res de funcion padre= %d",res),"arad-gorthor");
		return 0;
	}
	res=COMPROBAR_INTERACCION(TO,id);
	__debug_x(sprintf("res final= %d",res),"arad-gorthor"); 
	return res=COMPROBAR_INTERACCION(TO,id);
	
 }