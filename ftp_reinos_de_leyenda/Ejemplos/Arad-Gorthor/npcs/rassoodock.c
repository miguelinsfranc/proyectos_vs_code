// Dunkelheit 15-11-2009
// Grimaek 13/06/2023 comento para anular inteligencia del NPC creada por Dunkel para usar la nueva funcion anyadir_ataque_pnj
#include "../path.h";
//#include "../include/inteligencia.h";
inherit "/obj/monster.c";



int check_anyone_here() { return 1; } // Para no perder el HB

void setup()
{
	set_name("rassoodock");
	set_short("Rassoodock el Aojador");
	add_alias(({"aojador", "hobgoblin"}));
	set_long("Es un diminuto hobgoblin escondido tras unas vestimentas tan esperpénticas que sólo "
	"pueden significar una cosa: él es el líder. Una larga capa roja, sembrada con plumas de muchos "
	"colores, daría a cualquier otra criatura un aspecto carnavalesco... pero este no es el caso. "
	"Embutido en una cota de cuero obviamente encantada, viste como colofón una cinta con una enorme "
	"pluma, mucho más grande que las que pueblan la capa. Su rostro, "
	"rollizo y de un rojo más intenso que ningún otro hobgoblin, parece haberse quedado permanentemente "
	"atrapado en una expresión de cólera. Uno de sus ojos tiene un visaje aterrador, y es el que le ha "
	"dado el sobrenombre de \"el Aojador\", pues concede males de ojo sin ningún reparo.\n");
	set_main_plural("Rassoodocks los Aojadores");
	add_plural(({"aojadores", "hobgoblins"}));
	
	fijar_altura(115);
	fijar_peso_corporal(43500);
	
	fijar_subraza("hobgoblin");
	fijar_clase("chaman");
	fijar_religion("gurthang");
	fijar_longitud_pelo("largo"); // Para que pueda ponerse la pluma
	
	fijar_fue(18);
	fijar_con(18);
	fijar_des(18);
	fijar_int(13);
	fijar_sab(32);
	fijar_car(17);
	
	fijar_nivel(50);
	fijar_alineamiento(80000);
	
	add_property("grupoaojador", 1);	
	add_loved("propiedad", "grupoaojador");	
	add_loved("raza", "goblin");

	set_aggressive(1);
	
	add_clone(ARMAS + "baston_caprino", 1);
	add_clone(ARMADURAS + "pluma_roc", 1);
	add_clone(ARMADURAS + "peto_totemista", 1);
	add_clone(BARMADURAS + "amuleto", 1);
	add_clone(BARMADURAS + "pendiente", 1);
	add_clone(BARMADURAS + "anillo", 1);
	add_clone(BARMADURAS + "sandalias", 1);
	add_clone(BARMADURAS + "capa", 1);
	init_equip();
	
	fijar_pvs_max(18000);
	fijar_pe_max(9800);

	fijar_memoria_jugador(1);


	add_property("no_desarmar", 1);
	add_property("sin miedo", 1);
	add_property("no_desfondable", 1);
	add_property("abrigado", 1);
	add_property("libre-accion", 1);
	add_property("no_lanzar", 1);
	
	fijar_fe(220);
	
    anyadir_ataque_pnj(30, "curar heridas ligeras"  , (: dame_pvs() <= dame_pvs_max()*0.9 :) , (["objetivo": 2]));
	anyadir_ataque_pnj(50, "retener persona");	
    anyadir_ataque_pnj(30, "curar heridas moderadas", (: dame_pvs() <= dame_pvs_max()*0.8 :), (["objetivo": 2]));
	anyadir_ataque_pnj(50, "mordisco de la vibora");	
    anyadir_ataque_pnj(30, "curar heridas serias"   , (: dame_pvs() <= dame_pvs_max()*0.7 :), (["objetivo": 2]));
	//anyadir_ataque_pnj(50, "hervor de la sangre");	
    anyadir_ataque_pnj(30, "curar heridas criticas", (: dame_pvs() <= dame_pvs_max()*0.6 :), (["objetivo": 2]));
	anyadir_ataque_pnj(50, "gritos ancestrales", (: ! secure_present("/w/grimaek/arad-gorthor/npcs/campeon_hobgoblin",environment()) :));	
	anyadir_ataque_pnj(50, "columna de fuego");
	anyadir_ataque_pnj(30, "Favor de los Antiguos", 0, (["objetivo": 2]));
	anyadir_ataque_pnj(30, "Agilidad del Guepardo", 0, (["objetivo": 2]));
	
	fijar_objetos_morir(({"tesoro", ({"arma", "armadura"}), ({5, 6}), 1}));
}

object dame_protector()
{
	return secure_present(NPCS + "campeon_hobgoblin", environment());
}

status puedo_formular(string str) {
	return !query_property("formulando") ;
}

void event_exit(object quien_sale,string mensaje,object entorno_destino,object *seguidores)
{

	if (-1 != member_array(quien_sale, query_call_outed() + query_attacker_list())) {
		if (puedo_formular("Hervor de la sangre")) {
			habilidad("hervor de la sangre", quien_sale);
		}
	}
	return ::event_exit(quien_sale,mensaje,entorno_destino, seguidores);
}
/*
void bateria_basica(object quien)
{
	if (puedo_formular("Retener persona")) {
		habilidad("retener persona", quien);
	} else if (puedo_formular("Mordisco de la vibora")) {
		habilidad("mordisco de la vibora", quien);
	} else if (puedo_formular("Hervor de la Sangre")) {
		habilidad("hervor de la sangre", quien);
	} else if (puedo_formular("Columna de fuego")) {
		habilidad("columna de fuego", quien);	
	} else if (puedo_formular("Gritos Ancestrales")) {
		habilidad("gritos ancestrales", implode(query_call_outed() + query_attacker_list() , ","));	
	}
}
*/
void heart_beat()
{
	//object candidato, aliado;
	
	// Quitamos la lacra del silencio
	if (environment() && environment()->query_silencio())
	{
		tell_object(this_object(), "¡Gritas con todas tus fuerzas, destrozando los hechizos de silencio que afectaban a tu entorno!\n");
		tell_accion(environment(), "¡"+query_short()+" grita con todas sus fuerzas, destrozando el hechizo de silencio!\n", "Un grito atronador destroza tus tímpanos.\n", ({this_object()}), this_object());
		environment()->disipar_magia(query_name());
		environment()->destruir_silencio();
	}

	/*if (tengo_enemigos()) {
		// Ataques
		bateria_basica(victima_limpia());
	} else if (hay_peligro()) {
		// Curas y bendiciones
		aliado = secure_present(NPCS + "campeon_hobgoblin", environment());
		if (dame_pvs() < dame_pvs_max()) {
			candidato = this_object();
		} else if (aliado && aliado->dame_pvs() < aliado->dame_pvs_max() && aliado->dame_pvs() < dame_pvs()) {
			candidato = aliado;
		}
		
		if (candidato) {
			if (puedo_formular("Curar heridas criticas"))
				habilidad("curar heridas criticas", candidato);
			else if (puedo_formular("Curar heridas serias"))
				habilidad("curar heridas serias", candidato);
			else if (puedo_formular("Curar heridas moderadas"))
				habilidad("curar heridas moderadas", candidato);
			else if (puedo_formular("Curar heridas ligeras"))
				habilidad("curar heridas ligeras", candidato);
		} else if (puedo_formularme("Bendicion", "bendicion")) {
			habilidad("bendicion", this_object());
		}
	} else if (aliado && puedo_formular("Favor de los Antiguos")) {
		habilidad("favor de los antiguos", "");
	} else if (puedo_formularle("Agilidad del Guepardo", "agilidad del guepardo", aliado)) {
		habilidad("agilidad del guepardo", aliado);
	}*/

	::heart_beat();
}

int do_death(object asesino)
{
	
	if(asesino && !asesino->dame_hito_mision("hobgoblins","vencido_rassoodock"))
		asesino->nuevo_hito_mision("hobgoblins","vencido_rassoodock");
	::do_death(asesino);
}