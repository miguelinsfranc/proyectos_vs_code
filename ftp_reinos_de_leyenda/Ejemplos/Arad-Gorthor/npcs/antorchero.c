// Dunkelheit 10-11-2009

#include "../path.h";
inherit "/obj/conversar_npc.c";

void setup()
{
	set_name("antorchero");
	set_short("Antorchero");
	add_alias("goblin");
	set_long("Su trabajo es de todo menos apasionante, a menos que seas un pirómano. "
	"El gremio de antorcheros está compuesto por goblins que no han sido capaces de "
	"superar las pruebas mínimas de acceso a la Horda Negra, pero que al menos son "
	"capaces de atinar con una antorcha en las lámparas colgantes y las farolas que "
	"iluminan Arad Gorthor. Pese a su perenne semblante de aburrimiento, estos granujas "
	"están siempre ojo al parche y conocen todos los cotilleos de la ciudad.\n");
	set_main_plural("Antorcheros");
	add_plural(({"antorcheros", "goblins"}));
	
	fijar_altura(120 + random(30));
	fijar_peso_corporal(45000 + random(20000));
	
	fijar_raza("goblin");
	fijar_clase("aventurero");
	fijar_religion("gurthang");
	
	fijar_fue(12 + random(3));
	fijar_con(16);
	fijar_des(17);
	fijar_int(4);
	fijar_sab(4);
	fijar_car(6);
	
	fijar_nivel(10 + random(5));
	fijar_alineamiento(1000);
	
	add_clone(BMISC + "antorcha", 1);
	init_equip();

	if (random(2)) {
		add_clone(BMISC + "ganzua", 1);
	}
	
	ajustar_dinero(2 + random(4), "cobre");
	
	ROL_NPCS->definir(this_object());
	
	nuevo_dialogo("trabajo", ({
		":evita mirarte directamente a los ojos, parece nervioso.",
		"'Sólo soy un simple antorchero",
		"'Enciendo y apago las lámparas colgantes y las antorchas de la ciudad",
		"adios",
	}));

	load_chat(25, ({
		":camina lentamente de un lado a otro, inspeccionando las lámparas de la ciudad.",
		":saca un pequeño frasco del bolsillo y echa un líquido sobre su antorcha, que arde con fiereza.",
		":se rebaña el sudor de la frente. Es duro trabajar con fuego en mitad de las tierras yermas.",
		"'¿Qué dispondrán hoy los señores de la ciudad? Ay, ay, ay...",
	}));
	
	habilitar_conversacion();
}

void adios(object quien)
{
	abandonar_conversacion(quien, "Si me disculpas, tengo mucho trabajo...");
	run_away();
}
