// Dunkelheit 07-11-2009
// Grimaek 13/06/2013 Sustituido add_attack_spell por anyadir_ataque_pnj

#include "../path.h";
inherit "/obj/monster.c";

void pedrada()
{
	object *enemigos, enemigo;
	
	enemigos = query_attacker_list();
	
	if (!sizeof(enemigos)) {
		return;
	}
	
	if (!enemigo = element_of(enemigos)) {
		return;
	}
	
	if (enemigo->dame_ts("agilidad", -10, "tierra", "proyectil")) {
		tell_object(this_object(), "%^MAGENTA%^#%^RESET%^ ¡Un proyectil de piedra brota de tus brazos pero "+enemigo->query_short()+" lo esquiva ágilmente!\n");
		tell_object(enemigo, "%^MAGENTA%^*%^RESET%^ ¡Un proyectil de piedra brota de los brazos del Galeb Duhr pero consigues esquivarlo!\n");
		tell_accion(environment(), "¡Un proyectil de piedra brota de los brazos del Galeb Duhr pero "+enemigo->query_short()+" consigue esquivarlo ágilmente!\n", "", ({ this_object(), enemigo }), this_object());
		return;
	}
	
	if (-300 != enemigo->spell_damage(-roll(1,300), "tierra", this_object(), 0)) {
		tell_object(this_object(), "%^GREEN%^#%^RESET%^ ¡Un proyectil de piedra brota de tus brazos e impacta violentamente contra el pecho de "+enemigo->query_short()+"!\n");
		tell_object(enemigo, "%^RED%^*%^RESET%^ ¡Un proyectil de piedra brota de los brazos del Galeb Duhr e impacta violentamente contra tu pecho!\n");
		tell_accion(environment(), "¡Un proyectil de piedra brota de los brazos del Galeb Duhr e impacta violentamente contra el pecho de "+enemigo->query_short()+"!\n", "Escuchas un impacto seco.\n", ({ this_object(), enemigo }), this_object());
	}
}

void setup()
{
	set_name("galeb duhr");
	set_short("Galeb Duhr");
	add_alias(({"galeb", "duhr"}));
	set_long("Criaturas sin remordimientos hechas de piedra viva, los galeb duhrs son "
	"habitualmente los sirvientes de criaturas más poderosas del plano elemental de "
	"tierra, tales como gigantes de las colinas y titanes de roca. Dicen que antaño "
	"fueron hermanos de los enanos y los svirfneblin, ¿pero quién puede demostrarlo? "
	"El físico de estas bestias es imponente: es bastante más ancha que alta, su "
	"cuerpo parece ser la fusión de cientos y cientos de rocas de muy diferentes "
	"formas y tamaños, alcanzando una densidad terrible en la cabeza, que ocupa "
	"casi las tres cuartas partes del metro sesenta de esta criatura.\n");
	set_main_plural("Galeb Durhes");
	add_plural(({"galebs","duhres"}));
	
	fijar_altura(160);
	fijar_peso_corporal(165000);
	
	fijar_raza("galeb_duhr");
	fijar_clase("barbaro");
	
	fijar_fue(18);
	fijar_extrema(100);
	fijar_con(18);
	fijar_des(3);
	fijar_int(3);
	fijar_sab(3);
	fijar_car(3);
	
	fijar_nivel(24 + random(3));
	fijar_alineamiento(0);
	
	ajustar_bo(140);
	
	poner_estilo_combate_desarmado("gigante");
	
	anyadir_ataque_pnj(50, "cabezazo");
	anyadir_ataque_pnj(30, "pedrada");
	
	set_aggressive(1);
}

void attack()
{
	::attack();
	::attack();
}

object make_corpse()
{
	tell_accion(environment(), "El Galeb Duhr se derrumba hasta no ser más que un montón de piedras y rocas.\n", "Escuchas algo derrumbarse.\n", ({ this_object() }), this_object());

	for (int i=5+random(3); i > 0; i--) {
		clone_object("/baseobs/proyectiles/piedra")->move(environment());
	}
	
	return 0;
}
