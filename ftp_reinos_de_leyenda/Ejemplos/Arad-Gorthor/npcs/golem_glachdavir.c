//Grimaek 2023 

inherit "/baseobs/monstruos/bases/elemental.c";


int dame_golem() { return 1; }

void abrazo_de_golem()
{
	object *todos, *objetivos = ({});
	object objetivo;	
	int i, x, cuantos;
 
 	//Buscamos un objetivo...
	cuantos = 0;
	for(i = sizeof( todos = all_inventory( environment( this_object() ) ) ); i--;) {
		if(living(todos[i]) && !todos[i]->query_hidden() && todos[i]->query_player() && todos[i] != this_object()) {
			objetivos += ({todos[i]});
			cuantos++;
		}
	}
	
	if(cuantos > 0) {
		objetivo = element_of(objetivos);
		if(objetivo->dame_bloqueo_combate("abrazo_golem") || dame_bloqueo_combate("abrazo_golem")) return;
		if(objetivo->dame_efecto_combate("abrazo_golem")) return;
		tell_object(objetivo,
			query_short()+" se encara en tu dirección y comienza a desplazarse ágilmente, conformando una borrón de fuego y lava.\n"
		);
		tell_accion(ENV(TO),
			"Ves como "+query_short()+" se encara en dirección a "+objetivo->query_short()+" y comienza a desplazarse ágilmente, conformando una borrón de fuego y lava.\n",
			"Escuchas un leve crepitar.\n",
			({objetivo})
		);
    }
    else {
       tell_room(ENV(TO),query_short()+", desconcertado, se queda inmovil en su posición.\n");
       return;
    }
	
	if(!objetivo->dame_ts("aliento", 0, "fuego", 0)) {
		//Falla la tirada de salvacion...

		if(objetivo->spell_damage(-(200 + random(300)),"fuego",TO,0) == -300) {
			return; 
		}
		objetivo->nuevo_efecto_combate("abrazo_golem", 30, 1);
		tell_object(objetivo,"Sientes como se te escapa el aire cuando "+query_short()+" te golpea de lleno. El golem te abraza bloqueando todos tus movimientos. ¡Estas retenido!\n");
		tell_room(ENV(TO),"¡Escuchas el grito agónico de "+objetivo->query_short()+" mientras "+query_short()+" le golpea de lleno. El golem le abraza bloqueando todos sus movimientos. ¡Está retenido!\n",objetivo);
		x=3+random(3);
		objetivo->add_timed_property("nocast","El "+query_short()+" te tiene atrapado en un abrazo e impide que formules hechizos.\n",x);
		objetivo->add_timed_property("noguild","El "+query_short()+" te tiene atrapado en un abrazo e impide que uses tus habilidades.\n",x);
		objetivo->add_timed_property("atrapado","El "+query_short()+" te tiene atrapado en un abrazo e impide tu movimiento.\n",x);
		objetivo->nuevo_bloqueo_combate("abrazo_golem", x + 45);
		nuevo_bloqueo_combate("abrazo_golem", x + 15);
		
	}
	else {
		tell_object(objetivo,"¡Consigues zafarte del abrazo de "+query_short()+"!.\n");
		tell_room(ENV(TO),"¡"+objetivo->query_short()+" consigue zafarse del abrazo de "+query_short()+"!.\n",objetivo);
	}
}

void setup(){
	set_name("golem");
	set_short("Golem invocado de lava");
	set_main_plural("Golems invocados de lava");
	add_alias(({"golem","lava"}));
	add_plural(({"golems","lavas"}));
	set_long("Una pequeña mole de roca y fuego que emana gases tóxicos por toda su superficie, está formado completamente por magma y duras rocas flotan por su superfice moviéndse a lo largo del torso. A diferencia de sus hermanos gigantes, sus movimientos son agiles y rapidos al ser pequeño, pero la fuerza que debe poseer es igualmente aterradora.\n");

    add_zone("conclave");
    set_move_after(10, 6);

	fijar_bando("mercenario");

	fijar_raza("golem");
	fijar_clase("gragbadûr");

	fijar_tamanyo_racial(9);

	fijar_estilo("fuego");

	fijar_alineamiento(0);
	fijar_genero(1);

	fijar_fue(30);
	fijar_des(6);
	fijar_con(30);
	fijar_int(3);
	fijar_sab(3);
	fijar_car(25);
    add_loved("raza","demonio");

	fijar_nivel(30+random(5));
	add_spheres((["menor":({"combate"})]));
	set_aggressive(3);
	//ajustar_carac_tmp("daño",50,0,0,0);
	ajustar_bo(150);
	ajustar_bp(160);
	anyadir_ataque_pnj(50, "abrazo_de_golem");	
	add_static_property("libre-movimiento", 1);
	add_static_property("sin miedo", 1);
	fijar_pvs_max(2000);	
	fijar_pvs(2000,0);	

}
int weapon_damage(int cantidad,object atacante,object arma,string ataque){
	if(atacante->query_name()=="glachdavir") {
		atacante->stop_fight(TO);
        TO->stop_fight(atacante);
		return 0;
	}	
	if( !arma || arma->dame_encantamiento()<5)
	{
		string nom= arma ? arma->query_short(): "arma";
		
		tell_object(atacante,"Tu "+nom+" no parece dañar lo más mínimo a "+query_short()+".\n");
		tell_accion(environment(atacante),"El ataque de "+atacante->query_short()+" con su "+nom+" no parece dañar a "+query_short()+".\n","Oyes ruidos de golpes.\n",({atacante}),atacante);
		return 0;
	}
	return ::weapon_damage(cantidad,atacante,arma,ataque);
}

int unarmed_damage(int pupa,string estilo,object atacante) { return 0; }

int spell_damage(int pupa,string tipo,object lanzador,object hechizo,object arma,string nombre_ataque,mapping cfg_ataque) {
	int danio;
	if (tipo == "fuego" || tipo == "tierra" || tipo == "acido") { //Se cura con tierra, fuego y acido XD es lava!
		danio = to_int(floor(pupa*-0.5));
		ajustar_pvs(danio, lanzador);
		tell_object(lanzador,"El hechizo tipo "+tipo+" lanzado parece revitalizar a "+query_short()+".\n");	
		if(lanzador->query_name()=="glachdavir") {
		    lanzador->stop_fight(TO);
            TO->stop_fight(lanzador);
			return 0;
		}
		return danio;//teóricamente si les curan les baja xp 
	} 
	if (tipo == "agua" ||tipo == "frio") {//Le hacen un 20% más de daño si es agua o frio
		danio = to_int(ceil(pupa*1.2));
		::spell_damage(danio, tipo, lanzador, hechizo, arma, nombre_ataque, cfg_ataque);
		return danio;
	}

	return 0;
}
