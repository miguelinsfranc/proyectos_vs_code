//Zoilder 20/01/2011
//Clase NPC basica para marineros
// Altaresh tuneo para arad
#include "../path.h";
#include INCLUDES+"interaciones.h"
inherit "/obj/monster.c";

/*
*	Función que devuelve una descripción aleatoria para el NPC.
*/
string dame_descripcion()
{
	string base_raza=capitalize(dame_numeral())+" "+dame_raza()+" mariner"+dame_vocal()+".";
	string *descripciones=({
			base_raza+" Tiene sus tatuados brazos desnudos, esforzándose en que todos los transeúntes vean dichas figuras de tinta. "
				"Notas como mira a los demás por encima del hombro, con aires de superioridad.\n",
			base_raza+" De melena larga y trenzada que cae sobre su hombro izquierdo, dejando al descubierto varias cicatrices a lo largo "
				"de todo su cuello. A diferencia de otros marineros, en sus brazos en vez de tatuajes tiene cicatrices. Parece que la mar es peligrosa.\n",
			base_raza+" Notas como cojea al andar. Su rostro, demasiado rechoncho, muestra tristeza y denota la dureza de una vida en la mar. "
				"En su mano, observas un pequeño cuchillo ensangrentado, lo que hace que nadie se acerque.\n"
	});
	return element_of(descripciones);
}

/*
*	Configuración del NPC
*/
void setup()
{
	string my_raza = element_of(({"orco","goblin","gnoll","kobold"}));
	//string my_raza = element_of(({"orco"}));
  //Se establece en primer lugar el genero del NPC, pues a partir de eso se sacan las descripciones y nombres
	fijar_genero(random(2)+1);
	fijar_genero(1);
	set_name("mariner"+dame_vocal());
	add_alias(({"mariner"+dame_vocal(),my_raza}));
	add_plural(({"mariner"+dame_vocal()+"s",my_raza+"s"}));
	set_short("Mariner"+dame_vocal()+" "+my_raza);
	set_main_plural("Mariner"+dame_vocal()+"s "+my_raza+"s");
	fijar_raza(my_raza);
  //Se obtiene una descripción aleatoria
	set_long(dame_descripcion());
	fijar_religion("gurthang");
	fijar_clase("aventurero");
	fijar_oficio("marinero");
    fijar_ciudadania("golthur");

	nuevo_lenguaje("negra", 100);

	set_random_stats(10,18);
	fijar_nivel(3 + random(16));
	fijar_altura(150+random(50));
	fijar_peso_corporal(40000+random(50)*1000);
 
  
	PONER_INTERACCIONES(TO, "humano",({"oler_humano"}));
	PONER_INTERACCIONES(TO, "prostituta",({"piropear_prostituta"}));
	PONER_INTERACCIONES(TO, "goblin",({"deuda_goblin"}));
	PONER_INTERACCIONES(TO, "orco",({"pelea_orcos"}));
  
	load_chat(30, ({
			//1,":piropea a un transeúnte.",
			//1,":se seca el sudor de la frente.",
			//1,":muestra su tatuaje del brazo con orgullo.",
			"'¡Oh sí, aquí estoy yo para animar la fiesta!",
			"#iniciar_cualquier_interaccion",
	}));

	add_clone(BARMAS			+ "cuchillo.c", 1); 
	add_clone(BARMADURAS	+ "gorro_cuero.c.c",1);	
	add_clone(BARMADURAS	+ "camiseta.c",1);
	add_clone(BARMADURAS	+ "cinturon.c",1);	
	add_clone(BARMADURAS	+ "zapatillas.c",1);
	add_clone(BMISC				+	"cuerda.c",1);
	init_equip();
}
 int puedo_iniciar_interaccion(string id){
	 
	int res; //variable que guardara el resultado de la funcion padre

	//Comprobaciones iniciales
	//Si no tenemos ide... fuera
	if (!id)
		return 0;	
	
	//Si la funcion padre devuelve 0.... fuera
	if (!res=::puedo_iniciar_interaccion(id)){
		__debug_x(sprintf("res de funcion padre= %d",res),"arad-gorthor");
		return 0;
	}
	res=COMPROBAR_INTERACCION(TO,id);
	__debug_x(sprintf("res final= %d",res),"arad-gorthor"); 
	return res=COMPROBAR_INTERACCION(TO,id);
	
 }
