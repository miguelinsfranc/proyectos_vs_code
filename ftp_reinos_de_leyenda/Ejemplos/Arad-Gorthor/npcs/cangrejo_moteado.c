// Dherion 14/04/2017; Cangrejo de la costa de los Dioses
inherit "/baseobs/monstruos/animales/cangrejo.c";
void setup() {
    ::setup();
    set_short("Cangrejo moteado");
    set_main_plural("Cangrejos moteados");
    generar_alias_y_plurales();
    add_zone("playa");
    set_long("A primera vista, dirías que es un cangrejo común, de los que puedes encontrar levantando cualquier roca en algún arrecife o acantilado de otra parte de Eirea, buscando una presa fácil para devorar."
      "Esa impresión se desvanece cuando te fijas en las pequeñas motas que cubren su exoesqueleto, características de los cangrejos moteados de las Costas de los Dioses."
      "Estos cangrejos poseen un caparazón que los hace más resistentes a estas frías aguas, además de tener unas pinzas algo más largas de lo normal.");
    // Son un poquito más poderosos que los cangrejos normales.
    set_random_stats(15, 18);
    fijar_nivel(10);
}
