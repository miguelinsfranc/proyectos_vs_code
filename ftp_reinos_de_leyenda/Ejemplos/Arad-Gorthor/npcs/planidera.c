// Dunkelheit 23-11-2009

#include "../path.h";
inherit "/obj/monster.c";

void setup()
{
	string raza = element_of(({"orco", "goblin", "semi-orco"}));
	string corto = (["orco":"orca", "goblin":"goblin", "semi-orco":"semi-orca"])[raza];

	set_name("plañidera");
	set_short("Plañidera");
	add_alias(({"planyidera", "planidera", corto, raza}));
	set_long("Esta "+corto+" es una de las famosas plañideras de Lord Gurthang. "
	"Las plañideras son una secta matriarcal de chamanas "
	"que gran parte de su vida al rezo y la imploración constante del favor de "
	"su dios. No es que tengan muchas otras actividades, salvo los peregrinajes "
	"habituales ente Golthur Orod, el Templo de la Guerra, y Arad Gorthor. Se las "
	"reconoce fácilmente por sus ropajes de color escarlata y por llevar siempre "
	"un rosario de dientes en el cuello, que rezan con fervor.\n");
	set_main_plural("Plañideras");
	add_plural(({"plañideras", "planyideras", "planideras", corto+"s", raza+"s"}));
	fijar_genero(2);
	
	fijar_altura(140+random(20));
	fijar_peso_corporal(50000+random(20000));
	
	fijar_raza(raza);
	fijar_clase("chaman");
	fijar_religion("gurthang");
	fijar_ciudadania( (["orco":"golthur", "goblin":"mor_groddur", "semi-orco":"golthur"])[raza] );
	
	fijar_fue(10 + random(6));
	fijar_con(17);
	fijar_des(14);
	fijar_int(9);
	fijar_sab(18);
	fijar_car(13);
	
	fijar_nivel(20 + random(11));
	fijar_alineamiento(2950);
	
	ajustar_dinero(roll(2, 10), "cobre");
	
	ROL_NPCS->definir(this_object());
	
	load_chat(30, ({
		":se tira al suelo como poseída y empieza a rodar de un lado a otro.",
		":se pone de rodillas y dedica letanías a su dios, Lord Gurthang.",
		":reza el rosario con una pasión que jamás habías visto antes.",
		":golpea su frente contra el disco sagrado hasta abrirse una brecha.",
		"'¡Guerra para el Dios de la Guerra!",
		"'¡Bendito sea Lord Gurthang!",
		"'¡Alabado sea el Lord Caído!",
		"'¡Lord Gurthang, escucha nuestras plegarias, las de tus más fervientes beatas!",
		"'¡Planta tu semilla impía en mi fertil vientre! ¡oh mi amo y señor!",
		"'¡Amén, hermanas!",
		"'¡Amén, por Lord Gurthang!",
		"'¡Amén, por la Guerra!",
	}));
	
	load_a_chat(50, ({
		"'¡¡¡Cómo osas profanar la tierra impía de Lord Gurthang!!!",
		"'¡Guardias del templo! ¡Guardias!",
	}));
}

string dame_noconversar_personalizado()
{
		return "La plañidera está absorta en sus plegarias y sus rezos, será mejor dejarla en paz.\n";
}
