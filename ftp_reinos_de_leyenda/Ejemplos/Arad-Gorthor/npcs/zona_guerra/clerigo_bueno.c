// Eckol 13Ene15: Clérigo para las puertas de Kattak. Basado en los Cruzado Clérigo de Takome.
//Eckol 15Abr15: Quito el attack_by, ahora los avisos estan en la base de guardia.

//#include "../path.h"
#include <guardias.h>
#include <conquistas.h>
#include <baseobs_path.h>

inherit _GUARDIAS_BASE;
inherit BSNIPPETS + "npc_curador.c";

void setup_curas();

void setup()
{
	string organizacion = DAME_CONQUISTADOR_ZONA("boreal");
	
	create(organizacion);
}
	

void setup_personalizacion(object org)
{
	
	string a     = dame_vocal();
    string name  = a == "a" ? "cleriga" : "clerigo";
    string corto = dame_genero() == 2 ? "clériga" : "clérigo";
    string donde = org ? " mercenario de " + org->dame_corto() : "";
    string dios = dame_religion() == "ateo" ? "su dios olvidado" : dame_nombre_religion();

    set_name(name);
    set_short(capitalize(corto) + donde);
    set_main_plural(capitalize(corto) + donde);
    generar_alias_y_plurales();
 
    set_long(sprintf(
        "La búsqueda de gloria y aventuras no son anhelos exclusivos de "
        "afamados guerreros o heróicos caballeros; l%ss %ss de "
        "los Reinos, como %s que está ante tí, también ansían "
        "estos galardones, si bien por motivos muy distintos. "
        "Llamad%ss tanto como por su devoción, como por el deseo de que sus "
        "gestas ayuden a propagar la fe de %s, estos %s se aventuran "
        "a lo desconocido con el único solaz de su fe y sus letanías.\n\n"

        "  Si bien no son ajen%ss a los horrores de la guerra, su papel suele "
        "limitarse al de apoyar a aquellos que buscan un lugar en las páginas "
        "de la historia de Eirea mediante métodos más violentos (aunque los "
        "galardones de la histora después usen otros eufemismos que suenen "
        "mejor para narrar sus historias). De esta forma, los logros de estos "
        "héroes pasarán a ser "
        "automáticamente méritos propios de %s, pues si hay algo que la "
        "religión sepa hacer bien, es usar su propaganda para dar a saber al "
        "mundo que los méritos de otros pertenecen a las entidades inmortales "
        "que aquejan en este mundo con sus intrigas.\n",
        a,
        corto,
        dame_este(1),
        a,
        dios,
        "hombres y mujeres de fe",
        a,
        dios));

  
  fijar_raza("humano");
  nuevo_lenguaje("adurn",100);
  fijar_lenguaje_actual("adurn");
}

void setup_equipo(object org){
	
    add_clone(BARMAS + "maza.c", 1);
    add_clone(BESCUDOS + "escudo.c", 1);

    add_clone(BARMADURAS + "gorro_cuero.c", 1);
    add_clone(BARMADURAS + "guante_cuero.c", 1);
    add_clone(BARMADURAS + "guante_cuero_izq.c", 1);
    add_clone(BARMADURAS + "grebas.c", 1);
    add_clone(BARMADURAS + "brazalete_izq.c", 1);
    add_clone(BARMADURAS + "brazalete.c", 1);
    add_clone(BARMADURAS + "botas.c", 1);
    add_clone(BARMADURAS + "cuero.c", 1);
}
void setup_poder(object org)
{
    fijar_habilidades_guardia(({"curar heridas moderadas",
                                "curar heridas serias",
                                "curar heridas criticas"}));

    fijar_clase("sacerdote");
    fijar_maestria("Aplastantes medias", 40);
    fijar_esferas((["neutral":({"curadora"})]));

    fijar_fue(16);
    fijar_des(16);
    fijar_con(18);
    fijar_car(14);
    fijar_int(14);
    fijar_sab(18);
    fijar_nivel(50);
    fijar_pvs_max(3000);
    fijar_fe(220);

    setup_curas();
}  

/*******************************************************************************
 * Lógica de combate
 ******************************************************************************/
void setup_curas()
{
    // Siempre tendrán estas a su disposición
    add_spell("curar heridas ligeras");
    curador_fijar_porcentaje_peligro(60);
    curador_fijar_permitir_curar_otros(1);
    curador_habilitar_curas(1, 0, 0, 0, query_spells());
}
object *dame_potenciales_curados()
{
    return dame_aliados_validos() | ({TO});
}
void aprender_mejora_ataque(string skill)
{
    add_spell(skill);
    setup_curas();
}
void olvidar_mejora_ataque(string skill)
{
    remove_spell(skill);
    setup_curas();
}
int check_anyone_here()
{
    return npc_curador::check_anyone_here() ||
           base_guardia_ciudades::check_anyone_here();
}
void heart_beat()
{
    heart_beat_curar();
    ::heart_beat();
}

status curador_puedo_curar_objetivo(object curador, object b)
{
    if (!curador || !b) {
        return 0;
    }

    if (curador->dame_ver_realmente() < b->query_hidden()) {
        return 0;
    }

    if (-1 != member_array(b, curador->dame_lista_enemigos())) {
        return 0;
    }

    if (curador_porcen(b) >= 100) {
        return 0;
    }

    if (curador->dame_organizacion_guardia() !=
        b->dame_organizacion_guardia()) {
        return 0;
    }

    return 1;
}