// Altaresh 19
#include "../path.h"
#include <baseobs_path.h>
#include <conquistas.h>
#define DIOSES ({"nirve", "ralder"})
inherit "/obj/monster.c";
inherit "/baseobs/snippets/npc_curador.c";
inherit "/baseobs/snippets/codigo_pnj_organizacion.c";

void setup()
{

    string organizacion = DAME_CONQUISTADOR_ZONA("boreal");

    _setup_personalizacion_organizacion(organizacion);
}

void setup_descripciones(object org)
{

    string organizacion = DAME_CONQUISTADOR_ZONA("boreal");
    string corto;
    string donde = " Elite de " + capitalize(organizacion);

    ::setup_descripciones(org);

    fijar_genero(random(2) + 1);

    corto = dame_genero() == 2 ? "cleriga" : "clerigo";

    set_name(corto);
    set_short(capitalize(corto) + donde);
    set_main_plural(capitalize(corto) + donde);
    generar_alias_y_plurales();
	
	set_long("Ante ti tienes un"+dame_a()+" "+corto+" con rudo aspecto. Presumiblemente es la mano derecha del mando del campamento, "
		"encargado de dirigir a los sacerdotes del puesto avanzado y servir a su comandante. Lleva el equipo típico de los sacerdotes "
		"de combate, algunas abolladuras en su armadura te muestran que no es la primera vez que entra en combate.");
}

void setup_poder(object org)
{

    fijar_nivel(60);
    fijar_pvs_max(40000);

    fijar_clase("sacerdote");
    fijar_esferas((["mayor":({"curadora"})]));

    set_random_stats(17, 18);

    add_spell("curar heridas ligeras");
    add_spell("curar heridas moderadas");
    add_spell("curar heridas serias");
    add_spell("curar heridas criticas");
    curador_fijar_porcentaje_peligro(60);
    curador_fijar_permitir_curar_otros(1);
    curador_habilitar_curas(4, 0, 0, 0, query_spells());

    if (dame_religion() == "ateo") {
        fijar_religion(element_of(DIOSES));
        fijar_fe(200);
    }
}
void setup_equipo(object org)
{

    add_clone(BARMAS + "maza.c", 1);
    add_clone(BESCUDOS + "escudo.c", 1);

    add_clone(BARMADURAS + "gorro_cuero.c", 1);
    add_clone(BARMADURAS + "guante_cuero.c", 1);
    add_clone(BARMADURAS + "guante_cuero_izq.c", 1);
    add_clone(BARMADURAS + "grebas.c", 1);
    add_clone(BARMADURAS + "brazalete_izq.c", 1);
    add_clone(BARMADURAS + "brazalete.c", 1);
    add_clone(BARMADURAS + "botas.c", 1);
    add_clone(BARMADURAS + "cuero.c", 1);

    init_equip();
}

void heart_beat()
{

    heart_beat_curar();

    ::heart_beat();
}

int check_anyone_here()
{
    return npc_curador::check_anyone_here() || monster::check_anyone_here();
}

int pj_valido(object pj)
{

    return member_array(
        DAME_CONQUISTADOR_ZONA("boreal"),
        ({pj->dame_ciudadania(), pj->dame_gremio(), pj->dame_familia()}));
}

status curador_puedo_curar_objetivo(object curador, object b)
{
    if (!curador || !b) {
		__debug_x(sprintf("entra en 1"),"arad-gorthor");
        return 0;
    }

    if (curador->dame_ver_realmente() < b->query_hidden()) {
		__debug_x(sprintf("entra en 2"),"arad-gorthor");
        return 0;
    }

    if (-1 != member_array(b, curador->dame_lista_enemigos())) {
		__debug_x(sprintf("entra en 3"),"arad-gorthor");
        return 0;
    }

    if (curador_porcen(b) >= 100) {
		__debug_x(sprintf("entra en 4"),"arad-gorthor");
        return 0;
    }
	
	__debug_x(sprintf("CLERO  b: %s, resultado: %d\n",b->query_name(),member_array(b,children("/d/arad-gorthor/npcs/zona_guerra/mando_soldado.c"))),"arad-gorthor");

    if (!b->query_player())
	{
		if(-1!=member_array(b,children("/d/arad-gorthor/npcs/zona_guerra/mando_soldado.c")))
		{
			__debug_x(sprintf("entra en 5"),"arad-gorthor");
			return 1;
		}
		__debug_x(sprintf("entra en 6"),"arad-gorthor");
        return 0;
    }

    if (pj_valido(b) < 0) {
		__debug_x(sprintf("entra en 7"),"arad-gorthor");
        return 0;
    }
__debug_x(sprintf("sale con 1"),"arad-gorthor");
    return 1;
}
