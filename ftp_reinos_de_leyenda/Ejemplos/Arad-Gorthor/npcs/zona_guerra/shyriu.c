//Golthiryus 2.IX.08: Patrullero lancero aranäe
//Eckol Ago16: Nuevos guardias.
#include "/d/eldor/path.h"
inherit NPCS+"guardias/patrullero_lancero.c";


//status dame_guardia() {return 1;}

//void ataque_guardia(object*);

void setup()
{
    set_name("Shyriu");
    add_alias(({"lancero","aranae","aranäe","shyriu"}));
    add_plural(({"lanceros","aranaes"}));
    set_short("Shyriu, Brazo derecho del Senescal");
    set_main_plural("Shyrius");
    set_long("Te encuentras ante el herculeo Shyriu, comandante del campamento a las ordenes del mismisimo Senescal de eldor."
		"Es uno de los humanos mas grandes que has conocido y eso que no porta una enorme armadura pesada. Al reves, parece que su "
        "equipo busca mas la destreza en el combate que la seguridad de una indumentaria mas aparatosa. Tienes rasgos norteños, "
        "su pelo es rubio y cae recto sobre sus hombros y espada, posee unos ojos grandes del color del mar con una mirada penetrante. "
        "Siempre cerca de su lanza de la cual cuelgan penachos que parecen estar hechos de crin de caballo. "
        "En medio de cota de mallas aparece el simbolo de un Elizhim Blanco.\n");
    fijar_fue(21);
    fijar_extrema(100);
    fijar_des(18);
    fijar_con(20);
    fijar_int(8);
    fijar_sab(8);
    fijar_car(15);
	
	nuevo_lenguaje("adurn",100);
    nuevo_lenguaje("elfico",100);
    nuevo_lenguaje("eldorense",100);
    nuevo_lenguaje("gnomo",100);
    fijar_lenguaje_actual("eldorense");

    fijar_clase("lancero_aranae");
    fijar_ciudadania("eldor",90);
    fijar_nivel(60 + random(10));
    //add_static_property("eldorian_configurado",1);

    //::setup();

    //set_move_after(10,10);
    fijar_cobardia(0);

    nueva_habilidad("ensartar");
    nueva_habilidad("amenazar");
    nueva_habilidad("remolino");
	nueva_habilidad("golpedemastil");
    fijar_maestria("lanza",100);

    add_clone("/d/eldor/armas/lanza_valar",1);
	add_clone("/baseobs/armaduras/mallas",1);
	add_clone("/baseobs/armaduras/yelmo",1);
	add_clone("/baseobs/armaduras/brazalete",1);
	add_clone("/baseobs/armaduras/brazalete_izq",1);
	add_clone("/baseobs/armaduras/grebas",1);
	add_clone("/baseobs/armaduras/botas_campaña",1);
	add_clone("/baseobs/armaduras/manopla",1);
	add_clone("/baseobs/armaduras/manopla	_izq",1);
    init_equip();
	
	add_hated("relacion", "takome");
    add_hated("enemigo","takome");
	
	add_attack_spell(50,"ensartar",3);
    add_attack_spell(50,"remolino",3);
	add_attack_spell(50,"amenazar",3);
	add_attack_spell(50,"golpedemastil",3);
	
	set_join_fight_mess("Shyriu exclama: ¡Por Eldor!");
	load_chat(10,
      ({
	"'Derramaremos hasta la ultima gota de Sangre para defender este campamento.",
	":observa vigilante a su alrededor.",
	":mira la punta de su lanza, y pasa un paño para limpiarla.",
	"'Hiros nos guiara en el camino.",
	"'Permanecez atentos, esperamos un ataque inminente.",
	"'Ensartare a cualquiera que intente tomar el campamento.",
      }));
	load_a_chat(30,
      ({
	"'¡Montad hijos de las estepas, nos atacan!",
	":prepara su lanza para el ataque.",
	":Empieza a realizar una danza girando sobre si mismo.",
	"'¡Por Hiros!.",
      }));
}