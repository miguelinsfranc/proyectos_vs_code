/**
 * Satyr 04/11/2019 11:48
 *
 * Añado el "pnj de organización" al código de Altaresh
 */
#include <baseobs_path.h>
#include <conquistas.h>
#include <organizaciones.h>
#include <rol.h>
#include <estatus.h>
#include <alineamiento.h>
#define DIOSES ({"nirve", "ralder"})

inherit "/baseobs/monstruos/bases/sacerdote_curandero.c";
inherit "/baseobs/snippets/codigo_pnj_organizacion.c";

void setup()
{
	
   string organizacion = DAME_CONQUISTADOR_ZONA("boreal");

   _setup_personalizacion_organizacion(organizacion);


    crear_cartel("Cartel");
}

void setup_descripciones(object org)
{
	 string organizacion = DAME_CONQUISTADOR_ZONA("boreal");


    set_name("curandero");
    set_short("Curandero mercenario de " + capitalize(organizacion));
    set_main_plural("Curandero mercenario de " + capitalize(organizacion));
    generar_alias_y_plurales();
    set_long(
        "Ves a un viejo humano lleno de cicatrices apoyado en su bastón, "
        "claramente curtido en mil batallas y ahora a los servicios del mejor "
        "postor."
        " Ahora se dedica a prestar sus servicios de sanación en este puesto "
        "avanzado de " +
        capitalize(organizacion) + "\n.");
	
}


void setup_poder(object org)
{
	
	set_random_stats(17, 18);
    fijar_clase("clerigo");
    fijar_nivel(42);
    fijar_genero(1);
		
	if ( dame_religion() == "ateo" ) {
        fijar_religion(element_of(DIOSES));
		fijar_fe(200);
	}

}
void setup_equipo(object org)
{
	add_clone(BARMAS + "baston.c", 1);
    add_clone(BARMADURAS + "tunica.c", 1);
  
	init_equip();
}


