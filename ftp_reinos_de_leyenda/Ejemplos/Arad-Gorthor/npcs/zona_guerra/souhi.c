// Nohur 16.10.2017
#include <baseobs_path.h>
#include "/w/altaresh/arad-gorthor/path.h"
inherit "/baseobs/monstruos/bases/sacerdote_curandero.c";


void setup_conversacion()
{

    nuevo_dialogo("nombre","Hola siervo del Bien, gracias ante todo por venir a ayudarnos en estos dias de conflicto, "
         "soy el monje encargado de ayudar a los combatientes heridos y traer la palabra de %^BOLD%^WHITE%^H%^YELLOW%^iros%^RESET%^ a nosotros. ");



    nuevo_dialogo("curar",({
        "'Para eso estoy aqui, nombrado directamente por el Guia. Ayudo a los heridos a volver "
        "al combate lo más rapido posible. ",
        }),"nombre");


    nuevo_dialogo("precio",({
        ":se encoge de hombros.",
        "'Hay que mantener los sumistros del campamento, esto no se paga solo. ",
        ":te mira con cara de circunstancia.",
        }),"curar");

    habilitar_conversacion();
}

void setup()
{
    set_name("Souhi");
    set_short("Souhi");
	add_alias(({"monje","souhi","humano"}));
    set_long("Ves a Souhi, un joven monje humano enfundado en su tunica perfectamente abrochada con una cinta "
            "acabada en un penacho que asemeja a la crin de un caballo. Carece a simple vista de armas, pero sus "
            "callosas manos te dan una pista de las herramientas que usa en la lucha. Su cabeza no posee ningun pelo,"
            "en su cuero cabelludo aparecen cicatrices de alguna antigüa reyerta. Su mirada es calida y te reconforta "
            "mirar a sus ojos de color del cobre, poseen un extraño brillo divino.\n");

    set_random_stats(17,18);
    fijar_raza("humano");
    fijar_bando("bueno");
    fijar_clase("monje");
    fijar_religion("hiros");
    fijar_ciudadania("eldor");
    fijar_nivel(42);
    fijar_alineamiento(-10000);
    fijar_genero(1);
	fijar_bo(400);


    nuevo_lenguaje("adurn",100);
    nuevo_lenguaje("elfico",100);
    nuevo_lenguaje("eldorense",100);
    nuevo_lenguaje("gnomo",100);
    fijar_lenguaje_actual("eldorense");

    add_hated("relacion", "takome");
    add_hated("enemigo","takome");
    set_aggressive(12);

   
    add_clone(BARMADURAS + "tunica.c", 1);
    init_equip();
	
	
	fijar_estilo(NPCS"nuevokhaldar");
	
	add_attack_spell(20, "retener persona", 3);
	set_join_fight_mess("Souhi exclama: ¡Sentiras la furia de Hiros!");
	/*load_chat(10,
      ({
	"'Derramaremos hasta la ultima gota de Sangre para defender este campamento.",
	":observa vigilante a su alrededor.",
	":mira la punta de su lanza, y pasa un paño para limpiarla.",
	"'Hiros nos guiara en el camino.",
	"'Permanecez atentos, esperamos un ataque inminente.",
	"'Ensartare a cualquiera que intente tomar el campamento.",
      }));
	load_a_chat(30,
      ({
	"'¡Montad hijos de las estepas, nos atacan!",
	":prepara su lanza para el ataque.",
	":Empieza a realizar una danza girando sobre si mismo.",
	"'¡Por Hiros!.",
      }));
*/
    setup_conversacion();
}
