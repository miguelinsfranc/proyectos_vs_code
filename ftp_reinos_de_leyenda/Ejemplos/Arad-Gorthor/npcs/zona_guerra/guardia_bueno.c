//Durmok 27/05/2003
//Snaider 12-XI-2006
//Modernizo la base de Durmok, introducciendo nuevos conceptos y cambiando
//funciones
//Eckol 18May15: Les subo un poco los stats y el nivel, para que sea más difícil entrar.
#include "/w/nohur/arad-gorthor/path.h"
inherit "/obj/guardia.c";

void setup()
{
    string vocal,raza=({"enano","humano","gnomo","semi-elfo"})[random(4)];
    
    !random(5) ? fijar_genero(2):fijar_genero(1);
    vocal=dame_vocal();
    set_name("soldado");
    add_alias(({"guardian","soldado","guardia"}));
    add_plural(({"guardias","guardianes","soldados"}));
    set_short("Soldado de Eralie");
    set_main_plural("Soldados de Eralie");        
    set_long(capitalize(dame_numeral())+" "+({"grues"+vocal,"fornid"+vocal,"musculos"+vocal,"atletic"+vocal})[random(4)]+
    " soldado perteneciente a uno de los ejércitos del bando bueno. Su "+({"largo","corto","rizado","ondulado"})[random(4)]+
    " pelo da paso a unos "+({"grandes","pequeños","estilizados"})[random(3)]+
    " ojos de color "+({"marrón","negro","verde oscuro"})[random(3)]+" que brillan"
    " valerosos y honorables, ya que estar aquí significa que tanto él como sus compañeros"
	" han conseguido conquistar el ansiado Campamento Boreal.\n");

    fijar_raza(raza);
        
    fijar_bando("bueno");
    fijar_religion("eralie");
    fijar_fe(50);
    fijar_clase("soldado");

    nuevo_lenguaje("adurn",100);
    quitar_lenguaje("dendrita");
    
    fijar_lenguaje_actual("adurn"); 

    fijar_alineamiento(-4001-random(2000));

    fijar_fue(20+random(3));
    fijar_extrema(70+random(30));
    fijar_carac("des",13+random(5));
    fijar_carac("con",18);
    fijar_carac("int",12+random(6));
    fijar_carac("sab",8+random(9));
    fijar_carac("car",14+random(5));

    load_chat(10,
      ({
	"'Preservaré este campamento con mi vida.",
	":observa vigilante a su alrededor.",
	":mira al cielo observando las nubes.",
	"'Nadie puede imaginarse la satisfacción de haber conquistado el Campamento Boreal.",
	"'Shyriu es un gran capitán, seguro que con él no perderemos esta posición.",
	"'Estoy deseando poner a prueba mis habilidades.",
	"'Este no es un sitio seguro, este paso es causa de una guerra continua.",
      }));

    add_clone(BARMADURAS+"completa",1);
    add_clone(BARMADURAS+"gran_yelmo",1);
    add_clone(BARMADURAS+"brazal",1);
	add_clone(BARMADURAS+"brazal_izq",1);
    add_clone(BARMADURAS+"grebas_metalicas",1);
    add_clone(BARMADURAS+"botas_guerra",1);
	add_clone(BARMADURAS + "guantelete", 1);
    add_clone(BARMADURAS + "guantelete_izq",1);
    add_clone(BESCUDOS+"escudo_corporal.c",1);
    add_clone(BARMAS+"espada_bastarda",1);

	fijar_armadura_natural(40+random(10));
    fijar_nivel(40+random(10));
    fijar_pvs_max(dame_pvs_max()+6500);
    init_equip();
    
    add_attack_spell(50,"furia",1);
    add_attack_spell(50,"golpecertero",3);
    
    ajustar_dinero(roll(1,6),"khaldan");
    
   /* nuevo_dialogo("campamento","'Estas en la entrada del Campamento Boreal, "
	"el ansiado lugar de paso por el que todos luchan independientemente de sus creencias. "
	"Por suerte, está en nuestras manos, así que tenemos libre paso por él. "
	"Aprovechalo y no dejes que caiga en manos ajenas, sino nos tocará recuperarlo de nuevo.");
    habilitar_conversacion();*/
}
