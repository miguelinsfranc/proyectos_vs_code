// Dunkelheit 10-11-2009
// Grimaek 14/06/2023 arregladas maestrias

#include "../path.h";
inherit "/obj/conversar_npc.c";

void setup()
{
	set_name("maton");
	set_short("Matón Orco");
	add_alias("orco");
	set_long("Este orco no ha debido llevar una vida buena a juzgar por el lugar "
	"al que ha ido a parar: los suburbios de Arad Gorthor. Estos descerebrados "
	"orcos venderían a su madre por un par de monedas de oro, así que ten mucho "
	"cuidado con ellos, pueden llegar a ser muy imprevisibles. Lleva un arma de "
	"filo con la que juega en sus ratos libres... cuando no está robando o peleándose "
	"con alguien.\n");
	set_main_plural("Matones Orcos");
	add_plural(({"matones", "orcos"}));
	
	fijar_altura(150 + random(20));
	fijar_peso_corporal(70000 + random(20000));
	
	fijar_raza("orco");
	fijar_clase("aventurero");
	fijar_religion("gurthang");
	
	fijar_fue(15 + random(4));
	fijar_con(17 + random(2));
	fijar_des(13 + random(6));
	fijar_int(4);
	fijar_sab(4);
	fijar_car(7);
	
	fijar_nivel(10 + random(6));
	fijar_alineamiento(2500);
	fijar_maestria("Penetrantes ligeras", dame_nivel());
	fijar_maestria("Penetrantes medias", dame_nivel());
	switch (random(3)) {
		case 2:
			add_clone(BARMAS + "cuchillo", 1);
			break;
		case 1:
			add_clone(BARMAS + "daga", 1);
			break;
		default:
		case 0:
			add_clone(BARMAS + "espada_corta", 1);
			break;
	}

	if (!random(3)) {
		add_clone(BARMADURAS + "cuero", 1);
	}
	
	init_equip();
	
	ajustar_dinero(5 + random(6), "estaño");
	
	ROL_NPCS->definir(this_object());
	
	nuevo_dialogo("trabajo", ({
		element_of(({
			"'¿Qué demonios te importa a qué me dedique, so cabestro?",
			"'¿A ti que te importa, mequetrefe?",
			"'Yo no soy tu amigo, zoquete",
		})),
		"'¿Es que estás buscando pelea?",
		"'¿Quieres que te haga una cara nueva?",
	}));
	nuevo_dialogo("pelea", ({
		"pelea",
	}), "trabajo");
	
	load_chat(25, ({
		":juega con una moneda de estaño.",
		":te mira desafiante de arriba a abajo.",
		"'¿Eh? ¿qué miras, es que tienes algún problema?",
		":bromea con uno de sus amigos matones para reforzar su autoestima.",
		":saca un cuchillo del bolsillo y juega con él.",
		"'¿A qué lugar de esta miserable ciudad tiene que ir un orco para tener un poco de diversión?",
	}));
	
	load_a_chat(50, ({
		"'¡Te mataré!",
		":escupe sangre al suelo.",
	}));
	
	habilitar_conversacion();
}

void pelea(object quien)
{
	if (quien->dame_genero() == 2) {
		do_say("¡¡¡Tú lo has querido, furcia, te rajaré la cara y luego te enseñaré lo que un macho orco es capaz de meter en tus carnes!!!", 0);
	} else {
		do_say("¡¡¡Tú lo has querido, maldito comeflores!!! ¡¡¡Te partiré esa boquita de nenaza que tienes!!!", 0);
	}
	
	attack_ob(quien);
	quien->attack_by(this_object());
}

int frases_personalizables(string tipo, object npc, object pj)
{
	switch (tipo)
	{
		case "saludo":
			pj->do_say("¡Oye!", 0);
			break;
		case "bienvenida-pj":
			if (HORDA_NEGRA->es_oficial(pj)) {
				npc->do_emote("gira el rostro y te mira con desprecio. Este idiota parece no saber qué rango tienes en la Horda Negra.");
			} else {
				npc->do_emote("gira el rostro y te mira con desprecio.");
			}
			break;
		case "bienvenida-npc":
			break;
		case "despedida-pj":
			pj->do_say("Adiós, tengo que irme", 0);
			break;
		case "despedida-npc":
			npc->do_say("¡Lo que tú digas!", 0);
			break;
		default:
			return 0;
	}

	return 1;
}
