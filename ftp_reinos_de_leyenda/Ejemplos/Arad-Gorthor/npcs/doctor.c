// Dunkelheit 22-11-2009
// Grimaek 13/06/2013 Sustituido add_attack_spell por anyadir_ataque_pnj

#include "../path.h";
inherit "/obj/conversar_npc.c";

void setup()
{
	set_name("doctor");
	set_short("Doctor de la Plaga");
	add_alias(({"orco", "plagas"}));
	set_long("En los reinos de Lord Gurthang, las plagas están a la orden del día. No "
	"como manifestación de la cólera de este dios, sino como consecuencia de la total "
	"falta de higiene de sus fieles. En concreto, la lepra era una enfermedad tan común "
	"que no fue hasta bien entrada la Era 4ª que se decidió combatirla en serio. Y por "
	"ello nacieron los doctores de la plaga, chamanes especializados en la curación "
	"de esta y otras enfermedades. Visten una larga gabardina de cuero negro que les "
	"aisla de agentes contaminantes, al igual que una gran máscara que les cubre todo "
	"el rostro y que tiene una nariz como la de un pájaro, en la que untan ungüentos "
	"perfumados para protegerse de los apestosos orcos y goblins de Arad Gorthor. Su "
	"bastón no es una ayuda para andar, sino que les permite manipular a las víctimas "
	"sin tocarlas directamente. Una aparición harto tétrica.\n");
	set_main_plural("Doctores de la Plaga");
	add_plural(({"doctores", "plagas", "orcos"}));
	
	fijar_altura(150 + random(30));
	fijar_peso_corporal(65000 + random(20000));
	
	fijar_raza("orco");
	fijar_clase("chaman");
	fijar_religion("gurthang");
	
	fijar_fue(11);
	fijar_con(18);
	fijar_des(13);
	fijar_int(12);
	fijar_sab(18);
	fijar_car(13);
	
	fijar_nivel(25 + random(5));
	fijar_alineamiento(2500);
	
	add_clone(BARMAS + "baston", 1);
	add_clone(ARMADURAS + "gabardina_negra", 1);
	add_clone(ARMADURAS + "mascara_buitre", 1);
	add_clone(BARMADURAS + "botas", 1);
	add_clone(BARMADURAS + "guante", 1);
	add_clone(BARMADURAS + "guante_izq", 1);
	init_equip();

	ajustar_dinero(3 + random(4), "oro");
	
	ROL_NPCS->definir(this_object());
	
	nuevo_dialogo("lepra", ({
		"curar_lepra",
	}));

	load_chat(30, ({
		":camina apartando a la plebe con su bastón, examinándolos como si fueran ganado.",
		"'¡Leprosos, venid a mí! ¡yo os impondré la curación de Lord Gurthang!",
		":cura la lepra de un peregrino que pasaba por el lugar, arrancando los aplausos de los testigos.",
		":extiende un oloroso cataplasma en el pico de su máscara.",
		"'¡Nuestro dios Gurthang, Señor de la Guerra y Padre de los Demonios, os curará de las plagas que el resto de envidiosos dioses os ha contagiado!",
	}));
	
	load_a_chat(50, ({
		"'¡¡¡Tú eres la auténtica plaga que hay que erradicar de nuestras tierras!!!",
		"'¡Lord Gurthang, yo te exhorto fuerzas para acabar con este infecto enemigo!",
		"'¡La cólera de Lord Gurthang te doblegará!",
	}));
	
	habilitar_conversacion();
	
	fijar_pvs_max(5000);
	fijar_pe_max(2000);
	
	anyadir_ataque_pnj(100, "mordisco de la vibora");
	anyadir_ataque_pnj(100, "columna de fuego");
}

void curar_lepra(object pj)
{
	if (environment() != environment(pj)) {
		return;
	}

	if (query_timed_property("bloqueo-Curar enfermedad")) {
		do_say("Un poco de paciencia, ¡todavía no me he recuperado de la última vez que curé a un apestado!", 0);
		return;
	}

	if (!pj->dame_incapacidades("enfermedades")) {
		do_say("No tienes la lepra... ¡por el momento! ¡así que no me hagas perder más el tiempo!", 0);
		return;
	}

	do_emote("inicia su ritual de curación.");
	do_say("¡¡¡Lord Gurthang, imploro tu magnanimidad para curar a este fiel!!!", 0);
	habilidad("curar enfermedad", pj);
	return;
}

int frases_personalizables(string tipo, object npc, object pj)
{
	switch (tipo)
	{
		case "bienvenida-pj":
			if (HORDA_NEGRA->es_oficial(pj)) {
				npc->do_emote("hace una profunda y muy sentida reverencia.");
				npc->do_say("¡Salve, "+HORDA_NEGRA->dame_titulo_corto(pj)+"! ¿qué puedo hacer para servirle?", 0);
			} else {
				npc->do_emote("se ajusta bien la máscara.");
				npc->do_say("¿Sí? ¿acaso estás infectado tú también?", 0);
			}
			break;
		case "bienvenida-npc":
			break;
		case "despedida-pj":
			pj->do_say("Adiós, tengo que irme", 0);
			break;
		case "despedida-npc":
			npc->do_say("¡Yo te bendigo en nombre de Lord Gurthang!", 0);
			break;
		default:
			return 0;
	}

	return 1;
}
