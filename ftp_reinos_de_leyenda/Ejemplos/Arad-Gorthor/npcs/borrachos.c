//Zoilder 20/01/2011
//Clase NPC basica para borrachos
//Altaresh 19 tuneo para arad
#include "../path.h";
#include <alimentacion.h>
#include INCLUDES+"interaciones.h"

inherit "/obj/monster.c";

/*
*	Función que devuelve una descripción aleatoria para el NPC.
*/
string dame_descripcion()
{
	string base_raza=capitalize(dame_numeral())+" "+dame_raza()+" borrach"+dame_vocal()+".";
	string *descripciones=({
			base_raza+" Desprende un olor insoportable a ron. Por la cantidad de veces que tropieza al andar, "
				"puede decirse que tiene una buena borrachera encima.\n",
			base_raza+" Tiene la ropa llena de suciedad, como si hubiera estado más rodando por el suelo que de pie. "
				"Su caminar es algo grotesco y sus movimientos, torpes, realizando sólo bien el movimiento de llevarse la botella a la boca.\n",
			base_raza+" Observas como sobresale bajo el sombrero una mata de pelo larga y asquerosa, llena de todo tipo de basura. "
				"Fijándote un poco más en el pelo que cae sobre sus ojos, puedes ver como está impregnado en algo parecido a vómito.\n"
	});
	return element_of(descripciones);
}

/*
*	Configuración del NPC
*/
void setup()
{
	string my_raza = element_of(({"orco","goblin","gnoll","kobold"}));
	//Se establece en primer lugar el genero del NPC, pues a partir de eso se sacan las descripciones y nombres
	fijar_genero(random(2)+1);
    
    set_name("borrach"+dame_vocal());
    add_alias(({"borrach"+dame_vocal(),my_raza}));
    add_plural(({"borrach"+dame_vocal()+"s",my_raza+"s"}));
    set_short("Borrach"+dame_vocal()+" "+my_raza);
    set_main_plural("Borrach"+dame_vocal()+"s "+my_raza+"s");
    fijar_raza(my_raza);
    //Se obtiene una descripción aleatoria
    set_long(dame_descripcion());
    fijar_religion("gurthang");
    fijar_clase("aventurero");
    ajustar_volumen(D_ALCOHOL,170);
    fijar_ciudadania("golthur");
    nuevo_lenguaje("negra", 100);

    set_random_stats(10,18);
    fijar_nivel(3 + random(16));
    fijar_altura(150+random(50));
    fijar_peso_corporal(40000+random(50)*1000);

    PONER_INTERACCIONES(TO, "borracho",({"conversacion_borrachos"}));	
  
  load_chat(30, ({
      //1,":vomita encima de un transeúnte.",
			//1,":se tambalea sin cesar y acaba chocando contra un árbol.",
			"'¿Quieres ser mi amiguito? Hic. Jugaremos juntitos. Hic.",
			"#iniciar_cualquier_interaccion",
  }));

  add_clone(BASEOBS			+ "comestibles/botella_ron.c", 1); 
  add_clone(BARMADURAS	+ "sombrero.c",1);	
  add_clone(BARMADURAS	+ "casaca.c",1);	
  add_clone(BARMADURAS	+ "pantalones.c",1);
  add_clone(BARMADURAS	+ "zapatillas.c",1);
  init_equip();
}
 int puedo_iniciar_interaccion(string id){
	 
	int res; //variable que guardara el resultado de la funcion padre

	//Comprobaciones iniciales
	//Si no tenemos ide... fuera
	if (!id)
		return 0;	
	
	//Si la funcion padre devuelve 0.... fuera
	if (!res=::puedo_iniciar_interaccion(id)){
		__debug_x(sprintf("res de funcion padre= %d",res),"arad-gorthor");
		return 0;
	}
	res=COMPROBAR_INTERACCION(TO,id);
	__debug_x(sprintf("res final= %d",res),"arad-gorthor"); 
	return res=COMPROBAR_INTERACCION(TO,id);
	
 }
