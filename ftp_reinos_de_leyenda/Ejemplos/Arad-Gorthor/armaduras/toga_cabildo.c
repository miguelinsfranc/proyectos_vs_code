// Dunkelheit 08-12-1983

inherit "/obj/armadura";

void setup()
{
	fijar_armadura_base("tunica");

	set_name("toga");
	set_short("Toga de la Orden de Cabildos");
	add_alias("orden");
	set_main_plural("Togas de la Orden de Cabildos");
	add_plural(({"togas", "cabildos"}));
	set_long("Una sencilla y minimalística toga gris portada por los integrantes de "
	"la Orden de Cabildos de la Horda Negra. Tiene bordado el símbolo de la horda "
	"en el pecho.\n");
	fijar_genero(2);
	
	fijar_encantamiento(1);
}
