// Dunkelheit 22-11-2009

inherit "/obj/armadura.c";

void setup()
{
	fijar_armadura_base("tunica");
	set_name("gabardina");
	set_short("%^BLACK%^BOLD%^Gabardina Negra%^RESET%^");
	add_alias("negra");
	set_main_plural("%^BLACK%^BOLD%^Gabardinas Negras%^RESET%^");
	add_plural(({"gabardinas", "negras"}));
	set_long("Es una larga gabardina de cuero negro que cubre todo el cuerpo, desde "
	"la barbilla hasta los tobillos. Parece muy cálida y confortable, aunque es "
	"bastante pesada.\n");
	fijar_genero(2);
	add_static_property("enfermedad", 1);
	fijar_material(3);
	fijar_peso(dame_peso() * 2);
}
