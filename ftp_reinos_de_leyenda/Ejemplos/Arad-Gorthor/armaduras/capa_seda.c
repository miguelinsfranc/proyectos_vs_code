// Dherion, 12/09/2017; capa para la recompensa de misión del arquero
inherit "/obj/armadura";
string dame_nombre_material()
{
    return "seda";
}
void setup()
{
    fijar_armadura_base("capa");
    set_name("capa de seda");
    set_short("Capa de seda");
    set_main_plural("Capas de seda");
    generar_alias_y_plurales();
    set_long(
      "Es una larga capa realizada con la piel de algún animal, forrada con una de las mejores sedas del continente."
      "\n"
    );
    fijar_material(4);
    fijar_genero(2);
    fijar_BE(20);
}
