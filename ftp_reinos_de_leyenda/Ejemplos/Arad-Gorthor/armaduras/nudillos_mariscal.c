// Dunkelheit 03-11-1983

#define DADOS 4
#define CARAS 38

inherit "/obj/armadura";

string dame_nombre_lado() { return "derecha"; }

void setup()
{
	fijar_armadura_base("guantelete");
	set_name("nudillos");
	set_short("%^WHITE%^BOLD%^Nudillos del Mariscal%^RESET%^");
	add_alias("mariscal");
	set_main_plural("%^WHITE%^BOLD%^Nudillos del Mariscal%^RESET%^");
	add_plural("mariscales");
	set_long("Un ligerísimo puño plateado que cubre desde el dedo índice hasta el "
	"meñique, dejando libre el dedo gordo para poder empuñar un arma sin "
	"problemas. Al moverlo deja una ténue estela luminosa, casi imperceptible para "
	"el ojo mortal.\n");
	fijar_genero(-1);
	fijar_encantamiento(10);
}

int set_in_use(int i)
{
	int equipado = ::set_in_use(i);
	if (i && equipado) {
		if (environment()->query_npc()) {
			set_heart_beat(2);
		} else {
			set_heart_beat(10);
		}
	} else if (!i && !equipado) {
		set_heart_beat(0);
	}
	return equipado;
}

void heart_beat()
{
	object *atacantes, victima, quien;
	int damage = 0;

	if (!quien = environment(this_object())) {
		return;
	}

	if (!query_in_use()) {
		return;
	}

	if (!sizeof(atacantes = quien->query_attacker_list())) {
		return;
	}

	victima = element_of(atacantes);
	if (environment(quien) != environment(victima)) {
		return;
	}
	
	damage = roll(DADOS,CARAS);
	if (-300 != victima->unarmed_damage(-damage, "desarmado", quien)) {
		tell_object(quien, "%^GREEN%^#%^RESET%^ Descargas un devastador revés con tus nudillos metálicos que hacen crujir la mandíbula de "+victima->query_cap_name()+".\n");
		tell_object(victima, "%^RED%^#%^RESET%^ Tu mandíbula cruje ante el devastador revés que te inflige "+quien->query_cap_name()+" con sus nudillos metálicos.\n");
		tell_accion(environment(victima), "La mandíbula de "+victima->query_cap_name()+" cruje ante el devastador revés que le inflige "+quien->query_cap_name()+" con sus nudillos metálicos.\n", "", ({ quien, victima }), quien);
	}	
}
