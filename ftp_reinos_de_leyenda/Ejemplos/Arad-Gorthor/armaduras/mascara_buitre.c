// Dunkelheit 22-11-2009

inherit "/obj/armadura.c";

void setup()
{
	fijar_armadura_base("capucha");
	set_name("mascara");
	set_short("Máscara Aguileña");
	add_alias(({"aguilenya", "aguileña"}));
	set_main_plural("Máscaras Aguileñas");
	add_plural(({"mascara", "aguilenya", "aguileña"}));
	set_long("Una máscara de blanco hueso que cubre el rostro entero. Su nariz es de "
	"lo más peculiar, pues tiene la forma del pico de un pájaro. En la punta del pico "
	"parece que hayan untado algún ungüento perfumado.\n");
	fijar_genero(2);
	add_static_property("enfermedad", 1);
	fijar_material(7);
}
