inherit "/obj/armadura"; 

void setup()	
{ 
	set_base_armour("tunica");
	set_name("tunica");
	set_short("%^BLUE%^Túnica del %^RED%^BOLD%^Profeta%^RESET%^");
	add_alias("profeta");
	set_main_plural("%^BLUE%^Túnicas de %^RED%^BOLD%^Profeta%^RESET%^");
	add_plural("profetas");
	set_long("Esta túnica de un negro color brillante, tiene un tacto frío como una joya "
		"y su peso es tan ligero como un pergamino. El hilo con el que está tejido es "
		"flexible como un pétalo pero duro e incómodo como el sedal.\n");
	fijar_material(4);//tela
	fijar_genero(2);
	ajustar_BE(25);
	add_static_property("bien",10);
	add_static_property("fuego", 10);
}
