// Dunkelheit 17-11-1983

inherit "/obj/armadura";

void setup()
{
	fijar_armadura_base("cuero");

	set_name("cuero");
	set_short("%^RED%^Peto del Totemista%^RESET%^");
	add_alias("totemista");
	set_main_plural("%^RED%^Petos del Totemista%^RESET%^");
	add_plural(({"petos", "totemistas"}));
	set_long("Es un peto de cuero recubierto de una sustancia pegajosa y membranosa, del mismo color rojo "
	"oscuro que la piel de un hobgoblin. De hecho te preguntas si no será la piel de un hobgoblin arrancada "
	"a tiras y mantenida con vida de forma mágica, aunque ya sabes hay cosas que es mejor no saber. Por ello, "
	"resulta bastante desagradable al tacto pero da un aspecto tétrico al portador. Esta pieza de armadura "
	"perteneció originalmente al líder de los rebeldes hobgoblins, Rassoodock el Aojador, fundador de la "
	"orden de los Totemistas.\n");
	fijar_genero(2);
	
	fijar_encantamiento(3);
	add_static_property("bien", 1);
}
