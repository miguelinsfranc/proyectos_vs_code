// Grimaek 22/05/2023

#include "../path.h"
inherit "/obj/armadura";

void setup()
{
	fijar_armadura_base("capa");
	set_name("capa");
	set_short("Capa del %^RED%^Señor de Mae'Ree%^RESET%^");
	set_main_plural("Capas del %^RED%^Señor de Mae'Ree%^RESET%^");
	set_long("Esta capa está imbuida de la propia esencia del Señor de Mae´Ree, el demonio conocido como Glach'Davir.\n"
        "Mae'Ree es un dominio del abismo demoniaco profundo, donde los fuegos infernales queman la poca tierra existente y las almas son atormentadas entre mares de lava y corrientes de fuego. \n"
        "Los demonios que aquí viven, han sufrido quemaduras continuamente en su piel, y los que se consiguen adaptar ganan la habilidad de usar esa lava a modo de protección sobre su piel.\n");
	add_alias(({"capa","maeree","Mae'Ree"}));
	add_plural(({"capas","maerees","Mae'Rees"}));	
	fijar_genero(2);
	fijar_peso(200);
    fijar_BE(15);
    fijar_material(3);	
	fijar_encantamiento(30);
	fijar_tamanyo_armadura(6);
    fijar_valor(500 * 15);
	
	add_static_property("messon"    , "Cuando te envuelves en la "+query_short()+" un manto de lava incandescente crece cubriendo los trozos de piel demoniaca de la capa.\n");
	add_static_property("tierra", 5);
	add_static_property("fuego", 5);
	add_static_property("acido", 5);   
	add_static_property("regeneracion_pvs", 30);	
	add_static_property("armadura-magica", 5);

}