// Dunkelheit 17-11-1983

inherit "/obj/armadura";

string dame_nombre_material() { return "Una pluma de roc negro."; }

void setup()
{
	fijar_armadura_base("capucha");

	set_name("pluma");
	set_short("Pluma de %^BLACK%^BOLD%^Roc Negro%^RESET%^");
	add_alias(({"roc", "negro"}));
	set_main_plural("Plumas de %^BLACK%^BOLD%^Roc Negro%^RESET%^");
	add_plural("plumas");
	set_long("Los rocs son criaturas colosales semejantes a águilas que habitan en las cumbres de "
	"montañas cálidas. Su fuerza es tal que es habitual verlos arrastrando desde elefantes hasta "
	"ballenas; por ello no te sorprende que esta pluma mida casi... ¡medio metro! Lo normal es que "
	"esta pluma fuera de color blanco, al igual que la mayoría de los rocs. Si ese hubiera sido el "
	"caso, esta pluma tendría un gran valor por aportar buena suerte, o al menos eso dicen las "
	"leyendas. Pero esta plmua es de un roc negro, un roc cuya mera presencia en el horizonte es "
	"vestigio de tragedias de proporciones bíblicas. Y por eso el poder de esta pluma puede ser "
	"de dudosa utilidad.\n");
	fijar_genero(2);
	
	fijar_encantamiento(4);
	
	add_static_property("ts-aliento",  -2);
	add_static_property("ts-artefacto", 1);
	add_static_property("bien", 1);
}

int set_in_use(int i)
{
	if (i) {
		if (environment()->dame_longitud_pelo() == "calvo") {
			tell_object(environment(), "¿Cómo piensas engarzarte la pluma en el pelo? ¿es que has olvidado que estás calv"+environment()->dame_vocal()+"?\n");
			return 0;
		} else {
			tell_object(environment(), "Engarzas la pluma en tu cabellera y un inquietante hormigueo recorre tu cuero cabelludo.\n");
		}
	}
	
	return ::set_in_use(i);
}
