// clang-format off
// Dunkelheit 04-11-2009
// Altaresh may19, se incluyen elementos necesarios para incluir las partes nuevas creadas por Nohur y mias.
//	aprovecho para ponerlo un poco al dia, metiendo el dirrecciones,h y el baseobs_path

// Bases
#include <direcciones.h>
#include <baseobs_path.h>

#define ARAD "/w/grimaek/arad-gorthor/"
#define LOGS ARAD+"logs/"
#define NPCS ARAD+"npcs/" 
#define DIREXPLO     ARAD+"exploracion/"
#define INCLUDES     ARAD+"include/"
#define NPCS_G "/d/golthur/npcs/"
#define NPCS_BO "/baseobs/monstruos/"
#define GUARDIAS NPCS+"guardias/"
#define ARMAS ARAD+"armas/"
#define ARMADURAS ARAD+"armaduras/"
#define OBJETOS ARAD+"objetos/"
#define ZONAS ARAD+"zonas/" 
#define BASTION ZONAS+"bastion/"
// Zonas golthur_eldor/
#define ROOMS           ARAD+"zonas/golthur_eldor/"
#define PUERTO ROOMS+"puerto/"
#define CORDILLERA  ROOMS+"cordillera/"
#define RBOREAL  ROOMS+"ruta_boreal/"
#define ZGUERRA  ROOMS+"zona_guerra/"
#define ACANTILADO ROOMS+"acantilado/"
#define SENDA  ROOMS+"acantilado/"

// Zonas zonaoeste/
#define CAMINOS ZONAS+"zonaoeste/caminos/"
#define RUTA_BOREAL CAMINOS+"ruta_boreal/"
#define CEMENTERIO ZONAS+"zonaoeste/cementerio/"
#define TOTEMISTAS ZONAS+"zonaoeste/totemistas/"
#define CAVERNAS ZONAS+"zonaoeste/cavernas/"
#define PRADERA_AULLIDOS ZONAS+"zonaoeste/pradera_aullidos/"
// Zonas zonaeste/
#define CAMINOE ZONAS+"zonaeste/camino/"
// Zonas fuera de juego
#define PLAYA ZONAS+"playa/"
#define BOSQUE ZONAS+"bosque/"
#define CUEVA ZONAS+"cueva/"
#define CERRO ZONAS+"cerro/"
#define COSTA ZONAS+"costa/"
#define RUINAS ZONAS+"ruinas/"
#define NAUFRAGIO ZONAS+"naufragio/"


// Includes
#define PASO_GENERAL ARAD+"include/paso_general.h"

// Handlers
#define GENERADOR_BASTION (ARAD+"handlers/generador_bastion")
#define GENERADOR_CEMENTERIO (ARAD+"handlers/generador_cementerio")
#define GENERADOR_COSTA (ARAD+"handlers/generador_costa")
#define GENERADOR_CUEVA (ARAD+"handlers/generador_cueva")
#define GENERADOR_TOTEMISTAS (ARAD+"handlers/generador_totemistas")
#define GENERADOR_CONCLAVE (ARAD+"handlers/generador_conclave")
#define GENERADOR_CAMINOS (ARAD+"handlers/generador_caminos") //CAMINOS ZONAOESTE
#define GENERADOR_CAMINOE (ARAD+"handlers/generador_caminoe") //CAMINOS ZONAESTE
#define GENERADOR_BOSQUE (ARAD+"handlers/generador_bosque")
#define GENERADOR_PRADERA_AULLIDOS (ARAD+"handlers/generador_pradera_aullidos")
#define ROL_NPCS (ARAD+"handlers/rol_npcs")
// Horda Negra y equipo
#define HORDA_NEGRA "/grupos/gremios/horda_negra"

// defines necesarios para parte de NOHUR y la de altaresh
//#define DOMINIO         ARAD+""
#define HANDLERS        ARAD+"handlers/"
#define GENERADORES     ARAD+"handlers/"

#define DESCRIPCIONES   ARAD+"descripciones/"
//#define OBJETOS 		DOMINIO"objetos/"
//#define NNPCS 			DOMINIO"npcs/"
#define NPCS_GOLTHUR 	"/d/golthur/npcs/"
// Macros de acceso
#define OB(x)          load_object(x)
#define HANDLER(x)     HANDLERS+"handler_" + x + ".c"
#define GENERADOR(x)   GENERADORES+x+".c"
#define GENERADOR_BASE GENERADOR("generador")
#define GENERAR(x, y)  OB(GENERADOR_BASE)->generar_sala_especifica(TO, x, y)
#define GENERAR_X(w,x,y)  OB(GENERADOR_BASE)->generar_sala_especifica(w, x, y)
#define OHANDLER(x)    OB(HANDLER(x))
#define OGENERADOR(x)  OB(OGENERADOR(x))
#define AUTOGENERAR    OHANDLER("funciones")->autogenerar_sala(TO)



// Dado una sala, zona y subzona devuelve la ruta del fichero de descripciones correspondiente
#define RUTA_DESCRIPCIONES(sala, zona, subzona) \
    OHANDLER("funciones")->buscar_descripciones(sala, zona, subzona)

// Devuelve el nombre de la estacia de una sala
#define DAME_ESTACION(sala) \
    OHANDLER("funciones")->dame_estacion(sala)

/**
 * Da un token a un jugador.
 *
 * El token tiene que estar en:
 *
 * /d/arad-gorthor/exploracion/<zona>/<nombre token>.c
 */
#define DAR_TOKEN(pj, zona, nombre) \
    OHANDLER("tokens")->dar_token(pj, zona, nombre)
