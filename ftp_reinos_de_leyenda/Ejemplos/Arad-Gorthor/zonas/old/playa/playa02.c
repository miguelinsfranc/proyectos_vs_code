// Dherion 19/04/2017: Costa de los Dioses
#include "/d/arad-gorthor/path.h";
inherit "/std/outside.c";
void setup()
{
    GENERADOR_COSTA->describir(this_object(),"playa");
    add_exit(O,PLAYA+"playa01.c","standard");
    add_exit(N,PLAYA+"orilla02.c","standard");
    add_exit(E,PLAYA+"playa03.c","standard");
}
