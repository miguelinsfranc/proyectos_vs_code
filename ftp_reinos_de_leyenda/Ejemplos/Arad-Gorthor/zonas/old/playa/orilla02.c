// Dherion 19/04/2017: Costa de los Dioses
#include "/d/arad-gorthor/path.h";
inherit "/std/outside.c";
void setup()
{
    GENERADOR_COSTA->describir(this_object(),"orilla");
    add_exit(O,PLAYA+"orilla01.c","standard");
    add_exit(S,PLAYA+"playa02.c","standard");
    add_exit(E,PLAYA+"orilla03.c","standard");
}
