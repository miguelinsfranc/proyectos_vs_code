#include "../../path.h"
inherit "/std/tienda";

void setup() {
        set_short("%^CYAN%^Tienda de Abastecimientos Marítimos de Juggash Bur%^RESET%^");
        set_long("La tienda del puerto de Juggash Bur es una pequeña casucha de madera en la que se venden algunos "
			"artefactos necesarios para la navegación, en su mayoría, obtenidos en el pillaje y revendidos al mejor "
			"postor.\n");
        set_light(50);
        fijar_tipo_tienda("general");
	
        poner_tabla_objetos(([
            "/baseobs/misc/sextante":roll(2,4),
            "/baseobs/misc/catalejo":roll(1,3),
            ]));
        crear_cartel("grabado");
		
        add_exit(O, COSTA + "puerto01.c","door");
		modify_exit(O, ({"tamaño",7}));
		setup_door(O, 0, 0);
		poner_tendero(NPCS+"dependiente_tienda.c");
}
