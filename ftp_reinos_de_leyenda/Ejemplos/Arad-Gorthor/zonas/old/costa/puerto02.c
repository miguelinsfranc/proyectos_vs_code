//Rezzah 10/11/2009
// Eckol 11/Oct/2014 Le añado ciudadania.

#include "../../path.h"
inherit "/std/taberna";
//Configura el Número de NPCS máximo a usar aquí. De cualquiera de los tipos definidos
#define NUM_MAX_NPCS 3

void setup() {
	int random;
	set_short("%^GREEN%^BOLD%^Taberna de Juggash Bur%^RESET%^");
	set_long("La taberna del puerto, es el infecto origen de todo el desagrable olor y bullicio "
		"que reina en el puerto. Un sencillo mostrador circular tiene a varios afanosos goblins "
		"corretaendo por su interior atendiendo las, poco educadas, solicitudes de la clientela. "
		"En el centro de la taberna una enorme plancha metálica llena de grasa y carbón reposa "
		"sobre cuatro pilares de piedra entre los cuales unas brasas constantemente alimentadas "
		"calientan el metal para convertirlo en un asador gigante.\n");
	fijar_luz(50);
	poner_tabernero(NPCS+"tabernero_puerto");
	poner_carisma(3);
	poner_plato("Pescado variado a la plancha", "comida", 30);
	poner_alias("pescado", "variado");
	poner_plato("Otra cosa a la plancha", "comida", 60);
	poner_alias("otra","cosa");
	poner_plato("Aguardiente", "alcohol",8);
	poner_alias("Aguardiente","aguardiente");
	
	fijar_ciudadania("arad_gorthor");
	
	poner_cartel("piel");
	
	add_exit(S, COSTA + "puerto00", "plain");
	add_exit(O, COSTA + "puerto03", "plain");
	add_exit(E, COSTA + "puerto01", "plain");
	add_exit(N, COSTA + "muelle", "plain");
	set_exit_color("green");
	set_zone("puerto");
	//Puede haber hasta 3 NPCS en la room de entre los siguientes tipos prostitutos, marineros, borrachos
	//Puede haber 1 de cada tipo, 2 de uno, etc. No serán los 3 del mismo tipo. (O sí, eso lo decide el aleatorio).
	//Se meterán tantos NPCS como el random siguiente indique
	for(int num=0;num<random(NUM_MAX_NPCS)+1;)
	{
		//Número de NPCS a meter (nunca sobrepasará el número Máximo definido)
		random=random(NUM_MAX_NPCS-num)+1;
		//Al numero de NPCs añadidos se le incrementa el random
		num+=random;
		//Se meten tantos NPCS aleatorios como diga el Random
		add_clone(NPCS + element_of(({"prostitutos", "marineros", "borrachos"})), random);
	}
}
