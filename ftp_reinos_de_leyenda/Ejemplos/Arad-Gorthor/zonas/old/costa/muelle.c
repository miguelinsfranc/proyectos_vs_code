#include "../../path.h"
inherit "/std/muelle";

void setup()
{
	set_short("%^BLUE%^BOLD%^Juggash Bur%^RESET%^, Muelle");
	set_long("El apestoso muelle");
	set_light(90);
	set_exit_color("cian");
	fijar_coordenadas( ({33, 14, 0}) );//33 Oeste 14 Sur
	fijar_clima("mar_de_los_dioses");
	fijar_ciudadania("golthur");
	add_exit(S, COSTA + "puerto02", "plain");
	add_exit(SE, COSTA + "puerto01", "plain");
	add_exit(SO, COSTA + "puerto03", "plain");
	add_exit(S, COSTA + "puerto02", "plain");
	
	//Se añaden pescadores. Aleatoriamente 1 o 2
	add_clone(NPCS + "pescadores.c", random(2)+1);
}
