//Rezzah 10/11/2009

#include "../../path.h"
inherit "/std/subterraneo.c";
//Configura el Número de NPCS máximo a usar aquí. De cualquiera de los tipos definidos
#define NUM_MAX_NPCS 3

void setup() {
	int random;
	set_short("%^BLUE%^BOLD%^Juggash Bur%^RESET%^, Puerto de Golthur Orod");
	set_long("Estás en la escalinata excavada toscamente sobre la piedra que da acceso al "
		"Juggash Bur, zona portuaria de Golthur Orod. El mal estado en el que se encuentra "
		"te hace temer por una estrepitosa caída pero no parece haber muchos trozos de orco "
		"desparramado por el suelo.\n");
	fijar_luz(90);
	add_item(({"escala", "escalera","escalinata","escalas","escaleras","escalinatas"}), 
		"Una pared de dura roca ha sido golpeda brutalmente hasta ceder a los deseos de los "
		"orcos de adquirir la forma aproximada de una escalera.");
	add_exit(AR, CAMINOS + "bastion_costa84", "stairs");
	add_exit(NE, COSTA + "puerto01", "plain");
	add_exit(N, COSTA + "puerto02", "plain");
	add_exit(NO, COSTA + "puerto03", "plain");
	set_exit_color("cian");
	if (!random(3)) add_clone(BMONSTRUOS+"animales/cangrejo.c", 1+random(3));
	//Puede haber hasta 3 NPCS en la room de entre los siguientes tipos prostitutos, marineros, borrachos
	//Puede haber 1 de cada tipo, 2 de uno, etc. No serán los 3 del mismo tipo. (O sí, eso lo decide el aleatorio).
	//Se meterán tantos NPCS como el random siguiente indique
	for(int num=0;num<random(NUM_MAX_NPCS)+1;)
	{
		//Número de NPCS a meter (nunca sobrepasará el número Máximo definido)
		random=random(NUM_MAX_NPCS-num)+1;
		//Al numero de NPCs añadidos se le incrementa el random
		num+=random;
		//Se meten tantos NPCS aleatorios como diga el Random
		add_clone(NPCS + element_of(({"prostitutos", "marineros", "borrachos"})), random);
	}
}

