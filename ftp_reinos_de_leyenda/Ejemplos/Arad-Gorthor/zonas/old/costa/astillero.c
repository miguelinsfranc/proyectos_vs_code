//Eckol 11Oct14: Rehago el astillero basándome en el de Aldara.

#include "/d/arad-gorthor/path.h"
inherit "/std/tienda.c";

void setup()
{
	set_short("Astillero Juggashi");
    set_long("Los poderosos navíos de Golthur Orod provienen de este astillero, en el que afanosamente "
		"trabajan decenas de goblins y orcos dirigidos con cruel eficiencia por un grupo de kobolds de "
		"andares tan dignos como un rey. A sus órdenes descomunales gnolls de férrea musculatura hostigan "
		"a los empleados para que no decaiga su trabajo.\n");
	fijar_luz(60);
	set_exit_color("amarillo_flojo");

	fijar_tipo_tienda("astillero");
	fijar_tabla_objetos(([
	"/d/oceano/barcos/escrituras/escritura_ballesta_ligera": roll(1,5),
"/d/oceano/barcos/escrituras/escritura_ballesta": roll(1,4),
"/d/oceano/barcos/escrituras/escritura_ballesta_pesada": 1,
"/d/oceano/barcos/escrituras/escritura_cañon_ligero": roll(1,3),
"/d/oceano/barcos/escrituras/escritura_cañon": roll(1,2),
"/d/oceano/barcos/escrituras/escritura_cañon_pesado": 1,
"/d/oceano/barcos/escrituras/escritura_catapulta_ligera": roll(1,2),
"/d/oceano/barcos/escrituras/escritura_catapulta": roll(1,2),
"/d/oceano/barcos/escrituras/escritura_catapulta_pesada": 1,
"/d/oceano/barcos/municiones/bala": roll(2,10),
"/d/oceano/barcos/municiones/obus": roll(2,8),
"/d/oceano/barcos/municiones/saeta": roll(2,25),
"/baseobs/misc/arpeo_abordaje": roll(1,4),
		"/baseobs/misc/catalejo": roll(1,3),
		"/baseobs/misc/sextante": roll(1,3),
		"/d/oceano/barcos/escrituras/escritura_bote": 3,
		"/d/oceano/barcos/escrituras/escritura_barco": 1,
		"/d/oceano/barcos/escrituras/escritura_cañonero": 1,
		"/d/oceano/barcos/escrituras/escritura_crucero": 1,
		"/d/oceano/barcos/escrituras/escritura_barco": 1,
		"/d/oceano/barcos/escrituras/escritura_destructor": 1,
		"/d/oceano/barcos/escrituras/escritura_fragata": 1,
		"/d/oceano/barcos/escrituras/escritura_goleta": 1,
		// "/d/oceano/barcos/escrituras/escritura_goleta_arcana": 1,
		"/d/oceano/barcos/escrituras/escritura_velero": 1,
"/d/oceano/barcos/pinturas/bote_pintura_amarillo": !random(3),
"/d/oceano/barcos/pinturas/bote_pintura_azul": !random(3),
"/d/oceano/barcos/pinturas/bote_pintura_azul_intenso": !random(3),
"/d/oceano/barcos/pinturas/bote_pintura_blanco": random(2),
"/d/oceano/barcos/pinturas/bote_pintura_cian": random(2),
"/d/oceano/barcos/pinturas/bote_pintura_cian_intenso": random(2),
"/d/oceano/barcos/pinturas/bote_pintura_gris": random(2),
"/d/oceano/barcos/pinturas/bote_pintura_magenta": !random(5),
"/d/oceano/barcos/pinturas/bote_pintura_magenta_intenso": !random(5),
"/d/oceano/barcos/pinturas/bote_pintura_marron": random(2),
"/d/oceano/barcos/pinturas/bote_pintura_negro": !random(5),
"/d/oceano/barcos/pinturas/bote_pintura_rojo": !random(3),
"/d/oceano/barcos/pinturas/bote_pintura_rojo_intenso": !random(3),
"/d/oceano/barcos/pinturas/bote_pintura_verde": !random(4),
"/d/oceano/barcos/pinturas/bote_pintura_verde_intenso": !random(4),

	]));

	add_exit(E, COSTA + "puerto03.c", "door");
	modify_exit(E, ({"tamaño",7}));
	setup_door(E, 0, 0);
    poner_tendero(NPCS + "dependiente_astillero.c");
}
