//Rezzah 10/11/2009

#include "../../path.h"
inherit "/std/subterraneo.c";
//Configura el Número de NPCS máximo a usar aquí. De cualquiera de los tipos definidos
#define NUM_MAX_NPCS 3

void setup() {
	int random;
	set_short("%^BLUE%^BOLD%^Juggash Bur%^RESET%^, Puerto de Golthur Orod");
	set_long("El puerto de Juggash Bur es, como todos los puertos, un lugar sucio que apesta a pescado "
		"y salitre por todas partes y, dada la naturaleza de sus administradores, resulta extremadamente "
		"peligroso. Por todas partes se ven marineros embutidos en sus ligeras ropas pues los orcos no "
		"temen el poderoso frío del mar mirar con gesto pendenciero y desafíante a cuantos osan acercarse "
		"más de lo necesario. Una puerta hacia el este se abre y cierra continuamente con afanados jóvenes "
		"que sirven como recaderos con frecuencia para los marineros aunque sin duda lo más llamativo es el "
		"rugido continuo proveniente de la taberna más al oeste.\n");
	fijar_luz(90);
	add_exit(SO, COSTA + "puerto00", "plain");
	add_exit(O, COSTA + "puerto02", "plain");
	add_exit(NO, COSTA + "muelle", "plain");
	add_exit(E, COSTA + "tienda", "door");
	setup_door(E, 0, 0);
	modify_exit(E, ({"tamaño",7}));
	set_exit_color("cian");
	set_zone("puerto");
	if (!random(3)) add_clone(BMONSTRUOS+"animales/cangrejo.c", 1+random(3));
	//Puede haber hasta 3 NPCS en la room de entre los siguientes tipos prostitutos, marineros, borrachos
	//Puede haber 1 de cada tipo, 2 de uno, etc. No serán los 3 del mismo tipo. (O sí, eso lo decide el aleatorio).
	//Se meterán tantos NPCS como el random siguiente indique
	for(int num=0;num<random(NUM_MAX_NPCS)+1;)
	{
		//Número de NPCS a meter (nunca sobrepasará el número Máximo definido)
		random=random(NUM_MAX_NPCS-num)+1;
		//Al numero de NPCs añadidos se le incrementa el random
		num+=random;
		//Se meten tantos NPCS aleatorios como diga el Random
		add_clone(NPCS + element_of(({"prostitutos", "marineros", "borrachos"})), random);
	}
}

