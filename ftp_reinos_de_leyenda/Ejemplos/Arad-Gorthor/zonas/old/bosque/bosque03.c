// Dherion 28/04/2017: Bosquecillo
#include "/d/arad-gorthor/path.h";
inherit "/std/bosque.c";
void setup()
{
    GENERADOR_BOSQUE->describir(this_object(),"bosque");
    add_exit(O,BOSQUE+"bosque04.c","bosque");
    add_exit(NO,BOSQUE+"bosque09.c","bosque");
    add_exit(N,BOSQUE+"bosque08.c","bosque");
    add_exit(E,BOSQUE+"bosque06.c","bosque");
    add_exit(NE,BOSQUE+"bosque07.c","bosque");
    add_exit(SO,BOSQUE+"bosque01.c","bosque");
    add_exit(SE,BOSQUE+"bosque05.c","bosque");
    add_exit(S,BOSQUE+"bosque02.c","bosque");
}
