// Dherion 28/04/2017: Bosquecillo
#include "/d/arad-gorthor/path.h";
inherit "/std/bosque.c";
void setup()
{
    GENERADOR_BOSQUE->describir(this_object(),"bosque");
    add_exit(N,BOSQUE+"bosque03.c","bosque");
    add_exit(NE,BOSQUE+"bosque06.c","bosque");
    add_exit(O,BOSQUE+"bosque01.c","bosque");
    add_exit(E,BOSQUE+"bosque05.c","bosque");
    add_exit(NO,BOSQUE+"bosque04.c","bosque");
}
