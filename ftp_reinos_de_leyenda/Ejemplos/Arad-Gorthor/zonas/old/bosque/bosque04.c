// Dherion 28/04/2017: Bosquecillo
#include "/d/arad-gorthor/path.h";
inherit "/std/bosque.c";
void setup()
{
    GENERADOR_BOSQUE->describir(this_object(),"bosque");
    add_exit(S,BOSQUE+"bosque01.c","bosque");
    add_exit(N,BOSQUE+"bosque09.c","bosque");
    add_exit(E,BOSQUE+"bosque03.c","bosque");
    add_exit(NE,BOSQUE+"bosque08.c","bosque");
    add_exit(SE,BOSQUE+"bosque02.c","bosque");
}
