// Dherion 28/04/2017: Bosquecillo
#include "/d/arad-gorthor/path.h";
inherit "/std/bosque.c";
void setup()
{
    GENERADOR_BOSQUE->describir(this_object(),"bosque");
    add_exit(NO,RUTA_BOREAL+"caminobosque01.c","path");
    add_exit(SE,BOSQUE+"bosque08.c","bosque");
    add_exit(S,BOSQUE+"bosque09.c","bosque");
    add_exit(E,BOSQUE+"bosque11.c","bosque");
}

