// Dherion 28/04/2017: Bosquecillo
#include "/d/arad-gorthor/path.h";
inherit "/std/bosque.c";
void setup()
{
    GENERADOR_BOSQUE->describir(this_object(),"bosque");
    add_exit(N,BOSQUE+"bosque10.c","bosque");
    add_exit(S,BOSQUE+"bosque06.c","bosque");
    add_exit(NO,BOSQUE+"bosque11.c","bosque");
    add_exit(SO,BOSQUE+"bosque03.c","bosque");
    add_exit(O,BOSQUE+"bosque08.c","bosque");
}
