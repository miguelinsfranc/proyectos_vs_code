// Dherion 28/04/2017: Bosquecillo
#include "/d/arad-gorthor/path.h";
inherit "/std/bosque.c";
void setup()
{
    GENERADOR_BOSQUE->describir(this_object(),"bosque");
    add_exit(AB,CUEVA+"cueva01.c","path");
    add_exit(NE,BOSQUE+"bosque10.c","bosque");
    add_exit(NO,BOSQUE+"bosque12.c","bosque");
    add_exit(N,BOSQUE+"bosque11.c","bosque");
    add_exit(S,BOSQUE+"bosque03.c","bosque");
    add_exit(O,BOSQUE+"bosque09.c","bosque");
    add_exit(E,BOSQUE+"bosque07.c","bosque");
    add_exit(SE,BOSQUE+"bosque06.c","bosque");
    add_exit(SO,BOSQUE+"bosque04.c","bosque");
}
