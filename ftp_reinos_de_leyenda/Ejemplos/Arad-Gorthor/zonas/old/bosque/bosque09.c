// Dherion 28/04/2017: Bosquecillo
#include "/d/arad-gorthor/path.h";
inherit "/std/bosque.c";
void setup()
{
    GENERADOR_BOSQUE->describir(this_object(),"bosque");
    add_exit(N,BOSQUE+"bosque12.c","bosque");
    add_exit(S,BOSQUE+"bosque04.c","bosque");
    add_exit(NE,BOSQUE+"bosque11.c","bosque");
    add_exit(E,BOSQUE+"bosque08.c","bosque");
    add_exit(SE,BOSQUE+"bosque03.c","bosque");
}
