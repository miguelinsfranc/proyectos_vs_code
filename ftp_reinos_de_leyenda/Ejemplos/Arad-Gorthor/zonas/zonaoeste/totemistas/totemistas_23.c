// Made by Dunkelheit  11/15/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.
// Empieza aqui

#include "../../../path.h"
inherit TOTEMISTAS"base_totemistas";

#define ROOM_VERTEDERO CEMENTERIO+"vertedero_20"

void setup() {

   GENERADOR_TOTEMISTAS->describir(this_object(), "cavernas");
   // set_light(30);
   // set_short("Tunnels");
   // set_long("LONG\n\n");
	add_exit(O,TOTEMISTAS+"totemistas_22","corridor");
	add_exit("vertedero",ROOM_VERTEDERO,"door",1,10);
	modify_exit("vertedero",({"funcion","puede_pasar"}));
	//Items para ayudar al jugador a salir de la caverna si la salida está oculta en el exterior
	add_item("antorchas", "Son antorchas que iluminan muy poco. Los "
			"hobgoblins no son conocidos precisamente por su amor a la luz. Una de las antorchas está algo más inclinada de lo normal, parece que se puede %^CURSIVA%^BOLD%^WHITE%^girar%^RESET%^.\n");
}

void init()
{
	add_action("girar","girar");
}

int girar(string str)
{
	object room_vertedero=load_object(ROOM_VERTEDERO);
	if(!str) return notify_fail("¿Qué quieres girar?\n");
	if(member_array(lower_case(str),({"antorcha","la antorcha"}))==-1)
		return notify_fail("Difícilmente vas a poder girar eso.\n");
	tell_object(TP,"Giras la antorcha con fuerza, que acto seguido vuelve a su posición inicial.\n");
	tell_accion(TO,TP->query_short()+" gira una de las antorchas de la pared de la cueva, volviendo instantáneamente a su posición inicial.\n",
		"Oyes un ruido extraño de metal sobre piedra. Te fijas y ves como una antorcha se mueve.\n",({TP}),TP);
	if(room_vertedero->desplegando_puerta())
	{
		return notify_fail("Ya se está desplegando la puerta al exterior.\n");
	}
	if(member_array("caverna",room_vertedero->query_dest_dir())==-1 &&
		!sizeof(find_match("acumulación de deshechos",room_vertedero))) //Si no existe la salida caverna en la room del vertedero y no existe la acumulación de deshechos
	{
		room_vertedero->funcion_aparece_entrada(1);
		return 1;
	}
	call_out((:tell_object:),3,TP,"No parece suceder nada.\n");
	return 1;
}

/*
*	Función que controla si se puede pasar por la puerta.
*	Se podrá pasar sólo cuando haya aparecido la puerta en la room correspondiente del vertedero
*/
int puede_pasar(string salida,object caminante,string msj_especial)
{
	object room_vertedero=load_object(ROOM_VERTEDERO);
	if(room_vertedero->query_property("caverna_taponada")) //Si se ha taponado la entrada de la caverna al intentar romper la puerta
		return notify_fail("Un derrumbe de deshechos tapona la salida al vertedero.\n");
	if(member_array("caverna",room_vertedero->query_dest_dir())==-1) //Si no existe la salida caverna en la room del vertedero
		return notify_fail("La salida al vertedero está oculta, no puedes pasar. Deberás buscar un modo de mostrar la puerta al exterior.\n");
	return 1;
}
