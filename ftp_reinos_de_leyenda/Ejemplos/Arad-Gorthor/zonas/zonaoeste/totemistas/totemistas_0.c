// Dunkelheit 16-11-2009

#include "../../../path.h"
inherit "/std/subterraneo";

string dame_sala_inicial(object player)
{
	return TOTEMISTAS + "totemistas_23";
}

void setup()
{
	set_short("%^ORANGE%^Caverna de los Totemistas%^RESET%^: Habitación del Líder");
	set_long("Te encuentras en lo más profundo de la caverna de los Totemistas, en la sala del líder de los hobgoblins, "
		"una pequeña sala cuyo único mobiliario es una piltra destartalada, una mesa con algún que otro libro "
		"y varias capas de colores llamativos colgadas en las paredes. \n");
	add_item("piltra","Una piltra de paja, destrozada por algunas zonas.");
	add_item("mesa","Una mesa de madera astillada, con un par de libros sobre ella.");
	add_item(({"libro","libros"}),"Unos extraños libros con unas inscripciones que te son desconocidas.");
	add_item(({"capa","capas"}),"Unas capas de colores llamativos, pertenecientes seguramente al líder de los hobgoblins.");
	set_exit_color("negro");
	fijar_luz(30);
	
	add_clone(NPCS+"campeon_hobgoblin", 3);
	add_clone(NPCS+"rassoodock", 1);

	add_exit(S, TOTEMISTAS+"totemistas_2.c", "corridor");
}
