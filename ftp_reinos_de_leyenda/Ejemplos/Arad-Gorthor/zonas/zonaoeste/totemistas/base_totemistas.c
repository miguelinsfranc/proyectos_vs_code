// Dunkelheit 15-11-2009
#include "../../../path.h"
inherit "/std/subterraneo";

string dame_sala_inicial(object player)
{
	return TOTEMISTAS + "totemistas_23";
}

int chequeo_salida(string tx, string verbo, object ob_caminante, object ob_que_sigo)
{
	if (ob_caminante->query_creator() && !ob_caminante->query_property("debug")) {
		return 1;
	}

	if (secure_present(NPCS + "hobgoblin", this_object())
		|| secure_present(NPCS + "guerrero_hobgoblin", this_object())
		|| secure_present(NPCS + "totemista_hobgoblin", this_object())
		|| secure_present(NPCS + "campeon_hobgoblin", this_object())) 
	{
		return notify_fail("Estás completamente rodead"+ob_caminante->dame_vocal()+" por hobgoblins que te impiden avanzar en cualquier dirección.\n", this_player());
	}

	return 1;
}
