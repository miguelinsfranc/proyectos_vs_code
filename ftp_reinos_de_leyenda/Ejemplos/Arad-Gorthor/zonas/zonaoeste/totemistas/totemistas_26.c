// Made by Dunkelheit  11/15/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

inherit "/d/arad-gorthor/zonas/totemistas/base_totemistas";
#include "../../../path.h"

void setup() {

   GENERADOR_TOTEMISTAS->describir(this_object(), "cavernas");
   // set_light(30);
   // set_short("Tunnels");
   // set_long("LONG\n\n");
add_exit(NO,TOTEMISTAS+"totemistas_18.c","corridor");
add_exit(SE,TOTEMISTAS+"totemistas_32.c","corridor");
}
