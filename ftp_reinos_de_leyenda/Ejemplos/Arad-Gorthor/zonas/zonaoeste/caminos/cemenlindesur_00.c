// Grimaek 10/04/2023

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_CAMINOS->describir(this_object(), "cemen_lindesur");	
	add_exit(NE, CEMENTERIO + "cementerio_91", "path");	
	add_exit(S, CAMINOS + "cemenlindesur_01", "path");

}
