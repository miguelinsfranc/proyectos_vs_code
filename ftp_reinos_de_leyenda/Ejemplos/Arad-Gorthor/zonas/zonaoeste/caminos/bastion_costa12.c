// Dunkelheit 04-11-2009

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_CAMINOS->describir(this_object(), "prox_bastion");	
	add_exit(N, CAMINOS + "bastion_costa13", "path");
	add_exit(SE, CAMINOS + "bastion_costa11", "path");
   add_exit(O, PRADERA_AULLIDOS + "pradera00", "path");
   add_sign("Un tosco cartel de madera, clavado en un poste. ",
				"Norte: Juggash Bur, Puerto de Golthur Orod\n"
				"Oeste: Desvío a la Pradera de los Aullidos\n"
				"Sudeste: Bastión de Arad-Gorthor\n",
				0,
				"Cartel de madera",
				"negra");
}
