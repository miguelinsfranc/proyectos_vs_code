// Dunkelheit 04-11-2009

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_CAMINOS->describir(this_object(), "prox_bastion");	
	add_exit(N, CAMINOS + "bastion_costa15", "path");
	add_exit(SE, CAMINOS + "bastion_costa13", "path");
}
