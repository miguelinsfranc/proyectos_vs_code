//Dherionh 28/03/2017 # Camino a Eldor
#include "../../../path.h";
inherit "/d/arad-gorthor/zonas/caminos/ruta_boreal/base_camino_eldor";
void setup()
{
    add_exit(SO,RUTA_BOREAL+"camino106.c","path");
    add_exit(E,RUTA_BOREAL+"camino108.c","path");
    set_zone("camino_eldor");
    GENERADOR_CAMINOS->describir(this_object(),"camino_eldor");
}
