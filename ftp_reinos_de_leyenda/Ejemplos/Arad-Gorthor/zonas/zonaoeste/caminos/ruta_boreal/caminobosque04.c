// Dherion Mayo 2017 # Camino al bosque
#include "../../../path.h";
inherit "/std/bosque.c";
void setup()
{
    add_exit(NE,RUTA_BOREAL+"caminobosque05.c","path");
    add_exit(S,RUTA_BOREAL+"caminobosque03.c","path");
    set_zone("camino_bosque");
    GENERADOR_CAMINOS->describir(this_object(),"camino_bosque");
}
