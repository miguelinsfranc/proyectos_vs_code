// Dherion Mayo 2017 # Camino al bosque
#include "../../../path.h";
inherit "/std/bosque.c";
void setup()
{
    add_exit(N,RUTA_BOREAL+"camino230.c","path");
    add_exit(SO,RUTA_BOREAL+"caminobosque04.c","path");
    set_zone("camino_bosque");
    GENERADOR_CAMINOS->describir(this_object(),"camino_bosque");
}
