// Dherion Mayo 2017 # Camino al bosque
#include "../../../path.h";
inherit "/std/bosque.c";
void setup()
{
    add_exit(NO,RUTA_BOREAL+"caminobosque02.c","path");
    add_exit(SE,BOSQUE+"bosque12.c","path");
    set_zone("camino_bosque");
    GENERADOR_CAMINOS->describir(this_object(),"camino_bosque");
}
