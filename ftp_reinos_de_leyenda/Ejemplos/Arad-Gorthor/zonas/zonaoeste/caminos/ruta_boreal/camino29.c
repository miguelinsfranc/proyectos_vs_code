//Rezzah 26/01/2011 # Camino a Eldor
#include "../../../path.h";

#define GUARDIA "protector" //alias de quien protege (npc)
#define CIUDADANIA "Golthur" //lo que tienen estatus malo aquí no pueden pasar
#define CIUDADANIAS_DEFENSORAS ({"golthur","ancarak","mor_groddur","dendra","ar'kaindia","arad-gorthor","grimoszk"}) //estas ciudadanias pueden defender
#define CAOTICO_MALVADO //para que los mensaje sean sangrientos
#define LISTA_BANDOS "malo" //para que bloquee a todos los que no son del bando malo
#define AGRESIVIDAD 1 //para que pegue a todos los bloqueados
#define PERMITIR_SOLO_BRIBONES 1 //para que pasen todo tipo de bribones
#define SALVACION_ESCONDIDOS 20 //se puede sigilar una de cada cinco veces
#define MSJ "Los protectores de la frontera no te permiten pasar.\n"
#define MSJ_ROOM "Los protectores de la frontera impiden el paso de "+caminante->query_short()+".\n"
#define MSJ_DORMIDO "Cruzas con cautela esperando no ser despertar a los guardias."

#include <paso.h>

inherit "/d/arad-gorthor/zonas/caminos/ruta_boreal/base_camino_eldor";
void setup()
{
    add_exit(NE,RUTA_BOREAL+"camino30.c","path");
    add_exit(SO,RUTA_BOREAL+"camino28.c","path");
    modify_exit(SO,({"funcion","pasar"}));
    set_zone("camino_eldor");
    GENERADOR_CAMINOS->describir(this_object(),"camino_eldor"); 
    set_short("Ruta Boreal: Frontera con Eldor");
    set_long("En mitad de la nada, en el largo camino que durante siglos han transitado todo tipo de criaturas "
      "tanto en guerra como en paz, encuentras cortado el camino hacia el suroeste por una pequeña vaya fácilmente "
      "rodeable, sobre ésta, a una altura considerable, una gigantesca campana dorada flota suspendida de ninguna parte "
      "como si de una ilusión se tratara.\n");
    set_exit_color("amarillo");
    add_item("campana", "Una tosca pero gigantesca campana que parece hecha de oro. Al fijarte bien podrías jurar que puedes ver a través de ella, como si no fuera un objeto real, sino sólo el fantasma de una campana.");
    add_clone(NPCS+"protector",3);
}
