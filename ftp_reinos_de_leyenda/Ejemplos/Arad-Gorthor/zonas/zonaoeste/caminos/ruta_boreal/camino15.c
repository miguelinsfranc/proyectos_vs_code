//Kiratxia 17/01/2011
#include "../../../path.h";
inherit "/d/arad-gorthor/zonas/caminos/ruta_boreal/base_camino_eldor";
#define TOKEN ("/d/arad-gorthor/zonas/caminos/ruta_boreal/tokens/apartar_matorral.c")
void setup()
{
    add_exit(NO,RUTA_BOREAL+"camino14.c","path");
    add_exit(E,RUTA_BOREAL+"camino16.c","path");
    set_zone("camino_eldor");
    GENERADOR_CAMINOS->describir(this_object(),"camino_eldor"); 
    set_long(query_long()+
      "Entre unos matorrales distingues lo que parece ser un pequeño sendero.\n");
}
void init(){
    anyadir_comando("apartar","los matorrales","do_apartar");
    ::init();
}
int do_apartar(){
    object token=load_object(TOKEN);
    tell_object(this_player(),"Apartas los matorrales y descubres que en efecto, hay un camino que continúa por el norte.\n");
    add_exit(N,RUTA_BOREAL+"caminocosta05.c","path");
    tell_accion(environment(this_player()), this_player()->query_cap_name()+" aparta los matorrales y abre un camino.\n", "Escuchas ruido a tu alrededor\n", ({this_player()}), this_player());
    if (token) {
        token->dar_token(this_player());
    }
    else {
        tell_object(this_player(),"Ha habido un error al asignar el token de exploración. Por favor, repórtalo a los inmortales.\n");
    }
    return 1;
}
