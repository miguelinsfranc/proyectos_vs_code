//Dherionh 28/03/2017 # Camino a Eldor
#include "../../../path.h";
inherit "/d/arad-gorthor/zonas/caminos/ruta_boreal/base_camino_eldor";
void setup()
{
    add_exit(NO,RUTA_BOREAL+"camino70.c","path");
    add_exit(NE,RUTA_BOREAL+"camino72.c","path");
    set_zone("camino_eldor");
    GENERADOR_CAMINOS->describir(this_object(),"camino_eldor");
}
