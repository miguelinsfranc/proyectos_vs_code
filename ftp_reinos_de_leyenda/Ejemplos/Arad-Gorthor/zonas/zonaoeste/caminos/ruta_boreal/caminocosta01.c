// Dherion 17/04/2017; Camino hacia la playa de la costa de los dioses
#include "../../../path.h";
inherit "/std/outside.c";
void setup()
{
    add_exit(N,PLAYA+"playa05.c","path");
    add_exit(S,RUTA_BOREAL+"caminocosta02.c","path");
    set_zone("camino_costa");
    GENERADOR_CAMINOS->describir(this_object(),"camino_costa");
}
