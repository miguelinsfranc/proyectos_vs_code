// Dunkelheit 04-11-2009

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	set_short("Ladera Sur de la Meseta Oriental");
	set_long("La ladera de la meseta oriental es menos irregular e intransitable "
	"en este sendero, de procedencia obviamente humana -u orca-. Los numerosos "
	"matojos secos están aplastados en este sinuoso camino que atraviesa la "
	"ladera sur de la meseta. A lo lejos se ve la cima, y hacia el sur un "
	"camino continua hacia el este para adentrarse en los dominios de Arad Gorthor.\n");
	add_item(({"camino", "tierra", "suelo"}), "El suelo aquí está sembrado de "
	"piedras y matojos aplastados.\n");
	add_item(({"pie", "matas", "hierbas"}), "Vegetación propia de terrenos "
	"áridos como este. Sin duda el azote del volcán de N'argh, durante el "
	"Cataclismo de la Era 3ª, acabó con el verdor del que antaño esta zona "
	"disfrutaba.\n");
	add_item("meseta", "Es la meseta oriental del Erial de los Condenados, "
	"una peligrosa zona infestada de lobos y alimañas.\n");
	add_item("ladera", "La ladera de la meseta es prácticamente intransitable, "
	"necesitarías ayudarte de tus extremidades superiores para poder subirla.\n");
	add_item("camino", "El camino une el sendero que conduce a Dendra con el "
	"sendero que conduce a Arad Gorthor. A lo lejos pueden verse estructuras de "
	"madera derruídas.\n");
	add_item("estructuras", "Parecen máquinas de asedio.\n");
	
	add_exit(N, CAMINOS + "pie-meseta01", "road");
	add_exit(S, CAMINOS + "pie07", "road");
	
	set_exit_color("amarillo");
	fijar_luz(90);
}
