// Grimaek 10/04/2023

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_CAMINOS->describir(this_object(), "cemen_lindesur");	
	add_exit(NE, CAMINOS + "cemenlindesur_49", "path");	
	add_exit(O, CAMINOS + "cemenlindesur_51", "path");

}
