// Grimaek 10/04/2023

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_CAMINOS->describir(this_object(), "cemen_lindesur");	
	add_exit(E, CAMINOS + "cemenlindesur_02", "path");	
	add_exit(SO, CAMINOS + "cemenlindesur_04", "path");

}
