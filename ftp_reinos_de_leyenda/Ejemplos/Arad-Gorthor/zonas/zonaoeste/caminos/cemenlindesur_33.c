// Grimaek 10/04/2023

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_CAMINOS->describir(this_object(), "cemen_lindesur");	
	add_exit(N, CAMINOS + "cemenlindesur_32", "path");	
	add_exit(S, CAMINOS + "cemenlindesur_34", "path");

}
