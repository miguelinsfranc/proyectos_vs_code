// Rezzah 14/01/2011

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
    GENERADOR_CAMINOS->describir(this_object(), "prox_puerto");	
    add_exit(E,RUTA_BOREAL+"camino01.c","path");
    add_exit(S, CAMINOS + "bastion_costa83", "path");
    add_exit(AB, COSTA + "puerto00.c", "stairs");
    set_long(query_long()+"Unas rocas pulidas con escasa gracia descienden hacia la zona portuaria.\n");
    //	modify_exit(E, ({"funcion", "cerrado"}));
}

int cerrado (string salida,object caminante,string msj_especial) {
    tell_object(caminante, "El paso al camino a Eldor está cerrado por una gruesas barricadas.\n");
    return 0;
}
