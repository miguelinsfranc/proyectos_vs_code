// Rezzah 14/01/2011

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_CAMINOS->describir(this_object(), "arco_volcan");	
	add_exit(SE, CAMINOS + "bastion_costa30", "path");
	add_exit(O, CAMINOS + "bastion_costa32", "path");
}

/*Una de cada tres veces, cae una bomba de lava. Si quien_entra tiene resistencia negativa a fuego, 
le hace daño (-10% de la vida que le quede). No caen varias bombas seguidas, porque no tiene sentido
que si entra 1 caiga 1 y si entran 3 caigan 3.*/
void event_enter(object quien_entra,string mensaje,object procedencia,object *seguidores){
	if(!random(3) && !query_timed_property("bomba_lanzada")){
		if(quien_entra && living(quien_entra)){
			if(quien_entra->dame_resistencia("fuego")<0){
				DAR_TOKEN(this_player(), "boreal", "golpeado_bomba_lava");
				tell_object(quien_entra,"Oyes un agudo silbido en el aire y levantas la vista para ver como "
											"una bomba de %^BOLD%^RED%^lava%^RESET%^ expulsada por el volcán N'argh cae justo a tu lado, "
											"salpicándote de lava y produciendote algunas quemaduras. \n");
				tell_room(this_object(),"Una bomba de %^BOLD%^RED%^lava%^RESET%^ expulsada por el volcán N'argh cae al lado de "+
										quien_entra->query_short()+", salpicándole de lava y produdiendole "
										"algunas quemaduras.\n",({quien_entra}));
				quien_entra->ajustar_pvs(-quien_entra->dame_pvs()/10);
			}
			else{
				tell_object(quien_entra,"Un agudo silbido en el aire te hace levantar la vista justo a tiempo "
											"para evitar una bomba de %^BOLD%^RED%^lava%^RESET%^ expulsada por el volcán N'argh\n");
				tell_room(this_object(),quien_entra->query_short()+" levanta la vista justo a tiempo para evitar "
										"una bomba de %^BOLD%^RED%^lava%^RESET%^ expulsada por el volcán N'argh\n",({quien_entra}));
			}
			add_timed_property("bomba_lanzada",1,10); //10 segundos entre bomba y bomba.
		}
	}
	
	
	
	return;
}

void init() {
	::init();
	add_action("mirar_firma", ({"mirar"}));
}

int mirar_firma(string tx) {
	if (tx == "firma" || tx == "la firma") {
		this_player()->comprobar_logro("dedicatorias_obreras", "desafios", 1, query_property("firma"));
	}

	
	
	
}

