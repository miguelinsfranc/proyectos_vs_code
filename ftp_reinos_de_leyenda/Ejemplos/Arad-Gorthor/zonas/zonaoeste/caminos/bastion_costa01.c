// Dunkelheit 04-11-2009

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_CAMINOS->describir(this_object(), "prox_bastion");	
	add_exit(O, CAMINOS + "bastion_costa00", "path");
	add_exit(NO, CAMINOS + "bastion_costa02", "path");
}
