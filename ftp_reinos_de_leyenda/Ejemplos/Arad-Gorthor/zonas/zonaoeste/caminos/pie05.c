// Dunkelheit 04-11-2009

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	set_short("Linde Sur de la Meseta Oriental");
	set_long("Te encuentras en un camino de tierra de unos cuatro metros de "
	"anchura y que se retuerce en forma de uve para salvar un pequeño terraplén "
	"de casi un metro de profundidad. A escasos metros en dirección norte se "
	"puede ver la meseta oriental del infame Erial de los "
	"Condenados. Entre el camino y el pie de la meseta hay un abrupto terreno "
	"pedregoso lleno de matas secas y malas hierbas. Hacia el sur se extiende "
	"una planicie árida en dirección a Dendra, todavía más desértica que el "
	"erial adyacente. Allende la planicie se encuentra la fortaleza de Galador, "
	"cuyos picos se asoman tímidos en el horizonte.\n");
	add_item(({"camino", "tierra", "suelo"}), "El camino de tierra está surcado "
	"numerosas huellas de carro. Algunas de ellas tan antiguas y poderosas que "
	"son más surcos que simples huellas. ¿Quizá se transportara por aquí algo "
	"más pesado que un mero carromato?\n");
	add_item(({"pie", "matas", "hierbas"}), "Vegetación propia de terrenos "
	"áridos como este. Sin duda el azote del volcán de N'argh, durante el "
	"Cataclismo de la Era 3ª, acabó con el verdor del que antaño esta zona "
	"disfrutaba.\n");
	add_item(({"sur", "fortaleza", "galador"}), "Apenas visible salvo pequeñas "
	"muescas en el horizonte. Por la noche incluso puede verse el fuego que "
	"arde en las atalayas.\n");
	add_item("planicie", "Una yerma y vasta extensión de tierra sin apenas "
	"vegetación ni accidentes geográficos. Por un momento te imaginas a la "
	"caballería dendrita avanzando a gran velocidad por el horizonte.\n");
	add_item("meseta", "Es la meseta oriental del Erial de los Condenados, "
	"una peligrosa zona infestada de lobos y alimañas.\n");
	add_item("terraplen", "Un pequeño boquete en el suelo de un metro cúbico, "
	"que parece ser el producto de una explosión que ocurrió hace mucho "
	"tiempo, a juzgar por la cantidad de matojos que pueblan el agujero.\n");
	add_item("matojos", "Hay muchos más matojos aquí que en el sector que separa "
	"el camino de la meseta. Por suerte, no hay nada de interés aquí.\n");
	
	add_exit(NO, CAMINOS + "pie04", "path");
	add_exit(NE, CAMINOS + "pie06", "path");
	
	set_exit_color("amarillo");
	fijar_luz(90);
}
