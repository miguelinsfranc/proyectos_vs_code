// Dunkelheit 12-11-2009

#include "../../../path.h";
inherit "/std/outside";

void setup() 
{
	set_short("%^BLACK%^BOLD%^Choza Abandonada%^RESET%^");
	set_long("A juzgar por la magnitud de esta choza, aquí debió habitar el líder "
	"de la tribu, comunidad, o dios-sabe-qué grupúsculo de bestias habitase en "
	"este poblado. También es la única estructura que se mantiene en pie, probablemente "
	"porque fue la que mas esmero recibió a la hora de ser construída. La choza "
	"tiene varias piltras de paja totalmente deshechas, dispuestas alrededor de "
	"una especie de mesa o altar de piedra. Tras el altar de piedra se encuenta un "
	"gran tótem de madera, que a diferencia de las máquinas del exterior no ha sido "
	"víctima de la carcoma.\n");

	add_item("suelo", "Es el mismo tipo de suelo que se presenta en el resto del "
	"cementerio: tierra y piedras.\n");
	add_item("piltras", "Son camas hechas con paja. Por su tamaño no hay lugar a "
	"dudas: aquí vivían goblins... o criaturas semejantes a ellos en tamaño.\n");
	add_item("totem", "Un tótem de madera ligeramente inclinado, con numerosas caras "
	"demoníacas talladas en él. Este debe de ser uno de los totems de los temidos "
	"totemistas hobgoblins.\n");

	fijar_luz(60);
	set_exit_color("amarillo");
	
	add_clone(OBJETOS + "altar_piedra", 1);
	
	add_exit(FU, CEMENTERIO + "abandonado_2", "gate");
}
