// Made by Dunkelheit  11/6/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

inherit "/std/outside";
#include "../../../path.h";

void setup() {

   // set_exit_color("white");
   GENERADOR_CEMENTERIO->describir(this_object(), "centro");
   // set_short("Tunnels");
   // set_long("LONG\n\n");
   set_long(query_long()+"\nUn pequeño bulto, enterrado bajo el suelo, sobresale apenas unos centímetros del suelo.\n");
	
	add_item("bulto", "Sea lo que sea, está firmemente anclado al suelo, necesitarías cavar para poder extraerlo.\n");
add_exit(SO,CEMENTERIO+"cementerio_54.c","path");
}
object * dame_objetos_enterrados() {
	object palanca;
	palanca = clone_object(OBJETOS+"palanca.c");
	if (palanca)
		return ({palanca});
	return ({});
}
