// Dunkelheit 11-11-2009

#include "../../../path.h";
inherit "/std/outside";

void setup() 
{
	set_short("Campamento Abandonado");
	set_long("En lo alto de este montículo meridional del cementerio se encuentran "
	"los estos de un pequeño campamento, visiblemente abandonado. Te encuentras en "
	"lo que debió ser la plaza del mismo, aunque la palabra \"plaza\" le queda muy "
	"grande a juzgar por su tamaño. Dos breves senderos se extienden hacia norte y "
	"sur, y en su derredor se encuentran los restos de las cuatro o cinco chozas "
	"que formaban el campamento. Desde aquí no se ve nada más de interés.\n");
	add_item("monticulo", "Es una pequeña colina, la primera que se divisa en el "
	"cementerio empezando por el sur.\n");
	add_item(({"campamento", "chozas"}), "Cuentas cinco chozas nada más, y la más "
	"grande de ellas está en la zona sur del campamento.\n");
	add_item("plaza", "De acuerdo; llamémoslo \"punto de convergencia de dos "
	"caminos sin asfaltar\".\n");
	add_item("suelo", "Es el mismo tipo de suelo que se presenta en el resto del "
	"cementerio: tierra y piedras.\n");

	fijar_luz(90);
	set_exit_color("amarillo");
	
	add_exit(O, CEMENTERIO + "cementerio_82", "path");
	add_exit(N, CEMENTERIO + "abandonado_1", "road");
	add_exit(S, CEMENTERIO + "abandonado_2", "road");
}
