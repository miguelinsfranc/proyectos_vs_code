// Dunkelheit 07-11-2009

#include "../../../path.h";
inherit "/std/outside";

void clonar_tesoro()
{
	object cofre = clone_object("/obj/baules/cofre");
	object monedero = clone_object("/obj/monedero");
	
	monedero->ajustar_dinero(15 + random(16), "platino");
	monedero->move(cofre);

	cofre->fijar_tesoro(1 + random(2) + !random(5), "arma");
	if (!random(5)) {
		cofre->fijar_tesoro(1 + random(2) + !random(5), "armadura");
	}
	
	cofre->move(this_object());
	cofre->fijar_trampa("/baseobs/trampas/esfera_acido", 1);
	cofre->setup_cerradura(4, 1, "/d/arad-gorthor/objetos/llave_inexistente");
	cofre->reset_get();
}

void setup() 
{
	set_short("Cima de la Torre de Asedio");
	set_long("El inestable suelo de esta torre de asedio cruje al menor suspiro, "
	"pues ha sido devorado por el tiempo -¡y por las termitas!-. El interior de "
	"esta máquina de asediar es totalmente hueco, exceptuando el chasis que la "
	"matiene unida y que no es más que varias vigas atravesadas de forma errática. "
	"Entre ellas hay un incómodo sistema de escalerillas que permite a las tropas "
	"alcanzar la cima de la torre. Ahora mismo estás en el nivel más bajo de esta "
	"torre, que parece haberse convertido en un improvisado cúbil: alguien ha "
	"mezclado la tierra del exterior con trozos de paja para formar una pequeña "
	"piltra. Alrededor de ella, orines y heces inundan el ambiente con un olor "
	"nauseabundo.\n");
	add_item("suelo", "Un suelo de madera que pronto queda cubierto por tierra y "
	"trozos de paja. Es tremendamente ruidoso y no permite moverse con sigilo de "
	"ninguna manera.\n");
	add_item(({"chasis", "vigas"}), "Desde aquí abajo puedes contar cinco o seis "
	"vigas de madera colocadas de forma caótica.\n");
	add_item("piltra", "Se trata de un improvisado catre de un metro cuadrado (la "
	"criatura que habita aquí no debe ser muy grande) hecho a base de tierra y "
	"paja. Está delimitado por un círculo de heces y orines.\n");
	add_item("orines", "Son pequeños restos de charquitos de orín, ya secos.\n");
	add_item("heces", "Pequeñas bolitas de color negro, como si fueran de oveja. "
	"Están secas y tienen un aspecto crujiente.\n");
	add_smell(({"ambiente", "orines", "heces"}), "¡Menudo pestazo! Sin duda la "
	"criatura que vive aquí es guarra hasta decir basta.\n");
	set_exit_color("amarillo_flojo");
	fijar_luz(90);
	
	add_clone(NPCS+"tirador_hobgoblin", 1);
	add_clone(NPCS+"totemista_hobgoblin", 1);
	
	add_exit(AB, CEMENTERIO + "torre_01b", "stairs");
	
	clonar_tesoro();
}
