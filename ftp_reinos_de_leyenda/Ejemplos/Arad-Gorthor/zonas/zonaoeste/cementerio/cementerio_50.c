// Dunkelheit 08-11-2009

#include "../../../path.h";
inherit "/std/outside";

void setup() 
{
	set_short("%^ORANGE%^Cementerio de Armas de Asedio: Sima%^RESET%^");
	set_long("El cementerio se hunde aquí hasta formar una sima, sombría y "
	"resguardada, protegida ante los ojos de las bestias salvajes que pueblan "
	"la zona. Es por ello que alguien ha dado ya buena cuenta de esta ventajosa "
	"posición, levantando un sencillo asentamiento. Una gigantesca piedra que "
	"sobresale del borde de la sima ha sido cercada con trozos de madera  "
	"obtenidos de las máquinas de asedio de alrededor para formar una pequeña "
	"guarida. Frente a este cúbil hay un enorme poste.\n");
	add_item("poste", "Hay restos de sangre por doquier. Parece ser un poste "
	"de ejecución. También hay restos de cuerda a su alrededor... ¿quizá sea "
	"usado para atar a la gente?\n");
	add_item("piedra", "Es una piedra que sobresale dos metros por la pared "
	"de la sima, con una anchura de un metro y medio. No parece haber sido "
	"manipulada de ninguna manera, tratándose así de un accidente natural.\n");
	add_item("guarida", "La única utilidad práctica que ves a este adefesio "
	"es la de resguardarse de la lluvia. Pero amigos míos, aquí nunca llueve.\n");
	
	set_exit_color("amarillo");
	fijar_luz(90);

	add_exit(O, CEMENTERIO+"cementerio_49.c", "path");
	add_exit(NO, CEMENTERIO+"cementerio_43.c", "path");
	add_exit(S, CEMENTERIO+"cementerio_58.c", "path");
	add_exit(SE, "/d/anduar/rooms/anduar/murallas/ruinmurnor_8.c", "path");
	
	add_clone(NPCS+"mercader_anduar", 1);
	add_clone(NPCS+"picaro_osgo", 3);
}
