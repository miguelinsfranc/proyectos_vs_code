// Dunkelheit 11-11-2009

#include "../../../path.h";
inherit "/std/outside";

void setup() 
{
	set_short("Campamento Abandonado");
	set_long("El sector sur del campamento debió ser el hogar de los habitantes "
	"más poderosos, a juzgar por los tamaños de las dos chozas que hay en él. "
	"De una de ellas sólo queda el discreto surco los caños que la delimitaban, "
	"al igual que sucede con las chozas del norte del campamento; la choza más "
	"grande del poblado, en cambio, está aparentemente intacta.\n");

	add_item("suelo", "Es el mismo tipo de suelo que se presenta en el resto del "
	"cementerio: tierra y piedras.\n");
	add_item("chozas", "No hay nada de interés en la choza pequeña; y desde aquí "
	"fuera no ves el interior de la choza grande.\n");
	add_item("cañas", "Son los caños de alguna planta, no tienen nada de particular.\n");

	fijar_luz(90);
	set_exit_color("amarillo");
	
	add_exit(N, CEMENTERIO + "abandonado_0", "road");
	add_exit(DE, CEMENTERIO + "choza_campamento", "gate");
}
