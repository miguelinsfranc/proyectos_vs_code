// Dunkelheit 07-11-2009

#include "../../../path.h";
inherit "/std/room";

void setup() 
{
	set_short("Torre de Asedio en Ruinas");
	set_long("El inestable suelo de esta torre de asedio cruje al menor suspiro, "
	"pues ha sido devorado por el tiempo -¡y por las termitas!-. El interior de "
	"esta máquina de asediar es totalmente hueco, exceptuando el chasis que la "
	"matiene unida y que no es más que varias vigas atravesadas de forma errática. "
	"Entre ellas hay un incómodo sistema de escalerillas que permite a las tropas "
	"alcanzar la cima de la torre. Ahora mismo estás en el nivel más bajo de esta "
	"torre, que parece haberse convertido en un improvisado cúbil: alguien ha "
	"mezclado la tierra del exterior con trozos de paja para formar una pequeña "
	"piltra. Alrededor de ella, orines y heces inundan el ambiente con un olor "
	"nauseabundo.\n");
	add_item("suelo", "Un suelo de madera que pronto queda cubierto por tierra y "
	"trozos de paja. Es tremendamente ruidoso y no permite moverse con sigilo de "
	"ninguna manera.\n");
	add_item(({"chasis", "vigas"}), "Desde aquí abajo puedes contar cinco o seis "
	"vigas de madera colocadas de forma caótica.\n");
	add_item("piltra", "Se trata de un improvisado catre de un metro cuadrado (la "
	"criatura que habita aquí no debe ser muy grande) hecho a base de tierra y "
	"paja. Está delimitado por un círculo de heces y orines.\n");
	add_item("orines", "Son pequeños restos de charquitos de orín, ya secos.\n");
	add_item("heces", "Pequeñas bolitas de color negro, como si fueran de oveja. "
	"Están secas y tienen un aspecto crujiente.\n");
	add_smell(({"ambiente", "orines", "heces"}), "¡Menudo pestazo! Sin duda la "
	"criatura que vive aquí es guarra hasta decir basta.\n");
	set_exit_color("amarillo_flojo");
	fijar_luz(65);
	
	add_clone(NPCS+"forajido_hobgoblin", 1);
	
	add_exit(AR, CEMENTERIO + "torre_01c", "stairs");
	add_exit(FU, CEMENTERIO + "cementerio_87", "corridor");
}

void event_inicio_pelea(object atacado, object atacante)
{
	object tirador;
	object victima;
	
	tell_object(find_player("dunkelheit"), "Inicio de pelea: "+atacante->query_short()+" ataca a "+atacado->query_short()+"\n");
	
	if (query_static_property("emboscada_iniciada")) {
		return;
	}
	add_static_property("emboscada_iniciada", 1);

	if (atacado->dame_subraza() != "hobgoblin") {
		victima = atacado;
	} else if (atacante->dame_subraza() != "hobgoblin") {
		victima = atacante;
	}
	
	tell_object(find_player("dunkelheit"), "Victima: "+victima->query_short());
	
	if (tirador = secure_present(NPCS + "tirador_hobgoblin", load_object(CEMENTERIO + "torre_01c"))) {
		tell_object(find_player("dunkelheit"), "Ataque culminado.\n");
		tirador->attack_ob(victima);
	}
}
