// Dunkelheit 12-11-2009

#include "../../../path.h";
inherit "/std/outside";

int chequeo_saltar(object saltador,object destino,object procedencia) {return 1;}

void setup() 
{
	object llave;
	set_short("%^GREEN%^Ciénaga Séptica: Fondo%^RESET%^");
	set_long("Te encuentras en el fondo de la Ciénaga Séptica, rodeado por completo de mierda. "
		"Puedes observar como por las paredes chorrea algo de aguas fétidas procedentes de la Ciénaga Séptica, "
		"extrañándote el echo de que exista este hueco y no se llene nunca de dichas aguas pestilentes y el suelo esté prácticamente seco. "
		"Parece ser obra de magia, una medida de protección ante visitantes no deseados a la choza que puedes ver más arriba.\n");
	add_item(({"pared","paredes"}), "Puedes ver como cae algo de agua fétida de la parte superior, desapareciendo antes de llegar al suelo. "
		"Ves una extraña capa de mierda en la pared sur.");
	add_item("suelo","Un suelo de roca normal y corriente.");
	add_item(({"mierda","capa","capa de mierda"}),"La capa de mierda parece que oculta algo, quizá debas %^CURSIVA%^remover%^RESET%^ dicha asquerosidad para ver que oculta.");	
	add_smell(({"olor","aire","ambiente"}),"De modo extraño, el olor en esta zona es casi nulo, a diferencia de en la parte superior.");
	fijar_luz(30);
	fijar_altura(-4);
	set_zone("fondo_cienaga");
	set_exit_color("amarillo_flojo");
	llave=clone_object(OBJETOS+"llave_cueva_totemistas.c");
	if(llave)
	{
		add_hidden_object(llave);
		if(llave->move(TO))
			llave->dest_me();
	}
}

/*
*	Se añade la acción adecuada para descubrir la escalera
*/
void init()
{
	::init();
	add_action("remover","remover");
}

/*
*	Acción para descubrir la escalera
*/
int remover(string str)
{
	if(!str) 
		return notify_fail("¿Qué quieres remover?\n");
	if(str!="mierda" && str!="capa" && str!="capa de mierda")
		return notify_fail("Dificilmente podrás remover eso.\n");
	if(query_property("descubierta_escalera"))
		return notify_fail("No hay ninguna "+str+" que remover ya.\n");
	tell_object(TP,"Remueves la mierda de la pared sur, provocando que se desplome sobre el suelo en el que te encuentras, "
		"dejando al descubierto una escalera escarbada en la pared.\n");
	tell_accion(TO,TP->query_short()+" remueve la mierda de la pared sur, provocando que se desplome sobre el suelo, dejando al descubierto "
		"una escalera escarbada en la pared.\n","Oyes algo desplomarse sobre el suelo en la pared sur y al mirar descubres una escalera escarbada en ella, que te había pasado inadvertida.",
		({TP}),TP);
	add_property("descubierta_escalera",1); //Mientras no haya reset de la room, se verá la escalera
	//Se reajustan los items de la room para adaptarla a la nueva circunstancia de ver la escalera
	modify_item("pared","Puedes ver como cae algo de agua fétida de la parte superior, desapareciendo antes de llegar al suelo. "
		"Ves una especie de escalones escarbados en la pared.");
	add_item(({"escalera","escalón","escalones"}),"Hendiduras a modo de escalones en la pared que llevan de vuelta a la Ciénaga Séptica.");
	remove_item("capa de mierda");
	//Se añade la nueva salida
	DAR_TOKEN(this_player(), "cementerio", "fondo_cienaga");
	add_exit(AR, CEMENTERIO + "cienaga_6", "stairs", 6);
	modify_exit(AR,({"mensaje",({
				"$N escala por lo que parecen unas escaleras escarbadas en la pared.",
				"$N llega desde el fondo de la ciénaga."
			})
		}));
	modify_exit(AR,({"funcion","pasar"}));
	return 1;
}

/*
*	Función que decide si el jugador puede subir por la escalera para salir de aquí
*/
int pasar(string salida,object caminante,string msj_especial)
{
	//Habrá un 25% de probabilidades de que resbale al intentar salir
	if(!random(4))
	{
		tell_accion(TO,caminante->query_short()+" comienza a subir por la escalera, pero resbala en uno de los escalones y se pega un buen golpetazo contra el suelo.\n",
			"Oyes un ruido seco en el suelo.\n",({caminante}),caminante);
		caminante->danyo_caida(random(abs(dame_altura())),4,caminante);
		return notify_fail("Comienzas a subir por la escalera, pero resbalas en uno de los escalones y caes de nuevo al suelo, haciéndote daño.\n");
	}
	return 1;
}

void descubre_llave()
{
	object llave;
	if((llave=secure_present(OBJETOS+"llave_cueva_totemistas",TO)) && dame_objeto_oculto(llave)) //Si está el objeto en la room y además oculto
	{
		remove_hidden_object(llave);
		tell_object(TP,"Descubres una llave de madera en una esquina de la zona.\n");
		tell_accion(TO,TP->query_short()+" descubre una llave en una esquina de la zona.\n","Oyes un ruido y al mirar en su dirección descubres una llave en el suelo.\n",({TP}),TP);
	}
}

/*
*	Función que al mirar la llave, hace que aparezca dicha llave en el suelo, para poder cogerla
*/
string long(mixed xx,int dark)
{
	if(lower_case(xx)=="suelo") //Si se mira el suelo
	{
		descubre_llave();		
	}
	return ::long(xx,dark);
}

//No hay forma de saber si la búsqueda fue con éxito, así que siempre que busque encontrará la llave.
void event_buscar(object habilidad,object buscador,string objeto_buscado,int bono)
{
	descubre_llave();
}