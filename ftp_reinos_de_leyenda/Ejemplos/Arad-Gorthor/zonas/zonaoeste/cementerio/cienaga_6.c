// Made by Dunkelheit  11/6/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

#include "../../../path.h";
inherit CEMENTERIO"base_cienaga";

int chequeo_saltar(object saltador,object destino,object procedencia) {return 1;}

void setup() {

   // set_exit_color("white");
   GENERADOR_CEMENTERIO->describir(this_object(), "cienaga");
   set_long(query_long()+"   Delante de tu posición puedes observar un precipicio.\n");
   // set_short("Tunnels");
   // set_long("LONG\n\n");
	add_exit(S,CEMENTERIO+"cienaga_10","path");
	add_exit(SO,CEMENTERIO+"cienaga_9","path");
	add_exit("precipicio",CEMENTERIO+"fondo_cienaga","precipicio",0,0); //Se podrá bajar sin tener que caerse al saltar
	modify_exit("precipicio",({"obvia",0})); //No será visible, deberá deducirlo el jugador
	add_exit("choza", ({CEMENTERIO + "choza_cienaga",CEMENTERIO + "fondo_cienaga"}), "precipicio", 0, 5);
	if (!random(3)) add_clone(BMONSTRUOS+"muertos_vivientes/copromante", 1);
	/*
	*	Desde esta room se accederá a la choza de la bruja, así que se reconfigura el item de estructura y se añaden los nuevos items necesarios
	*/
	add_item("estructura", "Puedes ver justo delante una choza construida sobre las aguas fecales, que pasan por debajo de "
		"ésta. Puedes ver un precipicio que separa tu posición actual de dicha choza.");
	add_item("precipicio","Estás ante un precipicio no demasiado profundo, pudiendo saltar a él, que te imposibilita el acceder a la choza que tienes delante de "
		"tus narices. Quizá puedes llegar a ella de un salto.");
}