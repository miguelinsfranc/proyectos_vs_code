// Made by Dunkelheit  11/6/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

/*
*	Zoilder - 20/03/2011: Preparación para misión de hobgoblins
*/

#include "../../../path.h";
inherit CEMENTERIO"base_vertedero";

#define ROOM_TOTEMISTAS TOTEMISTAS+"totemistas_23"

void taponar_caverna();

void setup() {

   // set_exit_color("white");
   GENERADOR_CEMENTERIO->describir(this_object(), "vertedero");
   // set_short("Tunnels");
   // set_long("LONG\n\n");
	add_exit(O,CEMENTERIO+"vertedero_19.c","path");
	add_exit(SO,CEMENTERIO+"vertedero_25.c","path");
}

/*
*	Función que indica si está preparándose para mostrar la puerta
*/
int desplegando_puerta()
{
	return find_call_out("funcion_aparece_entrada")!=-1;
}

/*
* Se habilita la acción de remover para localizar la entrada a la cueva de los totemistas
*/
void init()
{
	::init();
	add_action("remover","remover");
	add_action("saludar","saludar");
}

int remover(string str)
{
	object totem;
	if(!str) return notify_fail("¿Qué quieres remover?\n");
	if(member_array(str,({"mierda","basura","mierdas","basuras","desperdicio","desperdicios","colina","colinas"}))==-1)
		return notify_fail("Dificilmente podrás remover eso.\n");
	if(sizeof(find_match("totem",TO))) return notify_fail("Ya han sido removido los desperdicios, mejor dejarlos así o puede suceder una catástrofe.\n");
	tell_object(TP,"Remueves la montaña de desperdicios, provocando un corrimiento de materiales fétidos, dejando al descubierto "
		"un extraño tótem.\n");
	tell_accion(TO,TP->query_short()+" remueve la montaña de desperdicios, provocando un corrimiento de materiales fétidos, dejando al "
		"descubierto un extraño tótem.\n","Oyes un estrépito provocado por un corrimiento de materiales fétidos de la montaña de desperdicios, que deja un extraño tótem al descubierto.\n",
		({TP}),TP);
	/*
	* Descubrimos el totem
	*/
	add_property("descubierto_totem",1);
	totem= clone_object("/std/object.c");
	totem->set_name("totem");
	totem->set_short("Tótem de madera");
	totem->set_main_plural("Tótems de Madera");
	totem->set_long("Un tótem de madera ennegrecida y astillada. Puedes ver numerosas caras grotescas talladas a lo largo "
		"de su superficie. El tótem ha sido tallado de forma que parece estar saludándote e incitándote a saludarle.\n");
	if(totem->move(TO))
		totem->dest_me();
	else
		totem->reset_get();
	return 1;
}

int saludar(string str)
{
	if(!str || member_array(lower_case(str),({"totem","tótem","a totem","a tótem","al totem","al tótem"}))==-1 || !query_property("descubierto_totem")) return 0;
	if(!sizeof(find_match("totem",TO))) return 0;
	tell_object(TP,"Saludas al tótem alegremente.\n");
	tell_accion(TO,TP->query_short()+" saluda al tótem alegremente.","",({TP}),TP);
	if(!desplegando_puerta() && member_array("caverna",query_dest_dir())==-1) //Si no está apareciendo (ni ya apareció) la entrada
		call_out("funcion_aparece_entrada",2,1);
	else if(!desplegando_puerta())//Si ya apareció, se dice alguna briboná al jugador
	{
		call_out((:tell_object:),2,TP,"Te quedas como un tonto saludando al tótem esperando que suceda algo...\n");
		call_out((:tell_object:),4,TP,"El tiempo pasa y no sucede nada, así que decides dejar de hacer el soplagaitas.\n");
	}
	return 1;
}

void funcion_aparece_entrada(int paso)
{
	switch(paso)
	{
		case 1:	tell_room(TO,"El tótem se ilumina tenuemente.\n");break;
		case 2: 
			tell_room(TO,"La montaña de deshechos comienza a temblar.\n");
			tell_room(load_object(ROOM_TOTEMISTAS),"El suelo comienza a temblar.\n");
			break;
		case 3: 
			tell_room(TO,"El temblor se extiende a toda la zona, haciendo que te tambalees.\n");
			tell_room(load_object(ROOM_TOTEMISTAS),"El temblor se hace más potente, haciéndote tambalear.\n");
			break;
		case 4: 
			tell_room(TO,"Una fina línea azulada surge del suelo y comienza a ascender por la colina, trazando el contorno de una especie de puerta.\n"); 
			tell_room(load_object(ROOM_TOTEMISTAS),"Una luz azulada comienza a surgir de la puerta.\n");
			break;
		case 5: 
			tell_room(TO,"Finalmente aparece en la colina de desperdicios, una puerta de madera.\n");
			tell_room(load_object(ROOM_TOTEMISTAS),"El temblor del terreno cesa.\n");
			//Se añade la nueva salida
			add_exit("caverna", TOTEMISTAS + "totemistas_23", "door",1,10);
			setup_door("caverna",1,1,50,OBJETOS+"llave_cueva_totemistas",0,1000);
			set_door_long("caverna","Es una puerta de madera enclaustrada en la colina de deshechos. Puedes ver un par de tótems entrecruzados tallados en ella.");
			DAR_TOKEN(this_player(), "cementerio", "vertedero_20");
			return;
	}
	call_out("funcion_aparece_entrada",paso*2,++paso);
}

/*
*	Función que controla cuando varía la vida de la puerta, para haber una oportunidad de provocar el derrumbe.
*	La probabilidad de derrumbe dependerá de la vida quitada
*/
int adjust_door_health(string direccion,int vida,object causante,int flag)
{
	int probabilidad_derrumbe,se_tapona_entrada;
	switch(vida)
	{
		case -250..0:
			tell_room(TO,"La puerta vibra levemente.\n",0);
			probabilidad_derrumbe=12;
			break;
		case -750..-500:
			tell_room(TO,"La montaña de desperdicios vibra junto a la puerta.\n",0);
			probabilidad_derrumbe=4;
			break;
		case -750..-1000:
			tell_room(TO,"El fuerte golpe provoca un fuerte temblor de la montaña de desperdicios.\n",0);
			probabilidad_derrumbe=1;
			break;
		default:
			tell_room(TO,"La puerta se revigoriza.\n",0);
	}
	if(!random(probabilidad_derrumbe))
	{
		se_tapona_entrada=random(2);
		tell_room(TO,"La montaña de desperdicios se tambalea junto a la puerta de tal modo que se produce un corrimiento de "
			"deshechos que destruye la puerta"+(se_tapona_entrada?" y tapona la entrada":"")+".\n",0);
		if(se_tapona_entrada) //Si se tapona la entrada, se elimina la salida
			taponar_caverna();
		else
			romper_puerta(direccion,causante,0);
		return 0;
	}
	return ::adjust_door_health(direccion,vida,causante,flag);
}

void taponar_caverna()
{
	object deshechos, *totem;
	remove_exit("caverna");
	deshechos= clone_object("/std/object.c");
	deshechos->set_name("deshechos");
	deshechos->set_short("Acumulación de deshechos");
	deshechos->set_main_plural("Acumulaciones de deshechos");
	deshechos->set_long("Una acumulación de deshechos, provenientes al parecer, de un corrimiento de la montaña de desperdicios "
		"que tienes delante de tus narices. De su interior, sobresalen algo parecido a un tótem y a una puerta.\n");
	deshechos->add_alias(({"deshecho","deshechos","acumulacion","acumulación","acumulación de deshechos","acumulacion de deshechos"}));
	if(deshechos->move(TO))
		deshechos->dest_me();
	else
		deshechos->reset_get();
	//Nos cargamos el tótem
	if(sizeof(totem=find_match("totem",TO)))
		totem[0]->dest_me();
	add_item(({"totem","tótem"}),"Ves un tótem semidestruido sobresalir del interior de la acumulación de deshechos.\n");
	add_item("puerta","Ves un trozo de una puerta de madera astillada sobresalir del interior de la acumulación de deshechos.\n");
	add_property("caverna_taponada",1);
}