// Made by Dunkelheit  11/6/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

inherit "/std/outside";
#include "../../../path.h";

void setup() {

   // set_exit_color("white");
   GENERADOR_CEMENTERIO->describir(this_object(), "norte");
   // set_short("Tunnels");
   // set_long("LONG\n\n");
add_exit(SO,CEMENTERIO+"cementerio_12.c","path");
add_exit(NO, CAMINOS+"bastion_costa00.c", "path");//Camino a las ruinas de Dircin'Gah
add_exit(E, BASTION+"camino_0.c", "path");//Camino del cementerio a Arad-Gorthor
add_sign("Un obelisco de piedra negra laboriosamente tallado con una intrincada filigrana en bajorrelieve representando una batalla. En la sección superior unas puertas de las que brotan cientos de orcos, gnolls, goblins, kobols y otras criaturas que se dispersan a izquierda y derecha en diagonal dibujando un óvalo que se cierra en la parte inferior sobre una entretenida escena de elfos masacrados. Tiene multitud de golpes de espada, de maza, arañazos y aún así reluce con belleza propia. El centro del óvalo hay unas direcciones marcadas.\n",
	"Este: Bastión de Arad-Gorthor\n"
	"Noroeste: Ruta Boreal. Camino al Puerto de Golthur\n"
	"Suroeste: Reino de Ancarak\n","obelisco","Obelisco",	"negra");


}
