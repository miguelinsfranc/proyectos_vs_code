// Made by Dunkelheit  11/6/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

#include "../../../path.h";
inherit CEMENTERIO"base_vertedero";

void setup() {

   // set_exit_color("white");
   GENERADOR_CEMENTERIO->describir(this_object(), "vertedero");
   // set_short("Tunnels");
   // set_long("LONG\n\n");
add_exit(NO,CEMENTERIO+"vertedero_13.c","path");
add_exit(E,CEMENTERIO+"vertedero_19.c","path");
}

int remover(string str)
{
	object nota;
	if(!str) return notify_fail("¿Qué quieres remover?\n");
	if(member_array(str,({"mierda","basura","mierdas","basuras","desperdicio","desperdicios","colina","colinas"}))==-1)
		return notify_fail("Dificilmente podrás remover eso.\n");
	if(sizeof(find_match("totem",TO))) return notify_fail("Ya han sido removido los desperdicios, mejor dejarlos así o puede suceder una catástrofe.\n");
	tell_object(TP,"Remueves la montaña de desperdicios, descubriendo una mugrienta nota de papel.\n");
	tell_accion(TO,TP->query_short()+" remueve la montaña de desperdicios, descubriendo una mugrienta nota de papel.\n",
		"Oyes como alguien remueve la montaña de desperdicios.\n",
		({TP}),TP);
	//Ponemos una nota en el suelo para ayudar algo a conseguir la llave de apertura de la caverna
	nota= clone_object("/std/object.c");
	nota->set_name("nota");
	nota->set_short("Nota de Papel");
	nota->set_main_plural("Notas de Papel");
	nota->set_long("Una nota de papel.\n");
	nota->fijar_genero(2);
	nota->fijar_mensaje_leer("Día 67: No sé por qué siempre me toca a mí ir a \"saludar\" a la vieja bruja loca esa. ¿No ven que mis artes saltadoras "
		"no son gran cosa y que acabo siempre en el precipicio? Luego pierdo algo y me como la bronca. ¡Debo poner fin a esto!", "negra", 0);	
	DAR_TOKEN(this_player(), "cementerio", "vertedero_18");
	if(!TP->entregar_objeto(nota))
	{
		nota->dest_me();
		return notify_fail("Ha ocurrido un error al intentar coger la nota del Vertedero del Cementerio de Arad-Gorthor. Notifica con el comando %^CURSIVA%^error%^RESET%^.\n");	
	}
	return 1;
}