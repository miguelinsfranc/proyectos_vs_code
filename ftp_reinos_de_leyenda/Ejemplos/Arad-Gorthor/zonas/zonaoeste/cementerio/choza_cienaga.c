// Dunkelheit 12-11-2009

#include "../../../path.h";
inherit "/std/outside";
int chequeo_saltar(object saltador,object destino,object procedencia) {return 1;}

void setup() 
{
	set_short("%^ORANGE%^Choza apestosa%^RESET%^");
	set_long("Te encuentras en una choza medio en ruinas que se encuentra en pie "
		"en mitad de la Ciénaga Séptica en la que te encuentras. "
		"Lo único que cabe destacar en ella, en primer lugar, es el fuerte olor a mierda "
		"procedente tanto del exterior como del interior, donde ves varios cachibaches "
		"acumulados en los estantes de las paredes, echando una pestilencia incluso superior "
		"a la que te puedes encontrar en el exterior de la choza. "
		"Entre el poco mobiliario existente, destacan, además de los estantes de las paredes, "
		"una piltra con un orinal a sus pies, lo que demuestra que aquí es capaz "
		"de vivir alguien; una especie de mesa cochambrosa, con unos recipientes de los cuales sale un humo "
		"negruzco y un libro abierto; y una marmita burbujeante. El suelo de madera, está prácticamente deshecho en algunas zonas.\n");
	add_item("suelo", "Un suelo de madera prácticamente deshecho, con varios huecos a lo largo de su superficie, dejando ver la mierda "
		"que lleva la Cienaga Séptica sobre la que se ha construido esta choza.");
	add_item(({"hueco","huecos"}),"Los huecos parecen haber sido realizados a breve, a modo de vertedero a la Ciénaga Séptica.");
	add_item("piltra", "Es una cama hecha de paja, del tamaño de un humano.");
	add_item("orinal","Es un simple orinal, sin nada a destacar en él.");
	add_item(({"pared","paredes"}),"Están cubiertas de unos estantes en las que se pueden observar una serie de cachivaches.");
	add_item(({"estante","estantes"}),"Recubren las paredes de la choza y puedes observar cómo están llenos de cachivaches.");
	add_item(({"cachivache","cachivaches"}),"Cachivaches típicos de cualquier practicante de brujería. Están totalmente "
		"sucios y llenos de mierda, de haberlos \"lavado\" en las aguas fecales de la zona.");
	add_item("mesa","Una mesa de madera ennegrecida por el paso del tiempo, sobre la que se encuentran algunos cachivaches "
		"similares a los de los estantes de las paredes, junto a un libro abierto.");
	//add_item("libro","Un libro lleno de caracteres ininteligibles y en total desorden. Da la impresión de haber sido escrito por "
	//	"alguien que no estaba muy en sus cabales.");//
	add_item(({"TOKEN","cementerio","libro"}),"Un libro lleno de caracteres ininteligibles y en total desorden. Da la impresión de haber sido escrito por "
		"alguien que no estaba muy en sus cabales.");
	add_item("marmita","Una enorme marmita en cuyo interior parecen cocerse algunos componentes para algún hechizo o derivado, destacando "
		"entre ellos lo que parecen ser ojos de algun ser extraño.");
	add_smell(({"olor","aire","ambiente"}),"La choza está dominada por una mezcla del olor procedente del exterior, de la Ciénaga "
		"Séptica, con el olor propio de la choza, una mezcla de meados con excrementos algo desagradable.");
	fijar_luz(60);
	set_exit_color("amarillo_flojo");
	add_clone(NPCS + "bruja", 1);
	add_exit(FU, ({CEMENTERIO + "cienaga_6",CEMENTERIO + "fondo_cienaga"}), "precipicio", 0, 3);
	set_zone("choza_cienaga");
}
