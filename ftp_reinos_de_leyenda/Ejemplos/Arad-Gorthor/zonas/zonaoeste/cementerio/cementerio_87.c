#include "../../../path.h";
inherit "/std/outside";

void setup() 
{
	GENERADOR_CEMENTERIO->describir(this_object(), "monticulo");
	add_exit(O, CEMENTERIO + "cementerio_86.c", "path");
	add_clone(OBJETOS+"torre", 1);
}

void init()
{
	::init();
	add_action("entrar", ({"entrar", "introducirse", "pasar", "acceder"}));
}

int entrar(string str)
{
	if (!str || !regexp(str, "torre")) {
		return notify_fail("¿"+capitalize(query_verb())+" dónde?\n", this_player());
	}
	
	if (this_player()->move_player("torre", CEMENTERIO+"torre_00c", ({ "$N se introduce en la torre de asedio.\n", "$N llega del exterior.\n" }), 0, "fuera")) {
		tell_object(this_player(), "Apartas varios maderos vencidos y entras ruidosamente al interior de la torre de asedio.\n");
	} else {
		// Fallo
	}
	return 1;
}
