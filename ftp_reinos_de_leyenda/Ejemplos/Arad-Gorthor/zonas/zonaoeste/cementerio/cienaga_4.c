// Made by Dunkelheit  11/6/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

#include "../../../path.h";
inherit CEMENTERIO+"base_cienaga";

void setup() {

   // set_exit_color("white");
   GENERADOR_CEMENTERIO->describir(this_object(), "cienaga");
   // set_short("Tunnels");
   // set_long("LONG\n\n");
add_exit(O,CEMENTERIO+"cienaga_3.c","path");
add_exit(S,CEMENTERIO+"cienaga_7.c","path");
}
