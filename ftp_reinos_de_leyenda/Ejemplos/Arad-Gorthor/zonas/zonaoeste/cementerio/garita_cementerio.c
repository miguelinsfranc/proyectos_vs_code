#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_CEMENTERIO->describir(this_object(), "vertedero");
	set_short(query_short()+"%^GREEN%^:%^RESET%^ Garita de Acceso");
	set_long(query_long()[0..<2] + " Su acceso está custodiado por este ridículo "
	"puesto: una garita en la que un orco seboso se proteje del Sol.");
	add_item("garita", "Una garita unipersonal con un pequeño taburete.\n");
	add_item("taburete", "Un simple taburete de tres patas.\n");
	add_item("patas", "Al detenerte a observar las patas, descubres una frase "
	"escrita debajo del taburete.\n");
	add_item("frase", "¡Ugh! ¡no era una frase, es un moco pegado al taburete!\n");
	
	add_exit(NO, CEMENTERIO+"vertedero_30.c", "path");
	add_exit(S, CEMENTERIO+"cementerio_40.c", "path");
	
	add_clone(NPCS + "glotrub", 1);
}

int chequeo_salida(string tx, string verbo, object ob_caminante, object ob_que_sigo)
{
	object gordo = secure_present(NPCS + "glotrub", this_object());
	if (gordo && verbo == "noroeste") {
		if (HORDA_NEGRA->es_lider(ob_caminante)) {
			gordo->do_say("¿Qué se le ha perdido a su malignidad en este vertedero? ¡Bueno, prefiero no saberlo!", 0);
			return 1;
		}
		return notify_fail("La inmensa y sudorosa barriga de Glotrub es un serio obstáculo a vencer; y no parece dispuesto a apartarla para dejarte pasar.\n", ob_caminante);
	}
	
	return 1;
}
