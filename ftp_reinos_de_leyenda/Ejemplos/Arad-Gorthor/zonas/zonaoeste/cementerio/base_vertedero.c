inherit "/std/outside";
/*
*	Se permitirá remover la mierda en todas las rooms del vertedero, pero sólo en la room central es donde aparecerá el tótem
*/
void init()
{
	::init();
	add_action("remover","remover");
}

int remover(string str)
{
	if(!str) return notify_fail("¿Qué quieres remover?\n");
	if(lower_case(str)!="mierda" && lower_case(str)!="basura" && lower_case(str)!="mierdas" && lower_case(str)!="basuras")
		return notify_fail("Dificilmente podrás remover eso.\n");
	tell_object(TP,"Remueves la montaña de desperdicios sin encontrar nada en especial.\n");
	tell_accion(TO,TP->query_short()+" remueve la montaña de desperdicios.\n","Oyes como alguien remueve la montaña de desperdicios.\n",
		({TP}),TP);
	return 1;
}