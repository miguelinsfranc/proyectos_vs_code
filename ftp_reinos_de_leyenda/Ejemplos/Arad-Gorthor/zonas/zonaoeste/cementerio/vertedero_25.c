// Made by Dunkelheit  11/6/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

#include "../../../path.h";
inherit CEMENTERIO"base_vertedero";

void setup() {

   // set_exit_color("white");
   GENERADOR_CEMENTERIO->describir(this_object(), "vertedero");
   // set_short("Tunnels");
   // set_long("LONG\n\n");
add_exit(NE,CEMENTERIO+"vertedero_20.c","path");
add_exit(S,CEMENTERIO+"vertedero_30.c","path");
}
