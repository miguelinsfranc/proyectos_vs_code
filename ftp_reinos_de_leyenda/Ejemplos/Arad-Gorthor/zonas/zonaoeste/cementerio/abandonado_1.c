// Dunkelheit 11-11-2009

#include "../../../path.h";
inherit "/std/outside";

void setup() 
{
	set_short("Campamento Abandonado");
	set_long("El camino que conduce al sector norte del campamento terminó mucho "
	"antes de lo que te esperabas. Hay tres chozas, una frente al final del camino "
	"y las otras dos a ambos lados del mismo. De ellas sólo quedan varias cañas "
	"formando una circunferencia.\n");

	add_item("suelo", "Es el mismo tipo de suelo que se presenta en el resto del "
	"cementerio: tierra y piedras.\n");
	add_item("chozas", "Echas un vistazo pero no encuentras nada de interés.\n");
	add_item("cañas", "Son los caños de alguna planta, no tienen nada de particular.\n");

	fijar_luz(90);
	set_exit_color("amarillo");
	
	add_exit(S, CEMENTERIO + "abandonado_0", "road");
}
