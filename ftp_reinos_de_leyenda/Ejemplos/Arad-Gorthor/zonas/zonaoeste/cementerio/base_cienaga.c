inherit "/std/outside";

status query_lodazal() { return 1; }
status dame_lodazal() { return 1; }

int chequeo_salida(string tx, string verbo, object ob_caminante, object ob_que_sigo)
{
	int ran, destreza;

	if (ob_caminante->query_property("libre-accion")) {
		return 1;
	}

	ran = random(8);
	
	//Siguiendo a alguien es más facil pasar ya que va abriéndote camino
	if (ob_que_sigo && environment(ob_que_sigo)!=this_object()) {
		ran--;
	}
	
	if (17 <= destreza = ob_caminante->dame_carac("des")) {
		if (destreza>=20) {
			ran-= 2;
		} else if(ran>=1) {
			ran--;
		}
	}
	
	if (ran > 7) {
			tell_accion(this_object(),ob_caminante->query_short()+" se hunde en la mierda hasta la barbilla, tragando parte del manjar marrón. No puedes evitar reirte.\n",
				"Oyes un ruído espantoso de lodo moviéndose y un gorgoteo cochambroso.\n",({ob_caminante}),ob_caminante);
			tell_object(ob_que_sigo,ob_caminante->query_short()+" se queda atrapad"+ob_caminante->query_vocal()+" en las heces y no logra seguirte.\n");
			notify_fail("Intentas avanzar pero te hundes miserablemente en las pútridas heces, tragando inevitablemente parte del manjar marrón. No se puede caer más bajo en Eirea.\n",ob_caminante);
	}

	tell_accion(this_object(),ob_caminante->query_short()+" avanza penosamente entre las heces que entorpecen su camino.\n",
		"Oyes ruído de lodo moviéndose a tu alrededor.\n",({ob_caminante}),ob_caminante);
	tell_object(ob_caminante, "Avanzas chapoteando entre las heces de la ciénaga.\n");
	return 1;
}
