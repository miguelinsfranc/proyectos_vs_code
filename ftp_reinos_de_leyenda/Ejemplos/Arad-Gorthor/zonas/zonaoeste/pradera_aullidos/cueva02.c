// Zoilder 13-02-2011

#include "../../../path.h";
inherit PRADERA_AULLIDOS+"base_cueva";

void setup()
{
	GENERADOR_PRADERA_AULLIDOS->describir(this_object(), "cueva",86400);//Sólo reseteará una vez al día, lo que hará junto al random de los cachorros que no se pueda abusar
	add_exit("pradera", PRADERA_AULLIDOS + "pradera10.c", "escalable");
}