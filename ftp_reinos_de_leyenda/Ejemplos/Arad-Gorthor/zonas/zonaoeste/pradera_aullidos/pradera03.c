// Zoilder 13-02-2011

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_PRADERA_AULLIDOS->describir(this_object(), "inicio_camino");	
	add_exit(NE, PRADERA_AULLIDOS + "pradera02.c", "path");
	add_exit(SO, PRADERA_AULLIDOS + "pradera04.c", "path");
}