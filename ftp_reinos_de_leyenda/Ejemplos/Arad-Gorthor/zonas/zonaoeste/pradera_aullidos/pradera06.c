// Zoilder 13-02-2011

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_PRADERA_AULLIDOS->describir(this_object(), "zona_profunda");
	add_exit(E, PRADERA_AULLIDOS + "pradera05.c", "path");
	add_exit(NO, PRADERA_AULLIDOS + "pradera07.c", "path");
	fijar_frecuencia_sensaciones(66); //Tendrá una frecuencia algo más elevada para que no sea tan complicado el punto de exploración
	fijar_sensaciones(({"Ves un leve brillo procedente de unos arbustos"})); //Esta room sólo tendrá esta sensación
	add_item(({"arbusto","arbustos"}), "Ves algún que otro arbusto de tamaño medio esparcido a ambos lados del camino. "
		"Percibes un tenue brillo procedente de uno de ellos. Si te fijas bien, ves que hay "
		"algo en su interior. Quizá debas apartarlos para ver mejor qué hay.");
}

//Se añade la acción necesaria
void init()
{
	::init();
	add_action("apartar_arbustos","apartar");
}

/*
*	Función que controla la acción de apartar arbustos y todas sus condiciones.
*	Se encarga de dar el Punto de exploración al jugador
*/
int apartar_arbustos(string str)
{
	string quien_aparto;
	//Inicio Condiciones de Ejecución
	if(str==0) return notify_fail("¿Qué quieres apartar?\n"); //Si no se indica nada
	str=lower_case(str); //Mejor trabajar con el texto en minúscula
	if(str!="arbusto" && str!="arbustos" && str!="el arbusto" && str!="los arbustos") return notify_fail("No puedes apartar eso.\n"); //Debe apartarse el arbusto
	if(quien_aparto = query_property("arbusto_apartado")) //Se comprueba si ya se apartaron los arbustos
	{
		if(quien_aparto==TP->query_name()) //Si ya los apartó el propio jugador
			return notify_fail("Ya apartaste los arbustos y obtuviste lo que había en su interior.\n");
		else
			return notify_fail("Los arbustos ya han sido apartados. No hay nada en su interior.\n"); //Si ya están apartados por otro jugador
	}
	//Fin Condiciones de Ejecución
	DAR_TOKEN(TP, "cementerio", "arbusto_garfio");
	tell_object(TP,"Apartas los arbustos y rebuscas por su interior, descubriendo que el causante del brillo que se veía era un garfio de escalada, el cual te agencias.\n");
	tell_accion(TO,TP->query_short()+" aparta unos arbustos y tras rebuscar en su interior, encuentra un garfio de escalada, el cual se agencia.\n",
		"De repente unos arbustos son removidos.\n",({TP}),TP);
	if(!TP->entregar_objeto(BASEOBS+"misc/garfio_escalada.c"))
		return notify_fail("Ha ocurrido un error al intentar entregarte el garfio obtenido de los arbustos. Notifica con el comando %^CURSIVA%^error%^RESET%^.\n");	
	add_property("arbusto_apartado",TP->query_name());
	fijar_frecuencia_sensaciones(0); //No se mostrará ninguna sensación al no haber ya nada en el arbusto.
		modify_item("arbusto","Ves unos arbustos que parecen haber sido removidos. Miras en su interior y no ves nada.\n");
	return 1;
}