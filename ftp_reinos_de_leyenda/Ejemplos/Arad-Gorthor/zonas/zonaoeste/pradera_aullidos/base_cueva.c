inherit "/std/underground"; //Se hereda de este tipo de sala

/*
*Se habilita en las rooms de la cueva el poder remover los huesos
*/
void init()
{
	::init();
	add_action("remover_huesos","remover");
}

/*
*	Funcionalidad básica del comando para todas las rooms de cueva
*	Es la que se ejecutará en las rooms que tengan definida su propia función para el comando
*/
varargs int remover_huesos(string str, int consigue_objeto)
{
	string quien_removio;
	if(str==0) return notify_fail("¿Remover qué?\n");
	str=lower_case(str); //Mejor trabajar con el texto en minúsculas
	if(str!="hueso" && str!="huesos" && str!="el hueso" && str!="los huesos" && str!="pila" && str!="la pila")
		return notify_fail("No puedes remover eso.\n");
	if(quien_removio = query_property("removidos_huesos")) //Se comprueba si ya se apartaron los arbustos
	{
		if(quien_removio==TP->query_name()) //Si ya removió el propio jugador
			return notify_fail("Ya removiste la pila de huesos, desparramándolos por toda la cueva"+(consigue_objeto?"; y cogiste la cabeza que había en su interior":"")+".\n");
		else //Si ya removió otro jugador
			return notify_fail("Alguien removió ya los huesos, desparramándolos por toda la cueva"+(consigue_objeto?"; y cogió ya la cabeza que había en su interior":"")+".\n");
	}
	tell_object(TP,"Te acercas a la pila de huesos e insertas la mano en ella, desparramando los huesos por toda la cueva"+(consigue_objeto?" y descubriendo que lo que destacaba es una cabeza de algún ser":"")+".\n");
	tell_accion(TO,TP->query_short()+" inserta la mano en la pila de huesos, desparramando todos los huesos"+(consigue_objeto?" y obteniendo una cabeza de algún ser":"")+".\n",
		"Ves como la pila de huesos de la cueva se desmorona de repente.\n",({TP}),TP);
	add_property("removidos_huesos",TP->query_name());
	remove_item("hueso");
	add_item(({"hueso","huesos","restos"}),"Observas varios huesos esparcidos por la cueva."); //Aquí no interesa poder mirar ya la pila
	return 1;
}

/*
*	Función para controlar el cachorro, que se dormirá o despertará según sea día o noche
*/
void event_clima(object handler_climatico, int noche)
{
	object *cachorros=find_match("cachorro",({TO})+all_inventory(TO),1,-1); //Sólo se tomarán cachorros que estén en la room, si están en el inventario de un jugador, no se hará nada.
	if(!sizeof(cachorros)) return; //Si no hay cachorros
	foreach(object cachorro in cachorros)
	{
		if(cachorro && cachorro->dame_amo() == "salvaje") //Si hay un cachorro en la room y es salvaje
		{
			if(!noche)
			{
				cachorro->dormir_cachorro();
				return;
			}
			cachorro->despertar_cachorro();
		}
	}
}

/*
*	Función para permitir al huargo cachorro huir si es atacado (Así sólo huirá si se le ataca en la cueva y no cuando esté fuera de ella, pues este pnj se podrá usar en más sitios)
*/
void huir(object cachorro,object atacante)
{
	tell_object(atacante,"Atacas al cachorro de huargo, que ante el peligro se escabuye por un hueco de la cueva y desaparece completamente.\n");
	tell_accion(TO,"Al ser atacado por "+atacante->query_short()+", el cachorro de huargo se escabuye por un hueco de la cueva y desaparece completamente.\n",
		"Alguien ataca al cachorro de huargo, que ante el peligro se escabuye por un hueco de la cueva y desaparece completamente.\n",({atacante}),atacante);
	cachorro->dest_me();
}