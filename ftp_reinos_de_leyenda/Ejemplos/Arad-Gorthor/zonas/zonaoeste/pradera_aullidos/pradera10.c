// Zoilder 13-02-2011

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_PRADERA_AULLIDOS->describir(this_object(), "pradera");	
	add_exit(NE, PRADERA_AULLIDOS + "pradera09.c", "path");
	add_exit(SO, PRADERA_AULLIDOS + "pradera11.c", "path");
	add_exit("cueva", PRADERA_AULLIDOS + "cueva02.c", "escalable");
	add_item(({"hueso","huesos","restos"}), //Aquí el item de huesos tendrá más información
		"Observas varios huesos diseminados por toda la pradera. No tienes forma de saber a qué tipo de ser pertenecen. "
		"Te das cuenta que detrás de un arbusto de gran tamaño se encuentra un esqueleto, al parecer de un halfling o gnomo.");
	add_item(({"esqueleto"}),
		"Revisas el cuerpo del esqueleto y ves que lo único que queda a salvo es una cuerda enrollada en su cintura, o lo que era su cintura cuando había algo de carne sobre los huesos.");
	add_item(({"cuerda"}),
		"Se trata de una cuerda normal y corriente. No tiene nada en especial, pero quizá te pueda ser útil si la coges y te la quedas.");
}

void init()
{
	add_action("coger_cuerda","coger");
}

int coger_cuerda(string str)
{
	string quien_consiguio;
	if(str==0 || (lower_case(str)!="cuerda" && lower_case(str)!="la cuerda")) return 0;
	if(quien_consiguio = query_property("conseguida_cuerda")) //Se comprueba si ya se apartaron los arbustos
	{
		if(quien_consiguio==TP->query_name()) //Si ya cogió la cuerda el propio jugador
			tell_object(TP,"Ya cogiste la cuerda del esqueleto, provocando que éste se desmoronara.\n");
		else //Si ya cogió la cuerda otro jugador
			tell_object(TP,"Alguien cogió ya la cuerda del esqueleto y provocó que éste se desmoronara.\n");
		return 1;
	}
	DAR_TOKEN(TP, "cementerio", "coger_cuerda");
	tell_object(TP,"Te acercas al esqueleto y desenrollas con cuidado la cuerda de su cintura, pero éste cede y se desmorona, diseminándose "
		"todos los huesos por la zona.\n");
	tell_accion(TO,TP->query_short()+" se acerca al esqueleto y desenrolla la cuerda de su cintura, provocando que ceda y se desmorone, diseminándose "
		"todos los huesos por la zona.\n",
		"De repente se desmoronan los huesos de un esqueleto que había por la zona.\n",({TP}),TP);
	if(!TP->entregar_objeto(BASEOBS + "misc/cuerda.c"))
		return notify_fail("Ha ocurrido un error al intentar coger la cuerda del esqueleto. Notifica con el comando %^CURSIVA%^error%^RESET%^.\n");	
	add_property("conseguida_cuerda",TP->query_name());
	modify_item("hueso", "Observas varios huesos diseminados por toda la pradera. No tienes forma de saber a qué tipo de ser pertenecen.");
	modify_item("esqueleto", "Ves varias partes de un esqueleto de un ser menudo, diseminadas alrededor de un arbusto.");
	remove_item("cuerda");
	return 1;
}

/*
*	Función que configura el tipo de terreno si se cae al escalar
*/
int dame_factor_superficial()
{ 
	return 1;  //Se pone el terreno blando al caer, pues hay maleza, etc. que amortiguarían algo la caida
}