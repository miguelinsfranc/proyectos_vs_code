// Zoilder 13-02-2011

#include "../../../path.h";
inherit PRADERA_AULLIDOS+"base_cueva";

void setup()
{
	GENERADOR_PRADERA_AULLIDOS->describir(this_object(), "cueva",86400);//Sólo reseteará una vez al día, lo que hará junto al random de los cachorros que no se pueda abusar
	add_exit("pradera", PRADERA_AULLIDOS + "pradera11.c", "escalable");
}

int remover_huesos(string str)
{
	object cabeza;
	if(::remover_huesos(str,1)) //Si consigue remover los huesos, obtiene el objeto
	{
		cabeza=clone_object(BASEOBS + "cuerpos/parte_cabeza.c");
		cabeza->fijar_religion("eralie");
		if(!TP->entregar_objeto(cabeza)) //Entrega la cabeza, o avisa si hay algún error
	 		return notify_fail("Ha ocurrido un error al intentar coger la cabeza de la pila de huesos de la cueva. Notifica con el comando %^CURSIVA%^error%^RESET%^.\n");	
	 	return 1; //El comando se ejecutará correctamente SÓLO cuando llegue aquí
	}
	return 0; //Si llega aquí es porque NO pudo realizar el comando
}