// Zoilder 13-02-2011

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_PRADERA_AULLIDOS->describir(this_object(), "zona_profunda");	
	add_exit(SE, PRADERA_AULLIDOS + "pradera06.c", "path");
	add_exit(O, PRADERA_AULLIDOS + "pradera08.c", "path");
}