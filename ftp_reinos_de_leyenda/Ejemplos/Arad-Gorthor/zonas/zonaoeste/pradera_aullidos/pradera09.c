// Zoilder 13-02-2011

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_PRADERA_AULLIDOS->describir(this_object(), "pradera");	
	add_exit(E, PRADERA_AULLIDOS + "pradera08.c", "path");
	add_exit(SO, PRADERA_AULLIDOS + "pradera10.c", "path");
	add_exit("cueva", PRADERA_AULLIDOS + "cueva01.c", "escalable");
}

/*
*	Función que configura el tipo de terreno si se cae al escalar
*/
int dame_factor_superficial() 
{
	return 1; //Se pone el terreno blando al caer, pues hay maleza, etc. que amortiguarían algo la caida
}