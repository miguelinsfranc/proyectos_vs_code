// Zoilder 13-02-2011

#include "../../../path.h";
inherit "/std/outside";

void setup()
{
	GENERADOR_PRADERA_AULLIDOS->describir(this_object(), "zona_profunda");	
        add_exit(NO, PRADERA_AULLIDOS+"pradera12", "path");
        add_exit(SO, PRADERA_AULLIDOS+"pradera14", "path");
	add_exit(NE, PRADERA_AULLIDOS + "pradera03.c", "path");
	add_exit(O, PRADERA_AULLIDOS + "pradera05.c", "path");
}
