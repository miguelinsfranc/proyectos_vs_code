#include "../../../path.h"
inherit "/std/subterraneo.c";

int chequeo_salida(string tx, string verbo, object ob_caminante, object ob_que_sigo) {
	string * rooms_protegidas = ({CAVERNAS+"conclave13",CAVERNAS+"conclave15"});

	if (ob_caminante->dame_nomuerto()) {
		if (-1 != member_array(query_where_dir(verbo), rooms_protegidas)) { //Está en las rooms protegidas
			tell_object(ob_caminante, "Una fuerza mágica rodea el lugar al que quieres ir bloqueándote el acceso.\n");
			if (!query_timed_property("mensaje_enviado")) {
				add_timed_property("mensaje_enviado",1,1);
				tell_object(ob_que_sigo, "El "+ob_caminante->query_short()+" que te acompaña parece detenerse aturdido sin poder seguirte.\n");
			}
			return 0;
		}
	}
	return 1;
}
