//Rezzah 10/11/2009

#include "../../../path.h"
#define TRAMPA "/baseobs/trampas/esfera_acido.c"
inherit "/std/subterraneo.c";

void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "camino_oculto");
	add_exit(NO, CAVERNAS+"conclave18.c", "corridor");
	add_exit(S, CAVERNAS+"conclave20.c","door");
	setup_door(S, 1, 0, 0, 0, TRAMPA, 1000);
	set_long(query_long()+" Por la salida del sur hay evidentes signos de corrosión en la roca.\n");
	add_property("trampa_puerta_sur",4);//Si intenta detectarse se detecta siempre
	add_item(({"sur", "corrosion","roca","signo","signos","pared","paredes"}), "La salida sur es un pasillo grande aunque las paredes presentan signos evidentes de descalcificación y corrosión, como su hubieran sido bañadas por un poderoso ácido");
}

