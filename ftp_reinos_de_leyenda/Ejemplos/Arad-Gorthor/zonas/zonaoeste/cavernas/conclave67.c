//Rezzah 10/11/2009

#include "../../../path.h"
inherit "/std/subterraneo.c";

void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "laberinto");
	add_exit(SO,CAVERNAS+"conclave72.c","corridor");
	modify_item("salida", "El aire en la pared este parece más cálido, como si un gigantesco horno estuviera ardiendo al otro extremo.");
	modify_item("pared", "Es una altísima pared grisácea, como todas las anteriores en el Laberinto, pero parece que tiene una irregularidad a una altura considerable.");
	add_item("irregularidad", "Una pequeñísima fracción de la pared parece segmentada, como si se tratara de un botón, un pulsador, o algo similar.");
}

void init() {
	::init();
	add_action("abrir_pared", ({"pulsar","empujar"}));
}

void castigo(object pj) {
	tell_object(pj, "Aprovechas tu desproporcionada altura para pulsar el botón sin pensártelo dos veces cuando una tremenda sacudida eléctrica te lanza al otro extremo de la habitación.\n");
	tell_accion(environment(pj), pj->query_short()+" se estira cuan larg"+pj->dame_vocal()+" para alcanzar el botón pero un chispazo eléctrico le empuja hasta el otro extremo de la habitación.\n", "Oyes un grito de verdadero dolor seguido de un golpe seco.\n", ({pj}),0);
	pj->bloquear_accion("Estás aturdido por la fuerte descarga que acabas de recibir.\n", 6);
	pj->danyo_especial(-100-random(150), "electrico", this_object());
	tell_object(pj, "Durante la sacudida te ha parecido ver que tres piedras por debajo del pulsador hay otro boton pero... ¿Te atreves a pulsar otra vez?.\n");
	pj->add_static_property("glachdavir_salir_laberinto",2);
}
int abrir_pared(string tx) {
	if (!tx) {
		return notify_fail("¿Qué quieres "+query_verb()+"?\n");
	}
	if (tx != "pulsador" && tx != "boton" && tx != "botón") {
		return notify_fail("¿Qué quieres "+query_verb()+"?\n");
	}
	if (!this_player()->query_static_property("glachdavir_salir_laberinto")) {
		if (this_player()->dame_altura() < 300) {
			return notify_fail("El "+tx+" se encuentra demasiado alto como para que alcances el botón.\n");
		} else {
			castigo(this_player());
			return 1;
		}
	}
	if (this_player()->query_static_property("glachdavir_salir_laberinto") == 1)
		tell_object(this_player(), "Recordando la advertencia del espíritu de Gortag cuentas tres piedras por debajo del pulsador y presionas la pared.\n");
	else 
		tell_object(this_player(), "Cuentas tres piedras por debajo del maldito pulsador y presionas la pared descubriendo por sorpresa un botón completamente oculto.\n");

	this_player()->remove_static_property("glachdavir_salir_laberinto");
	tell_accion(TO, TP->query_short()+" cuenta piedras por debajo del pulsador y haciendo caso omiso de éste empuja la pared en un punto inconcreto de ésta.\n", "", ({TP}), 0);
	TP->bloquear_accion("Estás intentando mover las piedras de la pared este, no puedes hacer ninguna otra cosa.\n",3);
	call_out("se_abre", 3, TP, TO);
	return 1;
}

void se_abre(object pj, object room) {
	tell_accion(room, "Las rocas de la pared este parecen ceder al empuje de "+pj->query_short()+" y una abertura aparece en la roca.\n", "Oyes el ruido de piedra arrastrada sobre pierda.\n", ({pj}), 0);
	room->add_exit(E, CAVERNAS+"conclave86.c", "corridor");
	room->renew_exits();
}
