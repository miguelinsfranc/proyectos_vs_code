inherit "/std/subterraneo.c";
int ataque_magma() {//Se llama desde las rooms de la última zona y afecta a todo bicho que respiere con gas, y a todos con salpicaduras de lava
    foreach(object ob in all_inventory(this_object())) {
        if (living(ob)) { //afectado
            if (-1 == member_array(ob->query_name(), ({"glachdavir", "golem"}))) { //No es ni el demonio ni un golem de lava
                if(random(2)==1) {
					if (!ob->dame_nomuerto() ) { //Es un ser vivo, respira, le afecta el gas
						tell_object(ob, "El gas que inunda la sala entra en tus pulmones abrasándote.\n");
						ob->danyo_especial(-10-random(70), "veneno", this_object());
					}
				} else {
					if (ob->dame_ts("agilidad", -20)) { //esquiva el magma
						tell_object(ob, "Esquivas con gran fortuna una gota de magma salpicada desde uno de los charcos que se dirigía hacia tí.\n");
					} else { //sufre daño fuego y acido

						if(-300 != ob->spell_damage(-10-random(20),"fuego")){
							tell_object(ob, "Una diminuta gota de magma escupida desde un charco te golpea llenándote de un dolor indescriptible.\n");					
							ob->danyo_especial(-20-random(40), "acido", this_object());
						}

					}
				}
            }
        }
    }
    call_out("ataque_magma", 30+random(15));
}

void setup() {
	set_zone("conclave");
	call_out("ataque_magma", 6+random(5));
}
