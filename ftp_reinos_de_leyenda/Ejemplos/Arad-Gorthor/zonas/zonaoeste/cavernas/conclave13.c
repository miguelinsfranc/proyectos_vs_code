//Rezzah 10/11/2009
#include "../../../path.h"
#define DIARIO OBJETOS+"diario_gortag.c"


inherit CAVERNAS+"base_poblado.c";


void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "poblado");
	set_short(query_short()+": Rincón de Gortag");
	set_long(query_long()+" Esta zona, preparada para alojar un individuo, parece que estaba orientada a algún tipo de sanador de baja categoría por los pobres enseres y adornos de la estancia.\n");
	add_exit(NO, CAVERNAS+"conclave10.c", "standard");
	add_exit(N, CAVERNAS+"conclave12.c", "standard");
	add_exit(NE, CAVERNAS+"conclave15.c", "standard");
	add_exit(E, CAVERNAS+"conclave16.c", "standard");
	add_item(({"enseres","adornos"}), "Son realmente cutres y su aspecto es bastante descuidado, no solo por la suciedad que se acumuló con el tiempo, sino porque han sido tratados con descuido. Únicamente una pequeña cajita de piedra parece haber sido cuidada con algo de esmero.");
	add_item(({"caja","cajita"}), "Una caja cuadrada de apenas un palmo de lado tallada toscamente en piedra con una rendija dibujada en la parte superior.");
	add_item(({"rendija"}), "Una pequeña rendija cuadrada dibuja el interior de la cara superior de la caja, podría ser la tapa aunque carece de asa.");
	
}

int abrir_caja (string tx) {
	string aux;
	object diario;
	if (tx == "caja" || tx == "cajita") {
		if (-1 != member_array(query_verb(),({ "volcar","destapar","romper","levantar","abrir" }))) return notify_fail("Intentas "+query_verb()+" la caja pero no consigues nada, deberías probar otra cosa.\n");
		//El verbo que usó fue el correcto (girar)
		if (this_player()->query_static_property("glachdavir_sabe_girar")) {
			aux = "tal y como te indicó Gortag";
		}else {
			aux = "guiad"+this_player()->dame_vocal()+" por tu instinto";
		}
		if (query_static_property("caja_vacia")) {
			tell_accion(this_object(), this_player()->query_short()+" gira suavemente una caja de piedra y esta se abre revelando su contenido.\n", "", "Oyes la piedra resbalar contra la piedra.\n", ({this_player()}), 0);
			tell_object(this_player(), "Giras con sumo cuidado la caja "+aux+" y consigues abrirla pero está completamente vacía.\n");
		} else {
			add_static_property("caja_vacia",1);
			tell_accion(this_object(), this_player()->query_short()+" gira suavemente una caja de piedra abriéndola cogiendo lo que en ella encuentra.\n", "Oyes la piedra resbalar contra la piedra.\n", ({this_player()}), 0);
			tell_object(this_player(), "Giras con sumo cuidado la caja "+aux+" y consigues abrirla, en su interior ves un pequeño libro y lo coges rápidamente.\n");
			DAR_TOKEN(this_player(), "conclave", "conclave13");
			if (!diario = this_player()->entregar_objeto(DIARIO)) {
				tell_object(this_player(), "Intentando reorganizar tantas cosas como llevas dejas el libro en el suelo.\n");
				tell_accion(this_object(), this_player()->query_short()+" deja un libro en el suelo mientras intenta organizar sus pertenencias.\n","", ({this_player()}), 0);
            }
			
			
		
			
		}
		//Si no es de gurthang no está avisado (y si es de gurthang pero no sabe girar tampoco está avisado)
		if (!this_player()->query_static_property("glachdavir_sabe_girar") || this_player()->dame_religion() != "gurthang") {
			if (this_player()->nueva_incapacidad("gas", "venenos", 10, 60)) {//Le entró el veneno
				tell_accion(environment(), "De repente una nube gaseosa sale disparada de la caja y golpea a "+this_player()->query_short()+" en el rostro.\n", "Oyes un siseo proveniente de las cercanías.", ({this_player()}), this_player());
				tell_object(this_player(), "De repente una nube gaseosa sale dispara de la caja y te golpea en el rostro irritándo tus ojos, nariz y garganta.\n");
			} else {//No se ha podido envenenar
				tell_accion(environment(), "De repente un ruido sibilante, como un aire tirado con disimulo sale disparado de la caja al rostro de "+this_player()->query_short()+", quien no puede disimular una mueca de disgusto.\n", "Oyes un ruido demasiado parecido a un pedo como para sospechar que es otra cosa.\n", ({this_player()}), this_player());
				tell_object(this_player(), "De repente un ruido sibilante, como un aire tirado con disimulo sale disparado de la caja a tu rostro, con olor a huevos podridos por semanas, casi te entran arcadas del asco.\n");
			}
		}
		return 1;
	} 
	return notify_fail("¿No será la caja lo que quieres "+query_verb()+"?\n");
}
void init() {
	::init();
	add_action("abrir_caja", ({"abrir","levantar","romper","girar", "volcar","destapar"}));
}
