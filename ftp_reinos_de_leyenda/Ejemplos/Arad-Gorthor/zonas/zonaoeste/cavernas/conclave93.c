//Rezzah 10/11/2009

#include "../../../path.h"
inherit CAVERNAS+"base_conclave.c";

status dame_teleportar() { return 0; }

void setup() {
	::setup();
   GENERADOR_CONCLAVE->iniciar(this_object(), "conclave");
	set_long("Te encuentras en el centro de la amplia y calurosa caverna en la que te has metido. Los techos a pesar de ser alto lo único que hace es acumular los gases que vierte el pozo de magma que hay un poco más al este enturbiando el aire  y haciendo aún más dificil respirar. Pero por encima de todo, sin lugar a dudas, lo más destacado de aquí es el curioso círculo de esculturas que hay en el centro, cinco figuras de lava forman un corro alrededor de un complejo conjunto de rocas y trazos en lava en cuyo centro se alza una jaula de energía con una criatura en su interior.\n");
	set_short(query_short()+": %^RED%^Prisión de %^BOLD%^Glach'Davir%^RESET%^");
	add_item(({"prision","jaula"}), "Tiene forma esférica y está formada por haces de energía de distintas tonalidades aranjadas y vibran con un zumbido constante que crepita de vez en cuando al restallar la energía mágica.");
	add_item(({"techo","techos","pared","paredes","suelo"}), "Es pura roca volcánica, seguramente recubran las paredes originales que hayan quedado sepultadas tras el Cataclismo.");
	add_item(({"circulo","círculo","figura","figuras","estatua","estatuas","escultura","esculturas"}), "En el centro de la sala hay cinco esculturas formando un círculo, están talladas también con roca volcánica y la perfección de cada detalle te hacen pensar que se trata de un gran maestro del trabajo en piedra, ya que las particulares características de la roca volcánica la hacen difícil de trabajar. Están representando a cinco chamanes goblin, con diferentes expresiones en la cara: enfurecido, angustiado, cobarde, concentrado y enloquecido. Además por la postura de sus cuerpo y manos parece que estén llevando a cabo un ritual.");
	add_item(({"burz", "enfurecido"}), "Esta escultura representa a un goblin diminuto cargado de abolorios y con un gran tocado en la cabeza, debería tratarse de un goblin temido y respetado en su época pues el autor supo plasmar una auténtica expresión de odio en su rostro, con tanta fiabilidad que aún te estremeces al mirarla. Está mirando hacia sur aunque a ninguna parte en concreto.");
	add_item(({"angustiado", "berguk"}), "Esta escultura representa a un goblin obeso y ostentoso, sus ropajes son ridículos incluso para su época y con gran maestría el escultor supo mostrar la gordura de su papada, los ojos hinchados y los músculos flácidos cayendo por sus cuerpo, casi deformado. En su expresión puso un interés especial en representar el temor de la muerte, la mirada de profunda angustia ante algo inminente que no se puede evitar. Esta escultura mira hacia el noreste.");
	add_item(({"cobarde", "gortag"}), "Esta escultura te suena extrañamente familiar, su rostro, sus manos, jurarías que las has visto con anterioridad, quizá la casualidad haya hecho que te cruzaras con un descendiente suyo en algún momento de tu vida. La expresión es de auténtico miedo, no un miedo irracional, ni siquiera del miedo al futuro, sino la absoluta convicción del sufrimiento por venir y el temor a éste. Su posición en el círculo es mirando hacia el noroeste.");
	add_item(({"concentrado", "chaagrik"}), "Esta escultura representa un goblin de edad avanzadísima, del cuello le cuelga un cornetín por lo que seguramente la figura que inspiró esta obra maestra de la escultura era un goblin duro de oido. En su gesto y en sus manos se ha intentado reflejar una profunda sabiduría por ello expresa la concentración profunda, un estado de ánimo calmado pero muy intenso, frunciendo el ceño y con la lengua fuera, en plena concentración mientras sus ojos miran hacia el infinito en el oeste.");
	add_item(({"enloquecido", "shoj"}), "Esta escultura ha caracterizado un goblin muy joven, casi un niño goblin, aunque emana un gesto de cierto poder o estatus social pues luce riquezas sin ostentación,  y su tocado es voluminoso, indicando su destacada posición social. En profundo contraste con esto su expresión está completamente desencajada por la locura, la boca abierta y torcida, los ojos fuera de sus orbitas, como su viera la mismísima muerte acercarse desde el este, dirección en la que mira.");
	add_exit(O, CAVERNAS+"conclave92.c", "standard");
	add_exit(N, CAVERNAS+"conclave91.c", "standard");
	add_exit(NO, CAVERNAS+"conclave90.c", "standard");
	add_exit(E, CAVERNAS+"conclave94.c", "standard");
	add_exit(S, CAVERNAS+"conclave96.c", "standard");
	add_exit(SO, CAVERNAS+"conclave95.c", "standard");

	// clang-format off
	add_clone(
		NPCS+"glachdavir",
		1,
		0,
		(: sizeof(children(NPCS+"glachdavir")) < 2 :)
	);
	// clang-format on

	add_timed_property("no_limpiable",1,4*3600);
    add_timed_property("no_clean_up",1,4*3600);	
}
