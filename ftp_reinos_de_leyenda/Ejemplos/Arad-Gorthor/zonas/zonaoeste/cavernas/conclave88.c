//Rezzah 10/11/2009

#include "../../../path.h"
inherit "/std/subterraneo.c";

void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "camino_conclave");
	add_exit(SO, CAVERNAS+"conclave87.c", "corridor");
	add_clone(NPCS+"guardian_conclave.c", 1);
//	add_exit(E, CAVERNAS+"conclave89.c", "corridor");
}
int chequeo_salida(string tx,string verbo,object ob_caminante,object ob_que_sigo) {
	object guardian;
	guardian = secure_present(NPCS+"guardian_conclave.c", this_object());
	if (guardian && query_where_dir("este") && verbo == "este") {
		if (ob_caminante->query_static_property("glachdavir_paso_autorizado")) {
			tell_accion(TO, query_short()+" se retira lo justo para permitir a "+ob_caminante->query_short()+" cruzar el umbral hacia el este.\n", "", ({ob_caminante}), ob_caminante);
			tell_object(ob_caminante, query_short()+" se retira lo justo para permitirte cruzar el umbral hacia el este.\n");
			return 1;
		} else {
			tell_object(ob_caminante, guardian->query_short()+" te detiene cuando intentas cruzar el umbral este.\n");
			tell_accion(TO, ob_caminante->query_short()+" intenta cruzar el umbral este pero "+guardian->query_short()+" le detiene.\n", "", ({ob_caminante}), ob_caminante);
			return 0;
		}
	}
return 1;
}

