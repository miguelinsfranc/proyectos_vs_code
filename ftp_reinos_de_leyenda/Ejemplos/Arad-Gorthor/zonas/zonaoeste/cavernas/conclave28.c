//Rezzah 13/11/2009
//Las runas

#define T "[%^YELLOW%^T%^RESET%^]"
#define t "[%^YELLOW%^t%^RESET%^]"
#define BT "[%^B_YELLOW%^T%^RESET%^]"
#define Bt "[%^B_YELLOW%^T%^RESET%^]"
#define F "[%^RED%^F%^RESET%^]"
#define f "[%^RED%^f%^RESET%^]"
#define BF "[%^B_RED%^F%^RESET%^]"
#define Bf "[%^B_RED%^f%^RESET%^]"
#define A "[%^BOLD%^WHITE%^A%^RESET%^]"
#define a "[%^BOLD%^WHITE%^a%^RESET%^]"
#define BA "[%^B_WHITE%^A%^RESET%^]"
#define Ba "[%^B_WHITE%^a%^RESET%^]"
#define H "[%^BLUE%^H%^RESET%^]"
#define h "[%^BLUE%^h%^RESET%^]"
#define BH "[%^B_BLUE%^H%^RESET%^]"
#define Bh "[%^B_BLUE%^h%^RESET%^]"
#define R "[%^CYAN%^R%^RESET%^]"
#define r "[%^CYAN%^r%^RESET%^]"
#define BR "[%^B_CYAN%^R%^RESET%^]"
#define Br "[%^B_CYAN%^r%^RESET%^]"

#include "../../../path.h"

inherit "/std/subterraneo.c";

varargs void modificaciones(object);
mixed chequear_tablero();

string desc() {
	return "Te encuentras en un pequeño pasillo de piedra similar a los que has cruzado con anterioridad, en sus "
            "paredes la misma piedra de aspecto suave y redondeado emana aún más calor si cabe. Al menos su brillo rojizo te permite vislumbrar "
            "el camino. En esto punto parece terminarse el camino llevando a una pequeña sala trabajada hace ya mucho tiempo por una especie que "
			"no consigues identificar. Una de las paredes está grabada con una descomunal amalgama de runas que brillan con una luz pálida, "
			"se trata de símbolos desconocidos de los cuales es imposible extraer el significado. A los pies de la propia pared lo que parece un "
			"panel de mandos lanza constantemente pequeños haces de luz hacia las runas en la pared.\n";
}

void init() {
	::init();
	add_action("cambiar_runa", "sustituir");
	add_action("encantar_arma", "introducir");
}


int encantar_arma(string tx) {
	string * pals;
	string arma = "";
	object vieja;//,nueva;
	string resto = "";
//	string fich_name;
//	string fichero;
	string elemento;
	string color;
//	string nuevo_codigo;

	mixed tab;
	int arma_completa = 0;

	tab = chequear_tablero();

	if (tab[2] < 15 || query_static_property("encantar-arma-locked"))return notify_fail("%^DEFAULT%^Parece que %^GREEN%^BOLD%^introducir%^DEFAULT%^RESET%^ no produjo efecto alguno.\n");
	
	if (!tx) return notify_fail("¿Qué quieres introducir?¿En dónde?\n");
	pals = explode(tx, " ");

	if (sizeof (pals) < 3)  return notify_fail("¿Qué quieres introducir?¿En dónde?\n");

	//Desmontado la cadena que acompaña al comando para ver si lo escribe correctamente
	arma = "";
	resto = "";
	foreach (string pal in pals) {
		if (pal == "en") arma_completa = 1;		
		if (!arma_completa) 
			arma = arma == ""?pal:arma + " " + pal;
		else
			resto = resto== ""?pal:resto + " " + pal;
	}
	pals = explode(tx, " ");
	//Comprobamos que lo que intenta es lo que nosotros tenemos pensado
	if (-1 == member_array(resto, ({"en la urna", "en urna", "en receptaculo", "en el receptaculo", "en receptáculo", "en el receptáculo"}) ) )
		return notify_fail("¿Dónde quieres introducir tu "+arma+"?\n");
	//Comprobamos que tiene el objeto que quiere introducir
	if (!vieja = present(arma, this_player())) {
		return notify_fail("No encuentras tu "+arma+" por ninguna parte.\n");
	}
/* LAS ARMAS NO SE CLONARAN COPIANDO FICHEROS (eso no se hace, kk) SE LES AÑADIRÁ UN ATAQUE (QUE SE PERDERÁ TRAS EL RESETEO DEL ARMA AL ORBITAR)
	//Comprobamos que es un arma baseob
	if ( -1 == strsrch(real_filename(vieja), "/baseobs/armas/")) {
		tell_accion(this_object(), this_player()->query_short()+" intenta introducir su "+vieja->query_short()+" en la urna que hay en el centro de la habitación pero alguna fuerza desconocida parece resistírsele.\n","", ({this_player()}), 0);
		return notify_fail("Intentas introducir tu "+vieja->query_short()+" en la urna que hay en el centro de la habitación pero parece rechazarte.\n");
	}
	if (this_player()->dame_pe()<100) {
		tell_accion(this_object(), this_player()->query_short()+" intenta introducir su "+vieja->query_short()+" en la urna que hay en el centro de la habitación pero no consigue reunir la fuerza necesaria para aproximarse.\n", "", ({this_player()}), 0);
		tell_object(this_player(), "Intentas introducir tu arma en la urna pero la energía de ésta te repele y no consigues acercarte.\n");
		return 1;
	}
	//obtenemos el nombre del fichero	
	fich_name = replace(real_filename(vieja),"/baseobs/armas/","");
	//y comprobamos si existe el arma que va a encantar, para ello determinamos el elemento
	switch (tab[1]) { //El estado indica la runa dominante (ya sabemos que el dominio es máximo, 15)
		case A: elemento = "aire";color = "%^BOLD%^WHITE%^";break;
		case F: elemento = "fuego";color = "%^RED%^";break;
		case H: elemento = "agua";color = "%^BLUE%^";break;
		case R: elemento = "electrico";color = "%^CYAN%^";break;
		case T: elemento = "tierra";color = "%^YELLOW%^";break;
	}
	
	fichero = read_file(ARMAS+"encantador/"+fich_name+"_"+elemento+".c");
	if (!fichero) { //no existe, es necesario crear el fichero
		nuevo_codigo = read_file(real_filename(vieja)+".c");
		nuevo_codigo = replace(nuevo_codigo, "set_name", "add_attack(\"ataque_encantado\", \""+elemento+"\", 3,20,15,0,0,0,1);\nset_name");
		nuevo_codigo = replace(nuevo_codigo, vieja->query_short(), color+ vieja->query_short()+"%^RESET%^");
		write_file(ARMAS+"encantador/"+fich_name+"_"+elemento+".c", nuevo_codigo);
	}
	nueva = clone_object(ARMAS+"encantador/"+fich_name+"_"+elemento+".c");
	if (!nueva) {
		tell_object(this_player(), "Algo ha fallado en la urna. Repórtalo usando el comando bug siendo lo más detallado que puedas.\n");
		return 1;
	} 

	if (!nueva->move(this_object())) {
		tell_accion(this_object(), "La urna empieza a crepitar de energía que es absorbida rápidamente por "+vieja->dame_articulo()+" "+vieja->query_short()+" que introduce " + this_player() + " hasta que es consumida por completo. El arma, sin urna ya que la contenga, cae al suelo con un estallido de energía.\n", "", ({this_player()}), 0);
		tell_object(this_player(), "Al depositar tu "+vieja->query_short()+" en la urna notas como empieza a absorberla rápidamente hasta que la consume por completo cayendo inerte al suelo rebosante de la energía robada. El encantamiento parece haberte dejado agotado.\n");
		this_player()->ajustar_pe(to_int(floor(this_player()->dame_pe()*0.90))*-1);
	} else {
		tell_object(this_player(), "Algo ha pasado al encantar el arma. Repórtalo usando el comando bug siendo lo más detallado que puedas.\n");
		return 1;
	}
	destruct(vieja);
*/
    switch (tab[1]) { //El estado indica la runa dominante (ya sabemos que el dominio es máximo, 15)
        case A: elemento = "aire";color = "%^BOLD%^WHITE%^";break;
        case F: elemento = "fuego";color = "%^RED%^";break;
        case H: elemento = "agua";color = "%^BLUE%^";break;
        case R: elemento = "electrico";color = "%^CYAN%^";break;
        case T: elemento = "tierra";color = "%^YELLOW%^";break;
    }

	tell_accion(this_object(), "La urna empieza a crepitar de energía que es absorbida rápidamente por "+vieja->dame_articulo()+" "+vieja->query_short()+" que introduce " + this_player() + " hasta que es consumida por completo. El arma, sin urna ya que la contenga, cae al suelo con un estallido de energía.\n", "", ({this_player()}), 0);
    tell_object(this_player(), "Al depositar tu "+vieja->query_short()+" en la urna notas como empieza a absorberla rápidamente hasta que la consume por completo cayendo inerte al suelo rebosante de la energía robada. El encantamiento parece haberte dejado agotado.\n");
    this_player()->ajustar_pe(to_int(floor(this_player()->dame_pe()*0.40))*-1);

	vieja->add_attack("secundario", elemento, 3, 20, 15, 0,0,0,1);
	vieja->set_long(query_long()+"Parece haber sido encantada con algún "+color+"poder arcano%^RESET%^.\n");
	add_static_property("encantar-arma-locked",2);
	return 1;
}

varargs string runear(string runa, int bold) {
	if (!runa) return "";
	if (runa == "A") return bold?BA:A;
	if (runa == "a") return bold?Ba:a;
	if (runa == "R") return bold?BR:R;
	if (runa == "r") return bold?Br:r;
	if (runa == "T") return bold?BT:T;
	if (runa == "t") return bold?Bt:t;
	if (runa == "F") return bold?BF:F;
	if (runa == "f") return bold?Bf:f;
	if (runa == "H") return bold?BH:H;
	if (runa == "h") return bold?Bh:h;
	return "";
}

string runedomize() {
	return ({"A","a","F","f","T","t","R","r","H","h"})[random(10)];
}
int runediff(string ru, int nuevas) { //elimina una runa de r_nuevas si nuevas es true, sino la elimina de r_tablero, retorna el tamaño resultante
	int eliminada = 0;
	string * rdo;
	//la diferencia de arrays elimina todas las ocurrencias, lo recorremos porque solo queremos eliminar una
	if (!query_property(nuevas?"r_nuevas":"r_tablero")) { //Estaba vacío
		return 0;
	}
	rdo = ({});
		
	foreach (string aux in query_property(nuevas?"r_nuevas":"r_tablero")) {
		if (aux == ru && !eliminada) {
			eliminada = 1;
			continue;
		}else {
			rdo = rdo + ({aux});
		}
	}
	add_property(nuevas?"r_nuevas":"r_tablero", rdo); //machacamos el anterior
	return sizeof(rdo);
	
}
int runeadd(string ru, int nuevas) { //añade una runa a r_nuevas si nuevas es true, sino la añade a r_tablero, retorna el tamaño resultante
    if (!query_property(nuevas?"r_nuevas":"r_tablero")) { //Estaba vacío
        add_property(nuevas?"r_nuevas":"r_tablero", ({ru}));
        return 1;
    }
	add_property(nuevas?"r_nuevas":"r_tablero", query_property(nuevas?"r_nuevas":"r_tablero") + ({ru}));
	return sizeof(query_property(nuevas?"r_nuevas":"r_tablero"));
}
string tablero() {//retorna una cadena con las runas de tablero coloreadas
	string salida = "";
	if (!query_property("r_tablero")) return salida;
	foreach (string ru in query_property("r_tablero")) {
		salida = salida + runear(ru);
	}
	return salida;
}
string nuevas() { // retorna una cadena con las runas de sustitucion coloreadas
	string salida = "";
	if (!query_property("r_nuevas")) return salida;
    foreach (string ru in query_property("r_nuevas")) {
        salida = salida + runear(ru);
    }
    return salida;
}
mixed chequear_tablero() {//Calcula el estado del tablero y returna un array de int ESTADO y string RunaDominante
	string tab,dom;
	int est,sf,st,sa,sh,sr;
	sf = 0;
	st = 0;
	sa = 0;
	sr = 0;
	sh = 0;
	if (!query_property("r_tablero")) return "Hay un error";
	tab = query_property("r_tablero");
	foreach (string ru in tab) {
		switch (ru) {
		case "A": 
			sa = sa + 3;sf = sf + 2;sr = sr + 2;sh = sh - 3;st = st - 3;break;
		case "a":
			sa = sa + 2;sf = sf + 1;sr = sr + 1;sh = sh - 2;st = st - 2;break;
		case "R":
			sr = sr + 3;sh = sh + 2;sf = sf + 2;st = st - 3;sa = sa - 3;break;
		case "r":
			sr = sr + 2;sh = sh + 1;sf = sf + 1;st = st - 2;sa = sa - 2;break;
		case "H":
			sh = sh + 3;sr = sr + 2;st = st + 2;sa = sa - 3;sf = sf - 3;break;
		case "h":
			sh = sh + 2;sr = sr + 1;st = st + 1;sa = sa - 2;sf = sf - 2;break;
		case "F":
			sf = sf + 3;sr = sr + 2;sa = sa + 2;sh = sh - 3;st = st - 3;break;
		case "f":
			sf = sf + 2;sr = sr + 1;sa = sa + 1;sh = sh - 2;st = st - 2;break;
		case "T":
			st = st + 3;sa = sa + 2;sh = sh + 2;sr = sr - 3;sf = sf - 3;break;
		case "t":
			st = st + 2;sa = sa + 1;sh = sh + 1;sr = sr - 2;sf = sf - 2;break;
		}
	}
	est = st+sa+sh+sr+sf;
	if (max(({st,sa,sh,sr,sf}), 0) > 2) {
		dom = ({T,A,H,R,F})[max(({st,sa,sh,sr,sf}), 1)];
	} else {
		dom = ({t,a,h,r,f})[max(({st,sa,sh,sr,sf}), 1)];
	}
	return ({est, dom, max(({st,sa,sh,sr,sf}))});
}
//Comando: sustituir RUNA por RUNA
		
int cambiar_runa(string str) {
	string * pals;
	string rt, rn;
	string aux;

	if (!str) return notify_fail("¿Sustituir qué? Esto es un juego serio, por favor...\nLa sintaxis correcta es:\n\"sustituir [runa de la línea de 5 que desaparece] por [runa de la línea de 3 que se mueve al pentágono (la línea de 5)]\"\nRecuerda que el objetivo es estabilizar el tablero, para ello tienes que conseguir que la runa de la primera línea, que indica la tendencia elemental del tablero, se anule (es decir, indique que el tablero es estable).\n");
	pals = explode(str, " ");
	if (sizeof(pals) != 3) return notify_fail("Prueba mejor con 'sustituir <runa del tablero> por <runa de sustitución>'.\n");
	if (pals[1] != "por") return notify_fail("Prueba mejor a 'sustituir "+pals[0]+" por "+pals[2]+"'.\n");

	if (member_array(pals[0], query_property("r_tablero")) == -1) return notify_fail(pals[0]+" no es una runa del pentágono (la tercera fila).\n");
	if (member_array(pals[2], query_property("r_nuevas")) == -1) return notify_fail(pals[2]+" no es una runa de sustitución (la segunda fila).\n");
		
	rt = pals[0];
	rn = pals[2];

	//Eliminamos la runa de en juego
	runediff(rn, 1);
	//Creamos la nueva runa en juego
	aux = runedomize();
	//Y la añadimos a la zona en juego
	runeadd(aux, 1);

	//Eliminamos la runa vieja de tablero
	runediff(rt, 0);
	//Añadimos a tablero la runa de juego
	runeadd(rn, 0);

	modificaciones(this_player());
	
	return 1;
}

varargs void modificaciones(object jugador) {
	string tab, tab2;
	mixed la;

	la = chequear_tablero();

	tab = "El tablero del panel de mandos del mosaico rúnico tiene tres secciones.\n"
	"La primera de ellas parece indicar la tendencia de la inestabilidad del pentágono y brilla con una runa.\n"
	"La segunda sección contiene tres runas con las que puedes %^BOLD%^RED%^sustituir%^RESET%^ las del pentágono buscando estabilizar el mosaico.\n"
	"La tercera sección es un pentágono donde cinco runas brillan cada una según su elemento: \n";
	tab2="      ESTADO\n   NUEVAS\nTABLERO\n";
    tab2 = replace_string(tab2, "TABLERO", tablero());//runas de tablero
    tab2 = replace_string(tab2, "NUEVAS", nuevas());//runas de sustitucion
	if (la[0] == 0) {
		tab2 = replace_string(tab2, "ESTADO", 0);
	} else {
	    tab2 = replace_string(tab2, "ESTADO", la[1]);//calcula y fija el estado
	}
	modify_item("tablero", tab+tab2);
	if (jugador) { //Es que hay que mostrar los mensajes de lo sucedido
		tell_object(jugador, "Tocas una runa de la segunda fila y otra de la tercera después y haces que una sustituya a la otra quedando así:\n"+tab2);
		tell_accion(this_object(), jugador->query_short()+ " toca una runa de la segunda fila del tablero y después una de la tercera, la cual se desvanece y es sustituida por la runa que antes había tocado.\n Una nueva runa aparece reemplaza a la utilizada en la segunda fila.\n", "", ({jugador}), 0);
		if (la[2] == 15) {
			//El dominio de la runa es maximo (5 F, o 5 A, o 5 H, etc...) lo que activa la ranura para encantar armas
			//El encantamiento consiste en introducir un arma con filename en /baseobs/armas/ y devolver la misma pero con un encantamiento de la runa (un ataque extra de ese elemento)
			tell_room(this_object(), "En el centro de la habitación se abre una compuerta que había pasado por completo desapercibida y se eleva un receptáculo que palpita con energía.\n");
			add_item("compuerta", "");
			add_item(({"receptaculo","receptáculo", "urna"}), "Una urna cuyas paredes son sencillamente pura energía arcana, por su forma y tamaño podrías guardar como máximo un objeto en ella.");
		}	
	}
}
void setup() {
	string aux;
	int i;

    GENERADOR_CONCLAVE->iniciar(this_object(), "camino_caverna");
	add_exit(NE, CAVERNAS+"conclave27.c", "corridor");
	set_long(desc());
	add_item(({"amalgama","runa","runas", "pared", "símbolo", "simbolo", "símbolos", "simbolos", "mosaico"}), "Como un mosaico de distintos y vivos colores, toda la pared está bañada por la luz arcana que emana "
	"de las runas grabadas en ella. Toda la pared está grabada con símbolos arcanos en el que las piezas parecen encajar maravillosamente excepto en un rincón.");
	add_item(({"rincón","rincon"}), "Hay un rincón del gran mosaico arcano que recubre la pared en el que parece que las piezas no encajan correctamente y en el que la energía fluye torpemente, a trompicones, puedes ver que hay un total de cinco piezas formando un pentágono que no parecen estar bien dispuestas. A su lado otras tres runas parecen preparadas para sustituirlas y completar al fin el mosaico.");
	add_item("pentagono", "Es una pequeña sección del mosaico que parece inestable, crepita con el enfrentamiento de energías. Si consiguieras arreglar la disposición de las runas podrías completar el mosaico de una vez por todas.");
	add_item("panel", "Hilos de pura energía arcana unen este panel con la pared, en él hay una representación de las runas en la pared, aunque sólo de una sección, con forma de pentágono, que fluctúa por la energía desestabilizada que posee. En este mismo panel hay otras tres runas que parecen servir para sustituir las representadas en el pentagono, como si un %^RED%^BOLD%^tablero%^RESET%^ de juegos fuera.");
	add_item("tablero","El tablero del panel de mandos del mosaico rúnico tiene tres secciones.\n"
    "La primera de ellas parece indicar la tendencia de la inestabilidad del pentágono y brilla con una runa.\n"
    "La segunda sección contiene tres runas con las que puedes sustituir las del pentágono buscando estabilizar el mosaico.\n"
    "La tercera sección es un pentágono donde cinco runas brillan cada una según su elemento: \n");
	//add_sign("Una diminuta frase grabada sobre la roca del suelo", "Solo la sabiduría te llevará al destino que mereces", "inscripcion", "Inscripción", "negra"); 
	//Inicializamos el juego
	for (i=0;i<5;i++) {
		aux = runedomize();
		runeadd(aux, 0);
	}
	for (i=0;i<3;i++) {
		aux = runedomize();
		runeadd(aux,1);
	}
	modificaciones();//Se pasa sin parámetros para que no informe a ningún jugador del resultado
}
