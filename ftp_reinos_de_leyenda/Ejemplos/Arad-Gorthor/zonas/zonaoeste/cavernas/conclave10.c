//Rezzah 10/11/2009

#include "../../../path.h"


inherit CAVERNAS+"base_poblado.c";

void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "poblado");
	add_exit(O, CAVERNAS+"conclave09.c", "corridor");
	add_exit(NE, CAVERNAS+"conclave11.c","standard");
	add_exit(E, CAVERNAS+"conclave12.c","standard");
	add_exit(SE, CAVERNAS+"conclave13.c","standard");
}

