//Rezzah 10/11/2009
//Grimaek 28/05/2023 Sustituyo la firma "Zhariagh eztubo aqi" por pistas para el portal
#include "../../../path.h"
inherit "/std/subterraneo.c";

void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "portal");

	add_exit(O, CAVERNAS+"conclave21.c", "gate");
	add_exit(S, CAVERNAS+"conclave23.c", "standard");
	add_exit(SE, CAVERNAS+"conclave24.c","standard");
	add_exit(E, CAVERNAS+"conclave25.c", "standrard");
}

mixed *descifrar_inscripcion(string objeto_inscrito,object erudito)
{
   if(objeto_inscrito=="marca" || objeto_inscrito=="inscripcion" || objeto_inscrito=="techo" || objeto_inscrito=="peculiar")
      return ({"%^BOLD%^WHITE%^Burz sabe todos los secretos.%^RESET%^\n",4,"negra"});
   return 0;
}
