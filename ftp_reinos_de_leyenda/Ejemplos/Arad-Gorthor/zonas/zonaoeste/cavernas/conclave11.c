//Rezzah 10/11/2009

#include "../../../path.h"


inherit CAVERNAS+"base_poblado.c";

void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "poblado");
	add_exit(SO, CAVERNAS+"conclave10.c","standard");
	add_exit(S, CAVERNAS+"conclave12.c", "standard");
	add_exit(E, CAVERNAS+"conclave14.c","standard");
	add_exit(SE,CAVERNAS+"conclave15.c","standard");
	add_clone(NPCS+"gortag.c",1);
}

