//Rezzah 10/11/2009

#include "../../../path.h"
inherit CAVERNAS+"base_conclave.c";

status dame_teleportar() { return 0; }

void setup() {
	::setup();
   GENERADOR_CONCLAVE->iniciar(this_object(), "conclave");
	add_exit(O, CAVERNAS+"conclave89.c", "standard");
	add_exit(N, CAVERNAS+"conclave90.c", "standard");
	add_exit(NE, CAVERNAS+"conclave91.c", "standard");
	add_exit(E, CAVERNAS+"conclave93.c", "standard");
	add_exit(S, CAVERNAS+"conclave95.c", "standard");
	add_exit(SE, CAVERNAS+"conclave96.c", "standard");
	add_timed_property("no_limpiable",1,4*3600);
    add_timed_property("no_clean_up",1,4*3600);	
}

