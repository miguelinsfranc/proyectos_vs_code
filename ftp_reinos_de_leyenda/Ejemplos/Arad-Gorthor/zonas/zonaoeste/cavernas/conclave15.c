//Rezzah 10/11/2009

#include "../../../path.h"
#define MANUAL OBJETOS+"/diario_burz.c"
inherit CAVERNAS+"base_poblado.c";

///FUNCIONES DE LIBRERIA COPIADAS DE /std/biblioteca.c PARA ADAPTARLAS A UNA LIBRERIA SELF-SERVICE XD
mapping libros = ([
	"Compendio Demoníaco":"compendio_demoniaco",
	"Infracriaturas":"infracriaturas_burz",
	"Mosaico de Elementos":"mosaico_elementos",
	"Recetas de Elevación Espiritual":"recetas_drogas",
	"Rituales: Tipos y Modos":"rituales_tipos_modos",
	"Salmos":"salmos_chaman",
	"Torturas":"torturas_burz",
	"Visiones y Sueños Premonitorios":"visiones_burz"
]);
 
void nuevo_libro(mapping aux) { libros+= aux; }//Añade a los existentes
int solicitar_libro(string titulo)
{
    object libro;
    if(!titulo)
        return notify_fail("Sintaxis: recuperar <título>\n");
	DAR_TOKEN(this_player(), "conclave", "conclave15");	
    if(this_player()->query_timed_property("libro_prestado"))
    {
		tell_object(this_player(), "Las runas que adornan la biblioteca crepitan cuando intentas recuperar un segundo libro. Deberías devolver el anterior.\n");
		tell_accion(this_object(), this_player()->query_short()+" intenta recuperar un libro de la biblioteca pero esta crepita con energía impidiéndoselo.\n", "", ({this_player()}), this_player());
        return 1;
    }
    if(!libros[titulo])
    {
        int encontrado= 0;

        //Búsqueda más inteligente
        foreach(string t in keys(libros))
            if(-1!=strsrch(lower_case(t),titulo))
            {
                encontrado= 1;
                titulo= t;
                break;
            }
        if(!encontrado)
        {
			tell_object(this_player(), "Titubeas buscando entre las estanterías pero no encuentras ningún tomo titulado "+titulo+"\n");
            return 1;
        }
    }
    if(libros[titulo][0]=='/' && file_size(libros[titulo])>0)
    {
        //Es un libro especial, clonable desde un archivo especifico
        libro= clone_object(libros[titulo]);
    }
    else
    {
        libro= clone_object("/obj/libro.c");
        libro->set_short(capitalize(libros[titulo]));
        libro->add_alias(explode(lower_case(titulo)," ")-({"de","la","del","las","los","el","con","contra","desde"}));
        libro->fijar_archivo_lectura(libros[titulo]);
    }
    if(libro->move(this_player()))
    {
        tell_object(this_player(),"El libro se te cae al suelo.\n");
        libro->move(this_object());
    }
    libro->add_property("indice",titulo);//Para hacer referencia al libro
    tell_object(this_player(),"Recuperas el tomo '"+libro->query_short()+"' de la biblioteca.\n");
    tell_accion(this_object(),this_player()->query_short()+" recupera un libro de la biblioteca.\n","", ({this_player()}), this_player());
    this_player()->add_timed_property("libro_prestado",capitalize(titulo),2000);
    return 1;
}
int devolver_libro(string titulo)
{
    object libro;
    if(!titulo)
        return notify_fail("Sintaxis: devolver <libro>\n");
    if(!libro= present(titulo,this_player()))
        return notify_fail("No tienes ningún "+titulo+" que devolver a la biblioteca.\n");
    titulo= libro->query_property("indice");
    if(!libros[titulo])
    {
		tell_object(this_player(), "Cuando intentas devolver el tomo a la biblioteca ésta lo rechaza con un zumbido por no pertenecerle.\n");
		tell_accion(this_object(), this_player()->query_short()+" intenta devolver un libro a la biblioteca pero un zumbido se lo impide.\n", "Oyes un extraño zumbido.\n", ({this_player()}), 0);
        return 1;
    }
    tell_object(this_player(),"Devuelves el libro: "+titulo+".\n");
    tell_accion(this_object(), this_player()->query_short()+" devuelve un libro a la biblioteca.\n", "", ({this_player()}), this_player());
    this_player()->remove_timed_property("libro_prestado");
    libro->dest_me();
    return 1;
}
int listado(string tx)
{
    string *libs= keys(libros);
    int i= sizeof(libs);
    if(!i)
        return notify_fail("No hay ningún libro en la biblioteca.\n");
    tx= "\nInspeccionas los lomos de los ejemplares en las estanterías y ves los siguientes títulos:\n\n";
    while(i--)
        tx+= "  - "+libs[i]+"\n";
    tell_object(this_player(),tx+"\n");
    return 1;
}

void clonar_cofre() {
    object cofre = clone_object("/obj/baules/cofre");
    object manual = clone_object(MANUAL);
	cofre->set_long("Un cofre de considerables dimensiones que ocupa un lugar destacado en el Rincón de Burz. Es de madera negra y tiene grabadas runas que brillan con evidente poder mágico.\n");
	cofre->set_short("Cofre de Burz");

    manual->move(cofre);

    cofre->move(this_object());
    cofre->setup_cerradura(40, 1, OBJETOS+"llave_inexistente");
    cofre->reset_get();
}

void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "poblado");
	set_short(query_short()+": Rincon de Burz.");
	set_long(query_long()+" Por la amplitud de esta rincón, delimitado por antorchas, el habitante de esta estancia debería ser un personaje importante en el poblado, un erudito si te guías por la completa biblioteca que se extiende ante tí.\n");
	add_item("rincon", "El espacio que abarcan las antorchas que rodean este rincón es bastante mayor que otros que hayas visto, todo tiene un orden y se conserva indemne al paso del tiempo. Destaca entre todo un enorme arcón cerrado.\n");
	add_item("biblioteca", "Tallado directamente sobre la roca este un conjunto de estanterías conservan una extensa colección de tomos de aspecto viejo pero bien conservados, seguramente gracias a los glifos arcanos que tiene tallados.\n");
	add_item(({"glifo","glifos","runa","runas"}), "Alrededor de la biblioteca y en cada estante tallado en la piedra hay marcas arcanas de runas de protección para evitar que el paso del tiempo deteriore los libros que sujetan.\n");
	add_exit(NO, CAVERNAS+"conclave11.c", "standard");
	add_exit(O, CAVERNAS+"conclave12.c", "standard");
	add_exit(SO, CAVERNAS+"conclave13.c", "standard");
	add_exit(N, CAVERNAS+"conclave14.c", "standard");
	add_exit(S, CAVERNAS+"conclave16.c", "standard");
	clonar_cofre();

	
}
void init() {
	::init();
    add_action("solicitar_libro","recuperar");
    add_action("devolver_libro","devolver");
    add_action("listado","listar");
}
