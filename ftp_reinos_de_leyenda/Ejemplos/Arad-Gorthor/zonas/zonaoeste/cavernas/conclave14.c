//Rezzah 10/11/2009

#include "../../../path.h"


inherit CAVERNAS+"base_poblado.c";

void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "poblado");
	add_exit(O, CAVERNAS+"conclave11.c", "standard");
	add_exit(SO, CAVERNAS+"conclave12.c", "standard");
	add_exit(S, CAVERNAS+"conclave15.c", "standard");
}

void init() {
	::init();
	add_action("sacudir_pared", ({"sacudir", "golpear", "empujar"}));
}
int sacudir_pared(string txt) {
	if (-1 == member_array(txt, ({"muro","pared","el muro","la pared"})))
		return notify_fail("¿Sacudir qué?\n");

	tell_object(this_player(), "Al golpear la pared norte ésta tiembla como una hoja y se derrumba.\n");
	tell_accion(this_object(), this_player()->query_short()+" golpea la pared norte haciéndola temblar como una hoja hasta derrumbarse.\n", "Oyes el estruendo de cascotes rodando.\n", ({this_player()}), 0);
	add_exit(N, CAVERNAS+"conclave17.c", "standard");
	renew_exits();
	return 1;
		
}
