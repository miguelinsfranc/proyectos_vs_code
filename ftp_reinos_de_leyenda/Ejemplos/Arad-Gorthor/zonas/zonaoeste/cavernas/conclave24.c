//Rezzah 10/11/2009
//Grimaek 28/05/2023 Sustituyo la firma "Zhariagh eztubo aqi" por pistas para el portal
#include "../../../path.h"
inherit "/std/subterraneo.c";

void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "portal");
	add_exit(NO, CAVERNAS+"conclave22.c","standard");
	add_exit(O, CAVERNAS+"conclave23.c", "standard");
	add_exit(N, CAVERNAS+"conclave25.c", "standard");
}

mixed *descifrar_inscripcion(string objeto_inscrito,object erudito)
{
   if(objeto_inscrito=="marca" || objeto_inscrito=="inscripcion" || objeto_inscrito=="techo" || objeto_inscrito=="peculiar")
      return ({"%^BOLD%^WHITE%^Burz sabe todos los secretos.%^RESET%^\n",4,"negra"});
   return 0;
}
