//Rezzah 10/11/2009

#include "../../../path.h"


inherit CAVERNAS+"base_poblado.c";

void setup() {
   GENERADOR_CONCLAVE->iniciar(this_object(), "poblado");
	add_exit(O, CAVERNAS+"conclave10.c","standard");
	add_exit(S, CAVERNAS+"conclave13.c","standard");
	add_exit(NE, CAVERNAS+"conclave14.c", "standard");
	add_exit(E, CAVERNAS+"conclave15.c", "standard");
	add_exit(SE, CAVERNAS+"conclave16.c", "standard");
    add_exit(N, CAVERNAS+"conclave11.c", "standard");
}

