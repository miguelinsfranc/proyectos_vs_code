//Rezzah 10/11/2009

#include "../../../path.h"
inherit CAVERNAS+"base_conclave.c";

status dame_teleportar() { return 0; }

void setup() {
	::setup();
   GENERADOR_CONCLAVE->iniciar(this_object(), "conclave");
	add_exit(O, CAVERNAS+"conclave93.c", "standard");
	add_exit(NO, CAVERNAS+"conclave91.c", "standard");
	add_exit(SO, CAVERNAS+"conclave96.c", "standard");
	add_timed_property("no_limpiable",1,4*3600);
    add_timed_property("no_clean_up",1,4*3600);	
}

