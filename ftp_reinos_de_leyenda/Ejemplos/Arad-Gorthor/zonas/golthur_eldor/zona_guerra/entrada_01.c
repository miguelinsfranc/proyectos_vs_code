inherit "/std/outside.c";
#include "../../../path.h";
#include <guardias.h>
#include <conquistas.h>
#include <depuracion.h>

//#include PASO_GENERAL

void setup() {
	
	
	add_exit(NO, ZGUERRA+"empalizada_02","standard");
	add_exit(O, ZGUERRA+"campamento_02","standard");
	add_exit(SO, ZGUERRA+"empalizada_07","standard");
	add_exit(E, ZGUERRA+"buenos_00","standard");
	modify_exit(NO,({"funcion","pasar"}));
	modify_exit(O,({"funcion","pasar"}));
	modify_exit(SO,({"funcion","pasar"}));
	
	PREPARAR_GUARDIAS_DE(DAME_CONQUISTADOR_ZONA("boreal"), "entrada_este", TO);
	//PREPARAR_GUARDIAS_DE("golthur", "entrada_este", TO);	
	
    AUTOGENERAR;
	
	//__debug_x(sprintf("HANDLER_ORGANIZACIONES: %s, DAME_CONQUISTADOR_ZONA: %s, BANDO_CONQUISTADOR: %s\n",HANDLER_ORGANIZACIONES,DAME_CONQUISTADOR_ZONA("boreal"),BANDO_CONQUISTADOR),"paso_arad");
}

#include PASO_GENERAL
