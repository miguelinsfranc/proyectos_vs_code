inherit "/std/outside.c";
#include "../../../path.h";
#include <guardias.h>
#include <conquistas.h>

void setup() {
	
	add_exit(O, ZGUERRA+"malos_29","standard");
	add_exit(NE, ZGUERRA+"empalizada_00","standard");
	add_exit(E, ZGUERRA+"campamento_00","standard");
	add_exit(SE, ZGUERRA+"empalizada_03","standard");
	modify_exit(NE,({"funcion","pasar"}));
	modify_exit(E,({"funcion","pasar"}));
	modify_exit(SE,({"funcion","pasar"}));
    AUTOGENERAR;
	
	PREPARAR_GUARDIAS_DE("/d/arad-gorthor/handlers/conquistas_boreal.c"->dame_conquistador(), "entrada_este", TO);
	//PREPARAR_GUARDIAS_DE("golthur", "entrada_este", TO);	
}
#include PASO_GENERAL