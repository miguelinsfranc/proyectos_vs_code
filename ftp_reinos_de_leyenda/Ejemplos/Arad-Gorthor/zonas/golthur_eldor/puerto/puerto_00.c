inherit "/std/outside.c";
#include "../../../path.h";

void setup() {
	
    add_exit(O, PUERTO+"taberna","standard");
	add_exit(NO, PUERTO+"muelle","standard");
	add_exit(SO, PUERTO+"entrada","standard");
	add_exit(E, PUERTO+"tienda", "door");
	setup_door(E, 0, 0);
	modify_exit(E, ({"tamaño",7}));
    AUTOGENERAR;
}
