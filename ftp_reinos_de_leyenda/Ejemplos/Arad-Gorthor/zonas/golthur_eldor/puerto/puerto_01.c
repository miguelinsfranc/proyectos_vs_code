inherit "/std/outside.c";
#include "../../../path.h";

void setup() {
	
	add_exit(NE, PUERTO+"muelle","standard");
	add_exit(E, PUERTO+"taberna","standard");
	add_exit(SE, PUERTO+"entrada","standard");
	add_exit(O, PUERTO+"astillero", "door");
	setup_door(O, 0, 0);
	modify_exit(O, ({"tamaño",7}));
    AUTOGENERAR;
}
