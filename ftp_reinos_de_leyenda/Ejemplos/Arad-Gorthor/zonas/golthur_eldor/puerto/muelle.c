inherit "/std/muelle";  
#include "../../../path.h";

void setup() {
	
	add_exit(S, PUERTO+"taberna","standard");
	add_exit(SE, PUERTO+"puerto_00","standard");
	add_exit(SO, PUERTO+"puerto_01","standard");
    AUTOGENERAR;
}

void crear_cartel()
{
	object cartel;
	::crear_cartel();
	cartel=present("cartel",this_object());
	cartel->set_short("Cartel rodeado de Cráneos");
	cartel->set_long("Este cartel se encuentra decorado, al igual que la entrada al embarcadero, de cráneos"
					 " de esclavos y pobres desgraciados que tuvieron en desgracia cruzarse con alguna"
					 " de las formidables embarcaciones de la armada de Golthur. ");
}
