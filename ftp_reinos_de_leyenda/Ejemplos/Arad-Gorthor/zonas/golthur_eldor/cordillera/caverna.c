inherit "/std/underground.c";
#include "../../../path.h";
#define PREMIO1 "/baseobs/minerales/hierro.c"
#define PREMIO2 "/baseobs/misc/cepo_desmontado.c"


void setup() {
	
	add_exit("hueco", CORDILLERA+"paso_18","standard");
	modify_exit("hueco",({"entrar","un hueco en la pared"}));
	modify_exit("hueco",({"tamaño",4}));
	
    AUTOGENERAR;
	
	add_item(({"alimaña","alimañas"}),"No ves ninguna, ¿no te ha quedado claro?");
	add_item(({"artefactos","cachivaches","escombros"}),"Numerosos objetos amontonados, usados para "
		"la creación de todo tipo de cosas. Quizás puedes removerlos y encontrar algo de valor.");
	add_item(({"herramienta","herramientas"}),"Un martillo, una tenaza, un cuchillo afilado, "
		"solo estas tres, aunque parecen encontrarse en muy buen estado y ser de una excelente calidad.");
	add_item(({"alimaña","alimañas"}),"No ves ninguna, ¿no te ha quedado claro?");
	
}

void init()
{
	::init();
	add_action("remover", ({"remover", "rebuscar"}));
}

int remover(string str)
{
	int tirada;
	object premio;
	
	//artefactos,cachivaches,escombros 
	if (!str || -1==member_array(str,({"artefactos","cachivaches","escombros"}))) {
		return notify_fail("¿"+capitalize(query_verb())+" qué?\n", TP);
	}
	
	if (secure_present(NPCS+"karghul", ENV(TP))) { 
		   return notify_fail("El goblin que tienes delante jamás te dejara acercarte a sus cosas.\n", TP);
	}
	
	if(TP->query_timed_property("artefactos_removidos"))
		return notify_fail("Hace poco tiempo que buscastes entre los artefactos y no hay nada nuevo de valor.\n", TP);
	
	
	tirada = 1+random(100);
	
	if (tirada<11) //10 %
	{
		tell_object(TP, "Remueves los "+str+" sin encontrar nada de valor.\n");
		tell_accion(ENV(TP), TP->query_short()+" remueve los "+str+" y no parece muy satisfecho.\n", "Escuchas ruidos metálicos en la oscuridad", ({ TP }), TP);
		TP->add_timed_property("artefactos_removidos",1,86400);// un dia
		return 1;
	}
	if (tirada>65)
	{
		tell_object(TP, "Remueves los "+str+" y encuentras un "+PREMIO1->query_short()+" que decides guardarte.\n");
		tell_accion(ENV(TP), TP->QCN+" remueve los "+str+" y se guarda un "+PREMIO1->query_short()+".\n", "Escuchas ruidos metálicos en la oscuridad", ({ TP }), TP);
		
		if (!premio = clone_object(PREMIO1)) return notify_fail("Se ha producido un error al clonar el objero, por favor notifícalo con el comando error.\n", TP);
		premio->move(TP);
		
		TP->add_timed_property("artefactos_removidos",1,86400);// un dia
        DAR_TOKEN(TP, "boreal", "caverna_remover_premio1");
		return 1;
	}
	
		tell_object(TP, "Remueves los "+str+" y encuentras un "+PREMIO2->query_short()+" que decides guardarte.\n");
		tell_accion(ENV(TP), TP->QCN+" remueve los "+str+" y se guarda un "+PREMIO2->query_short()+".\n", "Escuchas ruidos metálicos en la oscuridad", ({ TP }), TP);
		if (!premio = clone_object(PREMIO2)){
            return notify_fail("Se ha producido un error al clonar el objero, por favor notifícalo con el comando error.\n", TP);
        }
        if ( ! TP->entregar_objeto(premio) ) {
            return notify_fail("Ocurrió un error al entregarte tu ansiado tesoro, por favor notifícalo con el comando error..\n",TP);
        }
        DAR_TOKEN(TP, "boreal", "caverna_remover_premio2");
		TP->add_timed_property("artefactos_removidos",1,86400);// un dia
	
	return 1;
}