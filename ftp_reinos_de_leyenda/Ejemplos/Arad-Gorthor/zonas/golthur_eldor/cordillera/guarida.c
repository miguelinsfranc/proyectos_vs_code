#include "../../../path.h";
inherit "/std/outside.c";


void setup() {
	
	add_exit(N, CORDILLERA+"paso_32","standard");
	add_exit(S,"/d/anduar/rooms/anduar/murallas/ruinmurnor_8","standard");
	
	AUTOGENERAR;
	
	set_long(TO->query_long()+" En este apartado del camino te encuentas que alguién ha "
	"levantando un sencillo asentamiento. Una gigantesca piedra que "
	"sobresale del borde de las rocas ha sido cercada con trozos de madera  "
	"obtenidos de las máquinas de asedio de algún lugar cercano para formar una pequeña "
	"guarida. Frente a este cúbil hay un enorme poste.\n");
	
	add_item("poste", "Hay restos de sangre por doquier. Parece ser un poste "
	"de ejecución. También hay restos de cuerda a su alrededor... quizás sea "
	"usado para atar a la gente?\n");
	add_item("piedra", "Es una piedra que sobresale dos metros por la pared "
	"de la sima, con una anchura de un metro y medio. No parece haber sido "
	"manipulada de ninguna manera, tratándose así de un accidente natural.\n");
	add_item("guarida", "La única utilidad práctica que ves a este adefesio "
	"es la de resguardarse de la lluvia. Pero amigos míos, aquí nunca llueve.\n");
	
	
	add_clone(NPCS+"mercader_anduar", 1);
	add_clone(NPCS+"picaro_osgo", 3);
}
