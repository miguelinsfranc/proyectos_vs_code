inherit "/std/outside.c";
#include "../../../path.h";

void setup() {
	
	add_exit(O, CORDILLERA+"paso_17","standard");
	add_exit(E, CORDILLERA+"paso_19","standard");
	add_exit("hueco", CORDILLERA+"caverna","hidden");
	modify_exit("hueco",({"entrar","un hueco en la pared"}));
	modify_exit("hueco",({"tamaño",4}));
    AUTOGENERAR;
	
	call_out("recomponer_long",0);
	add_item(({"hueco","huecos"}),"Un hueco en la pared de la roca, podrías pasar por el si lo intentas.");
	
}

void recomponer_long(){
	 
 set_long(query_long()+"Parece existir un pequeño hueco en una de las paredes de roca del paso.\n");
 
 }