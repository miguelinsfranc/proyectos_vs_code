// Altaresh Mayo19
inherit "/std/outside";
#include "../../../path.h";

void setup() {
    add_exit(NO, ACANTILADO+"senda_22","standard");
	add_exit(E, "/d/eldor/rooms/caminos/senda_ubonihs/arboleda/arboleda_86.c","standard");
    AUTOGENERAR;
	
	add_sign("Un gran cartel de madera podrida.\n",
	"===================================\n"
	"|                                 |\n"
	"| Oeste: Zona de Guerra           |\n"
	"|                                 |\n"
	"| Este: Bosque Oscuro, Eldor      |\n"
	"|                                 |\n"
	"|=================================|\n"
	"\n",
	"Cartel de madera",
	({"cartel","madera"}),
	"adurn"
	);
}

