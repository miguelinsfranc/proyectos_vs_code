inherit "/std/outside.c";
#include "../../../path.h";

void setup() {
	
	add_exit(S, RBOREAL+"cordillera_05","standard");
	add_exit(AB, PUERTO+"entrada","standard");
	add_exit(E, ZGUERRA+"malos_00","standard");
    AUTOGENERAR;
	
	add_sign("Una Piel posiblemente de humanoide seca y estirada por varios listones de madera.\n",
	"===================================\n"
	"|        Juggash Bur              |\n"
	"|                                 |\n"
	"| Este: Zona de Guerra            |\n"
	"|                                 |\n"
	"| Sur:  Golthur Orod              |\n"
	"|                                 |\n"
	"|=================================|\n"
	"\n",
	"Piel estirada",
	({"piel","cartel","estirada"}),
	"negra"
	);
}
