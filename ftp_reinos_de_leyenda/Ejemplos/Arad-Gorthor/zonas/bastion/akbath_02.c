// Made by Dunkelheit  11/19/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

inherit "/std/outside";
#include "../../path.h"

void setup() {

   // set_exit_color("white");
   // set_light(90);
   GENERADOR_BASTION->describir(this_object(), "akbath_oeste");
   // set_long("LONG");
add_exit(O,BASTION+"akbath_01.c","road");
add_exit(N,BASTION+"suburbios_12.c","road");
add_exit(SE,BASTION+"akbath_03.c","road");
}
