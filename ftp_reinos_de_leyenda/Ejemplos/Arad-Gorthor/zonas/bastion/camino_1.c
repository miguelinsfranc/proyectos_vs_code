// Dunkelheit 19-12-2009

inherit "/std/outside";
#include "../../path.h"

void setup()
{
	GENERADOR_BASTION->describir(this_object(), "exterior_atalaya_norte");

	add_exit(S, BASTION+"puerta_oeste.c", "path");
	add_exit("saetera", BASTION+"almena_norte", "escalable");
}
