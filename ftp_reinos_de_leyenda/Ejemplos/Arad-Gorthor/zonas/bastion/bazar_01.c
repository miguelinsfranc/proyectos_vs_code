// Made by Dunkelheit  11/19/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

inherit "/std/outside";
#include "../../path.h"

void setup() {

   // set_exit_color("white");
   // set_light(90);
   GENERADOR_BASTION->describir(this_object(), "bazar");
   // set_long("LONG");
add_exit(O,BASTION+"bazar_02.c","road");
add_exit(NO,BASTION+"bazar_04.c","road");
add_exit(E,BASTION+"bazar_00.c","road");
add_exit(SE,BASTION+"herejes.c","road");
}
