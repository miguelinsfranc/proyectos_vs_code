// Dunkelheit 24-11-2009

inherit "/std/templo";
#include "../../path.h"

void setup()
{
	GENERADOR_BASTION->describir(this_object(), "templo");

	add_exit(S, BASTION+"plaza", "stairs");
	
	fijar_deidad("gurthang");
	fijar_presencia(3);
}
