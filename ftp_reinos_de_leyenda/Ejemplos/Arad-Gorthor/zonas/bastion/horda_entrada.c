// Made by Dunkelheit  11/19/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

inherit "/std/outside";
#include "../../path.h"

int pasar(string salida,object caminante,string msj_especial){

	return notify_fail("El Bastión de la Horda Negra fue abandonado hace años, cuando "
								"los seguidores de Gurthang crearon sedes de la Horda en sus respectivas ciudades. "
								"Ya no queda nada aquí.\n");
}

void setup() {

   // set_exit_color("white");
   // set_light(90);
   GENERADOR_BASTION->describir(this_object(), "horda_entrada");
   // set_long("LONG");
add_exit(N,BASTION+"horda_00.c","road");
//modify_exit(N,({"funcion","pasar"}));
add_exit(S,BASTION+"akbath_09.c","road");
}
