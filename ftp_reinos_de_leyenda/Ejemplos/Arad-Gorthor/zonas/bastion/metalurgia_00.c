// Made by Dunkelheit  11/19/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

inherit "/std/outside";
#include "../../path.h"

void setup() {

   // set_exit_color("white");
   // set_light(90);
   GENERADOR_BASTION->describir(this_object(), "metalurgia");
   // set_long("LONG");
add_exit(O,BASTION+"metalurgia_02.c","road");
add_exit(N,BASTION+"metalurgia_entrada.c","road");
add_exit(E,BASTION+"metalurgia_04.c","road");
add_exit(S,BASTION+"metalurgia_01.c","road");
}
