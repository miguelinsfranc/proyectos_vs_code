// Dunkelheit 19-12-2009

inherit "/std/outside";
#include "../../path.h"

void setup()
{
	GENERADOR_BASTION->describir(this_object(), "exterior_murallas");

	add_exit(E, BASTION+"puerta_oeste.c", "path");
	add_exit(O, CEMENTERIO+"cementerio_8.c", "path");
	modify_exit(E,({"function","pasar"}));
}

int pasar(string salida,object caminante,string msj_especial) {

   return notify_fail("Los vigilantes de Arad-Gorthor te impiden pasar al bastión. Se está preparando una guerra y no quieren desconocidos en el interior.\n");

}