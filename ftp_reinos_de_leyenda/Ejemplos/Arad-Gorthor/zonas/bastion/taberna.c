// Dunkelheit 13-01-2010

#include "../../path.h"
#include <exploracion.h>

inherit "/std/taberna";

string *mensajes;

string *dame_mensajes() { return mensajes; }
void borrar_mensajes() { mensajes = ({ }); save_object(base_name()); }

void create()
{
	::create();
	
	mensajes = ({ });
	
	restore_object(base_name());
}

void setup()
{
	set_short("%^BLACK%^BOLD%^Cantina %^GREEN%^Escorbuto%^RESET%^");
	set_long("Estás en la taberna con más renombre de toda las sociedades Gurthang. Gracias "
	"al intenso tráfico de fieles y a la encomiable presencia -y sed de alcohol- de los "
	"integrantes de la Horda Negra, la cantina Escorbuto se ha convertido en un referente "
	"para los amantes del buen beber y el buen yantar. Además, puede presumir de ser la "
	"primera en realizar ofertas por temporadas y organizar eventos, cosa inusitada en el "
	"mundo de las tabernas de Eirea. Pese a tantos laureles, la cantina no dejaría de ser el "
	"típico tugurio orco ante los ojos de cualquier ser civilizado; por suerte los goblinoides "
	"tienen el don de apreciar las pequeñas diferencias que han llevado a Escorbuto a topar la "
	"cima de la restauración.\n"
	"Una puerta doble metálica abre paso a una extensa sala rectangular colmada de mesas "
	"redondas con pequeños taburetes -todos iguales y en una pieza, milagrosamente-. La barra "
	"está ubicada en medio del recinto y unida a la pared de la derecha, separando así la "
	"estancia en dos ambientes: uno malo y otro peor. El ambiente malo es donde se sientan "
	"aquellos que tienen prisa y sólo desean mojarse el gaznate, el ambiente peor es donde "
	"el tiempo se detiene en una barahúnda etílica de violencia, malas miradas y cuchillos "
	"enterrados en espaldas mojadas. No te acerques a este último si es tu primera visita.\n" 
	"Frente a la barra, y en el lado de la izquierda, hay una puerta que, a juzgar por el "
	"charco de orín que asoma bajo ella, conduce a las letrinas. El techo, abuhardillado, "
	"está segmentado por vigas de madera, una de ellas vencida y partida por la mitad. En "
	"sus alturas encontraron cobijo las ratas, zarigüeyas y un mastín rabioso. Es habitual "
	"ver a los parroquianos echando mano de ellas para saciar su apetito. Finalmente, y "
	"escondido bajo varias pilas de barriles, se encuentra lo que antaño fue un estrado, "
	"aunque su propósito te es desconocido. Desde luego, no era para recitar poesía.\n");
	add_item("barra", "La barra de este año nos deleita con una colección de borrachos y "
	"corazones solitarios cuyos codos resecos ya han echado telarañas en la superficie sucia "
	"y llena de alcohol de la barra. Bajo ella, una colección de mucosidades nasales y "
	"gargajos sirve de alimento a las ratas y cucarachas que han conquistado esta porción "
	"de la taberna.\n");
	add_item(({"pared", "paredes"}), "En lugar de fotos del dueño con celebridades, las paredes "
	"sirven de lienzo para los artistas del cuchillo y el escupitajo. Puedes leer mensajes de "
	"todo tipo -la mayoría obscenos o referentes a la madre de cierto parroquiano- y embelesarte "
	"con el arte abstracto de las composiciones de flema: escupitajos antediluvianos que han "
	"pasado a integrarse con los ladrillos de las paredes y que no saldrían ni a fuerza de "
	"cuchillo. No te quedes mirándolas mucho o el tabernero te cobrará.\n");
	add_item(({"techo", "tejado", "viga", "vigas"}), "Perteneciente a un mundo paralelo, el "
	"tejado es un lugar donde los parroquianos nunca miran... salvo cuando llueve. El agua que "
	"se filtra por las tejas hace que la fauna que habita entre las vigas (a saber: ratas, "
	"zarigüeyas y el famoso mastín rabioso) se asuste y salte al suelo, asustando a su vez "
	"a los clientes. Las estampidas de dichas ratas y zarigüeyas han causado ya cuantiosos "
	"daños materiales y numerosas bajas por las pelas entre éstas y las ratas de la barra, "
	"sus enemigas acérrimas.\n");
	add_item(({"mesa", "mesas"}), "Aunque suelen estar siempre llenas, en las mesas siempre "
	"puedes hacerte un hueco para degustar alguna de las cervezas del local. No es aconsejable "
	"intentarlo en las mesas del fondo, eso sí... la gente allí no es tan afable y no "
	"necesitan excusa alguna para liarse a mamporros.\n");
	add_item(({"taburete", "taburetes"}), "¿No es sorprendete que un arma arrojadiza en "
	"potencia como ésta siga intacta? Seguramente los sustituyan al cabo del día. Quién sabe.\n");
	add_item("estrado", "Está oculto bajo la pila de barriles vacíos, pero puedes distinguirlo "
	"con claridad. Es raro ver a bardos por estas tabernas. ¿En qué estarían pensando cuando lo "
	"montaron?\n");
	add_item(({"barril", "barriles"}), "Son barriles vacíos de cerveza, vino, o Gurthang sabe qué. "
	"No tienen nada de especial.\n");
	add_item(({"letrina", "letrinas"}), "Mejor no preguntes.\n");
	set_exit_color("rojo");
	fijar_luz(65);
	
	fijar_tabernero(NPCS + "trinchaelfas");
	
	add_exit(O, BASTION+"suburbios_03", "door", "madera", 10);
	
	nuevo_plato("Higo Podrido", "comida", 1);
	nuevo_alias("higo", "Higo Podrido");
	nuevo_alias("podrido", "Higo Podrido");

	nuevo_plato("Bayas Podridas", "comida", 1);
	nuevo_alias("bayas", "Bayas Podridas");
	
	nuevo_plato("Naranja Podrida", "comida", 1);
	nuevo_alias("naranjas", "Naranjas Podridas");

	nuevo_plato("Ortigas Rehogadas", "comida", 2);
	nuevo_alias("ortigas", "Ortigas Rehogadas");
	nuevo_alias("rehogadas", "Ortigas Rehogadas");
	
	nuevo_plato("Revuelto de Arañas", "comida", 4);
	nuevo_alias("revuelto", "Revuelto de Arañas");
	nuevo_alias("arañas", "Revuelto de Arañas");
	nuevo_alias("aranyas", "Revuelto de Arañas");
	
	nuevo_plato("Pescado de la Semana Pasada", "comida", 3);
	nuevo_alias("pescado", "Pescado de la Semana Pasada");
	nuevo_alias("semana", "Pescado de la Semana Pasada");
	nuevo_alias("pasada", "Pescado de la Semana Pasada");

	nuevo_plato("Rata en Salazón", "comida", 5);
	nuevo_alias("rata", "Rata en Salazón");
	nuevo_alias("salazon", "Rata en Salazón");
	
	nuevo_plato("Rata a la Plancha", "comida", 10);
	nuevo_alias("plancha", "Rata a la Plancha");

	nuevo_plato("Rata con Ajos", "comida", 12);
	nuevo_alias("ajos", "Rata con Ajos");

	nuevo_plato("Rata Gigante de la Ciénaga", "comida", 15);
	nuevo_alias("gigante", "Rata Gigante de la Ciénaga");
	nuevo_alias("cienaga", "Rata Gigante de la Ciénaga");

	nuevo_plato("Filete de Chacal", "comida", 11);
	nuevo_alias("filete", "Filete de Chacal");
	nuevo_alias("chacal", "Filete de Chacal");

	nuevo_plato("Jamón de Jabalí Negro", "comida", 8);
	nuevo_alias("jamon", "Jamón de Jabalí Negro");
	nuevo_alias("jabali", "Jamón de Jabalí Negro");
	nuevo_alias("negro", "Jamón de Jabalí Negro");

	nuevo_plato("Ojos de Trasgo con Guarnición", "comida", 20);
	nuevo_alias("ojos", "Ojos de Trasgo con Guarnición");
	nuevo_alias("trasgo", "Ojos de Trasgo con Guarnición");
	nuevo_alias("guarnicion", "Ojos de Trasgo con Guarnición");

	nuevo_plato("Cráneo de Enano", "comida", 45);
	nuevo_alias("craneo", "Cráneo de Enano");
	nuevo_alias("enano", "Cráneo de Enano");

	nuevo_plato("Cabeza de Cíclope", "comida", 50);
	nuevo_alias("cabeza", "Cabeza de Cíclope");
	nuevo_alias("ciclope", "Cabeza de Cíclope");
	
	nuevo_plato("Orines du Letrine", "bebida", 1);
	nuevo_alias("orines", "Orín du Letrine");
	nuevo_alias("letrine", "Orín du Letrine");

	nuevo_plato("Sangre de Murciélago", "bebida", 2);
	nuevo_alias("sangre", "Sangre de Murciélago");
	nuevo_alias("murcielago", "Sangre de Murciélago");
	
	nuevo_plato("Cerveza Blanca", "alcohol", 2);
	nuevo_alias("cerveza", "Cerveza Blanca");
	nuevo_alias("blanca", "Cerveza Blanca");
	
	nuevo_plato("Cerveza Roja", "alcohol", 4);
	nuevo_alias("roja", "Cerveza Blanca");
	
	nuevo_plato("Cerveza Negra", "alcohol", 7);
	nuevo_alias("negra", "Cerveza Blanca");

	nuevo_plato("Licor del Desierto", "alcohol", 10);
	nuevo_alias("licor", "Licor del Desierto");
	nuevo_alias("desierto", "Licor del Desierto");
	fijar_ciudadania("golthur");
	
	crear_cartel(0);
}

void init()
{
	::init();
	add_action("mirar",  "mirar");
	add_action("escribir", "escribir");
}

int mirar(string str)
{
	str = lower_case(str);
	if (regexp(str, "mensaje")) {
		if (sizeof(mensajes)) {
			tell_object(this_player(), "Echas un vistazo por los mensajes y encuentras uno que "
			"te llama la atención:\n\n  "+element_of(mensajes) + "\n\nSi tienes un cuchillo "
			"a mano, tal vez puedas %^WHITE%^BOLD%^escribir%^RESET%^ un mensaje.\n");
		} else {
			tell_object(this_player(), "No ves ningún mensaje que te llame la atención. Tal vez "
			"sea buena idea darles a los parroquianos una lección de poesía. ¿Por qué no buscas "
			"un cuchillo y pruebas a %^WHITE%^BOLD%^escribir%^RESET%^ un mensaje?\n");
		}
		this_player()->consumir_hb();
		return 1;
	}
	return 0;
}

int escribir(string str)
{
	if (!str || str == "") {
		return notify_fail("¿Qué mensaje quieres escribir?\n", this_player());
	}
	
	if (this_player()->query_timed_property("mensaje_escorbuto")) {
		return notify_fail("Hace muy poco que has escrito un mensaje. Será mejor no llamar mucho "
		"la atención o el cantinero podría abrirte la cabeza.\n", this_player());
	}
	
	if (!secure_present(BARMAS + "cuchillo", this_player())) {
		return notify_fail("Con esas uñas tal vez puedas intentarlo... pero yo que tú me agenciaría "
		"un cuchillo.\n", this_player());
	}
	
	if (strwidth(str) > 140) {
		return notify_fail("¿Qué quieres, escribir la biblia de Eralie? ¡sé más escuet"+this_player()->dame_vocal()+"!\n", this_player());
	}
	
	tell_object(this_player(), "Aprovechando que todo el mundo está mirando, plasmas tu perla creativa "
	"en las paredes de la cantina. ¡Estás hech"+this_player()->dame_vocal()+" tod"+this_player()->dame_vocal()+" "+this_player()->dame_numeral()+" poeta!\n");

	TOKEN(this_player());
	
	this_player()->consumir_hb();
	this_player()->add_timed_property("mensaje_escorbuto", 1, 600);
	
	mensajes += ({ str + "\n    --"+this_player()->query_cap_name() });
	
	write_file(LOGS+"mensajes_escorbuto", ctime()+" "+this_player()->query_cap_name()+": "+str+"\n");
	
	save_object(base_name());
	
	return 1;
}

void dest_me()
{
	save_object(base_name());
	
	::dest_me();
}

