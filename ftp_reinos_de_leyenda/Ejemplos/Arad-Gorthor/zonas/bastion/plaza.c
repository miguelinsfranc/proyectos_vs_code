// Dunkelheit 24-11-2009

inherit "/std/outside";
#include "../../path.h"

void setup()
{
	GENERADOR_BASTION->describir(this_object(), "plaza");

	add_exit(N, BASTION+"templo.c", "stairs");
	add_exit(O, BASTION+"akbath_05.c", "road");
	add_exit(E, BASTION+"akbath_07.c", "road");
	add_exit(SO,BASTION+"brujeria_entrada.c","road");
	add_exit(SE,BASTION+"metalurgia_entrada.c","road");	
	
	modify_exit("templo", ({"mensaje", ({ "$N sube las escaleras que conducen hasta el templo.", "$N llega desde el exterior del templo." }) }));
}

int chequeo_salida(string str, string verbo, object quien, object lider)
{
	if (verbo == "templo" && !quien->query_timed_property("lockout_mensaje_templo")) {
		quien->add_timed_property("lockout_mensaje_templo", 10);
		tell_object(quien, "Subes las escaleras que conducen al templo de Lord Gurthang.\n");
	}
	
	return 1;
}
