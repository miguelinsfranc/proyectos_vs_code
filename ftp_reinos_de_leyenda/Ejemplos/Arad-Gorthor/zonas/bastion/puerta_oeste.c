// Dunkelheit 19-12-2009

inherit "/std/outside";
#include "../../path.h"

status defensa = 0;

status defensa_activada()
{
	return defensa;
}

void activar_defensa()
{
	if (defensa) {
		return;
	}

	defensa = 1;
	set_heart_beat(3);
}

void heart_beat()
{
	adjust_property("heartbeat", 1);
	switch (query_property("heartbeat"))
	{
		case 1:
			tell_room(this_object(), "%^YELLOW%^Las tropas de Arad Gorthor se preparan para la batalla alentadas por el retumbar de los tambores.%^RESET%^\n");
			break;
		case 2:
			tell_room(this_object(), "%^YELLOW%^Una enorme figura roja se divisa en el interior de la ciudad fortificada, avanzando hacia las puertas entre una enorme polvareda.%^RESET%^\n");
			break;
		case 3:
			tell_room(this_object(), "%^YELLOW%^¡Un demonio Balor llega de entre la muchedumbre de orcos y goblins para defender la ciudad de Arad Gorthor de sus invasores!%^RESET%^\n");
			if (load_object(NPCS + "balor")) {
				object balor = clone_object(NPCS + "balor");
				balor->move(this_object());
				foreach (object ob in filter_array(all_inventory(this_object()), (: living :))) {
					balor->do_aggressive_check(ob);
				}
			}
			set_heart_beat(0);
			break;
	}
}

void setup()
{
	GENERADOR_BASTION->describir(this_object(), "puerta_oeste");

	add_exit(O, BASTION+"camino_0.c", "path");
	add_exit(N, BASTION+"camino_1.c", "path");
	add_exit(S, BASTION+"camino_2.c", "path");
	add_exit(E, BASTION+"explanada_oeste.c", "door");
	modify_exit(E, ({"tamaño", 15 }));
	modify_exit(E, ({"funcion", "pasar" }));
	adjust_open_door(E, 0, 0);

	add_clone(NPCS + "korg", 1);
}

int chequeo_adicional(string salida, object caminante, object guardia)
{
	if (caminante->dame_religion() != "gurthang") {
		return 0;
	}
	
	return 1;
}

int chequeo_abrir_puerta(string direccion, object quien)
{
	if (quien->query_creator()) {
		return 0;
	}
	tell_object(quien, "¿Pero tú estás mal de la cabeza? ¡estas puertas pesan TONELADAS!\n");
	return 1;
}

int chequeo_cerrar_puerta(string direccion, object quien)
{
	if (quien->query_creator()) {
		return 0;
	}
	tell_object(quien, "¿Pero tú estás mal de la cabeza? ¡estas puertas pesan TONELADAS!\n");
	return 1;
}

void event_enter(object quien, string mensaje, object procedencia, object *seguidores)
{
	object ballestero;
	
	if (!quien || !quien->query_player()) {
		return;
	}
	
	if (ballestero = secure_present(NPCS+"ballestero_horda", load_object(BASTION+"almena_norte"))) {
		ballestero->do_aggressive_check(quien);
	}

	if (ballestero = secure_present(NPCS+"ballestero_horda", load_object(BASTION+"almena_sur"))) {
		ballestero->do_aggressive_check(quien);
	}
}

#define GUARDIA "guardia_arad"
#define DIPLOMACIA_MINIMA PAZ
#define CIUDADANIA ({"Arad Gorthor", "Golthur Orod", "Ancarak", "Mor Groddûr"})
#define CIUDADANIAS_DEFENSORAS ({"arad_gorthor", "golthur", "ancarak", "mor_groddur"})
#define AGRESIVIDAD
#define MSJ "Las Puertas de la Guerra están blindadas por las tropas de Lord Gurthang, ¡tendrás que derrotarles si quieres pasar!\n"
#define MSJ_ROOM caminante->query_short()+" es interceptad"+caminante->dame_vocal()+" por las tropas defensivas de Arad Gorthor.\n"
#define SIN_SALUDOS
#include <paso.h>
