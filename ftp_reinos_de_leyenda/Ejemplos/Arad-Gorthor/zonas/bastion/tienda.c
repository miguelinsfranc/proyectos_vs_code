// Dunkelheit 17-01-2010

#include "../../path.h"
inherit "/std/tienda";

void setup()
{
	set_short("%^GREEN%^BOLD%^Todo Ahorro%^RESET%^");
	set_long("Te encuentras en una pequeña tienda desorganizada en el más estúpido rincón de los suburbios"
	" de Arad Gorthor. Montones de objetos, seguramente robados en escaramuzas a lo largo de la historia "
  "de su viejo propietario. Unas buen número de velas iluminan la estancia para que su propietario, que "
	"quita el ojo de encima a ningún visitante, pueda evitar los robos, tan frecuentes en esta zona.\n");
	add_item(({"velas"}),	"Están repartidas por toda la sala, para que no queden sombras donde trapichear sin el conocimiento del dueño.\n");

	set_exit_color("amarillo");
	fijar_luz(75);
	
	fijar_tipo_tienda("venta");
	fijar_tendero(NPCS + "galin");
	
	add_static_property("no_silencio", 1);
	
	poner_lista_ajustes(([
		"Gurthang": 1,
		"humano": 99,
	]));
	
	fijar_tabla_objetos(([
		BMISC+"abrojo": random(3),
		BMISC+"caña": 1,
		BARMAS+"lanza":2,
		BMISC+"venda": 3+random(5),
		BARMADURAS+"mochila":random(2),
		BMISC+"cubo":1,
		BMISC+"garfio_escalada":random(3)+1,
		BMISC+"pipa":1,
		BARMADURAS+"camiseta":random(6),
		BARMADURAS+"guante_izq":random(3),
		BESCUDOS+"rodela":1,
		BARMADURAS+"bolsita":1,
		BARMADURAS+"zapatos":1,
		BMISC+"odre":1+random(2),
		BARMADURAS+"cinturon_anilla":random(2),
		BARMADURAS+"falda":1,
		BARMADURAS+"brazal":random(2),
		BMISC+"antorcha":1+random(2),
		BARMADURAS+"brazal_izq":random(2),
		BARMAS+"latigo":1,
		BARMAS+"sable":random(3),
		BARMADURAS+"zapatillas":1,
		BARMAS+"pala":random(3),
		BARMAS+"cimitarra_larga":random(2),
		BARMADURAS+"guante":random(3),
		BARMAS+"cadena":random(2),
		BARMAS+"tridente":random(2),
		BESCUDOS+"escudo_corporal":1,
		BMISC+"cuerda": random(3)+1,
	]));
	
	add_exit(E, BASTION+"suburbios_00", "door");
}
