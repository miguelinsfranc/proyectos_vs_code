// Satyr 2012
//inherit "/std/salas/votaciones_organizacion.c";
inherit "/std/room";
#include "../../path.h"
void setup()
{
set_short("Sala del Caudillo de la Horda Negra.");
set_long("La sala del Caudillo, mejor conocida como el Ojo Odioso, es una estructura de piedra asentada sobre poderosas columnas de viejos troncos petrificados por el paso de años y más años. Sus retorcidas raíces permitieron crear una superficie horizontal fuerte y de aspecto pesado, reflejo de un poder basado en la pura fuerza y la superioridad de Gurthang sobre todas las cosas. Por razón de estas originales columnas la forma es ovoide vista desde el exterior contrastando fuertemente con la arquitectura de los edificios colindantes pero dotando de presencia y personalidad a esta torre. Un pequeño agujero, por el que apenas se puede ver el exterior, se dibuja en una pared orientada al sudoeste. Su forma cónica, más ancha en el exterior que en el interior da la sensación, cuando se examina la torre desde fuera de la misma, de tratarse del iris y la pupila de un gigantesco ojo que todo lo controla. Este hecho, casual en principio, se convirtió en un inquietante sentimiento opresor en todos los habitantes del bastión, que sienten continuamente la mirada de la torre en sus espaldas haciéndoles moverse con mayor viveza y no parar jamás a la vista de ese ojo. Algunos creen que el ojo incluso se gira y mira, a veces más al sur, hacia el bastión de los infieles, otras veces al sudeste, para no perder de vista a los siempre traidores.\n");
//    fijar_ruta_organizacion("gremios/horda_negra_org.c");
    
    // nº de votos, rango minimo necesario, mensaje a mostrar (0 o no muestra nada)
//    poner_votaciones_genericas_comunes(1, 3, 0);     // votación de 'cambiar grito'
//    poner_votaciones_genericas_iglesia(5, 3, 0);     // comulgar y excomulgar en gurthang
//    poner_votaciones_genericas_gremio( 2, 2, 0);      // Expulsar, amonestar...
//    crear_cartel("negra");
add_exit(AB, BASTION+"horda_05.c", "stairs");
}
//string dame_religion_comunion() { return "gurthang"; }
