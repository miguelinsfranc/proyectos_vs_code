// Dunkelheit 19-11-2009

#include "../../path.h"
inherit "/std/outside";

void setup() 
{
	GENERADOR_BASTION->describir(this_object(), "explanada_oeste");
	add_exit(O, BASTION+"puerta_oeste.c", "door");
	modify_exit(O, ({"tamaño", 15}));
	adjust_open_door(O, 0, 0);
	add_exit(N, BASTION+"atalaya_norte.c", "road");
	add_exit(E, BASTION+"akbath_00.c", "road");
	add_exit(S, BASTION+"atalaya_sur.c", "road");
}
int chequeo_abrir_puerta(string direccion, object quien)
{
    if (quien->query_creator()) {
        return 0;
    }
    tell_object(quien, "¿Pero tú estás mal de la cabeza? ¡estas puertas pesan TONELADAS!\n");
    return 1;
}

int chequeo_cerrar_puerta(string direccion, object quien)
{
    if (quien->query_creator()) {
        return 0;
    }
    tell_object(quien, "¿Pero tú estás mal de la cabeza? ¡estas puertas pesan TONELADAS!\n");
    return 1;
}
