// Made by Dunkelheit  11/19/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

inherit "/std/outside";
#include "../../path.h"

void setup() {

   // set_exit_color("white");
   // set_light(90);
   GENERADOR_BASTION->describir(this_object(), "atalaya_sur_base");
   // set_long("LONG");
add_exit(N,BASTION+"explanada_oeste.c","road");
add_exit(AR, BASTION+"atalaya_sur_cima.c", "stairs");
}
