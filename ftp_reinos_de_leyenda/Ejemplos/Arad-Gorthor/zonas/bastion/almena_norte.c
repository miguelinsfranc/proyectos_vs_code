// Dunkelheit 19-12-2009

inherit "/std/outside";
#include "../../path.h"

string *dame_rooms_apuntables()
{
	return ({
		BASTION + "puerta_oeste",
		BASTION + "camino_1",
	});
}

void configurar_ballestero(object ob)
{
	ob->fijar_rooms_apuntables(dame_rooms_apuntables());
}

void setup()
{
	GENERADOR_BASTION->describir(this_object(), "interior_atalaya_norte");

	add_exit(AB, BASTION+"camino_1", "escalable");
	fijar_altura(10);
	
	add_clone(NPCS + "ballestero_horda", 1, (: configurar_ballestero :));
}
