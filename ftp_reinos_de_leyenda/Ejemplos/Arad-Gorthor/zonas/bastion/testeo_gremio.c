// Satyr 2012
inherit "/std/salas/votaciones_organizacion.c";
void setup()
{
    set_short("Pruebas de sala de la horda negra");    
    fijar_ruta_organizacion("gremios/horda_negra_org.c");
    
    // nº de votos, rango minimo necesario, mensaje a mostrar (0 o no muestra nada)
    poner_votaciones_genericas_comunes(1, 3, 0);     // votación de 'cambiar grito'
    poner_votaciones_genericas_iglesia(5, 3, 0);     // comulgar y excomulgar en gurthang
    poner_votaciones_genericas_gremio( 1, 2, 0);      // Expulsar, amonestar...
    crear_cartel("negra");
}
string dame_religion_comunion() { return "gurthang"; }