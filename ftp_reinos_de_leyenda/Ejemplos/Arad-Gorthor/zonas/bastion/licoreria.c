// Dunkelheit 17-01-2010

#include "../../path.h"
inherit "/std/tienda";

void setup()
{
	set_short("%^BLACK%^BOLD%^Licorería \"El Hígado Necrótico\"%^RESET%^");
	set_long("Te encuentras en una diminuta tienda alumbrada por un somero "
	"quinque apostado en el mostrador, cuya crepitante vela proyecta haces "
	"que rebotan inoportunamente en las cientos de botellas que te rodean, "
	"creando un ambiente onírico, hasta el punto de que da la impresión de "
	"estar bajo los influjos del alcohol nada más entrar a la licorería.\n"
	"Nada más cruzar la pequeña puerta es menester bajar varios escalones "
	"de madera, ya que la tienda se encuentra en una especie de semi-sótano. "
	"Las cuatro paredes de esta estancia cuadrada apenas superan los dos "
	"metros y medio de longitud, y están colmadas por estanterías repletas "
	"de botellas. Espejos en los fondos de las estanterías causan una ilusión "
	"de saturación de productos, pareciéndote estar nadando en un mar etílico. "
	"Frente a la puerta hay un pequeño mostrador hueco de madera tras el cual "
	"se esconde un goblin con anteojos de aspecto intelectual y... sádico. \""
	"El Hígado Necrótico\" es posiblemente la licorería más especializada de "
	"Dalaensar, tanto que incluso enemigos del entente goblinoide han hecho "
	"negocios con su propietario y regente.\n");
	add_item("escalones", "Son tres escalones de madera que descienden hacia "
	"el semi-sótano en el que se encuentra la licorería. Son bastante ruidosos.\n");
	add_sound("escalones", "¡Grrrrñieeeeek!\n");
	add_item("quinque", "Un quinque metálico se encuentra a la derecha del "
	"del mostrador y es la única luz que alumbra la licorería. Como bien notaste "
	"anteriormente, su luz titilante inunda la estancia de onirismo.\n");
	add_item(({"estanterias", "botellas", "bebida", "bebidas"}), "Desde el suelo "
	"hasta el techo, las estanterías están repletas de botellas de lo más "
	"variopinto. La mayoría de ellas son de tonos dorados y marrones, tanto por "
	"su contenido como por la coloración del cristal, aunque también abundan "
	"las verdosas y las transparentes. Las de color verde tienen pinta de ser "
	"las más insalubres.\n");
	add_item(({"botellas verdes","verdes"}), "La mayoría son de Áspex, el licor "
	"típico de Golthur Orod.\n");

	set_exit_color("amarillo");
	fijar_luz(65);
	
	fijar_tipo_tienda("venta");
	fijar_tendero(NPCS + "cirrax");
	
	add_static_property("no_silencio", 1);
	
	poner_lista_ajustes(([
		"Gurthang": 1,
		"solo_lista": 1
	]));
	
	fijar_tabla_objetos(([
		"/baseobs/comestibles/alcohol/botella_absenta" : 1,
		"/baseobs/comestibles/alcohol/botella_aguardiente" : 1,
		"/baseobs/comestibles/alcohol/botella_anis" : 1,
		"/baseobs/comestibles/alcohol/botella_aspex" : roll(1, 10),
		"/baseobs/comestibles/alcohol/botella_bourbon" : 1,
		"/baseobs/comestibles/alcohol/botella_brandy" : 1 + random(2),
		"/baseobs/comestibles/alcohol/botella_cerveza" : roll(1, 5),
		"/baseobs/comestibles/alcohol/botella_conac" : 1,
		"/baseobs/comestibles/alcohol/botella_ginebra" : 1,
		"/baseobs/comestibles/alcohol/botella_grog" : 1,
		"/baseobs/comestibles/alcohol/botella_pacharan" : 1,
		"/baseobs/comestibles/alcohol/botella_ron_blanco" : roll(1, 3),
		"/baseobs/comestibles/alcohol/botella_ron_dorado" : 1,
		"/baseobs/comestibles/alcohol/botella_tequila_anejo" : 1,
		"/baseobs/comestibles/alcohol/botella_tequila_blanco" : roll(1, 3),
		"/baseobs/comestibles/alcohol/botella_tequila_reposado" : 1,
		"/baseobs/comestibles/alcohol/botella_vino_blanco" : 3 + random(3),
		"/baseobs/comestibles/alcohol/botella_vino_rosado" : 3 + random(3),
		"/baseobs/comestibles/alcohol/botella_vino_tinto" : 3 + random(3),
		"/baseobs/comestibles/alcohol/botella_vodka" : 1 + random(2),
		"/baseobs/comestibles/alcohol/botella_whisky" : roll(1, 3),
		"/baseobs/comestibles/alcohol/frasco_gluhwein" : 1,
	]));
	
	add_exit(E, BASTION+"suburbios_11", "door");
  setup_door(E, 0, 0, 0, 0, 0, 0, 0);
}
