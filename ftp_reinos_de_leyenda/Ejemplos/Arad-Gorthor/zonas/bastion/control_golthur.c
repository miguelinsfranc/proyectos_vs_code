// Base para las votaciones de tiranos
//inherit "/std/salas/votaciones_organizacion.c";
inherit "/std/room";
#include "../../path.h"
void setup()
{
set_short("Sala de control de Golthur-Orod");
set_long("La fría altitud de esta sala es realmente engañosa, por fuera no parece una gran construcción pero en su interior puedes ver la poderosa construcción. Un alarde exagerado de fuerza en cada roca, entrelazadas con magnífica precisión. Una minúscula bóveda corona este cuadrado de sólida roca colgando de su cénit una grandiosa lámpara que casi alcanza las cuatro paredes, unida a ella diversas antorchas iluminan la estancia suficientemente para poder visualizar cualquier rincón de la estancia. Justo en el centro dos palpitantes esferas cristalinas atienden cualquier comando del gobernador de Golthur Orod.\n");
    
//    fijar_ruta_organizacion("ciudadanias/golthur_org.c");
//    crear_cartel("negra");
add_exit(S, BASTION+"horda_05.c", "corridor");
    // nº de votos, rango minimo necesario, mensaje a mostrar (0 o no muestra nada)
//    poner_votaciones_genericas_comunes(1, 3, 0);     // votación de 'cambiar grito'
//    poner_votaciones_genericas_ciudadania(1, 3, 0);  // Votación de estatus (expulsar, noble, recuperar...) y diplomacias
}
//string dame_permiso() { return "todo_golthur"; }
