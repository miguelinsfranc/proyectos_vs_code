// Satyr 2012
// Base para las votaciones de tiranos
//inherit "/std/salas/votaciones_organizacion.c";
inherit "/std/room";
#include "../../path.h"
void setup()
{
set_short("Caverna de control de Ancarak.");
set_long("La caverna de Ancarak, obra maestra de la minería kobold. Diestros golpes aquí y allá convierten la estancia en una perfecta cueva que recrea su Ancarak natal, algo más amplia que las demás pero de estilo sencillo a la vez que magnífico, resplandece la pulida roca casi como un espejo engrandeciendo tu reflejo hasta hacerte sentir un verdadero gigante.
");
    
 //   fijar_ruta_organizacion("ciudadanias/ancarak_org.c");
 //   crear_cartel("negra");
add_exit(O, BASTION+"horda_05.c", "corridor");
    
    // nº de votos, rango minimo necesario, mensaje a mostrar (0 o no muestra nada)
//    poner_votaciones_genericas_comunes(1, 3, 0);     // votación de 'cambiar grito'
 //   poner_votaciones_genericas_ciudadania(1, 3, 0);  // Votación de estatus (expulsar, noble, recuperar...) y diplomacias
}
//string dame_permiso() { return "todo_ancarak"; }
