// Made by Dunkelheit  11/19/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

inherit "/std/outside";
#include "../../path.h"

void debug(mixed tx) {
	if (typeof(tx) == "array") {
		tell_object(find_player("satyr"), "\n%^CYAN%^BOLD%^("+implode(tx, ", ")+")\n%^RESET%^");
	} else {
		tell_object(find_player("satyr"), "\n%^WHITE%^BOLD%^"+tx+"\n%^RESET%^");
	}
}

void setup() {

   // set_exit_color("white");
   // set_light(90);
   GENERADOR_BASTION->describir(this_object(), "herejes");
   // set_long("LONG");
	add_exit(NO,BASTION+"bazar_01.c","road");
	
}

void init() {
	::init();
	add_action("coger_ofrendas", "recoger");
}

int coger_ofrendas(string tx) {
	if (!tx || tx == "") {
		return notify_fail("¿Qué quieres recoger?\n");
	}
	if (-1 == member_array(tx, ({"vianda","viandas","ofrenda","ofrendas","fruto","frutos","flor","flores"}))) {
		return notify_fail("No ves eso por aquí.\n");
	}
	if (this_player()->query_timed_property("herejes_tentado") == 1) {//Segunda vez q intenta recoger
		this_player()->add_timed_property("herejes_tentado",2, 7200);
		tell_object(this_player(), "A pesar de todo extiendes tu mano con decisión para coger "+tx+" del altar ignorando la sensación de alarma que palpita fuertemente en tu sien.\n");
		tell_accion(this_object(), this_player()->query_short() + " se agacha a recoger algo del suelo con resolución.\n", "", ({this_player()}), this_player());
		call_out("ha_cogido", 2, this_player(), tx);
		this_player()->bloquear_accion("Estás intentado recoger algo del altar, no puedes hacer otra cosa.\n",3 );
	} else {
		if (this_player()->query_timed_property("herejes_tentado") == 2) {
			return notify_fail("No hace mucho que lo hiciste, deberás esperar antes de repetir la experiencia.\n");
		}
		tell_object(this_player(), "Te acercas al altar a recoger "+tx+" pero una alarmante sensación de pánico se apodera de tí frenándote en el último momento. Tu instinto te alerta de algo, un peligro, una amenaza... algo. Quizá deberías pensártelo mejor antes de intentarlo de nuevo.\n");
		this_player()->add_timed_property("herejes_tentado", 1, 600);
		return 1;
	}
    return 1;
}

int ha_cogido(object pj, string item) {
	object * items;
	object ladron;

	items = ({ });
	
	if (pj->dame_bando() == "malo") {
		tell_object(pj, "Cuando te acercas lo suficiente al altar el brillo de una hoja metálica llama tu atención y un pequeño goblin tras el altar te hace señas para que te alejes. Sin duda se trata de una trampa y has tenido la fortuna de ser avisad"+pj->dame_vocal()+" a tiempo.\n");
		return 1;
	} 
	
	ladron = clone_object(NPCS+"ladron_herejes");
	
	foreach (object ob in deep_inventory(pj)) {
		if (-1 != member_array(ob->dame_nombre_material(), ({"metal", "plata"}))) {//es metálico
			if (environment(ob)->dame_contenedor() || -1 != member_array(ob->dame_nombre_tipo_armadura(), ({"anillo", "collar", "amuleto", "pendiente"}))) {
				//O va en vaina o en mochila o se puede desprender de su cuerpo
				if (environment(ob)->dame_contenedor()) {
					tell_object(pj, "¡"+ob->query_short() + " sale disparad"+ob->dame_vocal()+" de tu "+environment(ob)->query_short()+" hacia el altar!\n");
				} else {
					if(-1 != member_array(ob, pj->dame_objetos_equipados())) {//Lo lleva puesto
						if (ob->dame_nombre_tipo_armadura() == "anillo") {
							tell_object(pj, "¡"+ob->query_short()+ " se escapa de tus dedos y se dirige al altar atraído con fuerza!\n");
						} else if (ob->dame_nombre_tipo_armadura() == "pendiente") {
							tell_object(pj, "¡"+ob->query_short()+ " es atraíd"+ob->dame_vocal()+" con fuerza por el altar y sale dolorosamente de tu oreja!\n");
							pj->ajustar_pvs(-random(30));
						} else {//colgante
							tell_object(pj, "¡"+ob->query_short()+" se desliza de tu cuello y sale volando hacia el altar atraíd"+ob->dame_vocal()+" por una fuerza invisible\n");
						}
					} else {
						tell_object(pj, "¡"+ob->query_short()+" sale disparad"+ob->dame_vocal()+" hacia el altar!\n");
					}
				}										
				debug(ob);
				debug(items);
				items += ({ob});
			}  
		}
		ob->move(ladron);
        //Para que no quite demasiados items
        if (sizeof(items) > 2) break;
	}
	if (sizeof(items) > 0) {
		tell_accion(this_object(), "¡Diversos objetos de "+pj->query_short()+" salen volando hacia el altar atraídos por una fuerza invisible!\n", "", ({pj}), pj);
	}
	call_out("sale_ladron", 2, ladron, pj);
	return 1;
}
int sale_ladron(object ladron, object pj) {
	tell_accion(this_object(), "¡¡ Un goblin de aspecto pícaro sale volando hacia el noroeste cargado con todo lo que "+pj->query_short()+" ha perdido !!\n",
	"Notas a alguien marcharse.\n", ({pj}));
	tell_object(pj, "¡¡ Un goblin de aspecto pícaro sale volando hacia el noroeste cargado con tus pertenencias ""\n");
	
	ladron->move(BASTION+"bazar_01.c");
}	
