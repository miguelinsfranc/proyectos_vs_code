// Satyr 2012
// Base para las votaciones de tiranos
//inherit "/std/salas/votaciones_organizacion.c";
inherit "/std/room";
#include "../../path.h"
void setup()
{
set_short("Sala de control de Mor-Groddûr");
set_long("La chapuza llevada a estilo arquitectónico te permite reconocer una estancia ideada por goblins, cada paso que das por la estancia te hace estremecer por temor a que las paredes se vengan abajo hundiéndote hasta el cuello en lodo, argamasa y roca. A pesar de todo las tristes paredes consiguen sustentar un suelo de paja a través del cual se permite el paso de la escasa luz exterior. Un profundo agujero en suelo, sobre el que hay que agacharse para emitir cualquier comando, es todo lo que se aprecia en la lúgubre sala.\n");
    
//    fijar_ruta_organizacion("ciudadanias/morgroddur_org.c");
add_exit(N, BASTION+"horda_05.c", "corridor");
//    crear_cartel("negra");
    
    // nº de votos, rango minimo necesario, mensaje a mostrar (0 o no muestra nada)
    //poner_votaciones_genericas_comunes(1, 3, 0);     // votación de 'cambiar grito'
    //poner_votaciones_genericas_ciudadania(1, 3, 0);  // Votación de estatus (expulsar, noble, recuperar...) y diplomacias
}
//string dame_permiso() { return "todo_morgroddur"; }
