// Made by Dunkelheit  11/19/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

//Aquí irá el baúl si se trae el gremio.

inherit "/std/outside";
#include "../../path.h"

void setup() {

   // set_exit_color("white");
   // set_light(90);
   GENERADOR_BASTION->describir(this_object(), "horda");
   // set_long("LONG");
add_exit(O,BASTION+"horda_04.c","road");
add_exit(E, BASTION+"control_ancarak.c", "corridor");
add_exit(N, BASTION+"control_golthur.c", "corridor");
add_exit(S, BASTION+"control_mor-groddur.c", "corridor");
add_exit(AR, BASTION+"control_gremio.c", "stairs");
}
