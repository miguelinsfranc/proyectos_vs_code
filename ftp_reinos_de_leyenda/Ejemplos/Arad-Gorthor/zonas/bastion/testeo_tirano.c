// Satyr 2012
// Base para las votaciones de tiranos
inherit "/std/salas/votaciones_organizacion.c";
void setup()
{
    set_short("Pruebas de ancarak para tiranos");
    
    fijar_ruta_organizacion("ciudadanias/ancarak.c");
    crear_cartel("negra");
    
    // nº de votos, rango minimo necesario, mensaje a mostrar (0 o no muestra nada)
    poner_votaciones_genericas_comunes(1, 3, 0);     // votación de 'cambiar grito'
    poner_votaciones_genericas_ciudadania(1, 3, 0);  // Votación de estatus (expulsar, noble, recuperar...) y diplomacias
}
string dame_permiso() { return "todo_ancarak"; }