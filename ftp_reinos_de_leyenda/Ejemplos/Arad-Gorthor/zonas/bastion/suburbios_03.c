// Made by Dunkelheit  11/19/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

inherit "/std/outside";
#include "../../path.h"

void setup() {

   // set_exit_color("white");
   // set_light(90);
   GENERADOR_BASTION->describir(this_object(), "suburbios");
   // set_long("LONG");
add_exit(O,BASTION+"suburbios_02.c","road");
add_exit(SE,BASTION+"bazar_03.c","road");
add_exit(E, BASTION+"taberna", "door", "madera", 10);
}
