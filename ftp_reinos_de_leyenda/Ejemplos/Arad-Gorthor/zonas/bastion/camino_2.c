// Dunkelheit 19-12-2009

inherit "/std/outside";
#include "../../path.h"

void setup()
{
	GENERADOR_BASTION->describir(this_object(), "exterior_atalaya_sur");

	add_exit(N, BASTION+"puerta_oeste.c", "path");
	add_exit("saetera", BASTION+"almena_sur", "escalable");
}
