// Made by Dunkelheit  11/19/2009
// Area made with the ASCII Mapmaker
// copyright 1999 by Espen Austad
// www.stud.ux.his.no/~austad/mud/mapmaker
// for more info.

inherit "/std/outside";
#include "../../path.h"

void setup() {

   // set_exit_color("white");
   // set_light(90);
   GENERADOR_BASTION->describir(this_object(), "suburbios");
   // set_long("LONG");
add_exit(NO,BASTION+"suburbios_08.c","road");
add_exit(O, BASTION+"licoreria.c", "door");
add_exit(E,BASTION+"suburbios_12.c","road");
setup_door("licoreria", 0, 0, 0, 0, 0, 10000);
}
