//Gareth 26.09.03
//Satyr 2k8
//Altaresh, añado un ifndef a las cosas del Baseobs para compatibilidad con los guardias nuevos genericos
//Gholxien 05.06.2023: Añado #define para el campamento de bandidos

#define _PATH

//Directorios
#define URLOM       "/d/urlom/"
#define NPCS        URLOM + "npcs/"
#define ITEMS       URLOM + "objetos/"
#define ARMAS       URLOM + "armas/"
#define ARMADURAS   URLOM + "armaduras/"
#define CONJUNTOS   URLOM + "conjuntos/"
#define LOGS        URLOM + "logs/"
#define HANDLERS    URLOM + "handlers/"
#define PLANOS      URLOM + "planos/"
#define RECETAS     URLOM + "recetas/"
#define ROOMS       URLOM + "salas/"

// Baseobs
#ifndef BASEOBS
#define BASEOBS         "/baseobs/"
#define ARMADURASBASE   BASEOBS + "armaduras/"
#define ARMASBASE       BASEOBS + "armas/"
#define NPCSBASE        BASEOBS + "monstruos/"
#define MISCBASE        BASEOBS + "misc/"   
#define BROPAS          BASEOBS + "ropas/"
#define BPERGAMINOS     BASEOBS + "pergaminos/"
#endif

//Direcciones
#define N "norte"
#define S "sur"
#define E "este"
#define O "oeste"
#define NE "noreste"
#define NO "noroeste"
#define SE "sudeste"
#define SO "sudoeste"
#define DE "dentro"
#define FU "fuera"
#define AR "arriba"
#define AB "abajo"

//Zonas
#define CAMPBAN "/w/gholxien/programacion_de_contenido/bandidos_urlom/campamento_bandidos/"
#define ILUSION "/d/urlom/zonas/torre_ilusion/"
#define BASE "/d/urlom/zonas/"
#define TEMPLO BASE+"templo/"
#define BOSQUEURLOM BASE+"bosque/"
#define ORMERAK BASE+"ormerak/"
#define ORMEION BASE+"ormeion/"
#define NUEVA_ORMEION (BASE+"ormeion2/")
#define NUEVA_ORMERAK BASE+"ormerak2/"
#define MONTANYA BASE+"montaña/"
#define VOLCAN BASE+"volcan/"
#define VOLCAN1 VOLCAN+"nivel1/"
#define VOLCAN2 VOLCAN+"nivel2/"
#define VOLCAN3 VOLCAN+"nivel3/"
#define LESSIRNAK VOLCAN+"guarida_lessirnak/"
#define CIUDAD VOLCAN+"ak'anon/"
#define PUERTO BASE+"puerto/"
#define ISLA BASE+"isla/"
#define NEWBIE "/d/urlom/zonas/newbie/"
#define PALACIO CIUDAD + "palacio/"
#define MDV "/d/urlom/zonas/montanya_vapor/"
#define C_BOSQUE_ORMEION BASE+"caminos/bosq_ormeion/"
#define CLEHERDAVEL BASE+"caminos/desfiladero/"
#define LAGO "/d/urlom/zonas/lago_laftrache/"
#define PASO_MONTANYAS BASE + "caminos/paso_montañas/"
#define CAPILLA BASE + "capilla/"

//NPCS
#define NPCS_VOLCAN NPCS
#define NPCS_MONTANYA NPCS

//Handlers
#define GENERADOR "/d/urlom/handlers/generador_rooms"
#define OB_CIUDAD "/grupos/ciudadanias/ak'anon"
#define H_MONTANYAS PASO_MONTANYAS + "handler.c"
#define G_NEWBIE "/d/urlom/zonas/newbie/newbie_base.c"

//Includes
#define MDVH "/d/urlom/zonas/montanya_vapor/path_mdv.h"
