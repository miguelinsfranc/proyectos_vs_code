// Gareth 8.02.04
// Bastón Metálico, Tesoro de Lessirnak
// ************************************
// Daño más alto que un bastón normal
// Bono al daño contra Muertos Vivientes
// Doble de peso que un bastón normal
// Mejora la BP
// ************************************
// Ember 10-12-06, rehecho para el nuevo Lessirnak.
// Ahora puede lanzar descargas eléctricas que destrocen defensas de area.
// Satyr 25.06.2015 -- Revamp, +30, +30 poder mágico y a 1 mano.

inherit "/obj/arma";
#include "../path.h"

void setup() {
	
	fijar_arma_base("baston");	
	set_name("baston");
	set_short("Bastón %^BOLD%^Metálico%^RESET%^");
	set_main_plural("Bastones %^BOLD%^Metálicos%^RESET%^");
	set_long("Es un bastón de forma y tamaño comunes, pero "
		"con una característica especial, pues está construi"
		"do con algún tipo de metal plateado, en vez de la t"
		"ípica madera. A pesar de que obviamente pesa más qu"
		"e uno normal, su peso tampoco llega a ser desorbita"
		"do, debido a que debe estar hecho con un metal lige"
		"ro. La empuñadura está forrada con cuero negro, cuy"
		"o color contrasta con el brillo plateado que cubre "
		"el resto del bastón.\n"
		"De tanto en tanto, te parece notar que el bastón vi"
		"bra con una cadencia extraña. Como si algún tipo de"
		" reacción estuviera llevándose acabo dentro de él.\n");
		
	add_alias(({"baston","bastón","metalico","metálico"}));
	add_plural(({"bastones","metalicos","metálicos"}));
	add_attack("mental_baston", "mental", 1, 50, 5, 20, 0, 0, 1);	
	
	fijar_mensaje_leer("ArtilugioPropiedadDeLeoTodoElDiaYTodaLaNoche.\n", "gnomo", 0);
        
	fijar_genero(1);
    fijar_material(2);
	fijar_valor(115000);
    fijar_peso(dame_peso()*2);
    fijar_manos_necesarias(1);
    
    fijar_encantamiento(30);
    add_static_property("poder_magico", 30);
    add_static_property("clase", ({"Hechicero"}));

	add_static_property("nivel_minimo", 30);
}

string descripcion_encantamiento()
{
  return ::descripcion_encantamiento()+"El poder latente en el bastón, pese a provenir de alguna fuente desconocida y ser muy voluble, parece inmenso.\n";
}

void no_pasa_nada(object pj, object ob)
{
  if(ob == pj)
  	tell_object(ob, "Tu "+query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra ti chamuscándote... pero parece que no pasa nada más.\n");
  else
  {
  	tell_object(ob, "El "+query_short()+" de "+this_player()->query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra contra ti chamuscándote... pero parece que no pasa nada más.\n");  	
  	tell_object(pj, "Tu "+query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra "+ob->query_short()+" chamuscándole... pero parece que no pasa nada más.\n");
  }
  ob->spell_damage(-200-random(500), "mental", pj, 0);
  this_object()->add_timed_property("usado_recien", 1, 300); //5 mins sin poder
}

void init()
{
  add_action("haz_accion", "nyaraktuk");
  ::init();
}

int haz_accion(string str)
{
  object ob,*todos;
  
  if(!str)
  	return notify_fail("Debes indicar un objetivo.\n");
  
  if(!TO->query_in_use())
  	return notify_fail("No puedes desencadenar el poder del bastón sin empuñarlo previamente.\n");
  	
  ob = present(lower_case(str), environment(this_player()));

  if(!this_player()->query_property("sabe_usar_baston"))
  	return notify_fail("El bastón parece ignorar tu petición y no ocurre nada.\n");
  
  if(!ob)
  {
  	str = this_player()->expand_nickname(str);
  	ob = present(lower_case(str), environment(this_player()));  	
  	if(!ob)
  		return notify_fail("No hay nadie aquí con el nombre "+str+".\n");
  }
  
  if(query_timed_property("usado_recien"))
  	return notify_fail("El bastón parece no responder a tu palabra de mando.\n");
  
  if(!random(10)) //Pifia
  {
  	this_player()->do_say("Nyaraktuk "+capitalize(str)+"\n", 1);
  	tell_accion(environment(this_player()), "Acto seguido, una luz cegadora envuelve el "+this_object()->query_short()+" de "+this_player()->query_short()+", que empieza a temblar descontroladamente en su mano y... %^BOLD%^BOOOOOOOOOOOOOM%^RESET%^\n", "%^BOLD%^BOOOOOOOOOOOOOM.\n%^RESET%^", ({this_player()}), this_player());
  	tell_object(this_player(), "Una luz cegadora envuelve tu "+this_object()->query_short()+", que empieza a temblar descontroladamente en tu mano y... %^BOLD%^BOOOOOOOOOOOOOM%^RESET%^\n");
  	todos = all_inventory(environment(this_player()));
  	foreach(object obj in todos)
  	{
  		if(living(obj))
  		{
  			if(obj == this_player())
  			{
  				tell_object(obj, "Tu "+this_object()->query_short()+" desencadena todo su poder de forma descontrolada, alcanzándote de lleno y obligándote a soltarlo.\n");
  				tell_accion(environment(this_player()), "El "+query_short()+" de "+this_player()->query_short()+" sale volando por los aires.\n", "", ({obj}), obj);
  				this_object()->move(environment(this_player()));
  			}
  			else
  				tell_object(obj, "Una deflagración se extiende por la zona alcanzándote de lleno.\n");

			obj->spell_damage(-200-random(1000), "mental", this_player(), 0);
  		}
  	}
  	
  	this_object()->add_timed_property("usado_recien", 1, 600); //10 mins sin poder
  	
  	return 1;
  }
  
  //Tiene éxito, destozamos los poderes de defensa del objetivo: Wylan, Santuario y globo de invulnerabilidad menor/mayor (el mayor sólo en casos muy concretos)
  //Posibilidad de que no chute:

  this_player()->do_say("Nyaraktuk "+capitalize(str)+"\n", 1);
  tell_accion(environment(this_player()), "Acto seguido, una luz cegadora envuelve el "+this_object()->query_short()+" de "+this_player()->query_short()+", que empieza a temblar descontroladamente en su mano y... %^BOLD%^BOOOOOOOOOOOOOM%^RESET%^\n","%^BOLD%^BOOOOOOOOOOOOOM%^RESET%^\n", ({this_player()}), this_player());

  if(!random(20))
  {
	no_pasa_nada(this_player(), ob);
	return 1;
  }

  //Sólo elimina una cosa, y va siempre en el mismo orden:
  if(ob->dame_esfera_elastica_de_wylan()) //WYLAN
  {
  	if(ob == this_player())
  		tell_object(ob, "Tu "+query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra la esfera de Wylan que te envuelve, haciéndola pedazos.\n");
  	else
  	{
  		tell_object(ob, "El "+query_short()+" de "+this_player()->query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra la esfera de Wylan que te envuelve, haciéndola pedazos.\n");
  		tell_object(this_player(), "Tu "+query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra la esfera de Wylan que envuelve a "+ob->query_short()+", haciéndola pedazos.\n");
  	}
  	ob-> destruir_esfera_elastica_de_wylan();
  	ob->spell_damage(-20-random(200), "mental", this_player(), 0);
	this_object()->add_timed_property("usado_recien", 1, 1800); //30 mins sin poder
  	
  	return 1;
  }
  
  if(ob->en_santuario())  //SANTUARIO
  {
  	if(ob == this_player())
  		tell_object(ob, "Tu "+query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra el santuario que te envuelve, cancelando su poder.\n");
  	else
  	{
  		tell_object(ob, "El "+query_short()+" de "+this_player()->query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra el santuario que te envuelve, cancelando su poder.\n");
  		tell_object(this_player(), "Tu "+query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra en santuario que envuelve a "+ob->query_short()+", cancelando su poder.\n");  		
  	}
  	
  	ob->destruir_santuario();
  	ob->spell_damage(-20-random(200), "mental", this_player(), 0);
	this_object()->add_timed_property("usado_recien", 1, 1800); //30 mins sin poder
  	
  	return 1;
  }

  if(ob->dame_efecto("globo"))  //GLOBOS DE INVULNERABILIDAD
  { 
  	if(ob->dame_invulnerable_hechizos() == 3 || (ob->dame_invulnerable_hechizos() == 4 && !random(10))) //El menor lo rompemos "sin problemas"
  	{
  		if(ob == this_player())
  			tell_object(ob, "Tu "+query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra el globo transparente que te envuelve, cancelando su poder.\n");
  		else
  		{
  			tell_object(ob, "El "+query_short()+" de "+this_player()->query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra el globo transparente que te envuelve, cancelando su poder.\n");
  			tell_object(this_player(), "Tu "+query_short()+" se ilumina y vibra con fuerza mientras unas ondas de energía surgen de él y chocan contra el globo transparente que envuelve a "+ob->query_short()+", cancelando su poder.\n");  					
  		}
  	
  		ob->quitar_efecto("globo", 0);
  		ob->spell_damage(-20-random(200), "mental", this_player(), 0);
		this_object()->add_timed_property("usado_recien", 1, 1800); //30 mins sin poder
  	
  		return 1;  		
  	}
  	
	no_pasa_nada(this_player(), ob);
	return 1;
  }
  
  no_pasa_nada(this_player(), ob);
  return 1;  
  
}


