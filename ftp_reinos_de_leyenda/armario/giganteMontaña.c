//Kiratxia 06/05/2010

inherit "/obj/monster.c";

#include "/d/urlom/path.h"

void setup()
{
    set_name("gigante");
    add_alias(({"gigante","montañas"}));
    add_plural(({"gigantes","montañas"}));
    set_short("Gigante de las Montañas");
    set_main_plural("Gigantes de las Montañas");
    set_long("Se trata de un enorme gigante sin ningún tipo de pelo en cuerpo o cabeza. Su piel es pálida"
      " y de un color rosáceo y sus ojos grandes y púrpuras. Sus brazos y piernas están muy desarrollados"
      " y su mandíbula torcida. Tiene una gran barriga con forma ovalada y diversas cicatrices por todo el cuerpo"
      " sobre todo en el pecho.\n");

    fijar_religion("ateo");
    fijar_raza("gigante");
    fijar_clase("soldado");
    fijar_alineamiento(2500); 

    quitar_lenguaje("dendrita");
    fijar_lenguaje_actual("gruñidos");
    fijar_genero(1);
    fijar_fue(18 + random(7));
    fijar_extrema(60);
    fijar_des(7 + random(3));
    fijar_con(18 + random(7));
    fijar_int(3);
    fijar_sab(5);
    fijar_car(5);
    ajustar_bo(100);

    add_loved("raza", ({"npc/gigante"}));
    set_aggressive(2,10);

    set_move_after(10,5);
    fijar_nivel(20 + random(10));
    fijar_pvs_max(5000);
    adjust_tmp_damage_bon(120);

    load_chat(30,
      ({
        ":coge una piedra y la lanza contra la montaña con fuerza.",
        ":mira con furia a la montaña y gruñe enseñando sus dientes.",
      }));

    add_clone(ARMADURASBASE + "piel.c", 1);
    init_equip();
}
