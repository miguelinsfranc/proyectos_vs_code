// Gareth 26.09.03
// Bosque de Urlom, Urlom

inherit "/d/urlom/handlers/salidas_random";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_denso");
   set_light(50);
   add_exit(N,BOSQUEURLOM+"besp2_52.c","plain");
   add_exit(E,BOSQUEURLOM+"besp2_60.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp2_66.c","plain");
}
