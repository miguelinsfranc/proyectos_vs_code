// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom
// Sierephad Abrl 2k18 	-- 	Reajustando el mapeado de urlom
//https://www.reinosdeleyenda.es/foro/ver-tema/actualizacion-del-mapa-de-zmud/#post-301529

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "desfiladero");
   GENERADOR->poner_npcs(TO, "caminos_bosque");
   set_light(100);
   add_exit(E,BOSQUEURLOM+"cb_20.c","road");
   add_exit(SO,BOSQUEURLOM+"cb_27.c","road");
   add_exit(NO,BOSQUEURLOM+"cb_42.c","road");

    add_sign("Es un sencillo cartel de madera, donde hay grabadas orientaciones a los diferentes caminos a partir de aquí.\n",
    "%^BOLD%^ORANGE%^Estás en la parte norte del reino de Urlom%^RESET%^.\n"
    "%^BOLD%^YELLOW%^Noroeste%^RESET%^: acceso al reino de Kheleb.\n"
    "%^BOLD%^YELLOW%^Este%^RESET%^: Bosque de Urlom.\n"
    "%^BOLD%^YELLOW%^Sudoeste%^RESET%^: Paso entre las montañas, baía de Urlom.\n",
    "cartel", "cartel");
}
