// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_70.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_57.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_72.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp3_84.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_83.c","plain");

add_clone("/d/anduar/objetos/anduar/verano-circulo_ceniza", 1, (: $1->fijar_bosque_evento("urlom") :));
}
