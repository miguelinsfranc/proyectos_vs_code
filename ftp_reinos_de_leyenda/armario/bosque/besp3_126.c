// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom
// Sierephad Abrl 2k18 	-- 	Reajustando el mapeado de urlom
//https://www.reinosdeleyenda.es/foro/ver-tema/actualizacion-del-mapa-de-zmud/#post-301529

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_125.c","plain");
   add_exit(NO,BOSQUEURLOM+"besp3_113.c","plain");
   add_exit(SE,BOSQUEURLOM+"bosq3_51.c","bosque");
}
