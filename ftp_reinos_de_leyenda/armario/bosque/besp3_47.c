// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_46.c","plain");
   add_exit(N,BOSQUEURLOM+"bosq3_28.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_62.c","plain");
}
