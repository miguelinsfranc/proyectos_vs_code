// Gareth 26.09.03
// Bosque de Urlom, Urlom

inherit "/d/urlom/handlers/salidas_random";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_denso");
   set_light(50);
   add_exit(O,BOSQUEURLOM+"besp2_66.c","plain");
   add_exit(NO,BOSQUEURLOM+"besp2_60.c","plain");
   add_exit(N,BOSQUEURLOM+"besp2_61.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp2_62.c","plain");
   add_exit(S,BOSQUEURLOM+"besp2_73.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp2_72.c","plain");
}
