// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(NO,BOSQUEURLOM+"besp3_129.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_130.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_140.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_141.c","plain");
}
