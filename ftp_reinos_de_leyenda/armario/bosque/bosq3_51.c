// Gareth 27.09.03
// Bosque de Urlom, Urlom
// Sierephad Abrl 2k18 	-- 	Reajustando el mapeado de urlom
//https://www.reinosdeleyenda.es/foro/ver-tema/actualizacion-del-mapa-de-zmud/#post-301529

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior2");
   set_light(70);
   add_exit(S,BOSQUEURLOM+"bosq3_55.c","bosque");
   add_exit(SO,BOSQUEURLOM+"bosq3_54.c","bosque");
   add_exit(NO,BOSQUEURLOM+"besp3_126.c","plain");
}
