// Gareth 26.09.03
// Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior1");
   set_light(70);
   add_exit(NE,BOSQUEURLOM+"bosq_3.c","bosque");
   add_exit(S,BOSQUEURLOM+"bosq_11.c","bosque");
}
