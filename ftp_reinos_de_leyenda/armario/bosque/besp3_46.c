// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_45.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_29.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_47.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_61.c","plain");
}
