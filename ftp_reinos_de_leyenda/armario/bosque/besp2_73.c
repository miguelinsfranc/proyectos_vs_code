// Gareth 26.09.03
// Bosque de Urlom, Urlom

inherit "/d/urlom/handlers/salidas_random";
#include "/d/urlom/path.h"

void setup() {

    GENERADOR->generar_basico(TO, "bosque_denso");
    set_light(50);
    add_exit(O,BOSQUEURLOM+"besp2_72.c","plain");
    add_exit(N,BOSQUEURLOM+"besp2_67.c","plain");
	//Sierephad quito el viejo trasmutador de aqui y pongo el nuevo trasmutador en /d/urlom/zonas/bosque/besp3_41
    //add_clone(NPCS+"viejo_transmutador.c");
}
