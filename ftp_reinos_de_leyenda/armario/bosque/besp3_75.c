// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_74.c","plain");
   add_exit(NO,BOSQUEURLOM+"besp3_60.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_61.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_62.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_86.c","plain");
}
