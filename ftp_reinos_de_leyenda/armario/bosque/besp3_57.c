// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_56.c","plain");
   add_exit(NO,BOSQUEURLOM+"besp3_40.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_41.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_42.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_58.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_71.c","plain");
}
