// Gareth 6.12.03
// Cabaña del Bosque de Urlom
// Dominio de Urlom
// Eckol Ago16: Bloqueo al jugador.
// Sierephad Abrl 2k18 	-- 	Reajustando el mapeado de urlom
//https://www.reinosdeleyenda.es/foro/ver-tema/actualizacion-del-mapa-de-zmud/#post-301529
// Sierephad Abr 2k18	--	El plano esta en iou. comento la linea


inherit "/std/room";
#include "/d/urlom/path.h"

#define BLOQUEO_PROP "armario_planta_negra_abierto"
#define BLOQUEO_TIEMPO 6*3600

void setup() {
	
   set_light(20);
   set_short("%^GREEN%^Bosque de Urlom: %^RESET%^ORANGE%^Cabaña Abandonada%^RESET%^");
   set_long("Una cabaña de madera construida en medio del bosque, aunque ahora da el "
      "aspecto de estar deshabitada, y de hecho, parece que lleva bastante tiempo aba"
      "ndonada. Varios muebles, también de madera, pueblan la cabaña, aunque algunos "
      "están roídos y sucios, llenos de polvo. Hay una mesa con varias sillas alreded"
      "or tiradas en el suelo, y un armario en frente de la entrada, que sólo tiene u"
      "na puerta. La otra yace tirada en el suelo, a los pies del armario.\n");
   set_exit_color("naranja");
   
   add_item("armario", "Un armario situado en frente de la entrada, que ya sólo "
      "posee una de las dos puertas que lo formaban.\n");
   add_exit(N,BOSQUEURLOM+"bosq_101.c","door");
}

void init() {
   
   ::init();
   add_action("abrir_puerta","abrir");
}

int abrir_puerta(string str) {
   
   if(str=="puerta"||str=="armario")
   {
      if(TO->query_static_property("armario_abierto"))
         return notify_fail("El armario ya está abierto.\n");
      
      if(!TP->query_timed_property(BLOQUEO_PROP) && !random(3))
      {
		/*
      	tell_object(TP, "Abres la única puerta que le queda al armario, y una planta negra y un papel caen al suelo.\n");
      	tell_accion(TO, TP->query_short()+" abre la puerta del armario y una planta y un papel cae al suelo.\n", 
      		"Oyes el sonido de una puerta al moverse.\n", ({TP}), TP);
		*/
		tell_object(TP, "Abres la única puerta que le queda al armario, y algo cae al suelo.\n");
      	tell_accion(TO, TP->query_short()+" abre la puerta del armario y algo cae al suelo.\n", 
      		"Oyes el sonido de una puerta al moverse.\n", ({TP}), TP);
      	clone_object(ITEMS+"pnegra")->move(TO);
      	//clone_object("/baseobs/inventores/planos/seguridad_armas")->move(TO);
		TP->add_timed_property(BLOQUEO_PROP,1,BLOQUEO_TIEMPO);
      	write_file(LOGS+"plantas.log", ctime(time())+" "+TP->QCN+" ("+TP->dame_nivel()+") consigue una Planta Negra.\n");
      }
      else
      {
      	tell_object(TP, "Abres la única puerta que le queda al armario, pero está vacío.\n");
      	tell_accion(TO, TP->query_short()+" abre la puerta del armario.\n", 
      		"Oyes el sonido de una puerta al moverse.\n", ({TP}), TP);
      }
      TP->accion_violenta();
      TO->add_static_property("armario_abierto",1);
      return 1;
   }
}
