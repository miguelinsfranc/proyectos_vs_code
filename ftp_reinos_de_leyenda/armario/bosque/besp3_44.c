// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(N,BOSQUEURLOM+"besp3_27.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_28.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_45.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_60.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_59.c","plain");
}
