// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "desfiladero");
   GENERADOR->poner_npcs(TO, "caminos_bosque");
   set_light(100);
   add_exit(E,BOSQUEURLOM+"cb_47.c","road");
   add_exit(SE,BOSQUEURLOM+"cb_51.c","road");
   add_exit(SO,BOSQUEURLOM+"cb_50.c","road");

    add_sign("Es un cartel hecho de madera. En él están indicados los caminos disponibles desde aquí.\n",

    "%^BOLD%^ORANGE%^Estás en la parte septentrional del reino de Urlom%^RESET%^.\n"
    "\n%^BOLD%^YELLOW%^Este%^RESET%^: acceso hacia Kattak.\n"
    "%^BOLD%^YELLOW%^Sudeste%^RESET%^: bosque de Urlom.\n"
    "%^BOLD%^YELLOW%^Sudoeste%^RESET%^: paso entre las montañas.\n",
    "cartel de madera", "cartel");
}
