// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior1");
   set_light(70);
   set_short(TO->query_short()+"%^GREEN%^: %^RESET%^ORANGE%^Entrada a Cabaña%^RESET%^");
   set_long(TO->query_long()+"Hay una puerta algo sucia que conduce "
   	"a lo que parece ser una cabaña.\n");
   add_exit(N,BOSQUEURLOM+"bosq_92.c","bosque");
   add_exit(S,BOSQUEURLOM+"cabanya.c","door");
}
