// Gareth 27.09.03
// Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior2");
	set_long(query_long()+"   Los árboles de esta zona son diferentes a los que te has "
		"encontrado en el resto del bosque. Gruesos robles han crecido fuertemente "
		"en este lugar.\n");
	fijar_bosque(4);
	fijar_tipo_arbol("roble");
   set_light(70);
   add_exit(N,BOSQUEURLOM+"bosq3_35.c","bosque");
   add_exit(SO,BOSQUEURLOM+"bosq3_41.c","bosque");
}
