// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom
// Sierephad Abrl 2k18 	-- 	Reajustando el mapeado de urlom
//https://www.reinosdeleyenda.es/foro/ver-tema/actualizacion-del-mapa-de-zmud/#post-301529

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "desfiladero");
   GENERADOR->poner_npcs(TO, "caminos_bosque");
   set_light(100);
   add_exit(N,BOSQUEURLOM+"cb_40.c","road");
   add_exit(O,BOSQUEURLOM+"cb_32.c","road");
}
