// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_40.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_57.c","plain");
    //añado aqui al nuevo trasmutador
   add_clone("/d/naggrung/npcs/tundra_viejo_transmutador",1);
}
