// Gareth 27.09.03
// Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior2");
   set_light(70);
   add_exit(NE,BOSQUEURLOM+"bosq3_39.c","bosque");
   add_exit(S,BOSQUEURLOM+"bosq3_45.c","bosque");
   add_exit(SE,BOSQUEURLOM+"besp3_101.c","bosque");
}
