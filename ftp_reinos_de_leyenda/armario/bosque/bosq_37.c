// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_denso");
   set_light(50);
   add_exit(O,BOSQUEURLOM+"bosq_36.c","bosque");
   add_exit(NO,BOSQUEURLOM+"bosq_29.c","bosque");
   add_exit(N,BOSQUEURLOM+"bosq_30.c","bosque");
   add_exit(NE,BOSQUEURLOM+"bosq_31.c","bosque");
   add_exit(E,BOSQUEURLOM+"bosq_38.c","bosque");
   add_exit(SE,BOSQUEURLOM+"bosq_46.c","bosque");
   add_exit(S,BOSQUEURLOM+"bosq_45.c","bosque");
   add_exit(SO,BOSQUEURLOM+"bosq_44.c","bosque");
}
