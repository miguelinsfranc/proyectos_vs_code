// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior1");
   set_light(70);
   add_exit(NO,BOSQUEURLOM+"bosq_95.c","bosque");
   add_exit(NE,BOSQUEURLOM+"bosq_96.c","bosque");
}
