// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "desfiladero");
   GENERADOR->poner_npcs(TO, "caminos_bosque");
   set_light(100);
   add_exit(NO,BOSQUEURLOM+"cb_70.c","road");
   add_exit(NE,BOSQUEURLOM+"cb_71.c","road");
   add_exit(E,BOSQUEURLOM+"cb_75.c","road");
}
