// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(NO,BOSQUEURLOM+"besp3_22.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_40.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_55.c","plain");
}
