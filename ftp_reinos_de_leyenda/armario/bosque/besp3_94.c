// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(NO,BOSQUEURLOM+"besp3_80.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_81.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_95.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_109.c","plain");
}
