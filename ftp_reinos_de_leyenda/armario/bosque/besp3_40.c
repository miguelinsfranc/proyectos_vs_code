// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_39.c","plain");
   add_exit(NO,BOSQUEURLOM+"besp3_23.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_24.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_41.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp3_57.c","plain");
}
