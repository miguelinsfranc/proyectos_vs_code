// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(NO,BOSQUEURLOM+"besp3_49.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_50.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_51.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_78.c","plain");
}
