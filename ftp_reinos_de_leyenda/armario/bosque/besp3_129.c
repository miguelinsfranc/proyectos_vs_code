// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(N,BOSQUEURLOM+"besp3_119.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp3_139.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_138.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_137.c","plain");
}
