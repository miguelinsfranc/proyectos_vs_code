// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_111.c","plain");
   add_exit(NO,BOSQUEURLOM+"besp3_96.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_97.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp3_124.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_123.c","plain");
}
