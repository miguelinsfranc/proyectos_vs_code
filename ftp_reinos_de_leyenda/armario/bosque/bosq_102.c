// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior1");
   set_light(70);
   add_exit(N,BOSQUEURLOM+"bosq_93.c","bosque");
   add_exit(SE,BOSQUEURLOM+"bosq_112.c","bosque");
}
