// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_denso");
   set_light(50);
   add_exit(NO,BOSQUEURLOM+"bosq_16.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp_17.c","plain");
   add_exit(E,BOSQUEURLOM+"besp_29.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp_36.c","plain");
   add_exit(S,BOSQUEURLOM+"besp_35.c","plain");
}
