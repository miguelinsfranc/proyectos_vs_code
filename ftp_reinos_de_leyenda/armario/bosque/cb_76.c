// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "desfiladero");
   GENERADOR->poner_npcs(TO, "caminos_bosque");
   set_light(100);
   add_exit(O,BOSQUEURLOM+"cb_75.c","road");
   add_exit(NO,BOSQUEURLOM+"cb_71.c","road");
   add_exit(SE,BOSQUEURLOM+"cb_80.c","road");
}
