// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior1");
   set_light(70);
   add_exit(E,BOSQUEURLOM+"bosq_104.c","bosque");
   add_exit(SO,BOSQUEURLOM+"bosq_112.c","bosque");
}
