// Gareth 26.09.03
// Bosque de Urlom, Urlom

inherit "/d/urlom/handlers/salidas_random";
#include "/d/urlom/path.h"

int colgante=0;

void setup()
{

   GENERADOR->generar_basico(TO, "bosque_denso");
   set_light(50);
   add_exit(NO,BOSQUEURLOM+"besp2_59.c","plain");
   add_exit(N,BOSQUEURLOM+"besp2_60.c","plain");
   add_exit(E,BOSQUEURLOM+"besp2_67.c","plain");
   if (random(30)>20)
   {
      set_long(query_long()+"Te das cuenta de que uno de los arboles está mas ahuecado que los demás.\n");      
      add_item("arbol","Un árbol de los muchos que hay plantados en este bosque, aunque parece que este tiene un gran "
         "hueco en su tronco.\n");
      add_item(({"hueco","agujero","tronco"}),"El hueco del árbol es enorme, puede ser que algún animal lo haya hecho para hacerse un nido ahí.\n");
      colgante=1;
   }
}

void init()
{
   ::init();
   if(!colgante) return;
   add_action("meter_mano","meter");
}

int meter_mano(string tx)
{
   string que,donde;
   
   if (!tx) return 0;
   if(!sscanf(tx,"%s en %s",que,donde))
      return notify_fail("¿Meter qué en dónde?\n");
   if(-1==member_array(que,({"mano","manos"})) || -1==member_array(donde,({"agujero","hueco","tronco","arbol"})))
      return notify_fail("No puedes meter eso ahí.\n");
   if(random(15)>9 && colgante)
   {
      tell_object(TP,"Metes la mano en el agujero y palpas con miedo la superficie interna del tronco. De "
         "repente sientes algo con una forma extraña, lo agarras y lo sacas rápidamente sin saber lo que te podrás "
         "encontrar en la mano.\n");
      tell_object(TP,"Te miras con intriga la mano, y ves que posees algún tipo de amuleto.\n");
      tell_accion(ENV(TP),TP->QCN+" mete la mano en el tronco de árbol y saca algo.\n","",({TP}),0);      
      clone_object(ARMADURAS+"colgante_bremen")->move(TP);
      colgante=0;
      return 1;
   }
   else
   {
      tell_object(TP,"Metes la mano en el agujero y palpas con miedo la superficie interna del tronco. Tras un rato palpando"
         " te cansas y la sacas decepcionado.\n");
      tell_accion(ENV(TP),TP->QCN+" mete la mano en el tronco de árbol y despues de un rato la vuelve a sacar.\n","",({TP}),0);     
      return 1;
   }
         
}

