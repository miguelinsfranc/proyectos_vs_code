// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_67.c","plain");
   add_exit(NO,BOSQUEURLOM+"besp3_53.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_54.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_69.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_80.c","plain");
}
