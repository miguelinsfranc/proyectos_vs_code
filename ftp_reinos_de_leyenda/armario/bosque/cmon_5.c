// Gareth 07.10.03
// Camino a la MontañaDeVapor
// Bosque de Urlom

inherit "/std/outside";
#include "/d/urlom/path.h"

void setup() {

   set_light(100);
   GENERADOR->generar_basico(TO, "camino_montaña");
   GENERADOR->poner_npcs(TO, "caminos_bosque");
   add_exit(NE,BOSQUEURLOM+"cmon_4.c","road");
   add_exit(SO,BOSQUEURLOM+"cmon_7.c","road");
}
