// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_54.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_39.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_69.c","plain");
}
