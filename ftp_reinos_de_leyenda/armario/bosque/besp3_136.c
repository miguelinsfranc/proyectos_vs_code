// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(E,BOSQUEURLOM+"besp3_137.c","plain");
   add_exit(S,BOSQUEURLOM+"bosq3_58.c","plain");
}
