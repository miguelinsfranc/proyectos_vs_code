// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_51.c","plain");
   add_exit(NO,BOSQUEURLOM+"besp3_36.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_53.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_66.c","plain");
}
