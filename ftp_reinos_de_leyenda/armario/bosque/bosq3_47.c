// Gareth 27.09.03
// Bosque de Urlom, Urlom
// Sierephad Abrl 2k18 	-- 	Reajustando el mapeado de urlom
//https://www.reinosdeleyenda.es/foro/ver-tema/actualizacion-del-mapa-de-zmud/#post-301529

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior2");
	set_long(query_long()+"   Los árboles de esta zona son diferentes a los que te has "
		"encontrado en el resto del bosque. Gruesos robles han crecido fuertemente "
		"en este lugar.\n");
	fijar_bosque(4);
	fijar_tipo_arbol("roble");
   set_light(70);
   add_exit(SE,BOSQUEURLOM+"bosq3_1.c","bosque");
   add_exit(NE,BOSQUEURLOM+"bosq3_44.c","bosque");
}
