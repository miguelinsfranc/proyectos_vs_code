// Gareth 26.09.03
// Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_denso");
   set_light(50);
   add_exit(O,BOSQUEURLOM+"besp2_59.c","plain");
   add_exit(NO,BOSQUEURLOM+"besp2_52.c","plain");
   add_exit(N,BOSQUEURLOM+"bosq_53.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp2_54.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp2_67.c","plain");
   add_exit(S,BOSQUEURLOM+"besp2_66.c","plain");
}
