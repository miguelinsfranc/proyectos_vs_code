// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_119.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_109.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp3_131.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_130.c","plain");
}
