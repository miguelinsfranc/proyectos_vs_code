// Gareth 27.09.03
// Bosque de Urlom, Urlom
// Sierephad Abrl 2k18 	-- 	Reajustando el mapeado de urlom
//https://www.reinosdeleyenda.es/foro/ver-tema/actualizacion-del-mapa-de-zmud/#post-301529

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior2");
   set_light(70);
   add_exit(NE,BOSQUEURLOM+"bosq3_34.c","bosque");
   add_exit(SE,BOSQUEURLOM+"bosq3_40.c","bosque");
}
