// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(N,BOSQUEURLOM+"bosq3_5.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_7.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_6.c","plain");
}
