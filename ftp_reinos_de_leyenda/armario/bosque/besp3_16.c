// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(NE,BOSQUEURLOM+"besp3_10.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_26.c","plain");
}
