// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(NO,BOSQUEURLOM+"besp3_89.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_91.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_107.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp3_119.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_118.c","plain");
}
