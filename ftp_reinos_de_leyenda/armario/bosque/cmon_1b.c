// Gareth 07.10.03
// Camino a la MontañaDeVapor
// Bosque de Urlom
// Sierephad Abrl 2k18 	-- 	Reajustando el mapeado de urlom
//https://www.reinosdeleyenda.es/foro/ver-tema/actualizacion-del-mapa-de-zmud/#post-301529

inherit "/std/outside";
#include "/d/urlom/path.h"

void setup() {

   set_light(100);
   GENERADOR->generar_basico(TO, "camino_montaña");
   GENERADOR->poner_npcs(TO, "caminos_bosque");
   add_exit(AR,BOSQUEURLOM+"cmon_2.c","road");
   add_exit(O,BOSQUEURLOM+"cmon_1.c","road");
}
