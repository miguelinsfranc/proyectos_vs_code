// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior1");
   set_light(70);
   add_exit(SE,BOSQUEURLOM+"besp_28.c","plain");
   add_exit(SO,BOSQUEURLOM+"bosq_27.c","plain");
   
   add_sign("Un cartel de madera colgado de uno de los árboles.\n", "AVISO: la zon"
   	"a boscosa que se sitúa al este es peligrosa y alguna extraña magia habita e"
   	"n ella, por lo que se recomienda no entrar. Si alguien lo hace, es responsa"
   	"bilidad suya únicamente.\n", "cartel", "cartel");
}
