// Gareth 26.09.03
// Bosque de Urlom, Urlom

inherit "/d/urlom/handlers/salidas_random";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_denso");
   set_light(50);
   add_exit(NO,BOSQUEURLOM+"besp_35.c","plain");
   add_exit(N,BOSQUEURLOM+"besp_36.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp_37.c","plain");
   add_exit(E,BOSQUEURLOM+"besp_45.c","plain");
}
