// Gareth 26.09.03
// Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior1");
   set_light(70);
   add_exit(E,BOSQUEURLOM+"bosq_100.c","bosque");
   add_exit(SO,BOSQUEURLOM+"bosq_110.c","bosque");
}
