// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/d/urlom/handlers/salidas_random";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_denso");
   set_light(50);
   add_exit(E,BOSQUEURLOM+"besp_18.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp_30.c","plain");
   add_exit(S,BOSQUEURLOM+"besp_29.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp_28.c","plain");
}  

