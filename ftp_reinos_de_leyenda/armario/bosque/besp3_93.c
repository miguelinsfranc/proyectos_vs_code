// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom
// Eckol Ago16: Fuera random. La planta aparece según la luna. Le anyado la explicacion a Naiad tambien.

inherit "/d/urlom/handlers/salidas_random";
#include "/d/urlom/path.h"

#define CLIMA "/handlers/clima"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(N,BOSQUEURLOM+"besp3_80.c","plain");

	if(CLIMA->ciclo_argan() ==2)
	   add_clone(ITEMS+"pblanca",1);
}
