// Gareth 26.09.03
// Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior1");
   set_light(70);
   add_exit(NO,BOSQUEURLOM+"bosq_47.c","plain");
   add_exit(NE,BOSQUEURLOM+"bosq_48.c","plain");
   add_exit(S,BOSQUEURLOM+"besp2_60.c","plain");
   
   add_sign("Un cartel de madera colgado de uno de los árboles.\n", "AVISO: la zon"
   	"a boscosa que se sitúa al sur es peligrosa y alguna extraña magia habita en"
   	" ella, por lo que se recomienda no entrar. Si alguien lo hace, es responsab"
   	"ilidad suya únicamente.\n", "cartel", "cartel");
}
