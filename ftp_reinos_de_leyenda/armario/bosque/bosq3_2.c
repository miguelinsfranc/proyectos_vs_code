// Gareth 27.09.03
// Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior2");
   set_light(70);
   add_exit(O,BOSQUEURLOM+"bosq3_1.c","bosque");
   add_exit(SE,BOSQUEURLOM+"bosq3_7.c","bosque");
   add_exit(SO,BOSQUEURLOM+"besp3_0.c","bosque");
}
