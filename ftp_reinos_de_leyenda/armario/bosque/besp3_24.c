// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(NO,BOSQUEURLOM+"besp3_13.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_14.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_25.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_40.c","plain");
}
