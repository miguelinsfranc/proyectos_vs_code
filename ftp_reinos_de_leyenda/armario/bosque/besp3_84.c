// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(NO,BOSQUEURLOM+"besp3_71.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_73.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_98.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_97.c","plain");
}
