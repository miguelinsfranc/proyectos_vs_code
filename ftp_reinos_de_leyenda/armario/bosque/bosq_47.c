// Gareth 26.09.03
// Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior1");
   set_long(query_long()+"Hay un árbol cuya corteza se ve desplazada y podrías meter la mano.\n");
   set_light(70);
   add_exit(NO,BOSQUEURLOM+"bosq_40.c","bosque");
   add_exit(SE,BOSQUEURLOM+"bosq_53.c","bosque");
   add_exit(SO,BOSQUEURLOM+"bosq_51.c","bosque");
}

void init()
{
	::init();
	add_action("do_meter","meter");	
}

int do_meter(string str) {
   
   if(str=="mano"||str=="la mano")
   {
      if(TO->query_static_property("armario_abierto"))
         return notify_fail("Alguien ya ha movido previamente la corteza.\n");
      
      if(!random(2))
      {
      	tell_object(TP, "Metes la mano por la coteza y ves un hueco que contiene un plano.\n");
      	tell_accion(TO, TP->query_short()+" mete la mano por la corteza y saca un plano.\n", 
      		"", ({TP}), TP);
      	clone_object("/baseobs/inventores/planos/pistola")->move(TO);
      }
      else
      {
      	tell_object(TP, "Metes la mano por la corteza y ves un hueco que está vacío hoy.\n");
      	tell_accion(TO, TP->query_short()+" mete la mano por la corteza y saca la mano vacía.\n", 
      		"", ({TP}), TP);
      }
      TP->accion_violenta();
      TO->add_static_property("armario_abierto",1);
      return 1;
   }
   else
   	return notify_fail("Podrías meter la mano.\n");
}
