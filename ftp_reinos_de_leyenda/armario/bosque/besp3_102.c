// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom
// Eckol Abr18: maestro bribones.

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup()
{

    set_light(50);
    GENERADOR->generar_basico(TO, "bosque_profundo");
    add_exit(N, BOSQUEURLOM + "besp3_87.c", "plain");
    add_exit(NE, BOSQUEURLOM + "besp3_88.c", "plain");
    add_exit(SE, BOSQUEURLOM + "besp3_116.c", "plain");
    add_exit(SO, BOSQUEURLOM + "besp3_115.c", "plain");
    add_clone(NPCS+"iknom");
}
