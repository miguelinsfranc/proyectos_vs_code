// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior1");
   set_light(70);
   add_exit(O,BOSQUEURLOM+"bosq_13.c","bosque");
   add_exit(SE,BOSQUEURLOM+"bosq_24.c","bosque");
   add_exit(S,BOSQUEURLOM+"bosq_23.c","bosque");
}
