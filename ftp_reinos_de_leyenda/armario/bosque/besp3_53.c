// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_52.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_37.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_54.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp3_68.c","plain");
}
