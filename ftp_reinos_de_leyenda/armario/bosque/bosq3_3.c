// Gareth 27.09.03
// Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior2");
   set_light(70);
   add_exit(N,BOSQUEURLOM+"bosq3_0.c","bosque");
   add_exit(SE,BOSQUEURLOM+"bosq3_8.c","bosque");
}
