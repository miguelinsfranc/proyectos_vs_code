// Gareth 27.09.03
// Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior2");
   set_light(70);
   add_exit(NE,BOSQUEURLOM+"bosq3_79.c","bosque");
   add_exit(SO,BOSQUEURLOM+"bosq_75.c","bosque");
}
