// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_77.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_64.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_65.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp3_91.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_90.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_89.c","plain");
}
