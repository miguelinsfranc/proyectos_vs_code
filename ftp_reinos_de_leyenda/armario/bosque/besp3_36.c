// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_35.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_21.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp3_52.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_50.c","plain");
}
