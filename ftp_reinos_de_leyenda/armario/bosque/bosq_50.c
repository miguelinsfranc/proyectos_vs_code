// Gareth 26.09.03
// Bosque de Urlom, Urlom
// Dalion, añadimos el cráter que se convertirá en la entrada a la capilla de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

	set_short("%^GREEN%^Bosque de Urlom: Cráter%^RESET%^");
	set_long("La monotonía del bosque queda quebrada por este gigantesco cráter que tienes a tus pies. "
	"Apenas ya hay restos de broza y leña esparcidas por el suelo, todo ha sido pasto de las llamas producidas "
	"por alguna brutal explosión. El olor a pólvora quemada se hace a ratos insoportable... "
	"lo que aquí ocurriera tuvo que ser mayúsculo.\nEn lo hondo del cráter se puede llegar a distinguir lo que parecen "
	"los restos de unas ruinas de algún tipo de edificación de piedra...\n");
	fijar_bosque(2);
	set_exit_color("naranja");
	set_zone("bosque_urlom");
	add_clone(NPCS+"bosque_animales.c",roll(2,2));
	add_item("hierba", "Una capa de hierba verde cubre el suelo.\n");
	add_item(({"arboles","árboles"}), "La mayoría de árboles aun se en"
	"cuentran en buen estado, a pesar del rastro de desolación que invade el "
	"lugar.\n");
	add_item(({"cráter", "crater"}), "Ves un enorme cráter en el suelo. Está lleno de restos de árboles, piedras, y lo que parecen "
	"unas ruinas de piedra de algún edificio que quedó sepultado por la tierra tiempo atrás...\n");
	fijar_luz(70);
	add_exit(NO,BOSQUEURLOM+"bosq_43.c","bosque");
	add_exit(SO,BOSQUEURLOM+"bosq_57.c","bosque");
}
