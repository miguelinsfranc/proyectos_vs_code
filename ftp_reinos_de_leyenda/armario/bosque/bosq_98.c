// Gareth 26.09.03
// Bosque de Urlom, Urlom
// Sierephad Abrl 2k18 	-- 	Reajustando el mapeado de urlom
//https://www.reinosdeleyenda.es/foro/ver-tema/actualizacion-del-mapa-de-zmud/#post-301529

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior1");
   set_light(70);
   add_exit(O,BOSQUEURLOM+"bosq_96.c","bosque");
   add_exit(S,BOSQUEURLOM+"bosq_82.c","bosque");
}
