// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(NE,BOSQUEURLOM+"besp3_3.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_15.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_14.c","plain");
   add_clone("/d/golthur/npcs/shamino", 1); // Dunkelheit 05-02-2007
}
