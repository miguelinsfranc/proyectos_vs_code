// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_58.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_43.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_44.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp3_74.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_73.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_72.c","plain");
}
