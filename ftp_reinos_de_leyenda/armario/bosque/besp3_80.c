// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

object unicornio;

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(NO,BOSQUEURLOM+"besp3_66.c","plain");
   add_exit(N,BOSQUEURLOM+"besp3_67.c","plain");
   add_exit(NE,BOSQUEURLOM+"besp3_68.c","plain");
   add_exit(SE,BOSQUEURLOM+"besp3_94.c","plain");
   add_exit(S,BOSQUEURLOM+"besp3_93.c","plain");
   modify_exit(S,({"function","pasar"}));
   add_exit(SO,BOSQUEURLOM+"besp3_92.c","plain");
   
   unicornio=clone_object(NPCS+"unicornio");
   unicornio->move(TO);
}

int pasar() {
   
   if(!secure_present("/d/urlom/npcs/unicornio", TO) || !TP->query_player() || TP->query_hidden()) return 1;
   if(TP->dame_alineamiento() < -10)
   {
      tell_object(TP, "El Unicornio te abre paso: tu corazón es puro.\n");
      tell_room(TO, "El Unicornio abre paso a "+TP->query_short()+".\n", TP);
      return 1;
   }
   else return notify_fail("El Unicornio te dice: sólo si tu corazón es puro te dejaré pasar.\n");
}