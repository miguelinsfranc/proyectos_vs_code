// Gareth 26.09.03
// Camino al Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "desfiladero");
   GENERADOR->poner_npcs(TO, "caminos_bosque");
   set_light(100);
   add_exit(NO,BOSQUEURLOM+"cb_85.c","road");
   add_exit(SE,BOSQUEURLOM+"cb_94.c","road");
   add_exit(SO,BOSQUEURLOM+"cb_93.c","road");
}
