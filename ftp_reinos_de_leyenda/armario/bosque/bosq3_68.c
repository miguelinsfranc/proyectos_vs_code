// Gareth 27.09.03
// Bosque de Urlom, Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   GENERADOR->generar_basico(TO, "bosque_exterior2");
   set_light(70);
   add_exit(NO,BOSQUEURLOM+"bosq3_59.c","bosque");
   add_exit(E,BOSQUEURLOM+"bosq3_69.c","bosque");
}
