// Gareth 30.09.03
// Zona Espesa del Bosque de Urlom

inherit "/std/bosque";
#include "/d/urlom/path.h"

void setup() {

   set_light(50);
   GENERADOR->generar_basico(TO, "bosque_profundo");
   add_exit(O,BOSQUEURLOM+"besp3_90.c","plain");
   add_exit(NO,BOSQUEURLOM+"besp3_78.c","plain");
   add_exit(E,BOSQUEURLOM+"besp3_92.c","plain");
   add_exit(SO,BOSQUEURLOM+"besp3_106.c","plain");
}
