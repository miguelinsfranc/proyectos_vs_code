#include "base_campamento.h"

inherit "/obj/monster";
#define SHADOW_ESCONDERSE "/habilidades/shadows/esconderse_sh.c"

void setup()
{
	set_name("Kyrha");
	add_alias("kyrha");
	set_short("Kyrha Fuegofatuo");
	set_main_plural("Kyrhas");
	
	set_long(
		"Ante tus ojos se encuentra una joven Zoridiana de aspecto vulpino.\n"
		"Su cuerpo esbelto y grácil se despliega con una elegancia cautivadora, revelando la singular"
		"belleza de su especie. Un suave y corto pelaje rojizo envuelve su figura, pero lo que "
		"verdaderamente capta la atención son los finos trazos de pelaje blanco entrelazados con maestría "
		"sobre su cuerpo. Estos delicados trazos de blancura parecen formar auténticos tatuajes naturales, "
		"creando patrones fascinantes que te invitan a perderse en ellos. Fluyen y toman vida propia con cada "
		"movimiento de la Zoridiana, como si narraran una historia oculta en su piel.\n"
		"Su cabello cae en cascadas blancas como la nieve, rozando el inicio de sus colas, enmarcando su rostro con suavidad y aportando un contraste encantador con su pelaje carmesí. Cada mechón parece "
		"meticulosamente colocado, resaltando la textura sedosa y brillante de sus hebras. "
		"Las distintivas orejas de zorro de los Zoridians se alzan con exquisita simetría sobre su cabeza. "
		"Blanquecinas por dentro y revestidas con un suave pelaje rojizo por fuera, estas orejas son "
		"auténticas obras de arte natural, cuidadosamente esculpidas en forma y tamaño. Su suavidad y "
		"sensibilidad son palpables, como si pudieran captar incluso el susurro más tenue del viento o el "
		"crujido de una hoja.\n"
		"Observando con detenimiento, se aprecia que la joven Zoridiana posee múltiples colas de zorro, unas "
		"seis en total, un distintivo inconfundible de su especie. Estas colas, largas, esponjosas y "
		"prensiles, se extienden graciosamente detrás de ella, simbolizando su experiencia y poder entre su gente.\n"
		"Su rostro revela una delicadeza cautivadora. Una pequeña nariz, casi etérea en su sutileza, resalta "
		"junto a sus ojos rasgados. Estos ojos, con iris intensamente azules y pupilas blancas, transmiten "
		"una mirada magnética que parece penetrar hasta el fondo de tu ser. Los labios de la Zoridiana, por "
		"su parte, son suaves y voluptuosos, de un delicado tono rosado.\n"
		"Las manos de la Zoridiana son delicadas, pero no pasan desapercibidos los callos en sus dedos, "
		"evidencia de su familiaridad con el trabajo físico. Sus uñas, afiladas con precisión, reflejan una "
		"atención minuciosa hacia los detalles. La musculatura fibrosa de su esbelto cuerpo sugiere una "
		"fuerza ágil y se combina armoniosamente para resaltar sus curvas femeninas visiblemente definidas "
		"bajo su vestimenta.\n"
	);

fijar_clase("guardabosques");
fijar_raza("humano");
	fijar_bando("mercenario");
	fijar_religion("ateo");
	fijar_alineamiento(-200);
	fijar_genero(2);

	fijar_fue(18);
	fijar_des(28);
	fijar_con(18);
	fijar_int(28);
	fijar_sab(20);
	fijar_car(18);

	ajustar_bo(214);
	ajustar_be(181);
	ajustar_bp(300);

fijar_carac("daño", 104);
fijar_pvs_max(25000);

	nueva_resistencia("fuego", 100, 0);
	nueva_resistencia("magico", 100, 0);
	nueva_resistencia("mental", 100, 0);
	nueva_resistencia("frio", -30, 0);

add_static_property("zoridian_fundador", 1);
add_loved("propiedad","zoridian_fundador");

	add_clone("/w/gholxien/programacion_de_contenido/bandidos_urlom/armas/susurro_ponzoñoso.c", 2);
add_clone(ARMADURASBASE + "capucha", 1);	
add_clone(ARMADURASBASE + "camiseta", 1);	
add_clone(ARMADURASBASE + "pulsera", 1);	
add_clone(ARMADURASBASE + "pulsera_izq", 1);	
add_clone(ARMADURASBASE + "manto", 1);	
add_clone(ARMADURASBASE + "guante", 1);	
add_clone(ARMADURASBASE + "guante_izq", 1);	
add_clone(ARMADURASBASE + "cinturon", 1);	
add_clone(ARMADURASBASE + "pantalones", 1);	
add_clone(ARMADURASBASE + "zapatillas", 1);	

fijar_maestrias((["Penetrantes ligeras": 100]));

	set_aggressive(10, 2);

	fijar_nivel(77);

	anyadir_ataque_pnj(100, "estocada");
	anyadir_ataque_pnj(80, "apunyalar_escondido", (: ! (dame_bloqueo_combate("Bloqueo:habilidad:apuñalar") && query_timed_property("apunyaladoreciente")) :)); 
	anyadir_ataque_pnj(75, "herir");
	

	init_equip();

fijar_objetos_morir(({"tesoro", "varios", ({3, 4}), 1}));
 fijar_objetos_morir(({"/w/gholxien/programacion_de_contenido/bandidos_urlom/armas/susurro_ponzoñoso.c", 1, 25}));
}

void escondete()
{
	if ( TO->dame_shadow_esconderse() ) {
		return;
	}

	clone_object(SHADOW_ESCONDERSE)->setup_sombra(this_object(),60,0);
	set_hidden(1);
}

function apunyalar_escondido(object *objetivos) {
	object objetivo = element_of(objetivos);
	/*TO->stop_fight(objetivo);
	objetivo->stop_fight(TO);*/
	tell_accion(environment(),query_short() + " desaparece en una nube de humo mágico.\n","",({TO}),TO);
	escondete();
	if (habilidad("apuñalar",objetivo)) add_timed_property("apunyaladoreciente",1,30);
	
}