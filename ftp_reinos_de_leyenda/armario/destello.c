// Hazrakh 14/06/2023

inherit "/obj/arma.c";

void setup()
{
	fijar_arma_base("lanza");
	set_name("lanza");
	add_alias(({"lanza", "destello", "sombraluna", "destello sombraluna"}));
	add_plural(({"lanzas", "sombralunas", "destellos", "destellos sombralunas"}));
	set_short("Destello Sombraluna");
	set_main_plural("Destellos Sombraluna");
	set_long("Esta arma encarna la fusión perfecta entre la estética y la funcionalidad. Esculpida con virtuosismo en ébano y engalanada con finos trazos de plata, exhala una innegable letalidad. La lanza, de longitud equilibrada y elegante, ofrece un manejo ágil y preciso, sin resultar pesada o aparatosa. La empuñadura, revestida en un cuero suave y resistente, se moldea ergonómicamente para lograr una simbiosis sin igual entre la lanza y la mano de quien la empuña, mientras que  un pomo ponderado, en el extremo inferior, acentúa la sensación de control absoluto.\n"
	"En el extremo superior de la lanza un intrincado adorno metálico de plata se enrosca en una danza sinuosa, formando delicadas espirales que capturan la luz en sutiles destellos. En el centro de este diseño adornado, un oscuro azabache reposa en una montura engastada con destreza. La gema parece poseer vida propia mientras palpita suavemente en presencia de otros seres vivos. Esta peculiar cualidad permite al arma actuar como una extensión del instinto del portador, mejorando su precisión y habilidad al blandirla, superando las capacidades habituales de un mago como si una mano invisible guiase sus golpes.\n"
	"Una enredadera de sinuosos zarcillos se despliega desde el corazón del azabache hasta la base de la lanza y la mano del portador, pulsando de manera acompasada a través de la madera. Al entrar en contacto con otro ser vivo, los zarcillos irradian un destello ámbar, absorbiendo y transportando la vitalidad del objetivo para revitalizar al poseedor del arma.\n");

	fijar_genero(2);
	fijar_encantamiento(60);
	fijar_valor(3500);

	ajustar_BO(42);
	ajustar_BE(10);
	ajustar_BP(30);

	nuevo_ataque("tabla", "penetrante", 1, 60, 25);
	nuevo_ataque("magico", "mental", 2, 20, 25);

	add_static_property("poder_magico", 25);
	add_static_property("nivel_minimo", 20);
	add_static_property("messon", "Un pulso despierta en la gema que corona el arma, serpenteando por los retorcidos zarzillos hasta encontrarse con tus manos y sincronizarse con tus latidos.\n");

	nuevo_efecto_basico("ataques_rapidos", 0, 0, 30);
	nuevo_efecto_basico("quemadura");
}
