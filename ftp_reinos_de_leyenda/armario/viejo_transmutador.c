/*
Astel. 27.06.2014
  Balance:
    + NPC que teleporta de naggrung a dalaensar y viceversa.
    - Te pide que le busques un pergamino arcano de nivel = tunivel/8
    - Los no hechiceros tienen la dificultad extra de que no ven el nombre del pergamino pero lo pueden intentar comprar a ciegas.
    - Solo teleportará a un jugador a la vez. Una vez teleportado el hechicero hará el viaje con él también, con lo que para que alguien viaje después, debe estar en el otro lado.
    - Desde el otro lado pedirá otro pergamino aleatorio para viajar.
    - El hechicero tendrá el lock del teleportar igual que cualquier jugador.
    - Al clonarse se pondra aleatoriamente en uno de los dos lados.

Astel 28.06.2014
    - Ahora lleva a todo el grupo del jugador que le consigue el pergamino (max 5)

Idea:
    - Quest para conseguirle al viejo 3 objetos +1 int (Lanza diamante, Capucha Tenebrosa, Grimorio, Anillo Lish, Pensante...) a cambio de que te teleporte sin pergaminos a partir de ese momento.

*/

#include <spells.h>
#include <tiempo.h>

//#define A_ESCUELAS ({ABJURACION,EVOCACION,ILUSION,ALTERACION,CONJURACION,ENCANTAMIENTO,ADIVINACION,NECROMANCIA})
#define DIR_PERGA "/baseobs/pergaminos/magico/"
//#define FORMULAR "/cmds/npc/formular"
//#define LUGAR1 "/w/astel/workroom"
//#define LUGAR2 "/w/comun"
#define LUGAR1 "/d/urlom/zonas/bosque/besp2_73"
#define LUGAR2 "/d/naggrung/salas/tundra/tundra_39"

inherit "/obj/conversar_npc";

string* pergaminos = get_dir(DIR_PERGA);

string dame_continente_destino()
{
  if("/handlers/fronteras.c"->dame_datos_frontera_jugador(environment())[2] == "Dalaensar")
    return "Naggrung";
  else return "Dalaensar";
}

void setup()
{
    set_name("viejo");
    set_short("Viejo misterioso");
    add_alias(({"viejo","misterioso"}));
    set_main_plural("Viejos misteriosos");
    set_long("Bajito, decrépito y algo encorvado. Con el pelo blanco, desaliñado y grasiento, los ojos pequeños y negros, la cara llena de arrugas y un gesto de mal humor de forma perpetua en el rostro, así es el hombre que tienes ante ti. Un hombre nada amistoso que a simple vista dirías que podría masticar piedras con los dientes.\n");
    fijar_nivel(50);
    fijar_clase("transmutador_negro");
    fijar_raza("humano");
    set_random_stats(18,22);
    fijar_carac("int",25);
    fijar_pvs_max(16000);
    //fijar_especializacion("transmutador_negro");
    //add_spheres((["mayor":({"evocacion"})]));
    add_spell("teleportacion");
    nuevo_lenguaje("adurn",100);
    nuevo_lenguaje("dendrita",100);
    nuevo_lenguaje("drow",100);
    nuevo_lenguaje("negra",100);
    set_language("dendrita");
    nuevo_dialogo("viejo","No te fies de las apariencias joven ignorante, este viejo que ves aquí de seguro podría hacerte temblar de miedo.");
    nuevo_dialogo("miedo",({":sonríe disimuladamente.","'Eso es, soy un experimentado transmutador. Podría desintegrarte ahora mismo si quisiera. Así que mejor no jueges con mi paciencia."}),"viejo");
    nuevo_dialogo("transmutador","Eso he dicho. Poderosos hechiceros especializados en alteración. No llegarás a conocer a muchos como yo. No se suelen ver facilmente por ahí excepto los que van en busca de conocimiento arcano.","miedo");
    nuevo_dialogo("busca","Yo vengo de lejanas tierras buscando pergaminos mágicos con los que poder incrementar aún más mi poder.","transmutador");
    nuevo_dialogo("poderoso","Pocos hechiceros conocen hechizos tan magníficos como los que conozco yo. Gracias a mi teleportación soy capaz de estar en cualquier parte de Eirea en un instante. Así es como he llegado hasta aquí.","transmutador");
    nuevo_dialogo("teleportacion","¿No te lo crees? Que estúpid"+this_player()->dame_vocal()+" eres. Puedo demostrártelo cuando quiera. En cuanto consiga lo que he venido a buscar lo verás.","poderoso");
    nuevo_dialogo("consiga",({"pergamino"}),"teleportacion");
    habilitar_conversacion();
    add_clone("/baseobs/armaduras/capucha.c",1);
    add_clone("/baseobs/armaduras/tunica.c",1);
    add_clone("/baseobs/armaduras/botas.c",1);
    add_clone("/baseobs/armaduras/cinturon.c",1);
    add_clone("/baseobs/armas/baston.c",1);
    
    fijar_objetos_morir(({"tesoro", "pergamin", ({2, 5}), 1 }));    
    fijar_objetos_morir(({"tesoro", "varios"  , 3, 1 }));

    set_move_after(10,5);
    add_attack_spell(80,"desintegrar",3);
    add_attack_spell(100,"cuerpo de acero",1);
    add_attack_spell(100,"apresuramiento",1);
    add_attack_spell(50,"inversion gravitatoria de exodus",0);
    add_attack_spell(100,"proyectil magico mayor",3);
    add_attack_spell(60,"meteoros diminutos de ignis",0);
    add_attack_spell(100,"debilidad magico",3);
    init_equip();
    call_out("chequeo_unico",0);
}

void chequeo_unico() {
    //object l1=load_object(LUGAR1),l2=load_object(LUGAR2);
    string filname = real_filename(this_object());
    status duplicado = 0;
    /*
    if(environment() == l2 && secure_present(filname,l1))
     duplicado=1;
    if(environment() == l1 && secure_present(filname,l2))
       duplicado=1;*/
     if(2<sizeof(children(filname)))
        duplicado=1;

     if(duplicado) {
        tell_accion(environment(),"El viejo desaparece en una nube de humo.\n","Oyes un pequeño estallido.\n");
        dest_me();
        return;
     }
     //add_static_property("no_clean_up",1);
     //l1->add_static_property("no_clean_up",1);
     //l2->add_static_property("no_clean_up",1);
}

// void colocar()
// {
    // if(CLIMA->query_mes()%2)
        // move(load_object(LUGAR1));
    // else
        // move(load_object(LUGAR2));
// }


void pergamino(object jugador, string tema)
{
    string *pergaminos_aux = ({}),perg;
    object obperga;
    int nivel = jugador->dame_nivel()/8,i;
    string aux,vocal=jugador->dame_vocal();

    if (perg=jugador->query_property("perg_encargado")){
        do_say("¿Aún no me has conseguido el "+perg->query_short_original()+" que te pedí? Pareces más inútil de lo que pensaba.",0);
        return;
    }



    if(nivel>9) nivel=9;

    foreach(perg in pergaminos) {
        if (perg[<2..] != ".c")
            continue;
        obperga = load_object(DIR_PERGA+perg);
        if (obperga->dame_nivel_hechizo() == nivel)
            pergaminos_aux += ({perg});
    }

    if (!nivel) {
      do_say("Pareces demasiado inexpert"+vocal+". No creo que puedas conseguirme lo que quiero. Mejor lárgate y no me hagas perder el tiempo.",0);
      return;
    }

    if (!i= sizeof(pergaminos_aux))
        return;

    perg=pergaminos_aux[random(i)];
    obperga= load_object(DIR_PERGA+perg);
    do_say("Mmmm, veo que estás interesad"+vocal+".... Escúchame bien, te propongo un trato: si te interesa cruzar el océano y viajar hasta "+dame_continente_destino()+" de forma segura, puedo ayudarte. Os teleportaré a tí y a tu grupo. Obviamente quiero algo a cambio.",0);
    switch(nivel) {
        case 1: aux = "No creo que puedas hacer grandes cosas. Te pediré algo fácil: estoy buscando un "+obperga->query_short_original()+". Seguramente puedas encontrarlo en alguna tienda de magia."; break;
        case 2: aux = "Pareces bastante seguro de tí mismo. Veremos si eres capaz de conseguirme un "+obperga->query_short_original()+" que necesito."; break;
        case 3: aux = "Te crees muy buen"+vocal+", ¿eh?. Búscame entonces un "+obperga->query_short_original()+" y hablaremos."; break;
        case 4..: aux = "Parece que ya eres mayorcit"+vocal+". Cuando me traigas un "+obperga->query_short_original()+" cumpliré mi parte del trato."; break;
    }
    do_say(aux,0);
    do_say("Si pasa demasiado tiempo y no apareces por aquí con el pergamino me olvidaré de nuestro trato.",0);
    jugador->add_timed_property("perg_encargado",DIR_PERGA+perg[0..<3],DIA*5);
}

string query_notify_fail_msg()
{
    string tx= ::query_notify_fail_msg();
       this_object()->do_say("No consigo teleportarme: "+tx);
    return tx;
}

string visualizar_destino(int random)
{
    string destino;
    string conti_dest = dame_continente_destino();
    // Preparar en la memoria el lugar del otro extremo
    if (!random)
        if(conti_dest == "Naggrung")
            add_property("memorizar",({LUGAR1,LUGAR1->query_short()}));
        else
            add_property("memorizar",({LUGAR2,LUGAR2->query_short()}));
    else {
        if(conti_dest == "Naggrung")
            destino = "/handlers/fronteras.c"->dame_sala_aleatoria_frontera(({0,0,0,"Naggrung","Bosques","Tundra"}));
        else
            destino = "/handlers/fronteras.c"->dame_sala_aleatoria_frontera(({0,0,0,"Urlom","Bosque","Bosque de Urlom"}));
        add_property("memorizar",({destino,destino->query_short()}));
    }
    return conti_dest;
}

object* dame_grupo_viajero(object quien)
{
    object lider;
    if(lider=quien->dame_grupo_seguir())
        return lider->dame_agrupados();
    else
        return ({quien});
}

int chequeo_todos_listos(object quien)
{
    object *todos = dame_grupo_viajero(quien);
    foreach(object o in todos){
        if(o->query_fighting()){
            do_say(o->query_short()+", no puedo llevarte conmigo si estás en combate con alguien.\n",0);
        return 0;
        }
        if(!o->dame_consentir("hechizos")){
            do_say(o->query_short()+", debes tener el consentir hechizos activado para poder llevarte conmigo.\n",0);
        return 0;
        }
    }
    return 1;
}


int event_dar(object quien, object* que) {

  string perg_encargado=quien->query_property("perg_encargado");

  if(!perg_encargado)
    return 0;

  if(sizeof(que)>1){
    do_say("Lo siento. No puedo aceptar tantas cosas de golpe.",0);
    return 0;
  }

  if(real_filename(que[0]) != perg_encargado) {
    do_say("Ese no es el pergamino que te he pedido que me traigas. ¿Acaso crees que vas a conseguir engañarme?",0);
    return 0;
  }

  do_say("¿Así que lo has conseguido? No pensaba que fueras capaz. Ahora cumpliré mi parte. Utilizaré mi teleportación para llevarte a "+dame_continente_destino()+". Prepárate.",0);

    if(!chequeo_todos_listos(quien)) {
        do_say("Vuélvelo a intentar cuando estés preparad"+quien->dame_vocal()+".",0);
        return 0;
    }

  call_out("teleportar",0,quien,que[0]);

  return 2;
}


void teleportar(object quien, object regalo)
{
    string teleport_spell = INDICE("teleportacion");
    string nombres = implode(dame_grupo_viajero(quien)->query_name(),",");
    string destino;

    destino = visualizar_destino(1);

    //queue_action("formular teleportacion:"+quien);
    //do_command("formular teleportacion:"+quien->query_name());
    //FORMULAR->cmd("teleportacion:"+quien->query_name(),this_object(),"formular");
    if(1> (teleport_spell->cast_spell(nombres,this_object(),0,0)))
    {
        do_say("Lo siento pero no pude lograr completar el hechizo con éxito.\n",0);
        query_notify_fail_msg();
        return;
    }
    if (regalo->dame_dosis())
        regalo->consumir_dosis();
    else
        regalo->dest_me();
    quien->remove_property("perg_encargado");
    write_file("/d/urlom/logs/teleport_viejo",quien->query_cap_name()+" teleportado a "+destino+" en "+ctime()+"\n");

}
