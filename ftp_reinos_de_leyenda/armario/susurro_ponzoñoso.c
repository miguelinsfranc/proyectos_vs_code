// Hazrakh 14/06/2023

inherit "/obj/arma.c";

string dame_nombre_material() { return "obsidiana"; }

void setup()
{
	fijar_arma_base("daga");
	set_name("daga");
	add_alias(({"daga","susurro","ponzoñoso","susurro ponzoñoso"}));
	add_plural(({"dagas", "obsidianas", "susurros", "ponzoñosos","susurros ponzoñosos"}));
	set_short("%^BOLD%^MAGENTA%^Susurro Ponzoñoso%^RESET%^");
	set_main_plural("Susurros ponzoñosos");
	set_long("Es una obra maestra de oscura elegancia. Su hoja, forjada con precisión a partir de la volcánica y reluciente obsidiana, posee un brillo negro intenso y pulido. Su superficie es suave y reflectante, como si fuera un espejo negro que captura la luz y la absorbe en su interior. Su hoja se extiende en una línea recta y estrecha, pero a medida que se acerca a la empuñadura se ensancha ligeramente, creando un equilibrio perfecto entre filo y peso.\n"
	"La empuñadura de la daga está envuelta en una suave y negra piel de cuero que proporciona un agarre firme y cómodo. La textura de este material contrasta con el brillo liso de la hoja, mientras que la guarda de la daga, tallada con precisión, es una estructura delgada pero resistente que protege la mano del portador sin obstaculizar el movimiento. Algunas runas de abjuración terminan de completar el aspecto de la empuñadura, resaltando con un tono purpúreo sobre el cuero negro.\n"
	"Sus formas angulares y afiladas, reminiscencia de las facetas de un cristal, no logran disimular del todo un pequeño compartimento oculto en el pomo del arma. Este compartimento se encuentra hábilmente disimulado y solo es revelado al deslizar una pequeña placa metálica. En su interior, se encuentra una cavidad hueca que se extiende a lo largo del arma siguiendo la longitud de la hoja hasta llegar a la punta de la daga.\n");

	fijar_genero(2);
	fijar_encantamiento(30);
	fijar_valor(2000);
	fijar_material(6);

	ajustar_BO(5);
	ajustar_BE(5);

	add_static_property("nivel_minimo", 20);
	add_static_property("messon", "Un sutil cosquilleo recorre tus dedos, recordándote la naturaleza letal del arma que tienes ahora en tus manos.\n");
	add_static_property("envenenar", 6);
	add_static_property("negacion-aliento", 10);
	add_static_property("tipo", ({"bribon"}));

	nuevo_ataque("tabla", "penetrante", 3, 18, 3);
	nuevo_ataque("magico_veneno", "veneno", 2, 20, 2);

	nuevo_efecto_basico("descarga", 300, 0, 40, 0, 0, 0, 0, "veneno");
	nuevo_efecto_basico("antimagia", 15, 0, 90, 0, 0, 0, 0, 1);
}
