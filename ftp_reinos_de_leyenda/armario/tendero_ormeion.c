//Dalion 9-10-06
//Tendero de Ak'Anon
/*
	Zoilder 11/06/2011:
		- Le añado ciudadanía y estatus en ak'anon.
		- Le añado que su idioma actual sea el gnomo.
*/

inherit "/obj/monster";

int dame_tendero() { return 1; }

void setup()
{
	set_name("tendero");
	set_short("Tendero gnomo");
	add_alias(({"tendero","gnomo"}));
	add_plural(({"tenderos","gnomos"}));
	set_long("Un enjuto gnomo, de delgado cuerpo y aspecto alegre.\n");
	fijar_lenguajes((["gnomo":100]));
	fijar_lenguaje_actual("gnomo");
	fijar_raza("gnomo");
	fijar_peso(50000);
	set_random_stats(3,18);
	fijar_nivel(1);
	fijar_ciudadania("ak'anon");
	fijar_estatus("Ak'Anon",300);
	
	add_hated("enemigo",  ({"ak'anon"}));
	add_hated("relacion", ({"ak'anon"}));
	set_aggressive(6);
	
	add_clone("/baseobs/armaduras/zapatos",1);
	init_equip();
}