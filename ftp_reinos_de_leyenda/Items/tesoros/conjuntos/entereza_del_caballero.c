// Satyr 08/05/2018 11:51
#include <baseobs_path.h>
#include <depuracion.h>
#undef BASE
#include <habilidades_acciones.h>
inherit "/obj/conjunto_nuevo.c";

#define BLOQUEOS (["sanar":0.5, "drenaje":0.5])

void setup()
{
    // El bloqueo se controlará en la función de más abajo, pero necesitamos
    // esto para la ayuda
    function f = function(){};

    set_short("%^WHITE%^BOLD%^Entereza del %^Caballero%^RESET%^");
    fijar_genero(1);
    add_alias("conjunto");

    fijar_piezas(({
        BTESOROS_ARMADURAS + "brazal_perlado_sanador.c",
        BTESOROS_ARMADURAS + "brazal_perlado_castigador.c",
    }));
    nuevo_beneficio_funcion(2, f);
}

string dame_ayuda_especial(int piezas, mixed beneficio)
{
    string tx;

    tx = sprintf(
        "Bloqueos de la habilidad 'sanar' y 'drenaje' reducido en un %.2f%%.\n",
        to_float(BLOQUEOS["sanar"] * 100));
    /*tx += sprintf(
        " - Porcentaje drenado con cada golpe de la habilidad 'drenaje' "
        "incrementado en un %.2f%%.\n",
        to_float(BLOQUEOS["drenaje"]));*/

    return tx;
}

void ejecutar_gancho_acciones(
    object habilidad, class ejecucion e, string gancho, mixed params...)
{
    string hb;

    if (!e->pj || !habilidad) {
        return;
    }

    if (-1 == member_array(hb = habilidad->query_name(), keys(BLOQUEOS))) {
        return;
    }

    if (2 > dame_piezas_equipadas(e->pj)) {
        return;
    }

    if (hb == "sanar" && gancho == "terminar_habilidad_sanar") {
        __debug_x(sprintf("%s: %d", e->pj->query_name(), e->datos->bloqueo));
        e->datos->bloqueo = to_int(e->datos->bloqueo * BLOQUEOS[hb]);
        __debug_x(sprintf("%s: %d", e->pj->query_name(), e->datos->bloqueo));
        return;
    }

    // La reducción del drenaje está a pelo en la propia habilidad.
}

/**
 * Funciones usadas desde el drenaje
 * Retirar cuando éste se pase al nuevo sistema de acciones
 */
mapping dame_bloqueos_conjunto()
{
    return BLOQUEOS;
}
int puede_beneficiarse_conjunto(object pj)
{
    return dame_piezas_equipadas(pj) >= 2;
}
