// Satyr 08/05/2018 11:51
#include <spells.h>
inherit "/obj/conjunto_nuevo.c";

#define HANDLER_TS "/table/tiradas_de_salvacion.c"

void setup()
{
    set_short(
        "%^RED%^T%^CYAN%^r%^BLACK%^BOLD%^o%^NOBOLD%^ORANGE%^feo "
        "%^RED%^D%^CYAN%^r%^BLACK%^BOLD%^a%^NOBOLD%^ORANGE%^cónico%^RESET%^");
    fijar_genero(1);
    add_alias("conjunto");

    fijar_conjunto_parcial();
    fijar_piezas(
        ({"/d/urlom/armaduras/collar_colmillo_lessirnak.c",
          "/d/orgoth/armaduras/brazalete_dragon_rojo.c",
          "/habilidades/objetos/recetas/objetos_creados/munyequera_dragon.c",
          "/habilidades/objetos/recetas/objetos_creados/botas_dragon_negro.c",
          "/d/naggrung/armaduras/coraza_dragon_tortuga.c",
          "/d/naggrung/armaduras/capa_dragon_marino.c",
          "/baseobs/tesoros/armaduras/garra_dragon_amatista.c"}));

    nuevo_beneficio_caracteristica(2, "bo", 5);
    nuevo_beneficio_caracteristica(2, "be", 5);

    nuevo_beneficio_caracteristica(4, "daño", 5);
    nuevo_beneficio_caracteristica(4, "poder_magico", 5);
    foreach (string _res in RESISTENCIAS) {
        nuevo_beneficio_caracteristica(4, _res, 3);
    }

    nuevo_beneficio_caracteristica(5, "armadura_magica", 5);
    nuevo_beneficio_caracteristica(5, "daño", 5);
    nuevo_beneficio_caracteristica(5, "poder_magico", 5);

    nuevo_beneficio_caracteristica(6, "armadura_magica", 5);
    foreach (string _ts in HANDLER_TS->dame_tipos_ts(1)) {
        nuevo_beneficio_caracteristica(6, _ts, 3);
    }
}
