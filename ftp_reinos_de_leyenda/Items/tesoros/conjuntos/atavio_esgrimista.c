// satyr 21/07/2022 02:28
#include <spells.h>
inherit "/obj/conjunto_nuevo.c";

void setup()
{
    set_short("Atavío del Esgrimista");
    fijar_genero(1);
    add_alias("conjunto");

    fijar_conjunto_parcial();
    fijar_piezas(
        ({"/d/golthur/items/florete_maestro.c",
          "/baseobs/tesoros/armaduras/yelmo_mithmel_encantado.c",
          "/habilidades/objetos/recetas/objetos_creados/calzas_mithril.c"}));

    nuevo_beneficio_caracteristica(3, "des", 1);
}
