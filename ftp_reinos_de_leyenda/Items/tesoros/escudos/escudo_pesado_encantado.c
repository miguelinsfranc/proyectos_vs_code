// Dunkelheit 02-06-2011
inherit "/obj/multiescudo.c";

void setup()
{
    fijar_nombre_generico("Escudo Pesado Encantado");
    fijar_tipos_validos(({ "gran escudo", "escudo corporal" }));
    fijar_sufijo("encantad$a");
    fijar_sufijo_coloreado("Encantad$a");
    
    set_long("Anhelados por cualquier aventurero que se precie, los escudos encantados como $este $tipo "
    "han sido imbuídos mediante magia arcana con sortilegios que los hacen mucho más protectivos y resistentes "
    "en el campo de batalla. Su valor y utilidad son, por lo tanto, mucho mayores que el de un escudo común. "
    "¡No vayas de aventura sin ellas!\n");

    fijar_encantamiento(3);
}

