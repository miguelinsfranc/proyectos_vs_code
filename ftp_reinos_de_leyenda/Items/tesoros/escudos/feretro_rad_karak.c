// Satyr 11/05/2018 12:11
inherit "/obj/escudo.c";
#include <combate.h>
#include <habilidades_acciones.h>

#define BLOQUEO 4

void setup()
{
    string str;

    fijar_escudo_base("escudo corporal");

    set_name("feretro");
    set_short(
        "%^BOLD%^BLACK%^F%^RESET%^éretro "
        "%^BLACK%^BOLD%^M%^RESET%^ancillado de "
        "%^BLACK%^BOLD%^R%^RESET%^ad-%^BOLD%^BLACK%^K%^RESET%^arak");
    set_main_plural(
        "%^BOLD%^BLACK%^F%^RESET%^éretros %^BLACK%^BOLD%^M%^RESET%^ancillados "
        "de %^BLACK%^BOLD%^R%^RESET%^ad-%^BOLD%^BLACK%^K%^RESET%^arak");
    generar_alias_y_plurales();

    set_long(
        "Ante ti, la pesadísima tapa del ornamentado féretro de un antiguo "
        "Rey Enano, cuya identidad se ha perdido, así como el recuerdo "
        "de su imperio. Su tamaño es burdamente descomunal y, aún así, "
        "alguien lo ha desacrado, ¡y para usarlo como escudo, nada menos!\n\n"

        "  El enorme bloque de piedra agrietada tiene -o tenía- en su parte "
        "frontal un intrincado relieve del rey, mostrando su semblante pétreo "
        "con una expresión tan firme que el cincel no fue "
        "capaz de esculpirla. A sus lados, rúbricas de sus vasallos adornan "
        "la tapa, haciéndola aún mas magnífica si cabe.\n\n"

        "  Sin embargo, la belleza que tenía antes no tiene nada que ver "
        "con lo que es ahora, pues en su superficie se han clavado puntas de "
        "hierro oxidado de tres palmos de longitud y un peso que se acercaría "
        "al de un gnomo adolescente.\n\n"

        "  Además de eso, su superficie ha sido mancillada con el beso de la "
        "brocha, pues obscenidades escritas en lengua negra manchan la "
        "superficie, escritas con sangre reseca que seguramente perteneció "
        "a su pueblo.\n\n"

        "  Otrora este féretro albergó el cuerpo -sin duda exhumado- de un "
        "dios al que los suyos le obraban respeto. Sin embargo, tal fue su "
        "reputación que los enemigos no dudaron en aventurarse a la infra"
        "oscuridad, donde seguramente moraba, para quitarle en muerte el "
        "propio techo de su hogar.\n");

    set_read_mess(
        "Rad-Karak eztá tan muerto como zu padre. Zu tumba zaqueada zervirá "
        "para recordar a todos que BRUULG ez el máz fuerte. Zu pueblo "
        "muerto ateztiguará mi grandeza. Zu féretro adornará miz cenaz y "
        "me protegerá para ziempre, dezacrando azí el recuerdo de eze patán. "
        "¡Jódete, Rad-Karak!, ¡BRUULG y zuz aliadoz gelatinozoz han "
        "ganado!",
        "negra");

    fijar_material(6);
    fijar_peso(26000);

    fijar_encantamiento(20);
    add_static_property("ts-conjuro", 5);

    str = nuevo_efecto_basico("descarga", __ud(5, "%"));

    fijar_bloqueo_efecto(str, 3);
    fijar_params_efecto(str, "aplastante");
    fijar_triggers_efecto(str, ({_TRIGGER_PARA, _TRIGGER_PARA_C}));
    fijar_sms_efecto(
        str,
        (
            : ({sprintf(
                    "%s ¡Cuando bloqueas el ataque de %s te vales del gran "
                    "peso "
                    "de %s "
                    "para asestarle un clamoroso batacazo!",
                    "%^GREEN%^#%^RESET%^ ",
                    $1->query_cap_name(),
                    TU_S(TO)),
                sprintf(
                    "¡%s %s te asesta un clamoroso batacazo con %s "
                    "tras parar tu ataque!",
                    "%^RED%^*%^RESET%^",
                    $2->query_cap_name(),
                    SU_S(TO)),
                ""})
            :));

    add_static_property("tipo", ({"luchador", "caballero"}));
    add_static_property("no_raza", "Enano");
}
string descripcion_juzgar(object pj)
{
    return "Reduce el bloqueo de la habilidad 'golpear' en " +
           pretty_time(BLOQUEO) + ".\n";
}
void ejecutar_gancho_acciones(
    object habilidad, class ejecucion e, string gancho, mixed params...)
{
    if (!habilidad || habilidad->query_name() != "golpear") {
        return;
    }

    if (gancho != "post_inicializar_ejecucion") {
        return;
    }

    e->datos->bloqueo = e->datos->bloqueo - BLOQUEO;
}
