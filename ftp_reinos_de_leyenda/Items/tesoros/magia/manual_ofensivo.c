// Grail 27/01/14
// Satyr 10.06.2015
inherit "/obj/magia/grimorio";

	void setup()
	{
	set_holdable(1);
	set_name("tratado");
	set_short("Tratado Arcano para Aprendices de %^BOLD%^RED%^Magia Ofensiva%^RESET%^");
	set_main_plural("Tratados Arcanos para Aprendices de %^BOLD%^RED%^Magia Ofensiva%^RESET%^");
	add_alias(({"tratado","tomo","libro","aprendiz","aprendices","magia","ofensiva"}));
	add_plural(({"tratados","tomos","libros","aprendiz","aprendices","magias","ofensivas"}));
	set_long("Un pequeño tomo de tapas de cuero rojo, de páginas amarillentas y gastadas. Su "
	"superficie está recorrida por pequeñas runas negras, que brillan tenuemente. En él se "
	"recogen multitud de hechizos ofensivos básicos.\n");
	fijar_valor(3000);
	fijar_material(8);
	fijar_genero(1);
	inicializar_grimorio();
	fijar_peso(100);
    
    add_static_property("poder_magico", 15);
    
    add_static_property(
        "messon",
        "Ojeas rápidamente la obra mientras la empuñas, tratando de descubrir sus secretos.\n"
    );
    add_static_property(
        "messoff",
        "Cierras el tomo, sintiéndote así físicamente más cansado.\n"
    );
}