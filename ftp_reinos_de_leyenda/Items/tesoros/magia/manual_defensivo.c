// Grail 27/01/14
// Satyr 10.06.2015
inherit "/obj/magia/grimorio";

	void setup()
	{
	set_holdable(1);
	set_name("tratado");
	set_short("Tratado Arcano para Aprendices de %^BOLD%^Magia Defensiva%^RESET%^");
	set_main_plural("Tratados Arcanos para Aprendices de %^BOLD%^Magia Defensiva%^RESET%^");
	add_alias(({"tratado","tomo","libro","aprendiz","aprendices","magia","defensiva"}));
	add_plural(({"tratados","tomos","libros","aprendiz","aprendices","magias","defensivas"}));
	set_long("Se trata de un pequeño libro encuadernado en cuero blando, de forma que"
	"ofrece cierto margen de elasticidad y comodidad a la hora de empuñarlo. Recoge los "
	"hechizos básicos de defensa, ideal para los novatos y aprendices"
	" que estén debutando en ese campo.\n");
	fijar_valor(3000);
	fijar_material(8);
	fijar_genero(1);
	inicializar_grimorio();
	fijar_peso(100);
    
    add_static_property("con"       , 1);
    add_static_property("ts-aliento", 5);
    
    add_static_property(
        "messon",
        "Ojeas rápidamente la obra mientras la empuñas, tratando de descubrir sus secretos.\n"
    );
    add_static_property(
        "messoff",
        "Cierras el tomo, sintiéndote así físicamente más cansado.\n"
    );
}