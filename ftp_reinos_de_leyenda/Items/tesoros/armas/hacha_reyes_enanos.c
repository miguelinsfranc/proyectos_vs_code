// Satyr 28.07.2016
#include <unidades.h>
inherit "/obj/arma.c";
string colorear(string tx) {
    string *slices = explode(tx, " ");
    
    slices = map(
        slices, 
        (: "%^ORANGE%^" + $1[0..0] + "%^BOLD%^" + $1[1..] + "%^RESET%^" :)
    );
    
    return implode(slices, " ");
}
void setup()
{
    fijar_arma_base("hacha doble");
    set_name("hacha");
    
    set_short(colorear("Hacha de los Reyes Enanos"));
    set_main_plural(colorear("Hachas de los Reyes Enanos"));
    
    generar_alias_y_plurales();
    
    set_long(
        "Una bella hacha de doble filo que fue creada en los albores del mundo. "
            "Está hecha íntegramente de piedra, lo que no quita que sus filos estén "
            "letalmente afilados. La técnica usada para hacerla es algo que data de "
            "unos tiempos en los que el imperio enanil se extendía por debajo "
            "de todo el mundo. El arma está adornada con runas y figuras de enanos "
            "de perfil y el centro del mango -justo entre los dos filos- tiene un "
            "enorme topacio tan puro como ninguno que pueda encontrarse a día de hoy."
            "\n"
    );
    
    nuevo_ataque("tierra", "tierra", 3, 20);
    fijar_encantamiento(30);
    fijar_material(6);
    
    nuevo_efecto_basico("abundancia"   , __ud(20, "%"));
    nuevo_efecto_basico("opulencia"    , __ud(20, "%"));
    nuevo_efecto_basico("matamonstruos", __ud(22, "%"));
	add_static_property("nivel_minimo",29);
}
