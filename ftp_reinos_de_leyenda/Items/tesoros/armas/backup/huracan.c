inherit "/obj/proyectiles/arma_proyectiles.c";
void setup()
{
    fijar_arma_base("arco corto");

    set_name("huracan");
    set_short("%^BOLD%^CYAN%^H%^BLUE%^uracán%^RESET%^");
    set_main_plural("%^BOLD%^CYAN%^H%^BLUE%^uracánes%^RESET%^");
    add_alias(({"huracán", "huracan"}));
    add_plural("huracanes");
    set_long(
        " Un pequeño arco corto tallado en un material tan flexible como ligero y resistente: "
        "hueso de dragón. Su cuerda está hecha de tendones de estas mismas criaturas y está anclado "
        "al cuerpo del arma mediante sendos clavos hechos de mithril, único metal capaz de perforar "
        "limpiamente el tendón. El arco no ha sido tratado y tiene un color azulado en el hueso, "
        "color que delata la variedad de dragón al que pertenecía. Toda su superficie está repleta "
        "de filigranas arcanas y su escaso peso y la elasticidad de sus materiales permite disparar "
        "flechas mucho más rápido y a un alcance mucho mayor. El arco no posee ningún sortilegio, "
        "pero sus materiales ya poseen cualidades mágicas innatas.\n"
    );
    
    ajustar_BE(7);
    ajustar_BO(10);
    fijar_encantamiento(20);
    fijar_alcance(dame_alcance() + 1);
    fijar_vida_max(dame_vida_max() * 2);
    fijar_cadencia_fuego(dame_cadencia_fuego() - 1);

    add_static_property("electrico", 8);
    add_static_property("frio", 5);
    add_static_property("agua", 5);
} 