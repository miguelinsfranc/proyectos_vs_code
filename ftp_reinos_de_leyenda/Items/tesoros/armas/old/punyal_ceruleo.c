// Dunkelheit 02-06-2011
// Satyr 26.11.2015
inherit "/obj/arma.c";

void setup()
{
    fijar_arma_base("puñal");
    set_name("puñal");
    set_short("Puñal %^CYAN%^Cerúleo%^RESET%^");
    add_alias(({"punyal", "ceruleo"}));
    set_main_plural("Puñales %^CYAN%^Cerúleas%^RESET%^");    
    add_plural(({"punyales", "puñales", "ceruleas"}));

    set_long("Observas cómo este puñal ha sido engarzado con un lapislázuli y su diseño ha sido "
    "cuidado con esmero para facilitar su uso, lo que le otorga una mayor probabilidad de impacto en el "
    "combate. Su empuñadura metálica ha sido tintada con el mismo color celeste que la susodicha gema.\n");

    ajustar_BE(5);
    nuevo_efecto_basico("abundancia", __ud(20, "%"));
    fijar_vida(dame_vida_max() * 2);
}
