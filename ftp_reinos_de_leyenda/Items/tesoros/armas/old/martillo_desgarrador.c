#include <baseobs_path.h>
inherit BTESOROS_ARMAS + "arma_desgarradora.c";

void setup()
{
    preparar_arma_desgarradora("martillo de guerra");
    set_main_plural("Martillos de guerra %^RED%^Desgarradores%^RESET%^");
    set_plurals(({"martillos","martillos de guerra","desgarradores","martillos de guerra desgarradores"}));
}