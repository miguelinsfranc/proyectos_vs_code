// Satyr 30.11.2015
// Eckol 23.02.2016: Látigos.
// Satyr 18.03.2016 -- Hago las lfuns varargs para que puedan usarse desde otro item
#define TABLA "/table/bd_armas.c"
inherit "/obj/arma.c.c";

//HOTFIX para usar la base para látigos.
string str_sin_tildes(string tx){
    string stx; 

    if(!tx)
        return tx;

    stx = string_basico(tx);

    if(strsrch(tx,"ñ") != -1)
        stx = replace_string(stx,"ny","ñ");

    return stx;
}

varargs void preparar_nombres(string tx, object que)
{
    mixed plural,splural;
    string stx = str_sin_tildes(tx);
    plural = implode(map_array(explode(tx, " "), (: pluralize($1) :)), " ");
    splural = implode(map_array(explode(stx, " "), (: pluralize($1) :)), " ");

    que->set_name(stx);
    que->set_short(capitalize(tx) + " %^RED%^Desgarrador" + que->dame_a() + "%^RESET%^");
    que->set_main_plural(capitalize(plural) + " %^RED%^Desgarrador" + que->dame_ae() + "s%^RESET%^");
    que->generar_alias_y_plurales();
}
varargs void preparar_arma_desgarradora(string tx, object que)
{
    string stx = str_sin_tildes(tx);

    if ( ! que )
        que = TO;
    
    if ( ! stx || -1 == member_array(stx, TABLA->dame_armas()) )
        error("Arma '" + stx + "' inválida.");
    
    que->fijar_arma_base(stx);
    preparar_nombres(tx, que);
    
    que->fijar_encantamiento(10);
    que->nuevo_efecto_basico("carnicero", 50);
}
