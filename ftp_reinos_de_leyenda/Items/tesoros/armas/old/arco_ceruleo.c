// Satyr 11.11.2015
#include <unidades.h>
inherit "/obj/proyectiles/arma_proyectiles.c";
void setup()
{
    fijar_arma_base("arco largo");
    set_name("arco");
    set_short("Arco %^CYAN%^Cerúleo%^RESET%^");
    add_alias(({"arco", "ceruleo"}));
    set_main_plural("Arcos %^CYAN%^Cerúleos%^RESET%^");    
    add_plural(({"arcos", "ceruleos"}));

    set_long(
        "Un arco cuyo cuerpo ha sido pulido con esmero para facilitar su uso y que está "
            "engarzado con lapislázuli. Su cuerpo ha sido tintado con el mismo color "
            "celeste que la susodicha gema.\n");

    ajustar_BE(5);
    nuevo_efecto_basico("abundancia", __ud(20, "%"));
    fijar_vida_max(dame_vida_max()*2);
}
