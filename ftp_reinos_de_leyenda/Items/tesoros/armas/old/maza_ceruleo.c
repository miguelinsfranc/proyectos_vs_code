// Dunkelheit 02-06-2011
inherit "/obj/arma.c";

void setup()
{
    fijar_arma_base("maza");
    set_name("maza");
    set_short("Maza %^CYAN%^Cerúlea%^RESET%^");
    add_alias(({"maza", "cerulea"}));
    set_main_plural("Mazas %^CYAN%^Cerúleas%^RESET%^");    
    add_plural(({"mazas", "ceruleas"}));
    fijar_genero(2);

    set_long("Observas cómo esta maza ha sido engarzada con un lapislázuli y su diseño ha sido "
    "cuidado con esmero para facilitar su uso, lo que le otorga una mayor probabilidad de impacto en el "
    "combate. Su empuñadura metálica ha sido tintada con el mismo color celeste que la susodicha gema.\n");

	ajustar_BE(5);
    nuevo_efecto_basico("abundancia", __ud(20, "%"));
    fijar_vida(dame_vida_max() * 2);
}
