// Dunkelheit 02-06-2011
// Satyr 21.10.2015 -- efectos
#include <unidades.h>
inherit "/obj/arma.c";

void setup()
{
    fijar_arma_base("espada larga");
    set_name("espada");
    set_short("Espada Larga %^CYAN%^Celeste%^RESET%^");
    add_alias(({"larga", "celeste"}));
    set_main_plural("Espadas Largas %^CYAN%^Celestes%^RESET%^");    
    add_plural(({"espadas", "largas", "celestes"}));
    fijar_genero(2);

    set_long("Observas cómo esta espada larga ha sido engarzada con un lapislázuli y su diseño ha sido "
    "cuidado con esmero para facilitar su uso, lo que le otorga una mayor probabilidad de impacto en el "
    "combate. Su empuñadura metálica ha sido tintada con el mismo color celeste que la susodicha gema.\n");

	ajustar_BO(5);
    nuevo_efecto_basico("abundancia", __ud(20, "%"));
    fijar_vida_max(dame_vida_max()*2);
}
