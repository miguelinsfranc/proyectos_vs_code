// Dunkelheit 01-06-2011
// Eckol 14Sep15: Le pongo material (metal).

inherit "/obj/arma";

void setup()
{
    fijar_arma_base("puñal");
    set_name("filo");
    set_short("Filo %^RED%^BOLD%^Pagano%^RESET%^");
    add_alias("pagano");
    set_main_plural("Filos %^RED%^BOLD%^Paganos%^RESET%^");
    add_plural(({"filos", "paganos"}));
    
    set_long("Las tribus paganas, aunque ya extintas, gozaron de una gran presencia "
    "en los primeros días de la Era 3ª, justo después del Cataclismo que asoló Eirea. "
    "La sensación de desamparo que padecieron los supervivientes hizo que muchos "
    "grupos renegasen de toda deidad para dedicarse a adorar a entidades de lo más "
    "variopinto: árboles, montañas, el aire, los elementos y un largo etcétera de "
    "despropósitos. Los así llamados paganos protagonizaban rituales que en "
    "numerosas ocasiones terminaban con algún sacrificio \"voluntario\", y ahí es "
    "donde entraba en juego este ornamentado puñal.\n");
   
	fijar_material(2);
 
    fijar_encantamiento(5);
    add_static_property("divino", 2);
}
