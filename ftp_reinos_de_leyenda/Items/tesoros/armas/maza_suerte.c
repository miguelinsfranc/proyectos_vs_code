// Dunkelheit 18-06-2009
inherit "/obj/arma";

void setup()
{
    fijar_arma_base("maza");
    set_name("maza");
    set_short("%^GREEN%^BOLD%^Maza de la Suerte%^RESET%^");
    add_alias("suerte");
    set_main_plural("%^GREEN%^BOLD%^Mazas de la Suerte%^RESET%^");
    add_plural(({"mazas", "suertes"}));
    fijar_genero(2);
    
    set_long("¿Será una leyenda mancillada por la sucesión de canciones que ha protagonizado, o realmente "
    "esta hermosa maza aumenta la fortuna de quien la empuña? Tan sólo la experiencia podrá decírtelo. La "
    "historia dice que los golpes de suerte beneficiarán a quienes alimenten su metal con sangre y huesos "
    "partidos. La maza tiene un mango de madera de casi cuarenta centímetros, cuyo considerable grosor se "
    "soluciona con una empuñadura ergonómica en el tercio inferior. La cabeza, prísmica, tiene pintado un "
    "trébol de cuatro hojas, pero los años parecen haber han desgastado la tinta.\n");
    
    fijar_encantamiento(10);
    //nuevo_efecto_basico("critico"   , 100);
    nuevo_efecto_basico("critico"   , 50);
    nuevo_efecto_basico("salvajismo", 90);
    //add_static_property("ts-aliento", 8);
}
/*

string descripcion_encantamiento()
{
    return ::descripcion_encantamiento() + "La suerte de infligir golpes que no puedan esquivarse aumentará "
    "a la par que los impactos infligidos por el arma. No consigues averiguar qué otros golpes de suerte "
    "puede regalarte esta maza, pero haberlos... haylos.";
}

int rutina_ataque(object defensor, object atacante, int msjs, int bono, int no_esquivar, int flag)
{
    int impactos;
    
    // La probabilidad de infligir un ataque inesquivable dependerá de la cantidad de impactos, limitados a 300
    impactos = dame_impactos();
    if (impactos > 300) {
        impactos = 300;
    }
    
    return ::rutina_ataque(defensor, atacante, msjs, bono, impactos*100/300 > random(100), flag);
}

*/
