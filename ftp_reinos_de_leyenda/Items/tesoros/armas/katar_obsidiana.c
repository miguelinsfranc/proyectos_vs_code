// Satyr 14/05/2018 12:12
#include <unidades.h>
inherit "/obj/arma_de_mano.c";

void setup()
{
    set_name("katar");
    set_short(
        "%^BOLD%^BLACK%^Katar de "
        "%^NOBOLD%^GREEN%^O%^BOLD%^BLACK%^bsidiana%^RESET%^");
    set_main_plural(
        "%^BOLD%^BLACK%^Katares de "
        "%^NOBOLD%^GREEN%^O%^BOLD%^BLACK%^bsidiana%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Los asesinos drow de la escuela de lucha \"Vasaaj'Veldrin\" "
        "eran conocidos por su habilidad para luchar a oscuras. "
        "En la suboscuridad -donde todas las criaturas "
        "gozan de algún tipo de infravisión- estos guerreros "
        "usaban sus poderes místicos para privar de la capacidad "
        "sensorial a sus víctimas. Con sus enemigos a ciegas, "
        "desplegaban todo un arsenal de truculentas artimañas para "
        "sobrevivir a los alocados bandazos de sus enemigos al "
        "tiempo que se aseguraban de que los suyos impactasen con "
        "fria certeza.\n\n"

        "  Una de las artimañas más usadas eran los Katares de "
        "Obsidiana: armas de mano que ofrecían al asesino la "
        "capacidad de manipular sus trampas con las manos al "
        "tiempo que permitían lanzar ataques capaces de hacer graves "
        "heridas que podían atravesar barreras mágicas.\n\n"

        "  La pieza que ahora tienes ante ti es una de esas armas "
        "de mano, aunque el tiempo ha heho mella en ella. Está formada "
        "por una ya no tan afilada cuchilla de obsidiana que se sujeta al "
        "brazo de su portador mediante un agarre hecho de acero o "
        "adamantita. Varias barras horizontales este armazón permiten "
        "agarrarlo con fuerza tanto para lanzar estocadas "
        "como para rápidos cortes.\n\n"

        "  Es un arma difícil de usar, pero cuya efectividad se ha "
        "probado a lo largo de los siglos tras acallar la vida de "
        "incesantes criaturas, enemigos de los drow y hasta alguna "
        "que otra matrona o archimago.\n");

    fijar_genero(1);
    fijar_encantamiento(20);
    nuevo_efecto_basico("antimagia");
}
