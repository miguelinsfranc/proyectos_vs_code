// Dunkelheit 02-06-2011
inherit "/obj/arma.c";

void setup()
{
    fijar_arma_base("martillo a dos manos");
    set_name("martillo");
    set_short("Martillo a Dos Manos Encantado");
    add_alias(({"encantado", "dos manos"}));
    set_main_plural("Martillos a Dos Manos Encantados");
    add_plural(({"martillos", "encantados"}));

    set_long("Anheladas por cualquier aventurero que se precie, las armas encantadas como este martillo "
    "han sido imbuídas mediante magia arcana con sortilegios que las hacen más rápidas, resistentes "
    "y mortíferas en el campo de batalla. Su valor y utilidad son, por lo tanto, mucho mayores que el de "
    "un arma común. ¡No vayas de aventura sin ellas!\n");

    fijar_encantamiento(3);
}

