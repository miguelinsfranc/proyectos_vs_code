// Satyr 09/02/2022 01:15
/**
 * Ver:
 *
 * -
 * https://www.reinosdeleyenda.es/foro/ver-tema/concurso-comunidad-diseno-objetos-de-gloria-temporada-35/#post-341319
 * -
 * https://www.reinosdeleyenda.es/foro/ver-tema/balance-objetos-recompensa-y502/#post-344515
 *
 * Ver también la sombra del hechizo "espejismo"
 */
#include <baseobs_path.h>
inherit "/obj/magia/grimorio";

#define PORCENTAJE 15
void setup()
{
    set_name("grimorio");
    set_short("%^CYAN%^G%^BOLD%^rimorio%^RESET%^ de las "
              "%^CYAN%^Q%^BOLD%^uimeras%^RESET%^");
    set_main_plural("%^CYAN%^G%^BOLD%^rimorios%^RESET%^ de las "
                    "%^CYAN%^Q%^BOLD%^uimeras%^RESET%^");
    generar_alias_y_plurales();
    set_long("Este nuevo tratado de magia, extraído de la joven torre de "
             "ilusionistas existente en Urlom, alberga una gran cantidad "
             "páginas que contienen hechizos de alucinación, engaño, sueño, "
             "fantasía y confusión. Sus tapas de color cían, son casi tan "
             "finas como las páginas de su interior, pero son lo "
             "suficientemente duras para que sus cantos permanezcan intactos. "
             "De hecho, se encuentra en un estado tan perfecto, que parece que "
             "haya salido del taller hace solo unos minutos.\n");

    inicializar_grimorio();

    add_static_property("poder_magico", 30);
    add_static_property("int", 1);
    fijar_valor_venta(142200);
    add_static_property("nivel_minimo", 30);
}

string descripcion_juzgar(object b)
{
    return sprintf(
        "Las imágenes creadas por el hechizo 'espejismo' que pertenezcan al "
        "portador de este grimorio tendrán un %.2f de posibilidades de "
        "regenerarse una vez hayan sido rotas al recibir un impacto.",
        to_float(PORCENTAJE));
}
// Ver /hechizos/shadows/espejismo_sh.c
int dame_evasion_rotura_espejismo(object pj)
{
    return pj && random(100) <= (PORCENTAJE - 1);
}
