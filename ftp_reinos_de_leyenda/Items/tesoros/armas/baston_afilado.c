/*
    Kiratxia el 02-09-2010
    Bastón que decapita.
*/
// Satyr 13.014.2015 - Un bastón cortante. Un tesoro nivel 6 DPM. Le pongo enchant para justificarlo
inherit "/obj/arma";
void setup()
{
    fijar_arma_base("baston");
    set_name("baston");
    add_alias(({"baston","afilado"}));
    add_plural(({"bastones","afilados"}));
    set_short("Bastón Afilado");
    set_main_plural("Bastones Afilados");
    set_long(   "Se trata de un bastón de metro y medio de largo y varios centímetros de grosor. "
                "Está compuesto de dos piezas de madera unidas con un ingenioso mecanismo que permite, "
                "dando un par de giros, desplegarlo alargándolo 40 centímetros dejando a la vista "
                "una afilada hoja en la parte central. Estando cerrado, el bastón es muy robusto; por "
                "contra, abierto es bastante inestable por lo que podría romperse con facilidad si, "
                "desplegado, es usado como arma. El bastón al completo está tallado con intrincados "
                "tribales, incluso por la parte de las empuñaduras, pues tiene una en cada pieza. Uno "
                "de los extremos es bastante más ancho que el otro, lo que indica qué empuñadura hay "
                "que usar para sujetarlo correctamente.\n");
    fijar_material(1);
    fijar_genero(1);
    fijar_peso(5000);
    fijar_valor(250);
    fijar_vida_max(7500);
    fijar_vida(random(1000)+7000);
    //fijar_manos_necesarias(2);
    fijar_encantamiento(30);
}
status permitir_decapitar() { return 1; } 
status permitir_cortar_maleza() { return 1; }
