// Satyr 28.11.2016
// Sierephad Ene 2k20	--	Mejorando los alias y plurales

#include <unidades.h>
inherit "/obj/arma_de_mano.c";
void setup()
{
    string tx;

    set_name("arma");
    set_short("Armas de mano %^CYAN%^Celestes%^RESET%^");
    set_main_plural("pares de Armas de mano %^CYAN%^Celestes%^RESET%^");
    generar_alias_y_plurales();
    add_alias("celeste");
    add_plural("celestes");

    set_long("Dos poderosos talismanes de lapislázuli con forma de serpiente "
             "que se enroscan entre sí "
             "para dar lugar a un único objeto destinado a ser sostenido. La "
             "magia que encierra "
             "se transfiere a los golpes desarmados de su poseedor.\n");

    fijar_genero(-2);
    ajustar_BO(5);
    tx = nuevo_efecto_basico("abundancia", __ud(20, "%"));
    fijar_ataques_efecto(tx, "fisicos");

    fijar_valor(20 * 500);
}
