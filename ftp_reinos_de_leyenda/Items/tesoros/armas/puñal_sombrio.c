// Baelair 13/01/2014 -- Solo se puede usar 1
// Satyr   23.06.2015 -- le subo la vida a 4k

inherit "/obj/proyectiles/arrojadiza";
void setup()
{
    set_base_weapon("puñal");
    set_name("puñal sombrio");
    set_short("Puñal %^BOLD%^S%^BLACK%^ombrío%^RESET%^");
    set_main_plural("Puñales %^BOLD%^S%^BLACK%^ombríos%^RESET%^");
    set_long("Es un precioso puñal cuya empuñadura encaja perfectamente en tu "
             "mano. Su "
             "hoja esta perfectamente afilada y como puedes observar la punta "
             "de este es "
             "fina que seria capaz hasta de clavarse en cualquier roca como si "
             "fuese "
             "mantequilla. El negro puño del arma esta protegido con una "
             "cazoleta de finas "
             "hebras de acero tiznado de un color negro como la noche y la "
             "empuñadura "
             "cubierta con finas tiras de cuero para que no se resbale. Parece "
             "un arma "
             "realmente temible\n");

    add_alias(({"puñal", "sombrio"}));
    add_plural(({"puñales", "sombrios"}));

    set_enchant(1);

    add_static_property("esconderse", 25);
    add_static_property("sigilar", 25);
    add_static_property("ts-agilidad", 5);

    ajustar_danyo_minimo("tabla", 30);
    fijar_vida_max(4000);

    add_static_property("copias", 1);
}
