// Satyr 14/05/2018 11:56
inherit "/obj/arma.c";

void setup()
{
    fijar_arma_base("cadena");
    set_name("cadena");
    set_short("%^RED%^C%^CYAN%^adena de %^RED%^C%^CYAN%^ontención%^RESET%^");
    set_main_plural(
        "%^RED%^C%^CYAN%^adenas de %^RED%^C%^CYAN%^ontención%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "En ocasiones, la justicia se ve obligada a llevar la ley a "
        "hechiceros con las mangas tan cargadas de trucos que la cosa "
        "se vuelve un poco injusta.\n\n"

        "  Hartos de tener que ver a sus compañeros convertidos en ovejas "
        "-o reducidos a un pozo burbujeante de brea-, los cruzados de Anduar "
        "pronto inventaron un juego de cadenas mágico para lidiar con los "
        "magos (sí, cuando lo crearon eran conscientes de la ironía).\n\n"

        "  La cadena en sí esta fabricada de obsidiana y cada eslabón ha sido "
        "minuciosamente grabado con palabras de poder capaces de anular la "
        "magia que les rodea y de dar unos tremendos latigazos mentales a "
        "aquellos capaces de formular. Gracias a ellas, los hechiceros "
        "se vieron obligados a volver a respetar la ley del comercio, "
        "como el resto del mundo, lo que trajo de vuelta la normalidad "
        "capitalista que caracteriza a la cosmopolita Anduar.\n");

    fijar_encantamiento(20);
    nuevo_efecto_basico("migraña");
    nuevo_efecto_basico("sorbemagia", 5);
}
