// Dunkelheit 18-06-2009
inherit "/obj/arma";

void setup()
{
    fijar_arma_base("florete");
    set_name("florete");
    set_short("Florete de Elaine");
    add_alias("elaine");
    set_main_plural("Floretes de Elaine");
    add_plural("floretes");
    fijar_genero(1);
    
    set_long(
        "Se trata de un florete ligero y de diseño vanguardista, carente de los extravagantes excesos "
        "barrocos tan apreciados por la mayoría de los espadachines, y de los que la maestra de armas que "
        "concibió este florete -Elaine Marley, antigua gobernadora de la Isla de Bucanero- siempre intentó "
        "desmarcarse. Un sencillo guardamanos dorado cubre la empuñadura de cuero negro. La forma de la hoja, "
        "de corte ligeramente elíptico, es la única diferencia con la otra arma predilecta de Elaine: el "
        "estoque.\n"
    );
    
    fijar_encantamiento(20);
    ajustar_danyo_minimo("tabla", 20); 
}
