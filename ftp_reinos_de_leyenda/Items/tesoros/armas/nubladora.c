// Daga+1. Ishmar May'98
// Adaptando a la nueva era -- Dunkelheit 31-05-2011
#include <combate.h>
#include <unidades.h>
inherit "/obj/arma.c";

void setup()
{
    string st;
    
    fijar_arma_base("daga");
    set_name("nubladora");
    set_short("%^WHITE%^BOLD%^Nubladora%^RESET%^");
    set_main_plural("%^WHITE%^BOLD%^Nubladoras%^RESET%^");
    add_plural("nubladoras");
    set_long("Las leyendas de la Era 2ª hablan de esta lujosa y mortífera daga "
    "de los antiguos pantanos del delta de Zulk. Es una fina daga de acero, tan "
    "pulida que tu cara se refleja a perfección en su esbelta hoja. Tiene un "
    "mango dorado, presumiblemente de oro, con un zafiro engastado en el centro "
    "de su cruz.\n");
    fijar_genero(2);
    fijar_encantamiento(10);

    st = nuevo_efecto_basico("agotamiento", __ud(5, "%"));
    fijar_triggers_efecto(st, ({_TRIGGER_DANYA, _TRIGGER_APUNYALAR}));
    fijar_bloqueo_efecto(st , 60);
    fijar_duracion_efecto(st, 40);
    
    /*
        ~ Así era antaño ~
        
        set_base_weapon("dagger");
        set_name("nubladora");
        set_short(BOLD+"Nubladora"+RESET);
        set_main_plural(BOLD+"Nubladoras"+RESET);
        set_long("   Es una fina daga de acero, tan pulida que tu cara se refleja "
                "en su esbelta hoja. Tiene un mango dorado, presumiblemente de "
                "oro, con un zafiro encastado en el centro de la cruz que forma "
                "mango y hoja.\n");
        set_enchant(1);
    */
}
