// Grail 28/1/14
// Satyr 26.11.2015
inherit "/obj/arma";
void setup()
{
    fijar_arma_base("daga");
    set_name("daga arcana");
    add_alias(({"daga","arcana"}));
    add_plural(({"dagas","arcanas"}));

    set_short("%^BOLD%^Daga %^BLACK%^Arcana%^RESET%^");
    set_main_plural("%^BOLD%^Dagas %^BLACK%^Arcanas%^RESET%^");

    set_long(
        "Un empuñadura de daga grabada con símbolos arcanos y engarzada con un rubí "
            "que pulsa con energías mágicas. La hoja del arma no es física, es una "
            "proyección del poder mágico de su portador capaz de ignorar cualquier "
            "armadura o protección convencional.\n"
    );
    /*
    fijar_genero(2);
    fijar_vida_max(20000);
    fijar_encantamiento(10);
    add_static_property("fuego",5);

    fijar_valor(150);
    ajustar_danyo_minimo("tabla",30);
    fijar_material(5);
    fijar_vida(20000);
    ajustar_peso(2000);
    */
    fijar_encantamiento(20);
    ajustar_BO(25);
    ajustar_BE(7);

    quitar_ataque("tabla");
    nuevo_ataque("magico", "magico", 3, 30, 50);

    nuevo_efecto_basico("coeficiente", 0.6);
    nuevo_efecto_basico("canalizador", 30);
    quitar_efecto("Penetracion"); // es un arma magica, ya ignora armadura.
    
    add_static_property("poder_magico", 10);
    add_static_property("clase", ({"Hechicero", "Mago-ladrón"}));
}