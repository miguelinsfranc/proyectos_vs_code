// Dunkelheit 25-12-2007
// Tesoros genéricos - arma - nivel 3
// Encantamiento 1, +5 de BO, restricción de alineamiento
inherit "/obj/arma.c";

string dame_nombre_material() { return "Marfil blanco."; }

void setup()
{
    fijar_arma_base("horca");
    set_name("horca");
    set_short("Horca del %^WHITE%^BOLD%^Buen Pastor%^RESET%^");
    add_alias(({"pastor", "buen pastor"}));
    set_main_plural("Horcas del %^WHITE%^BOLD%^Buen Pastor%^RESET%^");
    add_plural("horcas");
    fijar_genero(2);
    
    set_long("La Horca del Buen Pastor es el arma de los más humildes -pero reverenciados- luchadores del "
    "bien. Se trata de una sencilla horca de marfil con dos afiladas cabezas y un mango riveteado con "
    "numerosos grabados, que imitan los nudos de la madera. Entre tanta decoración barroca, encuentras dos "
    "zonas diseñadas para blandir el arma, cuya forma es semejante a la de las yemas de los dedos. El blanco "
    "impoluto de su marfil es una clara indicación de la bondad residente en este arma, y el hecho de que se "
    "trate de una horca no es sino un guiño a los más humiles ciudadanos de los reinos del bien.\n");
    
    fijar_alineamiento(-1000);
    
    fijar_BO(dame_BO() + 5);
    fijar_encantamiento(1);
    
    fijar_valor(25*500);
}
int set_in_use(int i)
{
    int p = ::set_in_use(i);
    object oveja;

    if (i && p) {

        if (query_timed_property("agotado")) {
            tell_object(environment(), 
                "Tu " + query_short() + " está agotada y no parece que ninguna oveja acuda a ti.\n"
            );
        }
        else if (!oveja = clone_object("/baseobs/monstruos/animales/oveja.c")) {
            tell_object(environment(), "Un error impide que acudan ovejas a ti.\n");
        }
        else if (oveja->move(load_object("/std/room.c"))) {
            tell_object(environment(), "Tu oveja no puede venir a tu sala.\n");
            destruct(oveja);
        }
        else if (!oveja->move_player("horca", environment(environment()), ({
            "$N se va a algun lado.",
            "$N llega aquí dando saltos y balando alegremente."
            }))
        ) {
            tell_object(environment(), "Tu " + oveja->query_short() + " no puede llegar a tu sala.\n");
            destruct(oveja);
        }
        else {
            oveja->move(environment(environment()));
            tell_object(environment(),
                "Inexplicablemente, una oveja cariñosa aparece en la sala y se frota contra ti "
                "cuando empuñas " + query_short() + ".\n"
            );

            environment()->nuevo_seguidor(oveja);
            add_property("oveja", oveja);

            if (!environment()->dame_protector()) {
                environment()->fijar_protector(oveja);  
                tell_object(environment(), oveja->query_short() + " bala amorosamente y te protege.\n");
            }
        }
    }
    else if (!i && !p) {
        oveja = query_property("oveja");

        if (oveja && environment(oveja)) {
            tell_room(environment(oveja), oveja->query_short() + " desaparece y se va.\n");
            destruct(oveja);
        }

        add_timed_property("agotado", 1, 600);
        remove_property("oveja");
    }

    return p;
}