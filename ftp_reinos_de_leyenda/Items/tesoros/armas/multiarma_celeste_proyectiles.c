// Satyr 21.06.2016
inherit "/obj/multiobjetos/multiarma_proyectiles.c";
void setup()
{     
    fijar_genero(1);
    fijar_titulos("%^CYAN%^Celeste%^RESET%^", "%^CYAN%^Celestes%^RESET%^");

    set_short("Multiarma de proyectiles %^CYAN%^Celeste%^RESET%^");
    set_main_plural("Multiarmas de proyectiles %^CYAN%^Celestes%^RESET%^");
    
    fijar_long_generico("Un orbe marfileño engarzado con lapislázuli que puede ser "
        "transmutado en todo tipo de armas.\n"
    );
}
void setup_especifico() 
{
	ajustar_BO(5);
    fijar_vida_max(dame_vida_max()*2);
    nuevo_efecto_basico("abundancia", __ud(20, "%"));
}
void poner_descripcion_especifica_multiobjeto() {
    set_long(
        capitalize(dame_numeral()) + " " + dame_arma_base() + " engarzad"
            + dame_vocal() + " con lapislázuli y que ha sido cuidad" + dame_vocal() + " con "
            "esmero para facilitar su uso. Su empuñadura ha sido tintado con el mismo color "
            "celeste de la gema que " + dame_lo() + " adorna.\n"
    );
}