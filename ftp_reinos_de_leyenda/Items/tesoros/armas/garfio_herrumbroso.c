// Satyr 09/05/2018 12:42
#include <unidades.h>
inherit "/obj/arma_de_mano.c";

void setup()
{
    string tx;

    set_name("garfio");
    set_short("%^WHITE%^BOLD%^G%^RESET%^a%^BOLD%^r%^RESET%^f%^BOLD%^i%^RESET%^"
              "RESET%^o %^BOLD%^BLACK%^Herrumbroso%^RESET%^");
    set_main_plural("%^WHITE%^BOLD%^G%^RESET%^a%^BOLD%^r%^RESET%^f%^BOLD%^i%^"
                    "RESET%^RESET%^os %^BOLD%^BLACK%^Herrumbrosos%^RESET%^");
    fijar_genero(1);
    generar_alias_y_plurales();

    set_long(
        "El garfio desusado de algún pirata que pasó a mejor vida en el mar, "
        "entendiendo como \"mejor vida\" una centuria de esclavitud bajo "
        "la maldición de Astaroth a lomos de su caravela consumida.\n\n"

        "  Su uña curvada, de un acero que en tiempos seguro fue magnífico, "
        "está ahora repleta de óxido y restos de percebes, si bien su núcleo "
        "escondido aún preserva la gloria y resistencia de antaño gracias a "
        "la infinidad de maldiciones y conjuros que se habrán contagiado "
        "a la pieza tras la no-vida de su dueño.\n\n"

        "  Un mango de metal recubierto de madera deshecha permite agarrarlo "
        "-aún cuando no se disponga de de la comunidad de una extremidad "
        "cercenada- permitiendo así llevar a cabo un estilo de lucha poco "
        "ortodoxo, pero capaz de colarse por las rendijas de la armadura "
        "más ornamentada para llegar a hincar el diente a su jugoso "
        "interior.\n");

    fijar_encantamiento(30);
    tx = nuevo_efecto_basico("machacaarmaduras", 10);
    fijar_bloqueo_efecto(tx, 50);
    nuevo_efecto_basico("matamonstruos", __ud(25, "%%"));

    add_static_property("no_clase", ({"druida"}));
}
