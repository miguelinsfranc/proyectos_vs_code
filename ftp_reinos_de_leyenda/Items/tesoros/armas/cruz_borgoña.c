// Dunkelheit 18-06-2009
// Astel 21.11.15 . Opulencia
#include <unidades.h>
inherit "/obj/arma";

string dame_nombre_material() { return "Acero bendito por los extintos Clérigos de Lummen."; } 

void setup()
{
    fijar_arma_base("espada a dos manos");
    set_name("espadon");
    set_short("Espadón de la %^WHITE%^BOLD%^Cruz %^RED%^de %^WHITE%^Borgoña%^RESET%^");
    add_alias(({"espadón", "borgonya", "borgoña", "cruz", "borgogna"}));
    set_main_plural("Espadón de la %^WHITE%^BOLD%^Cruz %^RED%^de %^WHITE%^Borgoña%^RESET%^");
    add_plural(({"espadones", "cruces", "borgonyas", "borgoñas", "borgognas"}));
    fijar_genero(2);
    
    set_long("Este épico espadón es objeto de codicia no sólo por parte de los más opulentos guerreros, sino "
    "también por una suerte de coleccionistas, historiadores y demás fetichistas que relacionan esta maravilla "
    "de la forja con las épocas más lóbregas de la historia de Takome, cuando el fanatismo de la iglesia de "
    "Lummen se cobró cientos de vidas de ciudadanos acusados, sin prueba alguna en la mayoría de los casos, "
    "de practicar magia negra. La remesa de Cruces de Borgoña, como se bautizó a estos espadones, fue destinada "
    "a las manos ensangrentadas de los verdugos que hacían rodar las cabezas de los infieles en la Plaza de "
    "Ejecuciones de la capital del bien. La hoja del espadón alcanza el metro y treinta centímetros de longitud, "
    "siendo uno de los más grandes que se hayan forjado jamás; por ello, su empuñadura en cruz hace honor al "
    "nombre del espadón, pues la cruz de borgoña, un aspa heráldica de color rojo con los tallos sesgados, se "
    "extiende por toda ella.\n");
    
    add_static_property("messon", "El vello de tus brazos se eriza al pensar en las cientos de cabezas rebanadas por este espadón en los tiempos de Lummen.\n");
    
    add_static_property("con", 1);
    fijar_encantamiento(40);
    ajustar_peso(1000);
    
    nuevo_efecto_basico("opulencia",__ud(30,"%"));
    
	add_static_property("nivel_minimo",20);
}
