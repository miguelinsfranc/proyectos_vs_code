// Satyr 15/05/201
inherit "/obj/magia/reliquia";

void setup()
{
    set_name("reliquia");
    set_short("Reliquia Divina de la %^RED%^P%^ORANGE%^ersistencia%^RESET%^");
    set_main_plural(
        "Reliquias Divinas de la %^RED%^P%^ORANGE%^ersistencia%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Una reliquia sagrada representada por un cubo de piedra cuyas caras "
        "han sido talladas con un número variado de runas. En una hay una "
        "sola marca y, tras revisarla con cuidado, puedes ver seis marcas "
        "en la cara opuesta, dejando las otras caras para el resto de "
        "números\n\n"

        "  El saber de qué es lo que simboliza se ha "
        "perdido con las centurias, pero otrora se asociaba con el poder de un "
        "dios ya extinto que ahora es anhelado por los ya presentes, y por "
        "ende, por sus clérigos.\n");

    inicializar_reliquia();

    add_static_property("con", 2);
    add_static_property("sab", -1);
    fijar_valor(100000);
    fijar_peso(6000);
    fijar_material(6);
}
