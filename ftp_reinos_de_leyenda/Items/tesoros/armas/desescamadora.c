// Dunkelheit 02-06-2011
/*
    Zoilder 01/11/2011:
        - Añado cocodrilo a las razas que recibirán daño extra tanto de modo
   normal como al apuñalar. Zoilder 05/11/2011:
        - Añado dragón (para los machos que se atrevan a atacarle) a las razas
   que recibirán daño extra tanto de modo normal como al apuñalar.
        - Le pongo que el daño al apuñalar sea de tipo spell_damage, que es el
   mismo tipo de daño que se usa en la rutina_ataque de las armas, para evitar
   casos donde a kills no se les pueda dar el golpe magico, pero al apuñalar sí
   (evitando esta incongruencia)
*/
// Satyr 21.10.2015
// Eckol Ago18: Limitación por raza en property
inherit "/obj/arma.c";
string descripcion_encantamiento()
{
    return "El arma está imbuída por un poderoso encantamiento que le permite "
           "infligir daño extra de "
           "tipo \"frío\" a criaturas escamosas como hombres-lagarto y "
           "sajuaguines.";
}

void setup()
{
    fijar_arma_base("espada corta");
    set_name("desescamadora");
    set_short("%^BOLD%^BLUE%^De%^GREEN%^se%^BLUE%^sc%^GREEN%^am%^BLUE%^ad%^"
              "GREEN%^or%^BLUE%^a%^RESET%^");
    set_long("Es el arma más temida por los hombres-lagarto, los sajuaguines y "
             "cualquier otra raza "
             "de índole escamoso. Esta espada corta de hoja dentada está "
             "imbuída con un poderoso encantamiento "
             "que la hace desarrollar púas de hielo cuando entra en contacto "
             "con piel escamosa, añadiendo un "
             "nuevo y doloroso componente a cualquier ataque que se lance con "
             "ella, mayor si se tiene en cuenta "
             "la debilidad racial que padecen dichas criaturas al frío.\n");
    set_main_plural("%^BOLD%^BLUE%^De%^GREEN%^se%^BLUE%^sc%^GREEN%^am%^BLUE%^"
                    "ad%^GREEN%^or%^BLUE%^as%^RESET%^");
    add_plural("desescamadoras");
    fijar_genero(2);

    fijar_encantamiento(20);
    nuevo_efecto_basico("matalagartos", 40);
    nuevo_efecto_basico("descarga", 175, 0, 0, 0, 0, 0, 0, "frio");

    add_static_property(
        "no_raza",
        ({"Hombre-lagarto",
          "Draxyar",
          "Sajuaguín",
          "Tritón",
          "Sirénido",
          "Cocodrilo",
          "Dragón"}));
}
/*
int rutina_ataque(object defensor, object atacante, int msjs, int bono, int
no_esquivar, int flag)
{
    int resultado;

    if (flag == 0 && regexp(defensor->dame_raza(),
"(hombre-lagarto|sajuaguin|triton|sirenido|cocodrilo|dragon)")) {
        add_attack("frio", "frio", 3, 60, 0, 0, 1, 1);
        resultado = ::rutina_ataque(defensor, atacante, msjs, bono +
atacante->dame_carac("des"), no_esquivar, flag); remove_attack("frio"); } else {
        resultado = ::rutina_ataque(defensor, atacante, msjs, bono, no_esquivar,
flag);
    }

    return resultado;
}
*/
int set_in_use(int i)
{
    if (i) {
        foreach (object arma in environment()->query_weapons_wielded()) {
            if (base_name(arma) == base_name()) {
                tell_object(
                    environment(),
                    "Eres incapaz de empuñar dos armas tan poderosas a la "
                    "vez.\n");
                return 0;
            }
        }
        /*
        if (regexp(environment()->dame_raza(),
        "(hombre-lagarto|sajuaguin|triton|sirenido|cocodrilo|dragon)")) {
            tell_object(environment(), "Un escalofrío sacude tus garras al
        intentar empuñar este arma maldita.\n"); return 0;
        }*/
    }

    return ::set_in_use(i);
}
/*
void query_message(int pupa,string tipo,string localizacion,object
atacante,object defensor)
{
    string msj_at = defensor->query_short(), msj_def = atacante->query_short(),
msj_room;

    if (tipo != "frio") {
        return;
    }

    msj_room = msj_def+" brota púas de hielo con su arma que atraviesan a
"+msj_at; msj_at = "%^GREEN%^#%^RESET%^ Tu desescamadora brota púas de hielo de
atraviesan a "+msj_at; msj_def = "%^RED%^*%^RESET%^ La desescamadora de
"+msj_def+" brota púas de hielo que te atraviesan"; if (localizacion != "") {
        msj_room += " en " + localizacion;
        msj_at += " en " + localizacion;
        msj_def += " en " + localizacion;
    }
    switch(pupa) {
        case 1..120:
            msj_room += ", haciendo que sus escamas caigan al suelo como
cristales rotos.\n"; msj_at += ", haciendo que sus escamas caigan al suelo como
cristales rotos.\n"; msj_def += ", haciendo que tus escamas caigan al suelo como
cristales rotos.\n"; break; case 151..300: msj_room += " y estalactitas de
sangre helada caen al suelo y estallan en mil pedazos.\n"; msj_at += " y
estalactitas de sangre helada caen al suelo y estallan en mil pedazos.\n";
            msj_def += " y estalactitas de sangre helada caen al suelo y
estallan en mil pedazos.\n"; break; default: msj_room += " y se extienden por
sus venas hasta hundirse en su mismísimo corazón.\n"; msj_at += " y se extienden
por sus venas hasta hundirse en su mismísimo corazón.\n"; msj_def += " y se
extienden por tus venas hasta hundirse en tu corazón.\n";
    }

    tell_object(atacante, msj_at);
    tell_object(defensor, msj_def);
    tell_accion(environment(defensor), msj_room, "", ({defensor, atacante}),
atacante);
}

void event_apunyalar(object habilidad, object bribon, object objetivo, int
danyo)
{
    if (danyo != -300 && regexp(objetivo->dame_raza(),
"(hombre-lagarto|sajuaguin|cocodrilo|dragon)")) { tell_object(bribon,
"%^GREEN%^#%^RESET%^ ¡Tu desescamadora brota púas de hielo que serpentean "
        "dolorosamente dentro de la herida infligida a
"+objetivo->query_short()+"!\n"); tell_object(objetivo, "%^RED%^*%^RESET%^ ¡La
desescamadora de "+bribon->query_short()+" brota " "púas de hielo que serpentean
dolorosamente dentro de la herida infligida a "+ objetivo->query_short()+"!\n");
        tell_accion(environment(objetivo), "%^GREEN%^#%^RESET%^ ¡La
desescamadora de "+ bribon->query_short()+" brota púas de hielo que serpentean
dolorosamente dentro de la herida " "infligida a
"+objetivo->query_short()+"!\n", "Escuchas un grito de dolor.", ({ bribon,
objetivo }), bribon); objetivo->spell_damage(-roll(6, 80),"frio",bribon,0);
        //objetivo->danyo_especial(-roll(6, 80), "frio", bribon);
    }
}

*/
