// Dunkelheit 02-06-2011
inherit "/obj/arma.c";

void setup()
{
    fijar_arma_base("tridente");
    set_name("tridente");
    set_short("Tridente Encantado");
    add_alias("encantado");
    set_main_plural("Tridentes Encantados");
    add_plural(({"tridentes", "encantados"}));

    set_long("Anheladas por cualquier aventurero que se precie, las armas encantadas como este tridente "
    "han sido imbuídas mediante magia arcana con sortilegios que las hacen más rápidas, resistentes "
    "y mortíferas en el campo de batalla. Su valor y utilidad son, por lo tanto, mucho mayores que el de "
    "un arma común. ¡No vayas de aventura sin ellas!\n");

    fijar_encantamiento(3);
}

