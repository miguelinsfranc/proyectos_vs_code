// Dunkelheit 01-06-2011
inherit "/obj/arma.c";

string dame_nombre_material()
{
    return "Madera, cuarzo y lapislázuli";
}

void setup()
{
    fijar_arma_base("vara");
    set_name("vara");
    set_short("Vara de Mordekainen");
    add_alias("mordekainen");
    set_main_plural("Varas de Mordekainen");
    add_plural(({"varas", "mordekainens"}));
    set_long("Una larga vara de madera de aproximadamente tres pies de longitud, que termina en "
    "una cabeza con forma de prisma pentagonal hecha de cuarzo, en cuya cumbre reina engastado "
    "un lapislázuli esférico. No es menester realizar una gran maniobra para infligir un buen "
    "golpe con esta vara; apenas un ligero movimiento de muñeca ya la hará silbar en el aire y "
    "hundir su cabeza cristalina con inusitado atino. Y a eso hay que añadir el encantamiento "
    "en el que las varas de los magos suelen estar imbuídas. ¡Una combinación letal!\n");
    fijar_genero(2);
    
    fijar_encantamiento(12);
    ajustar_BO(10);
    ajustar_BE(5);
    
    add_static_property("poder_magico", 15);
    nuevo_efecto_basico("sanguijuela", 3);
    
    add_static_property("clase", ({"Hechicero"}));
}
