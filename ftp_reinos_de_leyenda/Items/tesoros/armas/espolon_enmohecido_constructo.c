// Satyr 09/05/2018 12:42
#include <unidades.h>
#include <combate.h>
inherit "/obj/arma_de_mano.c";

void setup()
{
    string tx;

    set_name("espolon");
    set_short("Espolón %^GREEN%^enmohecido%^RESET%^ de Constructo");
    set_main_plural("Espolones %^GREEN%^enmohecidos%^RESET%^ de Constructo");
    fijar_genero(1);
    generar_alias_y_plurales();

    set_long(
        "Los constructos son artefactos mecánicos que gozan de consciencia "
        "propia -o no, pero debatirlo requeriría amplios conocimientos "
        "de filosofía y de ciencia del conocimiento- y cuyo origen "
        "\"NoEstaDelTodoClaroYNoSePorQueNosMiraisANosotros\".\n\n"

        "  Discusiones filosóficas y políticas apartes, estas criaturas "
        "se caracterizan por usar extremidades metálicas para cavar, construir,"
        " desplazar grandes pesos o triturar a aquellos orgánicos que "
        "consideran una amenaza para sus directivas principales.\n\n"

        "  El espolón que ahora tienes ante tí sin duda pertenecía a uno "
        "de estos artefactos: pues parte de los tendones de cobre que "
        "lo unían al ¿antebrazo? de su portador todavía se derraman desde uno "
        "de sus extremos.\n\n"

        "  La pieza en sí es una púa serrada muy fácil de agarrar. "
        "Su superficie, afilada y cruel es capaz de perforar "
        "cualquier material. Por desgracia, su resistencia a la corrosión "
        "no parece ser muy buena, puesto que el tiempo ha hecho estragos "
        "en su superficie, oxidándola y dejando que una colonia de moho "
        "supurante la domine y agriete desde dentro.\n\n"

        "  Para la mayoría de efectos es un apéndice inservible, pero alguien "
        "capaz de moverlo como un arma de mano podría desgarrar como lo hacía "
        "su portador original, así como utilizar el aciago pus del moho en su "
        "beneficio.\n");

    fijar_encantamiento(20);
    nuevo_ataque("veneno", "veneno", 2, 30, 10);
    tx = nuevo_efecto_basico("sangrado");
    // Esto es importante para que el "sangrado" no se asocie al ataque "veneno"
    // Al dejarlo a 0, las armas de mano lo asociarán al ataque principal del
    // estilo de combate.
    fijar_ataques_efecto(tx, 0);
    fijar_dato_efecto(tx, _EF_TRIGGER, ({_TRIGGER_IMPACTO_C}));
    add_static_property("acido", -5);
}
