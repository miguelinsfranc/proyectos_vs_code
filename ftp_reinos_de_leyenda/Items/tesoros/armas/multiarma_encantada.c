// Satyr 21.06.2016
// Eckol Ago19: Fijar precio venta, para controlar el valor que era alto por enchant..
inherit "/obj/multiobjetos/multiarma.c";
void setup()
{     
    fijar_genero(1);
    fijar_titulos("Encantada", "Encantadas");

    set_short("Multiarma Encantada");
    set_main_plural("Multiarmas Encantadas");
    fijar_long_generico(
        "Un orbe metálico del que irradia una tenue magia. Puede ser transmutado en "
            "cualquier tipo de arma.\n"
    );
    
    fijar_encantamiento(10);
}

void setup_especifico(){
    fijar_valor_venta(dame_valor_real()*3);
}

void poner_descripcion_especifica_multiobjeto() {
    set_long(
        capitalize(dame_numeral()) + " " + dame_arma_base() + " "
            + dame_texto_forjado() + " con magia y perfecta para iniciar tus "
            "aventuras en Reinos de Leyenda.\n"
    );
}

void poner_short_especifico_multiobjeto() {
    set_short(
        capitalize(dame_multiobjeto_base()) +  " Encantad" + dame_vocal()
    );
    set_main_plural(
        capitalize(pluralize(dame_multiobjeto_base())) + " " + 
            "Encantad" + dame_ao() + "s"
    );
}
