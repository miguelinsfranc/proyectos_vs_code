// Dunkelheit 18-06-2009
inherit "/obj/arma";

void setup()
{
	fijar_arma_base("mayal");
	set_name("mayal");
	set_short("Mayal de Troll");
	add_alias("troll");
	set_main_plural("Mayales de Troll");
	add_plural("mayales");
	fijar_genero(1);
	
	set_long("En la Era 2ª los mayales gozaban de gran aceptación entre la población de trolls de "
	"Golthur Orod, hasta el punto de que estas torpes criaturas se lanzaron a la aventura de construir "
	"su propia versión de un mayal. Lo que ven tus ojos es uno de esos productos. Además de su tamaño, "
	"que es considerablemente superior, la versión troll del mayal prescinde los pinchos en las tres "
	"enormes bolas macizas de hierro que cuelgan en sus respectivas cadenas; la razón más factible "
	"es la falta de pericia de sus fabricantes y el obvio poder de destrucción de este arma en manos "
	"de criaturas de gran tamaño, lo cual hace innecesario perder el tiempo clavando punzones.\n");
	
	fijar_manos_necesarias(2);
	
	fijar_material(2); // Metal, las cabezas son de hierro
	
	ajustar_BO(10); // En total 15, que sería 5 más que una maza a dos manos
	fijar_peso(7500); // Un kilo y pico más que una maza a dos manos
	
	// El mayal tiene tres bolones así que es más fácil que te dé alguno, en realidad esta lógica
	// sólo me interesa para darle una media de daños más regular que una maza a dos manos.
	// 5d33 tiene un máximo equivalente a una maza a dos manos (3d55)
	remove_attack("tabla");
	add_attack("tabla", "aplastante", 5, 33, 6, 0, 0, 0, 0);
}
