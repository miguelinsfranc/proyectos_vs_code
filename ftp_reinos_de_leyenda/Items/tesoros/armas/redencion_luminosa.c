// Satyr 14/05/2018
inherit "/obj/arma.c";
#include <combate.h>
void setup()
{
    fijar_arma_base("latigo");
    set_name("redencion");
    set_short(
        "%^GREEN%^BOLD%^R%^WHITE%^BOLD%^edención "
        "%^GREEN%^L%^WHITE%^uminosa%^RESET%^");
    set_main_plural(
        "%^GREEN%^BOLD%^R%^WHITE%^BOLD%^edenciones "
        "%^GREEN%^L%^WHITE%^uminosas%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "La exhumación, o saqueo de tumbas, no es algo muy bien visto por "
        "la sociedad religiosa en su conjunto, ¡pero ojo!, ¡que esto solo "
        "es así cuando el saqueador es un don nadie!\n\n"

        "  ¿Qué pasa cuando es a la iglesia a quien le da por saquear tumbas "
        "de clérigos, "
        "obispos y otros santos? Pues que extraen al cadáver putrefacto "
        "que otrora servía a su mismo dios y extraen, con precisión quirúrgica,"
        " los huesos, ojos y órganos putrefactos de su interior con el fin "
        "de guardarlas en museos o crear armas sagradas.\n\n"

        "  ¿Suena macabro?, pues es la verdad, ya que el látigo que ahora "
        "sujetas ha sido fabricado única y exclusivamente con huesos de santo,"
        " pulidos y separados en pequeñas piezas que ahora presentan una "
        "oquedad en el medio por la cual serpentea un núcleo de cuero.\n\n"

        "  No obstante, esta práctica tan hipócrita de la exhumación sagrada "
        "se justifica cuando sientes el poder de la pieza. Los huesos "
        "santificados brillan con poder benigno y los grabados que hay "
        "en cada uno de ellos se iluminan al ser tocados por el poder "
        "redentor de la fe. Cada restallar del látigo trae consigo "
        "un relámpago de luz que ciega a los presentes y trae consigo "
        "el fevor de su antiguo... ¿propietario?. No, propietario no. "
        "Quizás será mejor decir, de la persona a la que antiguamente "
        "pertenecían los huesos que forman este arma.\n");

    fijar_encantamiento(30);
    nuevo_ataque("bien", "bien", 2, 20);

    nuevo_efecto_basico("iluminacion", 50);
    nuevo_efecto_basico("redencion", __ud(60, "%%"));
}
