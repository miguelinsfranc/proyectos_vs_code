// Satyr 09.11.2015
inherit "/obj/proyectiles/arma_proyectiles.c";
void setup()
{
    fijar_arma_base("ballesta");

    set_name("empaladora");
    set_short(
        "%^RED%^BOLD%^E%^RESET%^RED%^mpaladora de %^RESET%^BOLD%^RED%^T%^RESET%^RED%^ilva%^RESET%^"
    );
    set_main_plural(
        "%^RED%^BOLD%^E%^RESET%^RED%^mpaladoras de %^RESET%^BOLD%^RED%^T%^RESET%^RED%^ilva%^RESET%^"
    );
    add_alias(({"empaladora", "tilva", "empaladora de tilva"}));
    add_plural(({"empaladoras", "tilva", "empaladoras de tilva"}));
    set_long(
        "Esta pesada y amenazante ballesta fue construída en Tilva para "
            "abastecer al creciente mercado de asesinos de la ciudad. "
            "Su diseño original nació a manos de un mago que quería "
            "lucrarse gracias a un arma capaz de convertir a cualquier "
            "don nadie en un asesino de reyes.\n\n"
            
        " El hechicero consiguió crear el artefacto que se imaginaba, fundando "
            "una empresa que se especializaba en los asesinatos "
            "selectivos e importantes a un precio muy económico. "
            "Su negocio era muy lucrativo y terminó un día que sus "
            "competidores, irónicamente, decidiéron contratar sus servicios "
            "y el mago "
            "murió asesinado por sus propios empleados... una larga "
            "historia.\n\n"
        
        "  La ballesta en sí tiene una cruz de madera maciza encantada "
            "capaz de disparar proyectiles con una fuerza tremenda. "
            "Tanto dispara la ballesta que es capaz "
            "de atravesar a sus víctimas, causando graves y escalofriantes "
            "heridas en el proceso.\n\n"
            
        "  Este tipo de armas, de las cuales existen ya muy pocas, fue tan "
            "conocida en Tilva que dió lugar a la creación de un "
            "montón de expresiones poco imaginativas, como "
            "\"levantar una empalizada\", \"hacer unas brochetas\" o "
            "\"colgar unos cuadros\". Por si no estaba claro, todas hacen "
            "referencia al asesinato. "
            "Al asesinato mediante empalación por ballesta, para ser más exactos.\n"
    );

    fijar_encantamiento(30);
    nuevo_efecto_basico("barrido", 3);
    nuevo_efecto_basico("sangrado");
    fijar_peso(dame_peso()*3);
	add_static_property("nivel_minimo",29);
} 
