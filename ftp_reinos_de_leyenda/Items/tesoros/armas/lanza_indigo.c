// Dunkelheit 25-12-2007    
// Tesoros genéricos - arma - nivel 4    
// +3 de encantamiento, resistente, ataque elemental 1d15+8 (+6 BO) en el agua
// Satyr 2014

#include <baseobs_path.h>
inherit BARMAS + "lanza.c";

void setup()
{
    fijar_arma_base("lanza");
    set_name("lanza");
    set_short("Lanza %^CYAN%^BOLD%^Índ%^RESET%^CYAN%^igo%^RESET%^");
    add_alias("indigo");
    set_main_plural("Lanzas %^CYAN%^BOLD%^Índ%^RESET%^CYAN%^igo%^RESET%^");
    add_plural("lanzas");
    fijar_genero(2);
    
    set_long("Te invade la gratificante sensación de estar contemplando una reliquia "
    "no sólo de la guerra, sino también del arte, ya que las lanzas índigo son el "
    "producto de un esmerado y lento proceso de forja y artesanía. Se trata de una "
    "lanza de un metro y noventa centímetros de longitud aproximadamente, de los "
    "cuales la cabeza metálica sólo supone un palmo de altura. El cuerpo del arma "
    "es sin duda la parte más cuidada, ya que ha sido labrada cual estatua de "
    "marfil. El patrón dotado al cuerpo tiene motivos marinos; hay numerosas "
    "figuras semejantes a las olas del océano y a cuerpos de criaturas antropomórficas "
    "parecidas a sirenas y tritones. Toda la madera ha sido cubierta con varias "
    "capas de barniz azul oscuro, de tal forma que contrasta con el brillante color "
    "índigo de la cabeza metálica, con forma de prisma triangular recto. Su acero "
    "refulge bajo la luz, al igual que otras armas que pueblan los tesoros marinos, "
    "como las armas cerúleas y celestes. La historia de esta familia de armas "
    "permanece desconocida; se te ocurre que tal vez, en tiempos más nobles, esta "
    "lanza perteneciese a alguna criatura ultraterrenal: un avatar de la misma Nirvë.\n");
    
    fijar_peso(dame_peso() + 250);

    fijar_encantamiento(10);
    nuevo_ataque("agua", "agua", 1, 30, 10);

    ajustar_vida_max(25, 1);
    fijar_valor(50*500);
}
/*
int rutina_ataque(object defensor, object atacante, int msjs, int bono, int no_esquivar, int flag)
{
    if (environment(atacante)) {
        if (environment(atacante)->dame_agua() && !query_ataque("encantado_agua")) {
            tell_object(atacante, "Tu "+query_short()+" arremolina el agua de enderredor, que gira en torno a "+dame_pronombre()+" formando un cono atacante.\n");
            tell_accion(environment(atacante), "El arma de "+atacante->query_short()+" arremolina a su alrededor el agua del entorno, formando un cono atacante.\n", "Escuchas un movimiento de agua.\n", ({ atacante }), atacante);
            add_attack("encantado_agua", "agua", 1, 15, 8, 6, 0, 0, 1);
        } else if (!environment(atacante)->dame_agua() && query_ataque("encantado_agua")) {
            remove_attack("encantado_agua");
        }
    }
    return ::rutina_ataque(defensor, atacante, msjs, bono, no_esquivar, flag);
}

string dame_nombre_material()
{
    return "Un magnífico cuerpo de madera labrada y barnizada, y una cabeza metálica de acero";
}

string descripcion_encantamiento()
{
    return ::descripcion_encantamiento() + "%^BLUE%^BOLD%^El encantamiento presente en este arma se multiplica en entornos acuáticos.%^RESET%^";
}

*/