// Satyr 2012
inherit "/obj/arma.c";
inherit "/obj/magia/objeto_formulador.c";
void setup()
{
    fijar_arma_base("espada a dos manos");
    set_name("espada");
    add_alias(({"espada", "redentora", "espada redentora"}));
    add_plural(({"espadas", "redentoras", "espadas redentoras"}));
    set_short("%^BOLD%^WHITE%^Espada%^CYAN%^ Redentora%^RESET%^");
    set_main_plural("%^BOLD%^WHITE%^Espadas%^CYAN%^ Redentoras%^RESET%^");
    set_long(
        "Una espada a dos manos de cruz muy recargada y empuñadura de oro. Su filo de plata"
        " es de un blanco impoluto a pesar de haber combatido en un centenar de guerras. "
        "Antaño símbolo de los Señores del Bien, estas armas pronto perdieron su poder divino "
        "y se convirtieron en reliquias del pasado que ningún vínculo mantienen ya con su "
        "antiguo señor.\n"
    );
    
    set_read_mess(  "Indica El Camino A La Justicia, Indica El Camino A La Absolución", "adurn", 0);
    fijar_encantamiento(30);
    
    add_static_property("bien"   , 5);
    add_static_property("mal"    , 5);
    add_static_property("divino" , 5);
    
    nuevo_efecto_basico("antimagia");
    fijar_esfera(2);
    fijar_nivel(20);
}
int disipa(string tx) 
{ 
    return formular("disipar magia", tx, 1); 
}
void init()
{
    ::init();
    add_action("disipa", ({"indica", "indicar"}));
}
void iniciar_formulacion(string nombre_hechizo,object portador)
{
    tell_object(portador, "Realizas una serie de aspavientos con tu " + query_short() + ", despertando el poder olvidado que está encerrado en ella.\n");
    
    tell_accion(environment(portador),
        portador->query_cap_name() + " realiza una serie de aspavientos con su " + query_short() + ", despertando un poder olvidado que está "
        "encerrado en ella.\n",
        "",
        ({portador}),
        portador
    );
}
void realizar_cantico(object lanzador,object hechizo,string cantico)
{
    tell_room(environment(lanzador),"¡" + query_short() + " comienza a envolverse en un aura azulada de magia de abjuración!\n");
}