//Alguien antes que yo
//Sierephad Jun 2k19	--	Mejorando los alias y los plurales

inherit "/obj/proyectiles/arma_proyectiles.c";
void setup()
{
    fijar_arma_base("arco largo");

    set_name("arco");
    set_short("%^BOLD%^YELLOW%^Arco Élfico de %^RESET%^%^ORANGE%^Edhelorn%^RESET%^");
    set_main_plural("%^BOLD%^YELLOW%^Arco Élfico de %^RESET%^%^ORANGE%^Edhelorn%^RESET%^");
    add_alias(({"arco", "elfico", "arco elfico", "élfico", "arco elfico", "arco elfico de edhelorn"}));
    add_plural(({"arcos", "élficos", "elficos", "arcos elficos", "arcos élficos"}));
	generar_alias_y_plurales();
	set_long(
        " Esta maravilla de la artesanía pertenece a eras muy pasadas, cuando los árboles Edhelorn "
        "eran mucho más abundantes. En la época en la que Wareth era un único bosque que manchaba "
        "la gran parte de Dalaensar con su oscuro color verde, los elfos se dividían en pequeños "
        "grupos, normalmente familiares, que migraban de una parte a otra del bosque, buscando "    
        "siempre la presencia de un Edhelorn para formar un asentamiento cerca. Estos árboles "
        "sagrados Élficos, increíblemente longevos, en terribles ocasiones morían por causas "
        "naturales. Cuando esto era así su cuerpo se usaba para crear armas como este arco que "
        "tienes aquí, que eran entregados a los guerreros más honorables de la casta de cazadores "
        "tras hacer jurar que los usarían con sabiduría.\n\n"
        " La madera de Edhelorn es dura como el hierro y tan maleable para un artesano como la masa "
        "de pan. Toda su longitud está tallada con runas élficas antiguas dedicadas para el cazador "
        "que iba a empuñarla. Su cuerda, de material no muy claro, es elástica y resistente. El "
        "secreto de la fabricación de estos arcos se perdió en el tiempo, cuando los Edhelorn "
        "empezaron a desaparecer, también los artesanos capaces de crear estas maravillas. A día de "
        "hoy sólo quedan unos pocos guardados por los elfos de Tearolin y otro puñado en manos de "
        "criaturas avariciosas, bandidos, anticuarios o aventureros poderosos.\n"
    );
    
    fijar_encantamiento(30);
    fijar_alcance(dame_alcance() + 1);
    fijar_vida_max(dame_vida_max() * 2);
    //nuevo_efecto_basico("ejecutor", 100);
    nuevo_efecto_basico("ejecutor", 60);
} 
void perder_concentracion()
{
    object quien = environment();

    if (quien && living(quien)) {
        tell_object(quien, "La magia de tu arco impide que te desconcentres al disparar.\n");
    }
}