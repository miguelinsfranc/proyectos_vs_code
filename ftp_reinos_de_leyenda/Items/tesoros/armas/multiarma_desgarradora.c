// Satyr 21.06.2016
// Eckol Ago19: Fijar valor venta
inherit "/obj/multiobjetos/multiarma.c";
void setup()
{   
    fijar_genero(1);
    fijar_titulos("%^RED%^Desgarradora%^RESET%^", "%^RED%^Desgarradoras%^RESET%^");

    set_short("Multiarma %^RED%^Desgarradora%^RESET%^");
    set_main_plural("Multiarmas %^RED%^Desgarradoras%^RESET%^");
    fijar_long_generico(
        "Un orbe marfileño rojizo que puede ser transmutado en todo tipo de armas.\n"
    );
    
    fijar_encantamiento(10);
}
void setup_especifico() 
{
    nuevo_efecto_basico("carnicero", 50);
    fijar_valor_venta(dame_valor_real()*3);
}
void poner_descripcion_especifica_multiobjeto() {
    set_long(
        capitalize(dame_numeral()) + " " + dame_arma_base() + " que ha sido "
            + dame_texto_forjado() + " con malas intenciones, ya que el daño que puede "
            "causar es increíble -especialmente en bestias-. Está tintad" + dame_vocal() + 
            " de rojo a causa de toda la sangre que ha derramado.\n"
    );
}
void poner_short_especifico_multiobjeto() {
    set_short(
        capitalize(dame_multiobjeto_base()) +  " %^RED%^Desgarrador" + dame_a() + "%^RESET%^"
    );
    set_main_plural(
        capitalize(pluralize(dame_multiobjeto_base())) + " " + 
            " %^RED%^Desgarrador" + dame_ae() + "s%^RESET%^"
    );
}
