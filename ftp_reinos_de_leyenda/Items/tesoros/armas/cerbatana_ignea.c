/*
Ánonimo anterior al 18-01-2010

Rutseg 15-II-2010
	Esto se debería hacer con un weapon_attack en vez de con todo esto, y todo el rollo 
    de los mensajes sobra mucho y en todo caso si se quieren poner se pueden hacer con 
    los sistemas que tiene la mudlib para ello

Satyr 24-06-2010
    Hago un apaño para que funcione bien con /habilidades/cazadores/base_disparo
*/

inherit "/obj/proyectiles/arma_proyectiles.c";

void setup()
{
    fijar_arma_base("cerbatana");

    set_name("cerbatana");
    set_short("%^BOLD%^BLACK%^Cerbatana %^BOLD%^RED%^Ígnea%^RESET%^");
    set_main_plural("%^BOLD%^BLACK%^Cerbatana %^BOLD%^RED%^Ígnea%^RESET%^");
    add_alias(({"cerbatana", "ignea", "cerbatana ignea"}));
    add_plural(({"cerbatanas", "igneas", "cerbatanas igneas"}));
    set_long(
        " Una cerbatana fraguada en obsidiana con la forma de un dragon estirado con las fauces "
        "abiertas. Este objeto es un pequeño instrumento de destrucción, ya que es capaz de inflamar"
        " mediante complicada magia de encantamiento y azufre los dardos que dispara. Es ligera, "
        "muy resistente y muy bonita, además de ser muy peligrosa. Su origen es incierto, aunque "
        "la unión de mentes que ha dado como fruto este artefacto ha tenido que sumar además de un "
        "gran herrero, hechiceros e inventores poderosos.\n"
    );

    ajustar_BE(15);
    ajustar_BO(10);
    fijar_encantamiento(20);
    fijar_alcance(dame_alcance() + 1);

    add_static_property("fuego", 15);
    nuevo_ataque("ígneo", "fuego", 2, 25, 20);
}
/*
private void poner_ataque_ignea(object b)
{
    b->nuevo_ataque("cerbatana_ignea","fuego",2,25,0,0,0,0,0);
}
private void quitar_ataque_ignea(object b)
{
    b->quitar_ataque("cerbatana_ignea");
}
void efecto_disparo(object pj, object victima, object flecha, int i, int pupa, mixed loc)
{
    if (flecha) {
        quitar_ataque_ignea(flecha);
    }
}
void efecto_pre_disparo(object pj, object victima, object flecha, int i, int pupa, mixed loc)
{
    if (flecha && victima->dame_invulnerable_hechizos() < 2) {
        poner_ataque_ignea(flecha);
    }
}
int impacto_proyectil(object arquero,object objetivo,object municion,string direccion,int distancia)
{
	int orig;

	if(municion && objetivo && objetivo->dame_invulnerable_hechizos()<2)
        poner_ataque_ignea(municion);
	
    orig= ::impacto_proyectil(arquero,objetivo,municion,direccion,distancia);
	
	//Si aún tiene el ataque, se lo quitamos
	if(municion)
        quitar_ataque_ignea(municion);

	return orig;
}
*/