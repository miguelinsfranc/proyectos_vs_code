// Dunkelheit 02-06-2011
inherit "/obj/arma.c.c";

void setup()
{
    fijar_arma_base("hacha de batalla");
    set_name("hacha de batalla");
    set_short("Hacha de Batalla %^CYAN%^BOLD%^%^Regia%^RESET%^");
    add_alias(({"hacha", "batalla", "regia"}));
    set_main_plural("Hachas de Batalla %^CYAN%^BOLD%^%^Regias%^RESET%^");
    add_plural(({"hachas", "batallas", "regias", "hachas de batalla"}));
    
	set_long("Es un hacha de reyes. Originalmente nacida en las cordilleras malditas "
	"de Bûrzum, el límite septentrional de Golthur, con un nombre indigno que aquí no será nombrado. "
	"Recogidas de entre los cuerpos sin vida de las hordas goblinoides, la sangre que bañaba este arma "
	"fue erradicada con la bendición de los nobles clérigos de Eralie. El resultado de esta conversión "
	"es un arma de gran poder pero que tan sólo las criaturas de corazón noble serán capaces de empuñar.\n");

	fijar_encantamiento(10);
    nuevo_efecto_basico("carnicero", 50);
}

int set_in_use(int i)
{
    if (i && environment(this_object())->dame_alineamiento() > 0) {
        tell_object(environment(this_object()), "Tu moral te impide portar un arma tan noble y de reputación benévola.\n");
        return 0;
    }
    return ::set_in_use(i);
}

