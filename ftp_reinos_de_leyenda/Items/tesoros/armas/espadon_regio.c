// Dunkelheit 02-06-2011
inherit "/obj/arma.c.c";

void setup()
{
    fijar_arma_base("espadon");
    set_name("espadon");
    set_short("Espadón %^CYAN%^BOLD%^%^Regio%^RESET%^");
    add_alias(({"regio"}));
    set_main_plural("Espadones %^CYAN%^BOLD%^%^Regios%^RESET%^");
    add_plural(({"espadones", "regios"}));
    
	set_long("Es un espadón de reyes. Originalmente nacida en las cordilleras malditas "
	"de Bûrzum, el límite septentrional de Golthur, con un nombre indigno que aquí no será nombrado. "
	"Recogidas de entre los cuerpos sin vida de las hordas goblinoides, la sangre que bañaba este arma "
	"fue erradicada con la bendición de los nobles clérigos de Eralie. El resultado de esta conversión "
	"es un arma de gran poder pero que tan sólo las criaturas de corazón noble serán capaces de empuñar.\n");

	fijar_encantamiento(10);
    nuevo_efecto_basico("carnicero", 50);
}

int set_in_use(int i)
{
    if (i && environment(this_object())->dame_alineamiento() > 0) {
        tell_object(environment(this_object()), "Tu moral te impide portar un arma tan noble y de reputación benévola.\n");
        return 0;
    }
    return ::set_in_use(i);
}

