// Satyr 2k8 -- Maza a dos manos encantada y utilizable por bardos!!
// Zoilder 21/06/2011 - Asigno género a la maza.
#include "/clases/bribones/bardo/musica.h"
#include <combate.h>
//#define GONG "¡¡%^BOLD%^YELLOW%^GONNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNG%^RESET%^!!\n"
inherit "/obj/arma.c";

/*
dame_instrumento
dame_maestria_base
instrumento->dame_maestria_base()
*/
string descripcion_juzgar(object o)
{
    return "Una maza como ésta puede ser usada como instrumento de percusión.";
}
void setup()
{
    fijar_arma_base("maza a dos manos");
    set_name("maza");
    add_alias(({"maza", "gong", "maza de gong gigante"}));
    add_plural(({"mazas", "gong", "mazas de gong gigante"}));
    set_short("%^ORANGE%^Maza de Gong Gigante%^RESET%^");
    set_main_plural("%^ORANGE%^Mazas de Gong Gigante%^RESET%^");
    set_long(
        " Las mazas de Gong son los instrumentos utilizados para hacer sonar estos místicos "
        "instrumentos. Está formada por un robusto mango de roble y una cabeza redonda maciza "
        "rodeada de un montón de tela para amortiguar el impacto contra el instrumento y no "
        "deteriorarlo con el paso del tiempo. El problema es que esta es de un tamaño ¡¡DESCOMUNAL!! "
        "Con casi dos metros de longitud y una cabeza con el tamaño de un gnomo esto que tienes ante "
        "ti es capaz de convertirse en un arma devastadora... ¡¡hasta en manos de un bardo!!\n"
    );

    fijar_encantamiento(10);
    ajustar_BO(25);

    // Antes era 4d50+25. Esto se le aproxima.
    ajustar_danyo_minimo("tabla", 33);
    nuevo_efecto_basico("canalizador", 60);
	add_static_property("nivel_minimo",20);
}
string *mensajes_base(int dam, string tipo, string local, object at, object def, int critico)
{
    return "/std/weapon_mess.c"->query_message(dam, tipo, local, at, def, critico);
}
status dame_legal_universal()
{
    object pj = environment();

    if (!pj || !living(pj) || pj->dame_clase() != "bardo") {
        return 0;
    }

    return 1;
}
int comprobar_previo_valido()
{
    object *aux = previous_object(-1);
    
    if ( ! aux || ! sizeof(aux) )
        return 0;

    foreach(object previo in aux) {
        if ( ! previo ) 
            continue;

        if ( base_name(previo) == "/habilidades/bribones/interpretar" )
            return 1;

        if ( previo->dame_cancion() )
            return 1;
        
    }
    
    return 0;
}
int dame_instrumento()
{
    return comprobar_previo_valido();
}
string dame_maestria_base()
{
    if ( ! comprobar_previo_valido() )
        return ::dame_maestria_base();
    
    return capitalize(_PERCUSION); 
}
string dame_familia()
{
    if ( ! comprobar_previo_valido() )
        return ::dame_familia();
    
    return capitalize(_PERCUSION);
}
