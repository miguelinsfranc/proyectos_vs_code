// Satyr 2016.12.30
#include <baseobs_path.h>
#include <combate.h>
inherit BARMAS + "lanza.c";
string  dame_nombre_material()
{
    return "¡Ni idea!, ¿quizás una... glándula de algún insecto?";
}
void setup()
{
    string tx;

    fijar_arma_base("lanza");
    set_name("aguijon");

    set_short("%^BOLD%^BLACK%^Aguijón del "
              "%^MAGENTA%^D%^RESET%^MAGENTA%^eseo%^RESET%^");
    set_main_plural(
        "%^BOLD%^BLACK%^Aguijones del %^MAGENTA%^D%^NOBOLD%^eseo%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Una larga extensión de masa quitinosa coronada por un aguijón "
        "de hueso que sujeta una burbujeante bolsa de fluidos. Del interior "
        "de éste apéndice rezuma una extraña baba rosácea.\n\n"

        "  Emite un asqueroso olor dulzón que te nubla la visión y por "
        "momentos "
        "te parece escuchar algún extraño crujido del interior de la "
        "bolsa superior.\n\n"

        "  Sin duda es una parte arrancada de alguna criatura cuyo origen "
        "desconoces "
        "y no estás seguro de querer averiguar, pues es horripilante y "
        "su extraño tacto, aunque firme y ligero, es muy húmedo a causa "
        "de la ingente cantidad de líquido viscoso que se derrama de la "
        "bolsa de perdición en su parte superior.\n\n"

        "  Algunos estudiosos afirman que esta asquerosidad es una glándula "
        "sexual "
        "de alguna especie de insecto gigante y que la baba que rezuma es "
        "una poderosa feromona creada para atraer a los especímenes del "
        "sexo opuesto. Pero... ¿qué sabran estos estudiosos?, después de "
        "todo, tras inspeccionar mejor este extraño objeto te das cuenta "
        "de que goza de una peculiar belleza que te atrae sin remedio...\n");

    fijar_genero(1);
    fijar_material(7);

    ajustar_BO(25);
    ajustar_BP(25);
    fijar_encantamiento(10);

    nuevo_ataque("baba_sexual", "mental", 2, 5, 5);

    tx = nuevo_efecto_basico("provocacion");
    fijar_ataques_efecto(tx, ({"baba_sexual"}));
    fijar_sms_efecto(
        tx,
        (
            : ({sprintf(
                    "¡%s inyecta algo en %s que provoca que centre su atención "
                    "en ti!",
                    capitalize(dame_tu()) + " " + query_short(),
                    $1->query_cap_name()),

                sprintf(
                    "¡%s de %s te inyecta algo que te obliga a centrar tus "
                    "ataques en %s!",
                    capitalize(dame_articulo()) + " " + query_short(),
                    $2->query_cap_name(),
                    $2->dame_pronombre()),

                ""})
            :));

    nuevo_efecto_basico("sanguijuela");
    tx = nuevo_efecto_basico("interruptor");
    fijar_triggers_efecto(tx, ({_TRIGGER_IMPACTO_C}));
}
string descripcion_juzgar(object b)
{
    return "El efecto 'interruptor' solo se aplicará en golpes críticos.";
}

varargs string *dame_mensaje_combate_impacto(
    int pupa, string tipo_danyo, string loc, object at, object def, mapping res,
    string n_ataque, object arma)
{
    string tx = res && res["crítico"] ? " críticamente"
                                      : dame_texto_pupa(tipo_danyo, pupa);

    if (-1 == member_array(tipo_danyo, ({"mental", "penetrante"})))
        return ::dame_mensaje_combate_impacto(
            pupa, tipo_danyo, loc, at, def, res, n_ataque, arma);

    if (tipo_danyo == "penetrante")
        return at->dame_ob_estilo("aguijon")->dame_mensaje_combate_impacto(
            pupa, tipo_danyo, loc, at, def, res, n_ataque, arma);

    return ({
        sprintf(
            "%s inyecta%s baba en %s de %s.",
            capitalize(dame_tu()) + " " + query_short(),
            tx,
            loc,
            def->query_cap_name()),

        sprintf(
            "%s de %s te inyecta%s baba en %s.",
            capitalize(dame_articulo()) + " " + query_short(),
            at->query_cap_name(),
            tx,
            loc),

        sprintf(
            "%s de %s inyecta%s baba en %s de %s.",
            capitalize(dame_articulo()) + " " + query_short(),
            at->query_cap_name(),
            tx,
            loc,
            def->query_cap_name()),
    });
}
varargs string *dame_mensaje_combate_critico(
    int pupa, string tipo_danyo, string loc, object at, object def, mapping res,
    string n_ataque, object arma)
{
    return dame_mensaje_combate_impacto(
        pupa, tipo_danyo, loc, at, def, res, n_ataque, arma);
}
