// Satyr 28.11.2016
// Sierephad Ene 2k20	--	Mejorando los alias y plurales

#include <unidades.h>
inherit "/obj/arma_de_mano.c";
void setup()
{
    set_name("arma");
    set_short("Armas de mano %^RED%^Desgarradoras%^RESET%^");
    set_main_plural("pares de Armas de mano %^RED%^Desgarradoras%^RESET%^");
    generar_alias_y_plurales();
	add_alias("desgarradora");
	add_plural("desgarradora");
    
    set_long(
        "Un par de cuchillas de ónice afiladas como navajas con un asidero "
            "de piedra fría. Su superficie esta manchada con la sangre reseca "
            "de la infinidad de bestias a las que han destrozado.\n"
    );
    
    fijar_genero(2);
    fijar_encantamiento(10);
    nuevo_efecto_basico("carnicero", 50);
	fijar_valor(20*500);
}
