// Satyr 2k8
// Eckol 22Ago15: Le pongo el material.
// Astel 09.11.15 . Efecto Adrenalina
inherit "/obj/arma.c";

void setup()
{
    string aux;

    fijar_arma_base("puñal");
    set_name("puñal");

    set_short("Puñal fugaz");
    set_main_plural("Puñales fugaces");
    generar_alias_y_plurales();

    set_long(
        " Un puñal de aspecto mortífero fabricado a partir del mejor de los "
        "hierros "
        "templados y afilado hasta el extremo "
        "de llegar a ser capaz de cortar un pelo al vuelo. Su empuñadura "
        "es de cuero y está diseñada para que el arma no "
        "se mueva ni un ápice de la mano de su dueño pase lo que pase. "
        "Esto sumado a su ligereza, resistencia y precisión "
        "lo convierten en el puñal favorito de todos los expertos, así como "
        "el de los más novatos, puesto que es mucho más facil "
        "aprender a manejar un puñal como este.\n");

    fijar_BO(dame_BO() + 35);
    fijar_BE(dame_BE() + 3);
    fijar_encantamiento(10);

    aux = nuevo_efecto_basico("ataques_rapidos");
    fijar_bloqueo_efecto(aux, 4);

    nuevo_efecto_basico("adrenalina", 5);
    fijar_ataques_efecto("Adrenalina", keys(dame_ataques()));
    fijar_duracion_efecto("Adrenalina", 60);
    add_static_property("minimo_atributos", (["des":19]));
}
