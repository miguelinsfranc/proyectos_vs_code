// Dunkelheit 18-06-2009
inherit "/obj/arma";
void setup()
{
    fijar_arma_base("luz del alba");
    set_name("estrella");
    set_short("%^BLACK%^BOLD%^Estrella del Ocaso%^RESET%^");
    add_alias("ocaso");
    set_main_plural("%^BLACK%^BOLD%^Estrellas del Ocaso%^RESET%^");
    add_plural(({"estrellas", "ocaso"}));
    fijar_genero(2);
    
    set_long("Es una estrella del alba cuyo aspecto es lo contrario a lo que este nombre sugiere, pues "
    "es del color de una noche sin luna ni estrellas. Su cabeza metálica, anclada a un pesado y extenso "
    "mango de madera maciza, tiene veintiún púas metálicas de corte piramidal. Esta estrella guiará "
    "por un calvario a tus enemigos, sin lugar a duda, pero... ¿qué encierra su atmósfera oscura?\n");
    
    add_static_property("bien", 3);
    fijar_encantamiento(21);
    ajustar_BO(30);
    
    nuevo_efecto_basico("alineamiento",  4);
    nuevo_efecto_basico("intolerancia", 20);
}