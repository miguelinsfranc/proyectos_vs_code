// Dunkelheit 18-06-2009
// Zoilder 09/12/2011: - Corrección de typos.
// Baelair 13/01/2014: - Solo se puede usar 1
// Satyr   30/11/2015: - 
#include <combate.h>
inherit "/obj/arma";

string dame_nombre_material() { return "cientos de capas del cristal más puro y refinado"; }

void setup()
{
    fijar_arma_base("puñal");
    set_name("puñal");
    set_short("%^YELLOW%^Puñal %^CYAN%^Cristal%^RESET%^");
    add_alias(({"cristal", "punyal", "pugnal"}));
    set_main_plural("%^YELLOW%^Puñales %^CYAN%^Cristal%^RESET%^");
    add_plural(({"cristales", "punyales", "pugnales", "puñales"}));
    fijar_genero(1);
    
    set_long("Este ligero y lujoso puñal es uno de los tesoros más buscados y codiciados de la Era 2ª, "
    "pues apenas sobrevivieron unos cuantos al Cataclismo. El Puñal Cristal es un objeto mágico que "
    "fue entregado por el antiguo Señor de Golthur a Kolooo, el cacique de los Goblins Oscuros que habitaban "
    "en el ya extinto bosque del norte de la Fortaleza Negra. La hoja de este puñal es de un cristal tan "
    "puro y refinado que apenas es visible a la luz del día; lo cual, sumado a su liviano peso, permite "
    "infligir un mayor número de punciones en mitad del combate.\n");

    add_static_property("tipo", ({"bribon"}));
    add_static_property("messon", "¡Lanzas un par de ataques al aire para deleitarte con la letal velocidad "
    "del puñal! ¡con él, pareces invencible!\n");
        
    //add_attack("puñal_cristal", "aire", 2, 40, 20, 0, 0, 0, 0);
    add_attack("puñal_cristal", "aire", 2, 20, 41, 0, 0, 0, 0);
    fijar_encantamiento(30);
    //ajustar_danyo_minimo("tabla", 20);
    
    ajustar_BO(8);
    ajustar_BE(5);
    fijar_material(5);
    
    nuevo_efecto_basico("presteza");
    fijar_triggers_efecto("Presteza", ({_TRIGGER_APUNYALAR}));
	add_static_property("nivel_minimo",29);
}

void event_apunyalar(object habilidad, object quien, object objetivo, int danyo)
{
    if (danyo == -300 && objetivo->dame_pvs() > 0) {
        tell_object(quien, "Pese a no surtir efecto tu puñalada, eres capaz de infligir una punción a "
        "tu víctima al retirarte de su espalda.\n");
        rutina_ataque(objetivo, quien, 0, 16, 1, 0);
    }
}

int set_in_use(int i)
{
    object *emp;
    if (i)
    {
        emp = environment()->query_held_ob();
        for (int j = sizeof(emp); j--;)
        {
            if (real_filename(emp[j]) == real_filename(this_object()))
            {
                tell_object(environment(), "Eres incapaz de equilibrar dos armas tan poderosas a la vez.\n");
                return 0;
            }
        }
    }
    return ::set_in_use(i);
}

