// Dunkelheit 18-06-2009
inherit "/obj/arma";

void setup()
{
	fijar_arma_base("estoque");
	set_name("estoque");
	set_short("Estoque de Elaine");
	add_alias("elaine");
	set_main_plural("Estoques de Elaine");
	add_plural("estoques");
	fijar_genero(1);
	
	set_long("Se trata de un estoque ligero y de diseño vanguardista, carente de los extravagantes excesos "
	"barrocos tan apreciados por la mayoría de los espadachines, y de los que la maestra de armas que "
	"concibió este estoque -Elaine Marley, antigua gobernadora de la Isla de Bucanero- siempre intentó "
	"desmarcarse. Un sencillo guardamanos dorado cubre la empuñadura de cuero negro. La forma de la hoja, "
	"de corte ligeramente rectangular, es la única diferencia con la otra arma predilecta de Elaine: el "
	"florete.\n");
	
	fijar_encantamiento(6);
}
