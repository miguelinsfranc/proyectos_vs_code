// Satyr 09.11.2015
inherit "/obj/proyectiles/arma_proyectiles.c";
void setup()
{
    fijar_arma_base("ballesta");

    set_name("ballesta");
    set_short("%^MAGENTA%^Ballesta Duergar de Repetición%^RESET%^");
    set_main_plural("%^MAGENTA%^Ballestas Duergar de Repetición%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        " Esta ballesta ha sido fabricada en las entrañas de la suboscuridad, pero no utiliza "
            "los mismos materiales que se utilizan en el resto de armas de los infrahabitantes, ya que "
            "no parece afectar a su "
            "integridad el hecho de estar en la superficie.\n\n"
        
        "  Tiene un cuerpo es de madera oscurecida, pesado "
            "a primera impresión, pero capaz de soportar mucho tiempo sin dañarse. "
            "Posee hueco para "
            "dos saetas que son propulsadas por una cuerda de acero duergar. Es un arma letal y que "
            "requiere acostumbrarse para ser disparada. El influjo de la suboscuridad, además, le "
            "confiere una magia innata y dificil de explicar para cualquier versado en la materia... "
            "pero que funciona.\n"
    );

    quitar_ataque("tabla");
    
    fijar_encantamiento(30);
    nuevo_ataque("rafaga_1", "penetrante", 1, 80, 0);
    nuevo_ataque("rafaga_2", "penetrante", 1, 80, 0);
    
    fijar_efectos(([]));
    
    preparar_efectos_basicos("penetrante", ({"rafaga_1", "rafaga_2"}));
} 
