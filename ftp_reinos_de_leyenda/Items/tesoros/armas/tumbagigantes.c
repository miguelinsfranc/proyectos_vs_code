// Satyr 10.11.2015
inherit "/obj/proyectiles/arma_proyectiles.c";
void setup()
{
    fijar_arma_base("honda");

    set_name("tumbagigantes");
    set_short("%^BOLD%^YELLOW%^Tumbagigantes%^RESET%^");
    set_main_plural("%^BOLD%^YELLOW%^Tumbagigantes%^RESET%^");

    add_alias(({"honda", "tumbagigantes"}));
    add_plural(({"hondas","tumbagigantes"}));

    set_long(
        "Narra la leyenda que un halfling heróico fue capaz de derrotar a un "
            "poderoso gigante de hielo en una épica batalla. El tiempo "
            "tergiversó la historia, pues el halfling era un cobarde que usó "
            "este poderoso artilugio que ahora sostienes para acabar con su "
            "víctima dormida.\n\n"
            
        "  Hecho con un tendón de gigante de las cavernas, esta honda dispara "
            "proyectiles encantados con un poderoso sortilegio que se nutre "
            "de la fuerza de tu enemigo para atacar con fuerza.\n\n"
            
        "  Valiéndose de ésto, nuestro astuto halfing derrotó al gigante con "
            "una pedrada con la colosal fuerza de su monstruoso enemigo y "
            "después cobró la recompensa ("
            "y se cepilló a la princesa de turno).\n"
    );

    fijar_encantamiento(30);
    nuevo_efecto_basico("mimetismo", 300);
}
