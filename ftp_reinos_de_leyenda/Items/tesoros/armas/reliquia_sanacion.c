// Satyr 15/05/2018
inherit "/obj/magia/reliquia";

void setup()
{
    set_name("reliquia");
    set_short("Reliquia Divina de la %^CYAN%^Sanación%^RESET%^");
    set_main_plural("Reliquias Divinas de %^CYAN%^Sanación%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Una columna de mármol en miniatura coronada por un elefante y "
        "cuya parte inferior termina en un mango de mármol. A priori "
        "puede parecer que es algún tipo de maza demasiado ostentosa, pero "
        "la verdad es que se trata de un canalizador de poder divino que "
        "permite a sacerdotes y caballeros potenciar sus poderes curativos.\n\n"

        "  El secreto de su origen se remonta a una época en la que los elfos "
        "aún no habían creado la magia rúnica (lo que significa que tampoco "
        "existía la arcana), pero ha perdurado hasta nuestros días, pues "
        "fue guardado celosamente por todas las órdenes eclesiásticas "
        "habidas y por haber.\n");

    inicializar_reliquia();

    add_static_property("poder_curacion", 50);
    fijar_valor(100000);
    fijar_material(6);
    fijar_peso(5000);
}
