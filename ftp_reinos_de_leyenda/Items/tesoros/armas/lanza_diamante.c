// Satyr 2k8
// Arma muy poderosa
// Satyr 18.11.2014 -- Solo se puede empuñar 1.
// Isham 05/02/2015 - Puesto el genero correctamente.
// Satyr 03.03.2016 -- Alias y plurales, nuevos mensajes

// Esto es algo especial de las lanzas.
// Si vais a hacer otra arma tendríais que poner solo ésta línea:
// inherit "/obj/arma.c";

#include <baseobs_path.h>
inherit BARMAS + "lanza.c";

void setup()
{
    // Lo primero es elegir un tipo de arma
    fijar_arma_base("lanza");
    
    // Ahora le ponemos el nombre, el corto, el plural y las descripciones
    set_name("lanza");
    set_short("%^BOLD%^WHITE%^Lanza de %^CYAN%^Diamante%^RESET%^");
    set_main_plural("%^BOLD%^WHITE%^Lanzas de %^CYAN%^Diamante%^RESET%^");
    set_long(
        " Hermoso y letal es el artefacto traedor de muerte que "
            "sostienes, legado de una Era pasada en la que "
            "todos, dioses y mortales, eran más nobles y poderosos. "
            "Construída con materiales que el mundo de hoy ya "
            "casi no conoce, esta lanza era el arma predilecta de "
            "los antiguos caballeros jinetes de dragón que "
            "batallaban en los cielos en legendarios combates "
            "repletos de heroicidades que llenaron libros de historia "
            "y de táctica militar.\n\n"
            
        " Dejando de lado las maravillas de la época de su creación y "
            "de sus portadores, esta lanza es una obra de arte "
            "para todo aquel que pueda utilizarla. Es un arma letal "
            "con una cabeza de diamante y un mango de cristal estelar. "
            "Tan resistente es que ni los implacables mordiscos de un "
            "dragón son capaces de quebrarla.\n\n"
            
        " Cada centímetro del ástil del arma está repleto de inscripciones "
            "poderosas que le otorgan una magia protectora contra "
            "sortilegios enemigos a la par que la iluminan cuando esta "
            "se mueve para golpear, dejando en el aire"
            " estelas amarillentas.\nºn"
        
        " En lo que los apasionados de las lanzas denominan como "
            "\"el cuello\" (la parte inmediatamente anterior a la cabeza), "
            "un hermoso labrado de oro en forma de garras le da un toque de "
            "magestuosidad a este arma que"
            ", sin lugar a dudas, es todo un orgullo portar.\n"
    );
    
    // Esto es para generar los alias. Usadlo así siempre.
    generar_alias_y_plurales();
    
    // Vamos a personalizar el arma.
    
    // Primero: es una lanza, género femenino.
    fijar_genero(2);
    
    // Vamos a darle 5 veces más vida que una lanza base.
    fijar_vida_max(dame_vida_max() * 5);

    // Material: Cristal.
    fijar_material(5);

    // +10 más de BO que cualquier otra lanza.
    ajustar_BO(10);
    
    /* Ahora el encantamiento: +30.
     *
     * 30 de enchant significa que el arma tiene las siguientes ventajas
     * respecto a una lanza normal:
     *
     * - +30 de daño
     * - +30 de BO
     * - +30 de BP
     * - Vida (y precio) incrementados
     */
    fijar_encantamiento(30);
        
    /* Añadiendo una propiedad con el nombre de un elemento (bien, mal, agua, etc)
     * estamos modificando las bonificaciones / penalizaciones que dicho arma otorga
     * a un elemento. Podéis ver la lista entera de elementos escribiendo el comando
     * 'resistencias'.
     * En particular, este arma otorga -6 a bien y mal puesto que está muy ligada
     * a los dragones, que suelen ser vulnerables a +estos daños. */
    add_static_property("bien",  -6);
    add_static_property("mal" ,  -6);
    
    /* Podemos meter una propiedad estática con el nombre corto de un atributo (
     * int, car, sab, con, fue, des) y el objeto pasará a dar una bonificación/
     * /penalización a dicho atributo. éste arma da +1 inteligencia.
     */
    add_static_property("int",    1);

    /* Ésto último sirve para hacer que el arma tenga +20 de daño extra.
     * Podríamos subir el encantamiento a +50, pero así el arma tendría más BO,
     * BP, etc (como describimos anteriormente) y no es necesario.
     */
    ajustar_danyo_minimo("tabla", 20);
	add_static_property("nivel_minimo",29);
    
    foreach(string str, mixed data in dame_efectos_tipo("penetracion"))
        quitar_efecto(str);

    nuevo_efecto_basico("penetracion", __ud(40, "%"));
}
/**
 * Ahora vamos a personalizar los mensajes de combate.
 *
 * Para ello tenemos que "enmascarar" ésta función.
 *
 * Enmascarar significa "redinir". En la mudlib esta función hace otra cosa
 * pero como nosotros queremos que cambie, la "redefinimos".
 *
 * Esta función tiene que devolver siempre un array de tres posiciones en los
 * que figurarán los mensajes a mostrar a atacante, defensor y sala, respectivamente.
 *
 * Para tunearlos a gusto tenemos varios parámetros interesantes que pasamos aquí:
 *
 * - (int)     pupa      : cantidad de daño que realizamos (por si queremos tunear en función del daño)
 * - (string)  tipo_danyo: tipo de daño del ataque (cortante, aplastante, fuego, bien, etc.)
 * - (string)  local     : localizacion del golpe (cabeza, pie, pidigio...)
 * - (object)  at        : el jugador o criatura que ataca
 * - (object)  def       : el jugador o criatura que defiende
 * - (mapping) res       : opción muy avanzada que se verá más tarde
 * - (string)  n_ataque  : otra opción avanzada
 */
string *dame_mensaje_combate_impacto(
    int pupa, string tipo_danyo, string local, object at, object def,
        mapping res, string n_ataque
    )
{
    // Vamos a crear un verbo y tres mensajes (jugador, defensor y sala)
    string verbo, msj_at, msj_def, msj_room;

    // Elegimos un verbo al azar entre unos cuantos
    verbo    = element_of(({"atraviesa", "desgarra", "horada", "hiere"}));
    
    // Inicializamos algunos mensajes con los nombres de los que dan / reciben
    msj_at   = def->query_short();
    msj_def  = at->query_short();

    // Preparamos el principio de los textos:
    
    // Sala
    msj_room = msj_def + " " + verbo + " a " + msj_at + " en " + local + " con su " + query_short();
    
    // Atacante
    msj_at   = capitalize(verbo)+"s a " + msj_at + " en " + local + " con tu " + query_short();
    
    // Defensor
    msj_def  = msj_def + " te " + verbo + " en " + local + " con su " + query_short();

    // Ahora vamos a preparar el "final" de los mensajes. 
    // Ésto cambiará respecto al daño
    switch(pupa) {
        case 1..80:
            msj_room += " dejando un rastro de luz en el aire antes de impactar.";
            msj_at   += " dejando un rastro de luz en el aire antes de impactarle.";
            msj_def  += " dejando un rastro de luz en el aire antes de impactarte.";
            break;

        case 81..300:
            msj_room += " dejando el aire cargado de chispas mágicas.";
            msj_at   += " dejando el aire cargado de chispas mágicas.";
            msj_def  += " dejando el aire cargado de chispas mágicas.";
            break;

        default:
            msj_room += " dejando un %^BOLD%^YELLOW%^cegador%^RESET%^ rastro de luz solar en el aire.";
            msj_at   += " dejando un %^BOLD%^YELLOW%^cegador%^RESET%^ rastro de luz solar en el aire.";
            msj_def  += " dejando un %^BOLD%^YELLOW%^cegador%^RESET%^ rastro de luz solar en el aire.";
            break;
    }

    // Ya tenemos los mensajes, los devolvemos en un array de 3 posiciones.
    return ({
        msj_at,     // Mensaje al atacante
        msj_def,    // Mensaje al defensor
        msj_room    // Mensaje a la room
    });
}
/**
 * Esta función es igual que la anterior, pero nos permite personalizar los golpes críticos.
 *
 * Fijaros que los parámetros son iguales.
 */
string *dame_mensaje_combate_critico(
    int pupa, string tipo_danyo, string loc, object at, object def,
        mapping res, string n_ataque
    )
{
    return ({
        "Empalas críticamente a " + def->query_cap_name() + " en " + loc + ", haciendo "
            "que puedas ver el radiante brillo de tu lanza al mirar en su interior "
            "cuando grita agónicamente.",
            
        at->query_cap_name() + " te empala críticamente con su " + query_short() + ", "
            "haciendo que aulles de dolor y que tus ojos se envuelvan en un "
            "brillo cegador.",
            
        at->query_cap_name() + " empala críticamente a " + def->query_cap_name() + ", "
            "haciendo que " + def->dame_este() + " aulle de dolor y que de sus ojos "
            "escapen brillantes rayos de luz."
    });
}
