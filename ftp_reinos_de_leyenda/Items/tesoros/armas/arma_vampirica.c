// Kaitaka jun 2011, QC por Dunkelheit el 04-06-2011
/*
    Satyr 2014 --
        - De +21 a +30
        - De 6d15 de vampirismo a 18
        - Pierde el +5 divino
        - Descripcion_juzgar
        - El vampirismo pasa de weapon_damage a danyo_especial (no quita
   imagenes asi)
*/
// Satyr 25.11.2015 -- Balance, +5 enchant, quito malus
inherit "/obj/arma";

#define DADOS 10
#define CARAS 18
#define PENALIZADOR 30
#define BLOQUEO 12

void setup()
{
    fijar_arma_base("cimitarra");
    set_name("cimitarra");
    set_short("Cimitarra %^BOLD%^RED%^V%^RESET%^ampírica");
    set_main_plural("Cimitarras %^BOLD%^RED%^V%^RESET%^ampíricas");
    generar_alias_y_plurales();
    set_long(
        "Tienes entre tus manos una de las armas legendarias con las que el "
        "ejercito de Drakull intentó arrasar los reinos hace muchos años. Lo "
        "que mas destaca "
        "es la empuñadura compuesta por un mango negro de hierro entrelazado y "
        "en cuyo "
        "extremo hay un murciélago con las alas abiertas. La hoja parece hosca "
        "y poco "
        "afilada, ya que el tiempo la ha castigado.\n");
    fijar_genero(2);
    fijar_encantamiento(30);
    add_static_property("ts-artefacto", 2);
    nuevo_efecto_basico("vampirismo", 15);
    add_static_property("nivel_minimo", 20);
}
