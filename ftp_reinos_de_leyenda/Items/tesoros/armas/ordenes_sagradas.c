// Satyr 15/05/201
inherit "/obj/magia/reliquia";

void setup()
{
    set_name("reliquia");
    set_short(
        "%^BOLD%^RED%^Ó%^WHITE%^rdenes %^RED%^S%^WHITE%^agradas%^RESET%^");
    set_short(
        "%^BOLD%^RED%^Ó%^WHITE%^rdenes %^RED%^S%^WHITE%^agradas%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Un papiro de varios metros de longitud que presenta la rúbrica lacrada"
        " de la máxima autoridad de los reinos: DIOS. ¿Qué dios?, pues eso "
        "ya es lo de menos. Realmente no es que nadie vaya a pararse a leerlas,"
        " ¿sabes? Son una excusa para que policías, caballeros y soldados usen "
        "la represión teniendo bajo ellos el auspicio del beneplácito de "
        "su dios, permitiéndoles así ejercer la violencia sin que deba pesar "
        "sobre su conciencia, o al menos, ¡eso les dicen sus obispos!\n\n"

        "  No es raro que aquellos que esgrimen estas órdenes entren en un "
        "estado"
        " de celosía, convirtiéndose en espectros que braman cánticos "
        "eclesiásticos "
        "al tiempo que se desplazan por el campo de batalla, arrastrando tras "
        "de sí la estela de este papiro y blandiendo su maza en la otra.\n\n"

        "  Además, su aspecto es imponente. La retahila de texto que contiene "
        "tiene una caligrafía magnífica, si bien es tan pequeña que es "
        "imposible"
        " leer algo que no sean las iniciales de cada frase, las cuales han "
        "sido dibujadas con mucho esmero. Su papel no es vulgar y corriente,"
        " es una lámina de oro tan fina que han tenido que usar las manos "
        "de muchos hechiceros sencillamente para crear un metro.\n\n"

        "  Esta reliquia no deja a nadie indiferente. A unos les inspira el "
        "coraje y lealtad de su dios. A otros les aborrece que las "
        "palabras de un ente inexistente se usen para justificar atrocidades "
        "y genocidios.\n");

    set_read_mess(
        "Aquellos que cuestionan nuestro derecho a exterminar a miles de "
        "personas son incrédulos y blasfemos, siendo necesario que sean "
        "juzgados como tal. La vida de nuestros enemigos no vale nada, "
        "pues %BOLD%^RED%^DIOS LO QUIERE%^RESET%^.",
        "adurn");
    inicializar_reliquia();

    add_static_property("daño", 30);
    fijar_valor(100000);
    fijar_peso(2000);
    fijar_material(8);

    add_static_property("no_clase", ({"druida", "monje", "chaman"}));
}
int dame_BO()
{
    return 50;
}
