// Dunkelheit 02-06-2011
inherit "/obj/arma.c.c";

void setup()
{
    fijar_arma_base("latigo");
    set_name("latigo");
    set_short("Látigo %^RED%^Estigio%^RESET%^");
    add_alias(({"latigo", "estigio"}));
    set_main_plural("Látigos %^RED%^Estigios%^RESET%^");
    add_plural(({"latigos", "estigios"}));
    
	set_long("Es un látigo maldito. Este arma, forjada en las entrañas de la cordillera de Bûrzum, "
	"ha sido bañada en la sangre innoble de los habituales sacrificios goblinoides a Gurthang, lo cual "
	"le ha dotado de un encantamiento más que notable. El único precio que ha de pagar el portador es "
	"el de ser lo suficientemente malvado como para que el arma le acepte como digno dueño.\n");

	fijar_encantamiento(10);
    nuevo_efecto_basico("carnicero", 50);
}

int set_in_use(int i)
{
    if (i && environment(this_object())->dame_alineamiento() < 0) {
        tell_object(environment(this_object()), "Tu moral te impide portar un arma tan innoble y de reputación tan malvada.\n");
        return 0;
    }
    return ::set_in_use(i);
}

