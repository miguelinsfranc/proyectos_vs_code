// Dunkelheit 18-06-2009
// Satyr 16.10.2015
inherit "/obj/arma";
void setup()
{
    fijar_arma_base("luz del alba");
    set_name("absolucion");
    set_short("%^WHITE%^BOLD%^Absolución de los Pecados%^RESET%^");
    add_alias(({"pecados", "absolución"}));
    set_main_plural("%^WHITE%^BOLD%^Absoluciones de los Pecados%^RESET%^");
    add_plural("absoluciones");
    fijar_genero(1);
    
    set_long("Cuando observas esta estrella del alba, a tu mente no llega otra imagen que la de "
    "la redención: el pecador arrodillado ante su dios, esperando recibir la absolución de sus "
    "pecados. Todo el mundo que ha empuñado o padecido esta ligera arma comparte esta visión, y "
    "no se sabe muy bien por qué, aunque siendo así lo más probable es que alguna mano divina "
    "urdiese la estratagema que vio nacer esta estrella en las forjas mortales. Su cabeza es de "
    "acero pulido con seis púas de base cilíndrica y cabeza abierta -para redimir mejor, dicen-. "
    "De la base del mango cuelga una borla de color carmesí, algo deshilachada por el paso de "
    "los años y los numerosos lavados por la sangre que la habrá salpicado.\n");

    add_static_property("messon", "Empuñas el arma con la férrea disposición de redimir los pecados de tus enemigos... a porrazo limpio.\n");

    add_static_property("mal", 3);
    fijar_encantamiento(21);
    ajustar_BP(30);

    nuevo_efecto_basico("alineamiento", -4);
    nuevo_efecto_basico("intolerancia", 20);
}
/*
string descripcion_encantamiento()
{
    return ::descripcion_encantamiento() + "Un encantamiento divino permitirá a quienes reciban los "
    "porrazos redentores de esta estrella encaminarse por el sendero del Bien. Durante lustros ha "
    "sido empleada por los fanáticos del Bien como arma de auto-castigo para exhortar el perdón por "
    "las acciones negativas que hayan podido cometerse.";
}
*/