// Satyr xx.yy.2008 -- Un garrote de la virgen bendita
// Satyr 25.11.2015 -- Revisando
// Astel 27.09.2018 -- El mensaje de ataque de la serpiente solo se aplica al
// ataque tabla (por si se le ayaden otros por efectos de hechizos muestre el
// mensaje por defecto)
// Sierephad Dex 2021	--	limitando que solo se pueda empuñar 1

#include <baseobs_path.h>
#include <tiempo.h>
inherit "/obj/arma.c";
// inherit "/obj/magia/objeto_formulador.c";
void setup()
{
    fijar_arma_base("garrote");
    set_name("garrote");
    add_alias(({"garrote", "xapitalh"}));
    add_plural(({"garrotes", "xapitalh"}));
    set_short("%^ORANGE%^RESET%^Garrote de %^GREEN%^Xapitalh%^RESET%^");
    set_main_plural("%^ORANGE%^RESET%^Garrotes de %^GREEN%^Xapitalh%^RESET%^");

    set_long(" El bastón primitivo que sostienes ante ti tiene una hermosa "
             "serpiente que ha sido tallada "
             "con sumo cuidado y dedicación. Las escamas están representadas a "
             "la perfección y la "
             "cuidadosa disposición de sus pliegues te hace dudar de si esta "
             "creación fue una obra "
             "mortal. Dos ojos de rubí adornan la cabeza de serpiente, que "
             "exhibe dos afilados colmillos "
             "de hueso del tamaño de tu dedo índice. Palabras de poder "
             "talladas recorren el garrote "
             "de cabeza a empuñadura y te hacen dudar de a que clase de animal "
             "representa o idolatra el "
             "garrote. ¿Quizás a un simple animal?, ¿a un dios?, ¿a un poder "
             "arcano?, ¿a algún mal de "
             "eras pasadas ya olvidado? desconoces que cultura creó un bastón "
             "cuyo poder mágico "
             "es palpable pero, sin duda, no es algo con lo que un mortal deba "
             "jugar.\n");
    // set_read_mess("S H I K A K A", "lagarto");

    fijar_vida_max(dame_vida_max() * 2);
    fijar_encantamiento(35);
    // ajustar_BE(21);
    ajustar_BE(6);
    ajustar_BO(12);
    ajustar_BP(12);

    add_static_property("enfermedad", 5);
    add_static_property("veneno", 15);
    add_static_property("ts-veneno", 15);
    add_static_property("ts-mental", 15);

    add_static_property(
        "messon",
        "Tu " + query_short() +
            " cobra vida y una cabeza de serpiente "
            "comienza a revolverse en tu mano.\n");
    add_static_property(
        "messoff",
        "La serpiente que sostenías se petrifica lentamente hasta convertirse "
        "en madera de nuevo.\n");

    // No cambio los dados, hago esto para poner los mensajes
    /*if (b &&  b = b->ojear_datos_armas(dame_arma_base())) {
        remove_attack("tabla");
        add_attack("tabla", "penetrante", b[4], b[5] + 10, b[6] + 25, 0, 0, 1,
    0);
    }*/
    ajustar_danyo_minimo("tabla", 30);
    ajustar_mensajes("tabla", 1);
    /*
    fijar_recuperacion(DIA);
    fijar_esfera(2);
    fijar_nivel(20);
    */
    nuevo_efecto_basico("canalizador", 90);
    nuevo_efecto_basico("descarga", 350);
    fijar_params_efecto("Descarga", "veneno");

    add_static_property("nivel_minimo", 30);

    fijar_conjuntos(({BCONJUNTOS + "panoplia_xapitalh.c"}));
	
	add_static_property("copias",
        "Eres incapaz de equilibrar dos armas tan poderosas a la vez.");

}
/*
void init()
{
    ::init();
    add_action("formular_mordisco_vibora", ({"shikaka"}));
}
int formular_mordisco_vibora(string tx)
{
    return formular("mordisco de la vibora", tx, 1);
}
void realizar_cantico(object controlador, object spell, string cantico)
{
    object sala = environment();

    if (sala && sala = environment(sala))
        tell_room(sala, query_short() + " pronuncia el cántico:
'"+cantico+"'\n");
}
status mensaje_hechizo(string nombre, mixed objetivos)
{
    tell_object(objetivos[0], "Los ojos de tu "+query_short()+" brillan en un
fulgor asesino y esta se abalanza sobre " +objetivos[1]->query_short()+".\n");
    tell_object(objetivos[1], "Los ojos del "+query_short()+" de
"+objetivos[0]->query_short()+" brillan en un fulgor asesino y " "esta se
abalanza sobre ti.\n"); tell_accion(environment(objetivos[1]), "Los ojos del
"+query_short()+" de "+objetivos[0]->query_short()+" brillan en un fulgor "
            "asesino y esta se abalanza sobre
"+objetivos[1]->dame_pronombre()+".\n","Oyes el sisear de una serpiente.\n",
            objetivos, objetivos[1]);
    return 1;
}
void iniciar_formulacion(string nombre_hechizo,object portador)
{
    tell_object(portador, "Echas tu "+query_short()+" hacia adelante para que
muerda a tus enemigos.\n"); tell_accion(environment(portador),
portador->query_short()+" echa su "+query_short()+" hacia adelante para que "
        "ataque a sus enemigos.\n", "Oyes el sisear de una serpiente.\n",
({portador}), portador);
}
*/
string *dame_mensaje_combate_impacto(
    int pupa, string tipo_danyo, string local, object atacante, object defensor,
    mapping res, string n_ataque, object arma)
{
    string verbo  = ({"muerde", "desgarra", "envenena", "fustiga"})[random(4)];
    string msj_at = defensor->query_short(), msj_def = atacante->query_short(),
           msj_room;

    if (n_ataque != "tabla")
        return ::dame_mensaje_combate_impacto(
            pupa, tipo_danyo, local, atacante, defensor, res, n_ataque, arma);

    msj_room = "La serpiente del " + query_short() + " de " + msj_def + " " +
               verbo + " a " + msj_at + " en " + local;
    msj_at = "La serpiente de tu " + query_short() + " " + verbo + " a " +
             msj_at + " en " + local;
    msj_def = "La serpiente del " + query_short() + " de " + msj_def + " te " +
              verbo + " en " + local;

    switch (pupa) {
        case 1..80:
            msj_room += " arrancándole la piel";
            msj_at += " arrancándole la piel";
            msj_def += " arrancándote la piel";
            break;

        case 81..300:
            msj_room += " haciendo que grite de agonía";
            msj_at += " haciendo que grite de agonía";
            msj_def += " haciendo que grites de agonía";
            break;

        default:
            msj_room += " dejando dos enormes agujeros sangrantes en su cuerpo";
            msj_at += " dejando dos enormes agujeros sangrantes en su cuerpo";
            msj_def += " dejando dos enormes agujeros sangrantes en tu cuerpo";
            break;
    }

    return ({msj_at + ".", msj_def + ".", msj_room + "."});
}
