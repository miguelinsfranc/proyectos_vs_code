// Kaitaka jun 2011, QC por Dunkelheit el 04-06-2011
// Satyr 27.11.2014
// Eckol 17Sep15: hotfix fijar material
inherit "/obj/arma.c";
#include <combate.h>
#include <unidades.h>
#define BONIFICADOR 30
#define PORCENTAJE 20
void setup()
{
    string tx;

    fijar_arma_base("puñal");
    set_name("puñal");
    set_short("Puñal %^RED%^BOLD%^V%^RESET%^ampírico");
    set_main_plural("Puñales %^RED%^BOLD%^V%^RESET%^ampíricos");
    generar_alias_y_plurales();

    set_long(
        "Tienes entre tus manos una de las armas legendarias con las que el "
        "ejercito de Drakull intentó arrasar los reinos hace muchos años. Lo "
        "que mas destaca "
        "es la empuñadura compuesta por un mango negro de hierro entrelazado y "
        "en cuyo "
        "extremo hay un murciélago con las alas abiertas. La hoja parece hosca "
        "y poco "
        "afilada, ya que el tiempo la ha castigado.\n");
    fijar_encantamiento(21);
    add_static_property("ts-artefacto", 2);
    add_static_property("divino", 5);
    fijar_material(2);

    tx = nuevo_efecto_basico("vampirismo", __ud(25, "%"), 0, 30);
    fijar_triggers_efecto(tx, ({_TRIGGER_APUNYALAR}));

    add_static_property("nivel_minimo", 20);
}
/*
string descripcion_juzgar(object pj)
{
    return ::descripcion_juzgar(pj) + "\n\n Además, al apuñalar el arma "
        "curará un " + PORCENTAJE + "% de vida del bribón, siempre y cuando "
        "el bloqueo del arma no esté activo y no se apuñale a
muertos-vivientes.\n"
    ;
}
void event_apunyalar(mixed algo, object quien, object objetivo)
{
    int cura;

    if ( ! quien || ! objetivo )
        return;

    if ( query_timed_property("vampirito") || objetivo->dame_raza() ==
"muerto-viviente" ) return;

    cura = to_int(quien->dame_pvs_max() * (PORCENTAJE / 100.0));

    if ( cura > 0 )
        quien->spell_damage(cura, "curacion", quien, TO, TO);
}
*/
