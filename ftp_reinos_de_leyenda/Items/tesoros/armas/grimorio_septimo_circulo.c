// Dunkelheit 01-06-2011
#include <baseobs_path.h>
inherit "/obj/magia/grimorio";

void setup()
{
    set_name("grimorio");
    set_short(
        "Grimorio del %^BLACK%^BOLD%^Séptimo %^RESET%^CYAN%^Círculo%^RESET%^");
    set_main_plural(
        "Grimorios del %^BLACK%^BOLD%^Séptimo %^RESET%^CYAN%^Círculo%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        "El Grimorio del Séptimo Círculo es un vasto incunable de la Era 3ª "
        "que que alberga una de las mayores colecciones de tratados sobre "
        "magia "
        "arcana; desde recetas de hechizos ya obsoletos e imposibles de "
        "formular "
        "hasta listas de entidades mágicas de otros planos, pasando por "
        "recetas de "
        "pociones y ensayos de alquimia. Sus gruesas tapas, agrietadas por el "
        "paso "
        "de los años, dan paso a un mundo de conocimiento que llevarán a su "
        "portador a un nivel superior de sapiencia.\n");

    inicializar_grimorio();

    add_static_property("poder_magico", 90);
    add_static_property("int", 1);

    fijar_valor(120 * 500);

    add_static_property("nivel_minimo", 30);
    fijar_valor(183000);

    fijar_conjuntos(({BCONJUNTOS + "enigma_9_circulos.c"}));
}
