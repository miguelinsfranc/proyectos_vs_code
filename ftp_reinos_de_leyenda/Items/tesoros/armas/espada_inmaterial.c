// Satyr 2012
#define VAINA "/baseobs/tesoros/armaduras/vaina_inmaterial"
inherit "/obj/arma.c";
object invocador;

int dame_arma_de_fase() {return 1; }
void setup()
{
	fijar_arma_base("espada larga");
	set_name("espada");
	add_alias(({"espada","inmaterial","espada inmaterial"}));
	add_plural(({"espadas","inmateriales","espadas inmateriales"}));
	set_short("%^BOLD%^WHITE%^Espada Inmaterial%^RESET%^");
	set_main_plural("%^BOLD%^WHITE%^Espadas Inmateriales%^RESET%^");
	set_long(
		"Un arma de energía que emana la luz más brillante que jamás hayas visto. "
		"Esta espada ha sido construída con los poderes de otro plano de existencia "
		"y tal es su poder, que existe en varias dimensiones a la vez. No deja de "
		"vibrar y brillar, emitiendo un característico zumbido que taladra tus "
		"oídos de forma implacable. El arma existe en varias dimensiones a la vez, "
		"siendo capaz de dañar a criaturas extraplanarias, mágicas, incorpóreas o "
		"que son capaces de teleportarse. Es un arma de dioses y su existencia formula "
		"demasiadas incógnitas.\n"
	);
 
	fijar_encantamiento(60);
	fijar_material(10);
        //Kaitaka - no se pueden vender objetos invocados, pero por si acaso.
        fijar_valor(0);
	set_heart_beat(1);
}
string descripcion_encantamiento()
{
	return "Solo puede ser empuñada por el portador de la vaina. Un hechizo de 'disipar magia: espada inmaterial' hará que la espada se desvanezca.";
}
object dame_invocador() 
{ 
    return invocador; 
}
void fijar_invocador(object ob)
{
	invocador = ob;
}
int desvanecerse(object ob)
{
	if (ob)
	{
		if (ob->query_room())
			tell_room(ob, query_short() + " parpadea varias veces antes de desaparecer de este plano.\n");
		else
			tell_object(
				ob, 
				"Tu " + query_short() + " parpadea varias veces antes de desaparecer de este plano.\n"
			);
		
		if (environment(ob))
			tell_accion(
				environment(ob), "La " + query_short() + " de " + ob->query_cap_name() + " "
				"parpadea varias veces antes de desaparecer de este plano.\n",
				"",
				({ob}),
				ob
		);
		
		if (query_in_use() && environment())
			environment()->unhold_ob(TO);
			
		if (invocador && invocador->dame_efecto("espada inmaterial"))
			invocador->quitar_efecto("espada inmaterial", 1);
			
		destruct(this_object());
	}
	
	return 0;
}
void heart_beat()
{
	if (!environment() || base_name(environment()) != VAINA && environment() != invocador)
	{
		desvanecerse(environment());
	}
}
int rutina_ataque(object defensor,object atacante,int msjs,int bono,int no_esquivar,int flag)
{
	object vaina = secure_present(VAINA, atacante);
	
	if (!vaina || !vaina->query_in_use())
	{
		return desvanecerse(atacante);
	}
	else
	{
		return ::rutina_ataque(defensor, atacante, msjs, bono, no_esquivar, flag);
	}
}
int dame_invocado()   { return 1; }
int query_auto_load() { return 1; }
