// Dunkelheit 01-06-2011
inherit "/obj/arma.c";

#define REGEXP_RAZAS "(gnomo|halfling)"
#define DADO_EXTRA roll(3, 40)

status raza_valida(object palomo)
{
    return regexp(palomo->dame_raza(), REGEXP_RAZAS);
}
void setup()
{
    fijar_arma_base("martillo de guerra");
    set_name("terror");
    set_short("%^RED%^BOLD%^Terror de los Medianos%^RESET%^");
    add_alias("medianos");
    set_main_plural("%^RED%^BOLD%^Terrores de los Medianos%^RESET%^");
    add_plural(({"terrores"}));    
    set_long("Observas este abominable martillo de guerra, de grueso mango metálico y vasta cabeza rectangular de "
    "piedra, sobre la que hay grabado el rostro deformado de una gárgola alada de Vorax. La oscura profundidad de "
    "su garganta se pierde en las entrañas del martillo; cuentan las leyendas que el alma del gárgola corrupta que "
    "habita en este martillo prende en cólera al verse en el campo de batalla frente a sus históricos enemigos "
    "raciales: los gnomos y los halflings.\n");
    
    fijar_encantamiento(15);
    
    ajustar_peso(dame_peso()/3);
    
    nuevo_ataque("acido", "acido", 3, 10, 5, 0, 0, 1);
    nuevo_efecto_basico("matatapones", 50);
}

int set_in_use(int i)
{
    if (i && raza_valida(environment())) {
        tell_object(environment(), "Intentar empuñar el "+query_short()+", pero... ¡la gárgola grabada en la cabeza "
        "del martillo abre sus fauces y te baña en un ácido corrosivo!\n");
        tell_accion(environment(environment()), environment()->query_short()+" intenta empuñar su arma... ¡pero ésta "
        "le responde con un dolorosísimo chorrazo de ácido!\n", "", ({ environment() }), environment());
        environment()->danyo_especial(-DADO_EXTRA, "acido", environment());
        return 0;
    }
    return ::set_in_use(i);
}
void query_message(
    int pupa, string tipo, string localizacion, object atacante,
        object defensor, int crit
)
{
    tell_object(atacante, "%^GREEN%^#%^RESET%^ ¡La gárgola grabada en tu "+query_short()+" abre sus fauces y baña "
        "a "+defensor->query_cap_name()+" con un devastador, humeante y burbujeante chorro de ácido!\n");
    tell_object(defensor, "%^RED%^*%^RESET%^ ¡La gárgola grabada en el arma de "+atacante->query_cap_name()+" "
        //"te reconoce como "+defensor->dame_numeral()+" enemig"+defensor->dame_vocal()+" sempitern"+defensor->dame_vocal()+" y "
        "abre sus fauces para bañarte en un devastador, humeante y burbujeante chorro de ácido!\n");
    tell_accion(environment(defensor), "¡La gárgola grabada en el arma de "+atacante->query_cap_name()+" "
    //"reconoce a "+defensor->query_short()+" como un"+defensor->dame_numeral()+" enemig"+defensor->dame_vocal()+" sempitern"+defensor->dame_vocal()+" y "
    "abre sus fauces para bañar a " + defensor->query_cap_name() + " en un "
    "devastador, humeante y burbujeante chorro "
    "de ácido!\n", "", ({ atacante, defensor }), atacante);
        
}


/*
string descripcion_encantamiento()
{
    return ::descripcion_encantamiento() + "Este martillo es mortíferamente efectivo contra los enemigos raciales "
    "de la gárgola que habita en él: gnomos y halflings.";
}

int rutina_ataque(object defensor, object atacante, int msjs, int bono, int no_esquivar, int flag)
{
    int resultado = ::rutina_ataque(defensor, atacante, msjs, bono, no_esquivar, flag);

    if (resultado > 0 && !msjs && raza_valida(defensor)) {
        tell_object(atacante, "%^GREEN%^#%^RESET%^ ¡La gárgola grabada en tu "+query_short()+" abre sus fauces y baña "
        "a "+defensor->query_cap_name()+" con un devastador, humeante y burbujeante chorro de ácido!\n");
        tell_object(defensor, "%^RED%^*%^RESET%^ ¡La gárgola grabada en el arma de "+atacante->query_cap_name()+" te "
        "reconoce como "+defensor->dame_numeral()+" enemig"+defensor->dame_vocal()+" sempitern"+defensor->dame_vocal()+" "
        "y abre sus fauces para bañarte en un devastador, humeante y burbujeante chorro de ácido!\n");
        tell_accion(environment(defensor), "%^RED%^*%^RESET%^ ¡La gárgola grabada en el arma de "+atacante->query_cap_name()+" "
        "reconoce a "+defensor->query_short()+" como un"+defensor->dame_numeral()+" enemig"+defensor->dame_vocal()+" "
        "sempitern"+defensor->dame_vocal()+" y abre sus fauces para bañarle en un devastador, humeante y burbujeante chorro "
        "de ácido!\n", "", ({ atacante, defensor }), atacante);

        defensor->danyo_especial(-DADO_EXTRA, "acido", atacante);
    }
    
    return resultado;
}


*/