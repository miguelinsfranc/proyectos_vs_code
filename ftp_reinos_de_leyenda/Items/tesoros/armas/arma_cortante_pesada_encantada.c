// Dunkelheit 02-06-2011
inherit "/obj/arma.c";

void setup()
{
    fijar_arma_base("espada bastarda");
    set_name("espada");
    set_short("Espada Bastarda Encantada");
    add_alias(({"encantada", "bastarda"}));
    set_main_plural("Espadas Bastardas Encantadas");
    add_plural(({"espadas", "bastardas", "encantadas"}));
    fijar_genero(2);

    set_long("Anheladas por cualquier aventurero que se precie, las armas encantadas como esta espada bastarda "
    "han sido imbuídas mediante magia arcana con sortilegios que las hacen más rápidas, resistentes "
    "y mortíferas en el campo de batalla. Su valor y utilidad son, por lo tanto, mucho mayores que el de "
    "un arma común. ¡No vayas de aventura sin ellas!\n");

    fijar_encantamiento(3);
}

