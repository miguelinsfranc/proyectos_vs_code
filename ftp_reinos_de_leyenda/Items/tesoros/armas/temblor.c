// Satyr 10.11.2015
inherit "/obj/proyectiles/arma_proyectiles.c";
void setup()
{
    fijar_arma_base("honda");

    set_name("temblor");
    set_short("%^ORANGE%^BOLD%^T%^RESET%^ORANGE%^emblor%^RESET%^");
    set_main_plural("%^ORANGE%^BOLD%^T%^RESET%^ORANGE%^emblores%^RESET%^");

    add_alias(({"honda", "temblor"}));
    add_plural(({"hondas","temblores"}));

    set_long(
        "Una honda hecha de cuero de wyvern encantada con un corazón "
            "de elemental de tierra. Es capaz lanzar certeros "
            "proyectiles a velocidades vertiginosas que al impactar "
            "estallan contra sus víctimas como un meteoro polvoriento "
            "y furibundo.\n"
    );

    fijar_encantamiento(40);
    nuevo_ataque("temblor", "tierra", 1, 10, 5);
	add_static_property("nivel_minimo",20);
}
