// Satyr 10/05/2018
// Sierephad Dec 2021	--	Añado comprobacion para las vetas que
//	especificamente no quieran ser expoliadas

#include <trazabilidad.h>
#include <combate.h>
inherit "/obj/arma.c";
#define BLOQUEO 200
void setup()
{
    string aux;

    fijar_arma_base("martillo a dos manos");
    set_name("almadena");
    set_short("%^BOLD%^GREEN%^Gran Almádena de %^ORANGE%^Burk-Kharod%^RESET%^");
    set_main_plural(
        "%^BOLD%^GREEN%^Grandes Almádenas de %^ORANGE%^Burk-Kharod%^RESET%^");
    generar_alias_y_plurales();
    fijar_genero(2);

    set_long(
        "Los hermanos de piedra, o \"Burk-Kharod\" en Khadum imperial, "
        "eran poderosos constructos que se movían animados por la voluntad "
        "de los geomantes del antiguo reino enano.\n\n"

        "  Gracias a la voluntad de sus dioses -y a la magia innata de la "
        "infraoscuridad- los Burk-Kharod tuvieron un papel fundamental en "
        "la expansión de las colonias, pues eran capaces de cavar "
        "y construir sin requerir el mantenimiento de sus congéneres vivos, "
        "¡y lo hacían de forma impresionante, blandiendo almádenas enormes "
        "capaces de destrozar cualquier roca!\n\n"

        "  Tiempo ha que no se ve un Burk-Kharod, o una colonia enana, pero "
        "algunas de sus almádenas han sobrevivido al olvido y se encuentran "
        "ahora en manos de coleccionistas de la suboscuridad.\n\n"

        "  Una de esas herramientas de minería es la que ahora tienes ante ti. "
        "Una pesada cabeza de obsidiana maciza sujeta a un mango de hierro "
        "ocre de más de un metro de longitud, capaz de demoler hasta las "
        "más olvidadas y frías entrañas de la suboscuridad.\n\n"

        "  Es pesado y difícil de blandir, sin embargo, su capacidad "
        "como herramienta minera no tiene parangón -por lo menos en manos de "
        "un gigante- y su tamaño y magia innata la convierten en un arma "
        "de leyenda para aquellos que puedan blandirla.\n");

    fijar_encantamiento(40);
    add_static_property("armadura-magica", 5);

    // 10 de penetracion
    nuevo_efecto_basico("barrido", 3);

    add_static_property("nivel_minimo", 30);
    fijar_peso(20000);
}
string descripcion_juzgar(object pj)
{
    return "Esta almádena puede usarse para golpear vetas de mineral y obtener "
           "un mineral o joya de su interior sin disminuir el máximo de "
           "minerales "
           "de la misma. "
           "Para conseguirlo será necesario superar una tirada de salvación de "
           "'fuerza bruta'. Este efecto tiene un bloqueo de " +
           pretty_time(BLOQUEO) + " y solo podrá aplicarse una vez por veta.\n";
}
int abrir_veta(mixed b)
{
    string path;
    object que;

    if (dame_bloqueo_combate(TO)) {
        return notify_fail(
            capitalize(dame_tu()) + " " + query_short() +
            " aún no ha recuperado su poder.\n");
    }

    if (TP->dame_bloqueo_combate(TO)) {
        return notify_fail(
            "Hace demasiado poco que usaste el poder de " + TU_S(TO) + ".\n");
    }

    if (b->query_property(base_name())) {
        return notify_fail(
            b->query_short() + " fue expoliad" + b->dame_vocal() + " por otr" +
            dame_vocal() + " " + query_short() + ".\n");
    }

    if (b->dame_no_expoliable()) {
        return notify_fail(
            "No crees poder expoliar nada del interior de " + b->query_short() +
            ".\n");
    }

    TP->accion_violenta();

    if (!TP->dame_ts("fuerza bruta")) {
        TP->nuevo_bloqueo_combate(TO, BLOQUEO / 2);

        tell_accion(
            environment(TP),
            "¡" + TP->query_cap_name() +
                " "
                "usa su " +
                SU_S(TO) + " para golpear con violencia " + b->dame_articulo() +
                " " + b->query_short() +
                ", pero su fuerza es insuficiente "
                "para extraer nada de su interior!\n",
            "Escuchas un tremendo ruido de cantería.\n",
            ({TP}),
            TP);

        return notify_fail(
            "%^MAGENTA%^#%^RESET%^ ¡Golpeas con fuerza a " + b->query_short() +
            " con " + TU_S(TO) +
            ", pero no logras romperla lo suficiente como para extraer "
            "nada de su interior!\n");
    }

    if (!path = b->dame_objeto()) {
        return notify_fail(
            "No parece que " + b->query_short() +
            " tenga nada más en su interior.\n");
    }

    if (!que = clone_object(path)) {
        return notify_fail(
            "Es extraño... pero " + b->query_short() +
            " posee algún tipo "
            "de tesoro en su interior que no pertenece a esta realidad. Quizás"
            " los dioses sepan qué hacer.\n");
    }

    // Movemos y trazamos el tesoro
    TRAZA_GUARDAR_MOVIMIENTO(que, TO, environment(TP));
    que->move(environment(TP));

    // Mensajitos
    tell_object(
        TP,
        sprintf(
            "%s ¡Tu %s azota con furia %s %s que esta ante ti, "
            "destrozando%s parcialmente y derramando %s precios%s %s por el "
            "suelo!\n",
            "%^GREEN%^#%^RESET%^ ",
            query_short(),
            b->dame_articulo(),
            b->query_short(),
            b->dame_lo(),
            que->dame_numeral(),
            que->dame_vocal(),
            que->query_short()));
    tell_accion(
        environment(TP),
        "¡%s mueve %s con furia para golpear %s %s que esta ante ti, "
        "destrozando%s parcialmente y derramando %s precios%s %s por el "
        "suelo!",
        TP->query_cap_name(),
        SU_S(TO),
        b->dame_articulo(),
        b->query_short(),
        b->dame_lo(),
        que->dame_numeral(),
        que->dame_vocal(),
        que->query_short());

    // Bloqueos
    TP->nuevo_bloqueo_combate(TO, BLOQUEO);
    b->add_property(base_name(), 1);
    nuevo_bloqueo_combate(TO, BLOQUEO);
    return 1;
}
void init()
{
    ::init();

    anyadir_comando_equipado(
        "expoliar", "<objeto:no_interactivo:aqui:primero'veta'>", "abrir_veta");
}
