// Dunkelheit 02-06-2011
// Satyr 26.11.2015
inherit "/obj/arma.c";

void setup()
{
    fijar_arma_base("espada corta");
    set_name("espada");
    set_short("Espada Corta %^CYAN%^Cerúlea%^RESET%^");
    add_alias(({"corta", "cerulea"}));
    set_main_plural("Espadas Cortas %^CYAN%^Cerúleas%^RESET%^");    
    add_plural(({"espadas", "cortas", "ceruleas"}));
    fijar_genero(2);

    set_long("Observas cómo esta espada corta ha sido engarzada con un lapislázuli y su diseño ha sido "
    "cuidado con esmero para facilitar su uso, lo que le otorga una mayor probabilidad de impacto en el "
    "combate. Su empuñadura metálica ha sido tintada con el mismo color celeste que la susodicha gema.\n");

    ajustar_BE(5);
    nuevo_efecto_basico("abundancia", __ud(20, "%"));
    fijar_vida(dame_vida_max() * 2);
}
