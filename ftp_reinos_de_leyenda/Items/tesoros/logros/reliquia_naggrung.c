// Sierephad Feb 2022

inherit "/obj/objeto_general.c";

void setup()
{
	set_name("emblema");
	set_short("Emblema de %^BOLD%^YELLOW%^Y%^BLACK%^nys%^NOBOLD%^ORANGE%^Iâ%^RESET%^");
	set_main_plural("Emblemas de %^BOLD%^YELLOW%^Y%^BLACK%^nys%^NOBOLD%^ORANGE%^Iâ%^RESET%^");
	add_alias(({"emblema","Emblema","ynysia","ynysiâ","YnysIâ"}));
	add_plural(({"Emblemas","emblemas"}));
	
	set_long(
        "Un pequeño amuleto tallado con forma ovalada en algun tipo de hueso o garra. En sus "
		"superficie se puede ver como se ha grabado una letra Y con un pequeño circulo entre "
		"sus dos extremos superiores. \n"
	);

    fijar_valor(5000);
	fijar_material(7);	
	fijar_genero(1);
	fijar_peso(1);
	
	add_static_property("frio",	5);
	add_static_property("cruzar_nieve",50);

}

int set_in_use(int i)
{
    
	if ( ! i || ! ENV(TO) )
        return ::set_in_use(i);
    
    if ( 1 != ENV(TO)->tiene_logro("reliquias_naggrung","dedicacion")) {
        tell_object(ENV(TO), 
			"Este objeto necesita tener el logro 'Las reliquias de Naggrung' desbloqueado "
			"por completo para poder usarse.\n");
        return 0;
    }

    return ::set_in_use(i);
}
