//Sierephad Juk 2k16	--	Le añado la comprobacion del logro para poder utilizarlo.
// Sierephad Feb 2k19	--	Fijo material para que pueda ser reparado
// Sierephad Ene 2k20	--	Ajusto bono y nivel minimo

inherit "/baseobs/misc/catalejo.c";

string dame_nombre_material()
{
    return "Estaño y nacar";
}

void setup()
{
        set_name("prismatico");
  		add_alias(({"prismatico", "nacar","prismático"}));
        add_plural(({"Prismaticos de Nacar"}));
        set_short("Prismáticos de %^BOLD%^%^CYAN%^Nácar%^RESET%^");
        set_main_plural("Prismáticos de %^BOLD%^%^CYAN%^Nacar%^RESET%^");
		generar_alias_y_plurales();
        set_long("Son unos prismáticos de estaño recubiertos de un fino nácar casi transparente "
			"que hace que el sol cuando se refleja dé una amplia gama de tonalidades. "
			"Dispone de dos manillas que permiten fijar el enfoque hasta más allá del horizonte, "
			"lo que mejora la navegación y la caza de embarcaciones por el Orthos.\n");

		//Configuracion del Item
		fijar_manos_necesarias(2);	//2 Manos
		fijar_material(7);			//Hueso
		fijar_valor(500*50);		//50 Platinos +-
		fijar_peso(600);			//Muy ligero
		
		//Configuracion de catalejo
		// Este será el mejor catalejo de Eirea...( el maximo hasta ahora era de 3 )
		fijar_bono_otear(5);
		fijar_nivel_minimo(10);
		
        
}

int set_in_use(int i)
{
    
    if ( ! i || ! environment() )
        return ::set_in_use(i);
    
    if ( 1 != environment()->tiene_logro("terror_de_los_mares","dedicacion"))
    {
        tell_object(environment(), "Este objeto necesita que tengas el logro 'Terror de los mares' para poder usarse.\n");
        
        return 0;
    }

    return ::set_in_use(i);
}
