// Satyr 2012
inherit "/obj/armadura";
void setup()
{
	fijar_armadura_base("cinturon");
	set_name("cinturon");
	set_short("%^BOLD%^WHITE%^Cinturón de Ranger de Eirea%^RESET%^");
	set_main_plural("%^BOLD%^WHITE%^Cinturones de Rangers de Eirea%^RESET%^");
	add_alias(({"cinturon","cinturón","cinturon de ranger"}));
	add_plural(({"cinturones","cinturones de ranger"}));
	set_long(
		"Un cinturón de cuero que se cierra con una hebilla de hueso con forma de una letra \"E\". "
		"El cuero es de un blanco impoluto y el hueso ha sido cubierto con una suave capa de color oro. "
		"Estos cinturones son utilizados por un culto de locos que deambula por los reinos impartiendo "
		"su justicia con pomposos actos y entradas teatrales.\n"
	);
	
	add_static_property("ts-agilidad"   ,  3);
	add_static_property("ts-conjuro"    ,  3);
	add_static_property("ts-proteccion" , 10);
	fijar_encantamiento(20);
	
	ajustar_BP(2);
	ajustar_BO(2);
	ajustar_BE(2);
	fijar_material(3);
}
int dame_vida()
{
	fijar_vida(dame_vida_max());
	
	return ::dame_vida();
}
int set_in_use(int i)
{
	if (i && environment() && 1 != environment()->tiene_logro("go_go_eirea_rangers","colaboracion"))
	{
		tell_object(environment(), "Este objeto necesita que tengas el logro 'go_go_eirea_rangers' para poder usarse.\n");
		
		return 0;
	}

	return ::set_in_use(i);
}