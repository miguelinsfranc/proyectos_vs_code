// Satyr 2012
inherit "/obj/arma.c";

void setup()
{
    fijar_arma_base("sarten");
    set_name("sarten");
    add_alias(({"sarten","demoníaca"}));
    add_plural(({"sartenes","demoníacas"}));
    set_short("%^BOLD%^RED%^Sartén Demoníaca%^RESET%^");
    set_main_plural("%^BOLD%^RED%^Sartenes Demoníacas%^RESET%^");
    set_long(
        "Una sartén de Mithril oscurecida por el humo de los oscuros rituales con la que fue creada. "
        "Su mango está hecho a partir de un cuerno de demonio deslucido que gira en espiral sobre si "
        "mismo en un caprichoso patrón. En la parte central de la plancha hay un ojo incrustado que se "
        "mueve frenéticamente de un lado a otro, intentando mirar lo que le rodea. El ojo, irritado e "
        "inflamado tras décadas de cautiverio en este artefacto de cocina, pertenecía a uno de los "
        "Ancestros que fue especialmente duro con Gurthang durante su cautiverio. La criatura, que "
        "disfrutaba devorando la carne del hijo de parís, sufrió su merecido castigo cuando Gurthang "
        "recuperó su fuerza, quedando encerrada en una simple sartén que le haría sufrir, por los "
        "siglos de los siglos, las penurias de ser incinerado y no morir jamás.\n"
    );

    fijar_encantamiento(40);
    remove_attack("tabla");
    add_attack("tabla" , "aplastante", 2, 35, 15, 0, 0, 0, 0);
    add_attack("divino", "divino"    , 1, 48,  5, 0, 0, 0, 0);
}
mixed dame_datos_ataque_manos(int manos)
{
    if (manos == 2) {
        return ({
            "aplastante",
            2,
            35,
            35,
            0,
            0,
            0,
            0
        });
    }
    
    return 0;
}
int set_in_use(int i)
{
    object ar;
    
    if ( ! i || ! environment() )
        return ::set_in_use(i);
    
    if ( 1 != environment()->tiene_logro("guerrero_improvisado","desafios"))
    {
        tell_object(environment(), "Este objeto necesita que tengas el logro 'guerrero improvisado' para poder usarse.\n");
        
        return 0;
    }
    
    if ( ! ar = environment()->dame_arma_principal_empunyada() )
        return ::set_in_use(i);

    if ( base_name(ar) == base_name() )
    {
        tell_object(environment(), "Tus conocimientos de gastronomía infernal no son suficientes "
            "para manejarte bien con dos " + query_main_plural() + ".\n");

        return 0;
    }

    return ::set_in_use(i);
}