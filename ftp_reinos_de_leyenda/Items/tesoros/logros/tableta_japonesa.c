// Satyr 19.05.2015 -- Estrenando utf8
// Eckol 21May15 -- Pongo el short "normal", que casi nadie podía verlo.
inherit "/obj/objeto_general.c";
void setup()
{
	set_name("tableta");
	add_plural("tabletas");
	//set_short("卞丹日しヨ卞丹　亡回几　亡丹尺Á亡卞ヨ尺ヨ己　尺丹尺回己");
	//set_main_plural("卞丹日しヨ卞丹己　亡回几　亡丹尺Á亡卞ヨ尺ヨ己　尺丹尺回己");
	set_short("Tableta con carácteres raros");
	set_main_plural("Tabletas con carácteres raros");
	set_long(
        "凵几　亡回しし丹尺　尸丹尺丹　亡回几冊ヨ冊回尺丹尺　ヨし　尸丹己回　句ヨ　し丹　冊凵句し工日　丹　凵卞乍８．"
        "ヨ己　凵几丹　亡回尸工丹　句ヨ　し丹　卞丹日しヨ卞丹　句ヨ　片工冊工工し　と　己回し回　レ丹しヨ　尸丹尺丹　冊回し丹尺．\n"
	);
	
	add_static_property("ts-conjuro", 8);
    fijar_valor(5000);
	fijar_material(6);	
	fijar_volumen(2);
	fijar_genero(2);
	fijar_peso(1);

    reset_drop();
}