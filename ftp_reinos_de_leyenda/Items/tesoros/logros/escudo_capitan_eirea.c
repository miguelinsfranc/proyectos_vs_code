// Satyr 2012
inherit "/obj/escudo.c";
int dame_legal_universal() { return 1; }
void setup()
{
    fijar_escudo_base("escudo");
    set_name("escudo");
    set_short("%^BOLD%^WHITE%^Escudo del %^BOLD%^RED%^Capitán%^CYAN%^ Eirea%^RESET%^");
    set_main_plural("%^BOLD%^WHITE%^Escudos del %^BOLD%^RED%^Capitán%^CYAN%^ Eirea%^RESET%^");
    add_alias(({"escudo", "escudo del capitan", "escudo del capitán", "escudo del capitan eirea"}));
    add_plural(({"escudos", "escudos del capitan", "escudos del capitán", "escudos del capitán eirea"}));
    set_long(
        "Un escudo perfectamente circular decorado con una estrella de cinco puntas sobre fondo rojo "
        "que está situada en su parte central. Círculos concéntricos de color rojo y azul rodean este "
        "motivo, dando lugar a un escudo muy colorido. Está fabricado a partir de un mineral único que no "
        "se da en este planeta y que llegó al mundo en una época muy lejana y fue forjado para el capitán Eirea, "
        "un heroe que feneció hace décadas defendiendo la paz, la integridad y la honradez de los reinos. "
        "Su hazaña fue en vano, puesto que Eirea está sumida en una guerra eterna de la que ningún héroe "
        "puede salvarla.\n"
    );
    
    fijar_encantamiento(20);
    
    add_static_property("ts-aliento"    ,  5);
    add_static_property("ts-horror"     ,  5);
    add_static_property("ts-agilidad"   ,  5);
    add_static_property("ts-conjuro"    ,  5);
    add_static_property("ts-proteccion" , 10);

    add_static_property("fuego"  , 5);
    add_static_property("acido"  , 5);
    add_static_property("frio"   , 5);
    add_static_property("magico" , 5);
    
    ajustar_BO(-10);
}
int dame_vida()
{
    fijar_vida(dame_vida_max());
    
    return ::dame_vida();
}
int set_in_use(int i)
{
    if (i && environment() && 1 != environment()->tiene_logro("avengers,_assemble","colaboracion"))
    {
        tell_object(environment(), "Este objeto necesita que tengas el logro 'avengers_assemble' para poder usarse.\n");
        
        return 0;
    }

    return ::set_in_use(i);
}