// Satyr 2012
// Eckol Nov16: comando dejar
inherit "/obj/mascota.c";

void setup()
{
	string *lenguas = ({"adurn","dendrita","drow","dwerg","eldorense",
				"elfico","gnomo","infraoscura","khadum","kobold","lagarto",
				"negra","ogro","natural"});
	mapping map = ([]);
	
    set_name("cachorrito");
    set_short("Cachorrito");
    set_main_plural("Cachorritos");
    set_long(
        "Un cachorrito de mestizo diminuto que te mira con ojos de pena. "
        "Su cuerpo es mayormente blanco, pero algunas manchas marrones aparecen en "
        "su linda barriga y sus ojos saltones. Le encanta correr en la hierba y "
        "olerte los pies. Su nariz húmeda siempre te deja marcas en la mejilla cuando "
        "salta sobre ti para lamerte.\n"
    );
    
    fijar_raza("canino");
    seguir_incondicional();
	
	foreach(string perfil in lenguas)
		map[perfil]= 100;

	fijar_lenguajes(map);
	
	fijar_lenguaje_actual("natural");
	
}
string *dame_comandos_permitidos()
{
    return ::dame_comandos_permitidos() - ({"proteger", "desproteger", "matar"}) + ({"cavar","dejar"});
}
