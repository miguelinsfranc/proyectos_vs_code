// Satyr 2012
// Isham 08/07/2013 - Hago que el anillo pueda dejarse, pero que tan solo lo pueda vestir el dueño del mismo.

inherit "/obj/armadura";
void actualizar_short();
void setup()
{
    fijar_armadura_base("anillo");
    set_name("anillo");

    call_out("actualizar_short",0);

    add_static_property("ts-enfermedad", 7);
    add_static_property("ts-muerte"    , 7);
    add_static_property("ts-enfermedad", 3);
    add_static_property("ts-proteccion", 3);
    add_static_property("ts-agilidad"  , 3);
    add_static_property("divino"       , 3);
    fijar_material(5);
}
string dame_duenyo() 
{
    return query_property("casado"); 
}
void fijar_duenyo(string t)
{
    add_property("casado", t);
}
void actualizar_short()
{
    set_short("%^BOLD%^YELLOW%^Anillo de compromiso%^RESET%^");
    set_main_plural("%^BOLD%^YELLOW%^Anillos de compromiso%^RESET%^");
    set_long(
      "Un anillo de plata batida incrustado con varias piedras preciosas. "
      "Fraguado por hábiles artesanos de Anduar, estos anillos son utilizados "
      "a nivel mundial por todas las parejas que se casan.\n"
    );
    add_alias(({"anillo","compromiso","anillo de compromiso"}));
    add_plural(({"anillos","compromiso","anillos de compromiso"}));
    if (dame_duenyo())
    {
        set_short("%^BOLD%^YELLOW%^Anillo de compromiso de " + capitalize(dame_duenyo()) + "%^RESET%^");
        set_read_mess("Con cariño de tu cuchi-cuchi.","",0);
    }

}
void setup_tesoro_logro(object p)
{
    fijar_duenyo(p->query_name());
    actualizar_short();
}

int set_in_use(int i)
{

	if(i) // si el jugador intenta equiparse el objeto.
    	{
        	if(environment()->query_name() != TO->dame_duenyo()) // Si quien intenta equiparse el objeto es alguien distinto al propietario del mismo.
        	{
            		tell_object(environment(), "¿Para qué ponerte un anillo de compromiso que no te pertenece?\n");
			return ::set_in_use(0);
        	}
    	}
	else return ::set_in_use(0);

return ::set_in_use(i);
}
