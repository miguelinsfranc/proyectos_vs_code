// Satyr 2012
inherit "/obj/armadura";
void setup()
{
	fijar_armadura_base("sombrero");
	set_name("sombrero");
	set_short("%^ORANGE%^Sombrero de Arqueólogo%^RESET%^");
	set_main_plural("%^ORANGE%^Sombreros de Arqueólogo%^RESET%^");
	add_alias(({"sombrero","sombrero de arqueologo"}));
	add_plural(({"sombreros","sombreros de arqueologos"}));
	set_long(
		"Un sombrero de ala ancha hecho de cuero marrón. Una pequeña cinta negra "
		"coronada hebilla dorada rodea toda su ala. Años de exposición al polvo y "
		"arena han hecho que la pieza amarillee en algunas partes.\n"
	);
	
	add_static_property("ts-agilidad", 7);
	add_static_property("ts-muerte"  , 7);
	fijar_encantamiento(20);
	
	ajustar_BP(5);
	fijar_material(3);
}

int set_in_use(int i)
{
    
    if ( ! i || ! environment() )
        return ::set_in_use(i);
    
    if ( 1 != environment()->tiene_logro("de_oficio_arqueologo","desafios"))
    {
        tell_object(environment(), "Este objeto necesita que tengas el logro 'De oficio: arqueólogo' para poder usarse.\n");
        
        return 0;
    }

    return ::set_in_use(i);
}
