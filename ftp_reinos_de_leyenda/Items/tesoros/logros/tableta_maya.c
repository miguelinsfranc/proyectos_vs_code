// Satyr 2012
inherit "/obj/objeto_general.c";
void setup()
{
	set_name("tableta");
	add_plural("tabletas");
	set_short("%^BOLD%^BLACK%^Tableta de %^RED%^BOLD%^Kíimil%^RESET%^");
	set_main_plural("%^BOLD%^BLACK%^Tableta de %^RED%^BOLD%^Kíimil%^RESET%^");
	
	set_long(
        "Una tableta de piedra caliza que ha sido tallada para representar una extraña "
		"cara de enormes dientes redondeados. Sus ojos inmensos observan al frente y una "
		"enorme lengua emerge de la boca del monstruo representado, siendo esta tan larga "
		"que el apéndice rompe la forma cuadrangular de la piedra. Esta tableta se ha "
		"entregado a los que sobrevivieron a la profecía maya.\n"
	);
	
	add_static_property("ts-muerte", 8);

    fijar_valor(5000);
	fijar_material(6);	
	fijar_volumen(2);
	fijar_genero(2);
	fijar_peso(1);

    reset_drop();
}
/*
FIXME
int set_in_use(int i)
{
    int p = ::set_in_use(i);
    
    if (i && p)
        environment()->adjust_static_property("bono_objetos", BONO);
    else if (!i && !p)
        environment()->adjust_static_property("bono_objetos", -BONO);
    
 
    return p;
}
*/