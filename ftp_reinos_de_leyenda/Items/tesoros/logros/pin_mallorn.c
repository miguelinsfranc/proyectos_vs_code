// Satyr 2012, nostalgia attack
/*
	Zoilder 18/05/2014: Hasta revisar lo de poder coger 3 objetos más, quito en texto de la descripción.
	Eckol 19Feb15: No ocupa inventario. No son tres huecos extra, peeeeeeeero... es lo que hay por ahora.
*/
#define BONO 3
inherit "/obj/objeto_general.c";

int no_ocupa_inventario(){ return 1; }

void setup()
{
	set_name("pin");
	add_plural("pin");
	set_short("%^BOLD%^YELLOW%^Pin %^WHITE%^'%^GREEN%^CURSIVA%^Yo escalé el Mallorn%^WHITE%^CURSIVA%^'%^RESET%^");
	set_main_plural("%^BOLD%^YELLOW%^Pins %^WHITE%^'%^GREEN%^CURSIVA%^Yo escalé el Mallorn%^WHITE%^CURSIVA%^'%^RESET%^");
	
	set_long(
        "Un pin con forma de hoja dorada sobre la que descansa una esmeralda verde de corte antiguo. "
        "Este pin es entregado a todos aquellos jugadores que han escalado el Mallorn en Reinos de Leyenda 2. "
        "El mallorn fue el principio de la aventura de Reinos de Leyenda para muchos jugadores e inmortales "
        "y, rodeado de la compañía adecuada, es el fruto de grandes conversaciones y reuniones sociales. "
        "Este pin conmemora todo aquello que nos hace mejores jugadores y personas gracias al Mallorn, a su "
        "newbie y a Reinos de Leyenda (1 y 2).\n¡Ah!, ¡y te deja cargar con 3 objetos más! "
		"¡Y además no ocupa espacio!\n"
	);

    fijar_valor(5000);
	fijar_material(1);	
	fijar_volumen(2);
	fijar_genero(1);
	fijar_peso(1);

    reset_drop();
}

/*
FIXME
int set_in_use(int i)
{
    int p = ::set_in_use(i);
    
    if (i && p)
        environment()->adjust_static_property("bono_objetos", BONO);
    else if (!i && !p)
        environment()->adjust_static_property("bono_objetos", -BONO);
    
 
    return p;
}
*/
