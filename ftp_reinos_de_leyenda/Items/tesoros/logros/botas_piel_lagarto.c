// Satyr 2012
inherit "/obj/armadura";
void setup()
{
	fijar_armadura_base("botas");
	set_name("botas");
	set_short("%^ORANGE%^YELLOW%^Botas de lagarto del Desierto%^RESET%^");
	set_main_plural("%^ORANGE%^YELLOW%^Botas de lagarto del Desierto%^RESET%^");
	add_alias(({"botas","botas de lagarto","botas de lagarto del desierto"}));
	add_plural(({"botas","botas de lagarto del desierto"}));
	set_long(
		"Un par de botas llenas de polvo y arena que están hechas de diminutas escamas de "
		"lagarto del desierto. Su tacto es muy suave y la naturaleza de las criaturas que "
		"forman su cuero las hace completamente impermeables. Posee algunas escamas de "
		"lagartos ígneos gigantes, lo que le confiere la resistencia natural de la piel "
		"de estos poderosos reptiles.\n"
	);
	
	fijar_encantamiento(10);
	add_static_property("ts-aliento", 10);
	add_static_property("fuego"     ,  5);
	add_static_property("frio"      , -5);
	fijar_material(3);
}