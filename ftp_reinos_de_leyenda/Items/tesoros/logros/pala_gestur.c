//Eckol May17.
//Sierephad Dic 2k18	--	Typos
//Sierephad Ene 2k19	--	Añado el alias y plurales

inherit "/obj/arma";

int no_catalogar_armeria(){ return 1; }
int dame_no_dejar_restos(){ return 1; }

int dame_legal_universal() { return 1; }//Para que cualquiera pueda empuñarla, Rutseg 4-III-2005

void setup()
{
   fijar_arma_base("pala");
   set_name("pala");
   set_short("Pala %^BOLD%^WHITE%^divina%^RESET%^");
   set_main_plural("Palas %^BOLD%^WHITE%^divinas%^RESET%^");
   generar_alias_y_plurales();
   
   set_long("A primera vista, se trata de una pala normal y corriente. "
			"Al observarla con atención, puedes apreciar que a pesar de ser totalmente sólida, "
			"su aspecto es etéreo, como si el metal y la madera que observas fueran solo una fachada "
			"que esconde un poderoso objeto creado por un dios del pasado.\n");

   fijar_manos_necesarias(2);
   fijar_genero(2);
}

string descripcion_juzgar(object o){
	return "Permite cavar sin dejar rastros: ni hoyos, ni montones de tierra.\n";
}
