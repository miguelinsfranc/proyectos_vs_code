// Satyr 2012
inherit "/obj/arma.c";
void setup()
{
	fijar_arma_base("latigo");
	set_name("latigo");
	add_alias(({"latigo","látigo","latigo de arqueologo"}));
	add_plural(({"latigos","látigos","latigos de arqueologo"}));
	set_short("%^ORANGE%^Látigo de Arqueólogo%^RESET%^");
	set_main_plural("%^ORANGE%^Látigos de Arqueólogo%^RESET%^");
	set_long(
		"Un látigo de cuero marrón trenzado con mucho esmero y cuidado con mimo para que su "
		"superficie esté siempre crujiente y brillante. Su mango está recubierto de marfil "
		"labrado en forma de dos elefantes que se dan la espalda y que poseen pequeñas "
		"ondulaciones para que el agarre sea mas efectivo. Es un látigo muy rápido y "
		"manejable, lo que lo convierte en un excelente arma para mantener alejados los "
		"ataques de armas más cortas.\n"
	 );

	fijar_encantamiento(30);
	ajustar_BO(25);
	ajustar_BE(15);
	ajustar_BP(25);
	
	add_static_property("ts-agilidad", 10);
	add_static_property("ts-muerte"  , 10);
}

int set_in_use(int i)
{
    
    if ( ! i || ! environment() )
        return ::set_in_use(i);
    
    if ( 1 != environment()->tiene_logro("de_oficio_arqueologo","desafios"))
    {
        tell_object(environment(), "Este objeto necesita que tengas el logro 'De oficio: arqueólogo' para poder usarse.\n");
        
        return 0;
    }

    return ::set_in_use(i);
}
