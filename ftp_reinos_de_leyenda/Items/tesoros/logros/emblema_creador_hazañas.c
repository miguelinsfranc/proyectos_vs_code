// Satyr 2012
inherit "/obj/objeto_general.c";
void setup()
{
	set_name("emblema");
	add_plural("emblema");
	set_short("Emblema del %^BOLD%^YELLOW%^Creador de %^RED%^Hazañas%^RESET%^");
	set_main_plural("Emblemas del %^BOLD%^YELLOW%^Creador de %^RED%^Hazañas%^RESET%^");
	
	set_long(
        "Este emblema es un símbolo de prestigio entregado por los dioses a determinados "
        "mortales que crearon hazañas memorables con sus aportaciones. El símbolo está hecho "
        "de una fina cadena de cristal de la que cuelga un pequeño zafiro que emite una "
        "figura de luz fantasmagórica que flota sobre él. Dicha figura adquiere la forma de "
        "una cruz de un color amarillo translúcido. Al atravesarla con uno de tus dedos una "
        "electrizante sensación de gozo recorre tu columna.\n"
	);

    fijar_valor(100000);
	fijar_material(10);	
	fijar_volumen(1);
	fijar_genero(2);
	fijar_peso(1);

    reset_drop();
}
int apretar_emblema()
{
    if (!query_in_use())
        return notify_fail("Necesitas usar tu " + query_short() + " para poder utilizar este comando.\n");
    else if (TP->query_timed_property("bloqueo-apretar " + query_name()))
        return notify_fail("Es demasiado pronto para tocar la cruz de nuevo.\n");
    else
    {
        string *aux;
        
        tell_object(TP, "Tocas la fantasmagórica cruz que acompaña a tu " + query_short() + " y, de repente, te sientes mucho mejor.\n");
        tell_accion(environment(TP),
            TP->query_cap_name() + " toca la fantasmagórica cruz que acompaña a su " + query_short() + " y parece sentirse mucho mejor.\n",
            "",
            ({TP}),
            TP
        );
        
        //Baelair 16/01/2014 - El lock era de 10 minutos, lo cambio a 5 horas on
        //TP->add_real_timed_property("bloqueo-apretar " + query_name(), 1, 18000);
        
        // Satyr 25.06.2015 -- Bajo los pjs. Real_timed. 1 hora.
        TP->add_real_timed_property("bloqueo-apretar " + query_name(), 1, 3600);
        TP->spell_damage(300 + random(101), "curacion", TP, TO);
        TP->ajustar_pe(random(TP->dame_nivel_ponderado()/2) + 1);
        
        foreach(string str in ({"heridas", "enfermedades", "venenos", "maldiciones"}))
        {
            if (aux = TP->dame_incapacidades(str))
            {
                TP->quitar_incapacidad(element_of(aux), str);
                break;
            }
        }

        return 1;
    }
}
void init()
{
    ::init();
    
    anyadir_comando(
        "agarrar", 
        "cruz", 
        (:apretar_emblema:), 
        0, 
        1, 
        0, 
        "Proporciona al personaje la determinación de los Creadores de Hazañas.", 
        base_name()
    );
}
