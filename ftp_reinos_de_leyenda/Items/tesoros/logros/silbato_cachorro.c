// Satyr 2012
//Kaitaka 10Sep2013 - Le añado el material.
//Sierephad Juk 2k16	--	Le añado la comprobacion del logro para poder utilizarlo.

#include <logros.h>
inherit "/obj/objeto_general.c";
#define CRIATURA _LOGROS_TESOROS + "silbato_cachorro_ob.c"

int llamar(int);

void setup()
{
    set_name("silbato");
    add_alias(({"silbato de cachorro"}));
    add_plural(({"silbatos","silbatos de cachorro"}));
    set_short("%^RED%^BOLD%^Silbato de cachorro%^RESET%^");
    set_main_plural("%^BOLD%^RED%^Silbatos de cachorro%^RESET%^");
    set_long(
        " Un pequeño silbato de madera que ha sido pintado de rojo con dios sabe que sustancia. "
        "Tiene pequeños huesos grabados y sirve para %^BOLD%^YELLOW%^llamar%^RESET%^ y ordenar "
        "a tu cachorro.\n"
    );
    
    fijar_genero(1);
    fijar_peso(100);
    fijar_valor(50000);
   fijar_material(1);
}

int set_in_use(int i)
{
    
    if ( ! i || ! environment() )
        return ::set_in_use(i);
    
    if ( 1 != environment()->tiene_logro("canis_no_tan_vulgaris","desafios"))
    {
        tell_object(environment(), "Este objeto necesita que tengas el logro 'Canis no tan vulgaris' para poder usarse.\n");
        
        return 0;
    }

    return ::set_in_use(i);
}




void init()
{
    ::init();
    anyadir_comando_equipado("soplar","silbato","llamar","Usa el silbato para llamar a tu mascota.");
}
int llamar(int i)
{
    object gotcha;
    
    foreach(object ob in children(CRIATURA))
    {
        if (ob->dame_invocador() == this_player())
        {
            gotcha = ob;
            break;
        }
    }

    if ( ! gotcha && dame_bloqueo_combate(TO) )
        return notify_fail("Tu cachorrito está durmiendo, dale tiempo a descansar.\n");

    if (gotcha && environment(gotcha) == environment(this_player()))
        return notify_fail(gotcha->query_short() + " ya está contigo, dando saltitos y haciendo el tonto.\n");
    else
    {
        if (gotcha)
            tell_accion(environment(gotcha), gotcha->query_short() + " sale corriendo cuando oye el silbato de su queridísimo dueño.\n", "Oyes un silbato y una criatura corriendo a toda velocidad.\n", ({gotcha}), gotcha);
        
        if (!gotcha)
        {
            if (gotcha = clone_object(CRIATURA)) {
                gotcha->fijar_invocador(this_player());
                nuevo_bloqueo_combate(TO, 200);
            }
            else
                return notify_fail("Tocas tu " + query_short() + ", pero no pasa nada.\n");
        }
        
        gotcha->fijar_genero(this_player()->dame_genero());
        gotcha->set_short(gotcha->query_short() + " de " + this_player()->query_cap_name());
        
        tell_object(
            this_player(), 
            "Haces sonar tu " + query_short() + " e inmediatamente " + gotcha->query_short() + " llega corriendo a la sala, content" + gotcha->dame_vocal() + " y "
            "excitad" + gotcha->dame_vocal() + " por verte de nuevo.\n"
        );
        
        tell_accion(
            environment(this_player()), 
            this_player()->query_cap_name() + " hace sonar su " + query_short() + " y " + gotcha->query_short() + " aparece corriendo para saltar sobre " 
            "" + this_player()->dame_pronombre() + " y hacer que sucumba a ante sus húmedos y lindos lametones.\n",
            "Oyes un silbato sonar.\n",
            ({this_player()}),
            this_player()
        );
        
        gotcha->move(environment(this_player()));
        return 1;
    }
}
