// Satyr 2012
inherit "/obj/armadura";
void setup()
{
	fijar_armadura_base("tunica");
	set_name("tunica");
	set_short("%^BOLD%^BLACK%^Túnica del Mago Negro%^RESET%^");
	set_main_plural("%^BOLD%^BLACK%^Túnicas del Mago Negro%^RESET%^");
	add_alias(({"túnica","mago negro", "tunica del mago negro", "túnica del mago negro"}));
	add_plural(({"tunicas","túnicas", "tunicas del mago negro", "túnicas del mago negro"}));
	set_long(
		"Una túnica de seda negra forrada con una piel parecida a la lana, pero mucho "
		"más fina y elegante. Su simple tacto te eriza los pelos debido a la extremada "
		"suavidad de su tela y a una extraña sensación de gozo y placer que recorre "
		"tus dedos cuando pequeñas descargas de magia oscura acarician tu epidermis. "
		"La túnica tiene 8 botones de marfil con los que se cierra y un cuello que "
		"es tan alto que puede tapar parte del rostro de su propietario cuando se cierra "
		"con unos gemelos de rubíes. Runas de magia oscura y perversa protegen "
		"la prenda de que las almas puras puedan tocarla.\n"
	);
	
	add_static_property("clase", "Hechicero");
	add_static_property("bando", ({"malo", "mercenario", "anarquico", "neutral", "renegado"}));

	fijar_encantamiento(20);
	ajustar_BE(4);
	
	add_static_property("ts-artefacto"   , 10);
	add_static_property("ts-mental"      ,  7);
	add_static_property("ts-paralizacion",  5);
	add_static_property("ts-conjuro"     ,  5);
	add_static_property("ts-fuerza bruta", -5);
	
	
	add_static_property("bien"      , 10);
	add_static_property("electrico" ,  5);
	add_static_property("agua"      ,  5);
	add_static_property("tierra"    ,  5);
}
