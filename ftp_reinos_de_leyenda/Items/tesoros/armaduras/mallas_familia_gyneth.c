// Armadura nivel 10
// Satyr 12.12.2014 -- Bajo el valor
inherit "/obj/armadura";

string dame_nombre_material() { return "Diminutos aros de cristal tratado encantado."; }

void setup()
{
	fijar_armadura_base("mallas elfa");
	set_name("cota");
	set_short("%^BOLD%^CYAN%^Cota de la Familia Gyneth%^RESET%^");
	set_main_plural("%^BOLD%^CYAN%^Cotas de la Familia Gyneth%^RESET%^");
	add_alias(({"gyneth",  "cota de la familia gyneth"}));
	add_plural(({"cotas", "gyneth", "cotas de la familia gyneth"}));
	set_long("Se trata de una de las ya casi extintas cotas de mallas forjadas por y para la "
	"familia Gyneth, un linaje élfico de la Era 2ª que amasó una gran fortuna durante cientos "
	"de años, lo suficiente como para permitirse el lujo de emitir una remesa lo suficientemente "
	"rica para abastecer a las generaciones venideras. La naturaleza del cristal empleado en esta "
	"cota la permitió sobrevivir a los estragos del Cataclismo, aunque su secreto ha quedado -por "
	"desgracia- en el olvido, y muy difícilmente alguien volverá a ser capaz de forjar estas "
	"magníficas armaduras. La cota está formada por minúsculos aros de cristal, tan pequeños que "
	"parecen meros hiros enhebrados los unos contra los otros. ¡No se te ocurre cómo es posible "
	"tejer una malla a partir de elementos tan pequeños! La elevada densidad de estos aros da a "
	"la armadura un color gris mate, color que se aclara al extenderse y se oscurece al apretarse. "
	"El corte de la cota es clásico; un cuello en forma de uve, protección aceptable en los hombros "
	"y caída larga, hasta muy por debajo de la cintura, sobre la que debe colocarse un cinturón.\n");

	add_static_property("agua", 3);
	add_static_property("frio", 3);

	add_static_property("messon",  "Deslizas tu torso con el mayor de los respetos en esta antiquísima pieza, de incalculable valor.\n");

	// Cristal superflipao irrompible y todo eso
	fijar_material(5);
	fijar_encantamiento(16);
    add_static_property("ts-conjuro", 5);
	fijar_proteccion(dame_proteccion() + 4);

	fijar_bono(3, "penetrante");
	fijar_bono(3, "aplastante");
	ajustar_BE(12);

	fijar_peso(8000);
    fijar_valor(8000);
	//fijar_valor(75000); // 2500 platis, Baelair Dic 2013
	add_static_property("nivel_minimo",29);
}
