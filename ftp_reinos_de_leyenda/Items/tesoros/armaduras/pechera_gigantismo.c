/*
	Kiratxia el 12/05/2012
	Pechera del gigantismo
*/

inherit "/obj/armadura";
inherit "/obj/magia/objeto_formulador";


void setup()
{
	fijar_armadura_base("cuero");
	set_name("gigantismo");
	add_alias(({"pechera","gigantismo"}));
	add_plural(({"pecheras","gigantismos"}));
	set_short("Pechera del %^GREEN%^G%^RESET%^i%^GREEN%^g%^RESET%^a%^GREEN%^n%^RESET%^t%^GREEN%^i%^RESET%^s%^GREEN%^m%^RESET%^o");
	set_main_plural("Pecheras del %^GREEN%^G%^RESET%^i%^GREEN%^g%^RESET%^a%^GREEN%^n%^RESET%^t%^GREEN%^i%^RESET%^s%^GREEN%^m%^RESET%^o");
	set_long(
		"Consiste en una pechera de rayas verdes alrededor formando circulos concéntricos en el medio. Parece hecha de una piel de oso"
		" blanco ya que es bastante suave y está caliente, en ocasiones parece vibrar, como si hubiera algo en su interior. En uno de sus costados, parece"
		" tener la forma de la palma de una mano.\n"
	);
	set_read_mess(
		"%^CURSIVA%^groezus%^RESET%^, palabra de los gigantes, fuerza de los hijos de la tierra, poder oculto.",
		"negra",  // De origen goblinoide
		0,
	);
	
	fijar_genero(2);
	fijar_material(3);
	fijar_encantamiento(5);
	fijar_valor(15000);
	fijar_nivel(25);
	fijar_esfera(2);
	
	fijar_recuperacion(120);
}
void init()
{
	::init();
	add_action("apretar", "groezus");
}
int apretar (string str)
{
	// De esta forma los objetos que no están equipados no interfieren con el verbo "groezus"
	if (!query_in_use())
		return 0;
	// Se usa "groezus", sin más, no necesita argumentos
	else if (str)
		return notify_fail("Uso de la palabra de poder: " + query_verb() + ".\n");
	// Aseguramos que el entorno es válido
	else if (!living(environment()) || !environment()->query_player())
		return notify_fail("No puedes utilizar el poder mágico de esta pechera.\n");
	// Todo ok, cedemos control al hechizo...
	else
		return formular("agrandar", environment()->query_name(), 0);			
}
void iniciar_formulacion(string nombre_hechizo,object portador)
{
	tell_object(portador,"Con cuidado colocas tu mano en el costado de tu "+query_short()+", la pechera suelta un gran aullido y notas como empieza a"
	" comprimir todo tu cuerpo haciéndo que aumente de volumen.\n");
	tell_accion(environment(portador),"%^BOLD%^RED%^¡GROAAAAAAAUW!%^RESET%^ La pechera de "+portador->query_short()+" parece tomar vida.\n", 0, ({portador}),portador);
}