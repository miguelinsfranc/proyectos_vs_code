// Satyr 28.07.2016
// Astel 17.11.2018
inherit "/obj/armadura.c";
inherit "/obj/magia/objeto_formulador.c";

#define _PODER ({90, 2, 30})
int formulacion(string hechizo, string obj);

void setup()
{
    fijar_armadura_base("anillo");
    set_name("anillo");
    set_short("%^BLUE%^BOLD%^A%^RESET%^BLUE%^nillo%^RESET%^ "
              "%^BLUE%^BOLD%^A%^RESET%^BLUE%^ntimágico%^RESET%^");
    set_main_plural("%^BLUE%^BOLD%^A%^RESET%^BLUE%^nillos%^RESET%^ "
                    "%^BLUE%^BOLD%^A%^RESET%^BLUE%^ntimágicos%^RESET%^");

    generar_alias_y_plurales();

    set_long(
        "Un anillo negro de obsidiana coronado con una sobria roca magmática. "
        "Tiene toscos surcos en forma de trenza recorren su superficie y "
        "su tacto es áspero, como el de una piedra caliza. Al tocarlo "
        "te parece oir un pequeño zumbido que cesa cuando lo sueltas.\n");

    add_static_property("poder_magico", -20);
    add_static_property("ts-conjuro", 20);
    add_static_property("ts-mental", 10);
    add_static_property("ts-petrificacion", 10);
    add_static_property("magico", 10);
    fijar_encantamiento(30);

    fijar_recuperacion(_PODER[0]);
    fijar_esfera(_PODER[1]);
    fijar_nivel(_PODER[2]);

    ajustar_BE(-dame_BE());

    fijar_mensaje_leer(
        "Creado por Anyexthas para el vicerrey Ghorarux. Que el poder del "
        "anillo "
        "defienda su reinado de los moradores de la superficie y le permita "
        "%^CURSIVA%^contrarrestar%^RESET%^ y %^CURSIVA%^disipar%^RESET%^ sus "
        "hechizos.",
        "gargola",
        0);

    add_static_property("nivel_minimo", 20);
}
string descripcion_juzgar()
{
    return "Capaz de formular los hechizos 'contrahechizo' y 'disipar magia' a "
           "nivel "
           "de esfera " +
           _PODER[1] + " y nivel de hechicero " + _PODER[2] +
           " con bloqueo de " + pretty_time(_PODER[0]) + ".";
}
void init()
{
    ::init();

    anyadir_comando_equipado(
        "contrarrestar",
        "<texto'Objetivo'>",
        (
            : formulacion("contrahechizo", $1)
            :),
        "Formula el hechizo 'contrahechizo' sobre un objetivo.");
    anyadir_comando_equipado(
        "disipar",
        "<texto'Objetivo'>",
        (
            : formulacion("disipar magia", $1)
            :),
        "Formula el hechizo 'disipar magia' sobre un objetivo.");

    fijar_cmd_visibilidad("contrarrestar", "<texto'Objetivo'>", 1);
    fijar_cmd_visibilidad("disipar", "<texto'Objetivo'>", 1);
}
int formulacion(string hechizo, string obj)
{
    return formular(hechizo, obj, 0);
}
void realizar_cantico(object lanzador, object hechizo, string cantico)
{
    lanzador->accion_violenta();
    tell_room(
        environment(lanzador),
        capitalize(dame_articulo()) + " " + query_short() +
            " empieza a brillar con fuertes destellos mágicos.\n");
}
void iniciar_formulacion(string nombre_hechizo, object portador)
{
    tell_object(
        portador,
        capitalize(dame_tu()) + " " + query_short() +
            " comienza a calentarse y chispear en tu mano.\n");
    tell_accion(
        environment(portador),
        capitalize(dame_articulo()) + " " + query_short() + " de " +
            portador->query_cap_name() +
            " comienza a chispear y ponerse al rojo blanco.\n",
        "",
        ({portador}),
        portador);
}
