// Tesoro de nivel 2 -- Dunkelheit 20-05-2011

inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("amuleto");
    set_name("trebol");
    set_short("%^GREEN%^Trébol de Cuatro Hojas%^RESET%^");
    add_alias(({"trébol", "cuatro"}));
    set_main_plural("%^GREEN%^Tréboles de Cuatro Hojas%^RESET%^");
    add_plural(({"treboles", "tréboles"}));
    set_long("Es una perfecta esfera de cristal que contiene un preciado trébol "
    "de cuatro hojas. Según el folclore de prácticamente todas las culturas de "
    "Eirea, el trébol de cuatro hojas otorga a quien lo lleva fortuna y suerte sin "
    "parangón.\n");
    fijar_material(5);
    add_static_property("ts-conjuro", 1);
    add_static_property("ts-veneno", 1);
    add_static_property("ts-paralizacion", 1);
    add_static_property("ts-aliento", 1);
    add_static_property("ts-agilidad", 1);
    add_static_property("ts-muerte", 1);
    add_static_property("ts-horror", 1);
    add_static_property("ts-enfermedad", 1);
    add_static_property("ts-mental", 1);
    add_static_property("ts-petrificacion", 1);
}
