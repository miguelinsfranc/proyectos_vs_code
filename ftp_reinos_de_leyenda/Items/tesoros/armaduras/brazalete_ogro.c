// Vhurkul 17.02.2004
// Brazalete del Ogro

/*
    Tipo: Armadura
    Base: Brazalete
    Nivel: 8
    Efectos:
    - Fuerza + 1
*/

// Dunkelheit 30-01-2008 -- Le meto malus a cosas...
// Zilar 2013 añadiendo alias
// Satyr 24.04.2015 -- Ahora es un brazalete metálico, no brazal, +3 critico

inherit "/obj/armadura";

string dame_nombre_material() { return "Metal encantado mediante sangre de ogro."; }

void setup()
{
    fijar_armadura_base("brazalete de mallas");
    set_name("brazal");
    set_short("%^BLUE%^Brazalete del %^BOLD%^Ogro%^RESET%^");
    set_main_plural("%^BLUE%^Brazaletes del %^BOLD%^Ogro%^RESET%^");
    add_alias(({"ogro", "orgo","brazalete"})); // Por si Elevereth...
    add_plural(({"brazaletes","ogros","orgos"})); // ...volviese a jugar
    set_long("Sin duda uno de los objetos más buscados por los aventureros de los Reinos, y también de los más "
    "antiguos y conocidos, puesto que figuran en los documentos de arqueología más antiguos que se conserven "
    "en Dalaensar. El brazalete del Ogro es una magnífica pieza de armadura pintada con sangre turquesa, ya "
    "que se deben forjar empleando sangre Ogra tratada mágicamente por los más ancianos de estos seres, para "
    "capturar su fuerza natural en la pieza de armadura y que ésta sea transmitida a su portador. Esta reliquia "
    "podía ser encontrada en la 2ª Era en algún lugar de Dendara -ahora conocida como Dendra-. Tiene unos "
    "antiguos garabatos ilegibles de color dorado en la parte más cercana de la muñeca, borrados tal vez "
    "por el paso del tiempo.\n");
    
    // Fuerza +1 implica...
    add_static_property("fue"      ,  1);
    add_static_property("critico"  ,  3);
    add_static_property("ts-mental", -5);
    
    fijar_encantamiento(1);
    fijar_material(2);
    
    fijar_valor(10*500);
	add_static_property("nivel_minimo",29);
}
