// Satyr 14/07/2022 02:10
// https://www.reinosdeleyenda.es/foro/ver-tema/concurso-comunidad-diseno-objetos-de-gloria-temporada-35/
#include <material.h>
#include <baseobs_path.h>
inherit "/obj/armadura";
inherit "/obj/magia/objeto_con_poderes";

#define PODER_INVULNERABILIDAD "susurro invulnerabilidad"
#define PODER_INVULNERABILIDAD_DURACION 180
#define PODER_INVULNERABILIDAD_BLOQUEO 360
#define LISTA_INVULNERABILIDADES ({"ceguera", "dormir", "enmudecer", "sordera"})

void setup()
{
    fijar_armadura_base("pendiente");
    set_name("pendiente");
    set_short(
        "%^MAGENTA%^S%^ORANGE%^usurro %^MAGENTA%^I%^ORANGE%^rascible%^RESET%^");
    set_main_plural("%^MAGENTA%^S%^ORANGE%^usurros "
                    "%^MAGENTA%^I%^ORANGE%^rascibles%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        "Un pendiente hecho de un mineral opalino que ha sido cincelado a "
        "conciencia para darle forma espiral. El acabado "
        "es burdo, irregular y repleto de aristas, cosa que te hace pensar que "
        "el orfebre no era muy experimentado y terminó haciendo una pieza "
        "ovalada cuando quería hacer una circular.\n\n"

        "  No posee ningún tipo de cadena para colgarlo a la oreja; en su "
        "lugar, el portador ha de colocarse la espiral alrededor de uno de sus "
        "lóbulos y procurar no moverse muy bruscamente para que no se suelte. "
        "Esta forma de llevarlo, sin duda, requiere algo de práctica "
        "previa.\n\n"

        "  La piedra tiene un color turbio lo suficientemente transparente "
        "como para permitirte robar un par de miradas a su interior, donde "
        "puedes ver un efecto óptico que te recuerda al crepitar rítmico de "
        "una llama, llama que jurarías que también puedes oír en un sonido "
        "que se materializa como un bisbiseo en tu cogote.\n");

    /*
        aro
        formas confusas en forma de hélice
        gira en torno al lóbulo de la oreja
        bisbisea
        cristal borros, velado, vidrioso
        color opalino
     */

    fijar_material(CRISTAL);
    add_static_property("bp", 10);
    add_static_property("evasion", 5);
    add_static_property("ts-paralizacion", 5);

    fijar_bloqueo_poderes(
        PODER_INVULNERABILIDAD_BLOQUEO, PODER_INVULNERABILIDAD);
}
string descripcion_juzgar(object pj)
{
    return "El poder de " + query_short() +
           " podrá otorgar a su portador una invulnerabilidad a uno "
           "de los siguientes efectos: " +
           nice_list(LISTA_INVULNERABILIDADES, "o") +
           " durante un tiempo que no superará los " +
           pretty_time(PODER_INVULNERABILIDAD_DURACION) +
           ". El poder está sujeto a un bloqueo de " +
           pretty_time(PODER_INVULNERABILIDAD_BLOQUEO) +
           " y no es capaz de eliminar dichas incapacitaciones si ya están "
           "activas en el personaje.";
}
void objeto_desequipado()
{
    if (environment()) {
        environment()->quitar_efecto("susurro irascible");
        environment()->destruir_sombra_susurro_irascible();
    }
}
void fin_invulnerabilidad(object pj, string tipo)
{
    if (!pj) {
        return;
    }

    tell_object(pj, "Tu susurro de invulnerabilidad '" + tipo + "' termina.\n");
    pj->destruir_sombra_susurro_irascible();
    poner_bloqueos(pj, 0, PODER_INVULNERABILIDAD);
}
int do_invulnerabilidad(
    object ejecutor, object victima, string poder, string tipo)
{
    string err;
    object sh;

    if (!ejecutor) {
        return 0;
    }

    if (err = chequeo_previo_finalizar_poder(ejecutor, victima, poder)) {
        tell_object(ejecutor, err + "\n");
        return 0;
    }

    if (!sh = clone_object(base_name() + "_sh.c")) {
        tell_object(ejecutor, "Ha ocurrido un error con " + TU_S(TO) + ".\n");
        return 0;
    }

    mostrar_mensajes_poderes(
        "finalizacion", ejecutor, 0, PODER_INVULNERABILIDAD, tipo);

    sh->setup_sombra(ejecutor, tipo, TO);

    // clang-format off
    ejecutor->nuevo_efecto(
        "susurro irascible",
        PODER_INVULNERABILIDAD_DURACION,
        (:  fin_invulnerabilidad($(ejecutor), $(tipo)) :)
    );
    // clang-format on

    return 1;
}
int activar_invulnerabilidad(string tipo)
{
    if (!chequeo_activacion_poder(TP, TP, PODER_INVULNERABILIDAD)) {
        return 0;
    }

    if (-1 == member_array(tipo, LISTA_INVULNERABILIDADES)) {
        return notify_fail(sprintf(
            "'%s' no es un tipo válido de invulnerabilidad.\n",
            capitalize(tipo)));
    }

    if (TP->dame_sombra_susurro_irascible() ||
        TP->dame_efecto("susurro irascible")) {
        return notify_fail(
            "Ya estás afectad" + TP->dame_vocal() +
            " por un efecto de ese tipo.\n");
    }

    // clang-format off
    call_out(
        "finalizacion_poder",
        2 + (TP->query_hb_counter() % 2) * 2,
        TP,
        TP,
        PODER_INVULNERABILIDAD,
        (: do_invulnerabilidad :),
        0,
        tipo
    );
    // clang-format on

    mostrar_mensajes_poderes("activacion", TP, 0, PODER_INVULNERABILIDAD, tipo);
    return 1;
}
void init()
{
    ::init();

    // clang-format off
    anyadir_comando_equipado(
        "susurro",
        "invulnerabilidad <texto:'Nombre de la invulnerabilidad'>",
        (: activar_invulnerabilidad($1) :),
        "Activa una invulnerabilidad contra "
        + nice_list(LISTA_INVULNERABILIDADES, "o") + "."
    );
    // clang-format on
}
string *dame_mensajes_invulnerabilidad(
    object quien, object victima, string poder, string tipo)
{
    return ({"invocado " + tipo, "", "invocado " + tipo});
}

/**
 * TS:
 *
 * Ceguera (efecto):
 *
 * -     if (def->dame_ts("conjuro", -u->cantidad, "ceguera", "ceguera")) {
 *
 * Ceguera (Incapacidad)
 *
 * Enmudecer (hechizo)
 *
 * - if (objetivo->dame_ts("conjuro", -esfera * 5 +
 * objetivo->dame_nivel_ponderado(), "enmudecer", "enmudecer"))
 *
 * Dormir (hechizo)
 *
 * - ob->dame_ts("conjuro", dame_bono_ts(ob, esfera), "mental", "dormir");
 *
 * Sordera (hechizo)
 *
 * - ????
 *
 */
