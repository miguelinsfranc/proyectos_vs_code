// Satyr 11/05/2018
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("anillo");
    set_name("sello");
    set_short(
        "%^CYAN%^S%^GREEN%^ello de la %^CYAN%^P%^GREEN%^untería%^RESET%^");
    set_main_plural(
        "%^CYAN%^S%^GREEN%^ellos de la %^CYAN%^P%^GREEN%^untería%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Lo más llamativo de este anillo es, sin duda, su parte "
        "plana adornada con una joya azulada ligeramente translúcida. "
        "Su interior, repleto de burbujas, alberga lo que parece "
        "ser la pata de un ave en miniatura. El aro no es nada "
        "destacable: una fina pieza de acero blanco que ha sido "
        "pulida y no presenta ningún adorno. Todo hace que la pieza "
        ", a simple vista, no tenga mucho valor. Sin embargo, "
        "tocar su superficie impregna tus dedos de un agradable "
        "calor que perezosamente se extiende por toda tu mano, "
        "lo que denota que hay magia en su interior.\n");

    fijar_BO(6);
    fijar_valor(30000);
}
