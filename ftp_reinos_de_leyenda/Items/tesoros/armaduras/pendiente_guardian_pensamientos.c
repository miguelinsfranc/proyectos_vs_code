// Astel 09.11.2018
// Satyr 03/06/2019 03:46 PM -- Añadir_comando va en el init() y lo hago solo
// para equipados
inherit "/obj/armadura";
inherit "/obj/magia/objeto_formulador";

#define COOL "%^BOLD%^CYAN%^Pens%^NOBOLD%^ami%^BOLD%^entos%^RESET%^"

void setup()
{
    set_name("guardian de pensamientos");
    set_short("Pendiente Guardián de los " COOL);
    set_main_plural("Pendientes Guardianes de los " COOL);
    set_long("Se trata de un precioso pendiente hecho de cristal de aguamarina "
             "con una cadena de acero que une el enganche de la oreja con una "
             "pequeña bola perfectamente lisa y semitransparente. "
             "En el interior de la esfera parece contener un líquido que emite "
             "pequeños destellos brillantes al ser agitado.\n");
    fijar_armadura_base("pendiente");
    generar_alias_y_plurales();

    fijar_nivel(1);
    fijar_esfera(1);
    fijar_material(5);
    ajustar_BE(3);
}
void init()
{
    ::init();

    anyadir_comando_equipado(
        "agitar", "esfera", "activar", "Activar la magia del pendiente.");
}
int activar()
{
    if (dame_bloqueo_combate(TO))
        return notify_fail("La magia del pendiente esta agotada.\n");

    if (TP->dame_bloqueo_combate(TO))
        return notify_fail(
            "Debes esperar antes de volver a usar el pendiente.\n");

    if (TP->dame_incapacidad("amnesia", "incapacidades")) {
        tell_object(
            TP,
            "Con una sensación de vacío en algún rincón de tu mente, agitas "
            "cuidadosamente el líquido  del interior del pendiente unos "
            "segundos.\n");
        tell_accion(
            ENV(TP),
            TP->query_cap_name() + " agita la esfera de su " + query_short() +
                ".\n",
            "",
            ({TP}),
            TP);
        TP->quitar_incapacidad("amnesia", "incapacidades", TO);
        TP->nuevo_bloqueo_combate(TO, 1200);
        nuevo_bloqueo_combate(TO, 1200);
        return 1;
    }
    tell_object(
        TP,
        "Agitas suavemente la pequeña esfera mientras la observas "
        "detenidamente.\n");
    tell_accion(
        ENV(TP),
        TP->query_cap_name() + " agita la esfera de su " + query_short() +
            ".\n",
        "",
        ({TP}),
        TP);
    if (formular("oclumancia", TP->query_name(), 0)) {
        TP->nuevo_bloqueo_combate(TO, 600);
        nuevo_bloqueo_combate(TO, 600);
        return 1;
    }
    return 0;
}

string descripcion_juzgar(object b)
{
    return "Este objeto permite iniciar un efecto de 'Oclumancia' sobre su "
           "portador o si este estuviera "
           "afectado por una incapacidad de 'amnesia', esta será extirpada.\n";
}
