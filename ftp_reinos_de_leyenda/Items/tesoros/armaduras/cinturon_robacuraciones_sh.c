// Satyr 2012
// Satyr 26.02.2017 -- disipable
#include <spells.h>
object pj, cinturon;
int capturados;
void capturar_hechizo()
{
    capturados++;
    
    if ( pj )
        pj->accion_violenta();
}
int dame_shadow_cinturon_robacuraciones()
{
    	return 1;
}
varargs void destruir_sombra_cinturon_robacuraciones(int forzar)
{
    // Si captura un hechizo consigue 15 segundos extra de duración
    if ( forzar || capturados-- < 1 ) {
        if (pj) {
            tell_object(pj, "El aura mágica de tu cinturón se desvanece tan rápido como vino.\n");
            pj->quitar_efecto("cinturon robacuraciones", 1);
        }
        
        destruct(this_object());
    }
    else {
        call_out("destruir_sombra_cinturon_robacuraciones", cinturon->dame_duracion_extra());
    }
}
void setup_sombra(object b, object cinto, int tiempo)
{
	pj 		 = b;
	cinturon = cinto;
	
	shadow(b, 1);
	call_out("destruir_sombra_cinturon_robacuraciones", tiempo);
}
int disipar_magia(string tx) {
    if ( tx && regexp(tx, "robacuraciones") ) {
        destruir_sombra_cinturon_robacuraciones(1);
        return 1;
    }
    
    if ( ! tx || tx == "" )
        destruir_sombra_cinturon_robacuraciones(1);
    
    return pj->disipar_magia(tx);
}
/*
KAKA
No se porqué, pero no rula. Los objetos van por referencía, así que pense que sí.
Una pena, porque sería super simple :P
mixed event_spell(object hechizo, object lanzador, mixed objetivo, object *objetivos_adicionales, object *fuera_alcance, int turno, int inaudible)
{
	if (hechizo && -1 != member_array(BASES + "curar_heridas.c", inherit_list(hechizo)))
	{
		if (objetivo && objectp(objetivo) && objetivo != pj && cinturon)
		{
			tell_object(pj, "¡Tu " + cinturon->query_short() + " roba el hechizo de " + lanzador->query_cap_name() + "!\n");
			tell_object(lanzador, "¡El " + cinturon->query_short() + " de " + pj->query_cap_name() + " roba tu hechizo!\n");
			
			objetivo = pj;
		}
	}
	
	return 0;
}

*/