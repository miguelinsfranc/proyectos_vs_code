// Eckol Mar18
#include <pk.h>

inherit "/obj/vaina";

#include <unidades.h>

#define DURACION 60*10
#define BLOQUEO 60*15
#define PODER_MAX 200

string dame_nombre_material()
{
    return "materia viva";
}

int no_puedo_robar(object ladron, object objeto){
    int ts = random(100);
    int dif = 100*dame_vida()/dame_vida_max();
    object portador;

    if(!environment())
        return 0;

    if(!query_in_use())
        return 0;

    portador = environment();

    portador->inform_tirada(dif,ts,"impedir robo ("+query_short()+")");

    if(ts > dif)
        return 0;

    return 1;
}

void setup()
{
    set_name("talabarte codicia");
    set_short("Talabarte de la "
              "%^GREEN%^C%^BOLD%^o%^RESET%^GREEN%^d%^BOLD%^i%^RESET%^GREEN%^c%^"
              "BOLD%^i%^RESET%^GREEN%^a%^RESET%^");
    set_main_plural("Talabarte de la "
                    "%^GREEN%^C%^BOLD%^o%^RESET%^GREEN%^d%^BOLD%^i%^RESET%^"
                    "GREEN%^c%^BOLD%^i%^RESET%^GREEN%^a%^RESET$%");
    generar_alias_y_plurales();

    set_long("Lo que a primera vista parece un talabarte de cuero de un color "
             "verde, es en realidad un ser vivo de extrañas características. "
             "Al inspeccionar detalladamente el talabarte, puedes ver que está "
             "cubierto por una infinidad de manos diminutas, sin uñas, que "
             "parecen abrazar el talabarte en toda su superficie. Las manos están "
             "totalmente quietas, pero al acercarles cualquier tipo de moneda, "
             "despiertan de su letargo para intentar hacerse con ella. El "
             "efecto es inmediato: al recorrer la superficie del talabarte con "
             "una pieza de cobre, las manos se levantan siguiendo "
             "el recorrido del dinero. Y es que es del dinero de lo que se "
             "alimenta el talabarte de la codicia: lo único que necesita para "
             "vivir son riquezas, que cogerá siempre que necesite recuperarse. "
             "Al mismo tiempo, si tiene fuerzas para ello, el talabarte evitará "
             "que nadie que no sea su propietario desenvaine el arma de su "
             "interior.\n");

    fijar_lugar("espalda");

    fijar_vida_max(1000);
    fijar_valor(20 * 500);
    fijar_encantamiento(5);
    fijar_peso(1500);
    fijar_material(3);

	fijar_BE(0);
	fijar_BO(10);

    fijar_armas("/table/bd_armas"->dame_armas());

    set_heart_beat(15);
}

string descripcion_juzgar(){
    string tx = sprintf("Se repara a él mismo, y al arma alimentándose del dinero del portador.\n");

    tx += sprintf("Impide robar el arma de su interior si se supera una tirada de salvación con dificultad según el estado del talabarte.\n");

    tx += sprintf("Añade el efecto opulencia al desenvainar, con poder proporcional al estado del talabarte, durante %s (bloqueo %s).\n",pretty_time(DURACION),pretty_time(BLOQUEO));

    return tx;
}

void objeto_equipado(){
    tell_object(environment(),sprintf("%s tiembla al ser equipado, deseoso de poder alimentarse de tus riquezas.\n",query_short()));
    set_heart_beat(15);
}

void objeto_desequipado(){
    tell_object(environment(),sprintf("%s emite un lamento al quitártelo, triste por no poder alimentarse ya de tus riquezas.\n",query_short()));
    set_heart_beat(0);
}

int ajustar_vida(int i){
    int r = ::ajustar_vida(i);

    return r;
}

void intentar_reparar(){
	int estado = min(({10,100*dame_vida()/dame_vida_max()}));
    object portador;
    int coste = estado*dame_valor()/100;

    portador = environment();

	if(dame_vida() >= dame_vida_max())
		return;

    if(!portador->pagar_dinero(coste,"cobre"))
        return;

    ajustar_vida(10*dame_vida_max()/100);
    tell_object(portador,sprintf("%s agarra alguna de tus monedas, que desaparecen entre los centenares de manos que la cubren.\n",query_short()));

    return;
}

void reparar_arma(){
    object *a = hay_armas();
    int coste;
    object ar,portador;
	int estado;

    if(!a || !sizeof(a))
        return;

    portador = environment();
    ar = a[0];

    if(ar->dame_vida()>=ar->dame_vida_max())
        return;

	estado = 100*ar->dame_vida()/ar->dame_vida_max();

	estado = min(({estado,10}));	

    coste = estado*ar->dame_valor()/100;

    if(!portador->pagar_dinero(coste,"cobre"))
        return;

    ar->ajustar_vida(10*ar->dame_vida_max()/100);
    tell_object(portador,sprintf("%s agarra alguna de tus monedas, que desaparecen entre los centenares de manos que la cubren.\n",query_short()));


    
}

int desenvainar_espada(string tx){
    object ar;

    if(dame_bloqueo_combate(TO))
        return ::desenvainar_espada(tx);

    if(sizeof(hay_armas()))
        ar = hay_armas()[0];

    if(!ar)
        return ::desenvainar_espada(tx);

    if(!::desenvainar_espada(tx))
        return 0;

    ar->nuevo_efecto_basico_temporal("opulencia",DURACION,1.*PODER_MAX*dame_vida()/dame_vida_max());
    nuevo_bloqueo_combate(TO,BLOQUEO);

    return 1;
}

void heart_beat(){
    if(!environment() || !living(environment())){
        set_heart_beat(0);
        return;
    }

    if(random(2) && dame_vida()<dame_vida_max()){
        intentar_reparar();
        return;
    }

    reparar_arma();
}
