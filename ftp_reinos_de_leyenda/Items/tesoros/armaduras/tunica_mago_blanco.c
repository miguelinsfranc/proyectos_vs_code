// Satyr 2012
inherit "/obj/armadura";
void setup()
{
	fijar_armadura_base("tunica");
	set_name("tunica");
	set_short("%^BOLD%^WHITE%^Túnica del Mago Blanco%^RESET%^");
	set_main_plural("%^BOLD%^WHITE%^Túnicas del Mago Blanco%^RESET%^");
	add_alias(({"túnica","mago blanco", "tunica del mago blanco", "túnica del mago blanco"}));
	add_plural(({"tunicas","túnicas", "tunicas del mago blanco", "túnicas del mago blanco"}));
	set_long(
		"Una túnica de seda blanca y gran caída. Se abrocha con botones de ámbar y un "
		"pequeño cinturón hecho de pelo de castor que ha sido teñido de blanco. Tiene un "
		"cuello elevado adornado con filigranas arcanas que imbuyen hechizos de protección "
		"en la pieza y otorgan ese acabado místico que tanto gusta lucir a los hechiceros. "
		"La longitud de la prenda hace que sea fácil arrastrarla por el suelo, pero los "
		"encantamientos que emanan de la túnica hacen que la tela levite a escasos "
		"centímetros del suelo cuando su portador se mueve, evitando manchar el blanco "
		"perlado impoluto que caracteriza la túnica.\n"
	);
	
	add_static_property("clase", "Hechicero");
	add_static_property("bando", ({"bueno", "mercenario", "neutral"}));

	fijar_encantamiento(20);
	ajustar_BE(4);
	
	add_static_property("ts-artefacto"   , 10);
	add_static_property("ts-mental"      ,  7);
	add_static_property("ts-paralizacion",  5);
	add_static_property("ts-conjuro"     ,  5);
	add_static_property("ts-fuerza bruta", -5);
	
	
	add_static_property("mal"       , 10);
	add_static_property("fuego"     ,  5);
	add_static_property("enfermedad",  5);
	add_static_property("veneno"    ,  5);
}
