// Dunkelheit 01-06-2011

inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("brazal");
    set_name("spelaea");
    set_short("%^BLACK%^BOLD%^Brazal Spelaea%^RESET%^");
    add_alias(({"brazal"}));
    set_main_plural("%^BLACK%^BOLD%^Brazales Spelaea%^RESET%^");
    add_plural(({"brazales", "spelaeas"}));
    
    set_long("Es el mítico brazal que perteneció a Kia Dai, el legendario "
    "antipaladín de la Era 2ª famoso por sus cruzadas impías en tierras "
    "kathenses. A diferencia de la mayoría de los brazales, el Spelaea "
    "cubre desde la totalidad del hombro hasta bien entrado el antebrazo, "
    "y otorga una movilidad decente gracias a que la codera está unida "
    "mediante mallas. Todas las piezas de la armadura, excepto las mallas, "
    "están cubiertas de una pintura negra mate.\n");
    
    fijar_encantamiento(10);
    
    fijar_genero(1);
}

string descripcion_encantamiento()
{
    return ::descripcion_encantamiento() + "\n%^BLACK%^BOLD%^Un aura de maldad rodea a esta armadura.%^RESET%^";
}

