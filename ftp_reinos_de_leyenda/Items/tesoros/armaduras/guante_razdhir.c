// Satyr 2012
inherit "/obj/armadura";
inherit "/obj/magia/objeto_formulador";
void setup()
{
    fijar_armadura_base("guante");
    set_name("guante");
    set_short("%^ORANGE%^Guante de %^RED%^Razdhir%^RESET%^");
    set_main_plural("%^ORANGE%^Guantes de %^RED%^Razdhir%^RESET%^");
    
    add_alias(({"razdhir", "guante de razdhir"}));
    add_plural(({"guantes", "guantes de razdhir"}));
    
    set_long(
		"Razdhir fue un poderoso mago que, poco después del Cataclismo, se labró una "
		"conocida fama gracias a su habilidad para manipular el fuego. Su obra maestra "
		"fue la creación de una serie de guantes que permitían controlar las llamas, "
		"los cuales manufacturó en gran número a lo largo de los años y vendió a un "
		"alto precio. Desgraciadamente, su arrogancia le llevó a desafiar al demonio "
		"Alchanar en su propio terreno y el secreto de la fabricación se perdió para "
		"siempre. Los guantes son de un cuero rojizo, con las puntas de los dedos "
		"descubiertas y permiten llamar al fuego una vez cada cierto tiempo.\n"
    );
    
    add_static_property("fuego"     ,  7);
	add_static_property("ts-aliento",  3);
	fijar_encantamiento(16);
		
	fijar_recuperacion(3600);
	fijar_esfera(2);
	fijar_nivel(20);
}
string descripcion_encantamiento()
{
	if (TP->dame_resistencia("fuego") < 70)
		return "La magia de fuego del guante es demasiado poderosa y no tienes la resistencia suficiente para conocer su secreto.";
	else
		return " %^RED%^F   L   A   M   A%^RESET%^.";
}
int formular_producir_llama(string tx) 
{ 
	return formular("producir llama", tx, 1); 
}
void init()
{
	::init();
	add_action("formular_producir_llama", "flama");
}
void iniciar_formulacion(string nombre_hechizo,object portador)
{
	tell_object(portador, "Abres la mano de tu " + query_short() + " al tiempo que pronuncias la palabra de mando.\n");
	tell_accion(environment(portador),
		portador->query_cap_name() + " abre la mano de su " + query_short() + " al tiempo que pronuncia la palabra de mando.\n",
		"",
		({portador}),
		portador
	);
}
void realizar_cantico(object lanzador,object hechizo,string cantico)
{
	tell_room(environment(lanzador),"¡Una bola de fuego anaranjada y brillante aparece en " + query_short() + ", "
		"levitando a escasos centímetros de su superficie!\n"
	);
}