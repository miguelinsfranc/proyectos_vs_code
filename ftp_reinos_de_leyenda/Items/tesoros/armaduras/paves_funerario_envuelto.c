// Satyr 2012
#include <spells.h>
inherit "/obj/escudo"; 
void setup()  
{ 
	set_base_shield("escudo corporal");
	set_name("paves");
	set_short("%^MAGENTA%^Pavés Funerario Envuelto%^RESET%^");
	set_main_plural("%^MAGENTA%^Escudos Pavés Funerio Envuelto%^RESET%^");
	
	add_alias(({"pavés", "funerario", "paves funerario", "pavés funerario", "pavés funerario envuelto"}));
	add_plural(({"escudos", "escudos paves", "escudos pavés"}));
	
	set_long(
		"Un escudo fruto de la exumación del Rey Funerario Nehertak-aphek-ithil III, uno de los "
		"antiguos dioses-gobernadores de los pueblos que habitaban en el desierto de Galador antes "
		"de que la fortaleza de la Inquisición de Seldar siquiera existiese. Está hecho de la propia "
		"madera que formaba el ataúd del Rey, un ébano muy oscuro que ha sido unido en forma de un "
		"grueso escudo pavés. El escudo, de aproximadamente un metro de altura, está envuelto en las "
		"vendas de la momia del difunto monarca y su espíritu furibundo las manipula, haciendo que "
		"estas se muevan como los siniestros tentáculos de un pulpo fantasmal. El espíritu "
		"del rey ya no entiende que es real y que no; que es vida y que es muerte, por lo que descarga "
		"su cólera con todos aquellos que están delante de este escudo que ahora es su cárcel y su cuerpo. "
		"Su único deseo es causar malestar y compartir con el mundo un fragmento de su miseria.\n"
	);

	add_static_property("ts-horror", 5);
	add_static_property("ts-muerte", 5);
	add_static_property("bien"     , 5);
	add_static_property("mal"	   , 5);
	
	fijar_encantamiento(30);
	fijar_material(1);
	ajustar_BO(-10);
	set_heart_beat(4);
	add_static_property("nivel_minimo",29);
}
string descripcion_encantamiento()
{
	return "El espíritu furibundo del rey golpeará y maldecirá a lo que encuentre enfrente del escudo.";
}
void heart_beat()
{
	if (query_in_use() && environment() && living(environment()))
	{
		if (!query_timed_property("agotado"))
		{
			object *enemigos = filter(environment()->query_attacker_list(), (:$1 && !$1->dame_efecto("maldicion"):));
			object spell = load_object("/hechizos/esferas/total/"+ "maldicion.c"), enemigo;
			string *sms;
			if (sizeof(enemigos) && spell && enemigo = element_of(enemigos))
			{
				sms = ({
					"%^MAGENTA%^#%^RESET%^ ¡Tu " + query_short() + " se desenvuelve y lanza un montón de vendas contra "
					+ enemigo->query_cap_name() + " pero no logra culminar sus malévolas intenciones!",
					"%^MAGENTA%^*%^RESET%^ ¡El " + query_short() + " de " + environment()->query_cap_name() + " se "
					"desenvuelve y lanza un montón de vendas contra ti, pero logras evitar su macabro abrazo!",
					"¡El " + query_short() + " de " + environment()->query_cap_name() + " se desenvuelve y lanza "
					"un montón de vendas contra " + enemigo->query_cap_name() + ", pero este logra evitar su "
					"macabro toque!"
				});
			
				if (enemigo->dame_ts("artefacto", 0, "paves envuelto", "paves envuelto"))
				{
				}
				else
				{
					int pupa = enemigo->weapon_damage(-50 - random(49), environment(), this_object(), "lacerante");
					
					if (-300 != pupa)
					{	
						sms = ({
							"%^GREEN%^#%^RESET%^ ¡Tu " + query_short() + " se desenvuelve, lanzando un montón de vendas "
							"contra " + enemigo->query_cap_name() + " que le laceran repetidas veces con sadismo sobrenatural, "
							"transmitiéndole su maldición de ultratumba!",
							"%^RED%^*%^RESET%^ ¡El " + query_short() + " de " + environment()->query_cap_name() + " se "
							"desenvuelve, lanzando un montón de vendas contra ti que te laceran repetidas veces "
							"con sadismo sobrenatural, transmitiéndote su maldición de ultratumba!",
							"¡El " + query_short() + " de " + environment()->query_cap_name() + " se desenvuelve, lanzando "
							"un montón de vendas contra " + enemigo->query_cap_name() + " que le laceran repetidas veces "
							"con sadismo sobrenatural, transmitiéndole su maldición de ultratumba!"
						});
						spell->otorgar_maldicion(enemigo, 15 + random(6), 200, environment()->dame_bando());
					}
					else
					{
					}
				}
				
				tell_object(environment(), sms[0] + "\n");
				
				if (enemigo)
					tell_object(enemigo, sms[1] + "\n");
				
				if (environment(environment()))
					tell_accion(environment(environment()), sms[2] + "\n", "", ({environment(), enemigo}), environment());
					
				add_timed_property("agotado", 1, 60);
			}
		}
	}
}
