// Satyr 08/05/2018
#include <baseobs_path.h>
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("pendiente");
    set_name("ornamento");
    set_short("%^BOLD%^YELLOW%^Ornamento de los Necios%^RESET%^");
    set_main_plural("%^BOLD%^YELLOW%^Ornamentos de los Necios%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Un bellísimo, opulento -y seguramente muy caro- pendiente cuya "
        "belleza "
        "te hace sentir un cálido cosquilleo.\n\n"

        "  A primera vista parece estar hecho de alguna gema preciosa, tallada "
        "con cariño y saber hacer, que después fue recubierta por una untuosa "
        "capa de oro sobre la que se ve el blanco nacarado de la "
        "madreperla. Hermosa, sin duda. Magnífica, tal vez. Indiscutiblemente, "
        "digna de reyes, caciques, emperatrices, "
        "dioses y cualquier otra criatura que tenga una elevada noción de si "
        "misma.\n\n"

        "  Sin embargo, la primera impresión no es más que eso: una primera "
        "impresión. "
        "Realmente esta pieza no es más que una burda tira de cuero de la que "
        "cuelga "
        "una piedra bastante ordinaria -¡y que no es nada untuosa!-.\n\n"

        "  ¡Pero ojo!, esto es una confidencia y realmente tu personaje no "
        "debería saberlo. La piedra es mágica (muy "
        "mágica) "
        "y su poder radica en que es capaz de manipular su aspecto mediante la "
        "ilusión, por lo que realmente no "
        "sabes que es horrible. De hecho, si te la pones, ¡tampoco nadie lo "
        "sabrá! "
        "Así podrás presumir delante de aquellos lo suficientemente necios "
        "como "
        "para valorar las apariencias sobre aptitudes más importantes.\n\n"

        "  Ten en cuenta, no obstante, que de optar por hacer esto último, "
        "irónicamente tu también serás un necio, pues estarás también cayendo "
        "en ese pozo de banalidad y superficialidad. Aunque puede que no te "
        "importe, claro.\n");

    add_static_property("car", 1);
    add_static_property("sab", -1);
    add_static_property("int", -1);
    fijar_encantamiento(20);
    fijar_material(5);
}
