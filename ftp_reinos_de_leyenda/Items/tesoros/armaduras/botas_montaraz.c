// Satyr 2012
// Kaitaka 15Feb2012 - Les pongo un material acorde a su descripción
// Zilar 12/5/2014 - Modifico las botas para que haya que activarlas y dure un tiempo

inherit "/obj/armadura";

#define DURACION_EFECTO 5
#define BLOQUEO_EFECTO 15

void setup(){
	fijar_armadura_base("botas");
	set_name("botas");
	set_short("Botas de %^ORANGE%^Montaraz%^RESET%^");
	set_main_plural("par de " + query_short());
	add_alias(({"montaraz", "botas de montaraz"}));
	add_plural(({"par de botas", "par de botas de montaraz"}));
	set_long(
		"Dos botas hechas de cuero de piel de varios animales que han sido remendados "
		"para proporcionar la mayor relación flexibilidad / resistencia en cada zona "
		"del pie. Estas botas son fabricadas por individuos que viven solos en las "
		"montañas o bosques de los reinos y son muy apreciadas porque su peculiar "
		"forma impide que se deje ningún tipo de rastro siempre y cuando esten impregnadas"
		"con las raices de Shalomn.\n"
	);
	fijar_material(3);
	fijar_valor(200);
}

void fin_efecto_montaraz(object quien){
	if (quien) {
		tell_object(quien, "Los efectos de " + query_short() + " se terminan.\n");
		quien->adjust_static_property("sin-huella", -100);
	
	}
}

void objeto_desequipado(){
	if (environment() && environment()->query_player() && environment()->dame_efecto("botas montaraz")){
		environment()->quitar_efecto("botas montaraz",0);
	}
}

status do_frotar (mixed planta){
	if (this_player()->query_timed_property("bloqueo-botas-montaraz") || (query_timed_property("agotado")) ) {
		notify_fail("Tus "+query_short()+" siguen pringosas de la última vez que las frotaste con "+planta->query_short()+"\n");
		tell_object(this_player(),this_player()->query_notify_fail_msg());
	}else if (this_player()->adjust_static_property("sin-huella",100)){
		tell_object(this_player(),"Frotas tus " + query_short() + " con una dosis de " + planta->query_short() + " y sientes como tus pies se hacen mas livianos.\n");
		tell_accion(environment(this_player()),this_player()->query_cap_name() + " frota una planta sobre sus " + query_short() + ".\n","",({this_player()}),this_player());
	
		this_player()->nuevo_efecto("botas montaraz", 60 * DURACION_EFECTO, (:fin_efecto_montaraz, this_player():), 0, 0);
		
		//bloqueo de las propias botas -- no se la pasen entre pjs
		add_timed_property("agotado", 1, 60 * BLOQUEO_EFECTO); 
		//bloqueo del player ( no tenga 4 botas y se vaya cambiando para frotarlas )
		this_player()->add_timed_property("bloqueo-botas-montaraz", 1, 60 * BLOQUEO_EFECTO);
		planta->consumir_dosis();
	}
	return 1;
}
void init(){
	::init();
	anyadir_comando_equipado("frotar", "(botas|montaraz) con <objeto:no_interactivo:yo:total:primero:buscar_en:0:dame_bolsita:0:funcion:dame_planta:No dispones de Shalomn'shalomn'>", "do_frotar", 0);
}

