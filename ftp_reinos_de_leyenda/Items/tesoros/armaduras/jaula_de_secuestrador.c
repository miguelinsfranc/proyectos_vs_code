// Satyr 09/05/2018 12:53
inherit "/obj/equipo_contenedor";

void setup()
{
    fijar_armadura_base("mochila");
    set_name("jaula");

    set_short("Jaula de secuestrador");
    set_main_plural("Jaulas de secuestrador");
    generar_alias_y_plurales();

    set_long(
        "¿Cuál es la santa trinidad de las cualidades en un buen trabajador? "
        "La persona que escribió esta descripción considera que son tres: "
        "experiencia, aptitud y unas buenas herramientas.\n\n"

        "  Es evidente que sin maña no va a poder llegarse a nada, pero más "
        "difícil sería trabajar sin los utensilios que la historia ha otorgado "
        "a la humanidad (y elfidad, gnomidad, orquidad...), ¿o te ves capaz "
        "de forjar un hierro a puñetazos?\n\n"

        "  Si bien el oficio del secuestrador no está muy reconocido por los "
        "gremios de Eirea, está bien claro que se trata de una profesión "
        "muy lucrativa y que no requiere estudiar mucho: basta con tener "
        "agallas, músculos y amigos.\n\n"

        "  Sin embargo, el capitalismo pronto llegó a esta antigua profesión, "
        "causando que se buscase, sobre todo, el optimizar tiempos para "
        "rentabilizar los secuestros, permitiendo así secuestrar más. Lo que "
        "antiguamente era una profesión artesanal, pronto pasó a ser algo "
        "que requería entender mucho de tiempos y \"flujos de optimización de "
        "recursos\", dejando a los secuestradores de toda la vida sin pan que "
        "llevarse a la boca.\n\n"

        "  Tras varios eventos celebrados -a donde acudían los más renombrados "
        "secuestradores (como cierto Bardo que se llevó a una reina delante "
        "de una corte para hacer con ella un principito)- los expertos "
        "concluyeron que la mejor forma de optimizar el flujo del secuestro es "
        "llevarte contigo a tus víctimas. Cosa que no es fácil si no tienes "
        "las herramientas necesarias.\n\n"

        "  Por suerte para ti tienes ante ti una Jaula de secuestrador: "
        "una especie de mochila (los más puristas dicen que el termino "
        "\"saco\" lo define mejor, semanticamente hablando) que sirve para "
        "contener a tus "
        "secuestrados mientras te encaminas al siguiente.\n\n"

        "  Se trata de un sólido y pesado armazón de hierro ennegrecido "
        "constituido de varias bandas que forman una jaula oval. Sendas bandas "
        "a los lados permiten llevarla a la espalda y una cerradura encantada "
        "hace que puedas olvidarte de tener que vigilar a esos escurridizos "
        "magos capaces de abrir las cerraduras normales, permitiendo así "
        "que te centres en el próximo secuestro.\n");

    fijar_BP(dame_BP() + 10);
    fijar_BE(-5);
    fijar_encantamiento(5);
    fijar_peso_max(50000);
    fijar_reduccion_de_peso(45);
    fijar_valor(28000);
    fijar_opacidad(1);
    fijar_genero(2);
    fijar_peso(30000);
    fijar_material(2);
}
