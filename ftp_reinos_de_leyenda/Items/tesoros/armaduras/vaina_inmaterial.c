// Satyr 2012
#define ESPADA "/baseobs/tesoros/armas/espada_inmaterial"
#define DURACION 3600   // Duración de la espada, una vez desenvainada
#define BLOQUEO 300     // Bloqueo durante el que la vaina no podrá crear una nueva espada. 
                        // Se aplica al desenvainar la espada por primera vez. Se le suma la duración
inherit "/obj/vaina";
object clonada;
void setup()
{
    set_name("vaina");
    add_alias(({"inmaterial", "vaina inmaterial"}));
    add_alias(({"inmateriales", "vainas inmateriales"}));
    set_short("%^BOLD%^WHITE%^Vaina inmaterial%^RESET%^");
    set_main_plural("%^BOLD%^WHITE%^Vaina inmaterial%^RESET%^");
    set_long(
        "Algunos creen que esta extraña vaina fue creada por alguno de los "
        "Dioses Primigéneos durante sus guerras, aunque son pocos los que "
        "dan credibilidad a esta teoría. Otros, sobre todo nigromantes, "
        "opinan que se trata de un objeto ajeno al Primer Plano Material, "
        "fabricado en el Limbo a partir de la propia esencia de aquella "
        "dimensión. Sea cual sea su origen, son muy codiciadas por los "
        "aventureros, ya que toda Vaina Inmaterial suele contener una "
        "fabulosa Espada Inmaterial.\n"
    );

    fijar_peso(500);
    fijar_genero(2);

    //fijar_encantamiento(30);
    fijar_lugar("cintura");
    fijar_material(10);
   
    fijar_armas(({ESPADA}));
    
    set_heart_beat(1);

	add_static_property("nivel_minimo", 30);
}
object dame_espada()
{
    return clonada ? clonada : secure_present(ESPADA, TO);
}
void poner_espada()
{
    object entorno = environment();
    
    if (living(entorno) && /*!query_timed_property("bloqueo-espada") &&*/ query_in_use())
    {
        object espada = clone_object(ESPADA);
        
        if (espada)
        {
            espada->move(this_object());
            clonada = espada;
            
            tell_object(entorno, "El brillo de la magia de tu " + query_short() + " te ciega cuando " 
                ""+ espada->query_short() + " se materializa en ella.\n"
            );
        
            if (environment(entorno))
                tell_accion(environment(entorno), "¡Un flash de luz cegador inunda la sala cuando " 
                    + espada->query_short() + " se materializa en la " + query_short() + " de " 
                    + entorno->query_cap_name() + "!\n", "", ({entorno}), entorno);
        }
    }
}
void quitar_espada()
{
    object esp;
    
    if (esp = dame_espada())
        esp->desvanecerse(environment());
}
void heart_beat()
{
    if (query_in_use() && environment() && living(environment()))
    {
        if (!dame_espada())
        {
            poner_espada();
        }
    }
}
void mensajes_vaina_desenv(object pj, object espada)
{
    tell_object(pj, "Desenvainas " + espada->query_short() + ", que emite una luz cegadora y radiante a todo el entorno.\n");
    tell_accion(
        environment(pj), 
        "Te ves obligado a cerrar los ojos cuando " + pj->query_cap_name() + " desenvaina " + espada->query_short() + ".\n",
        "",
        ({pj}),
        pj
    );      
}
void mensajes_vaina_env(object pj, object espada)
{
    tell_object(
        pj, 
        "El implacable brillo y zumbido de tu " + espada->query_short() + " se apaga cuando la envainas.\n"
    );
    tell_accion(environment(pj),
        "El brillo y zumbido que emitía la " + espada->query_short() + " de " + pj->query_cap_name() + " cesan "
        "cuando esta es envainada. Un pequeño pitido aún hace eco en tus oídos.\n",
        "",
        ({pj}),
        pj
    );
}
void fin_espada(object espada, object pj)
{
    if (espada)
    {
        espada->desvanecerse(pj ? pj : environment());
    }
}
int desenvainar_espada(string tx) 
{
    int i = ::desenvainar_espada(tx);
    
    if (i)
    {
        object espada = dame_espada();
        
        if (espada && ! espada->dame_invocador())
        {
            espada->fijar_invocador(TP);
            //TP->nuevo_efecto("espada inmaterial", DURACION, (:fin_espada, espada, TP:), 0, espada);
            //add_real_timed_property("bloqueo-espada", 1, DURACION + BLOQUEO);
        }
    }
    
    return i;
}
void objeto_desequipado()
{
    quitar_espada();
}
