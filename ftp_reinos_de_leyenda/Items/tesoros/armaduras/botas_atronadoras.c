// Satyr 23/05/2018 12:55
#include <combate.h>
inherit "/obj/armadura";

#define LENTITUD -6
#define DURACION 6
#define INTERRUPTOR 10
#define BLOQUEO 120

void setup()
{
    fijar_armadura_base("botas de guerra");
    set_name("botas");
    set_short("Botas de guerra %^YELLOW%^A%^NOBOLD%^tronadoras%^RESET%^");
    set_main_plural(
        "pares de Botas de guerra %^YELLOW%^A%^NOBOLD%^tronadoras%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Dos brutales botas de guerra cuyo concepto fue ingeniado por "
        "los svirfneblin, si bien fueron los duergar los que lo robaron "
        "y perfeccionaron para crear la semejante masa de hierro negro "
        "que tienes ante ti.\n\n"

        "  Las botas, pesadas como un bloque de hormigón, están fabricadas "
        "en una sola pieza y adornadas con dos relieves que representan "
        "la cara de un enano gris gritando en sus laterales.\n\n"

        "  Cada paso que dan retumba en los alrededores y su suela reforzada "
        "es capaz de despertar el poder de la tierra, que fue concebido "
        "inicialmente para facilitar la creación de túneles, pero que los "
        "seguidores de Oneex pervirtieron para convertirlo en una forma más "
        "de aplastar los cráneos de sus enemigos, por muy bien protegidos "
        "que se encontrasen.\n");

    fijar_encantamiento(20);
    fijar_peso(dame_peso() * 4);
    add_static_property("tierra", 7);
    add_static_property("ts-conjuro", 7);
    add_static_property("ts-mental", -10);
}

string descripcion_juzgar(object b)
{
    return "Este par de botas permite pegar un tremendo pisotón en el suelo "
           "que causará que todos los presentes en la sala del portador "
           "sean sacudidos brutalmente por un seísmo, ¡lo que le incluye a él "
           "mismo!\n\n"

           " Los objetivos sacudidos sufrirán un ataque de tipo 'tierra' de "
           "daño "
           "menor con los efectos "
           "Interruptor(" +
           INTERRUPTOR + ") y  Lentitud(" + LENTITUD +
           "), "
           "éste último con una duración de " +
           pretty_time(DURACION) + ".\n";
}
object crear_estilo(object ejecutor)
{
    string aux;
    object estilo;

    estilo = ejecutor->dame_clon_estilo("basicos/tierra");

    aux = estilo->nuevo_efecto_basico("interruptor", INTERRUPTOR);
    estilo->fijar_bloqueo_efecto(aux, -1);

    aux = estilo->nuevo_efecto_basico("lentitud", LENTITUD);
    estilo->fijar_bloqueo_efecto(aux, -1);
    estilo->fijar_duracion_bloqueo(aux, DURACION);

    return estilo;
}
object *dame_afectados(object ejecutor)
{
    return "/habilidades/target.c"->find_unique_match(
        "todos", environment(ejecutor), 0, 0, ejecutor);
}
int pisoton()
{
    object estilo, *afectados;

    if (dame_bloqueo_combate(TO)) {
        return notify_fail(
            "¡" + capitalize(dame_tu()) + " " + query_short() +
            " "
            "aún no están listas para otra sacudida!\n");
    }

    if (TP->dame_bloqueo_combate(TO)) {
        return notify_fail(
            "Hace poco que has usado ese poder y necesitas descansar "
            "antes de invocarlo de nuevo.\n");
    }

    if (!sizeof(afectados = dame_afectados(TP))) {
        tell_accion(
            environment(TP),
            "¡" + TP->query_cap_name() + " golpea furiosamente el suelo con " +
                SU_S(TO) + ", pero no parece que suceda nada!\n",
            "Oyes un tremendo patadón contra el suelo.\n",
            ({TP}),
            TP);
        return notify_fail(
            "%^MAGENTA%^#%^RESET%^ ¡Estampas con furia " + TU_S(TO) +
            " contra "
            "el suelo, sin embargo, su poder no se despierta al no "
            "haber nadie más en las inmediaciones!\n");
    }

    afectados |= ({TP});
    nuevo_bloqueo_combate(TO, BLOQUEO);
    TP->nuevo_bloqueo_combate(TO, BLOQUEO);
    estilo = crear_estilo(TP);

    tell_object(
        TP,
        "%^GREEN%^#%^RESET%^ ¡Tus " + query_short() +
            " golpean con cólera "
            "el suelo, creando una onda sísmica que te hace temblar a ti y a " +
            query_multiple_short(afectados - ({TP})) + "!\n");
    tell_accion(
        environment(TP),
        "¡" + TP->query_cap_name() + " golpea el suelo con " + SU_S(TO) +
            ", "
            "creando una onda sísmica que hace temblar toda la sala!\n",
        "¡Un tremendo seísmo sacude tus alrededores!\n",
        ({TP}),
        TP);

    foreach (object b in afectados) {
        estilo->realizar_ataques(b, TP, (["no_mensajes":1]), 3);
    }

    return 1;
}
void init()
{
    ::init();

    anyadir_comando_equipado("pisoton", "", "pisoton", descripcion_juzgar(0));
}
