// Satyr 2007
// Yelmo del Amanecer
// +5 magico
// +5 electrico
// +10 enchant
// +1 luz

inherit "/obj/armadura";

string dame_nombre_material()
{
    return "Hierro de la mejor calidad y Oro.";
}
void setup()
{
    fijar_armadura_base("yelmo");
    set_name("yelmo");
    set_short("Yelmo del %^BOLD%^YELLOW%^Amanecer%^RESET%^");
    set_main_plural("Yelmos del %^BOLD%^YELLOW%^Amanecer%^RESET%^");
    add_alias(({"yelmo","amanecer","yelmo del amanecer"}));
    add_plural(({"yelmos","amanecer","yelmos del amanecer"}));
    set_long(
        "\tUn nada discreto yelmo ligero, de cara descubierta y forma alargada usado principalmente por la caballería ligera. "
        "El yelmo está adornado y personalizado, ya que su parte superior presenta mechón de pelo de algún animal que ha sido tintado de un "
        "intenso color azulado. "
        "Sendas partes laterales del yelmo están adornadas con vivos colores de tonos amarillentos, líneas rectas brotan en todas direcciones "
        "desde su parte central, haciendo parecer que el pequeño dibujo de un sol que presenta en su parte central emite luz de verdad. "
        "El yelmo está forrado con suave terciopelo en su interior y multitud de adornos de oro -como los de los grabados- adornan esta "
        "pieza de armadura que, sin duda, tiene procedencia noble.\n"
    );
    
    add_static_property("electrico",5);
    add_static_property("magico",5); 
    fijar_encantamiento(30);
    fijar_luz(1);
}
