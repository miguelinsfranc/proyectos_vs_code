// Satyr 2012
inherit "/obj/armadura";
void setup()
{
	fijar_armadura_base("grebas metalicas");
	set_name("grebas");
	set_short("%^WHITE%^BOLD%^Grebas Centelleantes%^RESET%^");
	set_main_plural("par de " + query_short());
	add_alias(({"centelleantes", "grebas centelleantes"}));
	add_plural(({"pares", "pares de grebas centelleantes"}));
	set_long(
		"Dos grebas de metal grisáceo que han sido pulidas hasta que, "
		"prácticamente, se han convertido en dos espejos. Son gruesas y "
		"se cierran mediante unas correas que quedan escondidas tras "
		"las placas metálicas. Hace falta una curiosa combinación de "
		"ingenio y maña para poder ponérselas de forma adecuada. "
		"Las placas de las espinillas están más acolchadas, mientras que "
		"las de los muslos son más gruesas y absorben mejor los impactos. "
		"Varias runas, tan pequeñas que pasan desapercibidas, proporcionan "
		"a este objeto una calidad mágica.\n"
	);
	
	fijar_encantamiento(20);
	ajustar_BO(-3);
}
