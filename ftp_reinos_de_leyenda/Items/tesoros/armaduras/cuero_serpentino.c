// Satyr 2012
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("cuero");
    set_name("cuero");
    set_short("Armadura de Cuero %^GREEN%^S%^BOLD%^erpentino%^RESET%^");
    set_main_plural("Armaduras de Cuero %^GREEN%^S%^BOLD%^erpentino%^RESET%^");
    set_long("Cuero proveniente de la carcasa sin vida de varias serpientes. "
             "Las pieles han "
             "sido estiradas, hervidas y cosidas para formar una pechera de "
             "este material "
             "de color verde brillante y tacto suave, pero escamoso. La "
             "armadura es bastante "
             "delgada y no ofrece tanta protección como otro tipo de pieles, "
             "pero es ligera "
             "y se amolda a las formas de su portador, aparte de ser "
             "extremadamente hermosa.\n");
    generar_alias_y_plurales();
    fijar_genero(2);
    fijar_encantamiento(2);
    add_static_property("ts-veneno", 1);

    add_static_property("ts-fuerza bruta", -5);
    add_static_property("ts-agilidad", 8);
    add_static_property("esconderse", 15);
    add_static_property("sigilar", 15);
    add_static_property("envenenar", 10);

    fijar_proteccion(9);
    ajustar_BE(14);

    fijar_valor(10000);
}
