// Satyr 14/05/2018
inherit "/obj/armadura";
void setup()
{
    fijar_armadura_base("manto");
    set_name("manto");
    set_short("%^BLACK%^BOLD%^Manto del Saqueador de Tumbas%^RESET%^");
    set_main_plural("%^BLACK%^BOLD%^Mantos del Saqueador de Tumbas%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Robar a los vivos suele estar penado con cárcel o amputación súbita "
        "de una mano. Robar a los muertos, sin embargo, suele estar penado "
        "con la muerte. Tal es la sociedad en la que viven los hombres "
        "religiosos de hoy en día.\n\n"

        "  Por eso, los ladrones de tumbas optan por usar prendas que les "
        "escondan y les ayuden a defenderse: pues nunca se sabe cuando algún "
        "parroquiano va a querer ir, en el medio de una noche tormentosa, "
        "a visitar la tumba de una de sus tías lejanas. Y claro, si resulta "
        "que es la misma que tú estás saqueando, pues tendrás que despacharlo"
        " antes de que te desvelen y la autoridad se empeñe en meterte a ti "
        "en otro agujero.\n\n"

        "  Esta prenda no garantiza el éxito en la faena, ¡pero vaya si ayuda! "
        "Es muy amplia, ligera y sus colores sirven para camuflarse "
        "perfectamente en el grís páramo del cementerio nocturno. Además, "
        "su corte está rematado en una fina cuchilla, lo que permite usarla en "
        "combate "
        "para "
        "conseguir una ventaja extra que más de una vez te permitirá "
        "salir indemne de una exhumación embarazosa.\n");

    fijar_BO(20);
    fijar_BE(0);
    add_static_property("sigilar", 60);
    add_static_property("envenenar", 10);
}
