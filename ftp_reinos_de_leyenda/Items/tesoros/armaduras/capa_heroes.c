// Satyr 2012
inherit "/obj/armadura";
string dame_nombre_material()
{
	return "Hilos de seda muy rara";
}
string descripcion_encantamiento()
{
	return "La capa ha sido bendita con sortilegios que proporcionan una gran gracilidad y libertad de movimiento a su portador.";	
}
void setup()
{
	fijar_armadura_base("capa");
	set_name("capa");
	set_short("%^BOLD%^WHITE%^Capa de los Héroes%^RESET%^");
	set_main_plural("%^BOLD%^WHITE%^Capas de los Héroes%^RESET%^");
	add_alias(({"capa","héroes","capa de los heroes", "capas de los héroes"}));
	add_plural(({"capas","héroes","heroes","capas de los heroes"}));
	set_long(
		"Una larga capa hecha de plumas de grifo entretejidas con una seda importada de fuera del continente. "
		"Su parte interior, forrada en piel de borrego, es muy cálida y suave. Su parte exterior está teñida "
		"de un azul celeste que brilla cuando la luz incide sobre ella en ciertos ángulos, debido a la "
		"naturaleza mágica de las plumas. Su corte es excelente y su acabado... de lujo.\n"
	);

	fijar_material(4);
	
	// Lo interesante
	fijar_encantamiento(10);
	add_static_property("ts-agilidad", 5);
	add_static_property("ts-horror"  , 5);
	
	//Ember, lo cambio por el 10% de prote eléctrico
	add_static_property("mal", 2);
	add_static_property("car", 1);

	// Para que se flipen con un detectar magia
	add_static_property("fuego",1);
	add_static_property("electrico",1);	    
	add_static_property("bien",1);
	add_static_property("agua",1);
      
    fijar_valor(60*500);

	add_static_property("nivel_minimo", 30);
}
