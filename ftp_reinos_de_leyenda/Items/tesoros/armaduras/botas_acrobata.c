// Satyr 2012
#define MIN_DES 15
inherit "/obj/armadura";
void setup()
{
	fijar_armadura_base("botas");
	set_name("botas");
	set_short("Botas del Acróbata");
	set_main_plural("pares de Botas del acróbata");
	add_alias(({"acróbata","acrobata","botas del acrobata"}));
	add_plural(({"pares de botas", "pares de botas del acróbata"}));
	set_long(
		"Dos botas de cuero fino y de mala calidad que han sufrido incontables "
		"días de espectáculos y aventuras por parte de juglares, acróbatas, payasos "
		"y demás fauna circense. Su color se ha ido aclarando con el paso del tiempo, "
		"los cordones que sirven para atarlas han sido reemplazados ya varias decenas "
		"de veces y una de las hebillas que tenían ha desaparecido. A pesar de ser "
		"andrajosas, son muy apreciadas por sus propietarios por su comodidad y la "
		"libertar de movimientos que ofrecen a la hora de realizar pericias de destreza "
		"o acrobacia.\n"
	);
	
	add_static_property("ts-agilidad", 6); 
	fijar_proteccion(18); 
	fijar_peso(600);
	ajustar_BE(6);
}

int set_in_use(int i)
{
	if (i && environment() && environment()->dame_carac("des") < MIN_DES)
	{
		tell_object(environment(), "Necesitas una destreza mínima de " + MIN_DES + " para usar " + query_short() + ".\n");
		return 0;
	}
	
	return ::set_in_use(i);
}