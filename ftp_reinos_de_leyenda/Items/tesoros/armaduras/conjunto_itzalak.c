// Conjunto de armaduras -- Dunkelheit 02-06-2011
// Eckol Ago19: conj nuevo

#include <baseobs_path.h>
inherit "/obj/conjunto_nuevo";

void mostrar_mensajes_equipar_conjunto(object pj, object item, int piezas)
{
    if (piezas == 2)
        tell_object(
            pj,
            "Te fundes con las sombras de tu entorno al vestir las tres piezas "
            "del conjunto %^BLACK%^BOLD%^Itzalak%^RESET%^.\n");

    return;
}

void mostrar_mensajes_desequipar_conjunto(object pj, object item, int piezas)
{
    if (piezas == 3)
        tell_object(
            pj,
            "Ojos curiosos e inquisidores vuelven a clavarse en ti con "
            "facilidad.\n");

    return;
}

void setup()
{
    set_short("Conjunto Itzalak");
    generar_alias_y_plurales();
    fijar_genero(1);

    fijar_piezas(
        ({BTESOROS_ARMADURAS + "capa_itzalak",
          BTESOROS_ARMADURAS + "capucha_itzalak",
          BTESOROS_ARMADURAS + "peto_itzalak"}));

    fijar_conjunto_parcial();
    nuevo_beneficio_caracteristica(2, "envenenar", 40);
    nuevo_beneficio_caracteristica(3, "robar", 20);
    nuevo_beneficio_caracteristica(3, "envenenar", 20);
}
