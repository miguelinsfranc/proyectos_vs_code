/*
 * Un objeto de días más nobles para celebrar mi retorno krustiano: unas
 * legendarias sin encantamiento que dan +1 a destreza.
 *
 * Dunkelheit 18-05-2011, original de Paris
 */

inherit "/obj/armadura";

string descripcion_juzgar(object quien)
{
    if (quien && quien->dame_carac("sab") > 17) {
        return "Tu elevada sabiduría te permite reconocer este objeto como una "
               "de las mayores reliquias de la Era 3ª: cualquier aventurero "
               "que se preciase "
               "debía vestir ese poderoso par de botas.";
    }
    return 0;
}

void setup()
{
    fijar_armadura_base("botas");
    set_name("botas");
    set_short("Botas de Destreza Halfling");
    set_main_plural("pares de Botas de Destreza Halfling");
    add_alias(({"destreza", "halfling"}));
    add_plural(({"pares", "destrezas", "halflings"}));
    fijar_genero(-2);

    set_long(
        "Los buscadores de tesoros poco experimentados dejarían pasar sin duda "
        "este destartalado par de botas de cuero a la hora de saquear un baúl "
        "o la guarida "
        "de un dragón, pues por su aspecto humilde parecen más bien las botas "
        "que tú o "
        "cualquier otro aventurero podrían comprar en el más ordinario de los "
        "mercados. "
        "Pero sólo es necesario deslizar la punta de los dedos por el acogedor "
        "revestido de "
        "piel de borrego para advertir que estas botas son algo fuera de lo "
        "común; es más, "
        "una vez enfundadas hasta el fondo -más o menos hasta la un tercio de "
        "la espinilla-, "
        "su portador obtendrá una agilidad sobrehumana, tan sólo equiparable a "
        "aquélla de un "
        "halfling. El cuero de las botas ha sido teñido con una pintura de "
        "color verde "
        "oscuro, casi de camuflaje, y la apertura de la caña está vuelta, "
        "dejando asomar "
        "el revestido de color marfil oscuro.\n");

    add_static_property("des", 1);
    add_static_property("esconderse", 75);
    add_static_property("sigilar", 100);
    add_static_property("forzar", 50);
    add_static_property("trampas", 10);
    add_static_property("robar", 50);

    add_static_property("nivel_minimo", 30);

    add_static_property("no_raza", ({"halfling"}));
}
