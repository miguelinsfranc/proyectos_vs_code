// Dunkelheit 02-06-2011
inherit "/obj/multiarmadura.c";

void setup()
{
    fijar_nombre_generico("Armadura Ligera Encantada");
    fijar_tipos_validos(({ "camiseta", "tunica", "piel", "cuero", "acolchada", "capucha", "gorro duro", "brazalete", "zapatillas", "botas", "pantalones", "grebas" }));
    fijar_sufijo("encantad$a");
    fijar_sufijo_coloreado("Encantad$a");
    
    set_long("Anheladas por cualquier aventurero que se precie, las armaduras encantadas como $este $tipo "
    "han sido imbuídas mediante magia arcana con sortilegios que las hacen más ligeras, resistentes "
    "y protectivas en el campo de batalla. Su valor y utilidad son, por lo tanto, mucho mayores que el de una "
    "armadura común. ¡No vayas de aventura sin ellas!\n");

    fijar_encantamiento(3);
}

