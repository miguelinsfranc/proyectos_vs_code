// Vhurkul 17.02.2004
// Amuleto del Nigromante
// Satyr 16.12.2014
/*
    Tipo: Armadura
    Base: Amuleto
    Nivel: 6
    Efectos:
    - Anima un cuerpo y protege al usuario
*/

inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("amuleto");
    set_name("amuleto nigromante");
    set_short("Amuleto del %^BOLD%^BLACK%^Nigromante%^RESET%^");
    add_alias(({"amuleto","nigromante"}));
    set_main_plural("Amuletos del %^BOLD%^BLACK%^Nigromante%^RESET%^");
    add_plural(({"amuletos","nigromantes"}));
    set_long(
        "Este amuleto se compone por una pequeña cabeza momificada y reducida "
        "que cuelga gracias a un pesado cordón metálico. "
        "El halo de muerte que concentra es evidente incluso para los seres "
        "más torpes e idiotas. \n"
        /*"Tan solo "
        "algunos reúnen las agallas necesarias para hacer uso de su impío "
        "poder, que se activa haciendo oscilar "
        "el amuleto sobre un cadáver reciente mientras se pronuncian arcanas "
        "palabras de poder.\n"*/
    );
        
    fijar_material(3);
    fijar_encantamiento(16);
    add_static_property("tipo", "hechicero");
}
/*
void init()
{
    ::init();
    add_action("osci","oscilar");
}

int osci(string tx)
{
    mixed c;

    if (!tx)
        return 0;
    if (tx!="amuleto sobre cuerpo")
        return notify_fail("Uso: oscilar amuleto sobre cuerpo\n");
    if (query_time_remaining("recargando"))
        return notify_fail("El amuleto aún no ha recuperado su poder.\n");
    if (!sizeof(find_match("cuerpo", ENV(TP))))
        return notify_fail("No hay ningún cuerpo por aquí.\n");
    if(TP->dame_manos_libres()<1)
        return notify_fail("Necesitas al menos una mano libre para hacer oscilar el amuleto.\n");

    add_timed_property("recargando", 1, 18000); // Dos días reales (Baelair Ene'2014 - 5 horas reales)
    tell_object(TP, "Comienzas a oscilar la cabeza momificada sobre el cuerpo y un hechizo arcano brota de tu garganta escapando a tu control.\n");
    tell_accion(ENV(TP), TP->query_short()+" hace oscilar la cabeza momificada de su amuleto y un hechizo arcano brota de su garganta.\n", "", ({ TP }), TP);
    TP->do_say("arkmun... arive... etcon... ¡IMUS!", -1);
    c = find_match("cuerpo", ENV(TP));

    if (!c || !sizeof(c)) {
        tell_object(TP, "¡¡¡Algo ha ido mal, el muerto-viviente ha escapado a tu control!!!\n");
        return 1;
    }
    if (c = c->raise_me())
    {
        if (arrayp(c)) {
            c = c[0];
        }

        c->set_aggressive(0);
        this_player()->fijar_protector(c);
        this_player()->nuevo_seguidor(c);
        c->set_join_fights(0);
        c->anyadir_amigo(this_player());
        c->set_move_after(0, 0);
        c->fijar_invocador(this_player());
    }
    return 1;
}

*/