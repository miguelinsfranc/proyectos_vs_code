// Dunkelheit 02-06-2011
inherit "/obj/multiarmadura.c";

void setup()
{
    fijar_nombre_generico("Accesorio Encantado");
    fijar_tipos_validos(({ "capa", "manto", "amuleto", "collar", "cinturon", "pendiente", "anilla", "bozal" }));
    fijar_sufijo("encantad$a");
    fijar_sufijo_coloreado("Encantad$a");
    
    set_long("Anheladas por cualquier aventurero que se precie, los accesorios encantados como $este $tipo "
    "han sido imbuídos mediante magia arcana con sortilegios que otorgan a su portador mayor facilidad "
    "de movimientos y protección. Su valor y utilidad son, por lo tanto, mucho mayores que el de cualquier "
    "accesorio común. ¡No vayas de aventura sin ellos!\n");

    fijar_encantamiento(3);
}

