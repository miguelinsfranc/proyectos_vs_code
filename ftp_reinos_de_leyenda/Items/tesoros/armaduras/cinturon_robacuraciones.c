// Satyr xx.yy2012
// Satyr xx.yy.2014 -- Reduzco lock, aumento duración, quito malus a BO y le añado el concepto de "refrescar" la duración
// Satyr 26.02.2017 -- Reduzco duración, lo hago disipable.
#define EXTENSION 15
//#define DURACION 90
#define DURACION 50
#define BLOQUEO 300

#define SHADOW "/baseobs/tesoros/armaduras/cinturon_robacuraciones_sh.c"
inherit "/obj/armadura";
void setup()
{
	fijar_armadura_base("cinturon");
	set_name("cinturon");
	set_short("%^CYAN%^BOLD%^C%^WHITE%^inturón %^CYAN%^R%^WHITE%^obacuraciones%^RESET%^");
	set_main_plural("%^CYAN%^BOLD%^C%^WHITE%^inturones %^CYAN%^R%^WHITE%^obacuraciones%^RESET%^");
	add_alias(({"cinturon", "robacuraciones", "cinturon robacuraciones", "cinturón robacuraciones"}));
	add_plural(({"cinturones", "robacuraciones", "cinturones robacuraciones"}));
	set_long(
		"Un cinturón de cuero blanco que tiene una aquamarina en su hebilla. "
		"Es brillante, muy limpio y tiene filigranas que recorren su superficie "
		"por la parte interior. Fijándote detenidamente eres capaz de vislumbrar "
		"la silueta de un humanoide con cola de pescado en la aquamarina, aunque "
		"es tan forzado, que crees que pueda ser una casualidad.\n"
	);
	set_read_mess(
		"%^BLUE%^B%^RESET%^lurgurdur barg,\n"
		"%^BLUE%^a%^RESET%^biriblilurugurg,\n"
		"%^BLUE%^b%^RESET%^ialr urugufl\n"
		"%^BLUE%^u%^RESET%^iqria rooiria\n"
		"%^BLUE%^b%^RESET%^qiwjei jijiahr\n"
		"%^BLUE%^l%^RESET%^u lu ururu ugu\n"
		"%^BLUE%^u%^RESET%^qor qhqrhqohr\n",
		"aquan",
		0
	);
	
	fijar_encantamiento(10);	
	add_static_property("ts-mental", -3);
	add_static_property("agilidad" , -3);
	add_static_property("nivel_minimo",20);
}
string descripcion_encantamiento()
{
	return "Un misterioso poder te permitirá cambiar el objetivo de los hechizos de 'curar heridas' hacia ti "
		   "cuando estés bajo sus efectos. El efecto durará " + DURACION + " segundos, por lo que deberás "
           "calcular cuando tus objetivos van a curarse. Además, cada vez que robes un hechizo de curación, "
           "la duración del efecto se extendrá en " + EXTENSION + " segundos. El cinturón tiene un bloqueo de "
           + pretty_time(BLOQUEO) + "."
    ;
}
string descripcion_juzgar(object b)
{
    return descripcion_encantamiento();
}
void objeto_desequipado()
{	
	if (environment())
		environment()->destruir_sombra_cinturon_robacuraciones(1);
}
// Cada vez que 'robe' un hechizo ganará duración la shadow igual a lo que devuelva esta fun
int dame_duracion_extra()
{
    return EXTENSION;
}
int robaroba()
{
	object sh;
	
	if (!query_in_use())
		return notify_fail("No estás usando " + query_short() + ", por lo que no puedes activar su poder.\n");
	else if (query_timed_property("agotado") || TP->query_timed_property("bloqueo-robacuraciones"))
		return notify_fail("El poder de " + query_short() + " se ha agotado temporalmente.\n");
	else if (TP->query_timed_property("comm_lockout") || TP->query_property("formulando"))
		return notify_fail("No estás lo suficientemente calmado como para usar este poder.\n");
	else if (!sh=clone_object(SHADOW))
		return notify_fail("Un error impide activar el poder de " + query_short() + "\n");
	else
	{
		TP->add_timed_property("bloqueo-robacuraciones", 1, BLOQUEO);
		add_timed_property("agotado", 1, BLOQUEO);
		sh->setup_sombra(TP, TO, DURACION);
        
        // Duración alta porque la duración la controla la shadow (puede subir si absorbe hechizos)
		TP->nuevo_efecto("cinturon robacuraciones", 999, (: $(TP)->destruir_sombra_cinturon_robacuraciones(1) :));
        
		tell_object(TP, "Tocas la aquamarina de tu " + query_short() + " mientras pronuncias la palabra de poder. "
						"Un aura mágica te envuelve durante unos segundos.\n");
		tell_accion(environment(TP), TP->query_cap_name() + " toca la aquamarina de su " + query_short() + " mientras "
						"pronuncia la palabra de mando.\n", "", ({TP}), TP);
		return 1;
	}
}
void init()
{
	::init();
	add_action("robaroba", "babublu");
}
