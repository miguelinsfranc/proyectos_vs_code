// Satyr 2012
// Zilar 2013:  -bajando un poco el item, solo podra contener 10 bayas ( cada
// baya pesa 8, fijando peso_max a 80)
//                          -subido el set_heart_beat para que se llame cada mas
//                          tiempo
// Satyr 13.04.2015 - Retoco los alias. Y comentadme cambios de balance como el
// anterior...
#define BAYA "/hechizos/items/bayas_buenas_ob"
inherit "/obj/equipo_contenedor";
void setup()
{
    fijar_armadura_base("brazalete");
    set_name("brazalete");
    set_short("%^GREEN%^Brazalete de las Buenas Bayas%^RESET%^");
    set_main_plural("%^GREEN%^Brazaletes de las Buenas Bayas%^RESET%^");
    add_alias(({"buenas bayas", "brazalete", "brazalete de las buenas bayas"}));
    add_plural(({"brazaletes", "brazaletes de las buenas bayas"}));
    set_long("Un brazal de cuero sobre el que un druida ha formulado un "
             "poderoso encantamiento "
             "que permite que un sinfín de pequeños zarcillos y hojas crezcan "
             "en su superficie. "
             "Las plantas del brazal están en constante -aunque lento- "
             "movimiento, buscando "
             "pequeños rayos de luz que puedan aprovechar para seguir "
             "creciendo. Al meter los "
             "dedos entre las plantas, llegas a tocar el cuero del brazal; "
             "¡está lleno de tierra "
             "húmeda! El brazal se creó como un proyecto que pretendía "
             "eliminar el hambre de las "
             "zonas más pobres y con menos recursos, pero su fabricación es "
             "complicada y dió lugar "
             "a una serie de problemas y mutaciones que no gustaron a varios "
             "espíritus del bosque "
             "(principalmente los sátiros), por lo que el proyecto se perdió "
             "para siempre. Sin embargo, "
             "aún hay muchos ejemplares de este brazal que han sobrevivido a "
             "la purga Sátira.\n");

    fijar_alineamiento(12);
    add_static_property("veneno", 3);
    add_static_property("ts-veneno", 3);

    set_heart_beat(60);

    fijar_reduccion_de_peso(0);
    fijar_peso_max(80);
    fijar_valor_venta(10000);
}
string descripcion_encantamiento()
{
    return "Un hechizo permite que nazcan bayas en este brazalete cuando está "
           "usándose.";
}
void heart_beat()
{
    if (query_in_use() && environment() && living(environment())) {
        if (environment(environment()) &&
                environment(environment())->dame_bosque() ||
            !random(3)) {
            object b = clone_object(BAYA);

            if (b) {
                if (!b->move(TO)) {
                    tell_object(
                        environment(),
                        "Un pequeño capullo nace en tu " + query_short() +
                            ", que comienza a hincharse "
                            "hasta que estalla, dejando caer " +
                            b->query_short() + ".\n");

                    b->move(TO);
                    b->add_timed_property("potencia", 1, 300);
                } else
                    destruct(b);
            }
        }
    }
}
int test_add(object b, int no_cogible)
{
    if (!no_cogible && base_name(b) == BAYA)
        return ::test_add(b, no_cogible);

    return notify_fail("No puedes introducir eso en " + query_short() + ".\n");
}
