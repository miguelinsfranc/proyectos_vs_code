// Satyr 2007
// Yelmo del vigía
// Yelmo +12, muy ligero, no estorba para sigilar

inherit "/obj/armadura";

int dame_estorbo() { return 0; }

string dame_nombre_material()
{
    return "Hierro ligero perfectamente forjado";
}
string descripcion_encantamiento()
{
    return "La magia y el increíble forjado de este yelmo hacen que no cause estorbo alguno a su portador.";
}
void setup()
{
    fijar_armadura_base("yelmo");
    set_name("yelmo");
    set_short("Yelmo del vigía");
    set_main_plural("Yelmos del vigía");
    add_alias(({"yelmo","vigia","vigía","yelmo del vigia","yelmo del vigía"}));
    add_plural(({"yelmos","vigía","vigia","yelmos del vigia","yelmos del vigía"}));
    set_long(
        "\tUn magnífico yelmo ligero creado por las manos de algún herrero increíblemente habilidoso. "
        "Protege la parte superior y posterior de la cabeza, dejando la parte delantera libre de cualquier "
        "estorbo, otorgando así una mayor visibilidad y maniobrabilidad a su portador a cambio de un poco de "
        "protección. "
        "Este tipo de yelmos es habitualmente usado por la caballería ligera, así como por soldados o cazadores "
        "en misiones de reconocimiento. "
        "Su poca habitual ligereza viene por una parte de su increíble forjado y por otra del metal encantado "
        "del que esta construido, "
        "pues su simple tacto hace que el vello se te erice casi instantáneamente. "
        "Desconoces a quien pertenecía este yelmo, pero sin duda, lo estará echando en falta.\n"
    );

    fijar_proteccion(dame_proteccion() + 5);
    fijar_encantamiento(20);
    fijar_valor(5*500);
    fijar_peso(900);        // Yelmo normal: 1400
    ajustar_BE(3);
}
