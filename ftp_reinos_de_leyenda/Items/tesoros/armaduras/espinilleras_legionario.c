// Satyr 14/05/2018
inherit "/obj/armadura";
void setup()
{
    fijar_armadura_base("grebas de malla");
    set_name("espinillera");
    set_short("%^BOLD%^WHITE%^Espinilleras de Legionario%^RESET%^");
    set_main_plural(
        "pares de %^BOLD%^WHITE%^Espinilleras de Legionario%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Dos grebas de firme cuero que están reforzadas por varias telas "
        "de malla colocadas una encima de otra. Son ligeras y cómodas, "
        "además de estar reforzadas en la rodilla con una placa de acero "
        "que está rematada en tres afilados pinchos -¡perfectos para "
        "un rodillazo memorable!-.\n\n"

        "  Aunque esta pieza está bien labrada, originalmente las espinilleras "
        "de legionario eran una armadura confeccionada con los restos de las "
        "que vestían los primeros gladiadores de Galador en salir a la "
        "arena.\n\n"

        "  Su diseño resultó efectivo, pues son ligeras, a la par que "
        "resistentes y duraderas, ¡y esos pinchos!, ¡el público adoraba los "
        "pinchos!\n");

    fijar_encantamiento(10);
    fijar_BO(10);
    add_static_property("daño", 10);
}
