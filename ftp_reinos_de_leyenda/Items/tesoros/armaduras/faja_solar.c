// Vhurkul 17.02.2004
// Faja Solar

/*
	Tipo: Armadura
	Base: Cinturón
	Nivel: 1
	Efectos:
	- Luz: 20
*/

inherit "/obj/armadura";

void setup() {
	fijar_armadura_base("cinturon");
	set_name("faja solar");
	set_short("Faja %^YELLOW%^Solar%^RESET%^");
	add_alias(({"faja","solar"}));
	set_main_plural("Fajas %^YELLOW%^Solares%^RESET%^");
	add_plural(({"fajas","solares"}));
	set_long("Famosas en su tiempo entre los ciudadanos más pudientes de la Montaña de Vapor, las fajas solares "
		"fueron toda una moda, y se fabricaron múltiples diseños a cual mas atrevido. Se vendían en las tiendas "
		"con el eslogán 'Tenga una figura resplandeciente'. El modo en que se fabricaban no está muy claro, pero "
		"aún después de tanto tiempo estas prendas siguen irradiando un leve fulgor que se asemeja a la luz "
		"del sol.\n");
	fijar_tamanyo_armadura(2); // Gnomo
	fijar_luz(20);
    fijar_genero(2);
	}

