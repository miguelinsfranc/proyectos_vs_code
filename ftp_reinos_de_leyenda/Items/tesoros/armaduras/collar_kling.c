// Vhurkul 17.02.2004
// Collar de Madera Kling

/*
	Tipo: Armadura
	Base: Collar
	Nivel: 2
	Efectos:
	- Resistencia a fuego -5%
	- Resistencia a magico +10%

Eckol Ene20. Le quit oel random al la resi magica y o dejo en +5 (como salia en la armeria.
*/

inherit "/obj/armadura";

void setup() {
	fijar_armadura_base("collar");
	set_name("collar kling");
	set_short("Collar de %^ORANGE%^Madera%^RESET%^ %^BOLD%^RED%^Kling%^RESET%^");
	add_alias(({"collar","madera","kling"}));
	set_main_plural("Collares de %^ORANGE%^Madera%^RESET%^ %^BOLD%^RED%^Kling%^RESET%^");
	add_alias(({"collares","maderas","klings"}));
	set_long("Un sencillo collar de cuentas de madera, todas del mismo tamaño y peso, colocadas adecuada y "
		"ordenadamente. Al fijarte mejor descubres que la madera de que está hecho no es tan sencilla, pues se "
		"trata de la valiosa Madera Kling, famosa por sus propiedades mágicas y utilizada comúnmente por los "
		"magos rúnicos.\n");
	add_static_property("fuego",-5);
          add_static_property("magico",5);
	fijar_material(1); // Madera
       fijar_encantamiento(2);
	}

string descripcion_encantamiento() {
	return ::descripcion_encantamiento()+"\nDesprende una sensación de equilibrio muy fuerte.\n";
	}
