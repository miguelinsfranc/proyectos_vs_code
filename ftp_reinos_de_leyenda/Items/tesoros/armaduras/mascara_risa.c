// Vhurkul 17.02.2004
// Máscara de la Risa

/*
	Tipo: Armadura
	Base: Capucha
	Nivel: 5
	Efectos:
	- Protección de armadura: 0
	- Carisma -1
	- Cuando alguien mira al portador debe superar TS contra hechizos o sufrirá los efectos 
	  de un hechizo de "Risa Incontrolable de Shasha"
*/
#define SHADOW "/hechizos/shadows/ridiculo_sh.c"
inherit "/obj/armadura";
void setup() 
{
    fijar_armadura_base("capucha");
    set_name("mascara risa");
	set_short("Máscara de la Risa");
	add_alias(({"mascara", "máscara", "risa"}));
	add_plural(({"mascaras","máscaras","risas"}));
	set_long(
        " Algunos eruditos, tras haber estudiado incluso los textos más triviales conocidos sobre "
        "la ciudad de Ermite, creen que esta máscara era usada por los bufones de la corte para "
        "lograr un efecto que nunca será superado. El diseño de la máscara es tan absurdo que da "
        "un aspecto completamente ridículo a quien la lleva, motivo por el cual otros piensan "
		"que fue fabricada por seguidores de Osucaru.\n"
    );
    
    add_static_property("car", -1);
	fijar_encantamiento(30);
	fijar_proteccion(0);
	fijar_material(1);
}
int set_in_use(int i) 
{
    int exito = ::set_in_use(i);
    
	if(i && exito && !environment()->dame_ridiculo()) {
        object shadow = clone_object(SHADOW);
		shadow->setup_sombra(environment());
		return i;
    }
    if (!i && !exito) {
        environment()->destruir_ridiculo();
    }
	
	return i;
}	
