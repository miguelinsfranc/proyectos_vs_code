/*
	Descrita por Vhurkul el 17-02-2004 y programado por Dunkelheit el 31-12-2009

	Tipo: Armadura
	Base: Capa
	Nivel: 9
	Efectos:
	- Libre Movimiento
    
    Satyr 03/08/2010 -  lo convierte en formulador, porque acción libre perpetuo es muy gordo
    Satyr 25.06.2015 - +20 y +3 evasion
*/

inherit "/obj/armadura";
inherit "/obj/magia/objeto_formulador.c";

void setup()
{
	fijar_armadura_base("capa");
	set_name("capa");
	set_short("%^GREEN%^Capa %^YELLOW%^Fäernowen%^RESET%^");
	set_main_plural("%^GREEN%^Capas %^YELLOW%^Fäernowen%^RESET%^");
	add_alias("faernowen");
	add_plural(({"capas", "faernowens"}));
	fijar_genero(2);
	set_long(
		"Pocos pueden decir que conocen Eirea tan bien como lo hizo Fäernowen, la famosa "
		"exploradora elfa de la que se dice que "
		"llegó a lugares donde nunca llegará nadie. Según cuentan los bardos, no había nada capaz "
		"de interponerse en su camino, "
		"ni la fría nieve, las traicioneras arenas o los lodazales más empantanados, ni las "
		"pegajosas telas de las arañas "
		"gigantes de la Antípoda, y ni siquiera el ataque de retención de criaturas sobrenaturales "
		"como los necrarios o los "
		"más poderosos sacerdotes. Su capacidad para moverse incluso en el terreno más accidentado "
		"era mítica, pero no le ayudó "
		"a escapar la fatídica noche que su aldea fue arrasada por los drow. Puedes resguardarte "
        "tras ella.\n"
	);
	
	//add_static_property("libre-accion", 1);
    fijar_recuperacion(60 * 16);
    fijar_esfera(3);
    fijar_nivel(30);
    
    add_static_property("evasion", 3);
    fijar_encantamiento(20);
	add_static_property("nivel_minimo",29);
}
void init()
{
    ::init();
    add_action("formulacion", ({"resguardarse", "resguardarte"}));
}
int formulacion(string tx)
{
    return formular("accion libre", environment()->query_name(), 1);
    //this_player()->do_say(this_player()->query_notify_fail_msg(), 0);
    //return 1;
}
void iniciar_formulacion(string nombre_hechizo,object portador)
{
    tell_object(portador, "Te resguardas bajo tu "+query_short()+" y esta comienza a chispear.\n");
    tell_accion(
        environment(portador), 
        portador->query_short()+" se resguarda bajo su " + query_short() + " y esta comienza a chispear.\n",
        "", 
        ({portador}), 
        portador
    );
}
void objeto_desequipado() {
    if ( environment() )
            environment()->quitar_efecto("terminar libre accion");
}
