/*
 * Recreación del legendario y utilísimo cinturón de RL1
 *
 * Dunkelheit 18-05-2011
 * Satyr      12.12.2014 -- Bajo el valor
 * Satyr      28.07.2015 -- +2 de con
 */

inherit "/obj/armadura";

string descripcion_juzgar(object quien)
{
    if (quien && quien->dame_carac("sab") > 17) {
        return "Tu elevada sabiduría te permite reconocer este objeto como una "
        "de las mayores reliquias de la Era 3ª, capaz de aumentar la constitución "
        "de hasta el más enclenque aventurero.";
    }
    return 0;
}

void setup()
{
    fijar_armadura_base("cinturon");
    set_name("cinturon");
    set_short("Cinturón de Cirdan");
    set_main_plural("Cinturones de Cirdan");
    add_alias(({"cinturón", "cirdan"}));
    add_plural(({"cinturones", "cirdans", "cirdanes"}));
    
    set_long("Bautizado como el legendario guerrero de la Era 1ª, el Cinturón de Cirdan "
    "es una valiosísima reliquia con cientos de años de historia. Todos los ejemplares que "
    "existen hoy en día han pasado por manos de reyes, emperadores, héroes de la batalla y "
    "magos de renombre. Este magnífico cinturón de cuero dorado presenta una hebilla metálica "
    "de plata con una corona de laureles grabada en ella: el símbolo del triunfo.\n");
    
    add_static_property("con", 2);
    fijar_encantamiento(15);
    
    fijar_valor(5000); // 200 platinos

	add_static_property("nivel_minimo", 30);
}

