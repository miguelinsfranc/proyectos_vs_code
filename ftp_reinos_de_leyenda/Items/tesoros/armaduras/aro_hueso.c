// Vhurkul 17.02.2004
// Aro de hueso

/*
	Tipo: Armadura
	Base: Pendiente
	Nivel: 1
	Efectos:
	- Nada. Pero tiene colorines. ¿A que mola?
		No mola no :( -- Dunkelheit
*/

inherit "/obj/armadura";

void setup() {
	fijar_armadura_base("pendiente");
	set_name("aro hueso");
	set_short("Aro de %^BOLD%^Hueso%^RESET%^");
	add_alias(({"aro","hueso"}));
	set_main_plural("Aros de %^BOLD%^Hueso%^RESET%^");
	add_plural(({"aros","huesos"}));
	set_long("Los aros de hueso son un popular adorno entre las hembras orcas, así como de algunas poblaciones "
		"humanas primitivas del Este de Dalaensar. A pesar de tener un diseño bastante tosco, tiene complicación "
		"tallar un hueso para hacer con él un aro tan perfecto. Además es de marfíl.\n");
	fijar_material(7);
	fijar_tamanyo_armadura(0);
	ajustar_BE(2);
	}
