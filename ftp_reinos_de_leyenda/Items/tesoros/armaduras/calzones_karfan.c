// Vhurkul 17.02.2004
// Calzones de Karfán

/*
	Tipo: Armadura
	Base: Pantalón
	Nivel: 2
	Efectos:
	- TS Reflejos mejorada
*/

inherit "/obj/armadura";

void setup() {
	fijar_armadura_base("pantalones");
	set_name("calzones karfan");
	set_short("%^BOLD%^GREEN%^Calzones%^RESET%^GREEN%^ de %^BOLD%^RED%^Karfán%^RESET%^");
	add_alias(({"calzones","karfan","karfán"}));
	set_main_plural("%^BOLD%^GREEN%^Calzones%^RESET%^GREEN%^ de %^BOLD%^RED%^Karfán%^RESET%^");
	add_alias(({"calzones","karfanes","karfáns","karfans"}));
	set_long("No se sabe mucho de Karfán el Bárbaro, ya que los hombres-lagarto no suelen ser muy locuaces. Al "
		"parecer este salvaje gnoll llegó de algún modo a las Junglas de Zulk, por donde se columpiaba por los "
		"árboles de liana en liana, vestido únicamente con unos raídos calzones. Los lagartos, algo incomodados "
		"por su costumbre de ponerse a gritar estruendosamente a altas horas de la noche, le dieron caza. "
		"De cualquier modo, hay que decir en su favor que era increíblemente ágil para tratarse de un Gnoll.\n");
	add_static_property("ts-agilidad",10);
	}


