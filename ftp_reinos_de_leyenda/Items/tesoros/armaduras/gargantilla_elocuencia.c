// Satyr 2012
inherit "/obj/armadura";
void setup()
{
	fijar_armadura_base("collar");
	set_name("gargantilla");
	set_short("%^ORANGE%^Gargantilla de la Elocuencia%^RESET%^");
	set_main_plural("%^ORANGE%^Gargantillas de la Elocuencia%^RESET%^");
	
	add_alias(({"gargantilla", "elocuencia", "gargantilla de la elocuencia"}));
	add_plural(({"gargantillas", "elocuencia", "gargantillas de la elocuencia"}));
	
	set_long(
		"Una gargantilla hecha de cuentas de cuarzo enhebradas en un fino cordón hecho de pelo de camello. "
		"La luz que llega a las piedras se refleja en un tono amarillento apagado. El interior de los "
		"cristales, de aspecto nublado y sucio, es feo y poco atractivo; motivo por el que no se hacen "
		"joyas con un material tan poco elegante. Una gran piedra de cuarzo ha sido tallada en su parte "
		"central, tomando la forma de una extraña cara con enormes pómulos y una boca abierta. Su expresión "
		"es... ¡vaya!, ¡la cara acaba de hablar, delante de tus ojos!\n"
	);

	fijar_encantamiento(10);	
	fijar_material(5);
	
	set_heart_beat(120);
}
int formular_grito_poderoso()
{
	if (!query_in_use())
		return notify_fail("No estás usando " + query_short() + ", por lo que no puedes activar su poder.\n");
	else if (query_timed_property("agotado") || TP->query_timed_property("bloqueo-kayathus"))
		return notify_fail("El poder de " + query_short() + " se ha agotado temporalmente.\n");
	else
	{
		TP->add_timed_property("bloqueo-kayathus", 1, 600);
		add_timed_property("agotado", 1, 600);
		
		tell_object(TP, "Elevas tu " + query_short() + " mientras pronuncias las palabras de mando y de repente, "
			"el espíritu que posee en su interior lanza un grito elevadísimo, destructor, capaz de romper cualquier "
			"foco de silencio y de hacer estallar los tímpanos de los presentes.\n"
		);
		
		tell_accion(environment(TP),
			"¡" + TP->query_cap_name() + " eleva su " + query_short() + " mientras pronuncia las palabras de mando y "
			"su gargantilla comienza a gritar más y más fuerte hasta que el volumen es tan elevado que cualquier "
			"foco de silencio se rompe y tus tímpanos empiezan a doler!",
			"",
			({TP}),
			TP
		);
		
		TO->habla("%^BOLD%^RED%^¡¡DEJADME HABLAR!!, ¡¡DEJADME HABLAAARRRR!!%^RESET%^");
		environment(TP)->destruir_silencio();
		
		foreach(object ob in filter(all_inventory(environment(TP)), (:$1 && living($1):)))
		{
			if (!ob->dame_ts("artefacto", 0, "gargantilla de la resonancia", "gargantilla de la resonancia"))
			{
				ob->nueva_incapacidad("sordera", "incapacidades", 0, 100, 0);
			}
		}
		
		return 1;
	}
}
void init()
{
	::init();
	add_action("formular_grito_poderoso", "kayathus");
}
string dame_frase()
{
	// meted aquí cualquier subnormalidad que se os ocurra
	return element_of(({
		"EL INGREDIENTE SECRETO DE LA CERVEZA ENANA SON PELOS DE BARBA.",
		"LAS ENANAS NO TIENEN PELO AHÍ ABAJO. EN LA CARA, ABAJO, EN EL MENTÓN, VAMOS.",
		"LA CARNE, A LA BOLA DE FUEGO, SABE MEJOR.",
		"LOS ORCOS INVENTARON UNA PODEROSA ARMA DE ASEDIO CAPAZ DE DESTRUIR CIUDADES. "
		"DEJARON DE FABRICARLA PORQUE NO SABÍAN CONSTRUIR SU MUNICIÓN.",
		"¿CUANTOS ELFOS HACEN FALTA PARA CAMBIAR EL ACEITE DE UNA LÁMPARA?: 36, 1 PARA "
		"CAMBIAR EL ACEITE Y 35 PARA QUEJARSE DE LA BANALIDAD Y SUPERFICIALIDAD DE SEMEJANTE ACTO.",
		"LOS BARDOS NO ROBAN A NADIE. EMBORRACHAN A SUS VÍCTIMAS Y COGEN COSAS PRESTADAS. NO ES ROBAR "
		"SI DAS VINO A CAMBIO.",
		"LOS ORCOS DISFRUTAN MUCHO CON EL TRUEQUE. SUS TROCADORES SUELEN INTERCAMBIAR HOSTIAS POR OTRO "
		"TIPO DISTINTO DE HOSTIAS.",
		"EN EL AGUA NADIE PUEDE OIRTE GRITAR.",
		"LA GRAVEDAD NO EXISTE, ES UN INVENTO DE LOS PADRES.",
		"TAKOME NO ES UNA CIUDAD MUY BONITA.",
		"LOS HALFLINGS SUFREN MUCHO CUANDO NO ALMUERZAN 4 VECES.",
		"NO ES NADA RARO CONFUNDIR A LOS ELFOS CON ELFAS.",
		"LOS ORCOS NO SE LAVAN PORQUE LES DA VERGUENZA QUE OTROS "
		"ORCOS PIENSEN QUE SON ORCOSEXUALES.",
		"EN DENDRA, CUANDO NO ENTIENDEN ALGO, LO QUEMAN.",
		"DESDE AQUÍ NO PUEDO VER EL CIELO.",
		"ME GUSTA EL ESPACIO. ESPERO IR ALGÚN DÍA AL ESPACIO.",
		"EL OCEANO ME GUSTA, PERO PREFIERO EL ESPACIO. ME GUSTA EL ESPACIO.",
		"¡ARRRRRRRRRRRRRGH!",
		"LOS ANESTESISTAS ORCOS SON MUY APRECIADOS POR SUS HABILIDADES.",
		"LOS SEMI-ORCOS CONOCEN UN HECHIZO QUE SE LLAMA 'GOLPE MENTAL' Y "
		"CONSISTE EN GOLPEAR LA CABEZA DE SU ENEMIGO.",
		"ERALIE ES UNA MUJER, DIGAN LO QUE DIGAN.",
		"SELDAR ES CALVO. ¿DÓNDE SE VIÓ UN DIOS CALVO?",
		"EN VELEIRON SOLO SABEN BEBER VINO Y BAILAR BORRACHOS. NO SE "
		"DIFERENCIAN TANTO DE LOS ENANOS.",
		"LAS DIFERENCIAS ENTRE ENANOS Y ORCOS NACIERON DE UNA DISCUSIÓN "
		"DE MATEMÁTICAS.",
		"LAS HEMBRAS DE LOS HOMBRES LAGARTO SE LLAMAN MUJER-HOMBRE-LAGARTO.",
		"EL BACON ES UNA VACA GRANDE.",
		"ORGOS Y OGROS SE DIFERENCIAN EN QUE UNOS SON FEOS Y LOS OTROS TONTOS.",
		"NO LE DES CHOCOLATE A UN KOBOLD O SE QUEDARÁ CIEGO.",
		"NO ABANDONES A TU GNOLL, EL NUNCA LO HARÍA.",
		"LOS SEMI-ORCOS EXISTEN, A PESAR DE LO QUE DIGAN LOS RUMORES.",
		"LA PALABRA DE PODER ES K - A - Y - A - T - H - U - S."
	}));
}
varargs void habla(string t)
{
	if (environment())
	{
		object sala, portador;
		string tx = t ? t : dame_frase();
		
		if (living(environment()))
			portador = environment();
		else if (environment()->query_room())
			sala     = environment();
		else
			return;
			
		if (!sala && portador && environment(portador))
			sala = environment(portador);

		if (portador)
			tell_object(portador, "Tu " + query_short() + " grita: " + tx + "\n");
			
		if (sala)
			tell_room(sala, query_short() + " grita: " + tx + "\n", portador ? portador : 0);
	}
}
string descripcion_encantamiento()
{
	return "Un espíritu chillón habita en esta gargantilla como castigo por sus actos en vida. "
			"Una palabra de poder te permitirá usar su elocuencia para algo útil.";
}
void heart_beat()
{
	habla();
}