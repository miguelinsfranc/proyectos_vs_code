// Satyr 23/05/2018 12:47
inherit "/obj/armadura";
int dame_estorbo()
{
    return 0;
}
void setup()
{
    fijar_armadura_base("botas de campaña");
    set_name("escarpes");
    set_short(
        "%^BOLD%^BLACK%^Escarpes de la "
        "%^NOBOLD%^ORANGE%^V%^GREEN%^i%^ORANGE%^b%^GREEN%^o%^ORANGE%^r%^"
        "GREEN%^a%^RESET%^");
    set_main_plural(
        "pares de %^BOLD%^BLACK%^Escarpes de la "
        "%^NOBOLD%^ORANGE%^V%^GREEN%^i%^ORANGE%^b%^GREEN%^o%^"
        "ORANGE%^r%^GREEN%^a%^RESET%^");
    generar_alias_y_plurales();
    fijar_genero(-1);

    set_long(
        "Un par de botas de campaña concebidas para otorgar protección "
        "a aquellos guerreros que optan por un estilo de lucha más móvil "
        "del que practican los que prefieren usar armaduras de placas.\n\n"

        "  Están hechas de finas láminas de Mithril recubiertas de una "
        "malla del mismo material, que resulta engañosamente resistente. Su "
        "empeine "
        "se prolonga para terminar en un afilado punzón cuya cruel finalidad "
        "es la de ser usada por un jinete para herir a monturas enemigas.\n\n"

        "  El metal ha sido cuidadosamente mimado con tratamientos térmicos "
        "que le han conferido un singular patrón de colores que le hacen "
        "parecer la piel de una víbora. Patrón que, sin duda, no es casual, "
        "pues la magia que encierran estas botas de campaña confieren "
        "a su portador la misma letalidad y gracilidad que una de estas "
        "criaturas.\n");

    ajustar_BE(5);
    ajustar_BO(5);
    fijar_encantamiento(10);
    add_static_property("veneno", 5);
    add_static_property("frio", -5);
    add_static_property("ts-veneno", 5);
    add_static_property("ts-agilidad", 10);
}
