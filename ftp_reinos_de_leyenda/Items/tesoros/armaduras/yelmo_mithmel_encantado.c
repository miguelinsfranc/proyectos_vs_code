// Satyr 21/07/2022 02:24
// https://www.reinosdeleyenda.es/foro/ver-tema/concurso-comunidad-diseno-objetos-de-gloria-temporada-35/
#include <material.h>
#include <baseobs_path.h>
inherit "/obj/armadura";

#define PORCENTAJE_ESGRIMIR_DURACION 25
#define PORCENTAJE_ESGRIMIR_VIDA_MAX 25

void setup()
{
    fijar_armadura_base("yelmo");
    set_name("yelmo");
    set_short("Yelmo de %^CYAN%^BOLD%^M%^RESET%^ithmel Encantado");
    set_main_plural("Yelmos de %^CYAN%^BOLD%^M%^RESET%^ithmel Encantado");
    generar_alias_y_plurales();
    set_long(
        "Este enorme, pero estilizado Yelmo está realizado con una perfecta "
        "aleación de Mithmel con Mithril, lo que lo hace más ligero de lo "
        "habitual. La parte posterior de la cabeza se encuentra protegida por "
        "una brillante malla que cuelga hasta sus hombros. De la punta del "
        "yelmo sobresale una enorme pluma roja que se mantiene erguida, dando "
        "sensación de altura a su portador. La parte delantera se encuentra "
        "totalmente desprotegida en la cara, permitiendo al usuario ver todo "
        "con más claridad y respirar sin problema alguna, pero en su cuello "
        "puede verse como la malla trasera se arremolina evitando que "
        "cualquier cuchilla pueda realizar un corte indeseable.\n");

    fijar_encantamiento(30);
    add_static_property("evasion", 3);
    add_static_property("ts-agilidad", 10);
    fijar_conjuntos(({BTESOROS_CONJUNTOS + "atavio_esgrimista.c"}));
}
string descripcion_juzgar(object pj)
{
    return sprintf(
        "La habilidad 'esgrimir' recibirá las siguientes "
        "bonificaciones:\n\n" +
            " - +%d%% al daño total que podrá parar.\n"
            " - +%d%% a la duración de la habilidad.\n",
        PORCENTAJE_ESGRIMIR_VIDA_MAX,
        PORCENTAJE_ESGRIMIR_DURACION);
}
int dame_danyo_parado_esgrimir(object ejecutor, int danyo_maximo_parado_actual)
{
    int incremento =
        to_int(danyo_maximo_parado_actual * PORCENTAJE_ESGRIMIR_VIDA_MAX / 100);

    return danyo_maximo_parado_actual + incremento;
}
int dame_duracion_esgrimir(object ejecutor, int duracion_actual)
{
    int incremento =
        to_int(duracion_actual * PORCENTAJE_ESGRIMIR_DURACION / 100);

    return duracion_actual + incremento;
}
