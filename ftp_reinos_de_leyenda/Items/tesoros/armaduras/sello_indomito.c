// Satyr 11/05/2018
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("anillo");
    set_name("sello");
    set_short(
        "%^BLACK%^BOLD%^%^S%^NOBOLD%^ORANGE%^ello "
        "%^BLACK%^BOLD%^I%^NOBOLD%^ORANGE%^ndómito%^RESET%^");
    set_main_plural(
        "%^BLACK%^BOLD%^%^S%^NOBOLD%^ORANGE%^ellos "
        "%^BLACK%^BOLD%^I%^NOBOLD%^ORANGE%^ndómitos%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        "Un anillo cuyo aro de plomo sujeta un grueso chatón que está adornado "
        "con el relieve de un corazón. Mugre y polvo se han apoderado de los "
        "recovecos más inaccesibles de la pieza, extendiendo una ligera "
        "suciedad que oculta sus detalles más sutiles. No tiene mucho valor "
        "como abalorio, pero el mero hecho de "
        "tenerlo "
        "en la mano hace que tu pulso se estabilice.\n");

    fijar_BP(6);
    fijar_valor(30000);
}
