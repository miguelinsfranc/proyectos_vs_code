// Vhurkul 17.02.2004
// Botas de combate de Curgrim
/*
	Tipo: Armadura
	Base: Botas Guerra
	Nivel: 4
	Efectos:
	- Protección de armadura: 90
	- Furia (Bono al daño, penalización a la BE, BP, y BO, locura)

	Eckol 21Jun15: Pongo los bonos en el setup, no en el set_in_use, para que se muestren bien al juzgar, etc.
*/
#define SHADOW "/hechizos/shadows/locura_sh.c"
inherit "/obj/armadura";
void setup() 
{
    fijar_armadura_base("botas de guerra");
    set_name("botas curgrim");
    
	set_short("Botas de %^BOLD%^RED%^Combate%^RESET%^ de %^ORANGE%^Curgrim%^RESET%^");
    set_main_plural("Botas de %^BOLD%^RED%^Combate%^RESET%^ de %^ORANGE%^Curgrim%^RESET%^");
	add_alias(({"botas", "combate", "curgrim"}));
    add_plural(({"botas", "botas de combate", "botas de combate de curgrim"}));
	set_long(
        " Muchas son las sagas que hablan de Curgrim el enano, un poderoso guerrero entrenado e"
        " las técnicas de los Khazad Dum Uzbad. Famoso por sus tendencias agresivas y su irascible"
        " comportamiento, no tuvo muchos amigos, aunque muchos cayeron ante su hacha. Tras su muerte"
        " se supo que su eterna ira se debía a estas botas, que parecen sumir a su portador en una "
		" furia incontrolable.\n"
    );
    
	fijar_encantamiento(10);
    fijar_proteccion(dame_proteccion() + 5);

	ajustar_BE(-50);
	ajustar_BP(-30);
	add_static_property("daño",15);

}
int set_in_use(int i) 
{
    int exito = ::set_in_use(i);
    
	if(i && exito) {
        object shadow = clone_object(SHADOW);
		if(shadow)    
			shadow->setup_sombra(environment(), 0);
    }
    else {
        environment()->destruir_locura();
    }
    
    return exito;
}	
	
		

