// Armadura nivel 3
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("gran yelmo");
    set_name("yelmo");
    set_short("Yelmo Acorazado");
    set_main_plural("Yelmos Acorazados");
    add_alias(({"yelmo",   "acorazado",  "yelmo acorazado"}));
    add_plural(({"yelmos", "acorazados", "yelmo acorazado"}));
    set_long(
        " Un pesado yelmo que oculta el rostro de su portador. Su diseño es angulado y está"
        " ideado para absorber o desviar la potencia de cualquier golpe que llegue a su superficie."
        " Allí donde otros yelmos presentan vulnerabilidades, este ofrece una dura capa de acero que"
        " envuelve cabeza y cuello de su portador en un manto acorazado de los más resistentes"
        " conocidos en el mundo. Ni las armas penetrantes o aplastantes, que a menudo son un duro"
        " trago para los yelmos pesados, son capaces de encontrar un hueco en esta pieza de armadura.\n"
    );

    add_static_property("tipo", ({"luchador"}));
    fijar_proteccion(dame_proteccion() + 10);
    add_static_property("magico", -3);
    fijar_bono(2, "aplastante");
    fijar_bono(2, "penetrante");
    fijar_bono(2, "lacerante");
    fijar_bono(2, "cortante");

    fijar_peso(dame_peso() * 4);
}

void objeto_equipado(){
    fijar_luz(-5);
}

void objeto_desequipado(){
    fijar_luz(0);
}

string descripcion_juzgar(){
    return "Este objeto limita la visión al ser usado (penaliza la luz en -5).\n";
}
