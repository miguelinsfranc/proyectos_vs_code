// Dunkelheit 01-06-2011

inherit "/obj/armadura";

void setup()
{
    set_base_armour("cuero");
    set_name("hojaoscura");
    set_short("Cuero %^GREEN%^Hoja%^BLACK%^BOLD%^oscura%^RESET%^");
    add_alias("cuero");
    set_main_plural("Cueros %^GREEN%^Hoja%^BLACK%^BOLD%^oscura%^RESET%^");
    add_plural(({"cueros", "hojaoscuras"}));
    set_long("Las hojas oscuras y marchitas, bien por la sangre de inocentes vertida en los bosques, bien por "
    "el humo que a aquéllas asfixia, albergan la rabia y el deseo de venganza contra todos aquellos que han "
    "osado dañar a la madre naturaleza. Dicen que las armaduras de quienes perecieron en los bosques, sean "
    "víctimas o agresores, se impregnan de esta rabia natural y adquieren nuevos poderes defensivos. Este es "
    "uno de tantos petos de cuero que los saqueadores recogieron tras alguna desgracia acaecida bajo el "
    "inquietante cobijo de un bosque malherido.\n");
    fijar_encantamiento(2);
    add_static_property("ts-veneno", 1);
}
