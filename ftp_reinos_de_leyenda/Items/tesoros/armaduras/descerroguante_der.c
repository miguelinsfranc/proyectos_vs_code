/*
	Dunkelheit el 31-12-2009
	Guantes con bono a forzar
*/

inherit "/obj/armadura";

string dame_nombre_lado()
{
	return "derecha"; 
}

void setup()
{
	fijar_armadura_base("guante");
	set_name("descerroguante");
	set_short("%^BLACK%^BOLD%^Descerroguante%^RESET%^");
	set_main_plural("%^BLACK%^BOLD%^Descerroguantes%^RESET%^");
	add_alias("guante");
	add_plural("guantes");
	add_plural("descerroguantes");
	
	set_long("Los descerroguantes son artefactos muy codiciados por ladrones y "
	"otros bribones especializados en el forzado de cerraduras. Son extremadamente "
	"sedosos al tacto y tan finos que jurarías se desharían al agitarlos un poco. "
	"Pero una vez equipado, este guante multiplica la sensibilidad de las yemas de "
	"los dedos por mil, permitiendo alcanzar una comunión exquisita con las cerraduras "
	"que trae de cabeza a negocios aseguradores. Sin duda este guante ha de ser "
	"manufactura gnómica. Por su forma te percatas de que está diseñado para la "
	"mano derecha. Esta es la mano más común de los guantes, puesto que la mayoría "
	"de humanoides son diestros.\n");
	
	add_static_property("forzar", 25);
	add_static_property("messon", "La sensibilidad de las yemas de tus dedos se multiplica por mil.\n");
	
	fijar_valor(20 * 500);
}
