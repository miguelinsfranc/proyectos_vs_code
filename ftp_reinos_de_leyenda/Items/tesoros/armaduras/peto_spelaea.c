// Dunkelheit 01-06-2011

inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("bandas");
    set_name("spelaea");
    set_short("%^BLACK%^BOLD%^Peto Spelaea%^RESET%^");
    add_alias("peto");
    set_main_plural("%^BLACK%^BOLD%^Petos Spelaea%^RESET%^");
    add_plural(({"petos", "spelaeas"}));
    
    set_long("Es el mítico peto de bandas metálicas portado por el Kia Dai, el "
    "legendario antipaladín de la Era 2ª famoso por sus cruzadas impías en "
    "tierras kathenses. El acero que forma las bandas horizontales ha sido "
    "embellecido con pintura negra mate, de tal forma que el único brillo que "
    "desprende esta reliquia es el de las mallas metálicas que cubren las "
    "axilas y la cintura. En la espalda, tres pesadas hebillas doradas son "
    "cruzadas por sendas correas para permitir un ajuste perfecto.\n");
    
    fijar_encantamiento(10);
    
    fijar_genero(1);
}

string descripcion_encantamiento()
{
    return ::descripcion_encantamiento() + "\nUn aura de maldad rodea a esta armadura.\n";
} 
