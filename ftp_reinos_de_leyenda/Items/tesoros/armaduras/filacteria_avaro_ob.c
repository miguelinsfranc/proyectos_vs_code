// Satyr 16/02/2022 01:02
#include <baseobs_path.h>
inherit BMONSTRUOS + "bases/no_muerto.c";

nosave private object objeto_invocador;

void fijar_objeto_invocador(object filacteria)
{
    objeto_invocador = filacteria;
}
object dame_objeto_invocador()
{
    return objeto_invocador;
}

void setup()
{
    fijar_familia_muerto("momia");

    set_short("Sombra de la usura");
    set_main_plural("Sombras de la usura");
    set_long(
        "Una silueta humanoide cubierta en un humo negro impenetrable que "
        "oculta cualquier detalle de sus facciones. La figura es "
        "ligeramente obesa y tiene una cabeza coronada por dos ojos "
        "blancos que son pozos de luz blanca sin ningún tipo de iris. Está "
        "perfectamente inmóvil, pero su figura humea, parpadea y se desdibuja, "
        "haciendo que discernir su forma exacta sea difícil.\n");
    generar_alias_y_plurales();

    fijar_clase("ciudadano");
    fijar_tamanyo_racial(4);
    fijar_peso(90000);
    fijar_genero(1);
    fijar_des(7);
    fijar_fue(9);
    fijar_con(14);
    fijar_des(15);
    fijar_int(19);
    fijar_sab(5);
    fijar_car(5);
    fijar_nivel(30);

    /**
     * Esto lo hacemos ya para que la actualización en diferido no sobreescriba
     * la carga máxima
     */
    actualizar_caracteristicas_pendientes();

    fijar_altura(140 + random(32));
    fijar_peso_corporal(60000 + random(20000));

    fijar_peso_maximo_carga_fijo(500000);
    fijar_pvs_max(12000);

    add_extra_look(TO);
}
void setup_comun_invocacion()
{
    ver_siempre();
    permitir_coger();
    grupo_invocador();
    fijar_porcentaje_xp(0);
    invocacion_con_invocador();
    seguir_incondicional();
}

string *dame_comandos_permitidos()
{
    return ::dame_comandos_permitidos() -
           ({"proteger", "desproteger", "matar", "enterrar"});
}
void dest_me()
{
    if (environment()) {
        all_inventory(TO)->move(environment());
        tell_room(
            environment(),
            query_short() +
                " parpadea y desaparece en un charco de sombras.\n");
    }

    ::dest_me();
}
/*******************************************************************************
 * Sección: limitaciones de unas cuantas cosas
 ******************************************************************************/
void attack_by(object ob)
{
}
void attack_ob(object ob)
{
}
void attack()
{
}
object fijar_protector(object ob)
{
}
object set_protector(object ob)
{
}
object dame_protector()
{
    return 0;
}
object query_protector()
{
    return 0;
}
int query_protect_valid(object atacante, object protegido)
{
    return 0;
}
int dame_protector_valido(object atacante, object protegido)
{
    return 0;
}
int forzar_habilidad(string verbo, string tx)
{
    return 0;
}
