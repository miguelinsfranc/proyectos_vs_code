// Vhurkul 17.02.2004
// Brazal de Piedra-Negra
/*
	Zoilder 05/11/2011:
		- Corrijo el segundo add_alias que hay, pues debe ser add_plural.
*/

/*
	Tipo: Armadura
	Base: Brazal
	Nivel: 1
	Efectos:
	- Luz: -20
*/

inherit "/obj/armadura";

void setup() {
	fijar_armadura_base("brazal");
	set_name("brazal piedra negra");
	set_short("Brazal de %^BOLD%^BLACK%^Piedra-Negra%^RESET%^");
	add_alias(({"brazal","negra","piedranegra","piedra-negra"}));
	set_main_plural("Brazales de %^BOLD%^BLACK%^Piedra-Negra%^RESET%^");
	add_plural(({"brazales","negras","piedrasnegras","piedras-negras","piedranegras","piedra-negras"}));
	set_long("Es conocida por todos la habilidad de los Ogros-Mago de Al-Qualanda para extraer y trabajar ese "
		"mineral que solamente se encuentra en sus tierras y se conoce como Piedra-Negra. Éste es un brazal "
		"metálico en el cual se han incrustado pequeños trozos de este material parecido al cristal, famoso "
		"por poseer la cualidad única de absorber la luz casi por completo, creando en torno suyo zonas de "
		"densa oscuridad.\n");
	ajustar_luz(-5);
	fijar_tamanyo_armadura(8);
	}
