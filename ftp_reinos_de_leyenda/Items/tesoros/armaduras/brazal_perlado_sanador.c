// Satyr 08/05/2018
#include <baseobs_path.h>
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("brazal");
    set_name("brazal");
    set_short("%^BOLD%^WHITE%^Brazal perlado del Sanador%^RESET%^");
    set_main_plural("%^BOLD%^WHITE%^Brazales perlados del Sanador%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Un brazal de acero blanco cuyo avambrazo está repleto de filigranas "
        "grabadas que se enroscan sobre si mismas para formar espirales de "
        "varios "
        "tamaños.\n\n"

        "  La pieza está gobernada por una perla oval incrustada en su "
        "superficie. Ésta parece iluminarse y apagarse a intervalos constantes,"
        " como el leve respirar de una criatura dormida.\n\n"

        "  Sus codales están adornados con heráldica floral. No logras "
        "reconocer el blasón, cosa normal, pues la casa que lo lucía hace "
        "mucho que es historia olvidada.\n");

    add_static_property("poder_curacion", 35);
    fijar_encantamiento(10);

    fijar_conjuntos(BTESOROS_CONJUNTOS + "entereza_del_caballero.c");
}
