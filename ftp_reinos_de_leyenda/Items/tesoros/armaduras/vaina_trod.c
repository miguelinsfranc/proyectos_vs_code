// Armadura nivel 8
/*
	Zoilder 29/11/2011:
		- Corrección de typos.
*/
// Satyr 12.12.2014 -- Reducción de valor
inherit "/obj/vaina";
void setup()
{
    set_name("vaina");
    add_alias(({"trod", "vaina de trod"}));
    add_alias(({"trod", "vainas de trod"}));
    set_short("%^BOLD%^RED%^Vaina de Trod%^RESET%^");
    set_main_plural("%^BOLD%^RED%^Vainas de Trod%^RESET%^");
    set_long(
        " Trod fue un poderoso bárbaro humano que, sin duda, sufrió una vida que no se merecía."
        " Prácticamente nacido en un campo de batalla, no conoció otro oficio o cariño que el"
        " que le daba su propia espada. Sirvió durante décadas como mercenario y acabó"
        " con la vida de muchas criaturas de las tinieblas, aunque también ayudó a un"
        " centenar de señores del mal a cumplir sus maquiavélicos planes."
        " Mala fue la ocasión en la que se decidió por aceptar la oferta de destruir al poderoso"
        " Halim; un caótico mago del cónclave del Encantamiento que era seguidor de Osucaru."
        " La vida de Halim terminó cuando Trod le empaló con su temible espada a dos manos, pero"
        " le llevo varios años de persecución, durante los cuales, Halim gastó terribles bromas"
        " al bárbaro. A los pocos meses de su primer encuentro, Trod se convirtió en una mujer"
        " por culpa de un sortilegio del encantador que tenía intenciones de humillarle,"
        " lo que le trajo graves problemas de identidad y de fuerza; pero esto, comparado con"
        " lo que le paso después, no es nada.\n"
        " Tras darle el golpe de gracia a su rival, este lanzó un sortilegio que lo convirtió en"
        " lo que ves ahora... ¡una vaina!, ¡una vaina del cuero más duro y elástico que jamás veras!"
        " y no te creas que lo lleva mal, hace su trabajo mejor que ningún artefacto creado por"
        " hombre, enano o elfo; es capaz de guardar cualquier arma grande sin ningún problema."
        " Suerte extraña la suya, sin duda. Ahora el espíritu de Trod vive eternamente en esta"
        " vaina y la única alegría que tiene en su mísera vida, es aquella que ya tenía antaño: el"
        " afilado roce de sus amadas armas. Por desgracia para ti, no todas le van a gustar.\n"
    );

    fijar_peso(500);
    fijar_genero(2);

    fijar_encantamiento(15);
    fijar_BE(2);
    fijar_vida(150);
    fijar_lugar("cintura");
    fijar_valor(3000);
    //fijar_valor(dame_valor() * 10);

    ajustar_BE(-10);
    ajustar_BO(10);
    
    {
        string *armas = allocate(0);
        object tabla  = load_object("/table/bd_armas.c");
        int *data;

        if (tabla) {
            foreach(string tipo_arma in tabla->dame_armas()) {
                data = tabla->ojear_datos_armas(tipo_arma);

                if (!arrayp(data)  || sizeof(data) < 11) {
                    continue;
                }
                if (data[11] > 1) {
                    armas += ({tipo_arma});
                }
            }
        }

        fijar_armas(armas);
    }
	add_static_property("nivel_minimo",20);
}
int test_add(object ob,int flag)
{
    int exito = ::test_add(ob, flag);
    
    if (environment() && living(environment())) {
        if (exito) {
            tell_object(environment(), query_short() + " se abre y se estira, " + ob->query_short() + " le gusta mucho a Trod.\n");
        }
        else {
            tell_object(environment(), 
                query_short() + " se niega a guardar " + ob->query_short() + ". A Trod sólo le gustan las armas grandes.\n"
            );
        }
    }
    return exito;
}
