// Durmok 31/05/2003
// Dunkelheit 31-05-2011, 8º Aniversario del conjunto Arbóreo
// Eckol Ago19: conj nuevo

#include <baseobs_path.h>

inherit "/obj/armadura"; 

string dame_nombre_material()
{
    return "Seda enhebrada con hilos de mithril";
}

void setup()
{
    set_base_armour("capa");
    set_name("capa");
    set_short("Capa %^BLACK%^BOLD%^Itzalak%^RESET%^");
    add_alias("itzalak");
    set_main_plural("Capas %^BLACK%^BOLD%^Itzalak%^RESET%^");
    add_plural(({"capas", "itzalaks"}));
    set_long("La capa Itzalak es conocida por potenciar de forma exquisita las "
    "habilidades de subterfugio de su portador. Está tejida con seda de color "
    "negro y tiene enhebrados hilos de mithril procedentes de las antiguas minas "
    "élficas de la Era 2ª. Antaño era portada por los ladrones de leyenda para "
    "pasar desapercibidos en la muchedumbre y poder así robar con mayor facilidad.\n");
    fijar_conjuntos(BTESOROS_ARMADURAS+"conjunto_itzalak.c");
    fijar_genero(2);
    add_static_property("esconderse", 5);
    add_static_property("sigilar", 5);
    fijar_encantamiento(5);
    add_static_property("fuego", 1);
    add_static_property("electrico", 1);
    add_static_property("aire", 1);
}

