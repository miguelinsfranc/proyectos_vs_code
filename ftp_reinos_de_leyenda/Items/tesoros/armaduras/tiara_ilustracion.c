// Satyr 07.01.2015
// Para que la gente no se queje de los items de sab, uno para todos
inherit "/obj/armadura";
void setup()
{
    fijar_armadura_base("collar");
    set_name("gargantilla");
    set_short("%^CYAN%^Gargantilla de la Ilustración%^RESET%^");
    set_main_plural("%^CYAN%^Gargantilla de la Ilustración%^RESET%^");
    add_alias(({"ilustración", "ilustracion", "gargantilla de la ilustracion"}));
    add_plural(({"gargantillas", "gargantillas de la ilustracion", "gargantillas de la ilustración"}));
    set_long(
        "  Una fina gargantilla hecha de un hilo de lapisláluzi que está "
        "envuelta en una fuerte aura mágica. Varias piedras preciosas "
        "del tamaño de la cabeza de un alfiler adornan su acabado.\n\n"
        "  Los detalles de su creación pertenecen a los secretos de "
        "eras pasadas que fueron redescubiertos durante la 4ª era "
        "por unas criaturas diminutas de aspecto reptiloide cuyo "
        "nombre no vamos a mencionar en esta descripción.\n\n"
        "  Cualquiera que se la ponga disfrutará de una claridad "
        "mental, un nivel de percepción y unos conocimientos "
        "bastamente mejorados.\n"
    );

    fijar_encantamiento(10);
    add_static_property("sab", 1);
    fijar_material(5);
    
    add_static_property("mental"   , 3);
    add_static_property("ts-mental", 3);

	add_static_property("nivel_minimo", 30);
}
