/*
    Kiratxia el 02-09-2010
    Guante que llama a Enjambres
*/
// Satyr 24.04.2015 -- Guante de cuero.
inherit "/obj/armadura";
inherit "/obj/magia/objeto_formulador";

string dame_nombre_lado()
{
    return "derecha"; 
}

void setup()
{
    fijar_armadura_base("guante de cuero");
    set_name("apicultor");
    add_alias(({"guante","apicultor"}));
    add_plural(({"guantes","apicultores"}));
    set_short("Guante de %^YELLOW%^Apicultor%^RESET%^");
    set_main_plural("Guantes de %^YELLOW%^Apicultor%^RESET%^");
    set_long("Los apicultores siempre han usado guantes, entre otros objetos para protegerse"
    " de las picaduras de los insectos ya que en muchas ocasiones podían causarles infecciones"
    " o introducirles veneno en el organismo. Un mago un día decidió que si podía controlar a los insectos"
    " no tendría que temer a ser picado por ellos y por ello desarrolló este guante que podía usar"
    " esa habilidad con sólo alzar la mano.\n");
    fijar_genero(1);
    fijar_material(3);
    fijar_encantamiento(5);
    fijar_valor(15000);
    fijar_nivel(25);
    fijar_esfera(2);
    
    fijar_recuperacion(120);
}
    void init()
{
    ::init();
    add_action("alzar","alzar");
}

int alzar (string str)
{
    if(!str)
        return notify_fail("Si deseas implorar el poder de los antiguos, debes especificar un objetivo.\n");

    return formular("llamar enjambres", str, 0);            
}

void iniciar_formulacion(string nombre_hechizo,object portador)
{
    tell_object(portador,"Susurras unas palabras implorando el poder de tu "+query_short()+", mientras alzas tu mano al aire.\n");
    tell_accion(environment(portador),portador->query_short()+" alza su "+query_short()+" mientras susurra algo ininteligible.\n",
        "Alguien susurra algo ininteligible.\n",({portador}),portador);
}
