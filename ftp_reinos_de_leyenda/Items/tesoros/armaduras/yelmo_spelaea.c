// Dunkelheit 01-06-2011

inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("gran yelmo");
    set_name("spelaea");
    set_short("%^BLACK%^BOLD%^Gran Yelmo Spelaea%^RESET%^");
    add_alias(({"yelmo", "gran yelmo"}));
    set_main_plural("%^BLACK%^BOLD%^Grandes Yelmos Spelaea%^RESET%^");
    add_plural(({"grandes", "yelmos", "grandes yelmos", "spelaeas"}));
    
    set_long("Es el mítico gran yelmo que protegía el cráneo de Kia Dai, el "
    "legendario antipaladín de la Era 2ª famoso por sus cruzadas impías en "
    "tierras kathenses. De formas sobrias, este yelmo ha sido pintado por "
    "completo de negro aunque el paso de los años ha descostrado algunas "
    "de sus zonas, por las que asoma el brillante acero de esta reliquia.\n");
    
    fijar_encantamiento(10);
    
    fijar_genero(1);
}

string descripcion_encantamiento()
{
    return ::descripcion_encantamiento() + "\n%^BLACK%^BOLD%^Un aura de maldad rodea a esta armadura.%^RESET%^";
}

