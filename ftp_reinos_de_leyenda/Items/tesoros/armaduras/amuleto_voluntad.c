/*
	Kiratxia 15/02/2011
	Zoilder 28/11/2011:
		- Añado un plural correcto, porque si no, al meterlo en baúl, si hay varios, mostraría algo erróneo y no se
			sabría qué item es.
*/

inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("amuleto");
    set_name("Amuleto de Voluntad");
    set_short("Amuleto de %^YELLOW%^Voluntad%^RESET%^");
    set_main_plural("Amuletos de %^YELLOW%^Voluntad%^RESET%^");
    add_alias(({"amuleto","voluntad"}));
    add_plural(({"amuletos","voluntades"}));
    set_long("Se trata de un amuleto algo desgastado de color amarillo y bordes azules. Tiene forma de escudo, con los bordes redondeados.\n");

    add_static_property("messon","Tus reflejos se agudizan.\n");
    add_static_property("messoff","Tus sentidos vuelven a su intensidad original.\n");

    fijar_material(2);
    fijar_encantamiento(15);
    ajustar_BO(5);
    ajustar_BP(10);
    fijar_peso(300);
}
