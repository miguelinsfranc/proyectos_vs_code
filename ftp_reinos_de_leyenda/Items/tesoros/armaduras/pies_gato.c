// Dunkelheit 03-06-2011

inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("zapatillas");
    set_name("pies de gato");
    set_short("Pies de %^BLUE%^BOLD%^G%^RESET%^BLUE%^a%^BOLD%^t%^RESET%^BLUE%^o%^RESET%^");
    add_alias(({"pies","gato"}));
    set_main_plural("pares de Pies de %^BLUE%^BOLD%^G%^RESET%^BLUE%^a%^BOLD%^t%^RESET%^BLUE%^o%^RESET%^");
    add_plural(({"pares", "gatos"}));
    fijar_genero(-1);
    set_long("Pies de gato para escalar.\n");
    
    add_static_property("escalar", 20);
    add_static_property("sin-huella", 1);
}
