// Satyr 2012
inherit "/obj/escudo"; 
void setup()  
{ 
	set_base_shield("escudo de madera");
	set_name("escudo");
	set_short("%^ORANGE%^Escudo de la %^RED%^Mano Cercenada%^RESET%^");
	set_main_plural("%^ORANGE%^Escudos de la %^RED%^Mano Cercenada%^RESET%^");
	set_long(
		"Un pesado escudo de madera adornado con la sangrienta huella "
		"de una mano rodeada de extraños símbolos. Está hecho de caoba "
		"y reforzado con bandas de hierro por su parte exterior e interior. "
		"Está polvoriento y sucio, ya que esta reliquia viene de tiempos "
		"pasados en los que un caudillo orco -cuyo nombre se ha perdido "
		"en el olvido- arrancó la mano de un Rey Elfo cuando este le perdonó "
		"la vida a los suyos y le ofreció una tregua. El cacique rebanó "
		"mitad del brazo del incauto y semejante acto otrogó el valor suficiente "
		"a los suyos para ganar el combate de forma aplastante. Durante "
		"el resto de su vida, el Caudillo usó el escudo que empuñaba en "
		"aquella batalla, pero le colgó la mano del Rey para pavonearse "
		"de los seguidores del Bien y, en especial, de los Elfos. Cuando su "
		"dueño murió y fue incinerado, la mano del Rey se convirtió en cenizas, "
		"pero su huella sanguinaria ha dejado un rastro en el escudo, dejando "
		"una siniestra mueca que se rie de todo aquello que es bueno.\n" 
	);
	
	add_alias(({"gran","mano cercenada","escudo de la mano cercenada", "escudo de la mano"}));
	add_plural(({"escudos","escudos de la mano", "escudos de la mano cercenada"}));

    ajustar_BE(10);
	fijar_encantamiento(20);	
	add_static_property("ts-mental", 5);
	add_static_property("ts-muerte", 5);
	add_static_property("ts-horror", 5);
}