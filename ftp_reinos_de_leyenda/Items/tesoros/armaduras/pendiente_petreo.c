// Satyr 2012
// Satyr 25.06.2015 -- Danzantes
// Satyr 26.10.2016 -- Mago-ladrón
inherit "/obj/armadura";
void setup()
{
	fijar_armadura_base("pendiente");
	set_name("pendiente");
	set_short("Pendiente %^ORANGE%^P%^RESET%^é%^ORANGE%^t%^RESET%^r%^ORANGE%^e%^RESET%^o");
	set_main_plural("Pendientes %^ORANGE%^P%^RESET%^é%^ORANGE%^t%^RESET%^r%^ORANGE%^e%^RESET%^os");
	add_alias(({"pendiente", "pétreo", "petreo", "pendiente petreo"}));
	add_plural(({"pendientes", "pétreos", "petreos", "pendientes petreos", "pendientes pétreos"}));
	
	set_long(
		"Un pendiente de algún tipo de piedra preciosa que, misteriosamente, ha sido recubierto por una "
		"capa imperecedera de piedra caliza. Si rascas con fuerza la piedra, esta se desprende y te deja "
		"ver lo que parece un azabache, sin embargo, la magia del pendiente rápidamente hace que la piedra "
		"se regenere cuanto antes. Una cadena de nácar une la pieza a una pequeña espina de Ysym que se "
		"usa para atravesar el agujero de la oreja de sus portadores.\n"
	);
	
	fijar_material(6);
	fijar_encantamiento(20);
    add_static_property("agua"  , -5);
	add_static_property("armadura-magica", 5);
    add_static_property("clase", ({"Sacerdote de Velian", "Hechicero", "Mago Rúnico", "Danzante Rúnico", "Mago-ladrón", "Bardo"}));
	add_static_property("nivel_minimo",20);
}
string descripcion_encantamiento()
{
	return "El encantamiento de este objeto permite formular el hechizo 'piel de piedra' con más potencia.";
}
void objeto_equipado()
{
	if (environment())
		environment()->add_static_property("pendiente-petreo", 2);
}
void objeto_desequipado()
{
	if (environment())
		environment()->remove_static_property("pendiente-petreo");
}
