// Durmok 31/05/2003
// Dunkelheit 31-05-2011, 8º Aniversario del conjunto Arbóreo
// Eckol Ago19: Conj nuevo

#include <baseobs_path.h>
inherit "/obj/armadura"; 

string dame_nombre_material()
{
    return "Seda enhebrada con hilos de mithril";
}

void setup()
{
    set_base_armour("capucha");
    set_name("capucha");
    set_short("Capucha %^BLACK%^BOLD%^Itzalak%^RESET%^");
    add_alias("itzalak");
    set_main_plural("Capuchas %^BLACK%^BOLD%^Itzalak%^RESET%^");
    add_plural(({"capuchas", "itzalaks"}));
    set_long("Es una capucha grande de tela enhebrada con hilos de mithril "
    "extraído en minas élficas de la Era 2ª. Antiguamente la portaban los "
    "mejores ladrones para resguardar sus rostros de miradas hostiles y para "
    "camuflarse y hacerse casi invisibles. Las leyendas también cuentan que "
    "la capucha aporta suerte a su dueño cuando es vestida.\n");
    fijar_conjuntos(BTESOROS_ARMADURAS+"conjunto_itzalak.c");
    fijar_genero(2);
    add_static_property("esconderse", 20);
    add_static_property("sigilar"   , 20);
    fijar_encantamiento(10);
}

