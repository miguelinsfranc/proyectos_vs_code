// Vhurkul 17.02.2004
// Colgante del Oso

/*
        Tipo: Armadura
        Base: Amuleto
        Nivel: 7
        Efectos:
        - Armadura Natural: 5
*/
#include <baseobs_path.h>
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("amuleto");
    set_name("colgante oso");
    set_short("%^CYAN%^BOLD%^Colgante%^RESET%^ del %^YELLOW%^Oso%^RESET%^");
    add_alias(({"colgante", "oso"}));
    set_main_plural(
        "%^CYAN%^BOLD%^Colgantes%^RESET%^ del %^YELLOW%^Oso%^RESET%^");
    add_plural(({"colgantes", "osos"}));
    set_long("A pesar de que muchos achacan la fabricación de estos artefactos "
             "a los Druidas, estos siempre han "
             "negado su autoría. Y es que el colgante contiene el espíritu "
             "cautivo de un oso pardo, y aquel que lleve "
             "el colgante podrá esclavizar su esencia. La verdad es que a "
             "pesar de tener un origen tan aberrante, estos "
             "colgantes son tremendamente efectivos para quienes no tienen "
             "escrúpulos por la protección adicional que "
             "ofrecen.\n");
    fijar_material(3);
    fijar_encantamiento(10);
    add_static_property("armadura-magica", 5);
    add_static_property(
        "messon", "Sientes como el Espíritu del Oso se somete a tu control.\n");
    add_static_property(
        "messoff",
        "El Espíritu del Oso contenido en el amuleto te abandona.\n");
    add_static_property("nivel_minimo", 20);

    fijar_conjuntos(({BCONJUNTOS + "fortaleza_golem_de_piedra.c"}));
}
