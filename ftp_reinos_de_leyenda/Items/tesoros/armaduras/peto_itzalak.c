// Durmok 31/05/2003
// Dunkelheit 31-05-2011, 8º Aniversario del conjunto Arbóreo
// Eckol Ago19: conj nuevo

#include <baseobs_path.h>

inherit "/obj/armadura";

string dame_nombre_material()
{
    return "Cuero enhebrado con hilos de mithril";
}

void setup()
{
    set_base_armour("cuero");
    set_name("cuero");
    set_short("Peto de Cuero %^BLACK%^BOLD%^Itzalak%^RESET%^");
    add_alias(({"itzalak", "peto"}));
    set_main_plural("Petos de Cuero %^BLACK%^BOLD%^Itzalak%^RESET%^");
    add_plural(({"petos", "cueros", "itzalaks"}));
    set_long("Es un peto de cuero cuyo diseño recuerda al de los elfos, con "
    "hilos enhebrados de mithril, lo cual potencia su capacidad protectora sin "
    "eclipsar la movilidad. Es de color gris oscuro, ideal para que los bribones "
    "la porten y puedan camuflarse mejor incluso a la luz del día. Este peto "
    "fue famoso durante la Era 2ª, cuando era la armadura insignia de los "
    "ladrones de leyenda.\n");
    fijar_conjuntos(BTESOROS_ARMADURAS+"conjunto_itzalak.c");
    add_static_property("esconderse", 20);
    add_static_property("sigilar", 20);
    fijar_encantamiento(10);
}

