// Satyr 08/05/2018
#include <baseobs_path.h>
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("brazal");
    set_name("brazal");
    set_short("%^BLACK%^BOLD%^Brazal perlado del Castigador%^RESET%^");
    set_main_plural("%^BLACK%^BOLD%^Brazales perlados del Castigador%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Un brazal de acero blanco adornado con espirales angulares que se "
        "deslizan por su pulida "
        "superficie para converger en el extremo más cercano a la mano, "
        "el cual está gobernado por una solitaria -y extrañamente translúcida- "
        "perla negra.\n\n"

        "  La gema palpita al ritmo de una respiración pausada, refractando la "
        "luz como si fuese "
        "un caleidoscopio y haciendo que ésta dibuje formas sobre la "
        "superficie "
        "del brazal. Tras observar este curioso efecto durante un rato te das "
        "cuenta de algo insólito: en algunas posiciones la luz adopta formas "
        "que recuerdan a caballeros galopando a lo largo de la llanura "
        "representada por el propio brazal.\n\n"

        "  No reconoces los blasones que adornan esta pieza de panoplia, pero "
        "no cabe duda de que fuera quien fuere su portador, éste gozaba de "
        "un gusto exquisito para sus piezas de armadura.\n");

    fijar_BO(10);
    add_static_property("daño", 10);
    fijar_encantamiento(10);

    fijar_conjuntos(BTESOROS_CONJUNTOS + "entereza_del_caballero.c");
}
