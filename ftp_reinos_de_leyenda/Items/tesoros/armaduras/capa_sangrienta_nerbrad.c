// Satyr 29.07.2016 
// Eckol Feb19: corrijo comprobación de uso para que funcione en desarmados.
#include <tiempo.h>
// Valor real, valor a mostrar
#define PODER_VAMPIRISMO    ({50, 50})
#define DURACION_VAMPIRISMO 300
#define LOCK_CAPA           1800

inherit "/obj/armadura";

int activar_vampirismo();

// Devuelve una palabra de poder generada aleatoriamente
string dame_palabra_poder() {
    string tx;
    
    if ( tx = query_property("palabra_poder") )
        return tx; 
    
    tx  = element_of(({"plasma"   , "linfa"    , "flujo"  , "líquido" , "sangre"}));
    tx += " " + element_of(({
        "encarnado", "escarlata", "bermejo", "colorado", "granate", 
        "grana"   , "púrpura"  , "rubí"   , "carmesí" , "bermellón", 
        "corinto"
    }));
    tx += " del " + element_of(({"condenado", "réprobo", "malvado", "perverso", "malo", 
        "execrable", "detestable"
    }));
    add_property("palabra_poder", tx);
    
    return tx;
}
void setup()
{
    fijar_armadura_base("capa");
    set_name("capa");
    set_short(  
        "%^RED%^BOLD%^C%^RESET%^RED%^apa %^RED%^BOLD%^S%^RESET%^RED%^angrienta de "
            "%^RESET%^%^MAGENTA%^N%^BOLD%^erbrad%^RESET%^"
    );
    set_main_plural(  
        "%^RED%^BOLD%^C%^RESET%^RED%^apas %^RED%^BOLD%^S%^RESET%^RED%^angrientas de "
            "%^RESET%^%^MAGENTA%^N%^BOLD%^erbrad%^RESET%^"
    );
    generar_alias_y_plurales();
    
    set_long(
        "Nerbrad fue uno de los primeros vampiros nacidos allá cuando Astaroth recién "
            "comenzó sus experimentos para corromper a los Darunday. El joven humano se horrorizó "
            "de su nueva condición y juró jamás dañar a otro ser con su hambre, lo que le "
            "llevó a alimentarse únicamente de animales. Semejante dieta bastó durante los primeros días"
            ", pero la maldición de Astaroth es imparable y pronto la sangre de animales se "
            "hacía menos y menos nutritiva.\n\n"
            
        "  Con esto comenzó a exterminar bosques enteros para saciar su hambre, dejando "
            "tras de si un rastro de cadáveres resecos que aterrorizó a buena parte de los "
            "primeros nacidos.\n\n"
            
        "  La muerte de Nebrad fue confirmada poco después, pues a pesar de que su corazón era puro "
            "sus actos eran vistos como los de una bestia peligrosa de un hambre incontrolabre y "
            "los suyos lo cazaron. "
            "Sin embargo, esta capa púrpura de corte sobrio y cuello almidonado sobrevivió a "
            "aquellos tiempos para recordar la horrible historia del antiguo vampiro.\n\n"
             
        "  El corte de la capa es completamente siniestro y está recubierta de "
            "una película de sangre que parece manar incesantemente de ninguna parte y caer "
            "chorreando al suelo, dejando charcos ennegrecidos de perdición a su paso, pero "
            "lo peor es el hedor... un olor dulzón y metálico que te hace ver el mundo "
            "de color bermellón.\n"
    );
    
    // Este es un muy buen objeto pero quiero que para sigiladores tenga un coste...
    add_static_property("sigilar"   , -65); 
    add_static_property("esconderse", -65);
    fijar_peso(9750);

	fijar_genero(2);

    // Esto es un objeto eminentemente ofensivo
    add_static_property("armadura-magica", -5);
    add_static_property("daño"   , 18);
    add_static_property("critico",  6);
    ajustar_BO(10);
    ajustar_BE(-dame_BE() / 2);
    
    // La palabra de poder en condenado... para que el condenado valga para algo.
    set_read_mess(
        "La frase de poder es '" + dame_palabra_poder() + "'.",
        "condenado",
        0
    );
	add_static_property("nivel_minimo",29);
}
string descripcion_juzgar(object b) {
    return "La sangre de Nerbrad envuelve a su antigua capa y obedecerá a la palabra de mando "
        "de la misma para activar un efecto Vampirismo(" + PODER_VAMPIRISMO[1] + ") con una duración "
        "de " + pretty_time(DURACION_VAMPIRISMO) + " sobre una de tus armas empuñadas. Si no empuñas "
        "ningún arma, aplicará el efecto sobre tu estilo de combate desarmado activo. Este efecto "
        "no podrá activarse a la luz del sol."
    ;
}
// Añadimos la palabra de poder generada...
void init() {
    string *slices;
    
    ::init();
    
    slices = explode(dame_palabra_poder(), " ");
    
    anyadir_comando_equipado(
        slices[0],
        implode(slices[1..], " "),
        (: activar_vampirismo :)
    );
    fijar_cmd_visibilidad(
        slices[0],
        implode(slices[1..], " "),
        1
    );
}
// Si hay un portador, devuelve la sala de dicho portador
private object dame_sala() {
    object entorno = TO;
    
    while((entorno = environment(entorno)) && ! entorno->query_room()) {
    }
    
    return entorno;
}
// Determina si un arma es válida para meter el efecto
private int arma_valida(object b) {
    return b && ! b->dame_efectos_tipo("vampirismo");
}
// Devuelve las armas/estilos del jugador ordenadas por prioridad (mas bajo -> mejor)
private object *dame_armas_jugador(object pj) {
    object *armas = allocate(0);
    
    armas += ({pj->dame_arma_principal_empunyada()});
    armas += ({pj->dame_arma_principal_empunyada(1)});
    
    if ( ! sizeof(pj->dame_armas_empunyadas()) )
        armas += ({pj->dame_ob_estilo()});
        
    return armas;
}
// Devuelve el arma/estilo válido para el vampirismo que empuña el jugador (Si existe)
private object dame_arma_valida_jugador(object pj) {
    mixed arma = dame_armas_jugador(pj);
    
    arma = filter(arma, (:arma_valida:));
    
    return sizeof(arma) ? arma[0] : 0;
}
// Activa el efecto del vampirismo
private int activar_vampirismo() {
    object entorno = dame_sala();
    object que;
    string ef;
    
    if ( environment()->dame_bloqueo_combate(TO) )
        return notify_fail("Hace demasiado poco que has usado el poder de " + dame_tu() + " " + query_short() + ".");
        
    if ( dame_bloqueo_combate(TO) )
        return notify_fail(capitalize(dame_tu()) + " " + query_short() + " todavía no ha recuperado "
            "su poder mágico.\n"
        );
        
    if ( ! CLIMA->query_noche() && entorno->query_outside() )
        return notify_fail("El poder de " + dame_tu() + " " + query_short() + " no se despertará "
            "a plena luz del día.\n"
        );
        
    if ( ! que = dame_arma_valida_jugador(TP) )
        return notify_fail(
            "Ninguna de tus armas o estilos de combate son válidos para la magia de "
                 + query_short() + ".\n"
        );
        
    if ( ! arma_valida(que) )
        return notify_fail(
            capitalize(que)->dame_tu() + " " + que->query_short() + " ya está bajo el influjo "
                "de un efecto de 'vampirismo'.\n"
        );
    
    // Bloqueos
    TP->nuevo_bloqueo_combate(TO, LOCK_CAPA);
    TO->nuevo_bloqueo_combate(TO, LOCK_CAPA);
    
    // Metemos efecto y guardamos el nombre
    ef = que->nuevo_efecto_basico_temporal(
        "vampirismo",
        DURACION_VAMPIRISMO,
        PODER_VAMPIRISMO[0],
    );
        
    // Como no queremos que la gente haga cosas raras (me pongo la capa, meto vampirismo y 
    // dono el arma o me quito la capa) vamos a guardar esta información
    add_timed_property("objeto-vampirizado", ({que, ef}), DURACION_VAMPIRISMO);
    tell_object("eckol",sprintf("Que: %O\nEF: %O\nTiempo:%d\n",que,ef,DURACION_VAMPIRISMO)); 
    tell_object("eckol",sprintf("Chequeando property: %O\n",query_timed_property("objeto-vampirizado")));

    tell_object(TP,
        "Al pronunciar las palabras de poder " + dame_tu() + " " + query_short() + " envuelve "
            + que->dame_tu() + " " + que->query_short() + " con parte de su sangre maldita.\n"
    );
    tell_accion(
        environment(TP),
        "Cuando " + TP->query_cap_name() + " pronuncia la palabra de poder de " + dame_su() + " "
             + query_short() + " " + dame_este(1) + " envuelve " + que->dame_su() + " "
             + que->query_short() + " con parte de su sangre maldita.\n",
         "",
         ({TP}),
         TP
    );
    return 1;
}

int arma_en_jugador(object que){
    if(environment(que))
        return environment() == environment(que);

    //Si no tiene entorno, es que es un estilo desarmado.
    if(que->dame_estilo_desarmado())
        return -1 != member_array(environment(),que->dame_luchadores());  

    return 0; //Otros casos que no deberían darse.
}

// Determina si la capa ha encantado algo. Si ha encantado algo se asegura de que éste está con el
// portador original y de que la capa está equipada
void chequear_vampirismo() {
    mixed que = query_timed_property("objeto-vampirizado");
    
    if ( ! que || ! que[0] ) {
        remove_timed_property("objeto-vampirizado");
        return;
    }
        
    // Todo OK
    //if ( query_in_use() && environment() == environment(que[0]) )
    if ( query_in_use() && arma_en_jugador(que[0]) )
        return;
        
    que[0]->quitar_efecto(que[1]);
    remove_timed_property("objeto-vampirizado");
}
// El HB deja charcos de sangre, comprueba el vampirismo y se activa al equiparse la capa
void heart_beat() {
    object entorno;
    string sangre;

    chequear_vampirismo();
    
    if ( ! (entorno = dame_sala()) || entorno->query_timed_property("sangre") )
        return;
    
    if ( ! sangre = environment()->dame_sangre() )
        return;
    
    if ( random(100) )
        return;
        
    tell_object(environment(), "Tu capa derrama parte de la sangre que la recubre sobre el suelo.\n");
    tell_accion(
        entorno,
        capitalize(dame_articulo()) + " " + query_short() + " de " + environment()->query_cap_name() 
            + " derrama sangre sobre el suelo.\n",
        "",
        ({environment()}),
        environment()
    );
    entorno->add_timed_property("sangre", sangre, 360);
}
void objeto_equipado() {
    set_heart_beat(1);
}
void objeto_desequipado() {
    chequear_vampirismo();
    set_heart_beat(0);
}
