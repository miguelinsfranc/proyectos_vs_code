// Vhurkul 17.02.2004
// Anillo del Montañista

/*
	Tipo: Armadura
	Base: Anillo
	Nivel: 2
	Efectos:
	- Permite ver salidas escalables
*/

inherit "/obj/armadura";

void setup() {
	fijar_armadura_base("anillo");
	set_name("anillo montañista");
	set_short("Anillo del %^ORANGE%^Montañista%^RESET%^");
	add_alias(({"anillo","montanyista","montañista"}));
	set_main_plural("Anillo del %^ORANGE%^Montañista%^RESET%^");
	add_plural(({"anillos","montanyistas","montañistas"}));
	set_long("El Anillo del Montañista es un objeto imprescindible para aquellos que buscan aventuras en las "
		"zonas más accidentadas de Eirea. Imbuído con una extraña magia, este anillo dota a su portador de "
		"una capacidad de intuición pocas veces lograda sin un riguroso entrenamiento en lo que se refiere "
		"a averiguar los puntos clave donde anclar los garfios de escalada, y para que funcione sólo hay que "
		"frotarlo de vez en cuando. Toda una maravilla.\n");
	}
	
void init() {
	::init();
	add_action("frot","frotar");
	}
	
int frot(string tx) {
	string ret;
	if(!tx||tx!="anillo") return 0;
	if(!TO->query_in_use()) return notify_fail("No llevas puesto tu "+TO->query_short()+".\n");
	if(sizeof(ENV(TP)->dame_salidas_tipo("escalable"))) 
		ret=ENV(TP)->dame_salidas_tipo("escalable")[random(sizeof(ENV(TP)->dame_salidas_tipo("escalable")))];
	if(!ret) ret="pero no intuyes ningún punto de anclaje adecuado.\n";
	else ret="e intuyes que %^BOLD%^GREEN%^"+ret+"%^RESET%^ sería un buen punto de anclaje.\n";
	tell_object(TP,"Frotas tu "+TO->query_short()+" "+ret);
	TP->consumir_hb();
	return 1;
	}
	
