// Satyr 13/07/2022 01:53
#include <baseobs_path.h>
inherit "/obj/armadura";
void setup()
{
    fijar_armadura_base("guante");
    set_name("guante");
    set_short("%^MAGENTA%^G%^BOLD%^arra de %^NOBOLD%^D%^BOLD%^ragón "
              "%^NOBOLD%^A%^BOLD%^matista%^RESET%^");
    set_main_plural("%^MAGENTA%^G%^BOLD%^arras de %^NOBOLD%^D%^BOLD%^ragón "
                    "%^NOBOLD%^A%^BOLD%^matista%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        "Un guante grisáceo hecho de una redecilla de piel traslúcida y "
        "elástica atusada con un barullo de costuras que brotan del "
        "centro de la palma y recorren toda su superficie hasta "
        "desembocar en los dedos. Pequeños brillantes acompañan a estas "
        "costuras en su viaje por "
        "la pieza, refulgiendo como chispas efímeras cuando reciben una "
        "migaja de atención por parte de cualquier fuente de luz.\n\n"

        "  Todos los dedos terminan en garras de huesodragón de un tamaño nada "
        "desdeñable que han sido talladas y pulidas hasta que se tornaron "
        "níveas y nacaradas. No obstante, semejantes adornos palidecen al "
        "compararse con la enorme y majestuosa joya que corona el dorso de la "
        "pieza: un ojo de dragón cristalizado.\n"
        "");

    fijar_conjuntos(({BTESOROS_CONJUNTOS + "trofeo_draconido.c"}));
    add_static_property("daño", 5);
    add_static_property("poder_magico", 5);
}
