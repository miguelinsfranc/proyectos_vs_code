// Satyr 11/05/2018 01:10

inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("grebas");
    set_name("grebas");
    set_short("%^WHITE%^BOLD%^Grebas del %^GREEN%^Restablecimiento%^RESET%^");
    set_main_plural(
        "Pares de %^WHITE%^BOLD%^Grebas del "
        "%^GREEN%^Restablecimiento%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Unas grebas hechas con la piel de los raros y escurridizos "
        "Leshis, si bien desconoces quién ha tenido la voluntad y aptitud"
        ", ya no solo para rastrear, si no para luchar -y vencer- contra "
        "uno de estos espíritus naturales.\n\n"

        "  El cuero de las grebas es firme, pero suave y está siempre húmedo, "
        "haciendo que a su alrededor crezca musgo y los insectos se empeñen "
        "en subir y trepar por ellas. Caminar durante mucho tiempo con ellas "
        "no es un problema, puesto que la magia curativa que empapa su "
        "cuero hace que las ampollas y rozaduras de su portador se curen "
        "en apenas unas horas.\n");

    fijar_encantamiento(25);
    add_static_property("poder_curacion", 40);
    add_static_property("ts-muerte", 5);
}
