// Satyr 08/05/2018
#include <baseobs_path.h>
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("grebas");
    set_name("grebas");
    set_short("Grebas de la %^CYAN%^Esfinge%^RESET%^");
    set_main_plural("pares de Grebas de la %^CYAN%^Esfinge%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Un par de grebas que han sufrido varios procesos de encerado, "
        "cocinado y secado por manos de sastres capaces que le otorgaron "
        "de una dureza rara vez vista en este tipo de material.\n\n"

        "  Su frontal está adornado con surcos tintados "
        "que dibujan las caprichosas formas de dos alas plegadas, una en cada "
        "greba. En su parte inferior se ven más de estas tallas, emulando esta "
        "vez "
        "a un par de zarpas de felino.\n\n"

        "  Sin embargo, lo más llamativo de la pieza, sin ánimo de desmerecer "
        "a los "
        "complicados grabados que ya has visto, son el par de ojos de "
        "zafiro que adornan su parte central, donde más surcos "
        "dan rostro a una esfinge impertérrita.\n\n"

        "  Los zafiros palpitan lentamente, iluminando la pieza, otorgándole "
        "así un justificado aire enigmático y de misticismo.\n");

    add_static_property("poder_magico", 15);
    add_static_property("ts-conjuro", 10);
    add_static_property("evasion", 5);
    fijar_encantamiento(10);
}
