// Vhurkul 17.02.2004
// Peto Iris

/*
    Tipo: Armadura
    Base: Cota de Mallas
    Nivel: 7
    Efectos:
    - 5% de resistencia a todos los tipos de daño mágico
*/

inherit "/obj/armadura";

void setup() 
{
    fijar_armadura_base("cuero");
    set_name("peto iris");
    set_short("P%^BOLD%^RED%^e%^RESET%^ORANGE%^t%^BOLD%^o %^GREEN%^I%^CYAN%^r%^BLUE%^i%^RESET%^MAGENTA%^s%^RESET%^");
    add_alias(({"peto","iris"}));
    set_main_plural("P%^BOLD%^RED%^e%^RESET%^ORANGE%^t%^BOLD%^o%^GREEN%^s %^CYAN%^I%^BLUE%^r%^RESET%^MAGENTA%^i%^RESET%^s");
    add_alias(({"petos","irises"}));
    set_long("El peto iris, también conocido por los eruditos como peto prismático y por los profanos como 'Esa "
        "armadura de colores', es una maravilla de la sastrería mágica. En su creación se han de emplear elementos "
        "tan raros como escamas de dragón rojo, sangre de yeti, lágrimas de contemplador, rocas retromórficas, "
        "espinas de besugo... y así una larga lista, lo que hace su fabricación extremadamente costosa. A "
        "cambio, su poseedor obtiene cierto grado de inmunidad a los diversos efectos dañinos de las energías "
        "mágicas.\n");
        
    add_static_property("fuego",3);
    add_static_property("frio",3);
    add_static_property("acido",3);
    add_static_property("agua",3);
    add_static_property("electrico",3);
    add_static_property("tierra",3);
    add_static_property("magico",3);
    add_static_property("mental",3);
    add_static_property("aire",3);
    add_static_property("veneno",3);
    add_static_property("mal",3);
    add_static_property("bien",3);
    add_static_property("enfermedad",3);
    
    fijar_encantamiento(20);
}
int dame_estorbo()
{
    return 0;
}