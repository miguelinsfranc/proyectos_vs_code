// Falz 2007
// Dunkelheit 29-05-2008 -- QC
//Sierephad Dic 2k18	--	Añadiendo el bono a nadar

inherit "/obj/armadura";

int dame_legal_universal() { return 1; }

void setup() {

	fijar_armadura_base("anilla");
	set_name("anilla");
	set_short("Anilla %^BOLD%^BLUE%^Marina%^RESET%^");
	add_alias("marina");
	set_main_plural("Anillas %^BOLD%^BLUE%^Marinas%^RESET%^");
	add_plural(({"anillas", "marinas"}));
	set_long("\tUna anilla fabricada con los huesos de algún animal marino. "
		"En su interior esta recubierta por un material cartilaginoso que "
		"impide que su portador se dañe al golpear con la cola. Por fuera, "
		"una dura capa de lo que parecen vértebras proporciona protección "
		"a la cola del lagarto. Además, esta capa se halla cubierta por "
		"cientos de espinas de diferentes tamaños, dispuestas para aumentar "
		"la eficacia ofensiva de los coletazos. Despide un intenso olor "
		"a mar.\n");
	
	fijar_tamanyo_armadura(6); // Para Hombres-Lagarto
	fijar_genero(2);
	
	fijar_encantamiento(3);
	fijar_BE(6);
	fijar_material(7); // Raspas de peces
	
	add_static_property("agua", 6);
	add_static_property("nadar", 3);
}

string dame_nombre_material() {	return "Raspas y huesos de peces."; }

int bono_coletazo() { return 9; }

int dame_valor() { return 500*50; }

string descripcion_juzgar(object quien){
	return "Proporciona una bonificación de "+bono_coletazo()+" al daño de la habilidad 'coletazo'.";
}