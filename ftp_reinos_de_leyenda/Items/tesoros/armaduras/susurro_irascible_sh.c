// Satyr 15/07/2022 02:00
#include <clean_up.h>

// clang-format off
private object pj;
private string invulnerabilidad;
private object pendiente;
// clang-format on

void setup_sombra(object _pj, string _invulnerabilidad, object _pendiente)
{
    pj               = _pj;
    invulnerabilidad = _invulnerabilidad;
    pendiente        = _pendiente;
    shadow(pj, 1);

    pj->add_extra_look(TO);
}
int dame_sombra_susurro_irascible()
{
    return 1;
}
void destruir_sombra_susurro_irascible()
{
    if (pj) {
        pj->remove_extra_look(TO);
    }

    destruct(TO);
}
string extra_look(object afectado)
{
    string tx = afectado->extra_look(afectado) || "";

    tx += "Susurros de invulnerabilidad contra " + invulnerabilidad +
          " bisbisean en ";

    if (afectado == pj) {
        tx += "tus oídos.";
    } else {
        tx += "sus oídos.";
    }

    return tx + "\n";
}
varargs int dame_ts(mixed args...)
{
    if (sizeof(args) > 3 && args[3] == invulnerabilidad) {
        return 100;
    }

    return pj->dame_ts(args...);
}
varargs int dame_ts_silenciosa(mixed args...)
{
    if (sizeof(args) > 3 && args[3] == invulnerabilidad) {
        return 100;
    }

    return pj->dame_ts(args...);
}
varargs int
nueva_incapacidad(string nombre, string invulnerabilidad, mixed *args...)
{
    if (nombre == invulnerabilidad) {
        return 0;
    }

    return pj->nueva_incapacidad(nombre, invulnerabilidad, args...);
}
varargs int add_incapacidad(mixed *args...)
{
    return nueva_incapacidad(args...);
}

int interrumpir_habilidades(string efecto_causante, int nivel)
{
    if (efecto_causante == invulnerabilidad && nivel < 3) {
        return 0;
    }

    return pj->interrumpir_habilidades(efecto_causante, nivel);
}
