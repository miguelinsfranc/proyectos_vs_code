// Satyr 16/02/2022 12:53
/**
 * Ver:
 *
 * -
 * https://www.reinosdeleyenda.es/foro/ver-tema/concurso-comunidad-diseno-objetos-de-gloria-temporada-35/#post-341319
 * -
 * https://www.reinosdeleyenda.es/foro/ver-tema/balance-objetos-recompensa-y502/#post-344515
 */
#include <material.h>
inherit "/obj/armadura";
inherit "/obj/magia/objeto_con_poderes";

#define PODER_INVOCAR_SOMBRA "invocar sombra"
#define PODER_INVOCAR_SOMBRA_BLOQUEO 900

void setup()
{
    fijar_armadura_base("collar");
    set_name("filacteria");
    set_short("%^MAGENTA%^BOLD%^F%^NOBOLD%^ilacteria del "
              "%^YELLOW%^A%^NOBOLD%^varo%^RESET%^");
    set_main_plural("%^MAGENTA%^BOLD%^F%^NOBOLD%^ilacterias del "
                    "%^YELLOW%^A%^NOBOLD%^varo%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Las Artes nigrománticas siempre han evolucionado conforme a los "
        "deseos y anhelos de sus estudiosos y, entre ellos, la avaricia "
        "también puede estar presente en sus ambiciones. Este amuleto es "
        "prueba de ello. Su creador fue el conocido, y acaudalado, nigromante "
        "Crirow Mallus. Gracias a su esotérica, este hechicero pudo atrapar y "
        "maldecir el alma de un banquero (usurero) que intentó en su día "
        "arrebatar las opulentas posesiones que el nigromante atesoraba en su "
        "establecimiento. Desde ese día, dicho banquero está condenado sin "
        "remedio a custodiar los bienes del portador de este amuleto.\n");

    fijar_encantamiento(10);
    fijar_material(CRISTAL);
    add_static_property("mal", 5);
    add_static_property("frio", 5);
    add_static_property("bien", -5);
    add_static_property("fuego", -5);
    add_static_property("nivel_minimo", 30);
    fijar_valor_venta(62500);

    fijar_bloqueo_poderes(PODER_INVOCAR_SOMBRA_BLOQUEO, PODER_INVOCAR_SOMBRA);
}
string descripcion_juzgar(object pj)
{
    return "Esta filacteria encierra a una terrible criatura cuya avaricia es "
           "tal que no dudaría en sorber ruidosamente tu alma si eso le "
           "reportara algún beneficio ultraterrenal. Semejantes cualidades "
           "deberían dejar claro que estamos hablando de un banquero. Dicho "
           "banquero, además, ha sido envilecido —más, si cabe— por la "
           "maldición "
           "de la no-muerte y la esclavitud a la filacteria. El espíritu no es "
           "particularmente útil en combate, si bien su avaricia le permite "
           "cargar con un sinfín de objetos, \"liberando\" así a su invocador "
           "del calvario de tener que llevar encima cosas valiosas.\n";
}
string dame_ruta_sirviente()
{
    return base_name() + "_ob.c";
}
object buscar_sirviente_de(object pj)
{
    foreach (object ob in children(dame_ruta_sirviente()) || ({})) {
        if (ob->dame_invocador() == pj) {
            return ob;
        }
    }

    return 0;
}
void destruir_mis_sirvientes(object pj)
{
    if (environment() == pj && query_in_use()) {
        return;
    }

    foreach (object ob in children(dame_ruta_sirviente()) || ({})) {
        if (ob->dame_objeto_invocador() == TO) {
            ob->dest_me();
        }
    }
}
void objeto_desequipado()
{
    call_out("destruir_mis_sirvientes", 1, environment());
}
/*******************************************************************************
 * Sección: comandos
 ******************************************************************************/
object preparar_sirviente_para(object pj)
{
    object sirviente;

    if (!sirviente = clone_object(dame_ruta_sirviente())) {
        return 0;
    }

    sirviente->fijar_invocador(pj);
    sirviente->fijar_objeto_invocador(TO);

    return sirviente;
}
varargs int do_invocar(object ejecutor, object victima, string poder)
{
    object sombra;
    string err;

    if (!ejecutor) {
        return 0;
    }

    if (err = chequeo_previo_finalizar_poder(ejecutor, victima, poder)) {
        tell_object(ejecutor, err + "\n");
        return 0;
    }

    if (!sombra = preparar_sirviente_para(ejecutor)) {
        tell_object(
            ejecutor, "Un error impide traer a tu siriviente a este plano.\n");
        return 0;
    }

    mostrar_mensajes_poderes(
        "invocar_sombra", ejecutor, sombra, PODER_INVOCAR_SOMBRA);
    sombra->move(environment(ejecutor));
    poner_bloqueos(ejecutor, 0, PODER_INVOCAR_SOMBRA);

    return 1;
}
int invocar()
{
    if (!chequeo_activacion_poder(TP, 0, PODER_INVOCAR_SOMBRA)) {
        return 0;
    }

    mostrar_mensajes_poderes("activacion", TP, 0, PODER_INVOCAR_SOMBRA);

    // clang-format off
    call_out(
        "finalizacion_poder",
        2 + (TP->query_hb_counter() % 2) * 2,
        TP,
        0,
        PODER_INVOCAR_SOMBRA,
        (: do_invocar:),
        0
    );
    // clang-format on
    return 1;
}
void init()
{
    ::init();

    // clang-format off
    anyadir_comando_equipado(
        "invocar",
        "sombra de la filacteria",
        (: invocar() :),
        "Invoca a la criatura umbral asociada a esta filacteria. La criatura "
        "no puede combatir, pero su infame avaricia ha perfeccionado, a lo "
        "largo de las décadas, sus habilidades para cargar objetos."
    );
    // clang-format on
}
varargs string
chequeo_sencillo_objetivo(object jugador, object obj, string poder)
{
    object sombra = buscar_sirviente_de(jugador);

    if (!sombra) {
        return 0;
    }

    if (environment(sombra) != environment(jugador)) {
        return "Tu codiciosa sombra ya se encuentra en este mundo. Si las ha "
               "perdido de vista bien puedes despedirte de tus posesiones, "
               "pues su avaricia es un arma de doble filo.";
    }

    return "Tu sombra ya está aquí. Contigo. No es necesario invocarla de "
           "nuevo.";
}
varargs string
chequeo_sencillo_entorno(object jugador, object obj, string poder)
{
    return 0;
}
string *
dame_mensajes_invocar_sombra(object quien, object elemental, string poder)
{
    return (
        {sprintf(
             "Una sombra, lo suficientemente oscura para dos, se derrama de %s"
             ", formando un charco a tus pies. Antes de que pasen "
             "dos respiraciones una silueta humanoide de ojos codiciosos se "
             "alza del charco, reclamando para si toda la negrura antes de "
             "alzarse en toda su altura.",
             TU_S(TO)),
         "",
         sprintf(
             "Una sombra, lo suficientemente oscura para dos, se derrama de %s "
             "de %s, formando un charco a sus pies. Antes de que pasen "
             "dos respiraciones una silueta humanoide de ojos codiciosos se "
             "alza del charco, reclamando para si toda la negrura antes de "
             "alzarse en toda su altura.",
             dame_articulo() + " " + query_short(),
             quien->query_cap_name())});
}
