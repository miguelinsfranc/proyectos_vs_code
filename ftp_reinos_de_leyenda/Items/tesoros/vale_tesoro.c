// Sierephad Dic 2k14
// Eckol Mar15: Reviso la ortografía.
/******************************************************************************
Un vale Tesoro que podra ser canjeado por un jugador para obtener un tesoro de un determinado
nivel y tabla.

Las funciones para configurar el vale una vez clonado son:

//Fijar nivel minimo de tesoro
void fijar_nivel_minimo_vale(int i) 

//Fijar nivel maximo de tesoro
void fijar_nivel_maximo_vale(int i) 

//Fijar numero de tesoros RESTANTES  por los que se podra canjear el vale
//Al usar esta funcion se ajusta tambien automaticamente la cantidad  total de usos.
void fijar_cantidad_vale(int i) 

//Fijar numero de tesoros TOTALES  por los que se podra canjear el vale
void fijar_cantidad_total_vale(int i) 

//Fijar las tablas de tesoro a las k tendran acceso. 
void fijar_tablas_vale(string *t)

//Fijar el random de la tabla de tesoros 0 = manual o 1 = random  
void fijar_tabla_random(int i) 

//Fijar el random del nivel del tesoros  0 = manual o 1 = random  
void fijar_nivel_random(int i) 

//Fijar el random del objeto generado  0 = manual o 1 = random  
void fijar_objeto_random(int i) 

Cada vez que se use una de estas funciones se actualizan las propiedades del objeto, textos, descipciones mensajes.

Ademas tambien esta programada una funcion para generar configuraciones rapidas prefijadas para los vales de tesoros
int fijar_nivel_vale(int i) 

//1 Calidad baja
	3 tesoros de nivel 1 al 4 de las tablas arma y armaduras, seleccion manual para todo
//2 Calidad Media
	2 tesoros de nivel 1 al 8 de las tablas arma, armadura, pergamin y varios, seleccion manual para todo
//3 Calidad alta
	1 tesoros de nivel 1 al 10 de las tablas arma, armadura, pergamin, varios y plantas, seleccion aleatoria para todo


*********************************************************************************/

inherit "/std/item_estado";

#define H_TESOROS  "/handlers/tesoro"
#define LOG "vale_tesoros.log"

//Variables permamentes del objeto que se cargaran con el query_static_auto_load()
private mapping configuracion = allocate_mapping(0);



/*********************************************************************************/
/*			Funciones de generacion de textos y descripciones segun valores		 */
/********************************************************************************/


//Genera el texto con el mensaje a leer
string dame_texo_vale(){
	
	string texto = "";
	
	string texto_tabla = "%^BOLD%^YELLOW%^Manual%^RESET%^";
	string texto_nivel = "%^BOLD%^YELLOW%^Manual%^RESET%^";
	string texto_objeto = "%^BOLD%^YELLOW%^Manual%^RESET%^";
	
	if (configuracion["tabla_random"]) texto_tabla = "%^BOLD%^YELLOW%^Aleatoria%^RESET%^";
	if (configuracion["nivel_random"]) texto_nivel = "%^BOLD%^YELLOW%^Aleatoria%^RESET%^";
	if (configuracion["objeto_random"]) texto_objeto = "%^BOLD%^YELLOW%^Aleatoria%^RESET%^";
	
	texto="Tal y como puedes suponer este vale es un poderoso objeto que ha sido creado "
		"para que el jugador que lo posea pueda obtener un tesoro como recompensa a su esfuerzo.\n\n"
		"Este vale se creó con un total de %^BOLD%^YELLOW%^"+configuracion["cantidad_total_vale"]+"%^RESET%^ cargas de poder, de las cuales %^BOLD%^YELLOW%^"+configuracion["cantidad_vale"]+"%^RESET%^ "
		"aún pueden ser canjeadas por tesoros pertenecientes a las tablas de tesoro: %^BOLD%^RED%^"+nice_list(configuracion["tablas_vale"])+"%^RESET%^, "
		"pudiéndose elegir de entre los niveles %^BOLD%^RED%^"+configuracion["nivel_minimo_vale"]+"%^RESET%^ y "
		"%^BOLD%^RED%^"+configuracion["nivel_maximo_vale"]+"%^RESET%^.\n\n" 
		"La selección de la tabla del tesoro sera: "+texto_tabla+"\n"
		"La selección del nivel del tesoro sera: "+texto_nivel+"\n"
		"La selección del tesoro sera: "+texto_objeto+"\n\n"
		"Utiliza el comando '%^BOLD%^YELLOW%^canjear tesoro|vale%^RESET%^' para canjear una carga del vale por un tesoro.\n\n";

	if (texto) {
		return texto;
	} else {
		return "Tonto el que lo lea.\n";
	}
}

//Genera el texto con la descripcion del objeto 
string dame_descripcion_vale(){
	string texto = "";

	texto="Aparentemente es un sencillo trozo de papel no más grande que la palma de "
		"tu mano, como pudiera ser cualquier cuartilla de propaganda corriente. Sin embargo, "
		"al cogerlo, una extraña sensación te invade a través del tacto de tus dedos, sintiendo "
		"como la energia mística que desprende se canaliza a través de su suave superficie. "
		"Los bordes del papel, decorados con una extraña cenefa rúnica en tonos dorados, se encuentran "
		"impolutos, sin presentar la más mínima muesca o imperfección en el papel. Sin embargo al "
		"mirarlo toda tu atencion recae sobre el centro del papel en donde se puede contemplar "
		"un elaborado dibujo con la forma de una gran T que resplandece y brilla con una mística aura.\n";
		
	if (texto) {
		return texto;
	} else {
		return "Un vale de tesoro sin personalizar.\n";
	}
}

//Genera el short del objeto
string dame_short_vale(){
	string texto = "";
	
	texto="Vale Tesoro";

	if (texto) {
		return texto;
	} else {
		return "Vale Tesoro generico";
	}
}

/*********************************************************************************/
/*			Funciones para cargar y aplicar  los datos en el objeto		 */
/********************************************************************************/

//Funcion para calcular el valor del objeto en tienda
int calcular_valor(){
	
	//Valor base
	int valor=500;
	//Sumamos al valor del vale al nivel maxilo del objeto*5
	valor=valor*(configuracion["nivel_maximo_vale"]*10);
	//Multiplicamos el valor por la cantidad de objetos k obtenemos del vale
	valor=valor+(configuracion["cantidad_vale"]*10);
	//Multiplicamos el valor por el ambito de tablas canjeable.
	valor=valor+(sizeof(configuracion["tablas_vale"])*10);

	return valor;
}


//Funcion para cargar los valores de las variables
void cargar_variables(){
	
	//Valores por defecto de un vale de tesoros de calidad generica "NO USABLE"
	if (!configuracion["nivel_minimo_vale"]) 
		configuracion["nivel_minimo_vale"]=1;
	if (!configuracion["nivel_maximo_vale"]) 
		configuracion["nivel_maximo_vale"]=1;
	if (!configuracion["cantidad_vale"]) 
		configuracion["cantidad_vale"]=1;
	if (!configuracion["cantidad_total_vale"]) 
		configuracion["cantidad_total_vale"]=1;
	if (!configuracion["tabla_random"]) 
		configuracion["tabla_random"]=0;
	if (!configuracion["nivel_random"]) 
		configuracion["nivel_random"]=0;
	if (!configuracion["objeto_random"]) 
		configuracion["objeto_random"]=0;
	if (!configuracion["tablas_vale"]) 
		configuracion["tablas_vale"]=({});
	
}

//Funcion que aplica y actualiza la informacion del objeto
void aplicar_valores(){

	//Generacion de la informacion y textos del objeto
	configuracion["descripcion_vale"]=dame_descripcion_vale();
	configuracion["short_vale"]=dame_short_vale(); 
	configuracion["texo_vale"]=dame_texo_vale(); 

	//Ajuste de las caracteristicas especiales
	set_short(configuracion["short_vale"]);
	set_long(configuracion["descripcion_vale"]);
	fijar_mensaje_leer(configuracion["texo_vale"],0,0);
	fijar_valor(calcular_valor());
	
}


//Carga la configuracion del vale
void cargar_configuracion(){
	cargar_variables();
	aplicar_valores();
}

//Funcion para la consumicion de dosis
void consumir_dosis_tesoro(object player){

	configuracion["cantidad_vale"]=(configuracion["cantidad_vale"]-1);
	if (configuracion["cantidad_vale"]<1) {
		tell_object(player,query_short()+" brilla durante un momento y se consume en tus manos con una ardiente llamarada mágica.\n");
		cargar_configuracion();
		log_file(LOG, ctime()+": "+query_short()+" perteneciente a "+player->query_name()+", se agota por no tener mas cargas de tesoro.\n"); 
		dest_me();
	} else {
		tell_object(player,query_short()+" brilla durante un momento con gran intensidad mágica agotando parte de su poder.\n");
		cargar_configuracion();
	}
	

}

//Funcion para saber si el vale ha sido generado
int vale_generado(){
	
	int control=0;
	if (configuracion["nivel_minimo_vale"] && configuracion["nivel_maximo_vale"] && configuracion["cantidad_vale"] && configuracion["tablas_vale"] ) 
		control=1;
	return control;
}

/*********************************************************************************/
/*				Metodos para guardar y obtener las variables				*/
/*						Dames y fijares							 */
/********************************************************************************/

mixed dame_tablas_vale() { return configuracion["tablas_vale"]; }
int dame_nivel_minimo_vale() { return configuracion["nivel_minimo_vale"]; }
int dame_nivel_maximo_vale() { return configuracion["nivel_maximo_vale"]; }
int dame_cantidad_vale() { return configuracion["cantidad_vale"]; }
int dame_cantidad_total_vale() { return configuracion["cantidad_total_vale"]; }
int dame_tabla_random() { return configuracion["tabla_random"]; }
int dame_nivel_random() { return configuracion["nivel_random"]; }
int dame_objeto_random() { return configuracion["objeto_random"]; }


//Fijar nivel minimo de tesoro
void fijar_nivel_minimo_vale(int i) {
	configuracion["nivel_minimo_vale"]=i;
	cargar_configuracion();
}

//Fijar nivel maximo de tesoro
void fijar_nivel_maximo_vale(int i) {
	configuracion["nivel_maximo_vale"]=i;
	cargar_configuracion();
}

//Fijar numero de tesoros
void fijar_cantidad_vale(int i) {
	configuracion["cantidad_vale"]=i;
	configuracion["cantidad_total_vale"]=i;
	cargar_configuracion();
}

//Fijar numero de tesoros totales
void fijar_cantidad_total_vale(int i) {
	configuracion["cantidad_total_vale"]=i;
	cargar_configuracion();
}

//Fijar las tablas de tesoro a las k tendran acceso.
void fijar_tablas_vale(string *t){ 
	configuracion["tablas_vale"] = t; 
	cargar_configuracion();
}

//Fijar el random de la tabla de tesoros 0 o 1
void fijar_tabla_random(int i){ 
	configuracion["tabla_random"] = i; 
	cargar_configuracion();
}

//Fijar el random del nivel del tesoros 0 o 1
void fijar_nivel_random(int i){ 
	configuracion["nivel_random"] = i; 
	cargar_configuracion();
}

//Fijar el random del objeto generado 0 o 1
void fijar_objeto_random(int i){ 
	configuracion["objeto_random"] = i; 
	cargar_configuracion();
}


//Funcion prefijadas para establecer rapidamente calidades predefinidas para los vales
//1 Calidad baja
//2 Calidad Media
//3 Calidad alta
int fijar_nivel_vale(int i) {
	
	if (i>=1 && i<=3) {

		if (i==1){
			fijar_nivel_minimo_vale(1);
			fijar_nivel_maximo_vale(4);
			fijar_cantidad_vale(3);
			fijar_tablas_vale(({"arma","armadura"}));
			fijar_tabla_random(0);
			fijar_nivel_random(0);
			fijar_objeto_random(0);
		} else if (i==2){
			fijar_nivel_minimo_vale(1);
			fijar_nivel_maximo_vale(8);
			fijar_cantidad_vale(2);
			fijar_tabla_random(0);
			fijar_nivel_random(0);
			fijar_objeto_random(0);
			fijar_tablas_vale(({"arma","armadura","pergamin","varios"}));
		} else if (i==3){
			fijar_nivel_minimo_vale(1);
			fijar_nivel_maximo_vale(10);
			fijar_cantidad_vale(1);
			fijar_tablas_vale(({"arma","armadura","pergamin","varios","plantas"}));
			fijar_tabla_random(1);
			fijar_nivel_random(1);
			fijar_objeto_random(1);
		}
		cargar_configuracion();
		return 1;
	} else {
		return 0;
	}
}

/*********************************************************************************/
/*				Setup y otras funciones generales dle objeto				*/
/********************************************************************************/


//setup
void setup(){

	set_name("vale tesoro");
	set_short("Vale Tesoro generico");
	set_long("Un vale de tesoro sin personalizar.\n");
	set_main_plural("Vales Tesoro");
	add_alias(({"vale","tesoro"}));
	add_plural(({"vales","tesoros"}));
	
	fijar_vida_max(800);
    fijar_material(8); // papel
	fijar_peso(10);
	fijar_valor(500);

	reset_drop();
	
}

//Funcion para guardar los valores cuando el objeto sale
mapping query_static_auto_load()
{
    mapping datos;

    datos = ::query_static_auto_load();
	
	if (configuracion)
		datos=configuracion;
    
    return datos;
}

//Funcion para cargar lso valores al cargar el objeto.
void init_static_arg(mapping map)
{
    ::init_static_arg(map);
	
	if (map){
		configuracion=map;
		cargar_configuracion();
	}

}


//Se añade el comando para canjear el vale
void init(){
	anyadir_comando_inventario("canjear","[tesoro|vale]","canjear_tesoro","Canjea el "+query_short()+" por objetos provenientes de los dioses.\n");
}

//Funcion que general el objeto y se lo entrega al player
void respuesta_recompensa_final(string r_tabla,int r_nivel,string r_objeto,object jugador,mixed respuesta) {

	int alt;
	
	if (respuesta == "no") {
        tell_object(jugador, "\nDeja de hacerme perder el tiempo mortal...\n");
    }
	if (respuesta == "si") {
		
		if ( ! jugador->entregar_objeto(r_objeto)) {
			return notify_fail("Error al generar el objeto, contacta con el CdJ\n");
		} else {
			alt=configuracion["cantidad_vale"]-1;
			log_file(LOG, ctime()+": "+query_short()+" perteneciente a "+jugador->query_name()+", genera un tesoro "+r_tabla+" de nivel "+r_nivel+": "+r_objeto+".  Se consume 1 carga, quedan "+alt+"/"+configuracion["cantidad_total_vale"]+".\n"); 
            event_users(
                "inform", 
                jugador->query_cap_name() + " canjea un vale tesoro y obtiene " + base_name(r_objeto) + ".", "trampa"
            );
            consumir_dosis_tesoro(jugador);
		}
    }
	
	return;
}

//Funcion que inicia el menu para para la comprobacion final antes de crear el objeto
void respuesta_recompensa_comprobacion(string r_tabla,int r_nivel,string r_objeto,object jugador,mixed respuesta) {

	string recompensa_objeto;
	string texto="";
	
	recompensa_objeto=r_objeto;
	
	texto="\nMortal, tu recompensa esta preparada.\n";
	
	if (respuesta=="Continuar" || respuesta=="continuar") {
		tell_object(jugador,texto);
		jugador->mostrar_dialogo(
			"\n¿Es esto realmente lo que deseas?\n",
			({"si", "no"}),
			(:respuesta_recompensa_final,r_tabla,r_nivel,recompensa_objeto:)
		);
	} else {
		recompensa_objeto=respuesta;
		if (configuracion["objeto_random"]==0 && configuracion["tabla_random"]==0 && configuracion["nivel_random"]==0)
			texto="\nMortal, tu deseo es conseguir "+H_TESOROS->dame_short_tesoro(recompensa_objeto)+" del grupo "+r_tabla+" de nivel "+r_nivel+".\n";
		tell_object(jugador,texto);
		jugador->mostrar_dialogo(
			"\n¿Es esto realmente lo que deseas?\n",
			({"si", "no"}),
			(:respuesta_recompensa_final,r_tabla,r_nivel,recompensa_objeto:)
		);
	}
	return;
}

//Funcion que inicia el menu para seleccionar el tesoro soro final, manual o aleatorio
void respuesta_recompensa_objeto(string r_tabla,object jugador,mixed respuesta) {

	string *comprobacion_nivel_2=allocate(0);
	string *tesoros=allocate(0);
	string *m_tesoros=allocate(0);

	
	int recompensa_nivel=0;
	string recompensa_objeto="";
	
	int alt,alt2,i;
	string nombre_tesoro="";

	if (respuesta=="Continuar" || respuesta=="continuar") {
		alt2=(configuracion["nivel_maximo_vale"]-configuracion["nivel_minimo_vale"])+1;
		respuesta="Nivel "+((random(alt2)+configuracion["nivel_minimo_vale"]));
	}

	comprobacion_nivel_2=explode(respuesta," ");
	alt=to_int(comprobacion_nivel_2[1]);
	recompensa_nivel=alt;

	if (alt>=configuracion["nivel_minimo_vale"] && alt<=configuracion["nivel_maximo_vale"]) {

		tesoros=H_TESOROS->dame_tesoros(r_tabla,recompensa_nivel);

		for (i=0;i<sizeof(tesoros);i++) {
			nombre_tesoro=H_TESOROS->dame_short_tesoro(tesoros[i]);
			if (nombre_tesoro) 
				m_tesoros+=({({tesoros[i],nombre_tesoro})});
		}

		if (configuracion["objeto_random"]==1) {
			recompensa_objeto=tesoros[random(sizeof(tesoros))];
			jugador->mostrar_dialogo(
				"\nTu recompensa se generara aleatoriamente de entre los tesoros disponibles de las tablas y rangos previamente seleccionados.\n",
				({"Continuar"}),
				(:respuesta_recompensa_comprobacion,r_tabla,recompensa_nivel,recompensa_objeto:)
			);
		} else {
			if (sizeof(tesoros)>0) {
				jugador->mostrar_dialogo(
					"\nElige finalmente tu recompensa.\n",
					m_tesoros,
					(:respuesta_recompensa_comprobacion,r_tabla,recompensa_nivel,"":),
					0,
					0,
					1
				);
			}
		}
	} 
	return;
}

//Funcion que inicia el menu para seleccionar el nivel del tesoro manual o aleatorio
void respuesta_recompensa_nivel(object jugador,mixed respuesta) {
	
	string *nivel_objeto = ({});
	int i;
	string recompensa_tabla="";

	if (respuesta=="Continuar" || respuesta=="continuar") {
		respuesta=configuracion["tablas_vale"][random(sizeof(configuracion["tablas_vale"]))];
	}
	
	if (member_array(respuesta,configuracion["tablas_vale"])>=0) {
		
		recompensa_tabla=respuesta;
		
		for (i=configuracion["nivel_minimo_vale"];i<=configuracion["nivel_maximo_vale"];i++) {
			nivel_objeto += ({"Nivel "+i});
		}

		if (configuracion["nivel_random"]==1) {
			jugador->mostrar_dialogo(
				"\nTu recompensa se generara aleatoriamente con un nivel comprendido entre: "+configuracion["nivel_minimo_vale"]+" y "+configuracion["nivel_maximo_vale"]+"\n",
				({"Continuar"}),
				(:respuesta_recompensa_objeto,recompensa_tabla:)
			);
		} else {
			jugador->mostrar_dialogo(
				"\nElige con cuidado el nivel de tu recompensa.\n",
				nivel_objeto,
				(:respuesta_recompensa_objeto,recompensa_tabla:)
			);
		}
	}
    return;
}

//Funcion que inicia el menu para seleccionar la tabla del tesoro manual o aleatorio
void respuesta_recompensa_tabla(object jugador) {

	if (configuracion["tabla_random"]==1) {
		jugador->mostrar_dialogo(
			"\nTu recompensa se generara aleatoriamente de entre los siguientes grupos de tesoros: "+nice_list(configuracion["tablas_vale"])+"\n",
			({"Continuar"}),
			(:respuesta_recompensa_nivel:)
		);
	} else {
		jugador->mostrar_dialogo(
			"\nElige sabiamente que tipo de recompensa deseas.\n",
			configuracion["tablas_vale"],
			(:respuesta_recompensa_nivel:)
		);
	}
	return;
}

//Funcion que realiza las comprobaciones antes de lanzar el menu de seleccion
int canjear_tesoro(){

    if (TP && vale_generado()) {
		tell_object(TP,"\nMortal, los dioses te han concedido un gran tesoro como recompensa.\n");
        respuesta_recompensa_tabla(TP);
		return 1;
    } else {
		return 0;
	}
}







