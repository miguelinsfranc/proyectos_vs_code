// Satyr 16.11.2015 
inherit "/obj/paquete.c";

void fijar_tamanyo_bolsa(string tx);

void setup()
{
    fijar_genero(2);
    fijar_material(4);
    set_name("bolsa");
    add_plural("bolsas");

    
    fijar_tamanyo_bolsa(0);
}

void fijar_tamanyo_bolsa(string tx)
{
    
    switch(tx) {
        case "pequeña":
            fijar_dinero_paquete(25 + roll(1, 25));
            break;
        
        case "grande":
            fijar_dinero_paquete(75 + roll(1, 75));
            break;
            
        case "gigante":
            fijar_dinero_paquete(100 + roll(1, 100));
            break;
            
        case "descomunal":
            fijar_dinero_paquete(200 + roll(1, 100));
            break;
            
        default:
            tx = "";
            fijar_dinero_paquete(50 + roll(1, 50));
            break;
    }

    if ( tx != "" )
        tx = " " + tx;

    set_short("Bolsa" + tx + " de monedas");
    set_main_plural("Bolsas" + pluralize(tx) + " de monedas");
    add_alias(tx);
    add_plural(pluralize(tx));
    set_long("Una" + tx + " bolsa de cuero repleta monedas que "
        "está cerrada con la ayuda de un cordón de cuero. Pocas cosas "
        "tan gratificantes conoce el hombre como sentir el peso y escuchar "
        "el tintineo "
        "de una de éstos paquetes en tus manos. Para ver su contenido, la tendrás que %^BOLD%^WHITE%^abrir%^RESET%^.\n"
    );
    fijar_dinero_paquete(dame_dinero_paquete() * 500);
}
