// Satyr 17.11.2015 
// void nuevo_objeto_paquete(string str, int cantidad)
/*

5: 20k: opalo, azabache
6: 14k: turquesa
7: rubi, esmeralda, aguamarina
8: amatista, zafiro
9: diamante

"diamante"     : ({37500, 85, 0, ({"bien", "mal", "miedo", "dureza"}) }),
"rubi"         : ({21000, 85, 0, ({"fuego", "miedo"}) }),
"esmeralda"    : ({20000, 75, 0, ({"veneno", "enfermedad"}) }),
"zafiro"       : ({18000, 70, 0, ({"aire"}) }),
"azabache"     : ({15000, 70, 0, ({"mal", "oscuridad"}) }),
"turquesa"     : ({14000, 60, 1, ({"mal", "curacion"}) }),
"amatista"     : ({14000, 60, 0, ({"magico", "miedo"}) }),
"aguamarina"   : ({10000, 60, 0, ({"agua"}) }),
"olivina"      : ({10000, 60, 0, ({"resistencia"}) }),
"perla"        : ({5000,  60, 1, ({"agua"}) }),
"topacio"      : ({5000,  55, 0, ({"fuerza", "acido"}) }),
"jade"         : ({5000,  40, 0, ({"mal"}) }),
"opalo"        : ({5000,  80, 1, ({"agilidad"}) }),
*/
#include <baseobs_path.h>
#define TABLA "/table/tabla_gemas.c"
#define EXCLUIDAS ({"perla", "perla_gigante", "ojo_tremulo", "diamante"})
inherit "/obj/paquete.c";

void fijar_tamanyo_bolsa(string tx);

void setup()
{
    fijar_genero(2);
    fijar_material(4);
    set_name("bolsa");
    add_plural("bolsas");

    
    fijar_tamanyo_bolsa(0);
}

void fijar_tamanyo_bolsa(string tx)
{
    string *gemas;
    mapping gemas_m;
    
    switch(tx) {
        case "pequeña":
            gemas = TABLA->dame_gemas_aleatorias_por_valor(25 * 500, EXCLUIDAS);
            break;

        case "grande":
            gemas = TABLA->dame_gemas_aleatorias_por_valor(75 * 500, EXCLUIDAS);
            break;

        case "gigante":
            gemas = TABLA->dame_gemas_caras_por_valor(100 * 500, EXCLUIDAS);
            break;
            
        case "descomunal":
            //gemas = TABLA->dame_gemas_caras_por_valor(100*500, EXCLUIDAS);
            gemas = TABLA->dame_gemas_caras_por_valor(125 * 500, EXCLUIDAS - ({"diamante"}));
            break;
            
        default:
            tx = "";
            gemas = TABLA->dame_gemas_aleatorias_por_valor(50*500, EXCLUIDAS);
            break;
    }

    if ( tx != "" )
        tx = " " + tx;

    set_short("Bolsa" + tx + " de joyas");
    set_main_plural("Bolsas" + pluralize(tx) + " de joyas");
    add_alias(tx);
    add_plural(pluralize(tx));
    set_long("Una" + tx + " bolsa de cuero repleta joyas que "
        "está cerrada con la ayuda de un cordón de cuero. Esta "
        "bolsita encierra una pequeña fortuna. Para ver su contenido, la tendrás que %^BOLD%^WHITE%^abrir%^RESET%^.\n"
    );

    if ( ! sizeof(gemas) )
        return;
        
    gemas_m = ([]);
    
    foreach(string gema in gemas) {
        if ( ! gemas_m[gema] )
            gemas_m[gema] = 0;
            
        gemas_m[gema]++;
    }
    
    resetear_objetos_paquete();
    
    foreach(string gema, int cantidad in gemas_m)
        nuevo_objeto_paquete(BJOYAS + gema + ".c", cantidad);
}
