// Satyr 17.11.2015 
/*
7: mithril, berilio
6: cobre, cristal, plata, pandereta, compendio adurn,
5: hierro
4: cuarzo, magnesio
*/
#include <baseobs_path.h>
inherit "/obj/paquete.c";

void fijar_tamanyo_bolsa(string tx);

void setup()
{
    fijar_genero(2);
    fijar_material(4);
    set_name("bolsa");
    add_plural("bolsas");

    
    fijar_tamanyo_bolsa(0);
}

void fijar_tamanyo_bolsa(string tx)
{
    string *minerales;
    int cantidad;
    mapping mp;
    string path;
    switch(tx) {
        case "pequeña":
            minerales = ({"carbon", "cobre", "azufre"});
            cantidad  = roll(1, 2);
            break;

        case "grande":
            minerales = ({"hierro", "cobre", "mithmel"});
            cantidad  = 4;
            break;

        case "gigante":
            minerales = ({"hierro", "plata", "mithmel", "mithril", "cobre"});
            cantidad  = 5;
            break;
            
        case "descomunal":
            minerales = ({"oro", "plata", "hierro", "mithril"});
            path      = BLINGOTES;
            cantidad  = 1;
            break;
            
        default:
            tx = "";
            minerales = ({"carbon", "cobre", "hierro"});
            cantidad  = 3;
            break;
    }

    mp = ([]);
    
    while(cantidad--) {
        string aux = element_of(minerales);
        
        if ( ! mp[aux] )
            mp[aux] = 0;
            
        mp[aux]++;
    }
    
    if ( tx != "" )
        tx = " " + tx;

    set_short("Bolsa" + tx + " llena de minerales");
    set_main_plural("Bolsas" + pluralize(tx) + " llenas de minerales");
    add_alias(({"minerales", tx}));
    add_plural(pluralize(tx));
    set_long("Una" + tx + " bolsa de cuero repleta de minerales "
        "cerrada con unas fuertes correas. Es muy resistente, pero "
        "pronto cederá ante el peso de su contenido. Para ver su contenido, la tendrás que %^BOLD%^WHITE%^abrir%^RESET%^.\n"
    );

    resetear_objetos_paquete();
    
    if ( ! path )
        path = BMINERALES;
        
    foreach(string que, int i in mp)
        nuevo_objeto_paquete(path + que + ".c", i);
}
