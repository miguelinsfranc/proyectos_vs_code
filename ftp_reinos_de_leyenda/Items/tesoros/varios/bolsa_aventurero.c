// Satyr 17.11.2015
#include <baseobs_path.h>
inherit "/obj/paquete.c";

void fijar_tamanyo_bolsa(string tx);

void setup()
{
    fijar_genero(2);
    fijar_material(4);
    set_name("bolsa");
    add_plural("bolsas");

    fijar_tamanyo_bolsa(0);
}

void fijar_tamanyo_bolsa(string tx)
{
    mapping mp;
    int     dinero;
    switch (tx) {
            /*
            "yavethalion", "gefnul"
                 Nivel 10:
             Winclamit

              Nivel 9:
             Hojas de Marihuana

              Nivel 8:
             Yavethalion

              Nivel 7:
             Arena de Annj y Polvo Estelar

              Nivel 6:
             Gefnul, Yavethalion y Eskumla

              Nivel 5:
             Darsurion, Dugmuthur, Reglen, Thyrya y Hoja de Khadul

              Nivel 4:
             Mirenna y Ensue�o

              Nivel 3:
             Anthalas, Dursha, Gariig y Laudano

              Nivel 2:
             Akbutege, Arlan, Alhova, Rewk, Mu�rdago, Lishka, Ra�z de Klerm y
            Shalomn

              Nivel 1:
             Baya silvestre, Thurl, Tabaco y Fruto de Walahm
             */
        case "novato":
            dinero = 10;
            mp =
                ([BPLANTAS +
                    element_of(({"baya", "tabaco", "arlan", "alhova"})) +
                               ".c":1, BCOMESTIBLES + "pan.c":2, BCOMESTIBLES +
                    "hamburguesa.c":1, BCOMESTIBLES +
                         "filete.c":1,
                                    BMISC +
                                        element_of(
                                            ({"venda",
                                              "antorcha",
                                              "odre",
                                              "arpeo"})) +
                               ".c":1,
                                    BPOCIONES +
                                        element_of(
                                            ({"elixir_cicatrizacion",
                                              "elixir_curacion",
                                              "elixir_energia"})) +
                               ".c":1, ]);
            break;

        case "avanzado":
            dinero = 40;
            mp =
                ([BPLANTAS + element_of(({"yavethalion", "gefnul"})) +
                          ".c":1, BCOMESTIBLES +
                    "filete.c":4,
                               BMISC +
                                   element_of(
                                       ({"venda",
                                         "antorcha",
                                         "odre",
                                         "arpeo"})) +
                          ".c":3,
                               BPOCIONES +
                                   element_of(
                                       ({"elixir_curacion",
                                         "elixir_revitalizacion",
                                         "elixir_energia",
                                         "pocion_carisma",
                                         "pocion_con",
                                         "pocion_destreza",
                                         "pocion_fuerza",
                                         "pocion_inteligencia",
                                         "pocion_sabiduria"})) +
                          ".c":1, ]);
            break;

        case "experimentado":
            dinero = 80;
            mp =
                ([BPLANTAS +
                    element_of(({"yavethalion", "gefnul", "winclamit"})) +
                                 ".c":1, BCOMESTIBLES +
                    "pollo_cerveza.c":4,
                                      BMISC +
                                          element_of(
                                              ({"venda",
                                                "antorcha",
                                                "odre",
                                                "arpeo",
                                                "frasco_magico"})) +
                                 ".c":4,
                                      BPOCIONES +
                                          element_of(
                                              ({"pocion_carisma",
                                                "elixir_salud",
                                                "elixir_revitalizacion",
                                                "pocion_mayor_carisma",
                                                "pocion_mayor_con",
                                                "pocion_mayor_destreza",
                                                "pocion_mayor_fuerza",
                                                "pocion_mayor_inteligencia",
                                                "pocion_mayor_sabiduria"})) +
                                 ".c":1, ]);
            break;

        default:
            tx     = "";
            dinero = 25;
            mp =
                ([BPLANTAS + element_of(({"anthalas", "arlan", "alhova"})) +
                          ".c":1, BCOMESTIBLES + "leche.c":2, BCOMESTIBLES +
                    "filete.c":2,
                               BMISC +
                                   element_of(
                                       ({"venda",
                                         "antorcha",
                                         "odre",
                                         "arpeo"})) +
                          ".c":2,
                               BPOCIONES +
                                   element_of(
                                       ({"elixir_cicatrizacion",
                                         "elixir_curacion",
                                         "elixir_revitalizacion",
                                         "elixir_energia"})) +
                          ".c":1, ]);
            break;
    }

    if (tx != "")
        tx = " " + tx;

    set_short("Bolsa del aventurero" + tx);
    set_main_plural("Bolsas del aventurero" + tx);
    add_alias(tx);
    add_plural(pluralize(tx));
    set_long(
        "Una bolsa de cuero repleta de utensilios "
        "y pociones para los aventureros " +
        tx +
        ".\n"
        "Para ver su contenido, la tienes que "
        "%^BOLD%^WHITE%^abrir%^RESET%^.\n");

    resetear_objetos_paquete();

    foreach (string a, int cantidad in mp)
        nuevo_objeto_paquete(a, cantidad);

    fijar_dinero_paquete(dinero * 500);
}
