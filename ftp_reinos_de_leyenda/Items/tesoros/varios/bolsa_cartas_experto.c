// Satyr 17.11.2015 
#include <baseobs_path.h>
#define TIPOS ({"luz", "oscuridad", "caos", "equilibrio"})
inherit "/obj/paquete.c";

/*
10 - Expertos
8  - Experimentados
6  - Avanzados
4  - Principiantes
*/
void fijar_tamanyo_bolsa(string tx);

void setup()
{
    fijar_genero(2);
    fijar_material(4);
    set_name("bolsa");
    add_plural("bolsas");

    
    fijar_tamanyo_bolsa("experto");
}

void fijar_tamanyo_bolsa(string tx)
{
    string premio;
    
    if ( -1 == member_array(tx, ({"principiante", "avanzado", "experimentado", "experto"})) )
        error("Tipo de bolsa para cartas inválido: '" + tx + "'.");

    premio = "sobre_" + element_of(TIPOS) + "_" + tx + ".c";

    set_short("Bolsa de cartas para " + tx + "s");
    set_main_plural("Bolsas de cartas para " + tx+"s");
    add_alias(tx+"s");
    add_plural(pluralize(tx));
    set_long("Una bolsa repleta de cartas de Mystic para " + tx + "s.\n");

    nuevo_objeto_paquete(BCARTAS + premio, 1);
}