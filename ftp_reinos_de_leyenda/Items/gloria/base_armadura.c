// Satyr 21.06.2016
/* Eckol Feb21:
    Todos los comandos comprueban ahora que se estén ejecutando sobre este
   objeto, puesto que todos lo asumen en varios puntos. Antes se podía dar el
   caso de ejecutar el comando de un objeto sobre otro. Nota: mantengo que los
   comandos reciban el objeto, puesto que si no el jugador no podrá elegir cual
   aplica.
*/
#include <pk.h>
#include <dinero.h>
#include <depuracion.h>
#include <bd_equipo.h>
inherit GLORIA_OBJETOS + "base_objeto_gloria.c";
inherit "/obj/multiobjetos/multiarmadura.c";

int no_catalogar_armeria()
{
    return __FILE__ == base_name();
}

void create()
{
    base_objeto_gloria::create();
    multiarmadura::create();
    fijar_tipos_validos(allocate(0));

    if (base_name() + ".c" != __FILE__) {
        TO->setup_gloria();
        add_static_property("imperdible", 1);
    }
}

void actualizar_genero()
{
    ::actualizar_genero();

    if (!dame_multiobjeto_base())
        fijar_genero(2);
}
void permitir_estorbo(string tx)
{
    nuevo_tipo_valido(GLORIA_TABLA_OBJETOS->dame_objetos_base_por_tipo(tx));
}
void permitir_todos()
{
    foreach (string tx in({"tela", "cuero", "mallas", "placas"}))
        permitir_estorbo(tx);
}
void setup_gloria()
{
    permitir_todos();
}
void setup_especifico()
{
    GLORIA_TABLA_OBJETOS->preparar_pieza(TO);
}
string *dame_nombres_para(string *tx)
{
    string  nivel = dame_nombre_poder();
    string  tipo  = dame_tipo_equipo_gloria();
    string *ret   = allocate(2);

    ret[0] = tx[0] + " " + (nivel && nivel != "" ? nivel + " " : "") + tipo;

    nivel = dame_genero() >= 0 ? nivel : dame_nombre_poder(-dame_genero());

    ret[1] = tx[1] + " " + (nivel && nivel != "" ? nivel + " " : "") + tipo;

    return ret;
}
void poner_short_generico_multiobjeto()
{
    string *names = dame_nombres_para(({"Multiarmadura", "Multiarmaduras"}));

    set_short(names[0]);
    set_main_plural(names[1]);
}
void poner_short_especifico_multiobjeto()
{
    string *names = dame_nombres_para(
        ({capitalize(dame_multiobjeto_base()),
          capitalize(pluralize(dame_multiobjeto_base()))}));

    set_short(colorear_corto(names[0]));
    set_main_plural(colorear_corto(names[1]));

    actualizar_short_lado();
}
int dame_valor()
{
    return base_objeto_gloria::dame_valor();
}
int query_value()
{
    return base_objeto_gloria::query_value();
}
string dame_descripcion_gloria()
{
    switch (dame_tipo_equipo_gloria()) {
        case GLORIA_TIPO_GIGANTE:
            return "runas defensivas con una escritura gótica";

        case GLORIA_TIPO_COLERA:
            return "escrituras mágicas que se asemejan a frenéticos arañazos";

        case GLORIA_TIPO_ETER:
            return "runas mágicas translúcidas que parecen flotar por su "
                   "superficie";

        case GLORIA_TIPO_DUELISTA:
            return "hechizos de presteza, celeridad y precognición limitada";

        case GLORIA_TIPO_PICARO:
            return "oscuras y misteriosas runas que irradian malignidad";

        default:
            return "runas mágicas";
    }
}
void poner_descripcion_generica_multiobjeto()
{
    string tx = dame_descripcion_gloria();

    set_long(
        "Un orbe dorado adornado con " + tx +
        ". Su interior alberga energías "
        "caprichosas que le permiten ser transmutado en una armadura " +
        ((tx = lower_case(dame_nombre_poder())) != "" ? tx + " " : "") +
        "destinada a los héroes más gloriosos de los Reinos de Leyenda.\n"
        "Tiene un coste de gloria de " +
        dame_coste_gloria() + " puntos.\n");
}
void poner_descripcion_especifica_multiobjeto()
{
    set_long(
        capitalize(dame_numeral()) + " " + dame_multiobjeto_base() +
        " que ha sido " + dame_texto_forjado() +
        " por los dioses como recompensa para los héroes "
        "más gloriosos de Eirea. " +
        capitalize(dame_descripcion_gloria()) +
        " "
        "envuelven " +
        dame_este() + " armadura ");

    if (dame_propietario())
        set_long(
            query_long() + "que parece pertenecer a " +
            capitalize(dame_propietario()) + ".\n");
    else
        set_long(query_long() + "que parece no pertenecer a nadie.\n");
}
string long(string tx, int oscuridad)
{
    tx = ::long(tx, oscuridad);

    if (!tx || tx == "" || !dame_propietario())
        return tx;

    return tx += "Pertenece a " + capitalize(dame_propietario()) + ".\n";
}
int set_in_use(int i)
{
    if (i && !es_propietario(environment())) {
        tell_object(
            environment(),
            query_short() + " pertenece a " + dame_propietario() +
                " y no puedes "
                "usarl" +
                dame_vocal() + " en su nombre.\n");
        return 0;
    }

    return ::set_in_use(i);
}
varargs int
glorificar_fin(object item, int gloria, int dinero, object pj, string respuesta)
{
    string err, path;
    object nuevo;

    if (!pj || respuesta == "no")
        return 0;

    if (!item || environment(item) != pj)
        err = "El objeto que pretendías mejorar ha desaparecido.";
    else if (pj->dame_gloria_saldo() < gloria)
        err = "No tienes suficiente gloria.";
    else if (pj->query_value() < dinero * MONEDAS["platino"])
        err = "No tienes suficiente dinero.";

    if (err) {
        tell_object(pj, err + "\n");
        return 0;
    }

    if (2 != sscanf(base_name(item), "%s%*d", path))
        path = base_name(item) + "_" + 2;
    else
        path += item->dame_poder_equipo_gloria() + 1;

    if (!nuevo = pj->entregar_objeto(path + ".c")) {
        tell_object(pj, "Un error impide mejorar el objeto.");
        return 0;
    }

    pj->ajustar_gloria_saldo(-gloria);
    pj->pagar_dinero(dinero, "platino");

    log_file(
        "gloria/mejoras_gloria.log",
        ctime() + ": " + pj->query_cap_name() + " mejora " + base_name(item) +
            " y "
            "consigue " +
            base_name(nuevo) + " por " + gloria + "/" + dinero + ".\n");
    __debug_x(
        pj->query_cap_name() + " mejora " + base_name(item) +
            " y "
            "consigue " +
            base_name(nuevo) + " por " + gloria + "/" + dinero + ".",
        ({"gloria-todo", "objetos-gloria"}));

    if (item->query_in_use())
        pj->unwear_ob(item);

    nuevo->fijar_propietario(pj->query_name());
    item->add_property("usado");
    item->dest_me();
}
varargs int reconvertir(object b)
{
    if (!b)
        return notify_fail("¿Qué quieres " + query_verb() + "?\n");

    if (!b->dame_objeto_gloria())
        return notify_fail(
            "Solo puedes " + query_verb() + " objetos de gloria.\n");

    if (!b->es_propietario(TP))
        return notify_fail(
            "No eres el propietario de " + b->query_short() + ".\n");

    if (!b->dame_multiobjeto_base())
        return notify_fail(
            b->query_short() +
            " ya se ha reconvertido en una multiarmadura.\n");

    if (b != TO)
        return 0; // Pasaremos a ejecutar glorificar en otros objetos

    tell_object(TP, "Reconviertes " + b->query_short() + " en ");
    TP->unwear_ob(b);

    // Satyr 01.07.2016: Esto es muy sucio y muy feo, pero no voy a meter un
    // fijar_... en armadura que ahora no tengo ganas.
    armour_name = "armadura";

    actualizar_multiobjeto();
    fijar_valor_multiobjeto(
        "lado",
        0); // Eckol Feb21: para que puedas reconvertir en guante de otro lado
    tell_object(TP, b->dame_numeral() + " " + b->query_short() + ".\n");
    init();
    return 1;
}
varargs int glorificar(object b)
{
    int gloria, dinero;

    if (!b)
        return notify_fail("¿Qué quieres " + query_verb() + "?\n");

    if (b->query_property("usado"))
        return notify_fail(
            b->query_short() + " ya se intentó " + query_verb() +
            " en el "
            "pasado y no puede actualizarse de nuevo.\n");

    if (!b->dame_objeto_gloria())
        return notify_fail(
            "Solo puedes " + query_verb() + " objetos de gloria.\n");

    if (!b->es_propietario(TP))
        return notify_fail(
            "No eres el propietario de " + b->query_short() + ".\n");

    if (b->dame_poder_equipo_gloria() >=
        GLORIA_TABLA_OBJETOS->dame_max_niveles_mejora())
        return notify_fail(b->query_short() + " no puede mejorarse más.\n");

    if (b != TO)
        return 0; // Pasaremos a ejecutar glorificar en otros objetos

    gloria = GLORIA_TABLA_OBJETOS->dame_coste_gloria(
        b->dame_poder_equipo_gloria() + 1);
    dinero = GLORIA_TABLA_OBJETOS->dame_coste_platinos_riesgo(
        TP->dame_riesgo(), b->dame_poder_equipo_gloria() + 1);

    TP->mostrar_dialogo(
        "Mejorar " + b->query_short() + " costará " + gloria +
            " puntos de gloria y " + dinero + " moneda" +
            (dinero > 1 ? "s" : "") +
            " de platino. ¿Quieres "
            "continuar?",
        ({"si", "no"}),
        (
            : glorificar_fin, b, gloria, dinero:));
    return 1;
}
varargs int
desglorificar_fin(object item, int dinero, object pj, string respuesta)
{
    string err;
    object nuevo;

    if (!pj || !respuesta)
        return 0;

    if (!item || environment(item) != pj)
        err = "El objeto que pretendías mejorar ha desaparecido.";
    else if (dinero && pj->query_value() < dinero * MONEDAS["platino"])
        err = "No tienes suficiente dinero.";

    if (err) {
        tell_object(pj, err + "\n");
        return 0;
    }

    if (!nuevo = pj->entregar_objeto(respuesta)) {
        tell_object(pj, "Un error impide cambiar el objeto.");
        return 0;
    }

    if (dinero)
        pj->pagar_dinero(dinero, "platino");

    log_file(
        "gloria/cambios_gloria.log",
        ctime() + ": " + pj->query_cap_name() + " cambia " + base_name(item) +
            " por " + base_name(nuevo) + " por " + dinero + ".\n");
    __debug_x(
        pj->query_cap_name() + " cambia " + base_name(item) + " por " +
            base_name(nuevo) + " por " + dinero + ".\n",
        ({"gloria-todo", "objetos-gloria"}));

    if (item->query_in_use())
        pj->unwear_ob(item);

    nuevo->fijar_propietario(pj->query_name());
    item->add_property("usado");
    item->dest_me();
}
string *dame_rutas_otros_objetos()
{
    string *ficheros = get_dir(GLORIA_OBJETOS + "multiarmadura_*.c");
    string *ret      = allocate(0);
    foreach (string file in ficheros) {
        int nivel;
        int tipo;

        if (file == obtener_info_ruta(TO) + ".c")
            continue;

        if (sscanf(file, "multiarmadura_%s_%d.c", tipo, nivel) != 2) {
            nivel = 1;
            if (sscanf(file, "multiarmadura_%s.c", tipo) != 1)
                continue;
        }

        if (!tipo)
            continue;

        if (nivel != dame_poder_equipo_gloria())
            continue;

        ret += ({GLORIA_OBJETOS + file});
    }

    return ret;
}

varargs int desglorificar(object b)
{
    int   dinero;
    mixed resp;

    if (!b)
        return notify_fail("¿Qué quieres " + query_verb() + "?\n");

    if (b->query_property("usado"))
        return notify_fail(
            b->query_short() + " ya se intentó " + query_verb() +
            " en el "
            "pasado y no puede actualizarse de nuevo.\n");

    if (!b->dame_objeto_gloria())
        return notify_fail(
            "Solo puedes " + query_verb() + " objetos de gloria.\n");

    if (!b->es_propietario(TP))
        return notify_fail(
            "No eres el propietario de " + b->query_short() + ".\n");

    if (b != TO)
        return 0; // Pasaremos a ejecutar glorificar en otros objetos

    dinero =
        GLORIA_TABLA_OBJETOS->dame_coste_platinos_reconvertir_riesgo_objeto(
            TP->dame_riesgo(), dame_poder_equipo_gloria(), TO);

    resp = allocate(0);

    foreach (string tipo in dame_rutas_otros_objetos()) {
        resp += ({({tipo, load_object(tipo)->query_short()})});
    }

    TP->mostrar_dialogo(
        capitalize(query_verb()) + " " + b->query_short() + " te costará " +
            dinero +
            " "
            " moneda" +
            (dinero > 1 ? "s" : "") +
            " de platino. ¿En qué tipo de "
            "objeto lo quieres convertir?",
        resp,
        (
            : desglorificar_fin, b, dinero:),
        0,
        0,
        1);

    return 1;
}

void init()
{
    ::init();

    anyadir_comando(
        "glorificar",
        //"<objeto:no_interactivo:yo:simple:directo'Pieza de Gloria a
        //mejorar'>",
        "<objeto:no_interactivo:yo:simple'Pieza de Gloria a mejorar'>",
        (
            : glorificar:),
        "Intenta mejorar una pieza de gloria que especifiques.");
    anyadir_comando(
        "reconvertir",
        "<objeto:no_interactivo:yo:simple'Pieza de Gloria a reconvertir'>",
        (
            : reconvertir:),
        "Convierte una pieza de gloria ya transmutada en una multiarmadura sin "
        "transmutar.");
    anyadir_comando(
        "desglorificar",
        "<objeto:no_interactivo:yo:simple'Pieza de Gloria a cambiar de tipo'>",
        (
            : desglorificar:),
        "Cambia el tipo de una pieza de gloria a cambio de un coste "
        "monetario.");
}
/*
mapping query_static_auto_load()
{
    mapping datos;

    datos = ::query_static_auto_load();

    datos["poder_gloria"] = dame_poder_equipo_gloria();

    return datos;
}

void init_static_arg(mapping map)
{
    ::init_static_arg(map);

    fijar_poder_gloria(map["poder_gloria"] ? map["poder_gloria"] : 1);
}
*/
