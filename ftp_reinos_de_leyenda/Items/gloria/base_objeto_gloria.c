// Satyr 21.06.2016
#include <pk.h>
#include <dinero.h>
private int poder_gloria;
private string tipo_gloria;

void create() {
    int nivel, i = sscanf(base_name(), "%*s%d", nivel);
    
    poder_gloria = i == 2 ? nivel : 1;
}
int dame_objeto_gloria() {
    return 1;
}
int dame_poder_equipo_gloria() {
    return poder_gloria;
}
void fijar_poder_gloria(int i) {
    poder_gloria = i;
}
varargs string dame_nombre_poder(int genero) {
    return GLORIA_TABLA_OBJETOS->dame_nombre_nivel(
        poder_gloria, 
        genero ? genero : TO->dame_genero()
    );
}
string dame_tipo_equipo_gloria() {
    return tipo_gloria;
}
void fijar_tipo_equipo_gloria(string t) {
    tipo_gloria = t;
}
int dame_valor() {
    return MONEDAS["platino"] * GLORIA_TABLA_OBJETOS->dame_coste_platinos_calculado(poder_gloria);
}
int query_value() {
    return MONEDAS["platino"] * GLORIA_TABLA_OBJETOS->dame_coste_platinos_calculado(poder_gloria);
}
int dame_coste_gloria() {
    return GLORIA_TABLA_OBJETOS->dame_coste_gloria_calculado(poder_gloria);
}
string dame_propietario() {
    return TO->query_property("propietario_gloria");
}
void fijar_propietario(string tx) {
    TO->add_property("propietario_gloria", tx);
}
void es_propietario(object pj) {
    return pj && pj->query_name() == dame_propietario();
}
string colorear_caps(string tx, string color, string posterior) {
    string *slices = explode(tx, " ");
    
    for(int i = 0; i < sizeof(slices); i++) {
        slices[i] = color + slices[i][0..0] + posterior + slices[i][1..] + "%^RESET%^";
    }
    
    return implode(slices, " ");
}
string colorear_corto(string tx) {   
    switch(poder_gloria) {
        default : return "%^WHITE%^" + tx + "%^RESET%^";
        case 2  : return "%^BOLD%^WHITE%^" + tx + "%^RESET%^";
        case 3  : return colorear_caps(tx, "%^ORANGE%^", "%^BOLD%^WHITE%^");
        case 4  : return "%^ORANGE%^" + tx + "%^RESET%^";
        case 5  : return "%^BOLD%^YELLOW%^" + tx + "%^RESET%^";
        case 6  : return colorear_caps(tx, "%^ORANGE%^", "%^BOLD%^");
    }
}