// Satyr 24/05/2019 01:31
// Satyr 24/07/2020 02:49 -- conjunto
#include <pk.h>
#include <baseobs_path.h>
#include <fronteras.h>
#include <baseobs_path.h>
#include <move_failures.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

#define BLOQUEO 600
#define DURACION 600

/*******************************************************************************
 * SECCION: Prototipos
 ******************************************************************************/
string dame_palabra_poder();
int    activar_portal();

/*******************************************************************************
 * SECCION: Sistema
 ******************************************************************************/
void limpiar_efecto_rubrica()
{
    if (environment() && !query_in_use()) {
        environment()->quitar_efecto("rubrica de nyelphax");
    }
}
void objeto_desequipado()
{
    if (environment() && -1 == find_call_out("limpiar_efecto_rubrica")) {
        call_out("limpiar_efecto_rubrica", 2);
    }
}
void init()
{
    ::init();

    anyadir_comando_equipado(dame_palabra_poder(), "", ( : activar_portal:));
    fijar_cmd_visibilidad(dame_palabra_poder(), "", 1);
}
void setup()
{
    fijar_armadura_base("anillo");
    fijar_genero(2);
    set_name("rubrica");
    set_short("%^BOLD%^BLACK%^R%^NOBOLD%^MAGENTA%^úbrica de "
              "%^BLACK%^BOLD%^N%^RED%^yel'phax%^RESET%^");
    set_main_plural("%^BOLD%^BLACK%^R%^NOBOLD%^MAGENTA%^úbricas de "
                    "%^BLACK%^BOLD%^N%^RED%^yel'phax%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Un anillo de un hueso de tono purpúreo que ha sido pulido y coronado "
        "con láminas de oro grabadas con rúbricas hechas de líneas finas "
        "y bastante enrevesadas.\n\n"

        "  Su superficie está tallada con los blasfemos símbolos del culto a "
        "Nyel'Phax, una entidad cuya mera existencia es tan adorada por sus "
        "sectarios como repudiada por aquellos que tachan a esta deidad como a "
        "una blasfemia inexistente fruto de la superstición y el "
        "sectarismo.\n\n"

        "  Elijas lo que elijas creer, el temor que inspira esta pieza es "
        "ominoso. Las palabras grabadas en el hueso de la rúbrica están "
        "escritas en un idioma de trazos extraños que, en ocasiones, parecen "
        "abandonar la superficie de la talla para incrustrarse en lo más "
        "hondo de tu psique, susurrando en tu interior promesas de poder "
        "vacuas y repitiendo, una y otra vez, el nombre del horror "
        "durmiente.\n\n"

        "  Cuanto más te esfuerzas por ignorar estos símbolos, más voces en tu "
        "interior escuchas, convirtiendo la curiosidad en obsesión y haciendo "
        "que la vela de tu paciencia se queme con el fuego ciego de este culto "
        "secreto.\n\n"

        "  A veces cedes a estos pensamientos, intentando escuchar las voces "
        "y discernir qué es lo que dicen... en vano. Rumores, murmullos e "
        "imágenes de una negrura infinita te bombardean en estas sesiones de "
        "introspección, haciéndote soñar con la infinidad del plano Empíreo, "
        "o como algunos lo llaman: el Cosmos (o Kosm).\n");
    fijar_encantamiento(20);

    fijar_material(7);
    setup_limitado_gloria();

    set_read_mess(
        "Yo soy la puerta.\n"
        "Yo soy la llave.\n"
        "Yo soy el guardián:%^CURSIVA%^\n" +
            dame_palabra_poder() + "%^RESET%^",
        "gargola",
        0);

    fijar_conjuntos(({BCONJUNTOS + "sacramento_de_nyelphax.c"}));
}
void guardar_log(string tx)
{
    log_file(
        "balance/objetos/rubrica_nyelphax.log",
        sprintf("%s: %s\n", ctime(), tx));
}
string descripcion_juzgar(object pj)
{
    return capitalize(dame_articulo()) + " " + query_short() + " es un" +
           dame_vocal() +
           " poderosa llave capaz de romper el tejido que "
           "separa al plano material del Empíreo.\n\n"

           "  Semejante poder permitirá a su portador crear fisuras cósmicas "
           "que durarán " +
           pretty_time(DURACION) +
           " y le permitirán "
           "teleportarse a ellas si el poder de la rúbrica se activa de "
           "nuevo.\n\n"

           "  Cualquier incapacitación que impida el movimiento hará que "
           "el intento fracase y, además, la magia de la llave no será capaz "
           "de permitir a su portador escapar de (o moverse a) salas en las "
           "que la teletransportación no sea posible.\n\n"

           "  Sin embargo, este poder no está exento de riesgos. Teleportarse "
           "demasiado lejos implicaría un gran gasto de energía para el que "
           "la rúbrica podría no protegerte, atrayendo quizás atención no "
           "deseada hacia tu localización.\n\n"

           "  La misteriosa magia de este objeto está sujeta a un bloqueo de " +
           pretty_time(BLOQUEO) + ".\n";
}

/*******************************************************************************
 * SECCION: Palabra de poder
 ******************************************************************************/
string dame_palabra_poder()
{
    string *elements, *word, tx;
    int     i;

    if (environment() && environment()->query_name() == "satyr") {
        return "rubrica";
    }

    if (tx = query_property("palabra_poder")) {
        return tx;
    }

    /**
     * @link https://lovecraft.fandom.com/wiki/Yog-Sothoth
     */
    elements =
        ({"YAINGN",
          "GAHYOG",
          "SOTHOTH",
          "HEELGE",
          "BFAI",
          "THRODOGUAAAHH",
          "OGTHROD",
          "AIFGEBL",
          "EEHYOG",
          "SOTHOTH'",
          "NGAH'NG",
          "AIYZHRO"});

    i    = roll(2, 2);
    word = allocate(0);

    while (i-- > 0) {
        word |= ({element_of(elements)});
    }

    tx = implode(word, "-");
    add_property("palabra_poder", tx);

    return tx;
}
/*******************************************************************************
 * SECCION: Utilidades portal
 ******************************************************************************/
string dame_path_portal()
{
    return __DIR__ + "rubrica_nyelphax_ob.c:" + environment()->query_name() +
           ":" + DURACION;
}
object buscar_portal()
{
    object *a = filter(children(dame_path_portal()), ( : $1 && clonep($1) :));

    return sizeof(a) ? a[0] : 0;
}
/**
 * Determina si puede crearse un portal en la sala.
 *
 * @return 0 en caso de éxito, string con el error en caso contrario
 */
string es_sala_valida_portal(object b)
{
    if (!b) {
        return "No puedes crear una fisura cósmica en el vacío.\n";
    }

    if (function_exists("dame_teleportar", b) && !b->dame_teleportar()) {
        return "Un abrumador poder mágico impide que puedas crear un portal "
               "en esta sala.";
    }

    return 0;
}
/**
 * Determina si el jugador está en posición de teleportarse o no.
 *
 * @return 0 en caso de éxito, string con el error en caso contrario
 */
string puede_teleportarse(object b, object portal)
{
    if (b->dame_clon()) {
        return "No puedes activar el poder de tu " + query_short() +
               " en tu "
               "cuerpo actual.";
    }

    if (!query_in_use()) {
        return "Al no tener equipad" + dame_vocal() + " tu " + query_short() +
               " eres incapaz de usar su poder.";
    }

    if (b->dame_bloqueo("movimiento")) {
        return "¡Tu estado de incapacitación actual te impide activar tu " +
               query_short() + "!";
    }

    if (b->query_property("atrapado") || b->dame_retener()) {
        return "¡Tu estado de incapacitación actual te impide activar tu " +
               query_short() + "!";
    }

    return 0;
}
/*******************************************************************************
 * SECCION: Acciones
 ******************************************************************************/
/**
 * Llamado cuando el jugador intenta teleportarse.
 *
 * Es una llamada con call_out y tiene un ligero retraso.
 */
int iniciar_teleport(object portal)
{
    string err;

    if (err = puede_teleportarse(TP, portal)) {
        return notify_fail(err + "\n");
    }

    TP->nuevo_bloqueo_combate(TO, BLOQUEO);
    nuevo_bloqueo_combate(TO, BLOQUEO);

    TP->accion_violenta();
    tell_object(
        TP,
        sprintf(
            "Tu %s responde a las palabras de poder con extraños susurros "
            "cada vez más audibles. Pronto sientes la gélida gravedad del "
            "cosmos cercándose a tu alrededor, si bien la magia de tu "
            "objeto parece protegerte, ¡por ahora!\n",
            query_short()));
    tell_accion(
        environment(TP),
        sprintf(
            "%s %s de %s responde a las palabras de poder de su portador y "
            "llena el entorno de susurros que cada vez son más audibles "
            "al tiempo que la luz a tu alrededor comienza a distorsionarse.\n",
            capitalize(dame_articulo()),
            query_short(),
            TP->query_cap_name()),
        "Un enjambre de extraños susurros comienza a invadir el entorno.\n",
        ({TP}),
        TP);
    call_out("fin_teleport", 2 + (TP->query_hb_counter() % 2) * 2, TP, portal);
    return 1;
}
/**
 * Llamado cuando el teleport se hace entre reinos, ¡fallando!
 */
int fin_fallo_teleport(object pj, object portal)
{
    object horror;
    tell_object(
        pj,
        "¡Tu " + query_short() +
            " falla catastróficamente cuando intentas "
            "teleportarte demasiado lejos!, ¡los susurros del Empíreo pronto "
            "se convierten en los jadeantes balbuceos de un horror cósmico que "
            "rasga la realidad atraído por tu necia curiosidad!\n");
    tell_object(
        environment(pj),
        "¡Contemplas con estupefacción como la realidad se hace jirones cuando "
        "un balbuceante horror cósmico atraviesa los tejidos de entre los "
        "planos atraído por la magia de la " +
            query_short() + " de " + pj->query_name() + "!",
        "¡Escuchas una cacofonía de horripilantes jadeos balbulceantes!",
        ({pj}),
        pj);

    if (horror = clone_object(BMONSTRUOS + "planares/horror_cosmico.c")) {
        horror->move(environment(pj));
        horror->attack_ob(pj);
    }

    guardar_log(sprintf(
        "%s pifia al teleportarse desde %s a %s.",
        pj->query_name(),
        base_name(environment(pj)),
        base_name(environment(portal))));
    portal->dest_me();
    return 1;
}
/**
 * Llamado al terminar la teleportación.
 */
int fin_teleport(object pj, object portal)
{
    string err, *origen, *destino;
    int    ok;

    if (!pj || !portal) {
        return 0;
    }

    if (err = puede_teleportarse(pj, portal)) {
        portal->dest_me();

        return notify_fail(err + "\n");
    }

    origen  = CORREGIR_FRONTERA(FRONTERAS_DAME_FRONTERA_JUGADOR(pj) || ({}));
    destino = CORREGIR_FRONTERA(
        FRONTERAS_DAME_FRONTERA_JUGADOR(environment(portal)) || ({}));

    pj->accion_violenta();

    // Nice
    if (origen[_POS_DOMINIO] != destino[_POS_DOMINIO]) {
        return fin_fallo_teleport(pj, portal);
    }

    err = base_name(environment(pj));

    // clang-format off
    ok = pj->move(
        environment(portal),
        (: $2->colorea_me($1) + " llega aquí tras atravesar la fisura.\n" :),
        (: "¡" + $2->colorea_me($1) + " desaparece de forma instantánea "
            "cuando el cosmos lo engulle en un vórtice violáceo!\n" :)
    );
    // clang-format on

    if (ok != MOVE_OK) {
        return notify_fail("¡Algo ha ido mal y el cosmos te ha rechazado!\n");
    }

    tell_object(
        pj,
        "¡La fuerza del empíreo te agarra con fuerza, vapuleándote"
        " mientras atraviesas sus entrañas a una velocidad vertiginosa!\n");
    tell_object(
        pj,
        "Antes de que puedas comprender los horrores que te rodean "
        "en el plano cósmico, un fogonazo de luz te expulsa de nuevo en el "
        "Plano Material y hace que tu " +
            query_short() + " se caliente.\n");

    pj->mirar();
    portal->dest_me();

    guardar_log(
        pj->query_name() + " usa su rúbrica para teleportarse de " + err +
        " a " + base_name(environment(pj)));
    return 1;
}
/**
 * Callout llamado al terminar la duración del portal.
 */
int terminar_rubrica(object b, object portal)
{
    if (!b || !portal) {
        return 0;
    }

    portal->dest_me();
}
/**
 * LLamado cuando se activa el poder y no hay un portal.
 *
 * Intenta clonarlo en la sala.
 */
int clonar_portal()
{
    object portal;
    string err;

    if (err = es_sala_valida_portal(environment(TP))) {
        return notify_fail(err + "\n");
    }

    if (!portal = clone_object(dame_path_portal())) {
        return notify_fail(
            "Las fuerzas del cosmos no parecen responder al poder de tu " +
            query_short() + " por culpa de un error.\n");
    }

    portal->move(environment(TP));
    tell_object(
        TP,
        sprintf(
            "Tu %s responde a las palabras de poder con un brusco "
            "destello mágico que, con violencia y sin ningún tipo de sutileza, "
            "rasga el tejido del plano material para crear una fisura cósmica "
            "que comienza a levitar y zumbar ante tus ojos.\n",
            query_short()),
        query_short());

    tell_accion(
        environment(TP),
        sprintf(
            "%s %s de %s responde a sus palabras de poder con un brusco "
            "destello mágico que, con violencia y sin ningún tipo de sutileza, "
            "rasga el tejido del plano material para crear una fisura cósmica "
            "que comienza a levitar y zumbar ante tus ojos.\n",
            capitalize(dame_articulo()),
            query_short(),
            TP->query_cap_name()),
        "Un agudo zumbido comienza a pincharte en lo más profundo de tu "
        "psique.\n",
        ({TP}),
        TP);

    guardar_log(
        TP->query_name() + " crea una fisura en " + base_name(environment(TP)));

    TP->nuevo_efecto(
        "rubrica de nyelphax",
        DURACION,
        (
            : terminar_rubrica(TP, $(portal))
            :),
        0,
        portal);
    return 1;
}

/**
 * Llamado al activar la opción.
 *
 * Si no hay un portal, intenta clonarlo.
 * Si hay un portal, intenta teleportarse.
 */
int activar_portal()
{
    object portal;

    if (dame_bloqueo_combate(TO)) {
        return notify_fail(
            "Tu " + query_short() +
            " no ha recuperado sus capacidades mágicas.\n");
    }

    if (TP->dame_bloqueo_combate(TO)) {
        return notify_fail(
            "Es demasiado pronto como para volver a usar "
            "el poder de " +
            dame_numeral() + " " + query_short() + ".\n");
    }

    if (portal = buscar_portal()) {
        return iniciar_teleport(portal);
    }

    return clonar_portal();
}
