// Satyr 22/11/2022 01:07
// Creado por avatares de balance
// https://www.reinosdeleyenda.es/foro/ver-tema/ideas-items-gloria/
#include <combate.h>
#include <pk.h>
#define BLOQUEO 100

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

/*******************************************************************************
 * SECCIÓN: Funciones habituales de objetos
 ******************************************************************************/
void setup()
{
    fijar_armadura_base("cinturon");
    set_name("cola");
    set_short("%^ORANGE%^Cola de Simio%^RESET%^");
    set_main_plural("%^ORANGE%^Colas de Simio%^RESET%^");
    generar_alias_y_plurales();
    fijar_genero(2);

    set_long(
        "Tienes ante ti un pertrecho de manufactura peculiar. Embutida en una "
        "extraña malla, formada por lo que parecen unos finos hilos de metal "
        "argénteo, se encuentra una peluda cola de Simio. El tacto de la "
        "pieza, dejando al margen su envoltura, es suave y cálida debido al "
        "pelo que recubre toda su superficie.\n\n"
        "Unos garfios situados en los "
        "extremos, y decorados con una fina orfebrería engarzada en pequeñas "
        "piedras opalescentes, hacen las veces de hebilla para afianzar este "
        "extraño artículo a la cintura de quien decida portarlo. Lo más "
        "extraño de este cinturón, obviando sus materiales, es el extraño olor "
        "a madera recién cortada que emite constantemente.\n\n");

    ajustar_BO(5);
    ajustar_BE(2);
    fijar_encantamiento(20);
    setup_limitado_gloria();
}

string descripcion_juzgar(object pj)
{
    return "Al recibir cualquier ataque, la cola podrá aplicar un efecto "
           "'Fortuna(1)' al arma del atacante, causando que su ataque pueda "
           "desviarse."
           " Este poder tiene un bloqueo de " +
           pretty_time(BLOQUEO) + ".\n";
}
/**
 * Esto es un super hack. Por favor, no lo copiéis. He tenido que usarlo porque
 * balance hizo este objeto sin saber que los efectos no se aplican a armaduras
 * y vamos muy justos de tiempo.
 */
varargs mixed procesar_efectos_combate(
    object def, object at, object ar, string n_ataque, mapping r, mapping res,
    int *validos)
{
    string  tx;
    object *victimas;
    object  fortuna, room;

    if (!fortuna = load_object(BASEOBS_EFECTOS + "fortuna.c")) {
        return 0;
    }

    if (!def || !at || environment() != def) {
        return 0;
    }

    if (-1 == member_array(_TRIGGER_INICIAL, validos)) {
        return 0;
    }

    if (dame_bloqueo_combate(TO, "fortuna")) {
        return 0;
    }

    room = environment(def);

    if (!sizeof(victimas = fortuna->dame_victimas_entorno(at, def, room))) {
        return 0;
    }

    nuevo_bloqueo_combate(TO, BLOQUEO, 0, 0, "fortuna");
    tx = ar->nuevo_efecto_basico_temporal("fortuna", 1, 1, 0, 110);
    ar->fijar_meta_efecto(tx, "sin_mensajes_temporales", 1);
    ar->fijar_params_efecto(tx, TO);

    return 1;
}
