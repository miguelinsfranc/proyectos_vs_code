// Satyr 24/05/2019 01:53
inherit "/std/item_estado.c";

#include <depuracion.h>
private
string propietario;
private
int duracion;

/*******************************************************************************
 * SECCION: Auxiliar
 ******************************************************************************/
string dame_propietario()
{
    return propietario;
}
void fijar_propietario(string tx)
{
    propietario = tx;
}
object dame_ob_propietario()
{
    return find_player(propietario);
}
int dame_duracion()
{
    return duracion;
}
void fijar_duracion(int i)
{
    duracion = i;
    remove_call_out("destruyeme");

    if (i > 0) {
        call_out("destruyeme", i);
    }
}
void destruyeme()
{
    dest_me();
}
/*******************************************************************************
 * SECCION: Sistema
 ******************************************************************************/
void setup()
{
    set_name("fisura");
    set_short("%^BLACK%^BOLD%^Fisura Cósmica%^RESET%^");
    set_main_plural("%^BLACK%^BOLD%^Fisuras Cósmicas%^RESET%^");

    set_long(
        "Una luz danzante que flota ante ti, incorpórea, pero no intangible, "
        "rasgando el plano material para comunicarlo con el lejano y "
        "misterioso "
        "plano Empíreo. La luz a su alrededor parpadea y se distorsiona al "
        "ritmo de un constante zumbido que no pareces oír, pero que sí "
        "sientes en la parte trasera de tu nuca.\n\n"

        "  Atravesar semejante fisura sin una rúbrica de protección "
        "sería una sandez. Tu destino terminaría en un mundo desconocido, "
        "rodeado por criaturas que habitan en muchas más de las tres "
        "dimensiones a las que tus primitivos sentidos tan cómodamente "
        "se han acostumbrado.\n\n"

        "  Cercado por horrores incomprensibles y un vacío desolador, "
        "tu cordura cedería ante la realización de que tu existencia es "
        "insignificante comparada con las grandes blasfemias que habitan "
        "en el lejano -y no tan tranquilo- cosmos.\n");
    generar_alias_y_plurales();

    reset_get();
    fijar_material(12);
}
void destructor()
{
    object b;

    if (b = dame_ob_propietario()) {
        tell_object(
            b,
            "La " + query_short() +
                " comienza a retorcerse lentamente mientras se extingue, "
                "cerrándose "
                "poco a poco hasta que solo queda de ella un rastro de humo "
                "blanquecino en el aire.\n");
    }

    tell_accion(
        environment(),
        "La " + query_short() +
            " comienza a retorcerse lentamente mientras se extingue, "
            "cerrándose "
            "poco a poco hasta que solo queda de ella un rastro de humo "
            "blanquecino en el aire.\n",
        "",
        ({b}),
        TO);
}
/*******************************************************************************
 * SECCION: Instanciación de nuevos portales
 ******************************************************************************/
object create_virtual_object(string invocador, int duracion)
{
    object b = clone_object(__FILE__);

    __debug_x(sprintf("%O %O", invocador, duracion));
    b->fijar_propietario(invocador);
    b->fijar_duracion(to_int(duracion));

    return b;
}
/*******************************************************************************
 * SECCION: Personalización varia
 ******************************************************************************/
int permitir_disipar_magia()
{
    return 1;
}
int disipar_magia()
{
    dest_me();
    return 1;
}
