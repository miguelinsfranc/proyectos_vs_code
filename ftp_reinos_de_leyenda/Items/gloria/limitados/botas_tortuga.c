//Eckol Ene18
#include <pk.h>

inherit GLORIA_LIMITADOS+"base_limitados"; 
inherit "/obj/armadura"; 

void setup()  
{ 
	fijar_armadura_base("botas");
	set_name("botas tortuga");
	set_short("Botas de Piel de %^GREEN%^T%^ORANGE%^o%^GREEN%^r%^ORANGE%^t"
            "%^GREEN%^u%^ORANGE%^g%^GREEN%^a%^RESET%^");
	set_main_plural("Botas de Piel de Tortuga");
	generar_alias_y_plurales();
	set_long("Un par de botas de aspecto muy resistente. El cuero con el que "
                "están fabricadas es de color pardo verdoso, con grandes surcos. "
                "Tienen un aspecto brillante debido al proceso de curtido de la "
                "piel. Las distintas piezas de cuero que forman las botas han "
                "sido cortadas y cosidas con gran precisión, de forma que las "
                "costuras son prácticamente invisibles. La suela de las botas "
                "está fabricada con el mismo tipo de cuero, aunque ha recibido "
                "un procedimiento de curtido distinto que le da más elasticidad.\n"
                "Las botas de piel de tortuga están imbuidas con la esencia del "
                "reptil del que proviene la piel: usarlas impide moverse rápido, "
                "pero al mismo tiempo da una mayor seguridad.\n");
	fijar_genero(-2);

	add_static_property("trampas",60);
    /*Esto no funciona, -1 no se nota, -4 es demasiado.*/
	//add_static_property("movimiento",-1); //GASTA un pulso extra para moverse.
    fijar_encantamiento(5);
    fijar_proteccion(30);

    setup_limitado_gloria();
}
