// Satyr 18/03/2021 02:40
#include <clean_up.h>

private
object afectado;
private
object arnes;

void setup_sombra(object _pj, object _arnes)
{
    afectado = _pj;
    arnes    = _arnes;
    shadow(afectado, 1);
}
int dame_sombra_arnes_centellas()
{
    return 1;
}
void destruir_sombra_arnes_centellas()
{
    destruct(TO);
}
varargs int spell_damage(
    int pupa, string tipo, object lanzador, object hechizo, object arma,
    string nombre_ataque, mapping cfg_ataque)
{
    string err;
    int    aux;

    if (!arnes) {
        call_out("destruir_sombra_arnes_centellas", 1);
        return afectado->spell_damage(
            pupa, tipo, lanzador, hechizo, arma, nombre_ataque, cfg_ataque);
    }

    if (-1 == member_array(tipo, ({"electrico", "aire"})) || pupa >= 0) {
        return afectado->spell_damage(
            pupa, tipo, lanzador, hechizo, arma, nombre_ataque, cfg_ataque);
    }

    err = catch
    {
        aux = arnes->evento_spell_damage_desde_sombra(
            afectado,
            pupa,
            tipo,
            lanzador,
            hechizo,
            arma,
            nombre_ataque,
            cfg_ataque);
    };

    return afectado->spell_damage(
        err ? pupa : aux,
        tipo,
        lanzador,
        hechizo,
        arma,
        nombre_ataque,
        cfg_ataque);
}
varargs void event_danyo_hechizo(
    object handler_salud, object atacante, object defensor, int pupa,
    string tipo, object hechizo, object arma, string nombre_ataque,
    mapping cfg_ataque)
{
    // La carga por "recibir daño" ya se calcula en spell_damage
    if (!arnes || !afectado || afectado != atacante || pupa >= 0) {
        return;
    }

    if (-1 == member_array(tipo, ({"aire", "electrico"}))) {
        return;
    }

    arnes->event_danyo_hechizo_desde_sombra(
        atacante,
        defensor,
        pupa,
        tipo,
        hechizo,
        arma,
        nombre_ataque,
        cfg_ataque);
}
