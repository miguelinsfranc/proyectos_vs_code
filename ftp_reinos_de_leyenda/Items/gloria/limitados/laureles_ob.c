//Eckol Jun18: NPCs para invocar con los laureles.
//Eckol Jun19: Case draxyar, sacamos soldados hombre lagarto.
//Sierephad Sep 2021	--	Aplicando el fijar_invocador al final para que pille el resto 
//de configuraciones previas.

#include <maestrias.h>

inherit "/obj/invocacion"; //Heredamos de invocación


string dama_raza_soldado(string raza){
    switch(raza){
        case "genesis/draxyar":
            return "hombre-lagarto";

        default: return raza;
    }
}

void configurar_invocacion(object invocador){
    string raza;
	if(!invocador)
		return;

    raza = dama_raza_soldado(invocador->dame_raza());

	fijar_raza(raza);
	fijar_clase("soldado");

	set_short("Soldado laureado de "+invocador->query_cap_name());
	set_long(invocador->query_description());

    fijar_carac("fue",15+random(4));
    fijar_extrema(100);
    fijar_carac("con",18);
    fijar_carac("con_orig",18);
    fijar_carac("des",14);
    fijar_carac("car",9);
    fijar_carac("int",14);
    
	fijar_peso_corporal(invocador->dame_peso_corporal());
	fijar_altura(invocador->dame_altura());
	
	fijar_ciudadania(invocador->dame_ciudadania());
	fijar_religion(invocador->dame_religion());

	ajustar_nivel(30);

    for(int f=1;f<14;f++){
        fijar_maestria(NOMBRE_FAMILIA(f),80);
    }

	add_clone("/baseobs/armas/espada_larga",1)->fijar_valor(500);
	add_clone("/baseobs/armaduras/completa",1)->fijar_valor(500);
	init_equip();	

	//Invocación
	//fijar_invocador(invocador);
	sin_cadaver();
	mismos_idiomas();
	grupo_invocador();
	restringir_empunyar();
    permitir_coger();
    permitir_equiparse();
	permitir_decapitar();
	//Sierephad Sep 2021	--	Aplicando el fijar_invocador al final para que pille el resto 
	//de configuraciones previas.
	fijar_invocador(invocador);
	
	
}


void setup(){
	set_name("soldado");
	set_short("Soldado laureado");
	set_main_plural("Soldados laureados");
	generar_alias_y_plurales();
	set_long("Un soldado con laureles.\n");
}
