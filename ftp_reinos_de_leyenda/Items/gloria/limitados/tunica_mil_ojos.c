// Satyr 28/10/2021 01:53
/**
 * Objeto de Nirnesil elegido como ganador del concurso de gloria en:
 * https://www.reinosdeleyenda.es/foro/ver-tema/concurso-comunidad-diseno-objetos-de-gloria-temporada-35/
 */
#include <hechizos.h>
inherit "/obj/armadura.c";
inherit "/obj/magia/objeto_formulador";

#define PORCENTAJE_INVISIBLES 5
#define HECHIZO_ESFERA 2
#define HECHIZO_NIVEL 20
#define HECHIZO_LOCK 60
#define HECHIZO_PORCENTAJE 5

void setup()
{
    fijar_armadura_base("tunica");
    set_name("tunica");
    set_short("%^MAGENTA%^Túnica de los %^BOLD%^BLACK%^M%^WHITE%^il "
              "%^BLACK%^O%^WHITE%^jos%^RESET%^");
    set_main_plural("%^MAGENTA%^Túnicas de los %^BOLD%^BLACK%^M%^WHITE%^il "
                    "%^BLACK%^O%^WHITE%^jos%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        "Esta formidable e intimidante túnica fue creada por la magia de un "
        "antiguo residente del castillo de Agnur. Realizada con seda negra con "
        "trazos de hilo dorado, tiene cosidos numerosos ojos de diferentes "
        "animales y humanoides por toda su superficie. Entre ellos destaca un "
        "enorme "
        "ojo reptiliano que ocupa la mayor parte del abdomen.\n\n"

        "  Lo más impresionante es que estos globos oculares no paran de "
        "abrirse y cerrarse mientras sus pupilas se agitan y mueven en todas "
        "direcciones, siempre atentas, como si estuvieran buscando algo. "
        "Seguramente este nigromante se cansó de los pícaros y forajidos que "
        "pueblan la isla de Naggrung y de cómo tenía que andar con un ojo en "
        "su bolsa cada vez que andaba por la polémica ciudad de Keel.\n");

    fijar_encantamiento(20);
    fijar_proteccion(-6);
    fijar_BE(8);

    add_static_property("mal", 5);
    add_static_property("bien", -5);
    add_static_property("buscar", 20);
    add_static_property("ts-mental", 10);
    add_static_property("ts-conjuro", 5);
    add_static_property("ts-artefacto", 5);

    fijar_recuperacion(HECHIZO_LOCK);
    fijar_esfera(HECHIZO_ESFERA);
    fijar_nivel(HECHIZO_NIVEL);
}
void objeto_equipado()
{
    set_heart_beat(5);
}
void objeto_desequipado()
{
    set_heart_beat(0);
}
string descripcion_juzgar(object pj)
{
    return sprintf(
        "Los ojos de esta túnica, de forma errática e incansable, se dedican a "
        "barrer con su mirada todo lo que se mueve a su alrededor en una clara "
        "demostración de pavor paranoide.\n\n"
        " Semejante capacidad ofrece un %d%% de "
        "probabilidades de poder descubrir a "
        "objetivos bajo los efectos de un hechizo de 'invisibilidad' cuando su "
        "portador termine de realizar una búsqueda.\n\n"
        " Adicionalmente, el objeto podrá formular el hechizo 'búsqueda "
        "instantánea' de forma aleatoria cuando su portador esté en peleas "
        "contra otros jugadores.\n",
        PORCENTAJE_INVISIBLES);
}
/**
 * Llamado directamente desde /cmds/npc/buscar.c
 */
void event_fin_buscar(object buscador, object entorno, string cosa_buscada)
{
    object *invis, descubierto;

    if (!entorno || buscador != environment()) {
        return;
    }

    if (random(101) > PORCENTAJE_INVISIBLES) {
        return;
    }

    invis = filter(
        all_inventory(entorno), function(object item) {
            return item && living(item) && item->dame_invisibilidad();
        });

    if (!sizeof(invis) || !descubierto = element_of(invis)) {
        return;
    }

    tell_object(
        buscador,
        sprintf(
            "De forma súbita y apresurada notas como %s %s se contorsiona%s "
            "cuando todos sus ojos miran al unísono hacia una dirección, "
            "desvelando a %s, a quién buscabas, pero no sabías encontrar.\n",
            dame_tu(),
            query_short(),
            dame_n(),
            descubierto->query_cap_name()));
    tell_object(
        descubierto,
        sprintf(
            "De súbito, %s %s de %s te mira con todos los globos oculares de "
            "los que "
            "hace gala. Al principio lo ignoras, a sabiendas de que los "
            "muchos mantos ilusorios que te protegen ofuscan tu figura a "
            "su mirada, pero su incesante empeño te hace dudar, sembrando "
            "una semilla de duda que termina germinando en pavor cuando "
            "tus sortilegios de invisibilidad, de forma súbita e "
            "incomprensible, llegan a su fin.\n",
            dame_articulo(),
            query_short(),
            buscador->query_cap_name()));
    tell_accion(
        entorno,
        sprintf(
            "%s %s de %s se revuelve cuando todos sus ojos miran al unísono "
            "hacia "
            "una dirección en la que la figura translúcida e invisible de %s "
            "comienza a hacerse visible poco a poco.\n",
            capitalize(dame_articulo()),
            query_short(),
            buscador->query_cap_name(),
            descubierto->query_cap_name()),
        "",
        ({buscador}),
        buscador);

    descubierto->destruir_invisibilidad();
}

void formular_busqueda()
{
    mixed spell;

    // No queremos que ocupe al controlador, así que hacemos ñapa
    // formular("busqueda instantanea", "", 0, environment());

    spell = INDEXADOR_HECHIZOS->dame_hechizo("busqueda instantanea", 0);

    if (!spell || !spell = load_object(spell)) {
        return;
    }

    spell->formular(environment(), "", ({}), ({}), 0, 1, TO);
}
void iniciar_formulacion(string nombre_hechizo, object portador)
{
    tell_object(
        portador,
        sprintf(
            "Los ojos de %s comienzan a parpadear en extraños patrones de "
            "sincronía para formular un hechizo.\n",
            TU_S(TO)));
    tell_accion(
        environment(portador),
        sprintf(
            "Los ojos de %s %s de %s comienzan a parpadear en extraños "
            "patrones de "
            "sincronía para formular un hechizo.\n",
            dame_lo(),
            query_short(),
            portador->query_cap_name()),
        "",
        ({portador}),
        portador);
}
void heart_beat()
{
    if (!environment() || !sizeof(environment()->dame_peleando() > 0)) {
        return;
    }

    if (dame_bloqueo_combate("Agotado")) {
        return;
    }

    if (random(101) > HECHIZO_PORCENTAJE) {
        return;
    }

    formular_busqueda();
}
