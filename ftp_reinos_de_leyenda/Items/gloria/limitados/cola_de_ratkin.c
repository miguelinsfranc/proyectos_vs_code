// Satyr 18/10/2019 02:20
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

/*******************************************************************************
 * SECCIÓN: Funciones habituales de objetos
 ******************************************************************************/
void setup()
{
    fijar_armadura_base("collar");
    set_name("cola");
    set_short("%^BOLD%^BLACK%^Cola de Ratkin%^RESET%^");
    set_main_plural("%^BOLD%^BLACK%^Colas de Ratkin%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "La cola de un Ratkin tras haber sido arrancada, curada y curtida "
        "hasta convertirse en un trofeo que puede abrocharse al cuello "
        "con un mosquetón de bronce bruñido.\n\n"

        "  Su aspecto es el que puedes esperar de una cola de rata gigante: "
        "es naturalmente duro debido a los tratamientos que sufrió y no "
        "es especialmente agradable a la mayoría de los sentidos.\n\n"

        "  Los que visten estas piezas, sin embargo, no lo hacen para lucir "
        "mejor, si no para poder presumir de tener las suficientes agallas "
        "(o dinero) para haber conseguido una cola de estas "
        "bestias licántropas tan raras como feroces.\n");

    add_static_property("negacion-paralizacion", -5);
    add_static_property("negacion-agilidad", -5);
    add_static_property("daño", 5);
    fijar_encantamiento(13);
    ajustar_BO(10);

    setup_limitado_gloria();
}
