// Cinturón hecho por Akhlas
/**
 * Satyr 03/08/2021 01:21 -- rebalanceo y convierto en objeto de gloria
 *
 * Ver:
 *
 * - https://www.reinosdeleyenda.es/foro/ver-tema/objetos-gloria-t-35/
 */
#include <pk.h>
inherit "/obj/armadura.c";
inherit GLORIA_LIMITADOS + "base_limitados.c";

void setup()
{
    fijar_armadura_base("cinturon");
    set_name("cinturon");
    set_short("%^BOLD%^BLACK%^Cinturón de %^GREEN%^Heldrek%^RESET%^");
    set_main_plural("%^BOLD%^BLACK%^Cinturones de %^GREEN%^Heldrek%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        "Se trata de un ligero cinturón compuesto por un fino trozo de cuero "
        "negruzco, con pequeños orificios "
        "en uno de sus extremos y una gran hebilla carmesí con lo que se "
        "antoja un rostro goblinoide.\n ");

    add_static_property("tipo", ({"cazador", "bribon"}));

    fijar_encantamiento(30);
    add_static_property("evasion", 8);
    add_static_property("armadura-magica", 5);
    add_static_property(
        "messon",
        "Notas una ligera presión en el abdomen que te hace estar más "
        "alerta.\n");
    add_static_property(
        "messoff",
        "Vuelves a respirar más pausadamente al aflojar tu cinturón.\n");

    fijar_material(3);
    setup_limitado_gloria();
}
