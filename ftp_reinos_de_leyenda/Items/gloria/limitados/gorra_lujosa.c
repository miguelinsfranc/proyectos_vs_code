//Eckol Oct17, sobre una idea de Rezzah.

#include <pk.h> //Para las herencias de gloria.

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
	fijar_armadura_base("gorro duro");
	set_name("gorra");
	set_short("%^ORANGE%^Gorra%^RESET%^ de %^BOLD%^YELLOW%^L%^WHITE%^u%^YELLOW%^j%^WHITE%^o%^RESET%^");
	set_main_plural("Gorras de Lujo");
	generar_alias_y_plurales();
	set_long("La gorra que estás observando es una obra maestra tanto de la alta costura "
				"como de la orfebrería. Está fabricada usando cuero y piel de alta calidad, "
				"que le aporta en su interior un tacto suave. El cuero tiene el grosor ideal "
				"para que la gorra sirva tanto en invierno como en verano, incluso en los climas "
				"más extremos. "
			 "La parte exterior de la gorra está decorada con filigranas de oro y plata, que le dan "
			 "un aspecto lujoso sin llegar a la opulencia. En la parte frontal, las filigranas se "
			 "agrupan para dar forma a una letra \"A\", la inicial de un talentoso bardo que con "
			 "sus rimas hizo fortuna, y encargo este complemento para deslumbrar a todo aquel que le "
			 "escuchaba.\n");
	fijar_genero(2);
}

string descripcion_juzgar(){
	return "Al llevar esta gorra equipada, los tenderos de cualquier reino ignoraran cualquier diferencia "
			"diplomática contigo y te atenderán.\n";
}
