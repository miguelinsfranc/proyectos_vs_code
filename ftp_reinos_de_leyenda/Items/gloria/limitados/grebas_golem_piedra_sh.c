// Satyr 03/08/2021 01:51
#include <clean_up.h>

private
object afectado, objeto;
float  reduccion_danyo;

void setup_sombra(object _afectado, object _objeto, float _reduccion_danyo)
{
    afectado        = _afectado;
    objeto          = _objeto;
    reduccion_danyo = _reduccion_danyo;

    shadow(_afectado, 1);
}
void destruir_sombra_grebas_golem_de_piedra()
{
    destruct(TO);
}
int dame_sombra_grebas_golem_de_piedra()
{
    return 1;
}

varargs int weapon_damage(
    int cantidad, object atacante, object arma, string tipo_pupa,
    string nombre_ataque, mapping cfg_ataque)
{
    if (!objeto) {
        call_out("destruir_sombra_grebas_golem_de_piedra", 0);
        return afectado->weapon_damage(
            cantidad, atacante, arma, tipo_pupa, nombre_ataque, cfg_ataque);
    }

    catch
    {
        cantidad = objeto->evento_weapon_damage(
            afectado,
            cantidad,
            atacante,
            arma,
            tipo_pupa,
            nombre_ataque,
            cfg_ataque);
    };

    return afectado->weapon_damage(
        cantidad, atacante, arma, tipo_pupa, nombre_ataque, cfg_ataque);
}
varargs int unarmed_damage(
    int pupa, object estilo, object atacante, string tipo_pupa,
    string nombre_ataque, mapping cfg_ataque)
{
    if (!objeto) {
        call_out("destruir_sombra_grebas_golem_de_piedra", 0);
        return afectado->unarmed_damage(
            pupa, estilo, atacante, tipo_pupa, nombre_ataque, cfg_ataque);
    }

    catch
    {
        pupa = objeto->evento_unarmed_damage(
            afectado,
            pupa,
            estilo,
            atacante,
            tipo_pupa,
            nombre_ataque,
            cfg_ataque);
    };

    return afectado->unarmed_damage(
        pupa, estilo, atacante, tipo_pupa, nombre_ataque, cfg_ataque);
}
