// Satyr 21.05.2017

private object jugador, invocacion, activo;
private int porcentaje;

int dame_shadow_baston_martirizador() {
    return 1;
}
int dame_sombra_baston_martirizador() {
    return 1;
}
void destruir_sombra_baston_martirizador() {
    if ( jugador ) {
        jugador->remove_extra_look(TO);

        if ( ! invocacion )
            tell_object(jugador, "Tu vínculo del martirio se desvanece.\n");
        else
            tell_object(jugador, "El vínculo del martirio que mantenías con "
                + invocacion->query_cap_name() + " se desvanece.\n"
            );
    }

    if ( invocacion ) {
        invocacion->remove_extra_look(TO);
    }

    destruct(TO);
}
void destruir_shadow_baston_martiriador() {
    destruir_sombra_baston_martirizador();
}
void setup_sombra(object _jugador, object _invocacion, int _porcentaje) {
    jugador    = _jugador;
    invocacion = _invocacion;
    porcentaje = _porcentaje;

    shadow(jugador, 1);
    jugador->add_extra_look(TO);
    invocacion->add_extra_look(TO);
}
int comprobar_invocacion_martirizador() {
    if ( ! invocacion || invocacion->dame_invocador() != jugador ) {
        call_out("destruir_sombra_baston_martirizador", 0);
        return 0;
    }

    return invocacion && environment(invocacion) == environment(jugador);
}
int ajustar_pvs(int i, object causante) {
    int proteccion;

    if ( i >= 0 || ! causante || causante == jugador )
        return jugador->ajustar_pvs(i, causante);

    if ( ! comprobar_invocacion_martirizador() )
        return jugador->ajustar_pvs(i, causante);


    proteccion = abs(i * porcentaje / 100.0);
    i         += proteccion;

    invocacion->ajustar_pvs(-proteccion, causante);
    return jugador->ajustar_pvs(i, causante);
}
string extra_look(object afectado)
{
    string tx = TP->extra_look(afectado) || "";

    if ( ! jugador || ! invocacion )
        return tx;

    if ( afectado == jugador ) {
        if ( TP == afectado )
            return tx + "Un aura de martirio te vincula con " + invocacion->query_cap_name() + ".\n";
        else
            return tx + "Un aura de martirio " + TP->dame_le() + " vincula con " + invocacion->query_cap_name() + ".\n";
    }


    if ( TP == afectado )
        return tx + "Un aura de martirio te vincula con " + jugador->query_short() + "\n.";

    return tx + "Un aura de martirio " + jugador->dame_le() + " vincula con " 
        + jugador->query_short() + "\n"
    ;
}