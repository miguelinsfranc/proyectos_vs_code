// Satyr 22.08.2016
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";
inherit "/obj/magia/objeto_con_poderes.c";

#define BLOQUEO 600
#define DURACION 30

#define PODER_SERPIENTES "petrificación serpientes"
#define PODER_SERPIENTES_BLOQUEO 600
#define PODER_SERPIENTES_DURACION 30
#define PODER_DESPETRIFICAR "despetrificación serpientes"
#define PODER_DESPETRIFICAR_BLOQUEO 600

void setup()
{
    fijar_armadura_base("capucha");
    set_name("melenas");
    set_short("%^BOLD%^BLACK%^Melenas de %^RESET%^GREEN%^Dabahak%^RESET%^");
    set_main_plural(
        "Pares de %^BOLD%^BLACK%^Melenas de %^RESET%^GREEN%^Dabahak%^RESET%^");

    generar_alias_y_plurales();

    set_long(
        "Una maraña de serpientes siseantes que, en un estado perenne de "
        "animosidad, "
        "se ensañan entre ellas en una guerra perenne que solo hace una tregua "
        "cuando alguien se les acerca, momento en el que todas ellas se unen "
        "para amenazarle.\n\n"

        "  El cuerpo de las serpientes, evidentemente mágico, nace de un cuero "
        "duro de forma oval que en su día perteneció a una bestia maldita "
        "que tenía a los ofidios como cabellera. Arrancado del cráneo de "
        "su cadáver, esta pieza ahora sirve como una insólita capucha.\n\n"

        "  Aunque por su actitud no lo parezca, los ojos ambarinos de las "
        "serpientes "
        "encierran en su interior una inteligencia que parece ir más allá "
        "de la de un simple animal, pues si bien su huésped ha muerto, la "
        "maldición que les da vida -y propósito- está aún muy latente en su "
        "interior.\n");

    fijar_genero(-2);
    fijar_encantamiento(25);

    setup_limitado_gloria();

    fijar_bloqueo_poderes(PODER_SERPIENTES_BLOQUEO, PODER_SERPIENTES);
    fijar_bloqueo_poderes(PODER_DESPETRIFICAR_BLOQUEO, PODER_DESPETRIFICAR);
}
string descripcion_juzgar(object pj)
{
    return "Las " + query_short() +
           " encierran el poder de su dueño original y permiten a su portador "
           "convertirse en una estatua a voluntad, así como recuperar su forma "
           "corpórea si son convertidos "
           "en piedra (esto permite revertir la propia petrificación del "
           "objeto). Este poder solo puede despertarse "
           "cada " +
           pretty_time(PODER_SERPIENTES_BLOQUEO) +
           " y la duración máxima de su petrificación es de " +
           pretty_time(PODER_SERPIENTES_DURACION) + ".\n";
}
// Quitarse la petrificación ignora incapacitaciones
varargs string
chequeo_sencillo_incapacitacion(object jugador, object obj, string poder)
{
    if (poder == PODER_DESPETRIFICAR) {
        return 0;
    }

    return ::chequeo_sencillo_incapacitacion(jugador, obj, poder);
}
int activar_serpientes()
{
    int    petrificar = TP->dame_incapacidad("petrificar", "maldiciones");
    string poder, err;

    poder = petrificar ? PODER_DESPETRIFICAR : PODER_SERPIENTES;

    if (!chequeo_activacion_poder(TP, TP, poder)) {
        return 0;
    }

    if (err = chequeo_previo_finalizar_poder(TP, TP, poder)) {
        tell_object(TP, err + "\n");
        return 0;
    }

    TP->consumir_hb();
    poner_bloqueos(TP, 0, poder);

    finalizacion_poder(TP, TP, poder, "terminar_serpientes");
    return 1;
}
int terminar_serpientes(
    object pj, object victima, string poder, mixed params...)
{
    mostrar_mensajes_poderes("finalizacion", pj, 0, poder);

    if (poder == PODER_DESPETRIFICAR) {
        pj->quitar_incapacidad("petrificar", "maldiciones", TO, TO);
        return 1;
    }

    pj->nueva_incapacidad("petrificar", "maldiciones", 10, DURACION);
    return 1;
}
varargs mixed dame_mensajes_finalizacion(
    object quien, object victima, string poder, mixed params...)
{
    if (poder == PODER_DESPETRIFICAR) {
        return ({
            "Las serpientes de " + TU_S(TO) +
                " parecen reaccionar a tus pensamientos y "
                "muerden la dura piedra que "
                "es ahora tu carne, haciendo que tu maldición comience a "
                "desvanecerse a medida que su "
                "veneno circula por tu interior.",
            "",
            capitalize(dame_articulo()) + " " + query_short() + " de " +
                quien->query_short() +
                " muerden su carne petrificada, haciendo que ésta tome su "
                "aspecto normal a medida que el veneno circula por su "
                "interior.",
        });
    }

    return ({
        "Las serpientes de " + TU_S(TO) +
            " reaccionan a tu petición y te muerden "
            "en el cuello varias veces, causando que su veneno comience a "
            "circular por tu interior"
            ", convirtiendo tu piel en piedra gris y porosa.",
        ""
        "Las serpientes de " +
            dame_articulo() + " " + query_short() +
            " "
            "de " +
            quien->query_cap_name() +
            " se abalanzan sobre su cuello, mordiéndole con saña "
            "y haciendo que su piel comience a convertirse en piedra gris y "
            "porosa.",
    });
}
void init()
{
    ::init();

    anyadir_comando_equipado(
        "instigar",
        "serpientes",
        (
            : activar_serpientes:),
        "Instiga a las serpientes de tus melenas a que se vuelvan contra ti y "
        "te conviertan "
        "en piedra o, si estás petrificado, a que eliminen la maldición.");
}
