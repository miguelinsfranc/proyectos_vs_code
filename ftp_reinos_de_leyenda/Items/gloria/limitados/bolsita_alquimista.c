// Eckol Abr18: Del concurso de objetos de gloria.
// Satyr 13/06/2019 02:23: Bloqueo también a nivel de objeto
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/equipo_contenedor";

#define BLOQUEO 150
#define DURACION 60

void setup()
{
    fijar_armadura_base("bolsita");
    set_name("bolsita");
    set_short("Bolsita del %^BOLD%^RED%^A%^RESET%^lquimista");
    set_main_plural("Bolsitas del %^BOLD%^RED%^A%^lquimista");
    generar_alias_y_plurales();
    set_long("Una bolsita de cuero muy bien curtido, de color gris oscuro. "
             "Lleva un pequeño cordel negro que permite cerrarla para evitar "
             "que se pierdan sus contenidos. Su interior es áspero, y de un "
             "tono rojizo. Aunque parece ser simplemente la parte interna del "
             "cuero con el que se ha cosido la bolsita, al introducir tu mano "
             "en la bolsita puede sentir como se te reseca la piel.\n");

    fijar_peso_max(2000);
    fijar_genero(2);

    fijar_tamanyo_armadura(0);

    setup_limitado_gloria();
}

string descripcion_juzgar()
{
    return "Convierte las hojas de khadul y marihuana en un preparado "
           "alquímico que permite intoxicar a los guardias.\nLos guardias "
           "intoxicados dejaran pasar a un caminante sin esconder con la misma "
           "tirada de salvación que uno escondido, y a un caminante escondido "
           "con una tirada de salvación más fácil.\nEl efecto dura " +
           DURACION +
           " segundos, pero solo permite un solo intento: tanto si se falla "
           "como si se consigue pasar, los guardias dejan de estar "
           "intoxicados.\nRequiere dos hojas cada planta para preparar una "
           "dosis de polvo tóxico.\nEl poder de la bolsita tiene un bloqueo "
           "de " +
           pretty_time(BLOQUEO) + "\n";
}

string extra_look(object ob)
{
    return "Contiene un fino polvo de color pardo, preparado para intoxicar a "
           "alguien.\n";
}

void procesar_contenido()
{
    object khadul = present("khadul", TO);
    object maria  = present("marihuana", TO);
    object pj     = environment();

    if (query_property("llena"))
        return;

    // Comprobamos
    foreach (object ob in({khadul, maria}))
        if (!ob || ob->dame_dosis() < 2)
            return;

    // Consumimos dosis
    foreach (object ob in({khadul, maria}))
        ob->consumir_dosis(2);

    // Llenamos
    add_property("llena", 1);

    // Extra look
    add_extra_look(TO);

    // No hay nadie para mostrar los mensajes
    if (!pj || !pj->query_player())
        return;

    tell_object(
        pj,
        "Las propiedades mágicas de la bolsita secan inmediatamente las hojas "
        "en su interior, hasta dejarlas completamente deshidratadas y "
        "frágiles. El simple movimiento de la tela al cerrar la bolsita hace "
        "que se quiebren y se conviertan en un fino polvo de color pardo.\n");

    return;
}

// Aqui elegimos que tipo de objetos se pueden meter
int test_add(object ob, int flag)
{
    if (query_property("llena")) {
        tell_object(
            TP, sprintf("%s ya está llen%s.\n", TU_S(TO), dame_vocal()));
        return 0;
    }

    if (!ob->dame_planta() ||
        -1 == member_array(ob->query_name(), ({"khadul", "marihuana"}))) {
        tell_object(
            TP,
            sprintf(
                "En %s solo deberías poner khadul o marihuana\n", TU_S(TO)));
        return 0;
    }

    call_out("procesar_contenido", 1);

    return 1;
}
int objetivo_valido(object b)
{
    return !b->dame_bloqueo_combate(TO, "sufrido");
}
int intoxicar()
{
    object guardias = filter(
        all_inventory(environment(TP)),
        (
            : $1->dame_guardia()
            :));

    if (!query_property("llena"))
        return notify_fail(
            "Necesitas que la bolsita contenga el preparado alquímico.\n");

    if (TP->dame_bloqueo_combate(TO))
        return notify_fail("Es demasiado pronto para volver a utilizar el "
                           "preparado alquímico.\n");

    if (dame_bloqueo_combate(TO))
        return notify_fail(
            query_short() + " no ha recuperado todavía sus propiedades.\n");

    if (!sizeof(guardias))
        return notify_fail("No hay razón para malgastar el preparado alquímico "
                           "si no hay guardias a la vista.\n");

    if (!sizeof(guardias = filter(guardias, ( : objetivo_valido:)))) {
        return notify_fail("Todos los guardias están ojo a vizor y no caerán "
                           "de nuevo en esa treta.\n");
    }

    tell_object(
        TP,
        "Coges un puñado del preparado alquímico de tu bolsita. Lo colocas en "
        "la palma de tu mano, te acercas a los guardias y soplas con todas tus "
        "fuerzas, esparciendo el polvo sobre sus caras.\n");

    tell_room(ENV(TP), "Los guardias parecen algo aturdidos.\n");

    guardias->add_timed_property("bolsita_alquimista", 1, DURACION);
    remove_property("llena");

    if (BLOQUEO) {
        TP->nuevo_bloqueo_combate(TO, BLOQUEO);
        guardias->nuevo_bloqueo_combate(TO, BLOQUEO, 0, 0, "sufrido");
        nuevo_bloqueo_combate(TO, BLOQUEO);
    }

    remove_extra_look(TO);

    return 1;
}

void init()
{
    anyadir_comando_equipado(
        "soplar",
        "preparado sobre guardias",
        "intoxicar",
        "Sopla el preparado alquímico sobre los guardias.");
    ::init();
}
