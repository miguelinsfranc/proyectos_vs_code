// Satyr 22.08.2016 -- Usando una idea del creador de Nirnesil en otro tipo de objeto
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

#include <pk.h>
#define BLOQUEO       90
#define MODIFICADOR  -25
#include <spells.h>
void setup()
{
    fijar_armadura_base("collar");
    set_short(
        "%^GREEN%^BOLD%^T%^RESET%^alisman de la %^BOLD%^GREEN%^S%^RESET%^uerte"
    );
    set_main_plural(
        "%^GREEN%^BOLD%^T%^RESET%^alismanes de la %^BOLD%^GREEN%^S%^RESET%^uerte"
    );
    generar_alias_y_plurales();
    
    set_long(
        "Una cara tallada en un bloque de jade del tamaño de un puño. Sus orejas, labios "
            "y nariz son desproporcionadamente grandes para el tamaño del único ojo "
            "que puedes ver, pues el izquierdo está tapado por una palma abierta "
            "que carece del nivel de detalle en el resto de la pieza.\n\n"
            
        "  Un collar de cuentas de piedra atraviesa la cabeza a través de las orejas "
            "y permite que éste extraño talismán pueda llevarse al pecho. Otrora "
            "muchos veneraban a la suerte como una fuente de poder, sin embargo, "
            "el sacrificio de Osucaru en la guerra de los dioses hizo que ésta "
            "práctica, así como sus símbolos, cayera en el olvido.\n"
    );
    
    fijar_encantamiento(20);
    ajustar_BE(-8); // para quedarse a 0
    
    foreach(string tipo in  "/table/tiradas_de_salvacion.c"->dame_tipos_ts(1))
        add_static_property(tipo, 2);
        
    fijar_material(6);
    
//    fijar_valor(10000);
//    fijar_coste_gloria(255);
    setup_limitado_gloria();
}
string descripcion_juzgar(object pj) {
    return "Las tiradas de salvación falladas se repetirán, modificando su bonificador (o penalizador) en "
        + MODIFICADOR + ". Este efecto tiene un bloqueo de " + pretty_time(BLOQUEO) + " que se activará "
        "en el momento que superes una tirada.\n"
    ;
}
void objeto_equipado() {
    object sh;
    
    if ( ! environment() )
        return;
        
    if ( ! sh = clone_object(GLORIA_LIMITADOS + obtener_info_ruta(TO) + "_sh.c") )
        return;
        
    sh->setup_sombra(environment(), TO, MODIFICADOR);
}
void objeto_desequipado() {
    if ( environment() )
        environment()->destruir_shadow_talisman_suerte();
}
int poner_bloqueo(object pj) {
    nuevo_bloqueo_combate(TO, BLOQUEO);
    pj->nuevo_bloqueo_combate(TO, BLOQUEO);
    
    return 1;
}
int puede_suerte(object pj) {
    return ! dame_bloqueo_combate(TO) && ! pj->dame_bloqueo_combate(TO);
}
