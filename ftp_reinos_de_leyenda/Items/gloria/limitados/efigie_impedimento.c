// Satyr 24/05/2019 01:31
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/magia/reliquia";

void setup()
{
    fijar_genero(2);
    set_name("efigie");
    set_short("%^GREEN%^BOLD%^E%^NOBOLD%^figie del "
              "%^BOLD%^I%^NOBOLD%^mpedimento%^RESET%^");
    set_main_plural("%^GREEN%^BOLD%^E%^NOBOLD%^figies del "
                    "%^BOLD%^I%^NOBOLD%^mpedimento%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Una rudimentaria reliquia que pasaría desapercibida si no fuese "
        "porque la madera de palma que conforma su empuñadura sujeta una enorme"
        " pieza triangular hecha de puro Jade macizo.\n\n"

        "  Cierto es que la piedra podría estar mucho mejor cuidada: los "
        "estragos del tiempo han agrietado y llenado de musgo la roca "
        "metamórfica, pero tu atención está centrada en la imponente mirada "
        "de las tres caras que adornan cada uno de los tres lados de esta "
        "efigie como para darte cuenta de esos pequeños detalles.\n\n"

        "  El origen la pieza es incierto, si bien las burlonas y "
        "desproporcionadas muecas de las caras te hacen pensar que seguramente"
        " tenga un origen tribal.\n");

    inicializar_reliquia();
    fijar_encantamiento(15);
    fijar_material(6);

    add_static_property("ts-conjuro", 5);
    add_static_property("ts-paralizacion", 5);
    add_static_property("armadura-magica", 5);
    add_static_property("negacion-paralizacion", 15);
    add_static_property("poder_magico", 25);
    add_static_property("poder_curacion", 50);

    setup_limitado_gloria();
}
