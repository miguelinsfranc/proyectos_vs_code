// Satyr 06.07.2016
#include <pk.h>
#define SHORT "%^BOLD%^YELLOW%^Ca%^GREEN%^le%^RED%^id%^BLUE%^os%^RED%^co%^GREEN%^pi%^YELLOW%^co"
#define TIEMPO 360
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("capucha");
    set_short("Hipervisor " + SHORT + "%^RESET%^");
    set_main_plural("Hipervisores " + SHORT + "s%^RESET%^");
    generar_alias_y_plurales();
    
    set_long(
        "Ante ti se haya un ingenio de la arcanotecnica gnómica creado con el fin de "
            "ayuda a canalizar los vientos del éter a los hechiceros hacia su propia "
            "vista. El invento fue prometedor, pero rápidamente descartado al causar "
            "fuertes dolores de cabeza, alucinaciones y diarreas a todos los sujetos "
            "de prueba. El artefacto en sí no es más que una cinta repleta de lentes "
            "encantadas de varias formas y colores que parecen oscilar en todo "
            "momento. Se puede sujetar a la cabeza mediante tres tiras de cuero y con "
            "un ingenioso mecanismo gnómico inusualmente patentado llamado \"clip\".\n"
    );
    
    set_read_mess(
        "- NoUtiliceMaquinariaPesadaSiUsaEsteVisor.\n" 
        "- SiComienzaAverMuertosUanimalesAlPonerseElVisor,QuíteseloDeInmediato.\n"
        "- ParaUsosProlongadosRecomendamosUsarElPañalTácticoRecogetodo.\n",
        "- EnCasoDeAlertaMarrónAlejeElHipervisorDelMaterialOrgánico."
        "gnomo"
    );

        
    add_static_property("ts-conjuro", 15);
    
    fijar_encantamiento(20);
//   fijar_coste_gloria(160);
//    fijar_valor(5000);
    setup_limitado_gloria();
}
int dame_hipervisor_caleidoscopico() {
    return TIEMPO;
}
string descripcion_juzgar(object pl) {
    return "Los hechizos de 'ver realmente' que formules sobre tí mismo tendrán una duración "
        "incrementada de " + pretty_time(TIEMPO) + "."
    ;
}
int set_in_use(int i) {
    int p = ::set_in_use(i);
    
    if ( ! i && ! p )
        environment()->quitar_efecto("ver realmente");
    
    return p;
}
