// Satyr 06.07.2016
//private int coste_gloria;
#include <pk.h>
private int coste_gloria;
int no_catalogar_armeria() {
    return 0;
}
int dame_objeto_gloria_limitado() {
    return 1;
}
int dame_coste_gloria() {
    return coste_gloria;
}
void fijar_coste_gloria(int i) {
    coste_gloria = i;
}
void setup_limitado_gloria() {
    fijar_coste_gloria(GLORIA_TABLA_OBJETOS->dame_coste_gloria_objeto_limitado(base_name()));
    TO->fijar_valor_venta(GLORIA_TABLA_OBJETOS->dame_precio_objeto_limitado(base_name()));
}