// Satyr 22/11/2022 01:07
// Creado por avatares de balance
// https://www.reinosdeleyenda.es/foro/ver-tema/ideas-items-gloria/

#include <depuracion.h>
#include <unidades.h>
#include <combate.h>
#include <pk.h>

#define BLOQUEO_ELUDECORAZONES 100
#define PODER_ELUDECORAZONES "eludecorazones"
#define PROBABILIDAD_EFECTO_ELUDECORAZONES 500
#define PODER_EFECTO_ELUDECORAZONES 3

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/vaina";

/*******************************************************************************
 * SECCIÓN: Funciones habituales de objetos
 ******************************************************************************/
void setup()
{
    fijar_armadura_base("capa");
    set_name("talabarte");
    set_short("Talabarte del Adalid Acorazado");
    set_main_plural("Talabarte del Adaliz Acorazado");
    generar_alias_y_plurales();

    set_long(
        "No sabrías especificar con claridad el color de esta extraña prenda "
        "ya que, debido claramente a numerosos sortilegios mágicos, una "
        "cantidad considerable de un fluido metálico, similar al mercurio, se "
        "adhiere a la superficie de esta entre bamboleos y desplazamientos "
        "aleatorios refractando la luz de manera desigual.\n\n"
        "Gracias a este "
        "peculiar fluido, el tejido ostenta una pesantez importante. Dicho "
        "metal reacciona ante cualquier objeto que se le acerque, desplazando "
        "a este en el momento del impacto hacia otra parte de su superficie de "
        "manera instantánea. Unos ribetes dorados, situados a los extremos de "
        "este particular tejido y remachados con dos extrañas gemas, sirven "
        "para asir la pieza a la altura del cuello de quien decida "
        "portarla.\n");

    ajustar_BO(10);
    ajustar_BE(-23);
    ajustar_BP(10);
    fijar_encantamiento(10);

    {
        string *armas = allocate(0);
        object  tabla = load_object("/table/bd_armas.c");
        int *   data;

        if (tabla) {
            foreach (string tipo_arma in tabla->dame_armas()) {
                data = tabla->ojear_datos_armas(tipo_arma);

                if (!arrayp(data) || sizeof(data) < 11) {
                    continue;
                }
                if (data[11] > 1) {
                    armas += ({tipo_arma});
                }
            }
        }

        fijar_armas(armas);
    }

    setup_limitado_gloria();
}
string descripcion_juzgar(object pj)
{
    return "Este talabarte encierra un sortilegio misterioso que, al ser " +
           "activado, convierte la pátina refractante que aferra a "
           "superficie " +
           "en un remolino errático que distorsiona la luz, haciendo que tu " +
           "cuerpo se desdibuje y emborrone de forma sutil.\n\n" +

           "Semejante poder causará que todos a todos los ataques que " +
           "recibas " + "se le añada un efecto 'Eludecorazones (" +
           PODER_EFECTO_ELUDECORAZONES + ")', " +
           "haciendo así que tus atacantes tengan un " +
           (to_int(PROBABILIDAD_EFECTO_ELUDECORAZONES / 10.0)) +
           "% de posibilidades de impactar en las localizaciones más " +
           "protegidas de tu cuerpo.\n\n";
}
/**
 * Esto es un super hack. Por favor, no lo copiéis. He tenido que usarlo porque
 * balance hizo este objeto sin saber que los efectos no se aplican a armaduras
 * y vamos muy justos de tiempo.
 */
varargs mixed procesar_efectos_combate(
    object def, object at, object ar, string n_ataque, mapping r, mapping res,
    int *validos)
{
    string tx;
    object eludecorazones;

    if (!eludecorazones = load_object(BASEOBS_EFECTOS + "eludecorazones.c")) {
        return 0;
    }

    if (!def || !at || environment() != def) {
        return 0;
    }

    if (-1 == member_array(_TRIGGER_INICIAL, validos)) {
        return 0;
    }

    tx = ar->nuevo_efecto_basico_temporal("eludecorazones", 0);
    ar->fijar_meta_efecto(tx, "sin_mensajes_temporales", 1);
    ar->fijar_probabilidad_efecto(tx, PROBABILIDAD_EFECTO_ELUDECORAZONES);
    return 1;
}
