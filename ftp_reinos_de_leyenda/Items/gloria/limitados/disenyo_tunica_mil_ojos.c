// Satyr 04/11/2021 02:53
// Recompensa de gloria de la temporada 35
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/disenyo.c";

void setup()
{
    fijar_oficio("sastre");
    fijar_receta("tunica de los mil ojos");
    fijar_lenguaje("dendrita");

    setup_limitado_gloria();
}
