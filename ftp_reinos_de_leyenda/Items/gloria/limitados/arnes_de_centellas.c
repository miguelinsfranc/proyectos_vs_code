// Satyr 18/03/2021 02:26
#include <funciones_salud.h>
#include <baseobs_path.h>
#include <depuracion.h>
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

#define REDUCCION 0.1 // esto es un 90%
#define PORCENTAJE 5
#define PORCENTAJE_CURA 15
#define ATAQUE "arnes_centellas"

/*******************************************************************************
 * SECCIÓN: Funciones habituales de objetos
 ******************************************************************************/
void setup()
{
    fijar_armadura_base("completa");

    fijar_genero(1);
    set_name("arnes");
    set_short("Arnés de Centellas");
    set_main_plural("Arneses de Centellas");
    generar_alias_y_plurales();

    set_long(
        "Una armadura formada por varias bandas de distintos metales que " +
        "se superponen en una exquisita ejecución de la técnica de la " +
        "galvanización. El resultado final es el de un arnés cuya utilidad " +
        "como protección está en entredicho, pues expone demasiada carne de " +
        "su portador y sus bandas tampoco son especialmente gruesas, " +
        "resistentes o con capacidades de redirigir ataques.\n\n"

        " No obstante, el secreto de esta pieza está en los pequeños zafiros," +
        " oscuros como los ojos lechosos de un depredador abisal, que surcan " +
        "su superficie. Dichas joyas encierran una carga eléctrica que ha " +
        "sido colocada ahí gracias a los ingeniosos —y muy poco seguros— " +
        "experimentos de una orden de herreros-hechiceros auspiciados por " +
        "el secretismo. Sus creaciones son usadas exclusivamente por " +
        "una orden de caballería tan mística, como rara y antigua; bueno, y " +
        "ahora también pueden ser usadas por ti, claro, puesto que algún " +
        "desliz del destino ha hecho que este arnés caiga en tu posesión.\n\n"

        " Vestir esta pieza hace que las caprichosas magias despierten, " +
        "envolviendo su superficie en una barrera eléctrica capaz de repeler " +
        "ataques como si estuviesen hechas de acero. Además, tan curioso " +
        "poder no duda en nutrirse de ráfagas elementales afines para " +
        "hacerse aún más fuerte, cualidad que su antigua portadora " +
        "había convertido en técnica personal y perfeccionada.\n");

    set_read_mess(
        "Perteneciente a Nirra. Indómita Hija del Trueno, ejecutora de " +
            "Onerian y hermana de sangre de Aurmartynordas.\n" +
            "Que tu historia perdure mientras tu vieja armadura perviva.\n" +
            "Que tu temple y recuerdo sirva para modelar al resto de " +
            "hermanos y hermanas de la orden.\n" +
            "Que el eco de tus arengas, omnipresentes en los truenos, nos " +
            "inspire en los tiempos aciagos que nos esperan.\n",
        0,
        "elfico");

    add_static_property(
        "messon",
        "Una súbita sensación de euforia te aguijonea la nuca cuando " +
            "los zafiros de " + TU_S(TO) +
            " cobran vida con una luz cegadora " +
            "y rodean tu pecho con una barrera de electricidad.\n");
    add_static_property(
        "messoff",
        "Las corrientes de electricidad comienzan a retraerse hacia " +
            "los zafiros del " + query_short() + ", desapareciendo en " +
            "la oscura profundidad de las joyas.\n");

    // Lo weno
    fijar_conjuntos(({BCONJUNTOS + "bendicion_hija_del_trueno.c"}));
    add_static_property("afinidad-electrico", 10);
    add_static_property("afinidad-aire", 10);
    add_static_property("electrico", 10);
    add_static_property("aire", 10);
    fijar_encantamiento(30);
    ajustar_BO(13);

    // Lo malo
    add_static_property("afinidad-tierra", -12);
    add_static_property("tierra", -12);
    add_static_property("veneno", -12);

    // PK
    setup_limitado_gloria();
}
string descripcion_juzgar(object b)
{
    return "La magia del arnés encierra una afinidad elemental con el poder " +
           "del aire y el rayo, siendo capaz de absorber sus energías para " +
           "vigorizar a quien lo porte, quién se moldeará con ellas.\n\n" +

           " Tal poder hará que quien vista este arnés tenga un " + PORCENTAJE +
           "% de " + "posibilidades de ignorar el daño tipo 'eléctrico' " +
           "o 'aire' causado por hechizos, ataques elementales o algunos " +
           "efectos.\n\n"

           " De la misma forma, todo el daño de estos tipos que realice el " +
           "atacante con ataques de esas mismas fuentes " + "tendrá un " +
           PORCENTAJE_CURA + "% de curar al portador " +
           "una cantidad de puntos de vida que variará en función del daño " +
           "realizado.\n";
}
void objeto_equipado()
{
    object sh;

    if (!environment() || environment()->dame_sombra_arnes_centellas()) {
        return;
    }

    if (!sh = clone_object(base_name() + "_sh.c")) {
        return;
    }

    set_short("%^CYAN%^BOLD%^A%^NOBOLD%^rnés de "
              "%^BOLD%^C%^NOBOLD%^entellas%^RESET%^");
    set_main_plural("%^CYAN%^BOLD%^A%^NOBOLD%^rneses de "
                    "%^BOLD%^C%^NOBOLD%^entellas%^RESET%^");
    sh->setup_sombra(environment(), TO);
}
void objeto_desequipado()
{
    if (environment()) {
        environment()->destruir_sombra_arnes_centellas();
    }

    set_short("Arnés de Centellas");
    set_short("Arneses de Centellas");
}
/*******************************************************************************
 * Sécción: Añadir daño eléctrico a las armas (actualmente no se usa)
 ******************************************************************************/
private
void debug(string tx, string n)
{
    __debug_x(tx, ({"arnes_centellas", "arnes_centellas-" + n}));
}
/*******************************************************************************
 * Sécción: Posibilidad de absorber daño eléctrico
 ******************************************************************************/
int dame_porcentaje_exito()
{
    return PORCENTAJE;
}
int dame_porcentaje_exito_cura()
{
    return PORCENTAJE_CURA;
}
varargs int evento_spell_damage_desde_sombra(
    object afectado, int pupa, string tipo, object lanzador, object hechizo,
    object arma, string nombre_ataque, mapping cfg_ataque)
{
    int tirada;

    // Tirada de éxito para ver si se activa la absorción de daño
    tirada = random(101);
    debug(
        "evento_spell_damage_desde_sombra: éxito: " + tirada + " (" +
            dame_porcentaje_exito() + ")",
        afectado->query_name());

    if (tirada > dame_porcentaje_exito()) {
        return pupa;
    }

    debug(
        sprintf("evento_spell_damage_desde_sombra: superada tirada."),
        afectado->query_name());

    tell_object(
        afectado,
        sprintf(
            "Con un fogonazo, los zafiros de %s absorben el daño " +
                "%s%s, dejando una efímera telaraña de luz en el " +
                "aire que se desvanece a los pocos segundos.\n",
            TU_S(TO),
            tipo,
            lanzador ? " causado por " + lanzador->query_short()
                     : " que recibes"));

    if (lanzador) {
        tell_object(
            lanzador,
            sprintf(
                "Con un fogonazo, los zafiros %s de %s absorben parte del " +
                    "daño de tipo %s que descargas contra %s, dejando una " +
                    "efímera telaraña de luz en el aire que se desvanece " +
                    "a los pocos segundos.\n",
                dame_del() + " " + query_short(),
                afectado->query_cap_name(),
                tipo,
                afectado->dame_pronombre()));
        debug(
            "evento_spell_damage_desde_sombra: se activa poder",
            afectado->query_name());
    }

    if (environment(afectado)) {
        tell_accion(
            environment(afectado),
            sprintf(
                "Con un fogonazo, los zafiros %s de %s absorben parte " +
                    "del daño de tipo %s que%s, dejando una efímera " +
                    "telaraña de luz en el aire que se desvanece " +
                    "a los pocos segundos.\n",
                dame_del() + " " + query_short(),
                afectado->query_cap_name(),
                tipo,
                lanzador ? " " + lanzador->query_cap_name() + " descarga " +
                               "sobre " + afectado->dame_pronombre()
                         : " recibe"),
            "Un fugaz fulgor inunda el entorno, desdibujando las siluetas " +
                "de " + (lanzador ? "dos figuras" : "una figura") +
                " tras un " + "fondo de luz eléctrica.\n",
            ({afectado}),
            afectado);
    }

    return to_int(pupa * REDUCCION);
}
/*******************************************************************************
 * Sécción: Carga al hacer daño
 ******************************************************************************/
varargs void event_danyo_hechizo_desde_sombra(
    object atacante, object defensor, int pupa, string tipo, object hechizo,
    object arma, string nombre_ataque, mapping cfg_ataque)
{
    int tirada;

    if (!atacante || atacante == defensor) {
        return;
    }

    tirada = random(101);
    debug(
        sprintf("event_danyo_hechizo: tirada de éxito: %d", tirada),
        atacante->query_name());

    if (!(pupa = abs(pupa)) || tirada > PORCENTAJE_CURA) {
        return;
    }

    tirada = to_int(random(50) + log(pupa) * 30);
    debug(
        sprintf(
            "event_danyo_hechizo(%O, %O, %d): cura de %d",
            atacante,
            defensor,
            pupa,
            tirada),
        atacante->query_name());
    APLICAR_CURACION(atacante, tirada, atacante, TO, ([]));
}
