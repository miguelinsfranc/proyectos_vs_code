// Satyr 24/07/2020 02:26
#include <pk.h>
#include <material.h>
#include <baseobs_path.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("pendiente");

    set_short("%^BOLD%^WHITE%^E%^BLACK%^squirla "
              "%^WHITE%^BOLD%^C%^BLACK%^ósmica%^RESET%^");
    set_main_plural("%^BOLD%^WHITE%^E%^BLACK%^squirla "
                    "%^WHITE%^BOLD%^C%^BLACK%^ósmica%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Un pendiente compuesto del fragmento de un meteoro de basalto que, en "
        "algún momento del pasado, decidió desviarse de su rumbo por el cosmos "
        "infinito para enterrarse en el suelo de alguno de los Reinos. Por su "
        "origen es apreciado por los seguidores de Nyel'Phax como muestra de "
        "estatus.\n\n"

        "  La superficie naturalmente porosa de este tipo de mineral ha sido "
        "pulida concienzudamente hasta convertirse en una pieza lisa, "
        "oblonga y delicada, que es cálida al tacto y hace las veces de "
        "pendiente con la ayuda de un pequeño garfio de hueso que atraviesa uno"
        " de los muchos agujeros que presenta la roca.\n\n"

        "  No obstante, y dejando de lado las siempre interesantísimas "
        "características geológicas de este mineral ígneo extrusivo de "
        "composición máfica, lo más reseñable de la pieza es que en su interior"
        " parece habitar... algo. Quizás algún tipo de polizón que "
        "viajaba en el cometa antes de estrellarse en el mundo. No sabes si "
        "es benigno o no, pero a los seguidores de Nyel'Phax no parece "
        "molestarles.\n");

    fijar_BO(8);
    fijar_encantamiento(10);
    add_static_property("habilidad", "tentaculo tumefacto");

    add_static_property(
        "messon",
        "Al ponerte este pendiente el chirriante insecto que moraba en su "
        "interior se escapa de su madriguera para colarse por tu oreja, ante "
        "tu horror, y causar que un horrible absceso brote de tu espalda.\n");
    add_static_property(
        "messoff",
        "Quitarte el pendiente hace que su morador abandone tu cuerpo para "
        "volver a la piedra de basalto. Con él, el horrible tumor que brotaba "
        "de tu espalda parece remitir de forma instantánea a la par que el "
        "incesante chirrido que parecías oír constantemente en tu cuerpo.\n");
    fijar_material(PIEDRA);
    fijar_encantamiento(20);
    setup_limitado_gloria();
    fijar_tamanyo_armadura(0);

    fijar_conjuntos(({BCONJUNTOS + "sacramento_de_nyelphax.c"}));
}
