// Satyr 01/12/2020 02:29
#include <combate.h>
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

int dame_estorbo()
{
    return 0;
}
string dame_nombre_material()
{
    return "Quitina de Ankheg";
}
void setup()
{
    set_name("manopla");
    fijar_armadura_base("manopla");
    set_short("%^GREEN%^M%^anopla de %^BLACK%^BOLD%^Ankheg%^RESET%^");
    set_main_plural("%^GREEN%^M%^anoplas de %^BLACK%^BOLD%^Ankheg%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        "Los Ankhegs son artrópodos gigantes de grandes ojos que suelen " +
        "vivir metros bajo tierra en regiones olvidadas. Su presencia ya era " +
        "rara en la segunda era, pero las inestabilidades tectónicas del " +
        "cataclismo hicieron que sus números menguaran hasta desaparecer. " +
        "Si es debido a una migración a otras regiones (¡o profundidades" +
        "!) o a problemas demográficos es algo que aún no está claro.\n\n"

        "  Pero basta de entomología fantástica y centrémonos en la pieza " +
        "que tienes ante ti: una manopla hecha de láminas de cuero duro " +
        "de Ankheg entrecosidas con tendón y algún tejido conectivo cuyo " +
        "origen desconoces. Es cálida, cómoda y maleable —a la par que " +
        "resistente. Está reforzada por un duro tejido de gambesón grisáceo " +
        "que hace que las estrechas juntas entre las láminas no dejen la " +
        "mano desprotegida. El tono apagado del gambesón combina con los " +
        "colores verdes oscuros del pellejo curado.\n\n"

        "  Alguien que tuviese un extenso trasfondo académico centrado en el " +
        "fascinante estudio de los Ankheg, su morfología, ciclo vital y " +
        "reproductivo, hábitos migratorios, ecología y otras muchas " +
        "disciplinas que ahora no se van a mencionar —ya que tú conocimiento " +
        "sobre la materia es casi tan grande como el sopor que ésta te " +
        "produce... ¡y te está empezando a entrar el sueño!— " +
        "se daría cuenta de que estos guantes no son " +
        "lo suficientemente duros como para estar hechos de quitina madura, " +
        "lo que denota que seguramente su artesano encontrase alguno de " +
        "los muchos pellejos de Ankheg joven que éstos dejan atrás cuando "
        "mudan al acabar su etapa embrionaria.\n\n"

        "  Estas piezas carecen de las chispas, zumbidos, brillos y otras " +
        "sandeces propias de los objetos mágicos de los reinos, pero su " +
        "sólida construcción y la calidad de sus materiales suplen con " +
        "creces dicha carencia, con el beneficio de que te permiten ignorar " +
        "parte de las jaquecas ocasionales causadas por llevar tesoros " +
        "encantados hasta en las orejas, literalmente.\n");

    add_static_property("daño", 10);
    add_static_property("acido", 5);
    add_static_property("fuego", -5);
    fijar_proteccion(dame_proteccion() - 8);
    fijar_material(3);
    ajustar_BO(10);
    ajustar_BE(-5);

    setup_limitado_gloria();
}
