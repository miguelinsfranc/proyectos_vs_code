// Satyr 15/03/2021 03:09
/**
 * El asunto del "evitar fallar al rastrear" se comprueba en la habilidad.
 */
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("capa");
    set_name("embozo");
    fijar_genero(1);
    set_short("%^BLACK%^BOLD%^Embozo del "
              "%^RESET%^RED%^S%^BOLD%^BLACK%^icario%^RESET%^");
    set_main_plural("%^BLACK%^BOLD%^Embozos del "
                    "%^RESET%^RED%^S%^BOLD%^BLACK%^icario%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "El color negro tiende a ocultar más fácilmente las " +
        "manchas de la ropa y esta capa lo ilustra claramente, pues a pesar " +
        "de estar salpicada por manchas de barro, sangre reseca, líquidos " +
        "derramados y otros desperfectos fruto del uso continuo, su cuero " +
        "parece perfectamente limpio, si bien un poco arrugado.\n\n"

        "  Su corte ancho y largo hace que su cola tienda a " +
        "arrastrarse con facilidad, pero fuertes puntadas y tachuelas " +
        "se encargan de asegurarse de que las costuras soporten cualquier " +
        "maltrato que ésta pueda sufrir. No es especialmente cálida, pero " +
        "el cuero sí es un buen cortavientos y tiene tanto un olor como un " +
        "crujido al andar que son especialmente reconfortantes.\n\n" +

        "  Tiene un cuello alto –que " +
        "normalmente está doblado hacia abajo– que puede usarse a modo de " +
        "embozo para ocultar el rostro del portador, cosa que es muy " +
        "práctica para losasesinos a sueldo que suelen utilizar estos " +
        "atuendos, pues su línea de negocio suele atraer a clientes " +
        "suspicaces a los que no les " +
        "gustan los rostros con gestos de peligro y, mucho menos, los que " +
        "pertenecen a profesionales capaces de rastrearlos cuando intentan " +
        "huir de la atención que suele atraer el precio que tienen sus " +
        "cabezas.\n");

    setup_limitado_gloria();

    ajustar_BO(10);
    ajustar_BE(-10);
    fijar_encantamiento(30);
    add_static_property("critico", 3);
    add_static_property("evasion", 3);
    add_static_property("trampas", 25);
    add_static_property("sin-huella", 33);
    fijar_valor_venta(150000);
}
string descripcion_juzgar(object b)
{
    return "Mientras uses esta capa no encontrarás rastros falsos al " +
           "fallar al utilizar la habilidad 'rastrear'.\n";
}
