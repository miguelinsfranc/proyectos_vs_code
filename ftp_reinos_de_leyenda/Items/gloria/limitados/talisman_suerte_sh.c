// Satyr 17.01.2017
#include <clean_up.h>

private object pj, talisman;
private int modificador;

int dame_shadow_suerte() {
    return 1;
}
int dame_shadow_talisman_suerte() {
    return 1;
}
void destruir_shadow_talisman_suerte() {
    if ( pj )
        tell_object(pj, "Dejas de sentirte afortunad" + pj->dame_vocal() + ".\n");
        
    destruct(TO);
}
void setup_sombra(object _pj, object _talisman, int _modificador) {
    pj          = _pj;
    talisman    = _talisman;
    modificador = _modificador;
    
    shadow(pj, 1);
    
    tell_object(pj, "De repente todo parece mucho más positivo que antes. La suerte está contigo.\n");
}
int dame_ts_suerte(
    string tipo, int _modificador, string elemento, string nombre_efecto, int res, int quiet
) {
    if ( res > 0 || ! talisman || ! talisman->puede_suerte(pj) )
        return res;
        
    if ( ! res = call_other(
        pj,
        "dame_ts" + (quiet ? "_silenciosa" : ""),
        tipo, _modificador + modificador, elemento, nombre_efecto
    ) )
        return 0;
        
    talisman->poner_bloqueo(pj);
    return res;
}
int dame_ts(string tipo,int modificador,string elemento,string nombre_efecto) {
    return dame_ts_suerte(
        tipo, 
        modificador, 
        elemento, 
        nombre_efecto,
        pj->dame_ts(tipo, modificador, elemento, nombre_efecto),
        0
    );
}
int dame_ts_silenciosa(string tipo,int modificador,string elemento,string nombre_efecto) {
    return dame_ts_suerte(
        tipo, 
        modificador, 
        elemento, 
        nombre_efecto,
        pj->dame_ts_silenciosa(tipo, modificador, elemento, nombre_efecto),
        1
    );
}
