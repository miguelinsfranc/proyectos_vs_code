// Satyr 21/08/2018 12:55
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

#define PORCENTAJE_CURACION 10

void setup()
{
    fijar_armadura_base("pendiente");
    set_name("caracola");
    set_short("%^BLUE%^C%^CYAN%^aracola %^BLUE%^A%^CYAN%^cendrada%^RESET%^");
    set_main_plural(
        "%^BLUE%^C%^CYAN%^aracolas %^BLUE%^A%^CYAN%^cendradas%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Una concha de proporciones perfectas que fue traída al mundo por las "
        "diestras manos de un artesano y no por los caprichos de las "
        "corrientes. "
        "Su naturaleza artificial es evidente, pues está hecha de un cristal "
        "que refracta una miríada de tonos de azul y no de nácar blanquecino. "
        "No obstante, el nivel de "
        "detalle con el que se ha creado es tan realista que roza la "
        "perfección, pues su forma dextrógira se dobla sobre sí misma formando "
        "ángulos imposiblemente simétricos. Su interior alberga "
        "una luz mágica que, tal y como dicen los cuentos, susurra palabras "
        "ininteligibles con la dulce voz de la marea baja.\n");

    fijar_material(5);
    fijar_encantamiento(10);
    fijar_BO(0);
    setup_limitado_gloria();

    add_static_property("bien", -5);
    add_static_property("mal", -5);

    add_static_property(
        "messon",
        "Un aura azulada comienza a emanar de " + TU_S(TO) +
            " hasta rodearte "
            "por completo.\n");
    add_static_property(
        "messoff",
        "El aura de " + TU_S(TO) + " parpadea lentamente hasta desaparecer.\n");
}
object dame_shadow(object pj)
{
    return clone_object(base_name() + "_sh.c", pj, PORCENTAJE_CURACION);
}
string descripcion_juzgar(object pj)
{
    return sprintf(
        "Amplifica cualquier efecto de curación recibido en un %d%%.",
        PORCENTAJE_CURACION);
}
void objeto_desequipado()
{
    if (environment()) {
        environment()->destruir_sombra_caracola_acendrada();
    }
}
void objeto_equipado()
{
    if (environment() && !environment()->dame_efecto("caracola acendrada")) {
        dame_shadow(environment());
    }
}
