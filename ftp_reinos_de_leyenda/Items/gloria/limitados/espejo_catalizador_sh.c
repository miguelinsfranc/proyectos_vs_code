// Satyr 22/10/2019 12:25
#include <clean_up.h>

/*******************************************************************************
 * SECCIÓN: variables
 ******************************************************************************/
int    destruyendose, res_max, res_min;
object afectado, otro, artefacto;
string elemento;

/*******************************************************************************
 * SECCIÓN: lógica de shadow
 ******************************************************************************/
void setup_shadow(object _afectado, object _otro, mapping data)
{
    afectado  = _afectado;
    otro      = _otro;
    elemento  = data["elemento"];
    artefacto = data["artefacto"];
    res_max   = data["res_max"];
    res_min   = data["res_min"];

    shadow(afectado, 1);
    afectado->add_extra_look(TO);
}
string extra_look(object quien)
{
    string tx = afectado->extra_look(quien) || "";

    if (quien == TP) {
        tx += "Estás bajo los efectos de una catálisis elemental de '" +
              elemento + "'.\n";
    } else {
        tx += "Está bajo los efectos de una catálisis elemental de '" +
              elemento + "'.\n";
    }

    return tx;
}
void debug_shadow_espejo_catalizador()
{
    tell_object(
        TP,
        sprintf(
            "afectado: %O\n"
            "otro: %O\n"
            "artefacto: %O\n"
            "elemento: %O\n"
            "destruyendose: %O\n"
            "res (max/min): %O/%O\n",
            afectado,
            otro,
            artefacto,
            elemento,
            destruyendose,
            res_max,
            res_min));
}
int dame_shadow_espejo_catalizador()
{
    return 1;
}
void destruir_shadow_espejo_catalizador()
{
    if (destruyendose) {
        return;
    }

    destruyendose = 1;

    if (otro && otro->dame_shadow_espejo_catalizador()) {
        otro->destruir_shadow_espejo_catalizador();
    }

    if (afectado) {
        afectado->remove_extra_look(TO);
        tell_object(
            afectado,
            "La catálisis que afectaba a tus resistencias contra '" + elemento +
                "' se desvanece.\n");
    }

    destruct(TO);
}
void destruir_mi_shadow_espejo_catalizador(object _artefacto)
{
    if (artefacto == _artefacto) {
        destruir_shadow_espejo_catalizador();
    }
}
/*******************************************************************************
 * SECCIÓN: auxiliares
 ******************************************************************************/
mixed shadow_espejo_catalizador_lfun(string lfun, string tipo)
{
    int i;

    if (tipo != elemento) {
        return call_other(afectado, lfun, tipo);
    }

    lfun += "_catalizado";
    i = call_other(otro, lfun, tipo);

    if (i <= res_min) {
        return res_min;
    }

    if (i >= res_max) {
        return res_max;
    }

    return i;
}
/*******************************************************************************
 * SECCIÓN: enmascaradas
 ******************************************************************************/
int dame_resistencia(string tipo)
{
    return shadow_espejo_catalizador_lfun("dame_resistencia", tipo);
}
varargs mapping dame_resistencias(string tipo)
{
    return shadow_espejo_catalizador_lfun("dame_resistencias", tipo);
}
int dame_resistencia_equipo(string tipo)
{
    return shadow_espejo_catalizador_lfun("dame_resistencia_equipo", tipo);
}
int dame_resistencia_real(string tipo)
{
    return shadow_espejo_catalizador_lfun("dame_resistencia_real", tipo);
}
int dame_resistencia_permanente(string tipo)
{
    return shadow_espejo_catalizador_lfun("dame_resistencia_permanente", tipo);
}
int dame_resistencia_tmp(string tipo)
{
    return shadow_espejo_catalizador_lfun("dame_resistencia_tmp", tipo);
}
/*******************************************************************************
 * SECCIÓN: llamadas a funciones "reales" para depuración
 ******************************************************************************/
int dame_resistencia_catalizado(string tipo)
{
    return afectado->dame_resistencia(tipo);
}
int dame_resistencia_equipo_catalizado(string tipo)
{
    return afectado->dame_resistencia_equipo(tipo);
}
int dame_resistencia_real_catalizado(string tipo)
{
    return afectado->dame_resistencia_real(tipo);
}
int dame_resistencia_permanente_catalizado(string tipo)
{
    return afectado->dame_resistencia_permanente(tipo);
}
int dame_resistencia_tmp_catalizado(string tipo)
{
    return afectado->dame_resistencia_tmp(tipo);
}
