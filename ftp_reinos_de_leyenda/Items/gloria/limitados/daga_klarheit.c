// Eckol Jun18

#include <unidades.h>
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados";
inherit "/obj/arma";

#define NIVELES_PERDIDOS 1
#define CON_GANADA 5

int dame_legal_universal()
{
    return 1;
}

void actualizar_short();

int dame_poder()
{
    return query_property("poder");
}
void ajustar_poder(int i)
{
    add_property("poder", dame_poder() + i);
    actualizar_short();
}
void fijar_poder(int i)
{
    add_property("poder", i);
    actualizar_short();
}

void setup()
{
    fijar_arma_base("daga");
    set_short("Daga de la Esperanza de Klarheit");
    set_main_plural("Dagas de la Esperanza de Klarheit");
    generar_alias_y_plurales();
    set_long(
        "Aunque fue visto por última vez hace siglos aún son muchos los "
        "mortales "
        "que veneran a Klarheit, con al ansia de conseguir los poderes del "
        "vampiro. Los insensatos que en el pasado fueron a buscarle, cegados "
        "por "
        "la historia de amor eterno y poderes divinos, aprendieron con "
        "sufrimiento el alto coste del vampirismo.\nGuiados por la experiencia "
        "de un kobold que gozó y sufrió el toque de Klarheit, una tribu de "
        "chamanes ancarek observó que el cuerpo físico de los mortales no es "
        "capaz de soportar la presencia del poder vampírico, y se corrompe de "
        "forma lenta pero constante hasta perder la capacidad de sostener la "
        "vida.\nCon la ayuda del mejor herrero de Ancarak, forjaron esta daga, "
        "la única esperanza para alargar la existencia en el plano mortal de "
        "aquellos que aspiran a seguir los pasos de Klarheit.\nLa daga está "
        "forjada en un metal de alta calidad, que ha sido trabajado con gran "
        "esmero hasta conseguir un acabado brillante y sin ninguna "
        "imperfección "
        "aparente. La hoja está afilada en ambos lados, y lo largo de la parte "
        "central, una diminuta acanaladura recorre la hoja desde el mango "
        "hasta "
        "la punta. El mango está cubierto en cuero curtido, de color oscuro, y "
        "rematado con un rubí.\n");

    nuevo_efecto_basico("vampirismo", __ud(10, "%"), 0, 30);
    fijar_encantamiento(5);

    call_out(( : actualizar_short:), 0);
    add_extra_look(TO);

    setup_limitado_gloria();
}

string descripcion_juzgar()
{
    return "Al clavar la daga en el corazón de un cadáver, esta absorbe la "
           "esencia vital del cuerpo. Al realizar esta acción por quinta vez, "
           "el poder de la daga se desata, otorgándole a su portador 5 puntos "
           "de constitución a cambio de un nivel de personaje.\nLa daga solo "
           "absorbe poder de cuerpos de personaje jugador. El poder de la daga "
           "no se desata en personajes de nivel menor que 30, ni que hayan "
           "perdido poca constitución. La daga no modifica la constitución "
           "original.\n";
}

void actualizar_short()
{
    switch (dame_poder()) {
        case 1:
            set_short("Daga de la %^RED%^Es%^RESET%^peranza de Klarheit");
            return;
        case 2:
            set_short("Daga de la %^RED%^Espe%^RESET%^ranza de Klarheit");
            return;
        case 3:
            set_short("Daga de la %^RED%^Esperan%^RESET%^za de Klarheit");
            return;
        case 4:
            set_short("Daga de la %^RED%^Esperanza%^RESET%^ de Klarheit");
            return;
        case 5:
            set_short("Daga de la %^BOLD%^RED%^Esperanza%^RESET%^ de Klarheit");
            return;
        default:
            set_short("Daga de la Esperanza de Klarheit");
            return;
    }
}

string extra_look(object daga)
{
    switch (daga->dame_poder()) {
        case 1:
            return "Puedes ver una pequeña cantidad de sangre en el interior "
                   "de la "
                   "acanaladura de la hoja.\n";
        case 2:
            return "La mitad de la acanaladura más cercana a la empuñadura de "
                   "la daga "
                   "está llena de sangre de un color rojo brillante.\n";
        case 3:
            return "Más de la mitad de la acanaladura está llena de sangre, "
                   "que "
                   "destaca sobre el gris del metal con un color rojo "
                   "intenso.\n";
        case 4:
            return "Sangre de un color rojo brillante llena la acanaladura "
                   "casi hasta "
                   "la punta de la hoja.\n";
        case 5:
            return "La acanaladura está llena de sangre desde la empuñadura "
                   "hasta la "
                   "punta de la hoja.\n";
        default:
            return "La acanaladura de la hoja está vacía.\n";
    }
}

int dame_cuerpo_valido(object cuerpo)
{
    string d = cuerpo->dame_duenyo();

    if (!d)
        return 0;

    if (!user_exists(d))
        return notify_fail(
            "No aportará nada clavar la daga en el cuerpo de una "
            "criatura inferior.\n");

    if (cuerpo->query_static_property("daga_klarheit_usada"))
        return notify_fail(
            "Parece que este cuerpo no posee ya ninguna esencia vital.\n");

    return 1;
}

int chequeo_previo(object pj)
{
    if (pj->dame_nivel() < 30) {
        tell_object(
            pj,
            "Tu escaso nivel de experiencia impide que la daga desate su "
            "poder.\n");
        return 0;
    }

    if (pj->dame_carac("con_orig") - CON_GANADA > pj->dame_carac_real("con")) {
        return tell_object(
            pj,
            "Tu imponente constitución impide que la daga desate su poder.\n");
        return 0;
    }

    return 1;
}

int realizar_sacrificio(object pj)
{
    int perdida = TP->query_property("constitucion-perdida");
    if (!chequeo_previo(pj))
        return 0;

    if (dame_poder() < 5)
        return 0;

    tell_object(
        pj,
        sprintf(
            "Al llenarse de sangre, el poder de la %s se desata en tus manos, "
            "regenerando tu cuerpo a cambio de pagar un alto precio.\n",
            query_short()));
    TP->ajustar_nivel(-1);
    TP->recalcular_pvs_pes();
    TP->fijar_carac("con", TP->dame_carac_real("con") + CON_GANADA);

    if (perdida)
        TP->add_property(
            "constitucion-perdida",
            perdida > CON_GANADA ? perdida - CON_GANADA : 0);

    ajustar_vida(-2 * dame_vida_max());

    return 1;
}

int clavar_daga(object cuerpo)
{
    if (!dame_cuerpo_valido(cuerpo))
        return 0;

    // TODO a 2 manos, por ambientación.

    TP->accion_violenta();
    tell_object(
        TP,
        sprintf(
            "Clavas con todas tus fuerzas %s en el corazón del cadáver de "
            "%s.\n",
            TU_S(TO),
            capitalize(cuerpo->dame_duenyo())));
    tell_accion(
        ENV(TP),
        sprintf(
            "%s clava con todas sus fuerzas %s en el "
            "corazón del cadáver de %s.\n",
            TP->query_cap_name(),
            SU_S(TO),
            capitalize(cuerpo->dame_duenyo())),
        "",
        ({TP}));

    if (dame_poder() < 5)
        ajustar_poder(1);

    cuerpo->add_static_property("daga_klarheit_usada", 1);

    realizar_sacrificio(TP);

    return 1;
}

void init()
{
    ::init();
    anyadir_comando_equipado(
        "clavar",
        "la daga en el coraz(o|ó)n de "
        "<objeto:no_interactivo:aqui:funcion:dame_cuerpo:No "
        "se ha encontrado ningún cuerpo.'cuerpo'>",
        "clavar_daga");
}
