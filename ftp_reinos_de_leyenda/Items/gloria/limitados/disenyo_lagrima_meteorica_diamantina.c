// Satyr 05/11/2021 02:50
// Recompensa de gloria de la temporada 35
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/disenyo.c";

void setup()
{
    fijar_oficio("artesano");
    fijar_receta("lagrima meteorica diamantina");
    fijar_lenguaje("elfico");

    setup_limitado_gloria();
}
