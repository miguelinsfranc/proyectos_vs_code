// Satyr 03/08/2021 01:31
// Idea de
// Ver - https://www.reinosdeleyenda.es/foro/ver-tema/objetos-gloria-t-35/
#include <baseobs_path.h>
#include <pk.h>
inherit "/obj/armadura.c";
inherit "/obj/magia/objeto_con_poderes";

// clang-format off
#define _PALABRA_PODER_DESCIFRABLE_ALIAS ""
#define _PALABRA_PODER_DESCIFRABLE_IDIOMA "gargola"
#define _PALABRA_PODER_DESCIFRABLE_INSCRIPCION "Naxdat, que esta dote sirva " \
    "como ofrenda para obtener tu favor y el de tu primogénita. Recuerda el " \
    "dicho de los nuestros, pues despertará su verdadero potencial: \"" \
    + TO->dame_palabra_poder() + "\"."

#include "/baseobs/snippets/palabras_poder.c"
// clang-format on

#define CONJUNTO_GOLEM BCONJUNTOS + "fortaleza_golem_de_piedra.c"
#define DURACION 180
#define BLOQUEO 480
#define REDUCCION_DANYO_ENTRANTE 0.2
#define REDUCCION_DANYO_SALIENTE 0.2
#define NOMBRE_EFECTO "_" + obtener_info_ruta(TO)
#define NOMBRE_MODIFICADOR obtener_info_ruta(TO)

void fin_recubrirse_en_piedra(object b);

/*******************************************************************************
 * Sección: sistema
 ******************************************************************************/
void setup()
{
    fijar_armadura_base("grebas metalicas");
    set_name("grebas");
    set_short("%^ORANGE%^Grebas del Gólem de Piedra%^RESET%^");
    set_main_plural("pares de %^ORANGE%^Grebas del Gólem de Piedra%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        "Dos finas láminas de una aleación de hierro, berilio y mithril que "
        "poseen un par de broches cada una "
        "haciendo que pueden ser usadas a modo de grebas, si bien su "
        "aspecto es demasiado endeble como para invitar a hacerlos.\n\n"

        "  Su parte superior —la que protegería la rodilla— está coronada con "
        "una perfecta obra de orfebrería circular compuesta de una miríada de "
        "gemas en perfecta armonía cromática.\n\n"

        "  Sendas piezas están cubiertas de una pátina de polvo de roca que, "
        "por más que muevas la pieza, no parece desprenderse de su "
        "superficie y parece acumularse en unas muescas inscritas en "
        "su parte interior.\n");

    fijar_encantamiento(10);
    add_static_property("evasion", 6);
    add_static_property("armadura-magica", 5);
    add_static_property(
        "messon",
        "Tus " + query_short() +
            " crujen cuando una fina capa de piedra agrietada comienza a "
            "trepar desde las gemas que las adornan hasta recubrirlas por "
            "completo.\n");
    add_static_property(
        "messoff",
        "La pátina de piedra que recubría a tus " + query_short() +
            " se resquebraja y cae al suelo como si de escamas de piel "
            "muerta se trataran.\n");

    // Poderes y palabra de poder
    fijar_bloqueo_poderes(BLOQUEO);
    dame_palabra_poder();

    set_read_mess(
        "Las muescas inscritas del interior de estas grebas están repletas de "
        "polvo. No logras reconocer, a simple vista, en qué idioma están "
        "escritas.",
        "comun",
        0);

    fijar_conjuntos(({CONJUNTO_GOLEM}));
}
void objeto_desequipado()
{
    if (environment()) {
        fin_recubrirse_en_piedra(environment());
    }
}
string descripcion_juzgar(object b)
{
    return sprintf(
        "Este poder permite despertar la energía encerrada dentro "
        "de las piezas del conjunto 'Fortaleza del Gólem de Piedra'. "
        "Será necesario equipar tres piezas del conjunto para poder usarlo.\n\n"

        " Al realizar la activación del poder el ejecutor se verá envuelto en "
        "una densa barrera de piedra que será capaz de protegerle "
        "del daño físico de combate proveniente de otro causante que no sea "
        "él mismo, absorbiendo así un %.2f%% del daño entrante.\n\n"

        " Como contrapartida, todo el daño de combate que realice se"
        " verá disminuido en un %.2f%% durante la duración del poder, "
        "que es de %s con %s de bloqueo.\n\n"

        " Además, todos los enemigos con los que esté en "
        "peleas (en su sala o en otra) comenzarán a centrar de "
        "forma inmediata sus ataques sobre él.\n\n",
        to_float(REDUCCION_DANYO_ENTRANTE * 100.0),
        to_float(REDUCCION_DANYO_SALIENTE * 100.0),
        pretty_time(DURACION),
        pretty_time(BLOQUEO));
}
/*******************************************************************************
 * Sección: poderes
 ******************************************************************************/
/**
 * Función llamada cuando el poder se termina.
 *
 * @param b Quién activa el poder
 */
void fin_recubrirse_en_piedra(object b)
{
    if (!b) {
        return;
    }

    b->quitar_modificador_combate(NOMBRE_MODIFICADOR);
    b->destruir_sombra_grebas_golem_de_piedra();
    b->quitar_efecto(NOMBRE_EFECTO, 1);
}
/**
 * Función que gestiona la activación del poder.
 *
 * @param  quien   Quien lo activa (el portador)
 * @param  victima Esto va a ser 0
 * @param  poder   Nombre del poder activado
 * @param  tipo    Tipo de herida que va a quitar
 * @return         1 en caso de éxito, 0 en caso contrario.
 */
int do_recubrirse_en_piedra(
    object quien, object victima, string poder, string tipo)
{
    object sh;
    string err;

    if (err = chequeo_previo_finalizar_poder(quien, victima, poder)) {
        tell_object(quien, err + "\n");
        return 0;
    }

    mostrar_mensajes_poderes("finalizacion", quien, 0, poder, tipo);

    // clang-format off
    sh = clone_object(base_name() + "_sh.c");
    quien->nuevo_efecto(
        NOMBRE_EFECTO,
        DURACION,
        (: fin_recubrirse_en_piedra :),
        sh,
        TO
    );
    quien->nuevo_modificador_combate(
        NOMBRE_MODIFICADOR,
        DURACION,
        ([ "multiplicador_daño_final": REDUCCION_DANYO_SALIENTE ]),
        2,
        1,
        base_name()
    );

    if ( sh ) {
        sh->setup_sombra(quien, TO, REDUCCION_DANYO_ENTRANTE);
    }
    // clang-format on

    quien->dame_lista_enemigos()->fijar_centrar_ataques(quien);

    return 1;
}
/**
 * Acción disparada al activar el poder del objeto.
 *
 * @param  ppoder palabra de poder usada para activar el objeto
 */
int recubrirse_en_piedra(string ppoder)
{
    object conjunto;

    if (ppoder != dame_palabra_poder()) {
        return notify_fail(sprintf(
            "%s no parece responder a esa palabra de poder. Tendrás que " +
                "descubrir cuál es para poder usar este poder.\n",
            capitalize(dame_tu()) + " " + query_short()));
    }

    conjunto = load_object(CONJUNTO_GOLEM);

    if (!conjunto || conjunto->dame_piezas_equipadas(TP) < 3) {
        return notify_fail(
            "Necesitas equiparte más piezas del conjunto '" +
            conjunto->query_short() + "' para poder usar este poder.\n");
    }

    if (!chequeo_activacion_poder(TP, TP)) {
        return 0;
    }

    mostrar_mensajes_poderes("activacion", TP);
    poner_bloqueos(TP);

    // clang-format off
    call_out(
        "finalizacion_poder",
        2 + (TP->query_hb_counter() % 2) * 2,
        TP,
        TP,
        0,
        (: do_recubrirse_en_piedra :)
    );
    // clang-format on

    return 1;
}
void init()
{
    ::init();

    // clang-format off
    anyadir_comando_equipado(
        "bendicion_naxdat",
        "<texto'Palabra de poder'>",
        (: recubrirse_en_piedra($1) :),
        descripcion_juzgar(0)
    );
    // clang-format on
}

/*******************************************************************************
 * * Sección: Eventos externos
 ******************************************************************************/
/**
 * Función auxiliar para agrupar aquí la reducción de daño de weapon_damage y
 * unarmed_damage.
 *
 * @param  afectado Objeto afectado por el poder del objeto
 * @param  pupa     Cantidad de daño/curación recibida
 * @param  causante Causante del daño/curación (puede ser 0)
 * @param  arma     Arma o estilo que realiza el daño
 * @param  ataque   Nombre del ataque del arma que realiza el daño
 * @return          Cantidad de daño/curación a aplicar
 */
int evento_ajustar_pvs(
    object afectado, int pupa, object causante, object arma, string ataque)
{
    if (pupa > 0 || causante == afectado || !causante) {
        return pupa;
    }

    if (arma && ataque && arma->es_ataque_magico(ataque)) {
        return pupa;
    }

    return to_int(pupa - pupa * REDUCCION_DANYO_ENTRANTE);
}
/**
 * Evento llamado desde la sombra cada vez que el afectado recibe
 * unarmed_damage.
 *
 * Los parámetros son los esperados (man unarmed_damage)
 *
 * La función devolverá un valor que será el daño que será aplicado por la
 * sombra.
 */
varargs int evento_unarmed_damage(
    object afectado, int pupa, object estilo, object atacante, string tipo_pupa,
    string nombre_ataque, mapping cfg_ataque)
{
    return evento_ajustar_pvs(afectado, pupa, atacante, estilo, nombre_ataque);
}
/**
 * Evento llamado desde la sombra cada vez que el afectado recibe weapon_damage.
 *
 * Los parámetros son los esperados (man weapon_damage)
 *
 * La función devolverá un valor que será el daño que será aplicado por la
 * sombra.
 */
varargs int evento_weapon_damage(
    object afectado, int cantidad, object atacante, object arma,
    string tipo_pupa, string nombre_ataque, mapping cfg_ataque)
{
    return evento_ajustar_pvs(
        afectado, cantidad, atacante, arma, nombre_ataque);
}
