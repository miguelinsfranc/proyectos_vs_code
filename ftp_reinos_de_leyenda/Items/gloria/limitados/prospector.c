// Satyr 22.08.2016
// Eckol 04May17: Legal universal. Para que lo puedan usar aunque no puedan usar armas tipo pico.
#include <pk.h>
#include <fronteras.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/arma";

#define BLOQUEO     180
#define BONO_PICAR   10

int dame_no_limpiable() {
    return -1 != find_call_out("inicio") || -1 != find_call_out("buscar");
}
int dame_bono_picar(mixed mineral) {
    return BONO_PICAR;
}

int dame_legal_universal() { return 1; }
void setup()
{
    string aux;
    
    fijar_arma_base("pico");
    set_name("omoplato");
    set_short("%^WHITE%^O%^BOLD%^móplato %^ORANGE%^P%^BOLD%^rospector%^RESET%^");
    set_main_plural("%^WHITE%^O%^BOLD%^móplatos %^ORANGE%^P%^BOLD%^rospectores%^RESET%^");
    
    generar_alias_y_plurales();
    
    set_long(
        "Un omoplato hueco y desvaído cuya superficie porosa está cubierta de una " 
            "fina capa de tierra húmeda que emite un fuerte olor a descomposición. "
            "Su extremo superior está afilado y tiene una extraña oquedad "
            "que penetra hasta el oscuro interior de esta siniestra herramienta.\n\n"
            
        "  El pequeño túnel, que otrora hospedaba a la médula, hoy cobija a un enorme "
            "y abotargado gusano que palpita y secreta una sustancia verdosa. La "
            "criatura, que tolera su presencia con una pachorra inaudita en "
            "una criatura de su aparente intelecto, ha elegido este "
            "hueso como su hogar y como tal parece estar dispuesta a defenderlo.\n\n"
            
        "  Aunque el origen de este extraño artefacto parece nacer en una combinación "
            "fortuita de factores, el hecho de que el hueso transpire energía mágica "
            "te hace pensar que el objeto ha sido creado por alguien que sabía lo "
            "que hacía.\n"
    );

    fijar_material(7);
    fijar_encantamiento(30);
    quitar_ataque("tabla");
    nuevo_ataque("tabla", "penetrante", 2, 40, 20);
    nuevo_ataque("veneno", "veneno", 10, 5, 0);
    
    aux = nuevo_efecto_basico("tormenta", 100, 20, 20);
    fijar_ataques_efecto(aux, ({"veneno"}));
    fijar_params_efecto(aux, "veneno");
    fijar_sms_efecto(aux, (: ({
        "¡El gusano de " + TU_S(TO) + " vomita un horripilante charco de veneno que cubre el entorno!",
        "¡Un gusano surge del " + query_short() + " de " + $1->query_cap_name() + " y escupe un charco de veneno!",
        ""
    }) :) );
    
    fijar_material(9);
    
//    fijar_valor(10000);
//    fijar_coste_gloria(175);
    setup_limitado_gloria();
}
string descripcion_juzgar(object pj) {
    return "Este arma, creada por la forja orgánica Svirfneblin, puede utilizarse para clavarse en "
        "el suelo de una zona minera y detectar todas las vetas que se hallen en ese lugar. Adicionalmente, "
		"proporciona una bonificación de "+dame_bono_picar("todo")+" a la habilidad 'picar'. \n"
    ;
}
varargs string *dame_mensaje_combate_impacto(
    int pupa, string tipo_ataque, string loc, object at, object def,
        mapping res, string n_ataque, object arma, mapping config
    )
{
    if ( tipo_ataque != "veneno" )
        return ::dame_mensaje_combate_impacto(
            pupa, tipo_ataque, loc, at, def, res, n_ataque, arma, config
        );
    else {
        string txt_pupa = dame_texto_pupa(tipo_ataque, pupa);
        
        return ({   
            "El gusano de " + TU_S(TO) + " rocía" + txt_pupa + " " + loc + " de " 
                + def->query_cap_name() + " con un chorro de veneno.",
            
            "Un gusano emerge del arma de " + at->query_cap_name() + " y te rocía"
                + txt_pupa + " " + loc + " con un chorro de veneno.",
                
            "Un gusano emerge del arma de " + at->query_cap_name() + " y rocía"
                + txt_pupa + " " + loc + " de " + def->query_cap_name() + " con un "
                "chorro de veneno.",
        });
    }
}
void debug(string tx) {
    __debug_x(tx, "prospector");
}
void recoger() {
    set_get();
}
object *buscar_vetas(object sala) {
    return sala ? filter(all_inventory(sala), (: base_name($1) == "/obj/veta" :)) : ({});
}
int sala_valida(object b) {
    return 1;
}
void terminar(string *fronteras, object pj, mapping vetas) {
    recoger();
    
    tell_room(
        environment(TO),
        "¡Un horripilante gusano emerge del suelo y se introduce, rápidamente, "
            "en el interior de " + query_short() + "!\n",
        "",
        ({}),
        TO
    );
    
    if ( pj && ! m_sizeof(vetas) ) {
        tell_object(
            pj, 
            "El huésped de " + TU_S(TO) + " te hace saber, con un enlace telepático, "
                "que no hay ninguna veta de mineral en la zona.\n"
        );
    }
    
    if ( pj && m_sizeof(vetas) ) {
        tell_object(
            pj, 
            "El huésped de " + TU_S(TO) + " te hace saber, con un enlace telepático, "
                "la ubicación de las vetas de mineral de la zona:\n"
        );
        
        foreach(string zona, object *vv in vetas) {
            tell_object(TP, " - " + zona + ": " + query_multiple_short(vv) + "\n");
        }
    }
}
void buscar(string *fronteras, string *restantes, object pj, mapping vetas) {
    int tope = 15;
    
    if ( ! restantes || ! sizeof(restantes) ) {
        terminar(fronteras, pj, vetas);
        return;
    }
    
    for(int i = 0; i < tope && i < sizeof(restantes); i++) {
        object sala, *vetas_ob;
        
        catch { sala = find_object(restantes[i]) || load_object(restantes[i]); };
        
        if ( ! sala || ! sala_valida(sala) )
            continue;
        
        if ( ! sizeof(vetas_ob = buscar_vetas(sala)) )
            continue;
        
        if ( ! vetas[sala->query_short()] )
            vetas[sala->query_short()] = ({});
        
        vetas[sala->query_short()] += vetas_ob;
    }
    
    tell_accion(
        environment(TO), 
        "Notas los rápidos movimientos de una criatura excavando "
            "en el subsuelo.\n",
        "",
        ({environment(TO)}),
        TO
    );
    
    restantes = restantes[tope..];

    call_out("buscar", 4, fronteras, restantes, pj, vetas);
}
void inicio(object pj) {
    string *fronteras = FRONTERAS_DAME_FRONTERA_JUGADOR(environment());
    string *restantes;
    
    if ( ! fronteras || ! sizeof(fronteras) ) {
        recoger();
        return;
    }
    
    fronteras = fronteras[0.._POS_ZONA];
    restantes = FRONTERAS_DAME_SALAS_FRONTERA(fronteras);
    
    if ( ! restantes || ! sizeof(restantes) ) {
        recoger();
        return;
    }
    
    buscar(fronteras, restantes, pj, ([]));
}
int activar_prospector() {
    if ( ! query_in_use() )
        return notify_fail("Has de empuñar " + TU_S(TO) + " para poder usarl" + dame_vocal() + ".\n");
    
    if ( dame_bloqueo_combate(TO) )
        return notify_fail(
            "El gusano de " + TU_S(TO) + " parece estar un poco remolón y no tiene ganas de "
                "abandonar su morada.\n");
    
    if ( ! environment(TP) || ! environment(TP)->dame_mina() )
        return notify_fail("El poder de " + TU_S(TO) + " solo puede activarse en minas.\n");
    
    tell_object(
        TP, 
        "De un fuerte golpe, plantas " + TU_S(TO) + " en el suelo, haciendo que su siniestro "
            "habitante abandone su morada para cavar hasta colarse por un agujero "
            "en el subsuelo de la zona.\n"
    );
    tell_accion(
        environment(TP),
        TP->query_cap_name() + " planta " + SU_S(TO) + " en el suelo de un fuerte golpe, "      
            "haciendo que un abotargado gusano repte de su interior y comience a cavar "
            "hasta introducirse por completo en el subsuelo.\n",
        "",
        ({TP}),
        TP
    );
    
    nuevo_bloqueo_combate(TO, BLOQUEO);
    TO->move(environment(TP));
    TO->reset_get();
    call_out("inicio", 4, TP);
    return 1;
}
void init() {
    ::init();
    
    anyadir_comando_equipado(
        "plantar",
        query_name(),
        (: activar_prospector :),
        "Planta " + TU_S(TO) + " en el suelo, haciendo que su morador explore el subsuelo "
            "en busca de vetas de mineral."
    );
}
