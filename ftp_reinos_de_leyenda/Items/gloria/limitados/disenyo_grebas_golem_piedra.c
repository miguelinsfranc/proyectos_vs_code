// Satyr 06/08/2021 02:32
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/disenyo.c";
void setup()
{
    fijar_oficio("herrero");
    fijar_receta("grebas del golem de piedra");
    fijar_lenguaje("gargola");

    setup_limitado_gloria();
}
