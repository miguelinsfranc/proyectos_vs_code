// Satyr 15/03/2021 02:18

#include <pk.h>
#include <material.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

private
void __generar_short();

void setup()
{
    fijar_armadura_base("collar");

    __generar_short();
    generar_alias_y_plurales();
    set_long(
        "Un extraño talismán que más parece fruto de un taxidermista que de " +
        "un artesano, pues está hecho a partir del cadáver disecado de " +
        "un ejemplar de crótalo de aproximadamente un metro de tamaño total, " +
        "de cabeza a cola.\n\n" +

        "  El curioso fetiche tiene forma circular, pues la cabeza de la " +
        "criatura está mordiendo la parte final de su propia cola, lo que " +
        "hace que sus colmillos hagan las veces de broches. El peso del " +
        "vistoso nudo formado por el " +
        "mordisco y el estuche córneo —que cuelga flácido y aún emite su " +
        "característico sonido al moverse— hace que este tienda a moverse " +
        "hacia abajo, haciendo que luzca en el pecho de quien lo porte.\n\n" +

        "  Collares como este eran usados en las ceremonias rituales de los " +
        "pueblos que " +
        "habitaban en los inhóspitos desiertos que antaño cubrían la mayor " +
        "parte de la nación hoy conocida como Dendra. La longevidad " +
        "antinatural de los sacerdotes que la llevaban era bien reseñable, " +
        "si bien ésta no fue suficiente para salvarles del funesto final que " +
        "borró a su pueblo de los recueros de la historia.\n");

    add_static_property("fue", -1);
    add_static_property("des", -1);
    ajustar_BO(-8);

    add_static_property("con", 1);
    ajustar_BE(8);
    ajustar_BP(8);

    fijar_material(CUERO);
    setup_limitado_gloria();
    fijar_valor_venta(125000);

    add_static_property(
        "messon",
        capitalize(dame_tu()) + " " + query_short() + " se mueve mientras te " +
            dame_lo() + " colocas, haciendo que su cascabel emita su " +
            "característico sonido hueco.\n");
}
private
void __generar_short()
{
    string  nombre, plural, g_nombre = "", g_plural = "";
    string *colores = ({
        "%^GREEN%^",
        "%^ORANGE%^",
        "%^BOLD%^BLACK%^",
    });
    int     k;

    nombre = "Juramento del Crótalo";
    plural = "Juramentos del Crótalo";

    for (int i = 0; i < sizeof(nombre); i += 2) {
        g_nombre += colores[k] + nombre[i..i + 1] + "%^RESET%^";

        if (k++ >= 2) {
            k = 0;
        }
    }

    for (int i = 0; i < sizeof(plural); i += 2) {
        g_plural += colores[k] + plural[i..i + 1] + "%^RESET%^";

        if (k++ >= 2) {
            k = 0;
        }
    }

    set_short(g_nombre + "%^RESET%^");
    set_main_plural(g_plural + "%^RESET%^");
}
