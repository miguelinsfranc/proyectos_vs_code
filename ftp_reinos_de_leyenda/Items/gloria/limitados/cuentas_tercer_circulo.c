// Satyr 30/11/2020 03:09
#include <pk.h>
#include <baseobs_path.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void generar_religion();

void setup()
{
    fijar_armadura_base("collar");
    set_name("cuentas");
    set_short(
        "%^Cuentas del %^RED%^BOLD%^Tercer%^RESET%^%^CYAN%^ Círculo%^RESET%^");
    set_main_plural("collares de %^Cuentas del "
                    "%^RED%^BOLD%^Tercer%^RESET%^%^CYAN%^ Círculo%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Un vulgar collar hecho con un tendón del que cuelgan varias cuentas " +
        "redondas hechas de madera maciza. Tienen el tamaño de un huevo y " +
        "todas ellas están perfectamente coloreadas.\n\n"

        "  Extrañamente no huele a nada, lo que es de extrañar ya que los " +
        "colores no presentan daños, el collar parece antiguo y, sin embargo" +
        ", no parece que haya ningún tipo de tratamiento mundano en la " +
        "madera que proteja sus vivos pigmentos.\n\n"

        "  No obstante, lo más llamativo de la pieza son los símbolos que " +
        "están grabados en las esferas: la silueta de una cabeza, un ojo " +
        "atravesado por un rayo y una mandíbula estridente.\n\n"

        "  Todo el misterio que rodea al collar deja claro que se trata de " +
        "un artefacto mágico. Su origen, por desgracia, no te es conocido.\n");

    fijar_material(1);
    fijar_encantamiento(15);

    setup_limitado_gloria();

    add_static_property("negacion-conjuro", 8);
    add_static_property("negacion-aliento", 5);
    add_static_property("negacion-mental", 5);
    add_static_property("negacion-horror", 5);

    fijar_conjuntos(({BCONJUNTOS + "enigma_9_circulos.c"}));

    set_read_mess(
        "El dominio de los tres primeros círculos es tuyo. La mente cierra " +
            "la puerta al miedo. La voluntad la abre al poder arcano. Que " +
            "estas " +
            "runas te acompañen por siempre en tu camino a la iluminación.",
        "elfico");
}
