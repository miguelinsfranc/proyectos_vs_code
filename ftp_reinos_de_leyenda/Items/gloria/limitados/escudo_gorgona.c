// Satyr 29/07/2020 12:04
#include <baseobs_path.h>
#include <combate.h>
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/escudo";
inherit "/obj/magia/objeto_con_poderes";

void setup()
{
    string tx;

    fijar_escudo_base("escudo");
    set_name("broquel");
    set_short("%^BLACK%^BOLD%^B%^RESET%^GREEN%^roquel de la "
              "%^BLACK%^BOLD%^G%^%^RESET%^GREEN%^orgona%^RESET%^");
    set_main_plural("%^BLACK%^BOLD%^B%^RESET%^GREEN%^roqueles de la "
                    "%^BLACK%^BOLD%^G%^%^RESET%^GREEN%^orgona%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Un broquel de aspecto metálico que ha sido tiznado por un sinfín "
        "de manchas "
        "ocasionadas por el inexorable paso del tiempo. A pesar de su "
        "aspecto, está hecho de madera; ébano, concretamente. No "
        "obstante, su superficie ha sido revestida con la dura piel de "
        "una gorgona, lo que le da al escudo la apariencia metálica que "
        "previamente te había engañado.\n\n"

        "  La piel de la bestia se ha remachado con clavos de mithril para "
        "poder sujetarla -pues es este uno "
        "de los pocos minerales lo suficientemente fuertes como para "
        "perforarla y sostenerla sin convertirse en piedra- y gracias a "
        "ello todo el frontal de la pieza está protegido por la bendición "
        "de esta criatura mítica.\n\n"

        "  Una elaborada -¡pero recargada y ostentosa!- cabeza de gorgona "
        "hecha de cuero duro adorna el frontal del escudo. Lo que otrora "
        "fueron dos cuernos parecen haber sucumbido a algun golpe del "
        "pasado y ahora no son más que oquedades en la testa "
        "de la bestia. El único ojo de rubí que aún no se ha desprendido "
        "de la pieza parece brillar de forma ominosa.\n");

    fijar_encantamiento(30);

    add_static_property("poder_magico", 20);
    add_static_property("armadura magica", 5);
    add_static_property("poder_curacion", 20);
    add_static_property("ts-petrificacion", 15);

    tx = nuevo_efecto_basico("debilidad");
    fijar_triggers_efecto(tx, ({_TRIGGER_PARA, _TRIGGER_PARA_C}));

    setup_limitado_gloria();
    fijar_conjuntos(({BCONJUNTOS + "panoplia_xapitalh.c"}));
}
