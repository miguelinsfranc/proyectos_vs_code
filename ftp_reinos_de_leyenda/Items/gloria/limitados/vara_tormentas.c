// Satyr 22.08.2016
#include <pk.h>
#include <combate.h>
#include <depuracion.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/arma";

#define PODER    100
#define DURACION  30

//int no_catalogar_armeria() { return 1; }

string descripcion_juzgar(object juzgador) {
    return "Cuando esta vara esté empuñada todos los hechizos de area lanzados por su portador "
        "se considerarán afectados por el efecto Tormenta(" + PODER + ") de " 
        + pretty_time(DURACION) + " de duración."
    ;
}
void setup()
{
    fijar_arma_base("vara");
    set_name("vara");
    
    set_short("%^CYAN%^Vara de las tormentas%^RESET%^");
    set_main_plural("%^CYAN%^Varas de las tormentas%^RESET%^");
    generar_alias_y_plurales();
    
    set_long(
        "Un rayo electrizante que está envuelto por un misterioso campo de fuerza que "
            "permite que sea sujetado. Su poder mágico es innegable y éste es tal "
            "que solo puede ser manipulado por los versados en las artes arcanas. "
            "Su mera existencia es un misterio. Pero un misterio de los que involucran "
            "muchas horas encerradas en un laboratorio.\n"
    );

    fijar_material(10);
    fijar_encantamiento(30);
    
    add_static_property("copias", 1);
    add_static_property("tipo", "hechicero");
    
    quitar_ataque("tabla");
    nuevo_ataque("tabla", "electrico", 1, 64, 2); // Lo mismo que una vara
    nuevo_efecto_basico("coeficiente", 0.7);
    add_static_property("poder_magico", 20);
//     fijar_valor(4000);
//    fijar_coste_gloria(300);
    setup_limitado_gloria();
}
void objeto_equipado() {
    object sh = clone_object(base_name() + "_sh.c");
    
    if ( sh )
        sh->setup_sombra(environment(), TO);
}
void objeto_desequipado() {
    environment()->destruir_shadow_vara_tormentas();
}
void disparar_efecto(object hechizo, object lanzador, mixed objetivo) {
    object tor, obj;
    
    if ( ! hechizo || lanzador != environment() ) {
        __debug_x("Cancelamos en vara(1)", "vara_tormentas", 0, 1);
        return;
    }
    
    if ( ! tor = EFECTO_BASICO("tormenta") ) {
        __debug_x("Cancelamos en vara(2)", "vara_tormentas", 0, 1);
        return;
    }
    
    if ( ! objetivo ) {
        __debug_x("Cancelamos en vara(3)", "vara_tormentas", 0, 1);
        return;
    }
    
    if ( ! obj = arrayp(objetivo) ? element_of(objetivo) : objetivo ) {
        __debug_x("Cancelamos en vara(4)", "vara_tormentas", 0, 1);
        return;
    }
    
    __debug_x("Disparamos desde vara (" + base_name(obj) +")", "vara_tormentas", 0, 1);
    tor->iniciar_efecto(
        obj, 
        environment(),
        TO,
        dame_ataque_principal(),
        0,
        0,
        "tormenta",
        PODER,
        DURACION,
        0,
        "electrico"
    );
}
