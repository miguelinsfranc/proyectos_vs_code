// Satyr 16/03/2021 02:12
#include <depuracion.h>
#include <combate.h>
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

// Configuración de objeto con poderes
inherit "/obj/magia/objeto_con_poderes";
#define BLOQUEO 180
#define MOLDEAR_HERIDA "hemalurgia"

// Configuración de palabra de poder
#define _PALABRA_PODER_DESCIFRABLE_ALIAS ""
#define _PALABRA_PODER_DESCIFRABLE_IDIOMA "arcano"
#define _PALABRA_PODER_DESCIFRABLE_NIVEL 3
#define _PALABRA_PODER_DESCIFRABLE_INSCRIPCION                                 \
    "La palabra de poder es '" + dame_palabra_poder() + "'."
#include "/baseobs/snippets/palabras_poder.c"

/*******************************************************************************
 * Sección: Prototipos
 ******************************************************************************/
void otorgar_bonificacion(object quien, string tipo_herida);
void quitar_bonificacion(object quien, mixed data);

/*******************************************************************************
 * Sección: Sistema
 ******************************************************************************/
void setup()
{
    fijar_armadura_base("pendiente");

    fijar_genero(2);
    set_name("aguja");
    set_short("Aguja %^RED%^Hemalúrgica%^RESET%^");
    set_main_plural("Agujas %^RED%^Hemalúrgicas%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        "Una pieza de metal de aspecto cónico que presenta una ligera " +
        "curvatura. " +
        "Su forma recuerda a la de un colmillo, pero ambos extremos son " +
        "redondeados y no poseen filo alguno.\n\n" +

        "  Su superficie es suave. Muy suave. De hecho, creo que es " +
        "importante que entiendas %^CURSIVA%^cómo%^RESET%^ de suave es. " +
        "Imagina que alguien fuese capaz de extraer la esencia del pelaje " +
        "de una decena de camadas de gatitos y reducirlo a un extracto tan " +
        "puro cuyo solo contacto hiciese que tu sentido del tacto fuese " +
        "incapaz de procesarlo sin causar que tus extremidades se durmiesen " +
        "y todo tu poder cognitivo se redujese al de una trucha no muy " +
        "sagaz. " +
        "Esa misma esencia es la que parece haber sido imbuida en este " +
        "pendiente, haciendo que la sensación de sostenerlo sea tan efímera " +
        "e incorpórea como el intentar agarrar al vuelo un verso recién " +
        "recitado en un melancólico atardecer de Sayelie.\n\n" +

        "  Presenta un patrón de coloración marmoleado donde motas de tonos " +
        "cobre, verde y plateado se arremolinan entre sí generando curiosas " +
        "formas que fuerzan a tu mente a trabajar para intentar formar " +
        "símiles con objetos del mundo real. Sin embargo, examinando la " +
        "aguja con más detenimiento te das cuenta de que pequeñas letras " +
        "garabateadas parecen cubrir su superficie, pero su idioma no se " +
        "parece en nada a ninguna cosa que hayas visto y requerirán estudio " +
        "y dedicación para averiguar el secreto que encierran.\n\n" +

        "  El motivo de todas estas peculiares propiedades no se debe a la " +
        "magia —¡por una vez!— si no a que éste objeto no pertenece a esta " +
        "dimensión. Su origen es desconocido para cualquier habitante de " +
        "Eirea, sea mortal o divino, y la única explicación de que esté " +
        "presente ahora en tus manos es que algo o alguien lo haya traído " +
        "aquí en algún momento. Las leyes que rigen Eirea no se aplican " +
        "de igual manera a este fragmento ajeno a esta realidad, por lo que " +
        "intentar usar la lógica que conoces para explicar su funcionamiento " +
        "nunca llegará a ningún sitio.\n\n" +

        "  Encierra un poder desconocido en esta dimensión: la hemalurgia. " +
        "Este arte permite imbuir objetos con propiedades sobrenaturales " +
        "que están, de alguna forma, relacionadas con los distintos metales " +
        "que forman estas piezas. Dicho poder viene con un precio, si bien " +
        "es algo que no te vamos a decir... todavía.\n");

    // Balance
    ajustar_BO(5);
    ajustar_BP(5);
    ajustar_BE(-2);
    fijar_encantamiento(20);
    setup_limitado_gloria();
    fijar_tamanyo_armadura(0);
    add_static_property("regeneracion_pvs", 20);
    add_static_property("ts-mental", 5);

    // Poderes
    fijar_bloqueo_poderes(BLOQUEO, MOLDEAR_HERIDA);

    // Palabra de poder para el anyadir_comando
    dame_palabra_poder();

    add_static_property(
        "messon",
        "Al ponerte " + TU_S(TO) + " notas como ésta te hace un poco de " +
            "sangre; al instante, y desde otra dimensión, la ominosa " +
            "presencia " +
            "de la Ruina parece fijar su atención en ti sin que siquiera te "
            "des cuenta de tal hecho.\n");
}
/**
 * Descifrar no da puntos de oficio
 */
int descifrar_sin_mejora_oficio()
{
    return 1;
}
/**
 * Desequipar la aguja quita el bonificador
 */
void objeto_desequipado()
{
    if (environment()) {
        environment()->quitar_efecto("aguja hemalurgica");
    }
}
/**
 * La ayuda del comando
 */
string descripcion_juzgar(object b)
{
    return sprintf(
        "%s %s es un objeto extradimensional que encierra un " +
            "poder desconocido en Eirea capaz de curar heridas que afecten " +
            "a un jugador y catalizarlas en forma de bonificaciones. La " +
            "naturaleza de estas bonificaciones dependerá de la herida " +
            "catalizada y su duración dependerá de un factor aleatorio. " +
            "Este poder tiene un bloqueo de " + pretty_time(BLOQUEO) + ".\n",
        capitalize(dame_articulo()),
        query_short());
}
/*******************************************************************************
 * Sección: Poderes
 ******************************************************************************/
/**
 * Función que gestiona la activación del poder.
 *
 * @param  quien   Quien lo activa (el portador)
 * @param  victima Esto va a ser 0
 * @param  poder   Nombre del poder activado
 * @param  tipo    Tipo de herida que va a quitar
 * @return         1 en caso de éxito, 0 en caso contrario.
 */
int do_catalizar(object quien, object victima, string poder, string tipo)
{
    string err;

    if (err = chequeo_previo_finalizar_poder(quien, victima, poder)) {
        tell_object(quien, err + "\n");
        return 0;
    }

    if (!quien->quitar_incapacidad(tipo, "heridas", 0, TO)) {
        return notify_fail(sprintf(
            "La vibración de %s se detiene de súbito: " +
                "no parece haber sido capaz de cerrar mediante hemalurgia " +
                "tu herida %s.\n",
            TU_S(TO),
            tipo));
    }

    mostrar_mensajes_poderes("finalizacion", quien, 0, poder, tipo);
    otorgar_bonificacion(quien, tipo);
    return 1;
}
/**
 * Función llamada al finalizar con éxito la hemalurgia de una herida. Otorgará
 * al portador una bonificación en función del tipo de herida.
 *
 * @param quien       Portador de la aguja (el que activa el poder)
 * @param tipo_herida Tipo de herida quitada
 */
void otorgar_bonificacion(object quien, string tipo_herida)
{
    mapping caracs = (["hemorragia":({"con", 1, 180}),
              "hemorragia_degollar":({"con", 1, 180}),
                     "evisceracion":({"des", 1, 180}),
                        "multiples":({"int", 1, 180}),
                        "proyectil":({"fue", 1, 180}),
                        "sangrante":({"regeneracion_pvs", 50, 200})]);
    mixed   data   = caracs[tipo_herida] || ({"regeneracion_pvs", 25, 180});

    // data[2] += roll(quien->dame_carac("sab"), 5);
    data[2] += roll(1, 18) * 5;

    if (quien->dame_efecto("aguja hemalurgica")) {
        quien->quitar_efecto("aguja hemalurgica");
    }

    quien->ajustar_carac_tmp(data[0], data[1]);

    // clang-format off
    quien->nuevo_efecto(
        "aguja hemalurgica",
        data[2],
        (: quitar_bonificacion, quien, data :)
    );
    // clang-format on
}
/**
 * Función llamada al limpiar al jugador de sus efectos
 */
void quitar_bonificacion(object quien, mixed data)
{
    if (!quien) {
        return;
    }

    quien->ajustar_carac_tmp(data[0], -data[1]);
    tell_object(quien, "Los efectos de la hemalurgia desaparecen.\n");
}
/**
 * Función que gestiona la activación del comando
 *
 * @params ppoder Palabra de poder usada
 * @param  tx     Texto usado para activar el comando
 * @return        1 en caso de éxito, 0 en caso contrario
 */
int catalizar(string ppoder, string tx)
{
    if (ppoder != dame_palabra_poder()) {
        return notify_fail(sprintf(
            "%s no parece responder a esa palabra de poder. Tendrás que " +
                "descubrir cuál es para poder usar este poder.\n",
            capitalize(dame_tu()) + " " + query_short()));
    }

    if (!chequeo_activacion_poder(TP, TP, MOLDEAR_HERIDA)) {
        return 0;
    }

    if (!sizeof(TP->dame_incapacidades("heridas"))) {
        return notify_fail(
            "No estás afectad" + TP->dame_vocal() + " por ninguna herida.\n");
    }

    if (tx != "cualquiera" && !TP->dame_incapacidad(tx, "heridas")) {
        return notify_fail(
            "No estás afectad" + TP->dame_vocal() + " por ninguna herida " +
            tx + ".\n");
    } else {
        tx = element_of(TP->dame_incapacidades("heridas"));
    }

    mostrar_mensajes_poderes("activacion", TP, 0, MOLDEAR_HERIDA);
    poner_bloqueos(TP, 0, MOLDEAR_HERIDA);

    // clang-format off
    call_out(
        "finalizacion_poder",
        2 + (TP->query_hb_counter() % 2) * 2,
        TP,
        TP,
        MOLDEAR_HERIDA,
        (: do_catalizar :),
        0,
        tx
    );
    // clang-format on

    return 1;
}
void init()
{
    ::init();

    // clang-format off
    anyadir_comando_equipado(
        "hemalurgia",
        "<texto'Palabra de poder'>: " +
        "<texto'Nombre de una herida o \"cualquiera\"'>",
        (: catalizar($1, $2) :),
        descripcion_juzgar(0)
    );
    // clang-format on
}
/*******************************************************************************
 * Sección: Mensajes
 ******************************************************************************/
varargs mixed dame_mensajes_activacion(
    object quien, object victima, string poder, mixed params...)
{
    string *sms  = allocate(_SMS_TAM_TOTAL);
    sms[_SMS_AT] = sprintf(
        "%s comienza a vibrar en tu oreja acompañada de un zumbido " +
            "in crescendo que pareces oír en la lejanía.",
        capitalize(dame_tu()) + " " + query_short());
    sms[_SMS_SALA] = sprintf(
        "%s de %s comienza a vibrar en su oreja.",
        capitalize(dame_articulo()),
        query_short());

    return sms;
}
varargs mixed dame_mensajes_finalizacion(
    object quien, object victima, string poder, string tipo_herida)
{
    string *sms  = allocate(_SMS_TAM_TOTAL);
    sms[_SMS_AT] = sprintf(
        "El zumbido cesa y un extraño malestar crece en tu corazón. Notas " +
            "como éste comienza " +
            "a extenderse hacia la herida %s que te aqueja. Estupefact%s, " +
            "compruebas como la carne de tu cuerpo comienza a doblarse como " +
            "si fuese arcilla, cerrando tu herida para no dejar ni rastro de " +
            "cicatriz y convertir la quemazón de su dolor en una sensación " +
            "vigorizante.",
        tipo_herida,
        quien->dame_vocal());
    sms[_SMS_SALA] = sprintf(
        "%s contempla con estupefacción como la herida %s que %s aquejaba " +
            "se cierra cuando la carne que le rodea comienza a plegarse como "
            "si " +
            "fuese arcilla. Su mirada de sorpresa pronto se convierte en una "
            "de " +
            "vigor.",
        quien->query_cap_name(),
        tipo_herida,
        quien->dame_le());

    __debug_x(sprintf("%s::dame_mensajes_finalizacion -> %O", __FILE__, sms));
    return sms;
}
