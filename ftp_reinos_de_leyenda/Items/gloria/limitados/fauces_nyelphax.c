// Satyr 16/05/2018 01:02
// Satyr 30/05/2018 01:07 -- Reduzco duración a PNJs
// Satyr 24/07/2020 02:50  -- Conjunto
#include <pk.h>
#include <baseobs_path.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

#define DURACION 90
#define BLOQUEO 600

void setup()
{
    fijar_armadura_base("amuleto");
    set_name("fauces");
    set_short(
        "%^BOLD%^BLACK%^F%^RED%^auces de %^BLACK%^N%^RED%^yel'phax%^RESET%^");
    set_main_plural(
        "%^BOLD%^BLACK%^F%^RED%^auces de %^BLACK%^N%^RED%^yel'phax%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Nyel'Phax es un nombre que se habla entre cuchicheos. Un nombre que "
        "va unido a promesas de más poder susurradas en oídos de los que ya "
        "son "
        "poderosos. Un nombre cuya influencia se propaga como el fuego en un "
        "polvorín, solo que en este caso es una llama silenciosa "
        "y que solo brilla a ojos de aquellos que saben buscarla.\n\n"

        "  Se dice que esta entidad es un dios que ha permanecido en "
        "letargo "
        "durante eones y que su poder es tal que cuando se mueve en sueños "
        "altera el curso del mundo. No hay pruebas de que semejante criatura "
        "exista, ni tampoco figura su nombre en ninguno de las crónicas "
        "élficas que estudian el origen de Eirea.\n\n"

        "  Sin embargo, esto no ha impedido que se haya creado un culto a su "
        "alrededor. Un culto que honra símbolos como éste que ahora tienes "
        "ante "
        "ti. Un culto peligroso, y que ha encontrado en su nueva devoción una "
        "nueva forma de vivir.\n\n"

        "  Centrándonos en el símbolo: rezuma poder. Solo con tocarlo sientes "
        "como una oleada de energía recorre tus brazos y te pone los pelos de "
        "punta. También te parece oír murmullos en la parte de atrás de tu "
        "cabeza, pero jurarías que no están ahí y éstos realmente son causados "
        "por la terrible reputación del culto que honra a este... ¿dios?\n\n"

        "  Está hecho de un cristal opaco cuyo color recuerda al bronce. "
        "Gira en torno a un azabache que ha sido cortado en forma de un "
        "dodecaedro perfecto de la que cuelga una tira de cuero te permite "
        "colgarlo al cuello"
        ", permitiendo así lucir las blasfemas inscripciones que rodean "
        "al cristal.\n");

    add_static_property("divino", -10);
    fijar_encantamiento(20);
    ajustar_BE(-1);
    fijar_BO(7);
    fijar_BP(7);

    fijar_material(5);
    add_static_property("no_tipo", "cazador");

    setup_limitado_gloria();
    fijar_conjuntos(({BCONJUNTOS + "sacramento_de_nyelphax.c"}));
}
string descripcion_juzgar(object pj)
{
    return "Las fauces de Nyel'phax pueden abrirse bajo la voluntad de su "
           "portador para pronunciar una blasfema maldición que afectará "
           "a un único objetivo, causando que no pueda aplicar ningún "
           "tipo de efecto a sus ataques durante " +
           pretty_time(90) +
           ". El objetivo tendrá derecho a una tirada de salvación contra "
           "'artefacto' (penalizada por sabiduria del lanzador y diferencia "
           "de niveles) para librarse del efecto. La duración se verá reducida "
           "contra PNJs.\n";
}
int maldecir(object b)
{
    int ts;

    if (b->dame_bloqueo_combate(TO, "sufrido")) {
        return notify_fail(
            b->query_cap_name() +
            " ya ha sufrido recientemente "
            "los efectos de la maldición de Nyel'phax.\n");
    }

    if (dame_bloqueo_combate(TO)) {
        return notify_fail(
            "El poder mágico de " + TU_S(TO) + " aún no se ha recuperado.\n");
    }

    if (TP->dame_bloqueo_combate(TO)) {
        return notify_fail(
            "Hace poco que has liberado otra blasfemia y es demasiado "
            "pronto para que ésta vuelva a darse de nuevo.\n");
    }

    if (TP->query_hidden()) {
        return notify_fail(
            "Verás... Nyel'phax es una entidad caprichosa y ególatra a la "
            "que le gusta que sus seguidores sean vistos cuando pronuncian "
            "su nombre, por lo que tendrás que descubrirte para "
            "activar su blasfemia.\n");
    }

    if (environment(TP)->query_property("silencio")) {
        return notify_fail(
            "El silencio del entorno en el que te encuentras no te permitirá "
            "utilizar "
            "la blasfemia de Nyel'phax.\n");
    }

    TP->nuevo_bloqueo_combate(TO, BLOQUEO);
    nuevo_bloqueo_combate(TO, BLOQUEO);
    b->nuevo_bloqueo_combate(TO, BLOQUEO, 0, 0, "sufrido");
    ts = b->dame_nivel_ponderado() - TP->dame_nivel_ponderado() -
         TP->dame_carac("sab");

    tell_room(
        environment(TP),
        TP->query_cap_name() + " abre un millar de "
                               "bocas y blasfema: "
                               "¡¡%^BOLD%^RED%^Ph'nglui "
                               "mglw'nafh%^RESET%^!!\n",
        TP);
    tell_object(
        TP,
        "Abres un millar de bocas y blasfemas: ¡¡%^BOLD%^RED%^Ph'nglui " +
            "mglw'nafh%^RESET%^!!\n");

    TP->attack_ob(b);

    if (b->dame_ts("artefacto", ts, "divino", "blasfemia de nyel'phax")) {
        tell_object(
            TP,
            "%^MAGENTA%^#%^RESET%^ ¡Las fauces de Nyel'phax "
            "se abren para pronunciar su blasfemia, pero " +
                b->query_cap_name() +
                " es capaz de resistirse a sus efectos!\n");
        tell_object(
            b,
            "%^MAGENTA%^*%^RESET%^ ¡" + TP->query_cap_name() +
                " "
                "te mira mientras " +
                SU_S(TO) +
                " brilla con malicia, pero "
                "logras resistirte a sus efectos perniciosos!\n",
            "",
            ({TP}),
            b);
        return 1;
    }

    tell_object(
        TP,
        "%^GREEN%^#%^RESET%^ ¡Las fauces de Nyel'phax se abren "
        "y pronuncian su blasfemia a través de ti, marcando a " +
            b->query_cap_name() + " con su perniciosa afflicción!\n");

    tell_accion(
        b,
        "%^RED%^*%^RESET%^ ¡El rostro de " + TP->query_cap_name() +
            " "
            "se desfigura cuando un millar de bocas se abren en su cara para "
            "pronunciar una perniciosa blasfemia que te marca!\n",
        "",
        ({TP}),
        TP);

    if (b->query_player()) {
        b->nuevo_modificador_combate(
            "sin_efectos", DURACION, (["no_efectos":3]), 1, base_name());
    } else {
        b->nuevo_modificador_combate(
            "sin_efectos", DURACION / 3, (["no_efectos":3]), 1, base_name());
    }

    b->add_extra_look(TO);

    call_out("fin_blasfemia", DURACION, b);
    return 1;
}
void fin_blasfemia(object b)
{
    if (b) {
        tell_object(
            b, "Los efectos de la blasfemia de Nyelphax desaparecen.\n");
    }
}
string extra_look(object pj)
{
    if (pj == TP) {
        return "Estás marcado por la blasfemia de Nyelphax.\n";
    }

    return "Está afectad" + pj->dame_vocal() +
           " por la blasfemia de Nyelphax.\n";
}
void init()
{
    ::init();

    anyadir_comando_equipado(
        "blasfemar",
        "<objeto:living:aqui:primero'Objetivo a maldecir'>",
        (
            : maldecir:),
        descripcion_juzgar(0));
}
