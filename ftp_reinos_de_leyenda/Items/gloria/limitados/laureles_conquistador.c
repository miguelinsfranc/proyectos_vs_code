// Satyr 14/05/2018
// Eckol Jun18
#include <spells.h>
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

#define BLOQUEO 600
#define NUM_SOLDADOS 2
#define SOLDADO GLORIA_LIMITADOS+"laureles_ob"

nosave private object *dominados = ({});

object *dame_dominados()
{
	if(!dominados)
		dominados = ({});

	dominados = dominados - ({0});

    return dominados;
}

void setup()
{
    fijar_armadura_base("capucha");
    set_name("laureles");
    set_short("%^GREEN%^BOLD%^L%^NOBOLD%^aureles del "
              "%^BOLD%^C%^NOBOLD%^onquistador%^RESET%^");
    set_main_plural("pares de %^GREEN%^BOLD%^L%^NOBOLD%^aureles del "
                    "%^BOLD%^C%^NOBOLD%^onquistador%^RESET%^");
    fijar_genero(-1);

    // fixme
    set_long(
        "La historia es caprichosa y olvidadiza. La vida de aquellos que han"
        " sufrido barbaridades tienden a ser olvidadas en libros repletos "
        "de polvo que se acumulan en las estanterías recargadas de algún "
        "archivo. Sin embargo, a los conquistadores de nuevas tierras, a "
        "aquellos que cometen genocidios para tomar un mundo como suyo "
        "se les recuerda como héroes, campeones o conquistadores.\n\n"

        "  La tradición de comendar a estas figuras históricas con coronas "
        "de laurel es vieja, sin embargo, sus orígenes son difíciles de "
        "concretar -quizás porque han nacido en otra parte del multiverso-, "
        "pero es innegable que en todas las cultoras son vistas como un "
        "gran signo de estatus.\n\n"

        "  Esta, en concreto, no sirve para conmemorar el descubrimiento de un "
        "nuevo mundo, si no para celebrar a un nuevo campeón de gloria. Es "
        "una corona hecha de laurel -lo aclaramos por si tenías alguna duda "
        "al respecto- entretejida con finas hebras de oro y terciopelo. Un "
        "pequeño conjunto de gemas distribuidas estrategicamente sirven para "
        "evitar que el tiempo haga estragos con las hojas. La historia se "
        "encargará de que su portador tampoco los sufra.\n");

    fijar_encantamiento(20);
    fijar_BO(5);
    fijar_BP(5);
    add_static_property("car", 1);
    add_static_property("ts-conjuro", -10);

    setup_limitado_gloria();
}
string descripcion_juzgar(object b)
{
    return "Esta corona de laurel, llevada por reyes y emperadores de tiempos "
           "perdidos, permite invocar dos soldados, creados a imagen y semejanza "
		   "de su portador, para protegerle y ayudarle en el combate.\n";
}

void objeto_desequipado()
{
    if (!sizeof(dame_dominados())) {
        return;
    }

	foreach(object dominado in dame_dominados())
        dominado->orden_marcharse(environment());

    dominados = ({});
}

int dame_duracion_dominacion(object b, object dominado)
{
    return 120 + b->dame_carac("car") * 8;
}

int llamar_soldados(){
	//Ya tenemos
	if(sizeof(dame_dominados())){
		return notify_fail("No puedes llamar a más soldados hasta que se vayan los que ya tienes.\n");
	}

	 if (dame_bloqueo_combate(TO)) {
        return notify_fail(
            "La magia que impregnaba " + TU_S(TO) +
            " ha desaparecido y éstos son mucho menos imponentes sin ella.\n");
    }

    if (TP->dame_bloqueo_combate(TO)) {
        return notify_fail(
            "Has usado recientemente " + TU_S(TO) +
            " y no podrás invocar "
            "de nuevo su autoridad hasta que haya pasado un tiempo.\n");
    }

	if (TP->query_property("criatura_invocada")) {
        return notify_fail(
            "Las otras invocaciones bajo tu control te impiden ejercer "
            "la autoridad de " +
            TU_S(TO) + ".\n");
    }

	TP->accion_violenta();
	tell_object(TP,sprintf("Acaricias %s mientras te concentras en la imagen de los dos soldados que quieres traer a tu presencia.\n",TU_S(TO)));
	tell_accion(ENV(TP),sprintf("%s se concentra profundamente mientras acaricia %s.\n",TP->query_cap_name(),SU_S(TO)),"",({TP}));

	for(int i=0;i<NUM_SOLDADOS;i++){
		object ob = clone_object(SOLDADO);

		if (!ob)
			return notify_fail("Ha ocurrido un error al clonar a tus protectores. Avisa a un inmortal.\n");

	    ob->configurar_invocacion(TP); //Fija invocador, etc.
		ob->move(ENV(TP));

		tell_room(ENV(TP),ob->query_short()+" aparece de la nada.\n");

	    ob->nuevo_efecto(
			"dominar",
			dame_duracion_dominacion(TP, ob),
			( : TO->objeto_desequipado() :));

		dominados += ({ob});
	}	

	nuevo_bloqueo_combate(TO, BLOQUEO);
	TP->nuevo_bloqueo_combate(TO, BLOQUEO);

	return 1;
}

void init()
{
    ::init();
    anyadir_comando_equipado(
        "llamar",
        "a los soldados",
        (
            : llamar_soldados:),
       "Llama a dos soldados, creados a tu imagen y semejanza.");

    anyadir_comando_equipado(
        "convocar",
        "a los soldados",
        (
            : llamar_soldados:),
       "Llama a dos soldados, creados a tu imagen y semejanza.");

}

