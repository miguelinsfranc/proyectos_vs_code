// Satyr 21.05.2017
// Implementando una idea de perestur.
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/escudo";

void setup()
{
    fijar_escudo_base("escudo");
    set_short(
        "%^BOLD%^RED%^E%^CYAN%^scudo de la "
        "%^RED%^S%^CYAN%^alvaguarda%^RESET%^");
    set_main_plural(
        "%^BOLD%^RED%^E%^CYAN%^scudos de la "
        "%^RED%^S%^CYAN%^alvaguarda%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Este pequeño escudo de hierro se encuentra recubierto por una "
        "fina capa de piel de hidra que parece otorgarle un color "
        "turquesa algo desgastado. Su estilo \"adarga\" le da una forma "
        "ovalada, algo similar a la de un corazón. Dentro, dos tiras de "
        "cuero ennegrecido están colocadas perfectamente para adaptarse al "
        "antebrazo de su portador, haciéndolo perfecto para detener "
        "ataques a corta distancia, ya que este es capaz de cubrir sólo "
        "una pequeña parte de su brazo. En la parte central del escudo "
        "puedes observar un mal pintado corazón que destaca por su tosco "
        "aspecto.\n");

    fijar_encantamiento(17);
    ajustar_BE(16);
    ajustar_BO(-6);
    add_static_property("poder_curacion", 50);
    add_static_property(
        "messon",
        "Al ajustarte las tiras de cuero al brazo, "
        "observas como el corazón parece llenarse de "
        "vida y latir de forma exánime\n.");
    fijar_BO(5);

    setup_limitado_gloria();
}
