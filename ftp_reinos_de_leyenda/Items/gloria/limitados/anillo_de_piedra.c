// Satyr 22.08.2016
// Eckol  24Oct17: bajo bloqueo vueltaacasa de 24 a 12 horas.
// Eckol 05Nov17: Cambio el objeto_desequipado para evitar problemas con el polimorfismo.
// Sierephad Jul 2k20	--	Revisando la formula del descifrar
#include <pk.h>
#include <fronteras.h>

#define BLOQUEO                 3600 * 2
#define BLOQUEO_VUELTAACASA     43200
#define DURACION                43200
//#define DURACION 10

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

int terminar_djinn(object pj, object djinn);
//int no_catalogar_armeria() { return 1; }

// Llamado cuando el djinn usa el milagro "vueltaacasa"
void poner_bloqueo_vueltaacasa(object b) {
    // Tened en cuenta que se sumará al bloqueo restante...
    b->nuevo_bloqueo_combate(TO, BLOQUEO_VUELTAACASA);
}
string dame_palabra_poder() {
    string tx;
    object b = TP ? TP : TO;
    
    if ( tx = query_property("palabra_poder") )
        return tx; 

    tx = element_of(({
        b->dame_senyor(),
        "caballer" + b->dame_vocal(),
        "hidalg" + b->dame_vocal(),
        "soberan" + b->dame_vocal(),
        b->dame_genero() == 2 ? "patrona" : "patrón",
        "amig"+b->dame_vocal(),
        "camarad"+b->dame_vocal(),
        "leal"
    }));
    
    add_property("palabra_poder", tx);
    return tx;
}
mixed descifrar_inscripcion(string objeto,object quien)
{
    if ( objeto=="anillo" || objeto=="piedra" || regexp(objeto, "inscripci(o|ó)n(es)?") ) //Eckol: como esta el descifrar, solo con inscripcion no se puede.
        return ({
            "El viejo Djinn no es mas que tu propia razón y con la palabra " + dame_palabra_poder() + " "
                "te servirá hasta la muerte.",
            5,
            "dendrita"
        });
}
string descripcion_juzgar(object juzgador) {
    return "Un Djinn obedecerá a todo aquel que lo invoque mientras porte este anillo. "
        "La obediencia de la criatura será absoluta durante " + pretty_time(DURACION) + ", "
        "período tras el cual desaparecerá. El Djinn seguirá a su maestro y podrá "
        "otorgarle uno de varios deseos menores cada invocación, a demás de cumplir "
        "con el resto de tareas mundanas que se le puedan ordenar.\n"
    ;
}
void setup()
{
    fijar_armadura_base("anillo");
    set_name("anillo");
    
    set_short("Anillo de %^BOLD%^Piedra%^RESET%^");
    set_main_plural("Anillos de %^BOLD%^Piedra%^RESET%^");
    generar_alias_y_plurales();
    
    set_long(
        "Un anillo de piedra azul cristalina que carece de decoración más allá de un intrincado "
            "grabado que recorre el interior de la pieza. Perdido desde la 2ª Era, este anillo "
            "poseía el poder para invocar a servidores extraplanares.\n"
    );

    fijar_material(6);    
    fijar_encantamiento(20);
    add_static_property("electrico", 10);
    
    set_read_mess(
        "La viejchu vljinn du es naeás laesrae aresra aesrachuaeia sraazón. vli "
            "sechusra y él aree sesravisraá arehasarea laey naesraesraaree."
    );

    fijar_material(9);
    fijar_encantamiento(20);
//    fijar_coste_gloria(300);
//    fijar_valor(10000);
    setup_limitado_gloria();
}
int invocar_djinn() {
    object djinn;
    
    if ( dame_bloqueo_combate(TO) )
        return notify_fail(capitalize(dame_tu()) + " " + query_short() + " todavía no se ha recuperado "
            "desde su último uso.\n"
        );
        
    if ( TP->dame_bloqueo_combate(TO) )
        return notify_fail("Has usado el poder de " + dame_numeral() + " " + query_short() + " hace muy "
            "poco.\n"
        );
    
    if ( ! environment(TP) )
        return notify_fail("No puedes usar el poder de " + dame_tu() + " " + query_short() + " en la nada.\n");
    
    if ( ! djinn = clone_object(base_name() + "_ob.c") )
        return notify_fail("Un error impide traer a tu sirviente al plano material.\n");
    
    // Mensajes
    tell_object(TP, "¡Al usar la palabra de poder " + dame_tu() + " " + query_short() + " se electrifica "
        "y abre un portal que atraviesa " + djinn->query_short() + "!\n"
    );
    tell_accion(environment(TP), "¡" + TP->query_cap_name() + " activa " + dame_su() +" " + query_short() + " "
        "y abre un portal que se cierra cuando " + djinn->query_short() + " lo atraviesa!\n",
        "Escuchas una serie de rayos y relámpagos seguido de un *ZZZZZIIIIIP*.\n",
        ({TP}),
        TP
    );
    TP->accion_violenta();
    
    // Llegada del PNJ
    djinn->fijar_anillo(TO);
    djinn->fijar_invocador(TP);
    djinn->move(environment(TP));
    djinn->do_emote("hace una solemne reverencia.");
    djinn->do_say("A tu servicio, mi am" + TP->dame_vocal() + ".", 0);
    
    // Bloqueos
    nuevo_bloqueo_combate(TO, BLOQUEO);
    TP->nuevo_bloqueo_combate(TO, BLOQUEO);
   
    // Para controlar la duración. Se puede disipar.
    TP->nuevo_efecto("djinn", DURACION, (: terminar_djinn(TP, $(djinn)) :), 0, djinn);

    return 1;
}
int terminar_djinn(object pj, object djinn) {
    if ( ! djinn )
        return 0;
    
    djinn->do_say("Me temo que mi poder en este plano se agota. He de volver al anillo.", 0);
    djinn->orden_marcharse(pj);
    return 1;
}

void init() {
    ::init();
    
    anyadir_comando_equipado(
        no_catalogar_armeria() ? "señor" : dame_palabra_poder(),
        "",
        (: invocar_djinn :)
    );
    fijar_cmd_visibilidad(dame_palabra_poder(), "", 1);
}
void objeto_desequipado() {
    if ( environment() )
        call_out("completar_desequipar",0,environment());
        //environment()->quitar_efecto("djinn", 0);
}

//Chapuza para evitar que al polimorfarse se vaya.
void completar_desequipar(object portador){
    if(portador && !query_in_use())
        portador->quitar_efecto("djinn",0);
}
