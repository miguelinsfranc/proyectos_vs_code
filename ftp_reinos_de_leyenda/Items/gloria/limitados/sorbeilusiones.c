// Satyr 03/03/2020 12:22
// Eckol Mar21; implemento mejorar espejismo
#include <depuracion.h>
#include <combate.h>
#include <pk.h>

#define IMAGENES_MAXIMAS 6
#define BLOQUEO 90
#define DURACION 120
#define PODER_ROBAR "robar ilusiones"
#define PODER_LIBERAR "liberar ilusiones"
#define TIRADA ({1, 3, 1})

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";
inherit "/obj/magia/objeto_con_poderes";

/*******************************************************************************
 * SECCIÓN: Funciones habituales de objetos
 ******************************************************************************/
void setup()
{
    set_name("guante");
    fijar_armadura_base("guante");
    set_short("%^BLUE%^BOLD%^G%^RESET%^CYAN%^uante%^RESET%^ "
              "%^BLUE%^BOLD%^S%^RESET%^CYAN%^orbeilusiones%^RESET%^");
    set_main_plural("%^BLUE%^BOLD%^G%^RESET%^CYAN%^uantes%^RESET%^ "
                    "%^BLUE%^BOLD%^S%^RESET%^CYAN%^orbeilusiones%^RESET%^");
    generar_alias_y_plurales();
    set_long("Un guante de lino azulado cuya fina tela está surcada por una "
             "maraña de hilos de zafiro que  repican con cada "
             "movimiento. Las pequeñas joyas dibujan una letra \"O\" sobre la "
             "palma del guante y de su centro parece brotar una especie de "
             "niebla azulada cuyo origen no puedes explicar.\n");

    fijar_encantamiento(20);
    add_static_property("mental", 10);
    add_static_property("magico", 10);

    setup_limitado_gloria();

    fijar_bloqueo_poderes(BLOQUEO, PODER_ROBAR);
    fijar_bloqueo_poderes(6, PODER_LIBERAR);
    add_extra_look(TO);

    add_static_property("minimo_atributos", (["int":14, "sab":12]));
}
string descripcion_juzgar(object pj)
{
    return "El poder místico de " + dame_este() + " " + dame_armadura_base() +
           " " + dame_le() +
           " permite sorber imágenes espejo que afecten a un "
           "objetivo presente en la sala del lanzador y almacenarlas en las "
           "joyas "
           "que surcan la prenda. El objetivo tendrá derecho a una tirada de "
           "salvación contra 'artefacto'. El número de imágenes robadas será "
           "igual a " +
           TIRADA[0] + "D" + TIRADA[1] + "+" + TIRADA[2] +
           " y el guante no podrá almacenar nunca más de " + IMAGENES_MAXIMAS +
           ".\n\n En cualquier momento del portador del guante podrá liberar "
           "las imágenes, sea para ser afectado por un hechizo de 'espejismo' "
           "o para reforzar alguno que le afecte en ese momento.\n\n"
           " El poder de sorber imágenes es extremadamente sutil y puede "
           "ejecutarse desde las sombras sin ser descubierto.";
}
/*******************************************************************************
 * SECCIÓN: Auxiliares
 ******************************************************************************/
int dame_imagenes_capturadas()
{
    return query_property("imagenes_capturadas");
}
void fijar_imagenes_capturadas(int i)
{
    add_property("imagenes_capturadas", i);
}
int ajustar_imagenes_capturadas(int i)
{
    int _actuales = dame_imagenes_capturadas();

    if (_actuales >= IMAGENES_MAXIMAS) {
        return 0;
    }

    _actuales = min(({IMAGENES_MAXIMAS, _actuales + i}));
    fijar_imagenes_capturadas(_actuales);
    return 1;
}
string extra_look(object b)
{
    int _i;

    switch (_i = dame_imagenes_capturadas()) {
        case 1:
            return "Encierra el poder de una única imagen ilusoria.\n";

        case 0:
            return "";

        default:
            return "Encierra el poder de " + _i + " imágenes ilusorias.\n";
    }
}
/*******************************************************************************
 * SECCIÓN: Poderes
 ******************************************************************************/
int fin_robar(object quien, object victima, string poder)
{
    string err;
    int    _tirada;

    __debug_x(sprintf("fin_robar(%O, %O, %O)", quien, victima, poder));

    if (err = chequeo_previo_finalizar_poder(quien, victima, poder)) {
        tell_object(quien, err + "\n");
        return 0;
    }

    if (!victima->dame_espejismo()) {
        quien->accion_violenta();
        mostrar_mensajes_poderes("sin_espejismo", quien, victima, poder);
        return 0;
    }

    if (victima->dame_ts("artefacto", 0, 0, base_name(), quien)) {
        quien->accion_violenta();
        log_artefacto(sprintf(
            "%s resiste la TS de %s",
            victima->query_name(),
            quien->query_name()));
        mostrar_mensajes_poderes("ts_resistida", quien, victima, poder);
        return 0;
    }

    _tirada = roll(TIRADA[0], TIRADA[1]) + TIRADA[2];
    ajustar_imagenes_capturadas(_tirada);

    log_artefacto(sprintf(
        "%s roba %d imágenes de %s",
        victima->query_name(),
        _tirada,
        quien->query_name()));

    if (quien->query_hidden() > victima->dame_ver_realmente()) {
        mostrar_mensajes_poderes(
            "exito_robo_silencioso", quien, victima, poder, _tirada);
        victima->quitar_imagenes(_tirada, quien, 1);
    } else {
        mostrar_mensajes_poderes("exito_robo", quien, victima, poder, _tirada);
        victima->quitar_imagenes(_tirada, quien, 1);
        quien->attack_ob(victima);
    }

    return 1;
}
int robar_ilusiones(object objetivo)
{
    __debug_x(sprintf("robar_ilusiones(%O)", objetivo));

    if (!chequeo_activacion_poder(TP, objetivo, PODER_ROBAR)) {
        return 0;
    }

    if (!objetivo->dame_espejismo()) {
        return notify_fail(
            objetivo->query_cap_name() +
            " no está bajo los efectos de un hechizo de 'espejismo', por lo "
            "que no puedes absorberle sus ilusiones.\n");
    }

    mostrar_mensajes_poderes("activacion", TP, 0, PODER_ROBAR);
    poner_bloqueos(TP, 0, PODER_ROBAR);

    // clang-format off
    call_out(
        "finalizacion_poder",
        2 + (TP->query_hb_counter() % 2) * 2,
        TP,
        objetivo,
        PODER_ROBAR,
        (: fin_robar :),
        0
    );
    // clang-format on
    return 1;
}
string chequeo_puedo_formular()
{
    object ob = load_object("/hechizos/escuelas/ilusion/espejismo.c");
    string err;

    if (err = ob->chequeo_extra(TP, TP)) {
        return err;
    }

    return 0;
}
int liberar_ilusiones()
{
    int    i;
    string err;
    int restantes;
    object sh;

    if (!chequeo_activacion_poder(TP, TP, PODER_LIBERAR)) {
        return 0;
    }

    if (!i = dame_imagenes_capturadas()) {
        return notify_fail(sprintf(
            "%s no tiene suficiente poder para crear imágenes espejo.\n",
            capitalize(dame_tu()) + " " + query_short(),
            i));
    }

    if (!TP->dame_espejismo()) {
        if (err = chequeo_puedo_formular()) {
            return notify_fail(err);
        }
        sh = clone_object("/hechizos/shadows/espejismo_sh.c");
        sh->setup_sombra(TP, DURACION, i);
        mostrar_mensajes_poderes("liberar_imagenes", TP);
        restantes = 0;
    } else {
        int posibles = TP->dame_imagenes_iniciales() - TP->dame_espejismo();
        int a_sumar;
        if(posibles <= 0){
            return notify_fail("No puedes añadir más imágenes a tu espejismo.\n");
        }

        if(posibles <= i){
            a_sumar = posibles;
            restantes = i - posibles;
        }
        else{
            a_sumar = i;
            restantes = 0;
        }

        TP->sumar_imagenes(a_sumar, 1);
        mostrar_mensajes_poderes("fortalecer_imagenes", TP);
    }

    poner_bloqueos(TP, 0, PODER_LIBERAR);
    fijar_imagenes_capturadas(restantes);

    return 1;
}


void init()
{
    ::init();

    // clang-format off
    anyadir_comando_equipado(
        "sorber",
        "ilusiones de <objeto:living:aqui:primero'Objetivo'>",
        (: robar_ilusiones($1) :),
        "Intenta absorber parte de las imágenes ilusorias que afecten al objetivo "
        "para almacenarlas en " + query_short() +". El objetivo tendrá derecho "
        "a una tirada de salvación contra 'artefacto' para librarse de los "
        "efectos. Este efecto no descubrirá al lanzador si tiene éxito."
    );
    anyadir_comando_equipado(
        "liberar",
        "ilusiones",
        (: liberar_ilusiones() :),
        "Libera las ilusiones de " + query_short() + ", añadiéndolas a "
        "cualquier hechizo de 'espejismo' que tuvieras durante un tiempo máximo"
        " de " + pretty_time(DURACION) + ". Si no tuvieses un espejismo se "
        "creará uno sobre tí con ese número de imágenes."
    );
    // clang-format on
}

/*******************************************************************************
 * SECCIÓN: Mensajes
 ******************************************************************************/
string *dame_mensajes_sin_espejismo(object quien, object victima, string poder)
{
    return ({_C_FALLAR + "¡" + victima->query_cap_name() +
                 " ya no está afectado por el hechizo 'espejismo'!, ¡tu "
                 "intento de absorberle sus ilusiones fracasa!",
             _C_FALLAR + "¡" + quien->query_cap_name() +
                 " descarga una ráfaga "
                 "de magia que intenta arrebatarte algo que ya no tienes!",
             "¡" + quien->query_cap_name() +
                 " descarga una ráfaga "
                 "de magia contra " +
                 victima->query_cap_name() +
                 " que no parece "
                 "surtir ningún efecto!"});
}

string *
dame_mensajes_exito_robo(object quien, object victima, string poder, int img)
{
    return ({
        sprintf(
            "%s comienza a brillar intensamente mientras doblega %s "
            "de %s, quien no puede hacer nada cuando sus dobles ilusorios "
            "l%s abandonan sin miramientos.",
            capitalize(dame_tu()) + " " + query_short(),
            (img == 1 ? "una imagen" : img + " imágenes") + " espejo",
            victima->query_cap_name(),
            victima->dame_vocal()),
        sprintf(
            "%s abre la palma de su mano y %s comienza a brillar "
            "intensamente, haciendo que %s de %s se encoja%s de hombros antes "
            "de desaparecer y ser absorbidas por tan místico guante.",
            quien->query_cap_name(),
            SU_S(TO),
            img == 1 ? "una imagen espejo" : img + " imágenes espejo",
            victima->query_cap_name(),
            img == 1 ? "" : "n", ),
        sprintf(
            "%s abre la palma de su mano y %s comienza a brillar "
            "intensamente, haciendo que %s se encoja%s de hombros antes "
            "de desaparecer y ser absorbidas por la magia de tu rival.",
            quien->query_cap_name(),
            SU_S(TO),
            img == 1 ? "una imagen espejo" : img + " imágenes espejo",
            img == 1 ? "" : "n"),
    });
}

string dame_mensajes_exito_robo_silencioso(
    object quien, object victima, string poder, int img)
{
    return sprintf(
        "%s muy neci%s de %s ni siquiera se entera cuando el poder de %s le "
        "roba %s de sus imágenes.",
        capitalize(victima->dame_articulo()),
        victima->dame_vocal(),
        victima->query_cap_name(),
        TU_S(TO),
        img + "");
}
string *dame_mensajes_liberar_imagenes(
    object quien, object victima, string poder, int img)
{
    return ({sprintf(
                 "Luces y centellas brotan de %s cuando %s usa su poder "
                 "acumulado para rodearte de un puñado de imágenes espejo que "
                 "imitan todos y cada uno de tus movimientos.",
                 TU_S(TO),
                 dame_este(1)),
             0,
             sprintf(
                 "%s %s de %s emite luces y centellas antes de lanzar "
                 "una rociada de magia azul que rodea a su portador%s y la "
                 "rodea de un puñado de imágenes espejo que comienzan a imitar"
                 " todos y cada uno de sus movimientos.",
                 capitalize(dame_articulo()),
                 query_short(),
                 quien->query_cap_name(),
                 quien->dame_oa())});
}
string *dame_mensajes_fortalecer_imagenes(
    object quien, object victima, string poder, int img)
{
    return ({sprintf(
                 "Luces y centellas brotan de %s cuando %s usa su poder "
                 "acumulado para añadir un puñado de imágenes al espejismo "
                 "que ya te afectaba.",
                 TU_S(TO),
                 dame_este(1)),
             0,
             sprintf(
                 "%s %s de %s emite luces y centellas antes de lanzar "
                 "una rociada de magia azul que rodea a su portador%s y "
                 "fortalece el hechizo de espejismo que %s afectaba con un "
                 "nuevo puñado de imágenes ilusorias.",
                 capitalize(dame_articulo()),
                 query_short(),
                 quien->query_cap_name(),
                 quien->dame_oa(),
                 quien->dame_le())});
}
