// Satyr 25.11.2016
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("muñequera");
    
    set_short("%^ORANGE%^Muñequera de Servidumbre%^RESET%^");
    set_main_plural("%^ORANGE%^Muñequeras de Servidumbre%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        "Una muñequera tela raída cuyo color original ha desaparecido "
            "en favor de un tono grisáceo característico de la mugre. "
            "Tiene un tacto áspero que viene acompañado "
            "de una ligera jaqueca que desaparece al soltar la prenda, "
            "denotando que existen en su interior los "
            "restos languidecientes de un encantamiento mental.\n\n"
            
        "  Complementos similares a éste son usados por los "
            "esclavistas pertenecientes a razas con poderes mentales "
            "y sirven para domesticar y hacer mas serviles y eficientes "
            "aún a cada uno de los ejemplares con los que trabajan.\n\n"
    );
 
    fijar_encantamiento(20);
    add_static_property("habilidad", "trabajo duro");
    add_static_property("ts-mental", -5); // meramente simbólico
    
    fijar_valor(12000);
    fijar_coste_gloria(200);
}