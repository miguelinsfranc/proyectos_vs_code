// Satyr 21/10/2019 10:38
// Concepto de Sierephad
#include <pk.h>
#include <spells.h>
#include <combate.h>
#include <depuracion.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit GLORIA_LIMITADOS + "codigo_espejo_catalizador.c";
inherit "/obj/magia/reliquia";

/*******************************************************************************
 * SECCIÓN: Funciones habituales de objetos
 ******************************************************************************/
void setup()
{
    fijar_genero(2);
    set_name("reliquia");
    set_short("%^WHITE%^R%^BLACK%^BOLD%^eliquia de la "
              "%^WHITE%^C%^BLACK%^ulpabilidad%^RESET%^");
    set_main_plural("%^WHITE%^R%^BLACK%^BOLD%^eliquia de la "
                    "%^WHITE%^C%^BLACK%^ulpabilidad%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Una reliquia sagrada que se asemeja a la guarda de una lanza de "
        "caballería, pues está conformada de un ástil de madera que está "
        "recubierta con un cono de metal de contorno muy suavizado.\n\n"

        "  La madera añeja está agrietada y salpicada de la sangre que "
        "derramaron los antiguos portadores de la misma al arañarse con las "
        "muchas astillas que sobresalen de su superficie poco estilizada.\n\n"

        "  Su parte de metal, no obstante, está cuidada y pulida hasta el "
        "extremo, dejando en su parte central un curioso hueco en el que se "
        "aloja un extraño rubí que levita en su interior y gira constantemente "
        "sobre sí mismo.\n\n"

        "  Este objeto, cuyo origen está ya perdido en el tiempo, era usado por"
        " los caballeros de algún dios olvidado cuando necesitaban expiar "
        "faltas graves contra su dogma. Sus propiedades mágicas, aunque "
        "solían usarse para castigar a su portador, pueden usarse para "
        "canalizar resistencias elementales a su favor, si es que no te importa"
        " malusar la reliquia de un dios borrado de la memoria histórica del "
        "mundo.\n");
    fijar_material(2);
    // Atributos del objeto
    add_static_property("ts-conjuro", 8);
    add_static_property("ts-fuerza bruta", 8);
    fijar_encantamiento(20);

    // Inicialización de movidas
    preparar_codigo_espejo();
    inicializar_reliquia();
    setup_limitado_gloria();
}
void init()
{
    ::init();
    codigo_espejo_catalizador::init();
}
string *dame_lista_resistencias_validas()
{
    return (
        {"bien",
         "mal",
         "veneno",
         "enfermedad",
         "magico",
         "electrico",
         "agua",
         "fuego",
         "tierra"});
}
string *dame_mensajes_finalizacion(
    object quien, object victima, string poder, string elemento)
{
    return (
        {sprintf(
             "%sLa cúspide de %s gira sobre sí misma a una velocidad "
             "vertiginosa antes de "
             "descargar un haz de luz que relampaguea hasta golpear de lleno a "
             "%s, catalizando sus resistencias mágicas de '%s' con las tuyas.",
             _C_GOLPEAR,
             TU_S(TO),
             victima->query_cap_name(),
             elemento),
         sprintf(
             "%sLas piezas de %s de %s giran vertiginosamente sobre sí mismas "
             "antes de descargar un haz de luz relampagueante que te impacta "
             "de lleno, catalizando tus resistencias contra '%s'.",
             _C_GOLPEADO,
             dame_del(),
             query_short(),
             elemento),
         sprintf(
             "%sLas piezas de %s de %s giran vertiginosamente sobre sí mismas "
             "antes de descargar un haz de luz relampagueante que impacta "
             "de lleno contra %s, catalizando sus resistencias contra '%s'.",
             _C_GOLPEADO,
             dame_del(),
             query_short(),
             victima->query_cap_name(),
             elemento)});
}
