// Eckol Ene18

#include <pk.h>

inherit GLORIA_LIMITADOS+"base_limitados"; 
inherit "/obj/arma_de_mano.c";

void setup()
{
    set_name("arma");
    set_short("Armas de mano de %^YELLOW%^Rukan%^RESET%^");
    set_main_plural("pares de Armas de mano de %^YELLOW%^Rukan%^RESET%^");
    generar_alias_y_plurales();
    
    set_long(
        "Dos tiras de cuero trenzado, de aspecto seco, que se enrollan sobre las "
        "manos del portador y se atan con un pequeño cordel de cañamo. Centenares "
        "de pequeñas espinas vegetales se han clavado desde el interior hacia el "
        "exterior del cuero, de manera que la parte de la mano que golpea "
        "al adversario queda cubierta por las afiladas puntas. "
        "Al haber sido tratadas con la poderosa tóxina de las Bayas de Cyr, estas "
        "espinas tienen la capacidad de producir alucinaciones en el rival, cuando "
        "le golpean de forma crítica.\n"
    );
    
    fijar_genero(-2);
    ajustar_BO(10);
    add_static_property("bono_daño",5);
    nuevo_efecto_basico("alucinacion");
    fijar_material(3);
    fijar_vida_max(dame_vida_max()+1000);

    setup_limitado_gloria();
}
