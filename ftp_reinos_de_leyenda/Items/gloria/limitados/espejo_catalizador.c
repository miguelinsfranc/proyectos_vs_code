// Satyr 21/10/2019 10:38
// Concepto de Sierephad
#include <pk.h>
#include <spells.h>
#include <combate.h>
#include <depuracion.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit GLORIA_LIMITADOS + "codigo_espejo_catalizador.c";
inherit "/obj/magia/grimorio";

/*******************************************************************************
 * SECCIÓN: Funciones habituales de objetos
 ******************************************************************************/
void setup()
{
    set_name("catalizador");
    set_short("%^BOLD%^RED%^E%^RESET%^CYAN%^spejo "
              "%^BOLD%^CYAN%^C%^YELLOW%^atalizador%^RESET%^");
    set_main_plural("%^BOLD%^RED%^E%^RESET%^CYAN%^spejos "
                    "%^BOLD%^CYAN%^C%^YELLOW%^atalizadores%^RESET%^");
    generar_alias_y_plurales();
    set_long(
        "Un cetro de marfil coronado con estrechas piezas circulares "
        "concéntricas que reflejan toda la luz que llega a su superficie, "
        "fraccionando el reflejo de aquellos que mire a tan curiosa "
        "pieza.\n\n"

        "  Las piezas no parecen estar ancladas de ninguna forma al cuerpo del "
        "cetro y sencillamente levitan a escasos milímetros unas de otras, "
        "permitiéndolas girar sobre si mismas en todas direcciones, lo cual"
        " causa una hipnótica reacción cuando esta pieza se mueve bruscamente."
        "\n\n"

        "  Semejante artefacto en su interior un extraño poder capaz de "
        "manipular"
        " la propia esencia del éter de una forma que ni los más doctos arcanos"
        " serían capaces de reproducir. El precio a pagar por poseer tan "
        "valioso artefacto es el de desconocer los detalles del enigmático "
        "origen de este objeto mágico.\n");
    fijar_material(6);

    // Atributos del objeto
    add_static_property("ts-paralizacion", 8);
    add_static_property("ts-artefacto", 8);
    fijar_encantamiento(20);

    // Inicialización de movidas
    preparar_codigo_espejo();
    inicializar_grimorio();
    setup_limitado_gloria();
}
void init()
{
    ::init();
    codigo_espejo_catalizador::init();
}
