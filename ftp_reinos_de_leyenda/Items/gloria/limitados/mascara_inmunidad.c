// Satyr 21/08/2018 01:25
#include <spells.h>
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("yelmo");

    set_name("máscara");
    set_short(
        "%^MAGENTA%^M%^ORANGE%^áscara de la "
        "%^MAGENTA%^I%^ORANGE%^nmunidad%^RESET%^");
    set_main_plural(
        "%^MAGENTA%^M%^ORANGE%^áscaras de la "
        "%^MAGENTA%^I%^ORANGE%^nmunidad%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Una fina máscara de metal que oculta completamente el rostro de "
        "su portador tras el semblante ceñudo de una reina que ha sido olvidada"
        " por la historia, pero cuyo recuerdo sobrevive en el relieve de esta "
        "pieza. "
        "Antigüedades parecidas a esta se encuentran en los ajuares funerarios "
        "de "
        "la nobleza y su utilidad se reduce a la de hacer las veces de "
        "símbolo ceremonial. Esta, sin "
        "embargo, era utilizada en vida, tiempo ha, para ocultar los estragos "
        "que la lepra causaba en su poderosa dueña, a la par que para intentar "
        "retrasar -de forma innatural- el irremediable destino que "
        "pendía sobre la Señoría que la portaba.\n");

    // Bonos
    fijar_encantamiento(20);

    foreach (string tipo in "/table/tiradas_de_salvacion.c"->dame_tipos_ts(1)) {
        add_static_property(tipo, 7);
    }

    foreach (string tipo in RESISTENCIAS) {
        add_static_property(tipo, 5);
    }

    add_static_property("con", -1);
    // Fin bonos

    setup_limitado_gloria();
}
