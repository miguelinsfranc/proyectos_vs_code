// Satyr 21.05.2017
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("cinturon");

    set_short("%^Cadenas de %^RED%^P%^RESET%^hebek%^RESET%^");
    set_main_plural("%^Cadenas de %^RED%^P%^RESET%^hebek%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Una cadena cerrada hecha de gruesos y pesados eslabones de piedra rojiza que han "
            "sido grabadas, toscamente, con runas de poder que brillan con cada palpitación "
            "de la energía contenida en el interior del objeto.\n\n"

        "  Fueron usadas en la 2ª Era para encerrar a Phebek, un Demi-Lich que había conseguido "
            "encerrar su filacteria en una dimensión inaccesible, convirtiéndolo en un ser "
            "eterno e inmortal.\n\n"

        "  Fue derrotado incontables veces, pero su recalcitrante voluntad le permitía volver "
            "de la tumba una y otra vez para exterminar a aquellos que le habían destruido "
            "en el pasado.\n\n"

        "  La forja rúnica creo entonces estas cadenas en aras de destruir al peligroso seguidor "
            "de Astaroth. Fueron diseñadas exclusivamente para anular completamente las energías mágicas "
            "de su poseedor y ser completamente inmunes a las inclemencias del tiempo o los "
            "intentos de escapismo más singulares.\n\n"

        "  Cuando Phebek se enfrentó de nuevo a aquellos que se le oponían, lo inmovilizaron con "
            "estas cadenas y lo dejaron hundirse en el fondo del océano, haciendo que su historia "
            "terminase de facto, pues el peso de las cadenas lo mantuvo inmóvil en el fondo del "
            "mar y su poderosa anti-magia evitó que usase sus poderes para escaparse.\n\n"

        "  El mero hecho de que tengas estas cadenas significa que alguien o algo liberó a Phebek "
            "de su prisión submarina...\n\n"
    );

    //add_static_property("nocast"         ,  1);
    add_static_property("ts-conjuro"     , 20);
    add_static_property("ts-aliento"     , 15);
    add_static_property("ts-mental"      , 15);
    add_static_property("ts-artefacto"   , 15);
    add_static_property("ts-paralizacion", 15);

    add_static_property("magico"         , 20);
    add_static_property("mental"         , 10);
    fijar_BO(5);
    
    fijar_tamanyo_armadura(0);
    fijar_material(6);
    fijar_peso(10000);

//    fijar_valor(147000);
//    fijar_coste_gloria(300);
    setup_limitado_gloria();
}
