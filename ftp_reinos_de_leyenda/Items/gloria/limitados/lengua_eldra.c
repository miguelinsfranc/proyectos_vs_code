// Satyr 25.11.2016
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("pendiente");
    
    set_short("%^MAGENTA%^L%^BOLD%^engua de %^NOBOLD%^E%^BOLD%^ldra%^RESET%^");
    set_main_plural("%^MAGENTA%^L%^BOLD%^engua de %^NOBOLD%^E%^BOLD%^ldra%^RESET%^");
    generar_alias_y_plurales();
    
    set_long(
        "Eldra era una magnífica cazadora Halfling que perdió a sus padres en una "
            "triste pero nada original historia que involucraba a un grupo de orcos "
            "que se aventuró hasta su pacífico poblado.\n\n"

        "  Semejante evento la marcó desde joven, haciendo que se decidiese a usar "
            "todo su tiempo, recursos y "   
            "energía para mejorar en el arte del asesinato. Muchos halflings "
            "narran ebrios en sus conversaciones de sobremesa que, en más de una ocasión, "
            "vieron a nuestra protagonista saltarse el desayuno para poder entrenar "
            "más. Tal anécdota es recibida con desdén e incredulidad por "
            "sus oyentes, pues que un habitante de eloras deseche una comida es... "
            "impensable.\n\n"
            
        "  Pronto Eldra se convirtió en una asesina y los cadáveres de los orcos "
            "empezaron a aparecer apilados alrededor de todas las cercanías de Golthur "
            "Orod. Su maestría infundió temor en los orcos y la fémina se volvió vanidosa "
            "y fanfarrona, efectuando intrincados bailes antes de matar a sus víctimas.\n\n"
            
        "  Los ataques de la imparable cazadora terminaron cuando los orcos descubrieron " 
            "esta debilidad. Aunque se movía ligera y silenciosa como un gato a punto "
            "de robar una sardina, era incapaz de callarse si ponían en duda sus "
            "aptitudes. Los pelotones orcos pronto empezaron a patrullar mientras "
            "entonaban cánticos que insultaban y menospreciaban a su némesis halfling.\n\n"
            
        "  Eldra no podía guardar silencio ante tan alevósicas acusaciones, ¡claro que "
            "no!, siempre que encontraba una de estas patrullas, "
            "abandonaba su posición para gritar y perjurar, insultando a los que "
            "cantaban a gritos lo mal que se le daba matar. Esta actitud temeraria pronto terminó "
            "por llevarla a una muerte en la que sus últimas palabras fueron: \"¡eso lo "
            "serás tú!\".\n\n"
            
        "  Se dice que el orco encargado de su final desobedeció una orden directa "
            "de su superior y se comió el cadáver de Eldra. Su sargento, tras matar "
            "a su subordinado, exploró sus tripas y encontró esta lengua que ahora "
            "tienes ante ti, curada, atravesada por un burdo cordel y tan seca, "
            "arrugada y dura como la corteza de un árbol. Aparentemente inútil, "
            "este objeto ahora encierra un poder mágico innato, además "
            "de poseer un gran valor sentimental para los veteranos orcos "
            "que sufrieron con los ataques de la despiadada asesina rosa.\n"
    );
 
    fijar_encantamiento(20);
    
    fijar_BO(5);
    add_static_property("daño"           , 10);
    add_static_property("armadura-magica", -5);

    fijar_material(7); // Hueso, pero no es hueso xD
    fijar_encantamiento(20);
//    fijar_valor(7000);
//    fijar_coste_gloria(300);
    setup_limitado_gloria();
	fijar_tamanyo_armadura(0);
}
