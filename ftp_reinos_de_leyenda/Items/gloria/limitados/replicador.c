//Eckol Oct17, sobre una idea de Sheerinive
//Eckol 06.11.17 -- Material.

#include <pk.h>

#define BLOQUEO 3600
#define ESCUELA 0 //Afecta a la duración y a la vida (nivel).

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/objeto_general.c";
inherit "/obj/magia/objeto_formulador.c";

void setup(){
	set_name("replicador");
	set_short("%^BOLD%^WHITE%^ReplicadorHolográficoGnómicoMecánicoLumínico%^RESET%^");
	set_long("Observas un extraño dispositivo, de forma alargada, parecido a una antorcha. "
			"Parece estar fabricado con algún tipo de metal muy ligero. Su parte superior está "
			"cubierta por una esfera de cristal transparente, que deja ver una maraña de cables, "
			"cordeles y poleas en su interior. Un filamento oscuro y sin brillo, forma "
			"un círculo contra la esfera de cristal.\n"
			"En la parte inferior del dispositivo, cerca de la base, hay un pequeño botón. Está "
			"colocado para poder ser pulsado de manera cómoda usando el dedo pulgar, siempre que "
			"las manos del usuario no sean muy grandes.\n");

	generar_alias_y_plurales();

	fijar_genero(1);

    //Configuración hechizo
    fijar_recuperacion(BLOQUEO);
    fijar_esfera(ESCUELA);

    fijar_material(2);

    setup_limitado_gloria();
}

string descripcion_juzgar(){
    return "Este replicador permite crear un simulacro de su portador, permitiendo engañar a los enemigos.\n";
}

//Configuración hechizo
void iniciar_formulacion(string h,object portador){
    tell_object(portador,"Alzas tu "+query_short()+" y presionas el botón. "
        "Por un instante parece que no pasa nada.\n");
    tell_accion(environment(portador),portador->query_short()+" alza "
        "su "+query_short()+ "y presiona el botón. Por un instante parece que no ocurra nada.\n",({portador}),portador);
    return;
}

void realizar_cantico(object lanzador, object h, string c){
    tell_object(lanzador,"De repente, "+dame_articulo()+" "+query_short()+" se ilumina, llenando el entorno de una brillante luz blanca, que te envuelvo por completo.\n"); 
    tell_accion(environment(lanzador),"De repente, "+dame_articulo()+" "+query_short()+" se ilumina, llenando el entorno de una brillante luz blanca que envuelve por completo a "+lanzador->query_short()+".\n",({lanzador}),lanzador);
    return;
}

int activar_simulacro(){
    return formular("simulacro",TP->query_name(),0);
}

void objeto_desequipado(){
    if(environment())
        environment()->quitar_efecto("simulacro",0); //Quitamos el efecto evaluando.
}

void init(){
    ::init();
    anyadir_comando_equipado("pulsar","bot(o|ó)n","activar_simulacro","Activa el poder "+dame_del()+" "+query_short());
}
