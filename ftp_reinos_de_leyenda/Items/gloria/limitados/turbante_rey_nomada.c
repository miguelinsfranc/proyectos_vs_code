// Satyr 21.05.2017
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";


void setup()
{
    fijar_armadura_base("capucha");

    set_short("%^ORANGE%^T%^WHITE%^urbante del %^ORANGE%^R%^WHITE%^ey %^ORANGE%^N%^WHITE%^ómada%^RESET%^");
    set_main_plural("%^ORANGE%^T%^WHITE%^urbante del %^ORANGE%^R%^WHITE%^ey %^ORANGE%^N%^WHITE%^ómada%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Una suave chalina tintada de un color rojo que ha sido cosida por su interior para adoptar permanentemente "
            "la forma de un turbante. Está adornado con tres plumas de avestruz que brotan de uno de sus "
            "lados sujetas a la tela mediante un camafeo que ha sido cosido a la prenda. La tela ha perdido buena "
            "parte de su color y en su interior aún encuentras granos de arena, pues ha sufrido durante décadas "
            "el azote de las tormentas.\n\n"

        "  Otrora esta prenda perteneció al Rey Nómada, una figura histórica de orígenes inciertos que se dedicó "
            "a deambular por el desierto y unificar a todas las tribus independientes que se encontró. Para ello "
            "se valió de su afilada y pedante retórica, el poder de su hechiceria y, como no, de su su excelente "
            "habilidad marcial.\n\n"

        "  La leyenda del Rey se diluye, pues se le atribuyen todo tipo de heroicidades, cada cual más descabellada "
            "quee la anterior. Se sabe que su legado terminó poco después del cataclismo, cuando su tribu -conocida "
            "como \"la tribu de uno\"- sufrió una gran devastación a raíz de la manifestación del Arcángel de la "
            "Ruina.\n\n"

        "  Tras su muerte, su gente se dispersó por el mundo, pero no sin antes erigir una gran pirámide a su "
            "monarca para que le sirviese como lugar de descanso eterno. Los nómadas no tardaron mucho en olvidar "
            "a su rey, pues su lugar de descanso fue exhumado poco después para saquear todas sus riquezas.\n"
    );

    add_static_property("habilidad", "orientacion en desiertos");
    fijar_encantamiento(30);
    fijar_tamanyo_armadura(0);
    fijar_peso(10000);
    fijar_BO(10);
    
//    fijar_valor(6000);
//    fijar_coste_gloria(275);
    setup_limitado_gloria();
}
string descripcion_juzgar(object b) {
    return "Este turbante otorga a su portador las aptitudes del Rey Nómada, confiriéndole inmunidad "
        "contra los efectos de tipo 'desorientar'.\n"
    ;
}
