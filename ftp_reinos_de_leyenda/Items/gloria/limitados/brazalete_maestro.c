// Satyr 22.08.2016 -- Usando una idea del creador de Nirnesil en otro tipo de objeto
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

#define BONO 10
void setup()
{
    fijar_armadura_base("brazalete");
    set_short("%^RED%^B%^CYAN%^razalete %^RED%^M%^CYAN%^aestro%^RESET%^");
    set_main_plural("%^RED%^B%^CYAN%^razaletes %^RED%^M%^CYAN%^aestros%^RESET%^");
    generar_alias_y_plurales();
    
    set_long(
        "Un brazalete de cuero de bóvido extremadamente liviano que está adornado "
            "con bandas rojizas longitudinales y reforzado en su interior por láminas de hueso. "
            "Un cuerno del color macilento brota de entre los pliegues de piel curtida "
            "y parece tan peligroso ahora como seguramente lo fuese en vida.\n\n"
            
        "  Tiene un origen tan desconocido como la magia que encierran, capaz de "
            "otorgar a sus portadores una pericia con las armas más allá de lo imaginable.\n"
    );
    
    fijar_encantamiento(20);
    
    ajustar_BO(5);
    add_static_property("ts-fuerza bruta", 8);
    add_static_property("ts-paralizacion", 5);

//    fijar_valor(4000);
//   fijar_coste_gloria(200);
    setup_limitado_gloria();
}
string descripcion_juzgar(object pj) {
    return "Todas las maestrías del portador se incrementarán en +" + BONO + " al usar estos brazaletes.";
}
void objeto_equipado() {
    object sh;
    
    if ( ! environment() )
        return;
        
    if ( ! sh = clone_object(GLORIA_LIMITADOS + obtener_info_ruta(TO) + "_sh.c") )
        return;
        
    sh->setup_sombra(environment(), TO, BONO);
    environment()->equipo_fix();
}
void objeto_desequipado() {
    if ( environment() ) {
        environment()->destruir_shadow_brazalete_maestro();
        environment()->equipo_fix();
    }
}
