//Eckol Abr18: Del concurso de objetos de gloria.

#include <pk.h>

inherit GLORIA_LIMITADOS+"base_limitados.c";
inherit "/obj/escudo"; 

void setup()  
{ 
    fijar_escudo_base("rodela");
    set_name("escudo");
    set_short("%^MAGENTA%^Rodela%^RESET%^ de la %^BOLD%^YELLOW%^Fortuna%^RESET%^");
    set_main_plural("%^MAGENTA%^Rodelas%^RESET%^ de la %^BOLD%^YELLOW%^Fortuna%^RESET%^");
	generar_alias_y_plurales();

	set_long("Una escudo de metal dorado, pulido hasta el punto poderse ver perfectamente el reflejo del entorno en él Su tamaño no es muy grande, pero al ser ligero que otras rodelas similares, permite una gran manejabilidad. En la parte trasera lleva unas tiras de cuero, curtidas y teñidas en un color morado, que se ajustan comodamente en el antebrazo. Una pequeña asa, forjada en el mismo metal y cubierta de cuero para mejor agarre, permite sujetar la rodela con firmeza.\n");

    fijar_genero(1);

	fijar_peso(2200);

	fijar_material(2); //Metal

	ajustar_BO(7);

    nuevo_efecto_basico("fortuna");

	setup_limitado_gloria();
}
