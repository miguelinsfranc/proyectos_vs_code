//Eckol Jul17
//Eckol Oct17: Gana herbalismo.
// Astel 26.02.18 Bloqueo combate para el libro

#include <pk.h>
#include <plantas.h>

#define TIEMPO_BLOQUEO 120
#define PROP_BLOQUEO "usado" //Propiedad para el libro. El jugador tendra un blqqueo combate.

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/objeto_general.c";

void setup(){
    set_short("%^BOLD%^GREEN%^Manual de botánica de Lady Carstine%^RESET%^");
    set_long("Lady Carstine fue una hechicera de la 3a era. Su especialidad eran "
                "los hechizos de encantamiento, y en particular su depurada técnica "
                "le permitía imbuir su consciencia y conocimiento en objetos inanimados. "
                "Solo unos pocos de estos objetos encantados han sobrevivido al paso de los años. "
                "Uno de ellos es el libro que tienes en tus manos: un manual de plantas. A diferencia "
                "de un simple libro, basta con %^BOLD%^WHITE%^colocar el manual en el suelo%^RESET%^ "
                "para que en las páginas del libro aparezcan escritos los nombres de las plantas del lugar, "
                "de más frecuentes a menos frecuentes.\n"
                "La cubierta del manual está decorada con tres plantas, representadas una al lado de otra, "
                "con el nivel del suelo en la parte central del libro. De esta forma, las hojas y las raíces "
                "de las plantas están representadas con el mismo nivel de detalle.\n");
    set_main_plural("%^BOLD%^GREEN%^Manuales de botánica de Lady Carstine%^RESET%^");
    generar_alias_y_plurales();

	setup_limitado_gloria();

    fijar_genero(1);
    fijar_material(8);

    add_static_property("habilidad","herbalismo");

}

int chequeo_previo(){
    if(TP->dame_bloqueo_combate(TO))
        return notify_fail("Parece que has usado "+dame_numeral()+" "+query_short()+" recientemente.\n"
                            "Tendrás que esperar un poco más antes de volver a usarl"+dame_vocal()+".\n");

    //if(query_timed_property(PROP_BLOQUEO))
    if (dame_bloqueo_combate(PROP_BLOQUEO))
        return notify_fail("Parece que est"+dame_vocal(1)+" "+query_short()+" se ha usado recientemente. "
                            "Es demasiado pronto para volver a usar su poder.\n");

    return 1;
}

//Compara para ordenar en orden descendiente.
int comparar(int a,int b){
    if(a<b)
        return -1;
    if(a>b)
        return 1;

    return 0;
}

string construir_mensaje(string *plantas, string entorno){
    string r = "En "+entorno+" se pueden encontrar las siguientes plantas:\n";

    foreach(string p in plantas)
        r += " - "+capitalize(p)+".\n";

    return r;
}

mapping nuevo_mapping(mapping plantas,mapping map)
{
	foreach(string hierba in keys(map))
		if(plantas[hierba])
			plantas[hierba]+= map[hierba];
		else
			plantas[hierba]= map[hierba];
	return plantas;
}

int activar_manual(){
    mapping plantas_entorno;
    string *plantas_ordenadas;
    string res;
    if(!chequeo_previo())
        return 0; //Chequeo previo ya fija el notify_fail.

    tell_accion(ENV(TP),TP->query_cap_name()+" coloca su "+query_short()+" en el suelo\n",
                        "Oyes como alguien deja un objeto en el suelo.\n",({TP}),TP);
    tell_object(TP,"Colocas cuidadosamente tu "+query_short()+ " en el suelo.\n");

    tell_accion(ENV(TP), "Las raíces de las plantas de la cubierta "+dame_del()+" "+query_short()+
                            "empiezan a crecer y a introducirse en la tierra.\n",
                        "Oyes el ruido de algo hurgando en la tierra.\n",({TP}),TP);

    plantas_entorno = nuevo_mapping(PLANTAS->dame_plantas_entorno(ENV(TP)),PLANTAS->dame_plantas_autoctonas(ENV(TP)));

    if(!plantas_entorno || !sizeof(plantas_entorno)){
        tell_accion(ENV(TP),"Pasados unos segundos, "+TP->query_cap_name()+" recoge su "+query_short()+
                            " algo decepcionad"+TP->dame_vocal()+".\n", "Oyes como alguien recoge un objeto del suelo\n",
                            ({TP}),TP);
        tell_object(TP,"Al ver que las raíces se retraen, recoges "+dame_articulo()+" "+query_short()+".\n"
                        "Las plantas de la cubierta tienen un aspecto seco, indicando que nada crece en "
                        "este entorno.\n");

        //Si no encuentra nada, no ponemos bloqueo.
        return 1;
    }

    plantas_ordenadas = sort_array(keys(plantas_entorno),(:comparar($(plantas_entorno)[$1],$(plantas_entorno)[$2]):));

    res = construir_mensaje(plantas_ordenadas,ENV(TP)->query_short());

    fijar_mensaje_leer(res,TP->dame_lenguaje_actual());

    tell_accion(ENV(TP),"Pasados unos segundos, "+TP->query_cap_name()+" recoge su "+query_short()+
                            " y sonríe satisfech"+TP->dame_vocal()+".\n", "Oyes como alguien recoge un objeto del suelo\n",
                            ({TP}),TP);
    tell_object(TP,"Al ver que las raíces se retraen, recoges "+dame_articulo()+" "+query_short()+". "
                        "Las plantas de la cubierta tienen un aspecto fresco, indicando que en las páginas "
                        ""+dame_del()+" "+query_short()+" se pueden %^BOLD%^WHITE%^leer%^RESET%^ los nombres "
                        "de las plantas que crecen en este entorno.\n");

    TP->nuevo_bloqueo_combate(TO,TIEMPO_BLOQUEO);
    //add_timed_property(PROP_BLOQUEO,1,TIEMPO_BLOQUEO); 
    nuevo_bloqueo_combate(PROP_BLOQUEO,TIEMPO_BLOQUEO);
    return 1;
}

void init(){
    ::init();

    anyadir_comando_inventario("colocar","el manual en el suelo","activar_manual");
}



