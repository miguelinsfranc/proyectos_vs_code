// Satyr 22.08.2016
// Eckol Mar17: Ahora no puede usarse si no hay suficiente fe.
// Eckol Jul17: Revisión. Pasa a formular huellas vitales.
// Astel 23.02.18: Le fijo la esfera.

#include <pk.h>
#include <liquidos.h>
#include <baseobs_path.h>
#include <puntos_fe.h>
#include <spells.h>

#define BLOQUEO 300

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit BMISC + "odre.c";
inherit "/obj/magia/objeto_formulador.c"; // Lo uso solo para que el item sea el
                                          // controlador.

void setup()
{
    ::setup();

    set_short("%^ORANGE%^Odre de %^RED%^Ralder%^RESET%^");
    set_main_plural("%^ORANGE%^Odres de %^RED%^Ralder%^RESET%^");
    generar_alias_y_plurales();

    set_long("La vejiga seca y curada de un fiero oso de las cavernas de "
             "Ralder a la que se le ha "
             "cosido una burda cremallera en uno de sus extremos más "
             "estrechos. El tratamiento "
             "al que ha sido sometida ha hecho que buena parte de su capacidad "
             "se pierda a costa "
             "de ganar grandes poderes mágicos capaces de otorgar parte de los "
             "poderes "
             "insignia del Gargante a aquellos que lo usen apropiadamente.\n");

    fijar_material(3);
    fijar_capacidad_max(3);

    fijar_valor(500 * 500);
    fijar_coste_gloria(150);

    fijar_esfera(3);

    setup_limitado_gloria();
}

void activar(object pj)
{
    string hechizo;
    pj->add_extra_look(TO);
    // pj->nueva_habilidad_temporal("rastrear");
    pj->nuevo_bloqueo_combate(TO, BLOQUEO);

    tell_object(
        pj,
        "Embadurnas tus manos con el " + S_LIQUIDO("almizcle_ralder") +
            " y lo extiendes por tu "
            "cara con la ayuda de los dedos corazón y anular. Al instante un "
            "aroma pesado, fuerte, "
            "primal hace que tus pupilas se dilaten y tu olfato y oído se "
            "agudicen.\n\n");

    tell_accion(
        environment(pj),
        pj->query_cap_name() + " se embadurna las manos con el " +
            S_LIQUIDO("almizcle_ralder") + " de " + SU_S(TO) +
            " y se lo extiende por la cara con la ayuda "
            "de sus dedos corazón y anular. Al momento, sus pupilas se "
            "dilatan.\n",
        "",
        ({pj}),
        pj);

    if (!hechizo = INDICE_X("huellas vitales", MAGE_ROOT)) {
        tell_object(
            TP,
            "Se ha producido un error al buscar el hechizo. Avisa a un "
            "inmortal.\n");
        return;
    }
    hechizo->formular(TP, 0, 0, 0, 0, 1, TO);

    if (pj->dame_religion() == "ralder" || pj->dame_religion() == "ateo")
        return;

    tell_object(
        pj,
        "Marcarte con el " + S_LIQUIDO("almizcle_ralder") +
            " te hace cuestionar, por un momento, "
            "tus creencias religiosas.\n");

    pj->ajustar_fe(-FE_USO1);
}
void desactivar(object pj)
{
    pj->remove_extra_look(TO);

    tell_object(
        pj,
        "Tus pupilas se contraen y dejas de percibir el fuerte aroma de "
        "Ralder. Los efectos del " +
            S_LIQUIDO("almizcle_ralder") + " se han terminado.\n");
}
string descripcion_juzgar(object b)
{
    return "Este odre convierte en " + S_LIQUIDO("almizcle_ralder") +
           " cualquier líquido que se guarde en él. "
           "Un jugador que posea el odre podrá usar el contenido de su "
           "interior para "
           "marcarse la cara, llenándose con el espíritu animal del gargante y "
           "formulando mágicamente el hechizo \"Huellas vitales\".\n";
}
int intentar_marcarse()
{
    if (TP->dame_bloqueo_combate(TO))
        return notify_fail(
            "El fuerte hedor de Ralder aún perdura en tus papilas gustativas. "
            "No crees que otra dosis del " +
            S_LIQUIDO("almizcle_ralder") + " te permitiese respirar.\n");

    if (!dame_capacidad())
        return notify_fail("¡Tu " + query_short() + " está vacío!\n");

    if (dame_liquido() != "almizcle_ralder")
        return notify_fail(
            "Tu " + query_short() +
            " aún no ha convertido su contenido a almizcle.\n");

    if (-1 == member_array(TP->dame_religion(), ({"ateo", "ralder"})) &&
        TP->dame_fe() < FE_USO1)
        return notify_fail(
            "Tus débiles creencias religiosas hacen que te sientas incapaz de "
            "recibir el favor de ningún dios.\n");

    activar(TP);
    ajustar_capacidad(-1);          // 3 usos (tiene capacidad 3).
    call_out("desactivar", 10, TP); // Esto ahora es solo decorativo.
    return 1;
}
void init()
{
    ::init();

    anyadir_comando_equipado(
        "marcarse",
        "con el almizcle",
        (
            : intentar_marcarse:),
        "Te marca con el " + S_LIQUIDO("almizcle_ralder") +
            ", formulando el hechizo \"Huellas vitales\".");
}
void convertir_liquido(string tx)
{
    object x = dame_ob_liquido();

    if (!x) {
        return;
    }

    fijar_liquido("almizcle_ralder");

    if (environment())
        tell_object(
            environment(),
            "Un aroma fuerte y denso emana de tu odre de Ralder cuando "
            "convierte " +
                x->dame_articulo() + " " + x->query_short() + " en " +
                S_LIQUIDO("almizcle_ralder") + ".\n");
}
void fijar_liquido(string tx)
{
    ::fijar_liquido(tx);

    if (!tx || tx == "almizcle_ralder")
        return;

    if (-1 == find_call_out("convertir_liquido"))
        call_out("convertir_liquido", 5 + random(11), tx);
}
