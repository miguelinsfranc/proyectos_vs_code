// Satyr 22.08.2016
#include <pk.h>
#include <fronteras.h>
#define BLOQUEO 60
#define PUPA    -(100 + random(100))

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

// Devuelve una palabra de poder generada aleatoriamente
string dame_palabra_poder() {
    string tx;
    
    if ( tx = query_property("palabra_poder") )
        return tx; 

    tx = "";
    
    while(strlen(tx) < 8 + random(3))
        tx += element_of(explode("abcdefghijklmnopqrstuvwxyz", ""));

    add_property("palabra_poder", tx);
    
    return tx;
}
string descripcion_juzgar(object juzgador) {
    return "El poder del camafeo puede despertarse para permitir a su portador localizar "
        "a todas las criaturas afectadas por una herida en la frontera en la que se encuentra, "
        "así como "
        "a todos sus enemigos afectados por una herida (sin importar su ubicación). "
        "Esta activación "
        "tendrá un bloqueo de " + pretty_time(BLOQUEO) + " y costará "
        "al ejecutor una pequeña porción de su vida.\n"
    ;
}
void setup()
{
    fijar_armadura_base("collar");
    set_name("camafeo");
    set_short(
        "%^WHITE%^Camafeo de los %^BOLD%^BLACK%^G%^RED%^riemfis%^RESET%^"
    );
    set_main_plural(
        "%^WHITE%^Camafeos de los %^BOLD%^BLACK%^G%^RED%^riemfis%^RESET%^"
    );
    generar_alias_y_plurales();
    
    set_long(
        "Una fina y vieja cadena de plata que sujeta un ostentoso camafeo de "
            "mármol en el que se ve la talla del rosto de una mujer noble. "
            "La pieza no está muy pulida y sus facciones se pierden por la carencia "
            "de detalles, pero los emblemas que adornan la parte posterior del camafeo "
            "identifican a la figura como un miembro de la familia Griemfi.\n\n"
            
        "  Cualquier recuerdo de esta estirpe, caída en el olvido y desgracia cuando "
            "su fundador, Drakull, abrazó el vampirismo, ha sido borrada de los "
            "registros históricos de Eirea. Sin embargo el rastro de la magia oscura "
            "de la cabeza de Eniell, consorte de Drakull, aún parece habitar en "
            "esta joya y susurrarte al oído con voz siniestra cuando te lo acercas.\n"
    );
    
    add_static_property("critico"        ,    5);
    add_static_property("daño"           ,   15);
    add_static_property("armadura-magica",   -5);   
    add_static_property("sigilar"        ,  -20);   
    add_static_property("esconderse"     ,  -20);   
    
    set_read_mess(
        "La palabra de poder es " + dame_palabra_poder() + "",
        "gnomo"
    );

    fijar_material(9);
    fijar_encantamiento(20);
//    fijar_coste_gloria(255);
//    fijar_valor(8000);
    setup_limitado_gloria();
}
// Determina si un jugador puede ser localizado
// @TODO: centralizar esto en algún sitio....
int jugador_valido(object pl) {
    if ( ! pl || pl->dame_novato() || pl->query_invis() )
        return 0;
    
    if ( "/handlers/conexiones.c"->comprobar_sin_conexion(pl) )
        return 0;
    
    if ( environment(pl) && environment(pl)->query_property("indetectable") )
        return 0;
    
    if ( pl->query_property("indetectable") )
        return 0;
    
    return 1;
}
// Determina si un objeto está herido
int jugador_herido(object pl) {
    if ( ! pl || ! living(pl) || ! pl->dame_ob_raza() || ! pl->dame_ob_raza()->dame_sangre() )
        return 0;
    
    return sizeof(pl->dame_incapacidades("heridas"));

}
// Devuelve los objetivos válidos y heridos de la frontera de un jugador
object *dame_enemigos_frontera(object portador) {
    string *frontera = FRONTERAS_DAME_FRONTERA_JUGADOR(portador);
    object *quienes;
    mixed salas;
    
    if ( ! frontera || ! sizeof(frontera) ) 
        return allocate(0);
    
    if ( ! salas = FRONTERAS_DAME_SALAS_FRONTERA(frontera) )
        return allocate(0);
    
    if ( ! sizeof(salas = map(salas, (: find_object($1) :)) - ({0})) )
        return allocate(0);
    
    quienes = allocate(0);
    
    foreach(object sala in salas) {
        quienes |= filter(all_inventory(sala), (: jugador_herido($1) :));
    }
    
    return filter(quienes, (: jugador_valido :));
}
// Devuelve los objetivos válidos y heridos que estén en la lista de recientes de un objeto
object *dame_enemigos_heridos(object portador) {
    return filter(
        portador->dame_enemigos_recientes(),
        (: jugador_herido($1) && jugador_valido($1) :)
    );
}
// Devuelve los objetos localizables por el camafeo
object *dame_enemigos_activacion(object portador) {
    return (dame_enemigos_heridos(portador) | dame_enemigos_frontera(portador)) - ({portador});
}
// Activación de su poder...
int activar_camafeo() {
    object *enemigos;
    int pupa;
    
    if ( dame_bloqueo_combate(TO) )
        return notify_fail("El poder de " + dame_tu() + " "  + query_short() + " se ha agotado.\n");
    
    if ( TP->dame_bloqueo_combate(TO) )
        return notify_fail("Ya has desperado recientemente ese poder.\n");
        
    nuevo_bloqueo_combate(TO, BLOQUEO);
    TP->nuevo_bloqueo_combate(TO, BLOQUEO);
    
    if ( 1 > pupa = TP->danyo_sangrado(PUPA, TO) ) 
        return notify_fail(capitalize(dame_tu()) + " brilla intensamente al activarse, "
            "pero al no poder alimentarse de tu sangre " + dame_este(1) + " no "
            "parece reaccionar.\n"
        );
        
    tell_object(TP, "Al pronunciar las palabras de poder " + dame_tu() + " "
        + query_short() + " vibra y chirria, causando que un horrendo pitido in "
        "crescendo te haga sangrar por los oídos.\n"
    );
    
    if ( ! sizeof(enemigos = dame_enemigos_activacion(TP)) )
        return notify_fail(
            "El rostro engrabado en " + capitalize(dame_tu()) + " " + query_short() + " "
                "comienza a llorar la sangre que te ha arrebatado hasta que ésta "
                "se reduce a un hilillo carmesí que termina secándose. Parece que su "
                "poder no ha surtido efecto.\n"
        );
        
        
    tell_object(TP,
        capitalize(dame_tu()) + " " + query_short() + " comienza a llorar la sangre que "
            "te ha arrebatado y el horroroso pitido de tus oídos comienza a atenuarse "
            "para dejar lugar a un silencio que solo se rompe con " + (
                sizeof(enemigos) == 1 
                    ? "el incesante latido de un corazón"
                    : "los incesantes latidos de " + sizeof(enemigos) + " corazones"
            ) + "...\n"
    );
    
    foreach(object enemigo in enemigos) {
        string nombre, donde;
        
        donde  = environment(enemigo)->query_short();
        nombre = enemigo->dame_ob_raza()->color_racial(enemigo->dame_ob_raza());
        
        tell_object(TP, 
            " - " + element_of(({
            "Escuchas los ténues latidos del corazón de " + nombre + " en " + donde + ".",
            "Sientes la sangre de " + nombre + " manar en " + donde + ".",
            "Oyes como la vida de " + nombre + " se derrama a chorros en " + donde + ".",
            }))
            + "\n"
       );
    }
    
    return 1;
}
void init() {
    ::init();
    
    anyadir_comando_equipado(
        dame_palabra_poder(),
        "",
        (: activar_camafeo :)
    );
    fijar_cmd_visibilidad(dame_palabra_poder(), "", 1);
}
