// Grimaek 16/06/2023   Basado en la idea de ISTELL
// https://www.reinosdeleyenda.es/foro/ver-tema/ideas-para-items-de-gloria/#post-349961
#include <pk.h>
#include <depuracion.h>
#define BLOQUEO 300
#define DURACION 60

inherit GLORIA_LIMITADOS + "base_limitados";
inherit "/obj/armadura.c";

void setup()
{
    fijar_armadura_base("zapatos");
    set_name("Esparteñas Malolientes del Taumaturgo");
    set_short(
        "Esparteñas %^GREEN%^M%^ORANGE%^alolientes%^RESET%^ del Taumaturgo");
    set_main_plural("Pares de Esparteñas "
                    "%^GREEN%^M%^ORANGE%^alolientes%^RESET%^ del Taumaturgo");
    generar_alias_y_plurales();
    set_long("  Normalmente las esparteñas se suelen fabricar empleando una "
             "tela o cuero fuertemente unidos a una suela de cuerda de yute o "
             "cáñamo por manos artesanas.\n\n"
             "  Estas esparteñas que obran en tu poder, han debido ser ideadas "
             "por un hechicero loco o demente que ha sufrido algún golpe en la "
             "sesera tras un hechizo "
             "fallido, pues solo tal catástrofe podría dar el resultado de "
             "fabricar no una, sino un par de esparteñas con cuero de culo de "
             "goblin, unidas aún a las "
             "tripas malolientes, que sirven de suela. \n\n "
             "  Lo inexplicable de estas esparteñas, a pesar de su origen, es "
             "como las tripas siguen manteniendo un contenido asqueroso que "
             "trasciende los olores más viles.\n\n"
             "  En la esparteña izquierda, en el orificio de salida de las "
             "tripas, también llamado esfínter, tiene un pendiente de oro. "
             "Seguramente si se "
             "frota para limpiarlo aun conserve algo de valor.\n\n");
    fijar_genero(-2);

    add_static_property(
        "clase", ({"mago-ladron", "bardo", "hechicero", "mago_runico"}));

    add_static_property("poder_magico", 20);
    add_static_property("afinidad_fuego", 8);
    add_static_property("afinidad_agua", 8);
    add_static_property("afinidad_tierra", 8);
    add_static_property("afinidad_electrico", 8);

    add_static_property(
        "messon",
        "Al ponerte " + TU_S(TO) +
            " un olor repugnante invade el lugar. No obstante, sientes que el "
            "sacrificio merece la pena.\n");

    setup_limitado_gloria();
}
string descripcion_juzgar(object b)
{
    return sprintf(
        "%s %s es un objeto creado artesanalmente que encierra poder magico y "
        "otorga algunas bonificaciones. El " +
            "origen de estas esparteñas es desconocido, y aún sabiendo que "
            "tiene un poder oculto, temes usarlo porque seguro " +
            "que pasan factura en tu vida social. Este poder tiene un bloqueo "
            "de " +
            pretty_time(BLOQUEO) + ".\n",
        capitalize(dame_articulo()),
        query_short());
}
void arcadas(object quien, object propietario)
{
    int tirada = quien->dame_ts(
        "enfermedad", 50, "enfermedad", "esparteñas taumaturgo", propietario);

    if (tirada > 0) {
        return;
    }

    if (quien == propietario) {
        tell_object(
            quien,
            "El propio mal olor proveniente de tus " + query_short() +
                " te produce arcadas.\n");
    } else {
        tell_object(
            quien,
            "El mal olor proveniente de " + propietario->query_short() +
                " te produce arcadas.\n");
    }

    tell_accion(
        environment(quien),
        quien->query_short() + " tiene arcadas provocadas por el mal olor.\n",
        "Alguien está haciendo arcadas.\n",
        ({quien}),
        quien);

    quien->desconcentrar();
}
void heart_beat()
{
    object  propietario;
    object *alltodo;

    if (ENV(TO)) {
        propietario = ENV(TO);
    } else {
        return;
    }

    if (!query_timed_property("esparterastaumaturgo")) {
        set_heart_beat(0);
        tell_object(
            propietario,
            "La nube pestosa de tus esparteras comienza a disiparse lentamente "
            "mientras es arrastrada por el aire "
            "poco a poco hasta que solo queda de ella un rastro leve "
            "amarillento en el aire.\n");
        tell_accion(
            ENV(propietario),
            "La nube pestosa que inundaba el lugar con su aroma, comienza a "
            "disiparse lentamente mientras es arrastrada por el aire "
            "poco a poco hasta que solo queda de ella un rastro leve "
            "amarillento en el aire.\n",
            "",
            ({propietario}),
            TO);
        return;
    }

    alltodo = filter(all_inventory(ENV(propietario)), ( : living:));

    for (int x = 0; x < sizeof(alltodo); x++) {
        arcadas(alltodo[x], propietario);
    }
}
int activar_esparteras()
{
    if (TP->dame_bloqueo_combate(TO)) {
        return notify_fail(
            "Las tripas de tus " + query_short() +
            " aun no han rellenado su contenido apestoso.\n");
    }

    if (dame_bloqueo_combate(TO)) {
        return notify_fail(
            "Las tripas de tus " + query_short() +
            " aun no han rellenado su contenido apestoso.\n");
    }

    set_heart_beat(2);
    tell_object(
        TP,
        sprintf(
            "Te agachas para frotar el pendiente de tus %s que brilla con una "
            "luz dorada. "
            "Cuando te levantas, presionas incoscientemente los restos de "
            "tripas que forman "
            "su suela arrojando una nube pestosa y maloliente a tu "
            "alrededor.\n",
            query_short()),
        query_short());

    tell_accion(
        environment(TP),
        sprintf(
            "%s se agacha para frotar el pendiente de sus %s que brilla con "
            "una luz dorada. "
            "Cuando se levanta, presiona los restos de tripas que forman "
            "las suelas de las esparteñas arrojando una nube pestosa y "
            "maloliente a su alrededor.\n",
            TP->query_cap_name(),
            query_short()),
        "Un olor penetrante inunda el lugar.\n",
        ({TP}),
        TP);

    add_timed_property("esparterastaumaturgo", 1, DURACION);
    TP->nuevo_bloqueo_combate(TO, BLOQUEO, 0, 0, 0, 0, 0, 0);
    nuevo_bloqueo_combate(TO, BLOQUEO, 0, 0, 0, 0, 0, 0);
    TP->consumir_hb();
    return 1;
}
void init()
{
    ::init();
    anyadir_comando_equipado(
        "frotar",
        "pendiente de esparteñas",
        (
            : activar_esparteras()
            :),
        "Activa el poder oculto de las " + query_short() + ".");
}
void objeto_desequipado()
{
    set_heart_beat(0);

    if (query_timed_property("esparterastaumaturgo")) {
        remove_timed_property("esparterastaumaturgo");
    }
}
