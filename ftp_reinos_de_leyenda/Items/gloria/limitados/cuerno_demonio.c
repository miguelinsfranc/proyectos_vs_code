// Grimaek 16/03/2023. Basado en el cuerno basico /baseobs/misc/cuerno.c de
// Vhurkul, Ember y Satyr Basado en la descripción de
// https://www.youtube.com/watch?v=UDJwDofwkgk

#include <baseobs_path.h>
#include <diplomacias.h>
#include <material.h>
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit BMISC + "cuerno.c";

#define BLOQUEO 960
#define DURACION 300
#define BS 10
#define BONO_TS_HORROR 8

/*******************************************************************************
 * Sección: ajustes básicos del objeto
 ******************************************************************************/
void setup()
{
    set_name("cuerno");
    set_short("Cuerno de hueso del Príncipe %^RED%^Demonio%^RESET%^");
    set_main_plural("Cuernos de hueso del Príncipe %^RED%^Demonio%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "El cuerno es uno de los más simples y antiguos tipos de aerófonos. "
        "Consiste en un cuerno hueco, "
        "normalmente de origen animal, en el cual su punta es perforada para "
        "adaptarla como boquilla. En la mayoría "
        "de los casos, los labios son aplicados directamente a la punta; "
        "algunas veces, el cuerno tiene una boquilla "
        "adaptable. \n\n  Toros, carneros, cabros y antílopes están entre los "
        "animales cuyos cuernos se usaban frecuentemente "
        "para fines de llamada, comunicación, rituales o guerra, por su sonido "
        "estridente. Posteriormente, gracias a "
        "que mejoraron las técnicas para conseguir los despojos de los "
        "cadáveres enemigos, se consiguió por maestros "
        "artesanos imbuir en los cuernos parte de las propiedades especiales "
        "que eran esencia pura de los distintos "
        "tipos de criaturas. \n\n"
        "  Un ejemplo de esto, es el cuerno que tienes delante, procedente de "
        "una príncipe demoníaco de algún abismo desconocido. \n\n"
        "  El cuerno, de hueso negro, ha sido decorado con filigranas de oro "
        "en la abertura, y se ha incorporado una boquilla con madera de Ent "
        "ennegrecido. \n\n"
        "  Cuando se sopla, deja un sabor tan intenso en el paladar, que los "
        "más experimentados guerreros, serán capaces de degustar las notas "
        "de destrucción e incineración que se sienten en los grandes campos de "
        "batalla.\n\n");

    fijar_material(HUESO);
    fijar_peso(5000);
    fijar_valor_venta(50000);
    set_holdable(1);

    setup_limitado_gloria();
}
string dame_ayuda_cuerno()
{
    return "Este cuerno, creado mediante alta artesanía y magia arcana, "
           "permite envalentonar a las tropas y mejorar sus bonificaciones "
           "ofensivas y defensivas. Cuando lo soplas, suena un rugido "
           "demoníaco por todo el reino.";
}
void mostrar_mensajes_inicio_cuerno(object pj)
{
    tell_object(
        pj,
        "Empuñas con fuerza " + TU_S(TO) + " y " + dame_lo() +
            " haces sonar con todo el aire de tus pulmones. El cuerno "
            "exhala un hálito incandescente que hiede a azufre y a muerte.\n");

    if (!environment(pj)) {
        return;
    }

    tell_accion(
        environment(pj),
        sprintf(
            "%s empuña con fuerza %s y %s hace soplar con todo el aire de sus "
            "pulmones. El cuerno exhala un hálito incandescente que hiede "
            "a azufre y a muerte.\n",
            pj->query_cap_name(),
            SU_S(TO),
            dame_lo()),
        "",
        ({pj}),
        pj);
}
/**
 * Función que aplica al finalizar el efecto del cuerno de quien
 * lo oye.
 *
 * @param pj     Jugador que ha recibido el efecto
 */
int fin_efecto(object pj)
{
    if (pj) {
        tell_object(
            pj,
            "Sientes como ha finalizado el efecto inspirador de tus "
            "camaradas.\n");
        pj->ajustar_ts_tmp("horror", -BONO_TS_HORROR);
    }

    return 1;
}
void aplicar_efectos_cuerno(object oyente, object pj)
{
    /**
     * Si no es el oyente y NO tiene permiso de PK con él no recibe ningún bono
     */
    if (pj != oyente && !pj->tiene_permiso_diplomatico(oyente, _P_PK)) {
        return;
    }

    oyente->ajustar_bo_tmp(BS, DURACION, base_name());
    oyente->ajustar_be_tmp(BS, DURACION, base_name());
    oyente->ajustar_bp_tmp(BS, DURACION, base_name());

    // clang-format off
    oyente->nueva_ts_tmp(
        "horror", BONO_TS_HORROR, DURACION, (: fin_efecto, oyente :), base_name());
    // clang-format on

    if (pj != oyente) {
        tell_object(
            oyente,
            "Inspirado por tus camaradas te sientes reforzado para la "
            "guerra.\n");
    }
}
