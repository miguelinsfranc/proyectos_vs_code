// Satyr 21/10/2019 10:38
// Concepto de Sierephad
#include <pk.h>
#include <combate.h>

inherit "/obj/magia/objeto_con_poderes.c";

/*******************************************************************************
 * SECCIÓN: Prototipos y variables
 ******************************************************************************/
#define DURACION 90
#define BLOQUEO 140
#define PODER_ROBAR "catalización de esencia"
#define MINIMO_RESISTENCIA -10
#define MAXIMO_RESISTENCIA 75
#define EFECTO "espejo catalizador"

string *dame_lista_resistencias_validas();

/*******************************************************************************
 * SECCIÓN: Funciones habituales de objetos
 ******************************************************************************/
void preparar_codigo_espejo()
{
    fijar_bloqueo_poderes(BLOQUEO, PODER_ROBAR);
}
string descripcion_juzgar()
{
    return sprintf(
        "La catálisis generada por este objeto será capaz de intercambiar "
        "las resistencias mágicas a un elemento de un objetivo e "
        "intercambiarlas con las tuyas. "
        "Este poder está sometido a un bloqueo de %s y su duración ronda los "
        "%s.\n\n"
        " Cabe destacar que los objetivos tendrán derecho a superar una tirada "
        "de salvación contra 'artefacto' para librarse de los efectos. "
        "Adicionalmente, las resistencias catalizadas de esta forma no podrán "
        "reducirse por debajo de %d ni ascender por encima de %d.",
        pretty_time(BLOQUEO),
        pretty_time(DURACION),
        MINIMO_RESISTENCIA,
        MAXIMO_RESISTENCIA);
}
void objeto_desequipado()
{
    if (environment()) {
        environment()->destruir_mi_shadow_espejo_catalizador(TO);
    }
}
/*******************************************************************************
 * SECCIÓN: Chequeos sencillos y auxiliares
 ******************************************************************************/
string *dame_lista_resistencias_validas()
{
    return ({"acido",
             "aire",
             "frio",
             "mental",
             "magico",
             "electrico",
             "agua",
             "fuego",
             "tierra"});
}
string dame_ruta_shadow()
{
    return GLORIA_LIMITADOS + "espejo_catalizador_sh.c";
}
int terminar_espejo(object obj)
{
    if (obj) {
        obj->destruir_shadow_espejo_catalizador();
    }
}
/*******************************************************************************
 * SECCIÓN: Acciones
 ******************************************************************************/
int do_catalizar(object quien, object obj, string poder, string elemento)
{
    int     mi_res, su_res;
    string  aux;
    object  sh;
    mapping p;

    if (aux = chequeo_previo_finalizar_poder(TP, obj, poder)) {
        tell_object(quien, aux + "\n");
        return 0;
    }

    mi_res = max(({MINIMO_RESISTENCIA, quien->dame_resistencia(elemento)}));
    su_res = min(({MAXIMO_RESISTENCIA, obj->dame_resistencia(elemento)}));

    if (!mi_res && !su_res) {
        mostrar_mensajes_poderes("resistencias_nulas", quien, obj, poder);
        return 0;
    }

    if (obj->dame_shadow_espejo_catalizador()) {
        mostrar_mensajes_poderes("ya_afectado", quien, obj, poder);
        return 0;
    }

    if (obj->dame_ts("artefacto", 0, 0, base_name(), quien)) {
        log_artefacto(sprintf(
            "%s resiste la TS de %s", obj->query_name(), quien->query_name()));
        mostrar_mensajes_poderes("ts_resistida", quien, obj, poder);
        return 0;
    }

    if (!sh = clone_object(dame_ruta_shadow())) {
        mostrar_mensajes_poderes("error", quien, obj, poder);
        return 0;
    }

    p = (["artefacto":TO,
           "elemento":elemento,
            "res_max":MAXIMO_RESISTENCIA,
            "res_min":MINIMO_RESISTENCIA,
    ]);

    log_artefacto(sprintf(
        "%s usa la catálisis de %s contra %s (%d / %d)",
        quien->query_cap_name(),
        elemento,
        obj->query_player() ? obj->query_name() : base_name(obj),
        quien->dame_resistencia(elemento),
        obj->dame_resistencia(elemento)));

    sh->setup_shadow(quien, obj, p);
    sh = clone_object(dame_ruta_shadow());
    sh->setup_shadow(obj, quien, p);
    // clang-format off
    quien->nuevo_efecto(
        EFECTO,
        DURACION,
        (: terminar_espejo($(quien)) :)
    );
    obj->nuevo_efecto(
        EFECTO,
        DURACION,
        (: terminar_espejo($(obj)) :)
    );
    // clang-format on

    mostrar_mensajes_poderes("finalizacion", quien, obj, poder, elemento);
    return 1;
}
int try_catalizar(object obj, string elemento)
{
    __debug_x(sprintf("try_catalizar: %O, %O", obj, elemento));
    if (!chequeo_activacion_poder(TP, obj, PODER_ROBAR)) {
        return 0;
    }

    if (-1 == member_array(elemento, dame_lista_resistencias_validas())) {
        return notify_fail(sprintf(
            "'%s' no es una resistencia mágica que %s pueda %s. La lista con "
            "la que puede interactuar es: %s.\n",
            elemento,
            TU_S(TO),
            query_verb(),
            nice_list(dame_lista_resistencias_validas())));
    }

    mostrar_mensajes_poderes("activacion", TP, obj, PODER_ROBAR);
    poner_bloqueos(TP, obj, PODER_ROBAR);

    // clang-format off
    call_out(
        "finalizacion_poder",
        2 + (TP->query_hb_counter() % 2) * 2,
        TP,
        obj,
        0,
        (: do_catalizar :),
        0,
        elemento
    );
    // clang-format on
    return 1;
}
void init()
{
    // clang-format off
    anyadir_comando(
        "catalizar",
        "esencia elemental de <texto'Elemento intercambiar con tu objetivo'>[: "
        "<objeto:living:aqui:primero'Objetivo del poder'>]",
        (: try_catalizar($2, $1) :),
        "Invoca el poder de " + TU_S(TO) + " para intercambiar la resistencia "
        "a un elemento de un objetivo con la resistencia a dicho elemento "
        "que tú tengas.\n\n"
        "Las resistencias que pueden ser intercambiadas son: "
        + nice_list(dame_lista_resistencias_validas()) + ".\n"
    );
    // clang-format on
}
/*******************************************************************************
 * SECCIÓN: Mensajes
 ******************************************************************************/
string dame_mensajes_resistencias_nulas(
    object quien, object victima, string poder, mixed params...)
{
    return _C_FALLAR + "¡La catálisis no surte efecto al ser ambas "
                       "resistencias nulas!";
}

string dame_mensajes_ya_afectado(
    object quien, object victima, string poder, mixed params...)
{
    return _C_FALLAR + "¡" + victima->query_cap_name() +
           " ya está sufriendo los efectos de una catálisis!";
}
string
dame_mensajes_error(object quien, object victima, string poder, mixed params...)
{
    return _C_FALLAR + "¡Un error impide realizar la catálisis!";
}
string *dame_mensajes_finalizacion(
    object quien, object victima, string poder, string elemento)
{
    return (
        {sprintf(
             "%sLa cúspide de %s gira sobre sí misma a una velocidad "
             "vertiginosa antes de "
             "descargar un haz de luz que relampaguea hasta golpear de lleno a "
             "%s, catalizando sus resistencias mágicas de '%s' con las tuyas.",
             _C_GOLPEAR,
             TU_S(TO),
             victima->query_cap_name(),
             elemento),
         sprintf(
             "%s Las piezas de %s de %s giran vertiginosamente sobre sí mismas "
             "antes de descargar un haz de luz relampagueante que te impacta "
             "de lleno, catalizando tus resistencias contra '%s'.",
             _C_GOLPEADO,
             dame_del(),
             query_short(),
             elemento),
         sprintf(
             "%s Las piezas de %s de %s giran vertiginosamente sobre sí mismas "
             "antes de descargar un haz de luz relampagueante que impacta "
             "de lleno contra %s, catalizando sus resistencias contra '%s'.",
             _C_GOLPEADO,
             dame_del(),
             query_short(),
             victima->query_cap_name(),
             elemento)});
}
