// Satyr 06.07.2016 -- reviviendo un clásico
// Sierephad Jul 2k20	--	Revisando la formula del descifrar

#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("anillo");
    set_short("%^BLACK%^BOLD%^%^Anillo %^BOLD%^RED%^Olvidado%^RESET%^");
    set_main_plural("%^BLACK%^BOLD%^%^Anillos %^BOLD%^RED%^Olvidados%^RESET%^");
    generar_alias_y_plurales();

    set_long("Un liviano anillo negruzco que está adornado con "
             "inscripciones color bermellón que, si inspeccionaras más "
             "detenidamente, "
             "te darías cuenta de que están grabadas con sangre; sangre dura y "
             "seca, "
             "pero que al mismo tiempo parece serpentear y brillar como si "
             "estuviese viva. "
             "Está coronado por una gema nefaria en forma de ojo que parece "
             "palpitar "
             "con energías impías.\n");
    set_read_mess(
        "Puedes ver que en el anillo pone escrito a lo largo de todo el y"
        " en una lengua muy antigua:\n\n"
        "      %^RED%^BOLD%^Kherod-D'hin, Principe de los "
        "Demonios.%^RESET%^\n\n");

    // Este es el anillo de Kherod'Dhin, traedor de la muerte de las casas drow
    set_read_mess("nindol zhah l'anth d'kherod'dhin, ringha d'aphyon whol "
                  "l'ilythiiri qu'ellaren");

    add_static_property("habilidad", "espiar");
    add_static_property("envenenar", 25);

    fijar_material(5);
    fijar_encantamiento(20);
    setup_limitado_gloria();
}
mixed descifrar_inscripcion(string objeto, object quien)
{
    // Sierephad: como esta el descifrar, solo con inscripcion no se puede.
    // Sieguiendo el Fix de Eckol del anillo de priedra

    // if ( regexp(objeto, "inscripci(o|ó)n(es)?") )
    if (objeto == "anillo" || objeto == "olvidado" ||
        regexp(
            objeto,
            "inscripci(o|ó)n(es)?")) // Eckol: como esta el descifrar,
                                     // solo con inscripcion no se puede.
        return (
            {"Este es el anillo de Kherod'Dhin, traedor de la muerte de las "
             "casas drow",
             5,
             "drow"});
}
