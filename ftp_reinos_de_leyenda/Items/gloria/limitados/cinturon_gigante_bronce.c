// Satyr 26.09.2017
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("cinturon");

    set_short(
        "%^YELLOW%^C%^NOBOLD%^inturón del %^YELLOW%^G%^NOBOLD%^igante de %^YELLOW%^B%^NOBOLD%^ronce%^RESET%^"
    );
    set_main_plural(
        "%^YELLOW%^C%^NOBOLD%^inturones del %^YELLOW%^G%^NOBOLD%^igante de %^YELLOW%^B%^NOBOLD%^ronce%^RESET%^"
    );
    generar_alias_y_plurales();

    set_long(
        "Una fina y ancha lámina de bronce que está repleta de intrincados "
            "grabados -todos ellos hechos a base de ángulos rectos- que recorren la parte"
            " superior e inferior de su superficie. El hueco que dejan en su parte "
            "central está adornado con tallas que representan a los que fueron los "
            "ancestros de los enanos y de unas figuras gargantuescas: los mismos gigantes "
            "de bronce que vestían estos cinturones.\n\n"

        "Semejantes constructos de metal estaban atados a la voluntad de sus creadores "
            "y de Thrax, un dios de la suboscuridad que servía para proteger el legado "
            "enano en el mundo. Se decía que el propio dios había insuflado en estos "
            "titanes una parte de la llama que le daba la vida, dotándoles de una "
            "parte de su fuerza.\n\n"

        "Con la inevitable decaída del imperio enano y de sus fortalezas, los detalles "
            "de los gigantes de bronce, así como los de su creación, se perdieron en los "
            "castillos ruinosos que ahora pertenecen a las aborrentes criaturas que moran"
            " en la suboscuridad.\n\n"

        "Perdidos o no, esta pieza de anticuario ahora está en tus manos y su poder innato"
            " es palpable, pues su densidad es casi tan exorbitante como la fuerza "
            "que éste confiere.\n"
    );

    fijar_BO(6);
    fijar_BP(6);
    fijar_encantamiento(20);
    add_static_property("armadura-magica",  4);
    add_static_property("ts-fuerza bruta", 20);
    add_static_property("fue"            ,  1);
    add_static_property("ts-conjuro"     ,  5);
    add_static_property("ts-mental"      ,  5);

    fijar_material(2);
    fijar_encantamiento(20);
    fijar_peso(dame_peso() * 7);
    //add_static_property("tipo", ({"luchador", "caballero", "sacerdote"}));
    add_static_property("no_tipo", ({"cazador"}));

    setup_limitado_gloria();
}
