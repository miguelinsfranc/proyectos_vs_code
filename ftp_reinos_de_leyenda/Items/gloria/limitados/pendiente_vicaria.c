// Satyr 22.08.2016
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    
    fijar_armadura_base("pendiente");
    set_short(
        "%^CYAN%^Arete de la %^NOBOLD%^V%^BOLD%^icaria %^NOBOLD%^M%^BOLD%^inae%^RESET%^"
    );
    set_main_plural(
        "%^CYAN%^Aretes de la %^NOBOLD%^V%^BOLD%^icaria %^NOBOLD%^M%^BOLD%^inae%^RESET%^"
    );
    generar_alias_y_plurales();
    
    set_long(
        "Un alga reseca y encorvada de la que cuelga un finísimo aro perlado "
            "del diámetro de una manzana pequeña. La fabricación de una pieza "
            "de orfebrería tan delicada es algo impensable para la mayoría de "
            "moradores de la superficie, pues solo bajo el mar se pueden dar las "
            "condiciones necesarias para descomponer y recomponer perlas para darles "
            "este cabado. Las vicarias Minäe usaban estos aretes para potenciar "
            "las energías curativas de Nirvë -pues esta diosa es caprichosa "
            "y no siempre otorgaba el mismo poder a sus sacerdotes-, sin embargo, "
            "cualquier brujería que encierre la pieza no es ni la mitad de "
            "impresionante que los grabados de tamaño milimétrico que adornan "
            "el interior del arete.\n"
    );
    
    add_static_property("poder_magico"  , -50);
    add_static_property("poder_curacion", 200);
    add_static_property("tipo", ({"sacerdote"}));
    
    set_read_mess(
        "Propiedad de la Vicaria Bliglublë.",
        "aquan"
    );

    fijar_material(6);
    fijar_valor(8000);
    fijar_encantamiento(20);
//    fijar_coste_gloria(255);
//	fijar_tamanyo_armadura(0); //Universal
    setup_limitado_gloria();
}
