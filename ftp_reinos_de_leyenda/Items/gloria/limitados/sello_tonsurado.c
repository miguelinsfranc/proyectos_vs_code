// Satyr 30/11/2020 02:30
#include <pk.h>
#include <baseobs_path.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void generar_religion();

void setup()
{
    fijar_armadura_base("anillo");
    set_name("sello");
    set_short(
        "%^WHITE%^BOLD%^S%^RESET%^ello del %^WHITE%^BOLD%^T%^RESET%^onsurado");
    set_main_plural(
        "%^WHITE%^BOLD%^S%^RESET%^ellos del %^WHITE%^BOLD%^T%^RESET%^onsurado");
    generar_alias_y_plurales();

    set_long(
        "Una sortija de plata retorcida que está coronada con una pequeña " +
        "placa redonda grabada con letras menudas de caligrafía afilada. " +
        "Piezas como ésta han " +
        "sido habitualmente usadas como emblemas por los sacerdocios de gran " +
        "parte de los reinos, en muchas ocasiones sin importar las " +
        "diferencias de sus dogmas, hasta que los tiempos modernos —bastante " +
        "menos civilizados que la antigüedad— han dejado que dicha práctica " +
        "cayese en desuso.\n\n"

        "  El motivo de que creencias dispares compartan una misma pieza " +
        "se debe a que la plata nunca había sido un mineral abundante, " +
        "por una parte por su escasez y por otra porque los orfebres " +
        "aún no sabían trabajarla. Esto causó que un conjunto " +
        "reducido de artesanos, irónicamente ateos, se dedicase a fabricar " +
        "estas sortijas, que eran vendidas a los distintos sacerdocios de " +
        "los reinos para que los usasen como símbolos de iniciación al " +
        "sacerdocio de su religión.\n\n"

        "  La mayor abundancia del mineral —junto a las más acusadas " +
        "diferencias " +
        "de doctrina en las religiones mayoritarias— han hecho que esta " +
        "práctica se haya perdido y estas piezas no se fabriquen más, " +
        "haciendo que en el mundo solo perduren aquellas que antaño " +
        "fueron usadas por sacerdotes, algunos mundanos y otros "
        "legendarios.\n");

    fijar_material(9);
    fijar_encantamiento(20);

    setup_limitado_gloria();

    add_static_property("poder_magico", 5);
    add_static_property("poder_curacion", 20);
    add_static_property("negacion_petrificacion", 5);
    add_static_property("afinidad_bien", 3);
    add_static_property("afinidad_mal", 3);

    fijar_BO(-5);
    add_static_property("daño", -15);
    add_static_property("ts-conjuro", -5);

    if (!query_static_property("antigua_religion")) {
        generar_religion();
    }
}
void generar_religion()
{
    mapping dioses = (["oskuro":"dendrita", "lummen":"adurn", "lloth":"drow"]);
    string  que    = element_of(keys(dioses));
    string  tx;

    add_static_property("antigua_religion", que);

    switch (que) {
        case "oskuro":
            tx = "Que fieles, hermanos y hermanas sepan que hoy soy uno más, " +
                 "que camino con ellos por la sacra senda de la orden.";
            break;

        case "lummen":
            tx = "Esta banda es divina y quien la vista pertenece a la orden " +
                 "gracias a un vínculo de deber y honor.";
            break;

        case "lloth":
            tx = "La que vista esta banda será una de nuestras hermanas. " +
                 "Mísero el que no la reconozca como tal, pues incurrirá en " +
                 "la ira de nuestra señora.";
            break;
    }

    if (tx) {
        set_read_mess(tx, dioses[que]);
    }
}
