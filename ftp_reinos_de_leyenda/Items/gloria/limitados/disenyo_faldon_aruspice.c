// Satyr 25/11/2020 03:06
// Recompensa de gloria de la temporada 32
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/disenyo.c";

void setup()
{
    fijar_oficio("herrero");
    fijar_receta("faldon aruspice");
    fijar_lenguaje("negra");

    setup_limitado_gloria();
}
