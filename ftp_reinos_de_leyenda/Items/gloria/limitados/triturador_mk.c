// Satyr 26.09.2017
// Satyr 17/04/2020 02:58 -- admite carbón vegetal
// Satyr 22/04/2020 01:54 -- Subo bono crítico de +5 a +10% y talar de +15 a +25
#include <baseobs_path.h>
#include <unidades.h>
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/arma";

#define DESGARRADOR 30
#define BONO_EFECTO __ud(33, "%")
#define BONO_EFECTO_TXT "33%"
#define BONO_ENCANTAMIENTO 45
#define BONO_TALAR 25 // nótese que a esto se le sumará el enchant / 3
#define BONO_CRITICO_TALAR 2000 // 15%
#define MAXIMO_COMBUSTIBLE 1000 // en gramos
#define COSTE_HB 4 // coste (en gramos) por mantenerlo encendido cada turno

int dame_combustible();
int apagar();

private
int encendido;
private
string efecto;

string dame_short_basico()
{
    return "%^BOLD%^BLACK%^T%^NOBOLD%^ORANGE%^riturador%^RESET%^";
}
string dame_long_basico()
{
    return "Un enorme ástil de hierro negro que sujeta la cabeza de un hacha "
           "cuyo doble filo herrumbroso está unido por un horno en miniatura "
           "que "
           "se alza, estoico, en un extremo del pesado ástil. "
           "Desde esta caldera, negra pero aún así ennegrecida por el beso "
           "de la lumbre, se esparce una maraña de cadenas que se anclan a "
           "diversos puntos de cada uno de los filos gemelos.\n\n";
}
void actualizar_triturador()
{
    if (environment())
        environment()->equipo_fix();
}
void setup_encendido()
{
    fijar_encantamiento(BONO_ENCANTAMIENTO);
    efecto = nuevo_efecto_basico("machacaarmaduras", BONO_EFECTO);
    actualizar_triturador();
    set_heart_beat(1);

    set_short(dame_short_basico() + " (Encendido)");
    set_main_plural(pluralize(dame_short_basico()) + " (Encendidos)");
    set_long(
        dame_long_basico() +
        "  El horno de su cúspide alberga en su interior una llama que "
        "escupe una incesante nube de humo negro y que mantiene en "
        "movimiento circular las terroríficas uñas de obsidiana de "
        "esta pieza. El diseño de semejante instrumento es complejo, "
        "terrorífico y particularmente sádico.\n");

    generar_alias_y_plurales();
}
void setup_apagado()
{
    fijar_encantamiento(0);
    quitar_efecto(efecto);
    efecto = 0;
    actualizar_triturador();
    set_heart_beat(0);

    set_short(dame_short_basico());
    set_main_plural(pluralize(dame_short_basico()));
    set_long(
        dame_long_basico() +
        "  Su diseño parece complejo, como el de los mecanismos creados por "
        "los gnomos, sin embargo su propósito parece mucho más siniestro, "
        "fiable y terriblemente eficaz que el de cualquier invención de "
        "los nacidos en la tierra.\n");

    generar_alias_y_plurales();
}
void setup()
{
    fijar_arma_base("hacha doble");
    set_name("triturador");
    setup_apagado();
    fijar_genero(1);

    nuevo_efecto_basico("desssgarradora", 20);
    add_static_property("nivel_minimo", 29);

    setup_limitado_gloria();
}
/************************************************************************************
 * SECCIÓN: Lógica encendido / apagado / combustible
 ***********************************************************************************/
int dame_encendido()
{
    return encendido;
}
int dame_combustible()
{
    return query_property("combustible");
}
int ajustar_combustible(int i)
{
    int aux = dame_combustible() + i;

    aux = min(({MAXIMO_COMBUSTIBLE, aux}));
    aux = max(({0, aux}));

    add_property("combustible", aux);
    return 1;
}
varargs void heart_beat()
{
    if (!dame_encendido()) {
        set_heart_beat(0);
        return;
    }

    if (dame_combustible() < 1)
        apagar();

    ajustar_combustible(-COSTE_HB);
}
/************************************************************************************
 * SECCIÓN: Acciones
 ***********************************************************************************/
varargs int apagar()
{
    object pj;

    if (!encendido)
        return notify_fail(
            query_short() + " ya está apagad" + dame_vocal() + ".\n");

    pj = TP || environment();

    tell_object(
        pj,
        "El rugido de " + TU_S(TO) +
            " se apaga lentamente a medida "
            "que la fuerza mecánica que mueve sus dientes comienza a "
            "extinguirse.\n");
    tell_accion(
        environment(pj),
        "El rugido de " + dame_articulo() + " " + query_short() + " de " +
            pj->query_cap_name() +
            " comienza a extinguirse a medida que sus dientes "
            "se detienen.\n",
        "",
        ({pj}),
        pj);

    encendido = 0;
    setup_apagado();
    return 1;
}
varargs int encender()
{
    if (encendido)
        return notify_fail(
            "¡" + query_short() + " ya está activad" + dame_vocal() + "!\n");

    if (!dame_combustible())
        return notify_fail("No hay combustible en " + TU_S(TO) + ".\n");

    tell_object(
        TP,
        "Tiras con fuerza de la cadena de " + TU_S(TO) +
            " haciendo que el "
            "maquiavélico mecanismo de su interior comience a hacer girar sus "
            "dientes "
            "de metal.\n");
    tell_accion(
        environment(TP),
        TP->query_cap_name() + " tira con fuerza de la cadena de " + SU_S(TO) +
            " "
            "el maquiavélico mecanismo de " +
            dame_este(1) +
            " comience a hacer girar "
            "sus dientes de metal.\n",
        "",
        ({TP}),
        TP);

    encendido = time();
    setup_encendido();
    return 1;
}
int es_carbon(object b)
{
    return -1 != member_array(
                     base_name(b),
                     ({BMINERALES + "carbon", BMINERALES + "carbon_vegetal"}));
}
varargs int alimentar(object carbon)
{
    if (!carbon || !es_carbon(carbon)) {
        return notify_fail(
            "Lo que intentas insertar no es una pieza válida de carbón.\n");
    }

    tell_object(
        TP, "Alimentas " + TU_S(TO) + " con " + carbon->query_short() + ".\n");
    tell_accion(
        environment(TP),
        TP->query_cap_name() + " alimenta " + SU_S(TO) + " con " +
            carbon->query_short() + ".\n",
        "",
        ({TP}),
        TP);

    ajustar_combustible(carbon->dame_peso_puro());
    carbon->dest_me();
    return 1;
}
void init()
{
    ::init();

    anyadir_comando_equipado(
        "alimentar",
        query_name() +
            " con <objeto:no_interactivo:yo'Pieza de carbón a insertar'>",
        (
            : alimentar:),
        "Alimenta " + TU_S(TO) +
            " con un pedazo de carbón, que pasará a ser sumado "
            "a su reserva de combustible.");
    anyadir_comando_equipado(
        "activar",
        query_name(),
        (
            : encender:),
        "Activa " + TU_S(TO) +
            ", haciendo que sus dientes metálicos comiencen a girar "
            "a costa de consumir combustible.");
    anyadir_comando_equipado(
        "desactivar",
        query_name(),
        (
            : apagar:),
        "Desactiva " + TU_S(TO) +
            ", deteniendo sus habilidades especiales y el "
            "consumo de combustible.");
}
/************************************************************************************
 * SECCIÓN: Modificadores a talar
 ***********************************************************************************/
int dame_bono_talar()
{
    return dame_encendido() ? BONO_TALAR : 0;
}
int dame_bono_critico_talar()
{
    return dame_encendido() ? BONO_CRITICO_TALAR : 0;
}
string *dame_mensajes_talar(object quien, int pupa)
{
    if (!dame_encendido())
        return 0;

    switch (pupa) {
        // Pifia
        // case -2:
        // Caída de árbol

        // caida
        case 2:
            return random(3)
                       ? 0
                       : ({({"Hundes el filo de " + TU_S(TO) +
                                 " en un árbol, haciendo que su "
                                 "corteza se vea desmenuzada por el incesante "
                                 "y terrible giro "
                                 "de tu malintencionada herramienta.",
                             quien->query_cap_name() + " hunde el filo de " +
                                 SU_S(TO) +
                                 " en un "
                                 "árbol haciendo que su "
                                 "corteza se vea desmenuzada por el incesante "
                                 "y terrible giro "
                                 "de tal malintencionada herramienta."})});

        // Otros
        case 1:
            return random(2) ? 0
                             : ({
                                   "¡Los afilados dientes de " + TU_S(TO) +
                                       " destrozan, trituran "
                                       "y pulverizan el tronco del árbol que "
                                       "talabas, quien sucumbe "
                                       "ante la irresistible fuerza de tu "
                                       "maquiavélica invención!",
                                   "¡Los afilados dientes de " +
                                       dame_articulo() + " " + query_short() +
                                       " de " + quien->query_cap_name() +
                                       " "
                                       "destrozan, trituran "
                                       "y pulverizan el tronco de un árbol, "
                                       "que sucumbe "
                                       "ante la irresistible fuerza de "
                                       "semejante maquiavélica invención!",
                               });
        default:
            return 0;
    }
}
/************************************************************************************
 * SECCIÓN: miscelánea
 ***********************************************************************************/
string descripcion_juzgar(object pj)
{
    return capitalize(dame_articulo()) + " " + query_short() +
           " puede ser alimentado "
           "con carbón para poner en marcha un intricado mecanismo que hará "
           "que éste "
           "se convierta en una viciosa bestia de dientes de metal. Mientras "
           "el combustible "
           "perdure, el arma recibirá los siguientes bonificadores: \n"
           " - +" +
           BONO_ENCANTAMIENTO +
           " encantamiento.\n"
           " - +" +
           BONO_TALAR +
           " de bono a %^CURSIVA%^talar%^RESET%^.\n"
           " - +" +
           (BONO_CRITICO_TALAR / 100) +
           "% de bono crítico a %^CURSIVA%^talar%^RESET%^.\n"
           " - Ganará el efecto MachacaArmaduras (" +
           BONO_EFECTO_TXT + ").";
}
void objeto_desequipado()
{
    apagar();
}
string long(string objeto_mirado, int oscuridad)
{
    string tx = ::long(objeto_mirado, oscuridad);
    int    porcen;

    porcen = dame_combustible() * 100 / MAXIMO_COMBUSTIBLE;

    if (TP->dame_consentir("accesibilidad"))
        return tx + "Nivel de combustible: " + dame_combustible() + "/" +
               MAXIMO_COMBUSTIBLE + " + (" + porcen + " %)\n";

    return tx + "Nivel de combustible: [" + barra(porcen) + "] (" + porcen +
           " %)\n";
}
