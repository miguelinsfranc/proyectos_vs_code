// Satyr 28.03.2017
#include <pk.h>

#define EVASION   30
#define DURACION  18
#define BLOQUEO   180

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("capa");
    set_name("habitos");
    
    set_short("%^RED%^H%^CYAN%^ábito del %^RED%^M%^CYAN%^aestro%^RESET%^");
    set_main_plural("%^RED%^H%^CYAN%^ábitos del %^RED%^M%^CYAN%^aestro%^RESET%^");
    generar_alias_y_plurales();
    
    add_static_property("des", 1);
    
    set_long(
        "Una capa andrajosa de un color ocre que mancilló al blanco níveo que tenía "
            "en su época más esplendorosa. Un pequeño pasador de bronce acuñado con un "
            "sol repleto de óxido sirve para cerrarla sobre el cuerpo, desvelando así "
            "una larga cola que se arrastra por el suelo. Su parte posterior está "
            "engalanada con un calado del mismo astro, símbolo marcial "
            "cuyo significado a día de hoy se ha perdido tras décadas de conflicto "
            "alimentado por el odio de las guerras religiosas.\n"
    );

    fijar_encantamiento(20);
//    fijar_coste_gloria(400);
//    fijar_valor(4000);
    setup_limitado_gloria();
}
void objeto_desequipado() {
    if ( environment() )
        environment()->quitar_efecto("_evasion_tmp_habitos_maestro");
}
int poner_evasion() {   
    if ( TP->dame_bloqueo_combate(TO) )
        return notify_fail(
            "Tu cuerpo no podría someterse de nuevo al poder de " + query_short() + ".\n"
        );
        
    if ( dame_bloqueo_combate(TO) )
        return notify_fail(
            "El poder mágico de " + query_short() + " aún no se ha recuperado.\n"
        );
        
        
    nuevo_bloqueo_combate(TO, BLOQUEO);
    TP->nuevo_bloqueo_combate(TO, BLOQUEO);
    TP->ajustar_carac_tmp(
        "evasion",
        EVASION,
        DURACION,
        "habitos_maestro"
    );  

    tell_object(
        TP, 
        "Despliegas súbitamente " + TU_S(TO) + ", revelando tus brazos extendidos y una celeridad "
            "mágica recién adquirida.\n"
    );
    tell_accion(environment(TP),
        TP->query_cap_name() + " despliega súbitamente su " + TU_S(TO) + ", revelando sus "
            "brazos extendidos y moviéndose con una inusitadada celeridad recién adquirida.\n",
        "",
        ({TP}),
        TP
    );
    
    return 1;
}
string descripcion_juzgar(object pj) {
    return "Activar el poder de este hábito te otorgará un bonificador de " + EVASION + " puntos de "
        "evasión durante " + pretty_time(DURACION) + ". El bloqueo de éste efecto es de " 
        + pretty_time(BLOQUEO) + "."
    ;
}
void init() {
    anyadir_comando_equipado(
        "desplegar",
        "habito del maestro",
        (: poner_evasion :),
        descripcion_juzgar(TO)
    );
    ::init();    
}
