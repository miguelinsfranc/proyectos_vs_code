// Satyr 15/10/2019 01:05
#include <pk.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";
void setup()
{
    fijar_armadura_base("capa");
    set_name("estola");

    set_short("%^GREEN%^E%^BLACK%^BOLD%^stola del "
              "%^GREEN%^NOBOLD%^J%^BLACK%^BOLD%^iem-%^GREEN%^NOBOLD%^N%^BLACK%^"
              "BOLD%^etyer%^RESET%^");
    set_main_plural("%^GREEN%^E%^BLACK%^BOLD%^stola del "
                    "%^GREEN%^NOBOLD%^J%^BLACK%^BOLD%^iem-%^GREEN%^NOBOLD%^N%^"
                    "BLACK%^BOLD%^etyer%^RESET%^");
    generar_alias_y_plurales();

    add_static_property("poder_magico", 20);
    add_static_property("negacion-aliento", 5);
    add_static_property("negacion-veneno", 5);
    add_static_property("negacion-enfermedad", 5);
    add_static_property("habilidad", "bendicion de jarpek");

    set_long(
        "Una pieza ritual utilizada por los antecesores de las tribus nómadas "
        "que moraban en los desiertos de Galador. Las sacerdotisas de su "
        "religión, conocidas como Hem-Netyer (si las traducciones de los "
        "eruditos tienen credibilidad), vestían esta prenda ritual para "
        "ceremoniar ofrendas y traer la bienaventura a su congregación.\n\n"

        "  Este ornamento es similar a una bufanda, pues se lleva colgando del "
        "cuello hacia adelante, haciendo que los escritos que recorren su "
        "superficie rodeen el rostro de la sacerdotisa.\n\n"

        "  La pieza es negra, salvo en algunas partes centrales donde el verde "
        "hace acto de presencia con salpicaduras erráticas. Sin duda, la "
        "decoloración es fruto del tiempo y otrora la pieza sería muy "
        "diferente.\n\n"

        "  Lo que el tiempo no ha podido erosionar, sea por capricho o por "
        "motivos más ultraterrenales, son los bordados que recorren su "
        "superficie, decorando con pictogramas de antiguos dioses el recorrido "
        "de la pieza.\n");
    fijar_encantamiento(20);
    setup_limitado_gloria();
}
void objeto_desequipado()
{
    if (environment()) {
        environment()->quitar_efecto("bendicion de jarpek");
    }
}
