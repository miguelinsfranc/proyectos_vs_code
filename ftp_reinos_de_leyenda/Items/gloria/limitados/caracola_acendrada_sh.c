// Satyr 21/08/2018 12:59

#include <clean_up.h>

object _pj;
float  _porcentaje;

void create(object pj, float porcentaje)
{
    if (!clonep(TO)) {
        return;
    }

    _pj         = pj;
    _porcentaje = porcentaje;

    shadow(_pj, 1);
    pj->add_extra_look(TO);
}
int dame_sombra_caracola_acendrada()
{
    return 1;
}
void destruir_sombra_caracola_acendrada()
{
    destruct(TO);
}
void destructor()
{
    if (!_pj) {
        return;
    }

    _pj->remove_extra_look(TO);
    tell_object(_pj, "El aura azulada que te rodeaba se disipa lentamente.\n");
}
int ajustar_pvs(int i, object causante)
{
    if (i < 1) {
        return _pj->ajustar_pvs(i, causante);
    }

    return _pj->ajustar_pvs(i + to_int(i * _porcentaje / 100), causante);
}
string extra_look(object pj)
{
    string tx = _pj->extra_look(pj) || "";

    if (pj == TO) {
        return tx + "El aura de la caracola acendrada te rodea.\n";
    }

    return tx + "El aura de la caracola acendrada " + pj->dame_le() +
           " rodea.\n";
}
