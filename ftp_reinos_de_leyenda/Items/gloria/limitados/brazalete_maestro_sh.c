// Satyr 24.01.2017
#include <clean_up.h>

private object pj, brazalete;
private int bono;

int dame_sombra_brazalete_maestro() {
    return 1;
}
int dame_shadow_brazalete_maestro() {
    return 1;
}
void destruir_shadow_brazalete_maestro() {
    if ( pj )
        tell_object(pj, "Sientes que tu pericia en combate disminuye súbitamente.\n");
        
    destruct(TO);
}
void destruir_sombra_brazalete_maestro() {
    destruir_shadow_brazalete_maestro();
}
void setup_sombra(object _pj, object _brazalete, int _bono) {
    pj        = _pj;
    brazalete = _brazalete;
    bono      = _bono;
    
    shadow(pj, 1);
    
    tell_object(pj, "Una sensación de euforia te rodea al sentirte, de repente, mucho más diestro.\n");
}
mapping dame_maestrias() {
    mapping mp = copy(pj->dame_maestrias());
    string *fami = "/table/bd_armas.c"->dame_familias_str();
    
    
    foreach(string tipo in (keys(mp) | fami) ) {
                mp[tipo] = min(({100, mp[tipo] + bono}));
    }
        
        
    return mp;
}
int dame_maestria(mixed aux) {
    int i = pj->dame_maestria(aux);
    
    return min(({100, i + bono}));
}