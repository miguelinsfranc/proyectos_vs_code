// Satyr 17/10/2019 12:28
#include <pk.h>
#include <combate.h>
#include <depuracion.h>
#include <baseobs_path.h>

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";
inherit "/obj/magia/objeto_con_poderes";

/*******************************************************************************
 * SECCIÓN: Prototipos y variables
 ******************************************************************************/
#ifndef BELEMENTALES
#define BELEMENTALES BMONSTRUOS + "elemental_"
#endif

#define DURACION_INVOCACION 90
#define PODER_MAGICO 30
#define NEGACION_ARTEFACTO 3
#define NEGACION_CONJURO 3
#define BLOQUEO 250
#define PROP_CONTROL "bonos-" + __FILE__

#define PODER_ATRAPAR "aprisionar elemental"
#define PODER_LIBERAR "liberar elemental"

string dame_ruta_elemental_encerrado();

/*******************************************************************************
 * SECCIÓN: Funciones habituales de objetos
 ******************************************************************************/
void setup()
{
    fijar_armadura_base("anillo");
    set_name("prision");
    set_short("%^RED%^P%^BOLD%^rision %^NOBOLD%^CYAN%^E%^BOLD%^lemental de "
              "%^BLACK%^BOLD%^V%^RESET%^ulgur");
    set_main_plural(
        "%^RED%^P%^BOLD%^risiones %^NOBOLD%^CYAN%^E%^BOLD%^lementales de "
        "%^BLACK%^BOLD%^V%^RESET%^ulgur");

    generar_alias_y_plurales();

    set_long(
        "Este poderoso artefacto contiene en su interior el núcleo de un "
        "arma antimágica que el sacerdote de Velian Vulgur nunca llegó a "
        "acabar. Su poder era tal que permitía manipular las energías "
        "elementales, "
        "robándolas de su plano de origen para luego subyugarlas "
        "bajo la voluntad del portador del objeto.\n\n"

        "  El arma se destruyó en un desafortunado experimento, junto con el "
        "Sacerdote que la creó, pero parte de su poder se encerró en este "
        "curioso anillo de cristal que está coronado por una caja opaca "
        "compuesta por varias piezas que dejan escapar un fulgor blanquecino "
        "entre sus rendijas.\n\n"

        "  Tras varios intentos de replicar el arma poco fortuitos, los "
        "arcanócratas de Ar'Kaindia decidieron darse por vencidos para evitar "
        "un desastre mayor, no sin antes conseguir utilizar parte del poder "
        "del arma original para otros fines.\n");
    set_read_mess(
        "El pueblo Orgo te agradecerá por siempre tus avances tecnológicos, "
        "Vulgur. Aunque tu cuerpo se ha perdido en otra dimensión, tu legado y "
        "parte de tus experimentos aún perduran. Que este anillo ritual sirva "
        "para recordar por siempre tu memoria.",
        "ogro");

    fijar_material(5);
    fijar_encantamiento(20);
    add_static_property("poder_magico", -10);

    add_static_property(
        "clase",
        ({"velian",
          "hechicero",
          "mago-ladron",
          "khaol",
          "mago_runico",
          "bardo"}));

    setup_limitado_gloria();
    fijar_bloqueo_poderes(BLOQUEO, PODER_ATRAPAR);
    fijar_bloqueo_poderes(6, PODER_LIBERAR);
    add_extra_look(TO);
}
string extra_look(object b)
{
    if (dame_ruta_elemental_encerrado()) {
        return "Hay un elemental encerrado en su interior.\n";
    }
    return 0;
}
string descripcion_juzgar(object b)
{
    return sprintf(
        "Los restos latentes del arma de Vulgur encerrados en este anillo "
        "permitirán a su portador absorber las energías de cualquier "
        "elemental, encerrándolo así en su interior para que su portador "
        "reciba una bonificación de %s a su poder mágico, %s a sus tiradas de "
        "negación de 'artefacto' y %s a sus tiradas de negación de "
        "'conjuro' mientras éste permanezca en su interior.\n\n"

        " El elemental tendrá derecho a una tirada de salvación contra "
        "'artefacto' sin modificar. El bloqueo de esta habilidad es de %s.\n\n"

        " El portador, en cualquier momento, podrá liberar al elemental de la "
        "prisión, que pasará a obedecer a su portador durante %s antes de "
        "desaparecer.",
        "+" + PODER_MAGICO,
        "+" + NEGACION_ARTEFACTO,
        "+" + NEGACION_CONJURO,
        pretty_time(BLOQUEO),
        pretty_time(DURACION_INVOCACION));
}
void ajustar_bonos(object pj, int signo)
{
    if (signo > 0 && pj->query_static_property(PROP_CONTROL)) {
        return;
    }

    if (signo < 1 && !pj->query_static_property(PROP_CONTROL)) {
        return;
    }

    if (signo > 0) {
        pj->ajustar_carac_tmp("poder_magico", PODER_MAGICO);
        pj->add_static_property(PROP_CONTROL, 1);
        return;
    }

    pj->ajustar_carac_tmp("poder_magico", -PODER_MAGICO);
    pj->remove_static_property(PROP_CONTROL);
}
void objeto_equipado()
{
    if (dame_ruta_elemental_encerrado()) {
        ajustar_bonos(environment(), 1);
    }
}
void objeto_desequipado()
{
    if (dame_ruta_elemental_encerrado()) {
        ajustar_bonos(environment(), -1);
    }
}
/*******************************************************************************
 * SECCIÓN: Sobrecarga chequeos sencillos
 ******************************************************************************/
/**
 * Los chequeos de objetivo para "atrapar" son normales.
 *
 * En el caso de liberar no hacemos chequeo de objetivo normal y usamos
 * lógica propia.
 */
varargs string
chequeo_sencillo_objetivo(object jugador, object obj, string poder)
{
    object b;

    if (poder != PODER_LIBERAR) {
        return ::chequeo_sencillo_objetivo(jugador, obj, poder);
    }

    if (!dame_ruta_elemental_encerrado()) {
        return "No hay ningún elemental encerrado en " + query_short() + ".";
    }

    if (TP->query_property("criatura_invocada")) {
        return "Ya tienes a alguna criatura bajo tu control, lo que te impide "
               "liberar al elemental que encierra " +
               TU_S(TO) + ".";
    }

    if (!b = load_object(dame_ruta_elemental_encerrado())) {
        return "Un error te impide liberar las fuerzas que encierra " +
               TU_S(TO) + ".";
    }

    return 0;
}
/**
 * El chequeo de liberar no necesita hacer comprobaciones de entorno.
 */
varargs string
chequeo_sencillo_entorno(object jugador, object obj, string poder)
{
    if (poder != PODER_LIBERAR) {
        return ::chequeo_sencillo_entorno(jugador, obj, poder);
    }

    return 0;
}
/*******************************************************************************
 * SECCIÓN: Encierro elemental
 ******************************************************************************/
string dame_ruta_elemental_encerrado()
{
    return query_property("ruta_elemental_encerrado");
}
int fijar_ruta_elemental_encerrado(string path)
{
    if (!path) {
        remove_property("ruta_elemental_encerrado");
        return 1;
    }

    if (file_size(path) < 1 || !load_object(path)) {
        return 0;
    }

    add_property("ruta_elemental_encerrado", path);
    return 1;
}
string chequeo_encerrar_elemental(object b, object causante, string poder)
{
    if (!b) {
        return "¡Parece que tu objetivo se ha escapado!";
    }

    if (!b->dame_elemental()) {
        return capitalize(dame_tu()) + " " + query_short() +
               " solo puede encerrar a elementales.";
    }

    if (base_name(b)[0..sizeof(BELEMENTALES) -1] != BELEMENTALES) {
        return "El poder mágico que anima a " + b->query_short() +
               " es demasiado grande como para ser contenido en " + dame_tu() +
               " " + query_short() + ".";
    }

    return 0;
}

int do_aprisionar(object quien, object victima, string poder)
{
    string err;
    object env;

    __debug_x(sprintf("do_aprisionar: %O %O %O", quien, victima, poder));

    if (err = chequeo_previo_finalizar_poder(quien, victima, poder)) {
        tell_object(quien, err + "\n");
        return 0;
    }

    if (victima->dame_ts("artefacto", 0, 0, base_name(), quien)) {
        log_artefacto(sprintf(
            "%s resiste la TS de %s",
            victima->query_name(),
            quien->query_name()));
        mostrar_mensajes_poderes("ts_resistida", quien, victima, poder);
        return 0;
    }

    if (!fijar_ruta_elemental_encerrado(base_name(victima) + ".c")) {
        mostrar_mensajes_poderes("error_almacenaje", quien, victima, poder);
        return 0;
    }

    if (env = environment(victima)) {
        all_inventory(victima)->move(env);
    }

    ajustar_bonos(quien, 1);
    call_out(( : destruct($(victima)) :), 0);
    mostrar_mensajes_poderes("captura", TP, victima);
    log_artefacto(sprintf(
        "%s, en %s, aprisiona a %s (%s)",
        quien->query_cap_name(),
        base_name(environment(quien)),
        base_name(victima),
        !victima->dame_invocador() ? "0"
                                   : victima->dame_invocador()->query_name()));
    return 1;
}
int aprisionar(object b)
{
    string err;

    if (!chequeo_activacion_poder(TP, b, PODER_ATRAPAR)) {
        return 0;
    }

    if (dame_ruta_elemental_encerrado()) {
        return notify_fail(sprintf(
            "%s %s ya encierra un elemental en su interior y tendrás que "
            "liberarlo antes de intentar aprisionar a otro.\n",
            capitalize(dame_tu()),
            query_short()));
    }

    if (err = chequeo_encerrar_elemental(b, TP, PODER_ATRAPAR)) {
        return notify_fail(err + "\n");
    }

    mostrar_mensajes_poderes("activacion", TP, b, PODER_ATRAPAR);
    poner_bloqueos(TP, b, PODER_ATRAPAR);

    // clang-format off
    call_out(
        "finalizacion_poder",
        2 + (TP->query_hb_counter() % 2) * 2,
        TP,
        b,
        0,
        (: do_aprisionar:),
        0);
    // clang-format on
    return 1;
}
int finalizar_control(object quien, object elemental)
{
    if (quien) {
        if (elemental == quien->query_property("criatura_invocada")) {
            quien->remove_static_property("criatura_invocada");
        }
    }

    if (elemental) {
        elemental->fijar_invocador(0);
        elemental->destruir_elemental();
    }
}
varargs int do_liberar(object ejecutor, object victima, string poder)
{
    string err;
    object b;

    if (!ejecutor) {
        return 0;
    }

    if (err = chequeo_previo_finalizar_poder(ejecutor, victima, poder)) {
        tell_object(ejecutor, err + "\n");
        return 0;
    }

    // clang-format off
    b = clone_object(dame_ruta_elemental_encerrado());
    b->fijar_invocador(ejecutor);
    ejecutor->add_static_property("criatura_invocada", b);
    b->move(environment(ejecutor));
    ejecutor->nuevo_efecto(
        "invocar elemental",
        DURACION_INVOCACION,
        (: finalizar_control, ejecutor, b:)
    );
    // clang-format on

    fijar_ruta_elemental_encerrado(0);
    ajustar_bonos(ejecutor, -1);
    mostrar_mensajes_poderes("liberar_elemental", ejecutor, b, PODER_LIBERAR);

    log_artefacto(sprintf(
        "%s, en %s, libera a %s",
        ejecutor->query_cap_name(),
        base_name(environment(ejecutor)),
        base_name(b), ));

    return 1;
}
int liberar()
{
    if (!chequeo_activacion_poder(TP, 0, PODER_LIBERAR)) {
        return 0;
    }

    mostrar_mensajes_poderes("activacion", TP, 0, PODER_LIBERAR);
    poner_bloqueos(TP, 0, PODER_LIBERAR);

    // clang-format off
    call_out(
        "finalizacion_poder",
        2 + (TP->query_hb_counter() % 2) * 2,
        TP,
        0,
        PODER_LIBERAR,
        (: do_liberar:),
        0
    );
    // clang-format on
    return 1;
}
void init()
{
    ::init();

    // clang-format off
    anyadir_comando_equipado(
        "aprisionar",
        "<objeto:pnj:aqui:primero'Elemental a atrapar'>",
        (: aprisionar($1) :),
        "Intenta encerrar a una criatura elemental en tu " + query_short() + "."
        " La criatura tendrá derecho a una tirada de salvación de 'artefacto' "
        "para evitar sus efectos."
    );
    anyadir_comando_equipado(
        "liberar",
        "elemental",
        (: liberar() :),
        "Liberará un elemental encerrado en " + query_short() + ". Éste "
        "te obedecerá durante " + pretty_time(DURACION_INVOCACION) + " antes "
        "de volverse en tu contra o desaparecer."
    );
    // clang-format on
}
/*******************************************************************************
 * SECCIÓN: Mensajes
 ******************************************************************************/
string *dame_mensajes_captura(object quien, object elemental, string poder)
{
    return ({
        sprintf(
            "%s¡%s %s se abre, exponiendo una esfera de energía que comienza a "
            "absorber la figura de %s a medida que atomiza la misma "
            "esencia que le da existencia en este plano!, no pasa mucho "
            "hasta que su figura desaparece por completo, momento en el "
            "que tu artefacto mágico se cierra súbitamente.",
            _C_GOLPEAR,
            capitalize(dame_tu()),
            query_short(),
            elemental->query_short(), ),
        "",
        sprintf(
            "¡%s %s de %s se abre de repente, exponiendo una esfera de "
            "energía que comienza a absorber la figura de %s a medida que la "
            "esencia que le da existencia en este plano se atomiza!, no pasa "
            "mucho hasta que su figura desaparece por completo, momento en el "
            "que el sorprendente artefacto mágico se cierra.",
            capitalize(dame_articulo()),
            query_short(),
            quien->query_cap_name(),
            elemental->query_short()),
        "Un breve estallido sónico retumba en la zona.",
    });
}
string
dame_mensajes_error_almacenaje(object quien, object victima, string poder)
{
    return "Un error te impide capturar el poder de " + victima->query_short() +
           ".";
}

string *
dame_mensajes_liberar_elemental(object quien, object elemental, string poder)
{
    return ({sprintf(
                 "%s¡%s %s emite un ensordecedor estallido cuando libera de "
                 "su interior a %s %s, que, a duras penas, parece someterse a "
                 "tus órdenes!",
                 _C_GOLPEAR,
                 capitalize(dame_tu()),
                 query_short(),
                 elemental->dame_numeral(),
                 elemental->query_short()),
             "",
             sprintf(
                 "¡%s %s de %s emite un ensordecedor estallido cuando "
                 "libera de su interior a %s %s que, a duras penas, parece "
                 "seguir las órdenes de su invocador!",
                 capitalize(dame_articulo()),
                 query_short(),
                 quien->query_cap_name(),
                 elemental->dame_numeral(),
                 elemental->query_short()),
             "Escuchas un tremendo estallido mágico."});
}
