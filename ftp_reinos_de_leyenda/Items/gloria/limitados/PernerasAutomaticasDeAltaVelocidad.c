// Satyr 10/03/2020 02:40
// Eckol Jun20: bajona se llamaba por error a si misma al final de ajustar_carac
#include <depuracion.h>
#include <combate.h>
#include <pk.h>

#define DURACION_OK 25
#define DURACION_KO 60
#define BONO_OK 1
#define BONO_KO -4
#define BLOQUEO 400
#define PODER_CORRER "SobrecargaDeMovimiento"

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";
inherit "/obj/magia/objeto_con_poderes";

/*******************************************************************************
 * SECCIÓN: Funciones habituales de objetos
 ******************************************************************************/
void setup()
{
    set_name("perneras");
    fijar_armadura_base("grebas de malla");
    set_short("%^BLACK%^BOLD%^P%^RESET%^erneras%^BLACK%^BOLD%^A%^RESET%^"
              "utomáticas%^BLACK%^BOLD%^D%^RESET%^e%^BLACK%^BOLD%^A%^RESET%^"
              "lta%^BLACK%^BOLD%^V%^RESET%^elocidad");
    set_main_plural("pares de "
                    "%^BLACK%^BOLD%^P%^RESET%^erneras%^BLACK%^BOLD%^A%^RESET%^"
                    "utomáticas%^BLACK%^BOLD%^D%^RESET%^e%^BLACK%^BOLD%^A%^"
                    "RESET%^lta%^BLACK%^BOLD%^V%^RESET%^elocidad");
    crear_alias("perneras automáticas de alta velocidad");
    crear_plurales("pares de perneras automáticas de alta velocidad");

    set_long(
        "Un par de perneras metálicas donde cada una de las espinilleras "
        "está compuesta por una única pieza de metal que usa unas bisagras "
        "y un conjunto de candados para cerrarse sobre su portador.\n\n"

        "Su interior está repleto de cadenas, engranajes, alguna que otra "
        "parte brillante y un buen puñado de agujas muy afiladas cuyo fin "
        "no tienes muy claro.\n");

    fijar_encantamiento(40);
    setup_limitado_gloria();

    fijar_bloqueo_poderes(BLOQUEO, PODER_CORRER);
    add_extra_look(TO);

    add_static_property("minimo_atributos", (["des":13, "int":12]));
    fijar_peso(15000);
}
string descripcion_juzgar(object pj)
{
    return "Estas perneras, inspiradas por las partes hidráulicas de los "
           "constructos, poseen un intrincado mecanismo que puede activarse "
           "para sobre estimular los músculos de su portador con un cóctel de "
           "descargas eléctricas y afiladas micro agujas.\n\n"

           " Al hacerlo, su portador gozará de un beneficio temporal de +" +
           BONO_OK + " puntos de movimiento que se extinguirá pasados " +
           pretty_time(DURACION_OK) +
           ", momento en el que dicho bono será "
           "reemplazado por un penalizador de " +
           BONO_KO + " que durará " + pretty_time(DURACION_KO) + ".";
}
void objeto_desequipado()
{
    environment()->quitar_efecto("_movimiento_tmp_" + PODER_CORRER);
}
/*******************************************************************************
 * SECCIÓN: Auxiliares
 ******************************************************************************/
string extra_look(object b)
{
    if (dame_objeto_con_bloqueo(PODER_CORRER)) {
        return "Su superficie está al rojo blanco tras haber sido "
               "sobrecargadas recientemente.\n";
    }

    return "";
}
/*******************************************************************************
 * SECCIÓN: Poderes
 ******************************************************************************/
int bajona(object quien)
{
    if (!quien) {
        return 0;
    }

    // clang-format off
    mostrar_mensajes_poderes("bajona", quien);
    quien->ajustar_carac_tmp(
        "movimiento",
        BONO_KO,
        DURACION_KO,
        PODER_CORRER,
    );
    // clang-format on

    return 1;
}
int fin_sobrecargar(object quien, object victima, string poder)
{
    string err;

    if (err = chequeo_previo_finalizar_poder(quien, victima, poder)) {
        tell_object(quien, err + "\n");
        return 0;
    }

    poner_bloqueos(TP, 0, PODER_CORRER);
    mostrar_mensajes_poderes("sobrecarga", quien);
    quien->ajustar_carac_tmp(
        "movimiento",
        BONO_OK,
        DURACION_OK,
        PODER_CORRER,
        (: bajona($(quien)):)
    );

    return 1;
}
int sobrecargar()
{
    if (!chequeo_activacion_poder(TP, TP, PODER_CORRER)) {
        return 0;
    }

    return finalizacion_poder(TP, TP, PODER_CORRER, ( : fin_sobrecargar:), 0);
}
void init()
{
    ::init();

    // clang-format off
    anyadir_comando_equipado(
        "sobrecargar",
        "movimiento",
        (: sobrecargar :),
        "Activa el cuasi-diabólico mecanismo de estas perneras.\n"
    );
    // clang-format on
}

/*******************************************************************************
 * SECCIÓN: Mensajes
 ******************************************************************************/
string *dame_mensajes_bajona(object quien)
{
    return (
        {"El infernal sonido que emitían " + TU_S(TO) +
             " cesa de súbito y "
             "con él se va el vigor que se apoderaba de ti, haciendo que tus "
             "piernas se sientan mucho más pesadas que antes.",
         0,
         "El infernal sonido que emitían las " + query_short() + " de " +
             quien->query_cap_name() + " cesa de súbito."});
}
string *dame_mensajes_sobrecarga(object quien)
{
    return (
        {"Tiras varias veces de la gruesa cadena que sobresale de " + TU_S(TO) +
             " haciendo que éstas, súbitamente, comienzan a rugir y echar humo "
             "cuando sus mecanismos internos empiezan a funcionar y hacen que "
             "punzantes descargas eléctricas y afiladas mini agujas se claven "
             "en tus músculos, ¡llenándote de vigor!",
         0,
         quien->query_cap_name() +
             " tira varias veces de una gruesa cadena "
             "que sobresale de " +
             SU_S(TO) +
             " haciendo que éstas, súbitamente, "
             "comiencen a rugir y emitir una densa humareda negra."});
}
