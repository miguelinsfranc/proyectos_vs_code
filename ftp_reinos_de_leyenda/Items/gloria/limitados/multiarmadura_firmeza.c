// Satyr 28.03.2017
#include <pk.h>

#define BLOQUEO    300
#define DURACION   200

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/multiobjetos/multiarmadura";

int no_catalogar_armeria() {
    return multiarmadura::no_catalogar_armeria();
}
void setup() {
    string firm = "de la %^ORANGE%^F%^BLACK%^BOLD%^irmeza%^RESET%^";
    
    fijar_titulos(firm, firm);
    set_short("%^ORANGE%^M%^BLACK%^BOLD%^ultiarmadura " + firm);
    set_main_plural("%^ORANGE%^M%^BLACK%^BOLD%^ultiarmaduras " + firm);

    fijar_long_generico(
        "Una pesadísima coraza construida de un mineral flexible cuyo origen incierto "
            "solo puede atribuirse a la magia. Su pechera porosa está salpicada de un moho "
            "que se acumula en las oquedades de la gran runa que adorna su superficie.\n"
    );
    
    fijar_encantamiento(40);
    
//    fijar_coste_gloria(195);
//    fijar_valor(4000);
    setup_limitado_gloria();
}
void setup_generico_multiobjeto() {
    ::setup_generico_multiobjeto();
    
    fijar_tipos_validos(({"mallas", "completa"})); 
}
void setup_especifico() {
    fijar_material(6);
    fijar_peso(to_int(dame_peso() * 1.4));
}
void poner_descripcion_especifica_multiobjeto() {
    set_long(
        "Una pesadísima coraza construida de un mineral flexible cuyo origen incierto "
            "solo puede atribuirse a la magia. Su pechera porosa está salpicada de un moho "
            "que se acumula en las oquedades de la gran runa que adorna su superficie y dota "
            "de poder a la pieza.\n\n"
    );
}

void objeto_desequipado() {
    if ( environment() )
        environment()->quitar_efecto("firmeza");
}
void quitar_firmeza(object pj) {
    if ( ! pj || ! pj->dame_habilidad_temporal("cabeza dura") )
        return;
    
    pj->quitar_habilidad_temporal("cabeza dura");
    
    
    tell_object(pj, "El irresistible poder de " + TU_S(TO) + " se dispipa poco a poco.\n");
    tell_accion(
        environment(pj),
        "El poder irresistible de " + dame_articulo() + " " + query_short() + " que "
            "pertenece a " + pj->query_cap_name() + " se disipa poco a poco.\n",
        "",
        ({pj}),
        pj
    );
}
int poner_firmeza() {   
    if ( TP->dame_bloqueo_combate(TO) )
        return notify_fail(
            "Tu cuerpo no podría someterse de nuevo al poder de " + query_short() + ".\n"
        );
        
    if ( dame_bloqueo_combate(TO) )
        return notify_fail(
            "El poder mágico de " + query_short() + " aún no se ha recuperado.\n"
        );
        
        
    nuevo_bloqueo_combate(TO, BLOQUEO);
    TP->nuevo_bloqueo_combate(TO, BLOQUEO);
    TP->nuevo_efecto("firmeza", DURACION, (: quitar_firmeza, TP :));
    TP->nueva_habilidad_temporal("cabeza dura");
    
    tell_object(
        TP, 
        "La runa de " + TU_S(TO) + " comienza a brillar cuando la tocas, llenándote de "
            "un poder embriagador y haciéndote sentir imparable.\n"
    );
    tell_accion(environment(TP),
        TP->query_cap_name() + " toca la runa de " + SU_S(TO) + " y ésta comienza a brillar "
            "cuando insufla un poder irreductible en su portador" + TP->dame_a() + ".\n",
        "",
        ({TP}),
        TP
    );
    
    return 1;
}
string descripcion_juzgar(object pj) {
    return "Activar el poder de ésta armadura otorgará a su portador la habilidad 'cabeza dura' "
        "durante " + pretty_time(DURACION) + ". Éste efecto tiene un bloqueo de "
        + pretty_time(BLOQUEO) + " y puede ser disipado.\n"
    ;
}
void init() {
    anyadir_comando_equipado(
    //anyadir_comando(
        "activar",
        "la runa de la firmeza",
        (: poner_firmeza :),
        descripcion_juzgar(TO)
    );
    //fijar_cmd_visibilidad("kk", "", 1);
    ::init();    
}
