// Satyr 02/03/2020 11:30
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

/*******************************************************************************
 * SECCIÓN: Funciones habituales de objetos
 ******************************************************************************/
void setup()
{
    fijar_armadura_base("cinturon");
    set_name("fajin");
    set_short("%^ORANGE%^F%^BLACK%^BOLD%^ajín del "
              "%^NOBOLD%^ORANGE%^J%^BOLD%^BLACK%^inete "
              "%^RESET%^ORANGE%^D%^BLACK%^BOLD%^escabezado%^RESET%^");
    set_main_plural("%^ORANGE%^F%^BLACK%^BOLD%^ajines del "
                    "%^NOBOLD%^ORANGE%^J%^BOLD%^BLACK%^inete "
                    "%^RESET%^ORANGE%^D%^BLACK%^BOLD%^escabezado%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Existen mitos y leyendas, de esos que dan su sobrenombre a los "
        "Reinos, cuyo principal protagonista es un mítico y recalcitrante "
        "jinete sin cabeza que, a "
        "lomos de un semental color carbón, visita aldeas indefensas en las "
        "noches de conjunción para cosechar las vidas de aquellos incautos que "
        "se encuentren "
        "al alcance de su afilada guadaña.\n\n"

        "  Sin embargo, la historia real de los Reinos está plagada de "
        "crueldades y horrores mayores, por lo que los jovenzuelos "
        "que escuchan el comienzo de este mito no pueden evitar bostezar antes "
        "de preguntarle al cuentacuentos de turno si no se sabe otra con un "
        "poco más de sustancia.\n\n"

        "  Esta opinión no es compartida por aquellos que han visto al apóstol "
        "decapitado trabajar. Para ellos este fajín arrugado de color "
        "naranja les recuerda las horribles carnicerías de este infame "
        "caballero que utiliza los pequeños diamantes que cuelgan de la tela "
        "de su cinturón para encerrar las almas de los que consigue sesgar.\n");

    add_static_property("habilidad", "jinete espectral");
    add_static_property("con", 1);
    fijar_encantamiento(10);
    setup_limitado_gloria();
    ajustar_BP(5);
    ajustar_BO(5);
}
