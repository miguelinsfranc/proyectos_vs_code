
// Satyr 21/08/2018 01:21
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

void setup()
{
    fijar_armadura_base("capucha");

    set_name("pañoleta");
    set_short(
        "%^BOLD%^BLACK%^Pañoleta de "
        "%^RED%^Dyn%^BLACK%^'%^WHITE%^Dahjr%^RESET%^");
    set_main_plural(
        "%^BOLD%^BLACK%^Pañoletas de "
        "%^RED%^Dyn%^BLACK%^'%^WHITE%^Dahjr%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Dyn'Dahjr -del literal \"nuevo orden\", en drow- fue el nombre "
        "bajo el que se identificó un movimiento de los varones de "
        "Tzerneelle'Dol que trató de romper el liderazgo del concilio de "
        "matronas cuando Lloth desapreció de los reinos. Con el rostro oculto "
        "tras pañoletas como la que tienes ante ti, cometían asesinatos "
        "selectivos contra las mujeres más fuertes e importantes de la "
        "sociedad con el fin de hacer caer el castillo de naipes que formaba "
        "la jerarquía drow.\n\n"

        "  Sus atentados pararon de súbito cuando Draifeth -cuya aversión por "
        "los varones solo era superada por la de la propia Lloth- devolvió "
        "el poder a las sacerdotisas y, con su ayuda, derrotó a la rebelión, "
        "tomando a los instigadores como prisioneros para devorar sus "
        "corazones. Colorín y colorado, el %^CURSIVA%^statu quo%^RESET%^"
        " matriarcal volvió a la sociedad drow.\n\n"

        "  Muy pocos sobrevivieron a semejante purga y aquellos que lo "
        "hicieron, "
        "como Khaol Thalack, abandonaron la suboscuridad para escapar de la "
        "cólera de la diosa araña, llevándose con ellos estos símbolos que "
        "otrora utilizaban para representar el advenimiento de un nuevo "
        "orden.\n");

    fijar_BO(25);
    add_static_property("critico", 5);
    add_static_property("no_raza", ({"elfo", "semi-elfo"}));

    setup_limitado_gloria();
}
