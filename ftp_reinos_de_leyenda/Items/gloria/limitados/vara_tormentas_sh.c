// Satyr 23.09.2016
#include <clean_up.h>
#include <depuracion.h>
private object pj, vara;

int dame_shadow_vara_tormentas()        { return 1;     }
void destruir_shadow_vara_tormentas()   { destruct(TO); }
void setup_sombra(object b, object c) {
    pj   = b;
    vara = c;
    shadow(pj, 1);
}
mixed event_spell(
    object hechizo, object lanzador, mixed objetivo, object
    *objetivos_adicionales, object *fuera_alcance, int turno, int inaudible
    ) {
    mixed i;
    
    i = pj->event_spell(hechizo, lanzador, objetivo, objetivos_adicionales, fuera_alcance, turno, inaudible);
    
    if ( hechizo )
        __debug_x("Event_spell (" + turno + "/" + sizeof(hechizo->dame_turnos()) + ")", "vara_tormentas", 0, 1);
    
    if ( ! vara || ! hechizo || turno != sizeof(hechizo->dame_turnos()) || pj != lanzador ) {
        __debug_x("Cancelamos en shadow(1)", "vara_tormentas", 0, 1);
        return i;
    }
    
    if ( ! hechizo->dame_hechizo_area() ) {
        __debug_x("Cancelamos en shadow(2)", "vara_tormentas", 0, 1);
        return i;
    }
    
    __debug_x("Disparamos desde shadow", "vara_tormentas", 0, 1);
    vara->disparar_efecto(hechizo, lanzador, objetivo);
    return i;
}
