// Satyr 21.05.2017
// Sierephad Jul 2k20	--	Revisando la formula del descifrar

#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/arma";

#define BLOQUEO      500
#define DURACION     300
#define PORCENTAJE    20

string dame_palabra_poder() {
    string tx;

    if ( tx = query_property("palabra_poder") )
        return tx;

    tx = "";

    while(strlen(tx) < 8 + random(3))
        tx += element_of(explode("abcdefghijklmnopqrstuvwxyz", ""));

    add_property("palabra_poder", tx);
    return tx;
}
string drowfica(string tx) {
    return "/baseobs/lenguajes/drow.c"->entender_lenguaje(tx, 0);
}
void setup()
{
    fijar_arma_base("baston");
    set_name("baston");
    set_short("Bastón del Martirizador");
    set_main_plural("Bastones del Martirizador");

    generar_alias_y_plurales();

    set_long(
        "Una retorcida espiral de metal negro que se eleva a medida que gira sobre si misma hasta llegar a "
            "un bloque de obsidiana que ha sido tallado minuciosamente para tomar la forma de un Wyvern. "
            "La cara reptiliana está adornada con dos ojos de rubí opacos y nublados "
            "que parecen haber perdido el brillo tiempo atrás. La cabeza carece de maxilar inferior, "
            "causando que una lengua, presumiblemente hecha de cuero "
            "ya podrido, cuelgue de su interior. El apéndice rojizo "
            "presenta una inscripción rúnica ininteligible.\n"
    );

    set_read_mess(
        drowfica("Utiliza la palabra de poder " + dame_palabra_poder() + " seguida de una de "
                "tus invocaciones para iniciar el vinculo del martirio."),
        "drow",
        0
    );

    fijar_material(7);
    fijar_encantamiento(30);
    add_static_property("poder_magico", 30);

    fijar_material(2);
//    fijar_valor(7000);
//   fijar_coste_gloria(350);
    setup_limitado_gloria();
}
mixed descifrar_inscripcion(string objeto,object quien)
{
	//Sierephad: como esta el descifrar, solo con inscripcion no se puede.
	//Sieguiendo el Fix de Eckol del anillo de priedra
	
    if ( objeto=="baston" || objeto == "lengua" || objeto == "martirizador" || regexp(objeto, "inscripci(o|ó)n(es)?") )
        return ({
            "Utiliza la palabra de poder '" + dame_palabra_poder() + "'' seguida de una de "
                "tus invocaciones para iniciar el vinculo del martirio.",
            5,
            "drow"
        });
}
object dame_shadow() {
    return clone_object(base_name() + "_sh.c");
}
string descripcion_juzgar(object pj) {
    return "El bastón, templado por la esencia de la tortura y el sufrimiento durante décadas, te permite designar a "
        "una criatura invocada bajo tu control para establecer con ella el vínculo del martirio. Dicho vínculo, "
        "que durará " + pretty_time(DURACION) + ", hará que un porcentaje igual al " + PORCENTAJE + "% de todo el "
        "daño que recibas sea redirigido a tu invocación, que la sufrirá siempre que ésta esté en tu sala y "
        "no esté afectada por hechizos o efectos que le permitan ignorar daño. Este vínculo puede disiparse bajo "
        "el nombre de \"martirio\" y su lanzamiento tiene un bloqueo de " + pretty_time(BLOQUEO) + ".\n"
    ;
}
int terminar_martirio(object pj) {
    if ( ! pj )
        return 0;

    if ( pj->dame_sombra_baston_martirizador() )
        pj->destruir_sombra_baston_martirizador();

    TP->nuevo_bloqueo_combate(TO, BLOQUEO, 0, 1);
    TO->nuevo_bloqueo_combate(TO, BLOQUEO, 0, 1);
    return 1;
}
int martirio(object npc) {
    object sh;

    if ( ! npc )
        return notify_fail("Has de especificar una invocación bajo tu control sobre la que establecer el vínculo del martirio.\n");

    if ( ! npc->dame_invocacion() )
        return notify_fail(npc->query_short() + " no es una invocación.\n");

    if ( npc->dame_invocador() != TP )
        return notify_fail(npc->query_short() + " no está bajo tu control.\n");

    if ( TP->dame_sombra_baston_martirizador() )
        return notify_fail("Ya estás afectad" + TP->dame_vocal() + " por un vínculo del martirio.\n");

    if ( dame_bloqueo_combate(TO) )
        return notify_fail("El poder de " + TU_S(TO) + " se ha agotado temporalmente.\n");

    if ( TP->dame_bloqueo_combate(TO) )
        return notify_fail("Hace demasiado poco que has establecido un vínculo de martirio y no puedes invocar este poder tan pronto.\n");

    if ( ! sh = dame_shadow() )
        return notify_fail("Un error impide invocar el poder del bastón.\n");

    tell_object(TP,
        "Al pronunciar la palabra de poder los ojos de " + TU_S(TO) + " se iluminan y de su "
            "boca partida comienza a emanar una niebla que os envuelve a ti y a " 
            + npc->query_short() + " vinculándoos mediante el martirio.\n"
    );
    tell_accion(environment(TP),
        "Al pronunciar la palabra de poder que despierta a " + SU_S(TO) + ", el arma de "
        + TP->query_cap_name() + " se ilumina y de su boca partida comienzan a emanar unas nieblas "
        "que " + TP->dame_le() + " envuelven a " + TP->dame_pronombre() + " y a " 
        + npc->query_short() + ".\n",
        "",
        ({TP,npc}),
        TP
    );
    sh->setup_sombra(TP, npc, PORCENTAJE);
    TO->nuevo_bloqueo_combate(TO, BLOQUEO);
    TP->nuevo_efecto(
        "martirio",
        DURACION,
        (: terminar_martirio, TP :)
    );
    return 1;
}
void init() {
    ::init();

    anyadir_comando_equipado(
        //"martirizar",
        dame_palabra_poder(),
        "<objeto:pnj:aqui'Una invocación bajo tu control'>",
        (: martirio :)
    );
}
void objeto_desequipado() {
    if ( environment() )
        environment()->quitar_efecto("martirio", 0);
}
