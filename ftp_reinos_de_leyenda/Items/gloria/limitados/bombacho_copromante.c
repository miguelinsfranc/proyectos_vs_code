// Satyr 22.02.2018
#include <pk.h>
#include <combate.h>
#include <objetivos.h>

#define EVASION 30
#define DURACION 18
#define BLOQUEO 180

inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

#define PODER_CEGUERA 10
#define BLOQUEO_CEGUERA 360
#define ALCANCE 1

void setup()
{
    fijar_armadura_base("pantalones");
    set_name("bombacho");

    set_short(
        "%^GREEN%^B%^ORANGE%^ombacho del "
        "%^GREEN%^C%^ORANGE%^opromante%^RESET%^");
    set_main_plural(
        "%^GREEN%^B%^ORANGE%^ombachos del "
        "%^GREEN%^C%^ORANGE%^opromante%^RESET%^");
    // generar_alias_y_plurales();
    set_long(
        "Un par de ajados pantalones que en otra era seguramente sirvieran "
        "para dar un toque de distinción al que fuera su portador; sin duda, "
        "un ambicioso hechicero que tendría tanto poder como enemigos.\n\n"

        "  Sin embargo, a día de hoy no es más que un zarrapastroso pedazo "
        "de tela que está completamente teñido por el color de los "
        "excrementos solidificados que lo mancillan. Y cuando decimos "
        "\"completamente teñido\" nos referimos justo a eso, pues no hay "
        "un ápice de tela que presente otro color que no sea el del detrito, "
        "como si el pantalón se hubiese pasado una centuria sumergido en una "
        "fosa séptica solo para ser recuperado, siglos después, por las "
        "ansiosas manos de un arqueólogo que, seguramente, se las lavase "
        "después.\n\n"

        "  Aunque el aspecto es aterrador, sus propiedades mágicas son "
        "patentes, "
        "pues pertenecía a un poderoso copromante -liches que mueren en fosas "
        "sépticas- y sus poderes aún están patentes en este deslucido "
        "complemento que huele a vómito rancio y tiene un tacto áspero que "
        "te resulta, cuanto menos, desconcertante.\n");

    fijar_BO(7);
    fijar_encantamiento(20);
    setup_limitado_gloria();
}
void objeto_desequipado()
{
    if (environment()) {
        environment()->quitar_estilo("copromante");
        environment()->fijar_estilo("pelea");
        tell_object(
            environment(),
            "[Pierdes el estilo desarmado "
            "'%^ORANGE%^copromante%^RESET%^']%^RESET%^\n");
    }
}
void objeto_equipado()
{
    if (environment()) {
        environment()->nuevo_estilo("copromante");
        environment()->fijar_estilo("copromante");
        tell_object(
            environment(),
            "[Aprendes el estilo desarmado "
            "'%^ORANGE%^copromante%^RESET%^']%^RESET%^\n");
    }
}
string descripcion_juzgar(object ob)
{
    return "Estos bombachos confieren a su portador el estilo desarmado "
           "'copromante', un estilo que hace daño de tipo 'mal' y es "
           "bonificado por el poder mágico del portador. "
           "Adicionalmente permitirá lanzar a un enemigo un... \"proyectil\" "
           "capaz de cegarle durante " +
           pretty_time(PODER_CEGUERA) +
           " si el ataque impacta (este efecto tiene un bloqueo de " +
           pretty_time(BLOQUEO_CEGUERA) + "). El alcance de esta acción " +
           "es de " + ALCANCE + " sala(s).";
}
int arrojar_perdicion(string a_quien)
{
    object *quienes = HANDLER_OBJETIVOS->dame_objetivos(
        a_quien, "one_ranged_npc", TP, 0, (["alcance":ALCANCE]));

    if (!quienes || !sizeof(quienes)) {
        return notify_fail(
            "No ves a nadie con ese nombre para arrojarle nada.\n");
    }

    if (dame_bloqueo_combate(TO)) {
        return notify_fail(
            TU_S(TO) + " aún no ha recuperado su \"poder mágico\".\n");
    }

    if (TP->dame_bloqueo_combate(TO)) {
        return notify_fail(
            "La última vez que hiciste eso te causó una experiencia "
            "desagradable y no crees estar list" +
            TP->dame_vocal() + " para "
                               "repetirlo de nuevo.\n");
    }

    if (quienes[0]->dame_bloqueo_combate(TO, "recibida")) {
        return notify_fail(
            quienes[0]->query_short() +
            " está demasiado alerta "
            "como para que tu \"proyectil\" surta efecto.\n");
    }

    nuevo_bloqueo_combate(TO, BLOQUEO_CEGUERA);
    TP->nuevo_bloqueo_combate(TO, BLOQUEO_CEGUERA);
    TP->attack_ob(quienes[0]);
    quienes[0]->nuevo_bloqueo_combate(TO, 30, 0, 0, "recibida");

    if (1 > TP->dame_ob_estilo()->tirada_impactar(quienes[0], TP)) {
        __hc(TP)->entregar_mensajes(
            TP,
            quienes[0],
            ({sprintf(
                  "¡Recuperas de los bolsillos de %s un montón de detrito, "
                  "pero fallas miserablemente al lanzarlo contra "
                  "%s!",
                  TU_S(TO),
                  quienes[0]->query_cap_name()),

              sprintf(
                  "¡%s te lanza algo que se saca de %s, pero "
                  "falla!",
                  TP->query_cap_name(),
                  SU_S(TO)),

              sprintf(
                  "¡%s lanza algo que se saca de %s contra %s, pero falla!",
                  TP->query_cap_name(),
                  SU_S(TO),
                  quienes[0]->query_cap_name())}),
            1);
        return 1;
    }

    __hc(TP)->entregar_mensajes(
        TP,
        quienes[0],
        ({
            sprintf(
                "¡Recuperas un montón de detrito de los bolsillos de %s y lo "
                "arrojas contra %s, impactándole de lleno y derramando sobre "
                "su "
                "rostro una infamia zarrapastrosa que %s ciega por completo!",
                TU_S(TO),
                quienes[0]->query_cap_name(),
                quienes[0]->dame_lo()),

            sprintf(
                "¡%s se saca algo repugnante de %s y lo arroja contra ti, "
                "impactándote de lleno y derramando sobre tu cara una infamia "
                "zarrapastrosa que te ciega por completo!,",
                TP->query_cap_name(),
                SU_S(TO)),

            sprintf(
                "¡%s se saca algo repugnante de %s y lo arroja contra %s, "
                "impactándo%s de lleno y derramando sobre %s cara una infamia "
                "zarrapastrosa que %s ciega por completo!,",
                TP->query_cap_name(),
                SU_S(TO),
                quienes[0]->query_cap_name(),
                quienes[0]->dame_le(),
                quienes[0]->dame_su(),
                quienes[0]->dame_le()),

        }),
        0);
    quienes[0]->add_timed_property("cegado", 1, PODER_CEGUERA);
    return 1;
}
void init()
{
    ::init();

    anyadir_comando_equipado(
        "derramar",
        "perdicion sobre <texto'Jugador objetivo'>",
        (
            : arrojar_perdicion:),
        "Lanza \"algo\" que tienes en el bombacho contra un oponente con un "
        "alcance de " +
            ALCANCE + " sala(s). Un impacto exitoso cegará a "
                      "tu rival durante " +
            pretty_time(PODER_CEGUERA) + ".\n");
}
