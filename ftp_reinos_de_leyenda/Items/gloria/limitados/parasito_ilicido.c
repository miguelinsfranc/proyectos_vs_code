// Satyr 24/05/2019 01:31
#include <pk.h>
#include <combate.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/armadura";

string dame_nombre_material()
{
    return "carne viva de azotamentes";
}
void setup()
{
    fijar_armadura_base("pendiente");
    set_name("parasito");
    set_short("%^BLACK%^BOLD%^Parásito Ilícido Desdentado%^RESET%^");
    set_main_plural("%^BOLD%^BLACK%^Parásitos Ilícidos Desdentados%^RESET%^");
    generar_alias_y_plurales();

    set_long(
        "Una asquerosa larva del tamaño de uno de tus puños a la que "
        "un hábil artesano, con tanta maña en su oficio como en la "
        "carnicería, ha atravesado con un hilo de oro que permite... ¡¿"
        "usarla como un pendiente?!\n\n"

        "  La babeante criatura es la forma larval de un Azotamentes "
        "a la que han arrancado las afiladas garras que otrora "
        "sobresalían de los racimos de tentáculos que cuelgan de su figura "
        "oronda y brillante. Privada de su principal método para obtener "
        "sustento, el repelente "
        "gusano se ha visto obligado a permanecer en un estado larval del que "
        "no será capaz de salir nunca.\n\n"

        "  Esto permite a un %^CURSIVA%^muy%^RESET%^ ¿valiente? portador "
        "utilizar"
        " la larva como pendiente, cosa que parece una necedad, pero que "
        "causará "
        "que la criatura utilice sus tenues poderes mentales para fortalecer "
        "la capacidad de sugestión de su portador (pues los parásitos siempre "
        "han de debilitar a cualquier posible huésped más propicio que esté "
        "cerca de ellos).\n\n"

        "  En una situación normal esto sería una locura, pues los tentáculos "
        "armados de la criatura se colarían por cualquiera de los suculentos "
        "orificios faciales de "
        "su víctima para hacer que ésta sucumbiese a la ceremorphosis. Puesto "
        "que "
        "sus garras han sido extirpadas, el horror del licuado de cerebros "
        "se ve sustituido por ligeras jaquecas y una húmeda caricia en las "
        "orejas de vez en cuando.\n"

    );

    add_static_property("ts-mental", -5);
    add_static_property("negacion-mental", 10);
    add_static_property("negacion-conjuro", 5);
    fijar_encantamiento(20);
    fijar_BO(9);

    fijar_material(7);
    setup_limitado_gloria();

    add_static_property(
        "messon",
        "Al vestir " + dame_tu() + " " + query_short() +
            " sientes cómo sus recalcitrantes zarcillos "
            "comienzan a tantear el contorno de tu oreja, midiéndola con "
            "detenimiento y casi... saboreándola.\n");
    add_static_property(
        "messoff",
        capitalize(dame_tu()) + " " + query_short() +
            " emite un lastimero quejido cuando lo "
            "retiras de tu suculenta oreja, que ahora está llena de "
            "babas.\n");
}
