// Satyr 05/11/2021 03:25
// Recompensa de gloria de la temporada 35
#include <pk.h>
inherit GLORIA_LIMITADOS + "base_limitados.c";
inherit "/obj/disenyo.c";

void setup()
{
    fijar_oficio("sastre");
    fijar_receta("pantalones del acechador de caminos");
    fijar_lenguaje("kobold");

    setup_limitado_gloria();
}
