fijar_objetos_morir(mixed objetos)

 Esta función permite asignar al PNJ una serie de objetos que serán clonados en su cadáver cuando este se genere (es decir, al morir).

 De esta forma nos evitamos tener que enmascarar do_death() para esta trivialidad y, de paso, permitimos un mayor control.

 La función puede recibir varios parámetros:

  - String  : la ruta de un objeto a clonar
  - Mixed   : Array de tres posiciones, con ruta, cantidad y porcentaje de entregarlo.
  - String *: la ruta de varios objetos a clonar
  - Mapping : un mapping que especificará cantidades y rutas de objetos a clonar
  - Especial: para clonar tesoros

 A continuación detallo cada forma:

 %^BOLD%^WHITE%^Recibiendo un string%^RESET%^
 El PNJ clonará una copia del objeto pasado como parámetro cuando muera
 Ejemplo: fijar_objetos_morir("/baseobs/armas/daga.c");

 %^BOLD%^WHITE%^Recibiendo un array de 3 posiciones%^RESET%^
 Las posiciones corresponden a ruta, cantidad y porcentaje.
 Ejemplo: fijar_objectos_morir(({"/baseobs/armas/daga.c",1,20}))
 Entrega una daga, un 20% de las veces.

 %^BOLD%^WHITE%^Recibiendo un string *%^RESET%^
 El PNJ clonará una copia de cada objeto pasado como parámetro cuando muera.
 Ejemplo: fijar_objetos_morir(({"/baseobs/armas/daga.c", "/baseobs/armaduras/cuero.c"}));

 %^BOLD%^WHITE%^Recibiendo un mapping%^RESET%^
 El formato del mapping será el siguiente: ([ "ruta_1": cantidad, "ruta_2": cantidad ])
 El PNJ clonará tantos objetos de cada ruta como especifiquemos en el mapping.
 En el siguiente ejemplo, el PNJ clonaría 3 platanos y dos fresas.

 Ejemplo: fijar_objetos_morir((["/baseobs/comestibles/fresa.c":2, "/baseobs/comestibles/platano.c":3]));

 %^BOLD%^WHITE%^Formato especial de tesoros%^RESET%^
 El string* tendrá uno de los siguientes formatos:

 1- básico: ({"tesoro", "tipo_tesoro", nivel, cantidad})
 Se clonará un tesoro aleatorio de tipo "tipo_tesoro" (string) y nivel nivel_tesoro (int). En caso de especificar cantidad (int), se clonará ese número de tesoros.

 2- avanzado: ({"tesoro", ({"tipo_1", "tipo_2"}), nivel|({nivel_min, nivel_max}), cantidad)
 Se clonará un tesoro aleatorio de un tipo determinado aleatoriamente por los pasados en el array. El nivel del tesoro podrá especificarse en un entero o en un array de dos posiciones tipo ({nivel_minimo, nivel_maximo}). Al pasarse el ultimo formato, el nivel del tesoro será aleatorio.

 3- porcentajes: ({"objeto", 1, porcentaje})

 Se clonará el "objeto" especificado un porcentaje% de las veces. P.ej., para un 30% de clonar una daga:

 fijar_objetos_morir(({"/baseobs/armas/daga.c", 1, 30}));


 La entrada "tesoro" es obligatoria para que se reconozca.
 Este formato no puede combinarse con el formato mapping.

 Ejemplo: fijar_objetos_morir(({"tesoro", "arma", 9, 2})) // 2 armas de nivel 9

 %^BOLD%^WHITE%^Fijar_objeto_morir, resets e items cargando%^RESET%^

 Un NPC que tenga una entrada en resets (man resets) y que cargue un objeto que también esté en la lista de fijar_objeto_morir tendrá un comportamiento especial.

 Lo que sucederá es que, primeramente, el objeto del inventario del NPC se destruirá y, acto seguido, se clonará en el cuerpo si el reset lo permite.

 Esto es para evitar que un npc que cargue un objeto (por ejemplo, un prisma) no de tesoros por estar en reset pero si de el premio que va cargando.

 Mirar también:
    resets

    nuevo_pj_involucrado_muerte
    dame_pjs_involucrados_muerte

    fijar_objetos_morir
    dame_objetos_morir
    clonar_objetos_morir
    crear_objeto_morir
    mover_a_cuerpo

    proceso_clonar_tesoros_morir
    poner_bloqueo_tesoro_cuentas
    puedo_clonar_objetos_para
    dame_cuentas_pjs_involucrados_muerte_sin_bloqueo
    dame_cuentas_pjs_involucrados_muerte
    dame_cuenta_sin_bloqueo_tesoros
    dame_nombre_bloqueo_tesoro
    log_tesoros_muerte
    limpiar_tesoros_muerte

 Satyr 27/06/2012-01/01/2014-07/01/2015-26/03/2018

