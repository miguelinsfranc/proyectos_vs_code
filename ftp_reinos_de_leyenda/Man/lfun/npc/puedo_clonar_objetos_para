 varargs int puedo_clonar_objetos_para(string *cuentas, object objeto, object cadaver) 
 
 Esta es una lfun enmascarable que puede usarse para personalizar el comportamiento de proceso_clonar_tesoros_morir.
 
 - Si la función devuelve 1, el proceso de clonado de tesoros seguirá normalmente.
 - Si la función devuelve 0, no se clonará ningún tesoro, pero tampoco se pondrá ningún reset.
 
 El primer parámetro es un array de las cuentas involucradas en la muerte del npc.
 El segundo parámetro es el propio objeto del npc.
 El tercer parámetro es el cadáver u otro objeto en el que se meterán los tesoros.
 
 Hay que tener en cuenta, que aún devolviendo 1, se deben pasar los chequeos normales realizados por proceso_clonar_tesoros_morir para que el clonado sea exitoso.
 
 
 Ejemplo de uso (definido en un npc):
 
 varargs int puedo_clonar_objetos_para(string *cuentas, object objeto, object cadaver) 
 {
    // Los muertos-vivientes que hayan sido animados mediante la magia no podrán dar nada con fijar_objetos_morir 
    // para evitar invocar a uno, matarlo, animarlo y repetir
    if ( query_property("muerto-viviente-animado") ) {
        return 0; // no se clonarán tesoros nunca
    }
    
    /*
        Aquí podríamos poner más chequeos, como... 
        
        - Alineación de los astros
        - Nivel de fe del personaje
        - Número de goles marcados por la selección este año
    */
    
    return 1; // se clonarán tesoros si el reset lo permite
 }
 
 Mirar también:
    resets
    
    nuevo_pj_involucrado_muerte
    dame_pjs_involucrados_muerte
        
    fijar_objetos_morir
    dame_objetos_morir
    clonar_objetos_morir    
    crear_objeto_morir
    mover_a_cuerpo

    proceso_clonar_tesoros_morir
    poner_bloqueo_tesoro_cuentas
    puedo_clonar_objetos_para
    dame_cuentas_pjs_involucrados_muerte_sin_bloqueo
    dame_cuentas_pjs_involucrados_muerte
    dame_cuenta_sin_bloqueo_tesoros
    dame_nombre_bloqueo_tesoro
    log_tesoros_muerte
    limpiar_tesoros_muerte

 Satyr 27/06/2012-01/01/2014