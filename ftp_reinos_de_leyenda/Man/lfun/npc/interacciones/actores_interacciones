 Los actores son los PNJs que pueden interactuar en una interacción.
 
 Debido a como funciona el sistema, ten en cuenta que un actor puede ser cualquier PNJ existente sin necesidad de cambiar nada en su código.
 
 El sistema de interacción se basa en una forma de buscar actores en la sala del actor principal de la interacción (el PNJ que tiene la interacción definida) y para ello ofrece varias posibilidades de búsqueda.
 
 %^BOLD%^WHITE%^Buscar desde objetos%^RESET%^
 Si sabemos que hay un objeto que siempre va a ser un actor, podemos usarlo directamente y, si ese está en el entorno del actor principal, será usado de forma normal.
 
 Muy útil cuando queremos que sea el propio actor principal el que realice una acción. En este caso podemos usar this_object() para definir el actor.
 
 Ejemplo de conversación del actor principal en la que él mismo dice "la bolsa o la vida" (fijaros en el this_object()):
    nuevo_paso_interaccion("cv_mercader", this_object(), ({"'¡Eh tú!, ¡la bolsa o la vida!"}) );
    
 %^BOLD%^WHITE%^Buscar por alias%^RESET%^
 Cuando no sepamos a ciencia cierta que PNJ vamos a buscar, podemos usar un alias para encontrar cualquiera en la sala del actor principal que responda a un alias dado. 
 
 Ejemplo en el que buscamos a un actor que responda al nombre de "mercader" para que diga dos frases:
    nuevo_paso_interaccion("cv_mercader", "mercader",
        ({"'Mi señor, ¡no puedo!", "'¡Tengo hijos a los que alimentar!"})
    );

 %^BOLD%^WHITE%^Buscar por ruta%^RESET%^
 Así como buscamos por alias, podemos buscar por ruta. En este caso se buscará un objeto en la room del actor principal cuya ruta sea igual a la que pasamos.
 
 En este ejemplo buscamos a un tipo de mercader en concreto para usar en la interacción:
    nuevo_paso_interaccion("cv_mercader", "/d/aprendizaje/npc/interacciones/mercader.c ",
        ({"'Mi señor, ¡no puedo!", "'¡Tengo hijos a los que alimentar!"})
    );
 
 %^BOLD%^WHITE%^Buscar por función o lfun%^RESET%^
 La última forma, más avanzada, nos permite usar una función o lfun definida en el actor principal para buscar un objetivo.
 
 Dicha función será usada para filtrar a todos los actores válidos en la room del actor principal y recibirá como parámetro cada uno de los actores a recibir (si devuelve 1 el actor será válido y si devuelve 0, no).
 
  En este ejemplo buscamos a un actor kobold en nuestra sala para contestar:
    nuevo_paso_interaccion("dialogo_kobods", (: $1->dame_raza() == "kobold" :),
        ({"'Me gustan los huesos y hacer pipí en las alfombras"})
    );
    
 Satyr 08.11.2016
 