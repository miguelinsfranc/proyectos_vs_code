 Una interacción es una conversación scriptada entre varios PNJs.
 
 En dichas conversaciones un actor principal se encargará de llevar el peso del guión.
 
 El guión está basado en los siguientes conceptos:
 - Actor: Un actor es el PNJ que actuará en cada turno de la interacción. Podremos definir varias formas para elegir a nuestros actores.
 - Turno: La interacción va por turnos. Primero un actor realiza una serie de acciones y, posteriormente, el siguiente actor realiza su turno. 
 - Accion: En cada turno un actor puede realizar varias acciones. Las acciones van desde decir frases a hacer emotes o ejecutar funciones.
 
 Las interacciones son muy útiles para cosas como:
 - Ambientar una zona: dos PNJs hablan de sus preocupaciones, eventos, etc.
 - Dar pistas sobre quests: un grupo de ciudadanos narra algún problema en una conversación privada.
 - Tokens de exploración: puedes aprender algo de una cultura al escuchar a la gente hablar.

 Para entender mejor como funciona supongamos que tenemos un bandido y un mercader que se encuentran en un camino.
 
 Podríamos definir una interacción entre ambos que funcione tal que así:
 
    Bandido dice: ¡Eh tú!, ¡la bolsa o la vida!
    Mercader: Mi señor, ¡no puedo!
    Mercader: ¡Tengo hijos a los que alimentar!
    Bandido escupe al suelo.
    Bandido dice: ¡No lo pienso repetir!, ¡la bolsa o la vida!
    Mercader: ¡Clemencia por favor!
    Bandido dice: ¡Tu lo has querido!
    <se inicia el combate entre mercader y bandido>
    
 En este caso el bandido inicia una interacción a la que contesta el mercader, acto seguido, el bandido vuelve a tomar la iniciativa y el diálogo continúa.
  
 Podéis ver el código y la demostración de esta interacción en vivo poniendo el siguiente comando:
    %^BOLD%^RED%^BLINK%^goto /d/aprendizaje/npc/interacciones/sala_ejemplo.c%^RESET%^
  
 Como nota final para los creadores que estén haciendo zonas, las interacciones son una forma genial de dar color a una ciudad a través de un handler.
 
 Imaginemos que tenemos un handler que clona mercaderes, etc en una ciudad. Usando ese mismo handler podríamos crear interacciones dinámicas que se basen en cosas como el clima, el ciclo dia/noche, los eventos, mandatarios, etc.
 
