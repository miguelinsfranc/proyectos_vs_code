 void deshabilitar(string palabra, mixed propiedad_o_funcion)

  Similar al prohibir, impide que el npc responda a una "palabra clave", pero al revés que el prohibir, ésto pasa cuando el jugador POSEE una cierta "propiedad". Es decir, se puede deshabilitar una palabra de  la conversación colocándole una propiedad al jugador.

  Así, el primer parámetro es la palabra clave a deshabilitar, y el segundo es la propiedad que debe comprobarse. Aunque el segundo parámetro puede interpretarse de cuatro formas, concretamente:

  1) Puede ser una función del npc en cuyo caso ha de devolver un int significando un 1 verdadero (se deshabilita) o 0 falso (NO se habilita).La función recibirá como parámetro el jugador con el que se conversa.
  2) También puede ser una property (static, timed o permanente), como habíamos comentado.
  3) Puede ser un TOKEN de exploración que signifique que ya se ha superado cierta aventura (el TOKEN es la ruta de la sala donde se consigue).
   4) Puede ser el hito / hitos %^BOLD%^WHITE%^de una actual%^RESET%^. En este caso se tienen 2 variantes:
      -) "hito <mision> <hito1> <hito2>...": Válido en cuanto el jugador posee alguno de los hitos.
      -) "multihito <mision> <hito1> <hito2>...": Válido sólo si el jugador posee todos los hitos.
   5) Puede ser el hito / hitos %^BOLD%^WHITE%^de una misión activa y no completada%^RESET%^. Usa el mismo formato que el punto anterior, pero en lugar de "hito" y "multihito" usa las palabras clave "hito_actual" y "multihito_actual".
   5) Comprobar una misión %^BOLD%^WHITE%^actual%^RESET%^, en este caso la sintaxis es "mision <mision>".
   6) Comprobar una misión %^BOLD%^WHITE%^activa y NO completada%^RESET%^, la sintaxis es igual a la anterior pero usando el término "mision_activa".

  Si el segundo parámetro es una función, esta recibirá el jugador a comprobar como parámetro y se evaluará lo que devuelva como si fuese el valor de la propiedad.

  Ejemplo:

  Un caso concreto sería un npc que nos demanda conseguir unos cuantos objetos. Una vez entregados, no nos los tendría que volver a pedir.

  - deshabilitar("objeto1","funcion"); //Se mira la funcion para ver si puede hablar sobre "objeto1", si la función devuelve 1, no conversará sobre "objeto1". Por ejemplo, podría mirar que el npc tuviera el objeto cargando.

  - deshabilitar("objeto1","prop_ya_tengo_objeto1");//Para preguntar por "objeto1" NO han de tener la property "prop_ya_tengo_objeto1".

  - deshabilitar("objeto1","/d/orgoth/zonas/cueva/trono");//Si ya tenemos el TOKEN de haber superado esta quest, entonces ya no podemos volver a preguntar por ella.

  - deshabilitar("objeto1","hito venganza-padre drow_asesino");//Si ya tenemos el hito «drow_asesino» de la misión «venganza-padre» no podemos preguntar por «objeto1».

  - deshabilitar("druida", (: $1->dame_clase() == "druida" :)); // Si somos un druida no podremos preguntar por druida

  Si lo que pretendemos es evitar que el npc converse, 'man chequeo_inicial_cv'.

  Mirar también:
  nuevo_dialogo,
  chequeo_inicial_cv,
  habilitar_conversacion,
  deshabilitar_conversacion,
  prohibir,
  puede_conversar,
  frases_personalizables,
  anyadir_dialogo,

  Ember 13-02-2004
