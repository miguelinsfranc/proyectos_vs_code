varargs void nuevo_dialogo(string palabra_clave, mixed reaccion, string llave, string *respuestas, string pregunta)

Añade un nuevo dialogo al PNJ. Las conversaciones funcionan de la siguiente manera: mediante la función "%^CURSIVA%^nuevo_dialogo%^RESET%^"" se añaden una serie de "palabras clave" (primer parametro) a las que el PNJ responderá con el contenido del parámetro "%^CURSIVA%^reaccion%^RESET%^".

Solo los dos primeros parámetros son obligatorios.

El primer parámetro hace referencia a la palabra clave del diálogo. Esto es la palabra por la que los jugadores han de preguntar al usar el comando "%^CURSIVA%^conversar%^RESET%^".

El segundo parámetro es un array de strings que utilizan el formato de las "%^CURSIVA%^monster_strings%^RESET%^" (ver "%^BOLD%^YELLOW%^man monster_strings%^RESET%^"). Como es un array, cada diálogo puede hacer que un PNJ tenga varias reacciones, las cuales se irán mostrando de forma paulatina (ver "%^BOLD%^YELLOW%^man fijar_retardo_conversaciones%^RESET%^").

El tercer parámetro "%^CURSIVA%^llave%^RESET%^" hace que el diálogo añadido no esté disponible hasta que el jugador no haya conversado sobre el diálogo especificado por el valor del parámetro.

%^BOLD%^YELLOW%^Reacciones especiales de misión%^RESET%^

Además del las cadenas de monstruos normales, nuevo_dialogo permite utilizar cadenas que sigan los siguientes formato:

    1. hito <nombre de una mision> <nombre de un hito>
    2. mision <nombre de la mision> <nombre de un hito>

El primer formato, al ser evaluado, otorgará al conversador y a todo su grupo el hito especificado para la misión dada (ver %^BOLD%^YELLOW%^man misiones%^RESET%^) sin hacer preguntas.

El segundo formato será similar, pero mostrará al jugador la posibilidad de aceptar o rechazar el hito o misión.

%^BOLD%^YELLOW%^Reacciones especiales evaluables%^RESET%^

Cualquier reacción del array puede ser una función expresada de las siguientes formas:

    - Una expresión funcional o función anónima.
    - El nombre de una función definida del PNJ.

En cualquiera de los dos casos su comportamiento será el de ejecutarse recibiendo como parámetro el jugador que está conversando y el string con la palabra clave del diálogo actual.

Cabe destacar que este formato es diferente al formato evaluable de las %^CURSIVA%^monster strings%^RESET%^.

%^BOLD%^WHITE%^UNDERLINE%^La función se ejecuta una vez el PNJ ha respondido%^RESET%^. Esto, en el caso de una reacción grande con varias partes, puede tardar varios segundos, así que conviene hacer la siguiente comprobación en las funciones para asegurarse de que el jugador sigue aún en la misma zona que el PNJ:

    environment(jugador) ==environment(this_object())

%^BOLD%^YELLOW%^Preguntas y respuestas%^RESET%^

Por último, los parámetros 4º (respuestas) y 5º (pregunta) -que son opcionales- pueden ser utilizados para facilitar la creación de un menú de diálogo (%^BOLD%^YELLOW%^man mostrar_dialogo%^RESET%^) que permitan al jugador responder de alguna manera a la reacción del PNJ.

El parámetro "%^CURSIVA%^pregunta%^RESET%^" será el texto de la pregunta que se hará al jugador. Si es 0, se mostrará un mensaje por defecto.

El parámetro "%^CURSIVA%^respuestas%^RESET%^" referencia las posibles respuestas de entre las que podrá elegir el jugador. El texto de la respuesta se usará de la siguiente forma:

    - diálogo: Si el texto es igual a la palabra clave de un diálogo, al elegir dicha respuesta se iniciará el diálogo.
    - función "funcion_<respuesta>": si existe la función llamada "funcion_<respuesta>", ésta será ejecutada recibiendo como parámetros el objecto de jugador que conversa y el string de la respuesta dada.
    - función "funcion_respuestas_<tema>": si existe esta función, ésta será ejecutada recibiendo como parámetros el objecto de jugador que conversa y el string de la respuesta dada.

Puedes modificar las respuestas durante la ejecución de un diálogo mediante la función nuevas_respuestas() (ver man).

%^BOLD%^YELLOW%^Ejemplos%^RESET%^

%^BOLD%^WHITE%^UNDERLINE%^Ejemplo de una conversación básica%^RESET%^:

    nuevo_dialogo("drow", "Los drow son basura.");
    nuevo_dialogo("padre", ({
          ":parece entristecido.",
          "'Los drow lo asesinaron hace varios años.",
          ":hace una pausa dudando.",
          "'La venganza es lo único que me queda.",
          // Añadimos el hito "drow_asesino" de la misión "venganza-padre"
          "hito vengaza-padre drow_asesino",
          // Ejecutamos una función donde $1 es el jugador que conversa
          (: tell_object($1, "Te mira con deseo.") :),
        }),
        // Para que un conversador pueda preguntar por "padre" será
        // necesario haber preguntado antes por "drow"
        "drow"
    );


%^BOLD%^WHITE%^UNDERLINE%^Ejemplo de la ejecución de una función%^RESET%^

    // Se buscará la función "entregar_espada"
    nuevo_dialogo("padre", ({"entregar_espada"}));

    void entregar_espada(object jugador, string tema)
    {
        object espada;

        if(environment(jugador)!=environment()) {
            return;
        }

        if(!espada= present("espada",jugador)) {
        {
            do_command("decir Si me das una espada podré concluir mi venganza.");
            return;
        }

        espada->move(this_object());

        if(environment(espada)!=this_object()) {
            tell_object(jugador,"No lográs entregar la espada a "+query_short()+".\n");
            espada->move(jugador);
            do_command("decir Si me das una espada podré concluir mi venganza.");
            return;
        }

        do_command("decir Gracias por tu espada, me servirá para finalizar mi venganza.");
    }

%^BOLD%^WHITE%^Ejemplo de diálogo con respuestas #1%^RESET%^:

    nuevo_dialogo("aprender", ({
            "'¿Deseas aprender a forrajear?"
        }),
        0,
        ({"Sí","No"}),
        "¿Qué decides?"
    );

    void funcion_respuestas_aprender(object jugador,string respuesta)
    {
        if(respuesta=="Sí") {
            //<-- aquí irían otras comprobaciones para ver si puedes aprender -->
            do_say("Buena elección, has aprendido a forrajear.",0);
            jugador->nueva_habilidad("forrajear");
        }
        else {
	       do_say("Tal vez otro día.",0);
       }
    }

%^BOLD%^WHITE%^Ejemplo de diálogo con respuestas #1%^RESET%^:

    nuevo_dialogo("matar",({
    	":te mira mientras considera algo.",
    	"'¡SI! Seguro que tú puedes acabar con él.",
    	"'Así yo podría acercarme sin ningún tipo de peligro a la zona.",
    	"'¿Qué me dices?",
    	"mision zulk_matar_mastin personaje_informado",
    }));

	Esta configuración al llegar al elemento "mision" mostraría un dialogo al jugador de la siguiente manera.

	¿Quieres aceptar la misión 'Matar al Mastin' de Raghnall, profesor del Éter?
		1) Si     2) No     3) Salir
	Escribe salir para cancelar.

	En caso de respuesta afirmativa, se añadiría dicho hito de la misión al jugador y se ejecutaría la función del PNJ:

        void respuesta_positiva_conversador(object pj, string mision, string hito, string tema)

	En caso de respuesta negativa o al darle a salir, se ejecutaría la función del PNJ:

        void respuesta_negativa_conversador(object pj, string mision, string hito, string tema)


Mirar también:
  prohibir,
  deshabilitar,
  nuevas_respuestas,
  nueva_pregunta,
  chequeo_inicial_cv,
  habilitar_conversacion,
  deshabilitar_conversacion,
  puede_conversar,
  frases_personalizables,
  anyadir_dialogo,
  add_language,
  ciudadanias,
  respuesta_positiva_conversador,
  respuesta_negativa_conversador


Minrock y Rutseg %^BOLD%^BLUE%^Martillo de %^RESET%^GREEN%^Piedra%^RESET%^ [rutsegmp@yahoo.es] RL2 3-IV-2003
Rutseg: Añadida funcionalidad de respuesta 7-IV-2010
%^BOLD%^BLACK%^Sierephad%^RESET%^: Añadida la opcion "mision"  23-Ene-2018
Satyr 23/04/2021 02:31
