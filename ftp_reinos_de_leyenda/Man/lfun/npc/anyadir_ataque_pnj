varargs int anyadir_ataque_pnj(int probabilidad, string|function ataque, string|function condicion, mapping|null opciones)

Esta función añade un ataque al PNJ. Cada turno de combate habrá una probabilidad de ejecutar dicho ataque si el PNJ está en condiciones de hacerlo (p.ej.: si no tiene un bloqueo que se lo impida). Un PNJ solo podrá ejecutar uno de estos ataques por turno.

En este man se denomina "ataque" a una habilidad o hechizo o una función definida por el usuario.

Adicionalmente, el jugador podrá definir condiciones adicionales a sus ataques, que serán evaluadas para que un ataque se considere para el turno o no. Esto da flexibilidad inusitada al sistema para hacer cosas que no sean, necesariamente, ofensivas. Por ejemplo, el PNJ podría proteger a un aliado si no está protegiendo a nadie y este está en la room.

%^YELLOW%^Parámetros%^RESET%^

- %^WHITE%^BOLD%^probabilidad%^RESET%^: un entero de 1 a 100 que representa la posibilidad de que el ataque se ejecute cada turno
- %^WHITE%^BOLD%^ataque%^RESET%^: ataque a realizar. Puede ser uno de tres:
    - (string)   nombre de hechizo o habilidad
    - (function) un puntero a una función
    - (string)   un nombre de una lfun en el PNJ
- %^WHITE%^BOLD%^condicion%^RESET%^: chequeo extra para determinar si el PNJ ejecuta el ataque (si la función devuelve 1) o no (si devuelve 0). Puede ser uno de dos:
    - (function) un puntero a una función
    - (string)   el nombre de una lfun en el PNJ
- %^WHITE%^BOLD%^opciones%^RESET%^: mapping opcional de opciones para el ataque. Ver detalles en la sección %^WHITE%^BOLD%^opciones%^RESET%^

Las funciones del parámetro %^BOLD%^WHITE%^ataque%^RESET%^ recibirán siempre estos parámetros:

    - (*object) %^BOLD%^WHITE%^objetivos%^RESET%^: lista de atacantes del PNJ (ver %^BOLD%^WHITE%^man dame_lista_atacantes%^RESET%^)

Las funciones del parámetro %^BOLD%^WHITE%^condición%^RESET%^ recibirán siempre estos parámetros:

    - (object) %^BOLD%^WHITE%^pnj%^RESET%^   : PNJ que va a realizar la acción (normalmente %^BOLD%^WHITE%^this_object()%^RESET%^)
    - (string) %^BOLD%^WHITE%^nombre%^RESET%^: nombre interno del ataque añadido. Normalmente no quieres tocar esto

%^YELLOW%^Mapping de opciones%^RESET%^

El mapping de opciones permite añadir algunas opciones que alterarán ligeramente el comportamiento del ataque. Cada opción será una clave del mapping. A continuación se enumeran:

- (string) %^BOLD%^WHITE%^nombre%^RESET%^: nombre interno del ataque
- (int)    %^BOLD%^WHITE%^inaudible%^RESET%^: si es %^BOLD%^WHITE%1%^RESET%^, los hechizos añadidos como ataque se formularán con "inaudible"
- (int)    %^BOLD%^WHITE%^objetivo%^RESET%^: entero que indica qué tipo de objetivo ha de seleccionar el PNJ con el ataque. Por defecto se asumirán valores sensibles y esto solo ha de cambiarse en casos extraños. Los valores válidos son:

    - %^BOLD%^WHITE%^1%^RESET%^: Si es de tipo "many","one" o "self" se lo lanza sobre el propio npc.
    - %^BOLD%^WHITE%^2%^RESET%^: Los tipos "one", "area" y "many" se lanzan sobre aliados del npc (add_loved).
    - %^BOLD%^WHITE%^3%^RESET%^: Se lanza sobre un único enemigo los tipos "one", "area", "many" y "toque".
    - %^BOLD%^WHITE%^4%^RESET%^: Se lanza sobre todos los enemigos los tipos "area" y "many".

%^YELLOW%^Valores de retorno%^RESET%^

La función devuelve %^WHITE%^BOLD%^1%^RESET%^ si tiene éxito, o %^BOLD%^WHITE%^0%^RESET%^ en caso contrario.

%^YELLOW%^Ejemplos%^RESET%^

%^BOLD%^WHITE%^Ejemplo 1%^RESET%^: un PNJ con algunas habilidades de combate

    // 30% de probabilidad de ejecutar "golpecertero
     cada turno
    anyadir_ataque_pnj(30, "golpecertero");

    // 60% de probabilidad de ejecutar "barrido" cada turno
    anyadir_ataque_pnj(60, "barrido");

%^BOLD%^WHITE%^Ejemplo 2%^RESET%^: PNJ que gana ataques cuanta más vida le falte

    anyadir_ataque_pnj( 30, "doblegolpe"  , (: dame_porcentaje_pvs() <= 100 :));
    anyadir_ataque_pnj( 60, "golpecertero", (: dame_porcentaje_pvs() <= 60 :));
    anyadir_ataque_pnj( 90, "tajar"       , (: dame_porcentaje_pvs() <= 40 :));
    anyadir_ataque_pnj(100, "inmolacion"  , (: dame_porcentaje_pvs() <= 5 :));

%^BOLD%^WHITE%^Ejemplo 3%^RESET%^: PNJ que ejecuta una función como un ataque si está bajo de vida

    anyadir_ataque_pnj(50, "desvanecerse", (: dame_porcentaje_pvs() <= 25 :));

    function desvanecerse(object *objetivos) {
        tell_accion(
            environment(),
            query_short() + " desaparece en una nube de humo mágico.\n",
            "",
            ({TO}),
            TO
        );
        dest_me();
    }

%^BOLD%^WHITE%^Ejemplo 4%^RESET%^: PNJ que atrapa y desploma

    /**
     * 50% de atrapar, solo si no estoy ya atrapando
     *
     * Cuando estamos atrapados la mudlib nos pone un "spell_effect". No es
     * necesario entender los detalles. Basta decir que si "query_spell_effect("atrapar")
     * devuelve "0" es que no estamos atrapando. Si devuelve otra cosa, es que
     * estamos atrapando.
     *
     * Así pues, los valores de "query_spell_effect("atrapar")" significan:
     *
     * - si es 0: no estamos atrapando, por lo que podemos atrapar, pero no desplomar
     * - si es distinto de 0: estamos atrapando, por lo que no podemos atrapar, pero si desplomar
     */
    anyadir_ataque_pnj(50, "atrapar", (: ! query_spell_effect("atrapar") :));

    // 100% de desplomar, pero solo si estoy atrapando
    anyadir_ataque_pnj(100, "desplomar", (: query_spell_effect("atrapar") != 0 :))

%^BOLD%^WHITE%^Ejemplo 5%^RESET%^: PNJ que se cura a si mismo sin parar

    anyadir_ataque_pnj(100, "curar heridas ligeras"  , 0, (["objetivo": 1]));
    anyadir_ataque_pnj(100, "curar heridas moderadas", 0, (["objetivo": 1]));
    anyadir_ataque_pnj(100, "curar heridas serias"   , 0, (["objetivo": 1]));
    anyadir_ataque_pnj(100, "curar heridas criticas ", 0, (["objetivo": 1]));

%^BOLD%^WHITE%^Ejemplo 6%^RESET%^: PNJ que busca a alguien para protegerle

    // Buscará a satyr y le pedirá protección.
    // Usamos "queue_action" para que se muestren mensajes, pero podríamos
    // usar "fijar_protector"
    anyadir_ataque_pnj(
        100,
        (: queue_action("proteger frodo") :),
        (: present("frodo", environment()) && !es_protegido(present("frodo", environment())) :)
    );

%^BOLD%^WHITE%^Ejemplo 7%^RESET%^: a la inversa, PNJ que busca a un protector

    anyadir_ataque_pnj(
        100,
        (: present("gandalf", environment())->queue_action("proteger frodo") :),
        (: !dame_protector() && present("gandalf", environment()) :)
    );

%^BOLD%^WHITE%^Ejemplo 8%^RESET%^: PNJ que cambia sus ataques según que fase se encuentre

    /**
     * Este ejemplo es la lista de ataques de un PNJ que tiene varias fases, o
     * dicho de otra forma, un PN que se complica a medida que se alarga el
     * combate, o pasan cosas.
     *
     * Supongamos que tenemos un guerrero Draxyar. El guerrero empuña un
     * mandoble. Es un personaje legal y no busca matar al jugador, así que
     * empieza ligero.
     *
     * Un evento de la room podría hacerle pasar a la fase 2 (p.ej.: el jugador
     * insiste en el ataque), donde empieza a usar más habilidades.
     *
     * Otro distinto le pasasría a la fase 3. Quizas podríamos hacer que al "
     * morir", el Draxyar se regenere mágicamente y se encabrone mucho. En la
     * fase 3 ya usa todo su repertorio de ataques.
     *
     * Finalmente, en la fase 4, que se activaría, por ejemplo, cuando el
     * Draxyar esté muy mal de vida, la criatura solo usará su aliento o la
     * curación. Si la curación tiene éxito pues podríamos programar, a mayores
     * , que se baje la fase.
     */

    // Fase 1: combate fácil
    anyadir_ataque_pnj(25, "golpecertero", (: query_property("fase") == 1 :));

    // Fase 2: más agresivo, nuevas habilidades
    anyadir_ataque_pnj(60, "golpecertero", (: query_property("fase") == 2 :));
    anyadir_ataque_pnj(30, "tajar", (: query_property("fase") == 2 :));

    // Fase 3: mucho más agresivo, más habilidades y habilidad definitiva
    anyadir_ataque_pnj(100, "golpecertero", (: query_property("fase") == 3 :));
    anyadir_ataque_pnj(60, "tajar", (: query_property("fase") == 3 :));
    anyadir_ataque_pnj(25, "embestir", (: query_property("fase") == 3 :));
    anyadir_ataque_pnj(5, "aliento", (: query_property("fase") == 3 :));

    // Fase 4: habilidades definitivas y curación a si mismo
    anyadir_ataque_pnj(100, "aliento", (: query_property("fase") == 4 :));
    anyadir_ataque_pnj(100, "curacion", (: query_property("fase") == 4 :), 0, (["objetivo": 1]));

Mirar también:
  dame_hechizo,
  dame_habilidad,
  habilidad,
  quitar_ataque_pnj
  dame_ataques_pnj

Satyr 08/06/2023 01:13
