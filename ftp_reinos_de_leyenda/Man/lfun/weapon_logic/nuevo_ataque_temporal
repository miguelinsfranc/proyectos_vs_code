varargs int nuevo_ataque_temporal(
    string nombre, mixed tiempo, function fin, string tipo, int dados, int caras, int bono,
        int bo, int siempre, int msjs, int secundario
        )

 Añade un nuevo ataque temporal al arma durante una duración determinada.
 
 Al finalizar el ataque ejecutará una función que avisará al portador o, si se especifica, una personalizada.
 
 La duración puede ser tiempo (en segundos) o una clase 'unidad' (unidades.h) cuyo valor sea:
 - "dosis"   : nº de ataques, fallidos o no
 - "dosis-ok": nº de ataques exitosos
 - "dosis-ko": nº de ataques fallados
 
 Si la duración es 0 el temporal será para siempre.
 
 No pueden añadirse ataques temporales con el mismo nombre que ataques normales.
 
 Añadir un ataque temporal ya existente sustituirá sus datos.
 
 Entrada:
  - nombre     (string): Nombre que identificará al ataque.del ataque. 
                          Sin espacios ni acentos.
  - tiempo     (mixed) : duración del ataque (ver más arriba)
  - tipo       (string): Tipo de ataque (cortante, penetrante, acido, fuego, etc).
  - fin        (mixed) : String o function con el método a ejecutar al terminar el efecto
                         (recibirá como parámetro el nombre del ataque)
  - dados      (int)   : Nº de dados de daño.
  - caras      (int)   : Nº de caras de daño.
  - bono       (int)   : Bono de daño.
  - bo         (int)   : DEPRECATED. NO USAR. BO extra del ataque.
  - infalible  (int)   : DEPRECATED. El ataque nunca falla y siempre es crítico.
  - msjs       (int)   : DEPRECATED.
  - secundario (int)   : El ataque se considerará secundario, lo que tiene ciertas
                          implicaciones, como que no se ejecuta si el jugador está
                          muy herido.
 
 Ejemplos:
 
 #1: Añadir un nuevo ataque de ácido (2d60+10) durante 10 segundos
 nuevo_ataque_temporal("temporal-1", 10, 0, "acido", 2, 60, 10);
 
 #2: Añadir un ataque de fuego que se apagará tras 5 impactos exitosos
 nuevo_ataque_temporal("fuego_temporal" __ud(5, "dosis-ok"), 0, "fuego", 2, 60, 10);
 
 #3: Añade un ataque divino que ejecutará una función al terminar tras 5 impactos fallidos
 void avisar_final(string ataque) {
     if (environment())
         tell-object(environment(), "Fallas tantos ataques que el poder del arma desaparece.\n");
 }
 
 nuevo_ataque_temporal(
    "divino_temporal", 
    __ud(5, "dosis-nok"), 
    "avisar_final",
    "fuego", 
    2, 
    60, 
    10
 );
 
 Satyr 05.08.2015

Ver también:
 - dame_ataque
 - dame_ataques
 - dame_ataques_temporales
 - dame_ataques_magicos
 - dame_ataques_fisicos
 - fijar_ataques
 - fijar_dato_ataque
 - dame_danyos_ataque
 - nuevo_ataque
 - quitar_ataque
 - fijar_dato_ataque