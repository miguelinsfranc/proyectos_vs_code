Las zonas explorables se controlan desde la sala %^BOLD%^YELLOW%^/room/admin/control_exploracion%^RESET%^ donde pueden ser dados de alta.

En esa sala se controlan las diferentes zonas explorables del MUD, donde se pueden conseguir "tokens" de exploración.
Cada uno de estos "tokens" de exploración otorgan al jugador una experiencia considerable y el honor de estar entre los mayores exploradores de Eirea.
Para que una zona sea explorable debes añadirla al handler desde la sala de control especificando la ruta de la zona (atención, si mueves la zona alguna vez, tendrás que corregir la ruta en la sala de control). Además tendrás que distribuír 10 "tokens" por esa zona. Estos "tokens" no son más que una simple llamada a la función nuevo_token(jugador) del handler.
Por ejemplo en un add_action y con el #include <exploracion.h>:
EXPLORACION->nuevo_token(this_player());
o simplemente: TOKEN(this_player());
Le dará al jugador un token de la zona, y en el handler se añadirá la room donde se encuentra el add_action. Ten en cuenta que sólo puedes dar tokens desde objetos que se encuentren en el directorio especificado para la zona, que normalmente serán rooms.

Si quieres dar el token desde un archivo que está en un directorio diferente al de la zona (por ejemplo desde un npc) haz lo siguiente:
Define en la room en la que esté el npc (o en cualquier otro .c que esté en el directorio de la zona) una función similar a esta:
#include <exploracion.h>
int dar_token(object jugador)
{
  return TOKEN(jugador);
}
Cuando el npc (u otro objeto) quiera dar el token solo ha de hacer lo siguiente:

environment()->dar_token(jugador) (o "/d/reino/restodelaruta.c"->dar_token(jugador) si el npc no está en la room del token)

%^BOLD%^RED%^Actualización 15/12/2013:%^RESET%^
  Otra forma de dar tokens sin tener que definir la función 'dar_token' en la room,
  es definir la función 'string dame_ruta_exploracion()' en el propio objeto, devolviendo la ruta
  que deba usarse para la exploración, por lo general, la ruta de la room.

  Ejemplo:
    string dame_ruta_exploracion(){ return "/d/anduar/rooms/eloras/tokens/eloras_42"; }

Para comprobar si un jugador tiene un TOKEN determinado, utiliza:

  EXPLORADO_X("ruta",jugador)
O si estás en la sala que da el token, puedes usar simplemente:
  EXPLORADO(jugador)
Si la función devuelve 1 es que el jugador ya tiene ese TOKEN.

Ejemplo:
  #include <exploracion.h>
  
  void completar_quest(object jugador)
  {
    if(!EXPLORADO_X("/d/orgoth/zonas/hithrim/sala_1",jugador))
    {
    	tell_object(jugador,"Has superado la quest y recibes tu premio.\n");
    	TOKEN(j);
    }
    else
    {
      tell_object(jugador,"Tu ya has superado esta quest en otra ocasión, no puedes recibir premio.\n");
    }
  }
