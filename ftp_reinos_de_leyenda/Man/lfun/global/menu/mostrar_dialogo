void mostrar_dialogo(string mensaje, mixed* respuestas,
                     function funcion, function funcion_salida, function funcion_ayu,
                     int modo, string msj_destruccion, string opc_def)

Muestra al usuario un menú con opciones que no se ocultará hasta que seleccione uno de los elementos válidos de la lista. El jugador queda "bloqueado" hasta que introduce una opción correcta. La respuesta es enviada a la función del tercer parámetro. Este método es útil para sistemas interactivos que requieren respuestas concretas, como el sistema de creación de personajes.

Parámetros:
 - mensaje: Mensaje que se mostrará en la cabecera del menú de diálogo.
 - respuestas: Lista de opciones a mostrar al jugador. Puede ser:
   - Array de strings (Modo original): Lista de cadenas de texto que se mostrará al jugador como opciones. (Puede ser un array de ints, en cuyo caso se transformará cada opción a texto)
   - Array de Arrays de strings (Modo nuevo): Lista de arrays de texto con el siguiente formato:
     - Posición 0: Clave de la opción.
     - Posición 1: Descripción de la opción.
   Si no se indica este parámetro, se mostrará el menú con las opciones "Sí y No" por defecto.
 - funcion:
   - Si se indica, cada vez que el jugador pulse sobre una opción, se llamará a esta función pasando el jugador que ejecuta el menú y un texto de la opción (Según lo indicado en el parámetro "modo")
   - Si no se indica nada, tras mostrar el menú de diálogo, no se bloqueará al jugador, que podrá teclear lo que quiera, pero si teclea un número correspondiente a una de las opciones, se ejecutará ésta.
 - funcion_salida:
   - Si se indica, se ejecutará cuando el jugador salga del menú con "salir".
   - Si no se indica nada, al salir del menú no se le mostrará ningún texto al jugador.
 - funcion_ayu: 
   - Si se indica, configurará el menú de diálogo añadiendo un modo de ayuda que podrá lanzar el jugador escribiendo "? X" donde X es el número de opción. Además añadirá un texto explicativo en el menú de como usar la ayuda y modificará cada opción en MXP para añadirle un submenú para lanzar la ayuda con el ratón.
   - Si no se indica nada, la ayuda está inactiva, aunque escriba el jugador el texto de ayuda.
 - modo: Indica qué se enviará como respuesta a la función indicada en el parámetro "funcion".
   Valores:
   - 0 (Valor por defecto): Indica que lo mismo que ve el jugador es lo que se mandará.
   - 1: Indica que se mandará la clave de la opción seleccionada por el jugador.
   - 2: Indica que se mandará un array al jugador con el siguiente formato:
     - Posición 0: Clave de la opción.
     - Posición 1: Descripción de la opción.
 - msj_destruccion: Mensaje que se mostrará si el propietario de las funciones del menú ha sido destruído.
                    Por ejemplo, pongamos que hay un cartel en una room y al leerlo nos muestra 
                    un menú de opciones, y ahora llega un inmo y destruye el cartel mientras el jugador lo lee. 
                    En ese caso, cuando el jugador intente realizar alguna acción con dicho menú 
                    (por ejemplo seleccionar una opción), se le mostrará el mensaje aquí
                    dado y cancelará el menú. Si no se indica mensaje, mostrará uno genérico.
 - opc_def: Si se pulsa ENTER sin introducir ningún valor, usará la opción aquí indicada.
            Si no se usa nada, pues seguirá a la espera de que introduzca un valor.

Todo el código que usa el modelo antiguo de la función, con los parámetros originales (mensaje, respuestas,funcion) seguirán funcionando sin variación, pues los nuevos parámetros simplemente dan otra opción de configuración del menú más personalizada.
Ejemplo de código (Modelo Antiguo):
 ...
 TP->mostrar_dialogo("¿Aceptas la misión?",({"Si","No","Salir"}),(:respuesta_dialogo:));

void respuesta_dialogo(object jugador,string respuesta) //En el modelo antiguo, respuesta siempre era un string.{
  if(respuesta=="Salir")
    return;
  if(respuesta=="No){
    tell_object(jugador,"Misión rechazada.\n");
    return;
  }
}
Ejemplo de código (Modelo Nuevo):
 ...
 TP->mostrar_dialogo("¿Aceptas la misión?",({({"si","Si"}),({"no","No"}),({"salir","Salir"})}),(:respuesta_dialogo:),(:cancelar_menu:),(:ayuda_menu:),1);

void respuesta_dialogo(object jugador,mixed respuesta) //En el modelo nuevo, respuesta puede ser un string, o un array de strings.{
  if(respuesta=="salir") //Se recibe el texto en minúsculas porque el modo indicado en la función ha sido 1 (Devolver clave, que es la posición 0 de los subarrays)
    return;
  if(respuesta=="no){
    tell_object(jugador,"Misión rechazada.\n");
    return;
  }
}

void cancelar_menu(object jugador){
  tell_object(jugador,"Adiós.\n");
}

void ayuda_menu(object jugador, string clave, string descripcion){
  if(clave=="salir")
    tell_object("Con esta opción se cancela el menú.\n");
}

Mirar también:
  menu,
  mostrar_menu,
  dame_en_menu,
  in_input

Snaider RL2 21-II-2010
Zoilder RL2 25-I-2012