 varargs int nueva_ts_tmp(string tipo,int cantidad,int tiempo,function funcion, string efecto)
 
 Añade una bonificación/penalización temporal a las tiradas de salvación de un tipo al objeto sobre el que se llame.
 
 Entrada:
 - tipo     (string)   : Tipo de ts (horror, muerte, paralizacion...)
 - cantidad (int)      : Número de resistencia a otorgar/quitar.
 - tiempo   (int)      : Duración de la resistencia temporal.
 - funcion  (function) : Función a ejecutar al terminar el efecto. Si se pone 0 se usará la función por defecto. %^BOLD%^La función tendrá que limpiar los bonos%^RESET%^.
 - efecto   (string)   : Nombre del efecto que se usará. Si no se especifica se usará "res " +tipo o "deb " + tipo.

 Salida:
 - int: TS temporal al elemento 'tipo' tras el ajuste.
 
 Ejemplo:

    void terminar_bonificacion(object pj) {
        if (pj) {
            tell_object(pj, "Tu bonificación a 'horror' se termina.\n");
            pj->ajustar_ts_tmp("horror", -8);
        }
    }

    void dar_bonificacion() {
        object pj = find_player("satyr");

        pj->nueva_ts_tmp(
            // Tipo de TS a modificar
            "horror",

            // Cantidad a modificar (puede ser positiva o negativa)
            8,

            // Tiempo, en segundos, que durará la bonificación
            300,

            /**
             * Puntero a la función que se evaluará al terminar la bonificación
             * Se le pasará como parámetro "pj"
             */
            (: terminar_bonificacion, pj :),

            /**
             * Nombre del efecto. Podríamos personalizarlo, pero lo dejamos a 0
             * porque no nos interesa. Esto es útil si queréis efectos que
             * puedan ser disipados, pero en el 90% de los casos es mejor
             * dejarlo a 0 u omitirlo.
             */
            0
        )
    }


 Ver también:
 - resistencias
 - nueva_resistencia
 - nueva_resistencia_tmp
 - dame_bono_ts_tmp
 - dame_bono_ts
 - nueva_resistencia_equipo
 - dame_resistencia
 - dame_resistencias
 - dame_resistencia_equipo
 - dame_resistencia_real
 - dame_resistencia_permanente
 
 Satyr 01/06/2017
