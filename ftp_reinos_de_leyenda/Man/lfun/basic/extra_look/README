Los extra_look son descripciones adicionales que se pueden añadir de forma dinámica a un objeto afectado por otro objeto.
La descripción del extra_look se haya definida en otro objeto, y es llamada cada vez que se necesita generar la descripción del objeto afectado. Dicha función que ha de ser incluída se define como:

string extra_look(object afectado)

  Devuelve la descripción dinámica adicional que se añade a la descripción del objeto 'afectado'.

  Ejemplo:
    > mirarme
    Rutseg es un varón enano.
    > exec "/w/rutseg/mal_de_cara.c"->enfadar_jugador(this_player())
    
    //Siendo el objeto mal_de_cara.c como sigue:
    void enfadar_jugador(object jugador)
    {
      jugador->add_extra_look(this_object());
    }
    string extra_look(object afectado)
    {
      if(this_player()==afectado) //El jugador afectado es el que se está mirando
        return "Tienes muy mala cara.\n"; //Atención al salto de línea, hay que hacerlo
      else
        return afectado->dame_pronombre()+" tiene muy mala cara.\n"; //Está mirando otra persona
    }

    > mirame
    Rutseg es un varón enano.
    Tienes muy mala cara.
