varargs int
add_clone_personaje(string personaje, mixed ruta_identidad, int cantidad, function f, function condicion)

Esta función permite añadir un personaje PNJ (ver "man personajes") de forma que éste no sea clonado si alguna de sus otras identidades está en juego.

La función es exactamente idéntica a "add_clone" (man lfun/room/add_clone) con la excepción del primer parámetro que identifica el nombre del personaje a clonar.

Ejemplo de sala que clona una identidad del personaje "Rucualin":

    // Satyr 2014
    inherit "/std/room.c";

    void setup()
    {
        set_short("Sala de pruebas de add_clone_personaje");
        fijar_luz(80);

        add_clone_personaje(
            "Rucualin", "/d/kheleb/npcs/evento_gloignar/rucualin_kheleb.c", 1);
    }

 Ver también:

 - man "add_clone_personaje"
 - man "personajes.c"
 - fichero "personajes.h"

 Satyr 09/01/2020 01:35
