varargs object entregar_objeto(object | string objeto, string mensaje, int cantidad)

 Esta función es la manera ideal de entregar un objeto al jugador.
 
 La función existe en /std/carrying y puede llamarse sobre criaturas o jugadores de la siguiente manera:

 pj->entregar_objeto(item);

 La función intentará entregar al objeto sobre el que se llame el objeto pasado como parámetro.

 Además, se encargará de realizar chequeos tales como que el contenedor destino tiene carga o límite de objetos suficiente. En caso de que no pueda entregarse, el objeto a entregar se moverá al entorno del npc/jugador sobre el que se ha llamado. En cualquiera de los casos, al jugador se le informará de si recibe o no un objeto.

 Si pasamos el parámetro 'mensaje', se mostrará ese mensaje al jugador en lugar del mensaje por defecto ([Recibes objeto]). 
 Al mostrarse al jugador, el texto {objeto} se reemplazará por el objeto a entregar. Ejemplo práctico: si pasamos como parámetro "pepe te da {objeto}", se mostrará: pepe te da cubo.
 Si pasamos una cadena vacía (""), no se mostrará texto.
 
 Si se pasa el parámetro cantidad (%^BOLD%^RED%^sólo cuando objeto sea una string%^RESET%^),
 se entregarán al jugador tantas copias del objeto como se haya indicado.

 Sobre el parámetro 'objeto':
    Si pasamos un objeto, se intentará entregar ese objeto al jugador.
    Si pasamos un string con una ruta, la función intentará clonar primero ese objeto para luego entregarlo.

 Valor de retorno:
    Si no se indica una cantidad o la indicada es 1, se devuelve:
      - Si la función devuelve 0, la entrega del objeto no ha tenido éxito.
      - Si la función tiene éxito, devolverá el objeto entregado (o movido al entorno).

    Si se indica una cantidad, la salida variará:
      - Si no se puede clonar el objeto, devuelve 0.
      - Si puede mover todos los objetos al jugador (o no puede mover ninguno), devuelve un array 
        con cada uno de los objetos entregados o no entregados.
      - Si mueve tanto al jugador como al entorno, devuelve un array de 2 posiciones:
         - Pos. 0: Array de objetos movidos al jugador.
         - Pos. 1: Array de objetos movidos a su entorno.
      

 Ejemplo de uso:

    void funcion() {
        // Buscamos al jugador 'satyr'
        object b = find_player("satyr");
    
        if (b) {
            object daga;

            // Si nos devuelve 0, mostramos un error
            if (!daga = b->entregar_objeto("/baseobs/armas/daga.c")) {
                tell_object(b, "¡Te quedas sin daga!\n");
            }
            else {
                // Aquí podríamos hacer algo con el objeto 'daga' que acabamos de clonar
            }
        }
    }

 La función usa dos ficheros de logs:

   /log/entregar_objeto_ok.log: donde se registran todos los objetos entregados correctamente.
   /log/entregar_objeto_err.log: donde se registran que objetos no han podido ser clonados.

Satyr 09-04-2010
Satyr 27-08-2012
