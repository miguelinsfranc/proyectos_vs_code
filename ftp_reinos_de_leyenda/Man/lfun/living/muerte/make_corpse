object make_corpse()

  Crea un cuerpo/cadáver del personaje, mueve los objetos que llevaba en las manos a la sala, y el resto del equipo y su dinero son movidos al cuerpo/cadáver creado. Se producen mensajes que indican que has muerto y tus armas han caído al suelo.
  Esta función añade al cuerpo/cadáver así creado una serie de propiedades que permiten saber más cosas sobre el propietario, como su nombre, clase, religión, etc... explicadas en 'man cuerpo'.
  Es importante apuntar que llamar a esta función no provoca la muerte del personaje, sólo crea el cuerpo, siendo ésta función llamada durante el proceso de muerte de un personaje jugador o no jugador.
  Por sus características, enmascarar esta función es la forma correcta de crear un objeto cuando muere un NPC y meterlo en el cuerpo, hacer modificaciones a los atributos del cuerpo creado (cambiar el nombre, la descripción...), o incluso evitar que se genere un cuerpo (por ejemplo los fantasmas no generan cuerpo). Si por contra necesitamos crear un cuerpo especial, con funciones extras o diferentes a las del cadáver corriente, enmascararemos la función 'dame_cadaver_especial'.
  Esta función no es adecuada para hacer operaciones lógicas a la muerte de un NPC, para eso consultad 'do_death()'.
  Ejemplos:
  object make_corpse()
  {
    object cuerpo,espada;

    cuerpo= ::make_corpse(); //Llamamos al proceso normal creando un cuerpo
    espada= clone_object("/baseobs/armas/espada_larga"); //Clonamos un objeto
    espada->move(cuerpo);//Movemos el objeto al cuerpo
    //Podemos sacar coloridos mensajes si queremos
    tell_accion(environment(cuerpo),"Una espada que debía estar en el estómago rompe desde el interior las tripas y aflora entre la piel del cadáver.\n","Oyes piel rasgándose.\n",({}),0);
    //Y cambiar la descripción del cuerpo o cualquier otro factor
    cuerpo->set_short("Cadáver ensartado de elefante");
    cuerpo->set_long("El cuerpo muerto de un elefante, el estómago atravesado del cual ha sido rasgado desde dentro.\n");

    return cuerpo; //IMPORTANTE: Devolver el cuerpo muerto generado
  }
  object make_corpse()
  {
    //Imaginemos un NPC fantasma, no queremos que clone cadáver
	//En vez de eso, sólo queremos que salga un mensaje conforme el NPC ha muerto
	tell_accion(environment(this_object()),"La imagen espectral del "+query_short()+" se retuerce y esfuma con un grito aterrador.\n","Oyes un grito aterrador de un ser de ultratumba.\n",({}),0);

	return 0; //Como no creamos cuerpo, en este caso hay que devolver un 0
  }

Mirar también:
  do_death,
  dame_cadaver_especial,
  cuerpo

Rutseg %^BOLD%^BLUE%^Martillo de %^RESET%^GREEN%^Piedra%^RESET%^ [rutsegmp@yahoo.es] RL2
19-IX-2008