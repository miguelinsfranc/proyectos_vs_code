varargs int ajustar_dinero(mixed cantidad,string tipo)

  Esta función permite cambiar el número de monedas de un tipo determinado del personaje.
  Hay dos formas de usarla, pasando un array, en cuyo caso la función utiliza un sólo parámetro, o pasando dos variables, una para la cantidad y otra para el tipo de moneda.
  Si se usa el array, se pueden añadir más de un tipo de moneda, con el formato ({cantidad,"tipo",cantidad2,"tipo2"}).
    Ejemplo:
      ajustar_dinero(({10,"plata",2,"oro"}))

  Si se pasan las dos variables, la primera ha de ser un int que indíca el número de monedas, y la segunda el tipo de las monedas:
    Ejemplo:
      ajustar_dinero(1,"oro");
  Esta es la forma más usada.

  Es muy común utilizar esta función para poner dinero a un PNJ o monstruo en su función de setup(). Ten en cuenta que has de fijar el nivel del personaje antes de ajustarle el dinero, ya que sino saltará el informe de abuso de dinero. Si aún así sigue saltando, reduce la cantidad de dinero que estás poniendo al personaje porque es excesiva para la dificultad del mismo, usa el comando 'informar bug on' para recibir estos mensajes de error.
  Para %^BOLD%^RED%^quitar dinero a un jugador%^RESET%^ utiliza pagar_dinero.

  Los objetos que tienen valor, también incluyen esta función, pero en su caso no añaden monedas al objeto, sino que añaden el valor de esas monedas. Es decir, ajustar_dinero(5,"cobre") añadirá 5 al valor del objeto, mientras que ajustar_dinero(5,"plata") añadirá 50 al valor ya que las monedas de plata valen como 10 cobres.
  El valor de las monedas y los tipos de moneda válidos se establecen en /include/dinero.h

Mirar también:
  valor,
  dame_numero_monedas,
  recalcular_valor,
  fijar_monedas,
  dame_monedas,
  dame_array_dinero,
  fijar_array_dinero,
  pagar_dinero,
  dame_descripcion_monedas

Rutseg %^BOLD%^BLUE%^Martillo de %^RESET%^GREEN%^Piedra%^RESET%^ [rutsegmp@yahoo.es] RL2
27-VII-2005