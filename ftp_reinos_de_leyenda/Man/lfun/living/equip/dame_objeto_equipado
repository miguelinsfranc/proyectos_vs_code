object|*object dame_objeto_equipado(string|function|mixed)

  Esta función puede usarse sobre un jugador o PNJ para saber si está usando o empuñando uno o más objetos.
  
  Acepta varios tipos de parámetros, pudiendo combinarlos en un array para tener varios resultados.
  
  Los tipos aceptados son:
  
  - Rutas de objeto: Un string que apunta a la ruta de un objeto. Se comprobará si el living está usando/empuñando dicho objeto.
  - Function: Se evaluará si la función devuelve 1 (pasándole como parámetro cada objeto equipado/empuñado por el jugador).
  - Lfuns: Un string que no es una ruta se interpretará como un call_other que se llamará sobre cada objeto equipado por el jugador. Si devuelve distinto de 0, éste se devolverá.
  
  Tened en cuenta que si pasamos varios functions o lfuns se hará una suma de resultados, no una intersección de las ocurrencias.
  
  La función devolverá object o *object en función de si se encuentran 1 o más coincidencias en el inventario del jugador.  
  
  Los objetos devueltos se corresponderán con los que el jugador lleva equipado.
  
  Tened en cuenta que si buscáis un arma, es muy posible que tengáis más de un objeto devuelto.
  
  A continuación mostramos varios ejemplos prácticos para ilustrar el funcionamiento de la habilidad:
  

  Ejemplo 1: Comprobar si un clérigo usa su símbolo sagrado y, si lo usa, destruirlo.
  
    object simbolo;
    
    if ( ! simbolo = jugador->dame_objeto_equipado("/baseobs/sagrados/simbolo_" + jugador->dame_religion()) )
        return notify_fail("Necesitas el símbolo sagrado de tu religión para hacer eso.\n");
    
    tell_object(jugador, "¡Tu " + simbolo->query_short() +" estalla en mil pedazos!\n");
    simbolo->dest_me();
  
  
  Ejemplo 2: Requisar armas del jugador en una aduana
  
    object *armas;
    
    if ( armas = jugador->dame_objeto_equipado("dame_arma") ) {
        tell_object(jugador, "La aduana requisa tus armas antes de dejarte pasar.\n");
        armas->move(load_object("/d/algunreino/cofre_aduana.c"));
    }
    
  
  Ejemplo 3: Verificar si el jugador empuña armas mágicas
  
    function pp = function(object arma) { return arma && arma->dame_arma() && arma->dame_encantamiento(); };
    
    if ( ! jugador->dame_objeto_equipado(pp) )
        return notify_fail("Necesitarás armas mágicas para enfrentarte a esa lich.\n");
  
  Satyr 07.04.2015
  
  