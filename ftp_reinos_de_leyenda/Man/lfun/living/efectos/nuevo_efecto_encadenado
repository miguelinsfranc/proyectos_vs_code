void nuevo_efecto_encadenado(string nombre, int intervalo, mixed cadena, function final, int disparos, object shadow, object invocado)

	Añade un nuevo efecto encadenado el personaje. Los efectos encadenados nacen con la idea de poder producir varios efectos comunes en cadena 
	secuencial sin tener que controlar las llamadas a mano y sin usar call_outs.
	
	Por ejemplo, con efectos comunes, si se quiere hacer que a intervalos de 10 segundos el personaje sufra un calambrazo 3 veces habría que poner
	un efecto de 10sg y en la funcion final del efecto dar el calambrazo y volver a poner otro efecto.
	
	Con nuevo_efecto_encadenado basta una simple llamada para hacerlo. 
	  pj->("efecto-calambrazo",10,(:calambrazo:),0,3);
	
	La llamada anterior ejecuta la funcion "calambrazo" 3 veces a intervalos de 10 segundos.
	
	Entrada:
	 - nombre 		(string)	: El nombre del efecto. No puede colisionar con un nombre de efecto común. Si lo hace este acabará y se creará el nuevo efecto encadenado. Al igual que ocurre con los efectos comunes.
	 - intervalo 	(int)		: El tiempo de espera entre cada ejecución de los eslabones de la cadena del efecto.
     - cadena 		(mixed)		: Es un array donde los elementos son los eslabones de cadena, que deven ser punteros a funciones. Si en vez de un array se pasa una funcion, se creará una cadena con sólo ese eslabon. En cada disparo del efecto encadenado se avanzará por la cadena ejecutando las funciones en el orden del array.
	 - final		(function)	: Opcional. Una funcion que se ejecutará inmediatamente tras ejecutar el último eslabon de la cadena.
	 - disparos		(int)		: El numero de veces que se ejecutará uno de los eslabones de la cadena. Si no se pasa o es 0, se ejecutarán tantos como eslabones haya en la cadena. Si se pasan más disparos que eslabones existen en la cadena, el último se repetirá hasta completar los disparos.
	 - shadow 		(object)	: Si el efecto está asociado a una shadow, el objeto shadow será pasado en este argumento.
	 - invocado		(object)	: Si el efecto está asociado a un objeto invocado, el objeto será pasado en este argumento.
	 
    
   Las funciones que conforman los eslabones de la cadena deben devolver un valor entero o void, que se considerará como 0. El valor devuelto indicará si se debe o no avanzar disparando el siguiente eslabón de la cadena y en cuantos pasos. Si devuelve un 0 o void, la cadena volverá a ejecutar el disparo de ese mismo eslabón en el siguiente intervalo. Esto sirve para mantener retenida la progresión de la cadena en un determinado momento que nos interese. Si se devuelve un número 'n' mayor a 0, se pasará al n-ésimo disparo siguiente al actual, ignorando todos los disparos que puedan haber entre medio.
   
   Un ejemplo de prototipo de función eslabón sería:
   
		int calambrazo(object a_quien);
   
   En este caso hay que pasarle el parámetro 'a_quien' en los punteros eslabones de la cadena. 
   Para hacer uso de las propiedades fijadas al efecto dentro de las funciones eslabon, es necesario añadir al final un parametro de tipo mapping de la siguiente forma.
   
		int calambrazo(object a_quien, mapping props);
		
   Sin necesidad de pasar el parametro "props" en el puntero de la cadena, la mudlib añadirá automáticamente el mapping de propiedades que tuviera el efecto como último parametro del eslabón.
   Importante: En los eslabones de la cadena no se pueden consultar la propiedades del efecto con dame_propiedad_efecto porque el efecto ya se ha destruido previamente. La única forma es mediante este parámetro.
   
   
	Ejemplos:

    a) Llamar a una misma función un número determinado de veces.

		Pongamos que tenemos la funcion 'calambrazo' siguiente:

		void calambrazo(object quien) {
		  tell_object(quien,"Sufres un calambrazo\n");
		  quien->ajustar_pvs(-10);
		}    
		
		Si queremos pegar 3 calambrazos seguidos cada 10 segundos usariamos un efecto encadenado asi:
		
		pj->nuevo_efecto_encadenado("efectocalambrazo",10,(:calambrazo,pj:),0,3);
		
		Lo que hará que el personaje vea.
		
		Sufres un calambrazo
		... (10 segundos)
		Sufres un calambrazo
		... (10 segundos)
		Sufres un calambrazo
		
		Cada una de las llamadas a la funcion es lo que se llama un "disparo". En este caso hay 3 disparos que llaman cada uno a la funcion "calambrazo".
		
	b) Llamar a una funcion diferente en cada disparo.
	
		También se puede hacer que en lugar de llamar a la misma función en cada disparo se llame a una diferente. Por ejemplo si creamos una segunda funcion que se llame "espasmo" como la siguiente:
		
		void espasmo(object quien) {
		  tell_object(quien,"Sufres un espasmo\n");
		  quien->bloquear_accion("Estas sufriendo un espasmo",2);
		}
		
		Podríamos querer hacer un efecto que en el primer disparo sufriera un calambrazo y en el segundo un espasmo pasando un array de funciones al parámetro "cadena" de la siguiente forma:
		
		pj->nuevo_efecto_encadenado("calambrazoyespasmo",10,({ (:calambrazo,pj:),(:espasmo,pj:) }),0,2);
		
		Lo que hará que el personaje vea lo siguiente:
		
		Sufres un calambrazo
		... (10 segundos)
		Sufres un espasmo. (Bloquea al jugador 2 segundos)
		
		En el ejemplo anterior, si en vez de pasar el valor 2 al parámetro "disparos" pasasemos un 3, estaría ejecutando la funcion "espasmo" hasta llegar al tercer disparo. Sin necesidad de poner otra vez la funcion "espasmo" como tercer elemento del parametro "cadena". Esto lo podemos extrapolar a los disparos que queramos.
		
		pj->nuevo_efecto_encadenado("calambrazoyespasmo",10,({ (:calambrazo,pj:),(:espasmo,pj:) }),0,3);
		
		El resultado sería:
		
		Sufres un calambrazo
		... (10 segundos)
		Sufres un espasmo. (Bloquea al jugador 2 segundos)		
		... (10 segundos)
		Sufres un espasmo. (Bloquea al jugador 2 segundos)		
	
	Cuestiones importantes: 
	   a) Si el parámetro 'cadena' es un array y no contiene suficientes funciones para cubrir todos los disparos, la ultima función del array será ejecutada tantas veces como haga falta hasta acabar todos los disparos. 
	   
	   b) Si el parámetro 'cadena' es un array con mayor número de elementos a los disparos indicados, los elementos restantes serán ignorados.
	   
	   c) Si se disipa un efecto encadenado o se acaba forzosamente usando la función 'quitar_efecto' se ejecutará inmediatamente la funcion 'final' si se ha establecido y se abortará cualquier disparo que hubiera pendiente.
	   
	   d) Durante la duración de todo el efecto encadenado, es decir, durante todos los intervalos, el efecto encadenado mantendrá las propiedades fijadas por el usuario inicialmente al efecto despues de llamar a 'nuevo_efecto_encadenado'. Como en los efectos comunes, estas propiedades pueden ser consultadas con la funcion 'dame_propiedad_efecto' mientras el efecto esté activo pero nunca dentro de las funciones eslabones de la cadena (disparos) o funcion final de efecto. En tal caso habrá que usar el parámetro 'mapping props' indicado más arriba.
	   
	   e) Existen una serie de propiedades internas del efecto que se pueden usar para realizar comprobaciones adicionales en cada función de la cadena disparada. Para acceder a una de estas propiedades, deberás declarar en la función disparada un parámetro adicional de tipo mapping que las contenga. Este parámetro debe ir al final del prototipo siempre. Si no se añade no estarán disponibles (Ver explicación arriba). Las propiedades internas son:
	   
			_intervalo (int): Tiempo entre cada disparo.
			_disparos_restantes (int): Disparos que quedan por ejecutarse contando el actual.
			_disparos_totales (int): Disparos totales configurados en el efecto encadenado.
			_cadena (*function) : Funciones de la cadena.
			_final (function) : Funcion final de cadena.
		
		Por ejemplo si tenemos la función calambrazo anterior, y queremos usar estas propiedades tendríamos que cambiar la definición por la siguiente:
		
		void calambrazo(object quien, mapping prop) {
		  tell_object(quien,"Sufres un calambrazo\n");
		  quien->ajustar_pvs(-10);
		}    
		
		Para consultar las propiedades usariamos la variable 'prop':
		
			prop["_disparos_restantes"]
					
	    Todas estas propiedades se pueden modificar en los disparos de los eslabones para hacer cosas muy personalizadas pero ten mucho cuidado porque también puedes cometer errores si no te fijas bien. Para la mayoría de los casos no hace falta modificarlas. Un ejemplo podría ser cambiar el tiempo de intervalo a mitad del efecto encadenado, alargar o reducir el número de disparos, o cambiar la función del final.	
	   
Mirar también:
  quitar_efecto,
  dame_efecto,
  dame_contador_t,
  dame_tiempo_efecto,
  dame_funcion_efecto,
  dame_sombra_efecto,
  dame_invocado_efecto,
  add_spell_effect,
  query_effects,
  do_spell_effects,
  disipar_magia,
  nuevo_efecto,
  fijar_propiedad_efecto,
  dame_propiedad_efecto
  
Astel 31.12.2017  
  