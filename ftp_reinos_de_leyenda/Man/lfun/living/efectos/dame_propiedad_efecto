varargs mixed dame_propiedad_efecto(string efecto,string propiedad)

  Devuelve el valor de la propiedad asociada a un efecto. Si el efecto no existe, o ninguna propiedad se ha asignado al efecto (lo más habitual), esta función devolverá 0.
  
  La idea básica es no tener que crear una propiedad paralelamente al efecto, con lo que en redundancia de chequeos eso podría suponer.
  Si lo que quieres es saber si un jugador tiene un determinado efecto encima, usa mejor dame_efecto, salvo que en todos los casos tener el efecto sea sinónimo de tener la propiedad.
  
  Si se pasa un valor como parámetro 'propiedad' se intentará devolver sólo el valor de la clave correspondiente dentro de la propiedad del efecto en caso de que esta fuera fijada como un mapping usando la funcion 'fijar_propiedad_efecto'. De no ser así, cualquier valor del parámetro 'propiedad' será ignorado.
  
  Entrada:
   - efecto		(string)	: Nombre del efecto.
   - propiedad	(string)	: Opcional. Si se pasa, y previamente se ha establecido la propiedad del efecto como un mapping (lista de propiedades), de buscará el valor correspondiente a esta clave en dicho mapping.
   
  Salida:
	- (mixed)	: Si se pasa el parámetro propiedad devolverá el valor de clave correspondiente dentro de todas las propiedades del mapping asociado al efecto. Si no hay mapping asociado al efecto o no se pasa el parámetro 'propiedad', se devolverá cualquier otro tipo de dato asociado como propiedad única del efecto. Ver función fijar_propiedad_efecto.

Mirar también:
  nuevo_efecto,
  quitar_efecto,
  dame_efectos,
  dame_tiempo_efecto,
  dame_funcion_efecto,
  dame_sombra_efecto,
  dame_invocado_efecto,
  dame_propiedad_efecto,
  fijar_propiedad_efecto,
  add_spell_effect,
  query_effects,
  do_spell_effects,
  disipar_magia

Rutseg %^BOLD%^BLUE%^Martillo de %^RESET%^GREEN%^Piedra%^RESET%^ [rutsegmp@yahoo.es] RL2
9-XI-2003


Astel 13.12.2017 