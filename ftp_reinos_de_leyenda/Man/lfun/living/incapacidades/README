Las incapacidades son usadas para conseguir que ciertos efectos persistan incluso cuando el jugador entre y salga del juego.
Hay varios tipos de incapacidades:

    "maldiciones": Efectos mágicos que afectan al jugador como 'petrificar' o 'licantropía'.
    "enfermedades": Efectos de tipo enfermedad como la 'peste', la 'lepra' o un 'resfriado'.
    "incapacidades": Son flaquezas físicas como ser 'manco', 'ciego' o 'sordo'.
    "venenos": Efectos producidos por envenenamiento, como el de las serpientes.
    "heridas": Son daños físicos especiales como 'quemadura', 'congelamiento', 'sangrante', etc...

Cada incapacidad es controlada por un handler que se encuentra en /baseobs/ en un directorio específico para cada tipo de incapacidad, por ejemplo /baseobs/heridas/ para heridas. El handler tiene el mismo nombre que la incapacidad, así el veneno de la incapacidad controlada por /baseobs/venenos/circulatorio_daño.c es conocido como "circulatorio_daño".

Estos handlers necesitan una serie de funciones básicas y otras opcionales para la correcta funcionalidad de la incapacidad:

Básicas-
  void init_curse(object objetivo,int nivel,int duracion,mixed parametros_extra)
    - Es llamada por 'nueva_incapacidad' cuando se le pone a un personaje la incapacidad.
  void curse_end(object objetivo)
    - Es llamada por 'quitar_incapacidad' cuando se le quita a un personaje la incapacidad.
  void player_start(object objetivo)
    - Es llamada cuando se le pone la incapacidad y cada vez que el jugador entra al MUD.
  void player_quit(object herido)
    - Es llamada cuando se le quita la incapacidad y cada vez que el jugador sale del MUD.
  
Opcionales-
  void incapacidad_muerte(object objetivo)
    - Es llamada cuando un personaje afectado por la incapacidad muere, es útil para quitar la incapacidad para que no vuelva a sufrirla recién resucitado, aunque algunas de ellas persistirán.
  void curse_heart_beat(object herido)
    - Esta función es llamada cada 31 heart_beats (62 segundos, 1 minuto aproximadamente...). Desde ella se pueden hacer cosas como daño físico a causa de una herida o un veneno, o el contagio de una enfermedad a personajes de los alrededores.
  int prevent_remove(object objetivo,object curandero)
    - Se llama cuando alguien (el curandero) intenta curar la incapacidad al personaje (objetivo). Si esta función devuelve 0 la incapacidad será curada, cualquier otro valor provoca que la incapacidad persista.
  int dame_resistencia(object objetivo,int nivel,int duracion,mixed parametros_extra)
    - La incapacidad decide si un personaje es resistente a esta. Si el objetivo la resiste, devuelve 1, si es afectable por ella, 0. Es llamada con los mismo parámetros que init_curse.
