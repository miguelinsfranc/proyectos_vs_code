 varargs string nuevo_efecto_basico(
     string nombre, mixed poder, mixed duracion, mixed bloqueo, mixed sms, mixed nosms, string|string*ataque, 
         mixed triggers, mixed params
 )
 
 Permite añadir un nuevo efecto básico a un arma.
 
 Los efectos básicos se encuentran guardados en "/baseobs/efectos/", ver "ayuda efectos" para ver qué hace cada uno.
 
 Para añadir un efecto a un arma "sin complicarse" solo es necesario usar los 4 primeros parámetros: nombre, poder, duracion y bloqueo.
 
 Cualquier valor que no se especifique hará que el efecto tome el valor por defecto que tiene definido en su archivo.
 
 Por ejemplo, para añadir un efecto de vampirismo (20):
 
 > nuevo_efecto_basico("vampirismo", 20)
 
 Si queremos que tenga 10 segundos de bloqueo
 
 > nuevo_efecto_basico("vampirismo", 20, 0, 10)
 
 El parámetro "duración" en ocasiones no tiene sentido ya que algunos efectos no necesitan duración. En esos casos es irrelevante el valor que pasemos.
 
 El resto de parámetros son opciones avanzadas y no es necesario usarlas salvo que sepas muy bien lo que haces.
 
 Entrada:
  - nombre   (string): nombre del efecto (en /baseobs/efectos/)
  - poder    (mixed) : poder del efecto, puede ser int, function o class unidades
  - duracion (mixed) : duración del efecto, puede ser int o function
  - bloqueo  (mixed) : bloqueo del efecto, puede ser int o function
  - sms      (mixed) : mensajes del efecto
  - nosms    (mixed) : nomensajes del efecto (se mostrarán cuando el mensaje no pueda ejecutarse)
  - ataque   (mixed) : ataque al que asociar el efecto, por defecto se asocia al principal
  - triggers (int)  : triggers a los que asociar el efecto, USAR SOLO EN CASOS MUY ESPECIALES
  - params   (mixed) : parámetros extra que pueda necesitar el efecto

 Salida:
  - string: nombre del efecto creado. ¿Para qué es esto?, imagínate que un arma que tiene un efecto "Vampirismo" recibe otro efecto de "Vampirismo" (por ejemplo: una habilidad temporal), para saber cual es cual cada uno tendrá un nombre distinto (en este caso serían: "Vampirismo" y "Vampirismo #2"). El string que devuelve será el nombre del nuevo efecto. Es útil para hacer cosas como:
  
    // Un vampirismo que solo se dispara usar en críticos
    string tx = nuevo_efecto_basico("vampirismo", 20);
    fijar_triggers_efecto(tx, _TRIGGER_IMPACTO_C);
    
    // Lo anterior es equivalente a esto, pero quizas más legible:
    nuevo_efecto_basico("vampirismo", 20, 0, 0, 0, 0, 0, _TRIGGER_IMPACTO_C);
    
 Ver también:
 - crear_efecto_basico
 