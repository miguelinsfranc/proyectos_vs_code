 int add_sound(string|string* alias, string|funcion texto)

 Esta función permite añadir una acción en una sala que será desencadenada cuando un jugador use los verbos "%^CURSIVA%^escuchar%^RESET%^" u "%^CURSIVA%^oír.%^RESET%^" seguidos del alias pasado como primer parámetro.

 El segundo parámetro dictamina qué sucederá cuando un jugador use el verbo con el alias descrito anteriormente. Pueden darse dos opciones:

 Si "%^CURSIVA%^texto%^RESET%^" es un string, se mostrará su valor al jugador, sin más.

 Si "%^CURSIVA%^texto%^RESET%^" es una función esta será ejecutada recibiendo como parámetros el propio jugador y el alias que dispara la sensación.  La función puede devolver:

   - 0     : la acción se considerará fallida.
   - 1     : la acción se considerará exitosa.
   - string: la acción se considerará exitosa y se mostrará el string devuelto
             como mensaje.

 Ejemplos con un texto:

    add_sound("reggaeton", "Una música repetitiva y sosa cuyas líricas, en "
        "otro contexto, podrían considerarse como acoso sexual.\n"
    );

 Ejemplos con una función:

    add_sound("metal", function(object pj, string alias) {
        tell_object(pj, "El sonido de los dioses, bruto a la par que melódico. "
            "Enérgico, pero con una cierta dulzura que trae consigo recuerdos "
            "de una época repleta de heroicidades como las que ya no se ven.\n"
        );

        pj->comprobar_logro("metal", "exploracion", 1);
        return 1;
    });

 Ver también:

   - add_smell
   - add_sound
   - add_taste
   - add_item

Satyr 18/05/2021 01:13
